package com.mtl.service.impl;

import ilog.rules.res.xu.cci.IlrCCIClientFactory;

public class IlrStatelessSessionPOJOTest extends IlrStatelessSessionBaseTest implements IlrStatelessSessionTest
{
  private ClassLoader classLoader;

  public IlrStatelessSessionPOJOTest(IlrSessionFactoryBaseTest sessionFactory, IlrCCIClientFactory clientFactory, ClassLoader classLoader)
  {
    super(sessionFactory, clientFactory);
    this.classLoader = classLoader;
  }

  protected ClassLoader getClassLoader()
  {
    if (this.classLoader == null) {
      return super.getClassLoader();
    }
    return this.classLoader;
  }
}