package com.mtl.service.impl;

import com.ibm.rules.res.message.internal.LocalizedMessageHelper;
import ilog.rules.res.model.IlrPath;
import ilog.rules.res.model.trace.IlrDWTrace;
import ilog.rules.res.model.trace.IlrDWTraceFilterImpl;
import ilog.rules.res.persistence.trace.IlrTraceDAO;
import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionFactoryBase;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.IlrTraceFilter;
import ilog.rules.res.session.impl.IlrSessionResponseImpl;
import ilog.rules.res.session.impl.IlrSessionWarningImpl;
import ilog.rules.res.session.ruleset.IlrExecutionTrace;
import ilog.rules.res.session.ruleset.IlrRuleInformation;
import ilog.rules.res.session.ruleset.IlrTaskInformation;
import ilog.rules.res.session.ruleset.impl.IlrExecutionTraceImpl;
import ilog.rules.res.session.util.IlrSessionLocalization;
import ilog.rules.res.trace.IlrDWTraceImpl;
import ilog.rules.res.xu.cci.IlrCCIRuleEngineClient;
import ilog.rules.util.issue.IlrError;
import ilog.rules.util.issue.IlrErrorException;
import ilog.rules.util.issue.IlrWarning;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.resource.ResourceException;

public class IlrTraceHelperTest
{
  public static final String PROPERTY_VALUE_SEPARATOR = "=";
  public static final String VALUES_SEPARATOR = ",";
  private Map<String, String> rulesetProperties;
  private boolean previousTraceEnabled;
  private Map<String, String> previousFilters;
  private final IlrDWTraceFilterImpl savedDWFilter = new IlrDWTraceFilterImpl();
  private Map<String, String> serializedInputParams = null;
  private List<String> inOutParamsSerializationFilters;
  protected IlrCCIRuleEngineClient client;
  protected IlrSessionFactoryBaseTest sessionFactoryBase;

  public static Map<String, String> extractBAMFiltersProperties(String theBAMFilter)
    throws IlrSessionException
  {
    if (theBAMFilter == null) {
      throw new IllegalArgumentException();
    }
    if (theBAMFilter.endsWith("=")) {
      throw new IlrSessionException(IlrSessionLocalization.SESSION_BAM_EXCEPTION_INVALID_FILTERS, new String[] { theBAMFilter });
    }
    Map returnedValue = new HashMap();

    String[] filters = theBAMFilter.split("=");
    if (filters.length > 1) {
      int currentIndex = 0;
      String propertyName = filters[currentIndex];
      currentIndex++;
      while (currentIndex < filters.length) {
        String nextToken = filters[currentIndex];
        currentIndex++;
        if (currentIndex == filters.length) {
          returnedValue.put(propertyName, nextToken);
        } else {
          String[] tokens = nextToken.split(",");
          if (tokens.length == 1) {
            throw new IlrSessionException(IlrSessionLocalization.SESSION_BAM_EXCEPTION_INVALID_FILTERS, new String[] { theBAMFilter });
          }
          StringBuffer rebuiltValue = new StringBuffer();
          for (int i = 0; i < tokens.length - 1; i++) {
            rebuiltValue.append(tokens[i]);
            if (i < tokens.length - 2) {
              rebuiltValue.append(",");
            }
          }
          returnedValue.put(propertyName, rebuiltValue.toString());
          propertyName = tokens[(tokens.length - 1)];
        }
      }
    }
    else {
      throw new IlrSessionException(IlrSessionLocalization.SESSION_BAM_EXCEPTION_INVALID_FILTERS, new String[] { theBAMFilter });
    }

    return returnedValue;
  }

  public IlrTraceHelperTest(IlrCCIRuleEngineClient client, IlrSessionFactoryBaseTest sessionFactoryBase) throws ResourceException {
    this.client = client;
    this.rulesetProperties = client.getRulesetArchiveProperties();
    this.sessionFactoryBase = sessionFactoryBase;
  }

  public boolean isBamEnabled() throws ResourceException {
    String s = (String)this.rulesetProperties.get("monitoring.enabled");
    return (s != null) && (s.equals("true"));
  }

  public void prepareTrace(IlrSessionRequest request)
    throws IlrSessionException
  {
    this.previousTraceEnabled = request.isTraceEnabled();
    if (request.getTraceFilter() != null) {
      this.previousFilters = request.getTraceFilter().toMap();
    }

    request.setTraceEnabled(true);
    mergeAPIAndDWFiltersAndSaveDWFilters(request);

    String inOutFilters = (String)this.rulesetProperties.get("monitoring.inout.filters");
    if (inOutFilters != null) {
      this.inOutParamsSerializationFilters = new ArrayList();
      String[] splittedSerFilters = inOutFilters.split("[;,]");
      this.inOutParamsSerializationFilters.addAll(Arrays.asList(splittedSerFilters));
    }
  }

  private void mergeAPIAndDWFiltersAndSaveDWFilters(IlrSessionRequest request)
    throws IlrSessionException
  {
    String bamFilters = (String)this.rulesetProperties.get("monitoring.filters");
    if ((bamFilters == null) || ("".equals(bamFilters)))
    {
      request.getTraceFilter().setInfoAllFilters(Boolean.valueOf(true));
      this.savedDWFilter.setInfoAllFilters(Boolean.valueOf(true));
    } else {
      Map<String, String> traceFilters = extractBAMFiltersProperties(bamFilters);
      this.savedDWFilter.loadFromProperties(traceFilters);
      addInputOutputFiltersIfNotAlreadyThere(this.savedDWFilter);
      
      
      for (Map.Entry<String, String>entry : traceFilters.entrySet()) {
        String filterName = (String)entry.getKey();
        String filterValue = (String)entry.getValue();

        if (Boolean.parseBoolean(filterValue)) {
          request.getTraceFilter().setFilter(filterName, filterValue);
        }

      }

      request.getTraceFilter().setFilter("INFO_INET_ADDRESS", "true");
      this.savedDWFilter.setFilter("INFO_INET_ADDRESS", "true");

      request.getTraceFilter().setFilter("INFO_RULESET_PROPERTIES", "true");
      this.savedDWFilter.setInfoRulesetProperties(Boolean.valueOf(true));
    }
  }

  private void addInputOutputFiltersIfNotAlreadyThere(IlrDWTraceFilterImpl filter) {
    if (!filter.getFilters().containsKey("INFO_INPUT_PARAMETERS"))
      filter.setInfoInputParameters(Boolean.valueOf(true));
    if (!filter.getFilters().containsKey("INFO_OUTPUT_PARAMETERS"))
      filter.setInfoOutputParameters(Boolean.valueOf(true));
  }

  public void persistTrace(IlrSessionRequest request, IlrSessionResponse response)
    throws IlrSessionException
  {
    IlrTraceDAO traceDao = null;
    try {
      traceDao = this.sessionFactoryBase.getTraceDAO();
      if (traceDao == null) {
        addTraceDaoWarning(response, IlrSessionLocalization.SESSION_BAM_TRACE_DAO_NULL, null, null);
      } else {
        traceDao.beginTransaction();
        traceDao.saveTrace(toDWTrace(request, response), request, response);
        traceDao.commit();
      }
    } catch (Throwable t) {
      addTraceDaoWarning(response, IlrSessionLocalization.SESSION_BAM_TRACE_DAO_CREATION_EXCEPTION, new String[] { t.toString() }, t);
      if (traceDao != null)
        traceDao.rollback();
    }
    finally {
      if (traceDao != null)
        traceDao.close();
    }
  }

  private void addTraceDaoWarning(IlrSessionResponse response, String messageCode, String[] messageParams, Throwable errorCause)
  {
    IlrSessionWarningImpl w = new IlrSessionWarningImpl();
    w.setCode(messageCode);
    String message = IlrSessionLocalization.HELPER.getLocalizedMessage(messageCode, messageParams);
    w.setMessage(message);
    if (errorCause != null) {
      w.setErrorCause(errorCause);
    }
    List warnings = response.getWarnings();
    if (warnings != null)
      warnings.add(w);
  }

  public void filterTrace(IlrSessionRequest request, IlrSessionResponse response)
  {
    request.setTraceEnabled(this.previousTraceEnabled);
    IlrTraceFilter traceFilter = request.getTraceFilter();
    if (this.previousFilters != null) {
      Map props = new HashMap();
      props.putAll(this.previousFilters);
      traceFilter.loadFromProperties(props);
    }
    IlrExecutionTrace execTrace = response.getRulesetExecutionTrace();
    if (execTrace != null)
      if ((!request.isTraceEnabled()) && ((response instanceof IlrSessionResponseImpl)))
      {
        IlrSessionResponseImpl responseImpl = (IlrSessionResponseImpl)response;
        responseImpl.setRulesetExecutionTrace(null);
      } else if ((execTrace instanceof IlrExecutionTraceImpl))
      {
        IlrExecutionTraceImpl traceImpl = (IlrExecutionTraceImpl)execTrace;
        applyFilter(traceImpl, traceFilter);
      }
  }

  public IlrDWTrace toDWTrace(IlrSessionRequest request, IlrSessionResponse response) throws IlrSessionException
  {
    IlrDWTraceFilterImpl traceFilter = this.savedDWFilter;
    IlrExecutionTrace trace = response.getRulesetExecutionTrace();
    IlrExecutionTrace execInfos;
    if ((trace instanceof IlrExecutionTraceImpl)) {
      IlrExecutionTraceImpl newTrace = new IlrExecutionTraceImpl();
      newTrace.putAll((IlrExecutionTraceImpl)trace);
      applyFilter(newTrace, traceFilter);
      execInfos = newTrace;
    } else {
      execInfos = trace;
    }
    IlrPath requestRulesetPath = request.getRulesetPath();
    IlrPath executedRulesetPath = response.getCanonicalRulesetPath();
    Map rsProperties = new HashMap();
    rsProperties.putAll(this.rulesetProperties);
    String userData = null;
    if (request.getUserData() != null) {
      userData = request.getUserData().toString();
    }
    long nbRules = -1L;

    Collection ruleNames = new ArrayList();
    if (traceFilter.isInfoRules().booleanValue()) {
      Collection<IlrRuleInformation> rules = execInfos.getRules().values();
      for (IlrRuleInformation ruleInfo : rules) {
        ruleNames.add(ruleInfo.getName());
      }
      nbRules = rules.size();
    }

    long nbTasks = -1L;
    Collection<IlrTaskInformation> tasks = null;
    Collection taskNames = new ArrayList();
    if (traceFilter.isInfoTasks().booleanValue()) {
      tasks = execInfos.getTasks().values();
      for (IlrTaskInformation taskInfo : tasks) {
        taskNames.add(taskInfo.getName());
      }
      nbTasks = tasks.size();
    }

    String executionOutput = response.getRulesetExecutionOutput();
    String executionId = response.getExecutionId();

    Map outParams = null;

    if (this.savedDWFilter.isInfoOutputParameters()) {
      outParams = new HashMap();
      String bomSupportEnabledProp = (String)rsProperties.get("ruleset.bom.enabled");
      Map out = response.getOutputParameters();
      if ((bomSupportEnabledProp == null) || (bomSupportEnabledProp.equals("false")))
      {
        outParams = serializeParametersToString(out);
        addTraceDaoWarning(response, IlrSessionLocalization.SESSION_BAM_TRACE_DAO_BOM_NOT_ENABLED, null, null);
      }
      else {
        try {
          Map bomOutParams = this.client.getParametersAsBOM((byte) 2, this.inOutParamsSerializationFilters, true);
          outParams = serializeParametersToString(bomOutParams);
        }
        catch (ResourceException e) {
          outParams = serializeParametersToString(out);
          try {
            String error = printStackTrace(e);
            outParams.put("error", error);
          }
          catch (Exception ee) {
          }
          addTraceDaoWarning(response, IlrSessionLocalization.SESSION_BAM_TRACE_DAO_BOM_SERIALIZATION, new String[] { e.toString() }, e);
        }
      }
    }

    return new IlrDWTraceImpl(execInfos, requestRulesetPath, executedRulesetPath, this.savedDWFilter.isInfoRulesetProperties() ? this.rulesetProperties : null, userData, nbRules == -1L ? null : Long.valueOf(nbRules), nbTasks == -1L ? null : Long.valueOf(nbTasks), executionOutput, this.serializedInputParams, outParams, executionId);
  }

  public boolean isInfoInputParameters()
  {
    return this.savedDWFilter.isInfoInputParameters();
  }

  private void applyFilter(IlrExecutionTraceImpl trace, IlrTraceFilter traceFilter)
  {
    Map filter = traceFilter.toMap();
    for (Iterator iterator = trace.entrySet().iterator(); iterator.hasNext(); ) {
      String key = (String)((Map.Entry)iterator.next()).getKey();
      if ((!filter.containsKey(key)) || (!Boolean.parseBoolean((String)filter.get(key))))
        iterator.remove();
    }
  }

  public String printStackTrace(Throwable t)
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PrintStream pw = new PrintStream(baos);
    if (t != null) {
      try {
        printStackTrace(t, pw);
      } catch (Exception e) {
        t.printStackTrace(pw);
        pw.flush();
      }
      pw.flush();
    }
    try {
      baos.close();
      return baos.toString("UTF-8"); } catch (Exception e) {
    }
    return baos.toString();
  }

  public void printStackTrace(Throwable t, PrintStream ps) throws UnsupportedEncodingException, IOException
  {
    if ((t instanceof IlrErrorException)) {
      IlrErrorException ex = (IlrErrorException)t;
      Collection<IlrError> errors = ex.getErrors();
      for (IlrError e : errors) {
        String message = e.getMessage() + "\n";
        ps.write(message.getBytes("UTF-8"));
        if (e.getSourceException() != null) {
          StackTraceElement[] trace = e.getSourceException().getStackTrace();
          for (int j = 0; j < trace.length; j++) {
            ps.println("\tat " + trace[j]);
          }
          Throwable ourCause = t.getCause();
          if (ourCause != null) printStackTrace(ourCause, ps); 
        }
      }
      Collection<IlrWarning> warnings = ex.getWarnings();
      for (IlrWarning w : warnings) {
        String message = w.getMessage() + "\n";
        ps.write(message.getBytes("UTF-8"));
        if (w.getSourceException() != null) {
          StackTraceElement[] trace = w.getSourceException().getStackTrace();
          for (int j = 0; j < trace.length; j++) {
            ps.println("\tat " + trace[j]);
          }
          Throwable ourCause = t.getCause();
          if (ourCause != null) printStackTrace(ourCause, ps); 
        }
      }
      return;
    }
    String message = t.getMessage();
    if (message != null)
      ps.write(message.getBytes("UTF-8"));
    else {
      ps.write(t.toString().getBytes("UTF-8"));
    }
    ps.println();
    StackTraceElement[] trace = t.getStackTrace();
    for (int i = 0; i < trace.length; i++) {
      ps.println("\tat " + trace[i]);
    }
    Throwable ourCause = t.getCause();
    if (ourCause != null) printStackTrace(ourCause, ps); 
  }

  public Map<String, String> serializeParametersToString(Map<String, Object> params)
  {
    Map map = new HashMap();
    for (String key : params.keySet()) {
      Object value = params.get(key);
      String s = value == null ? null : value.toString();
      map.put(key, s);
    }
    return map;
  }

  public void setSerializedInputParams(Map<String, String> serializedInputParams) {
    this.serializedInputParams = serializedInputParams;
  }

  public List<String> getInOutParamsSerializationFilters() {
    return this.inOutParamsSerializationFilters;
  }
}
