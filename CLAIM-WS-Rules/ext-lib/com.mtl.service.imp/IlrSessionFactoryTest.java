package com.mtl.service.impl;

import ilog.rules.res.model.IlrPath;
import ilog.rules.res.persistence.trace.IlrTraceDAOException;
import ilog.rules.res.persistence.trace.IlrTraceDAOFactory;
import ilog.rules.res.session.IlrManagementSession;
import ilog.rules.res.session.IlrSessionCreationException;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrStatefulSession;

import java.io.Serializable;
import java.util.Map;

public abstract interface IlrSessionFactoryTest
{
  public abstract IlrStatelessSessionTest createStatelessSession()
    throws IlrSessionCreationException;

  public abstract IlrStatefulSession createStatefulSession(IlrPath paramIlrPath, Serializable paramSerializable, Map<String, Object> paramMap, boolean paramBoolean)
    throws IlrSessionCreationException;

  public abstract IlrSessionRequest createRequest();

  public abstract IlrManagementSession createManagementSession()
    throws IlrSessionCreationException;

  public abstract boolean isInterceptorEnabled();

  public abstract void setInterceptorEnabled(boolean paramBoolean);

  public abstract IlrTraceDAOFactory createTraceDAOFactory()
    throws IlrTraceDAOException;
}