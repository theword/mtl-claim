package com.mtl.service.impl;

import ilog.rules.res.model.IlrPath;
import ilog.rules.res.model.IlrRuleAppInformation;
import ilog.rules.res.session.IlrSessionFactory;
import ilog.rules.res.session.interceptor.IlrSessionInterceptorException;

import java.io.Serializable;
import java.util.Map;

public abstract interface IlrSessionInterceptorTest
{
  public abstract IlrPath transformRsPath(IlrPath paramIlrPath, IlrRuleAppInformation paramIlrRuleAppInformation, Serializable paramSerializable, Map<String, Object> paramMap, IlrSessionFactoryTest paramIlrSessionFactory)
    throws IlrSessionInterceptorException;

  public abstract void beforeExecute(IlrInterceptorContextTest paramIlrInterceptorContext)
    throws IlrSessionInterceptorException;

  public abstract void afterExecute(IlrInterceptorContextTest paramIlrInterceptorContext)
    throws IlrSessionInterceptorException;
}