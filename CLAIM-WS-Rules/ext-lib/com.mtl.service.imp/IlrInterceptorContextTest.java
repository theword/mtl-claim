package com.mtl.service.impl;

import ilog.rules.res.model.IlrMutableRepository;
import ilog.rules.res.model.IlrMutableRuleAppInformation;
import ilog.rules.res.model.IlrPath;
import ilog.rules.res.model.IlrRepositoryFactory;
import ilog.rules.res.model.IlrRuleAppInformation;
import ilog.rules.res.session.IlrManagementSession;
import ilog.rules.res.session.IlrSession;
import ilog.rules.res.session.IlrSessionFactory;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;

public class IlrInterceptorContextTest
{
  private IlrSessionRequest request;
  private IlrSessionResponse response;
  private IlrSession session;
  private IlrSessionFactoryTest sessionFactory;
  private IlrRuleAppInformation ruleAppInformation;
  private IlrPath initialRsPath;

  public IlrInterceptorContextTest(IlrPath initialRsPath)
  {
    this.initialRsPath = initialRsPath;
  }

  public IlrSessionRequest getRequest()
  {
    return this.request;
  }

  public void setRequest(IlrSessionRequest request)
  {
    this.request = request;
  }

  public IlrSessionResponse getResponse()
  {
    return this.response;
  }

  public void setResponse(IlrSessionResponse response)
  {
    this.response = response;
  }

  public IlrSessionFactoryTest getSessionFactory()
  {
    return this.sessionFactory;
  }

  public void setSessionFactory(IlrSessionFactoryTest sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }

  public IlrRuleAppInformation getRuleAppInformation()
  {
    if (this.ruleAppInformation == null) {
      try {
        IlrPath path = this.initialRsPath;
        IlrManagementSession managementSession = this.sessionFactory.createManagementSession();
        IlrRepositoryFactory repositoryFactory = managementSession.getRepositoryFactory();
        IlrMutableRuleAppInformation mutableRuleAppInfo;
        if (path.getRuleAppVersion() == null)
          mutableRuleAppInfo = (IlrMutableRuleAppInformation)repositoryFactory.createRepository().getGreatestRuleApp(path.getRuleAppName());
        else {
          mutableRuleAppInfo = (IlrMutableRuleAppInformation)repositoryFactory.createRepository().getRuleApp(path.getRuleAppName(), path.getRuleAppVersion());
        }
        this.ruleAppInformation = repositoryFactory.unmodifiableRuleApp(mutableRuleAppInfo);
      } catch (Exception e) {
        this.ruleAppInformation = null;
      }
    }
    return this.ruleAppInformation;
  }

  public void setRuleAppInformation(IlrRuleAppInformation ruleAppInformation)
  {
    this.ruleAppInformation = ruleAppInformation;
  }

  public IlrSession getSession()
  {
    return this.session;
  }

  public void setSession(IlrSession session)
  {
    this.session = session;
  }
}