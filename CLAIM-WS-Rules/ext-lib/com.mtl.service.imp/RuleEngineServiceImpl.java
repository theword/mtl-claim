package com.mtl.service.impl;

import ilog.rules.res.model.IlrFormatException;
import ilog.rules.res.model.IlrPath;
import ilog.rules.res.session.IlrPOJOSessionFactory;
import ilog.rules.res.session.IlrSessionCreationException;
import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionFactory;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.IlrStatelessSession;
import ilog.rules.res.session.ruleset.IlrExecutionEvent;
import ilog.rules.res.session.ruleset.IlrExecutionTrace;
import ilog.rules.res.session.ruleset.IlrRuleEvent;
import ilog.rules.res.session.ruleset.IlrRuleInformation;
import ilog.rules.res.session.ruleset.IlrTaskEvent;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.mtl.service.RuleEngineService;

public class RuleEngineServiceImpl implements RuleEngineService {
	private IlrSessionFactoryTest sessionFactory = null;
	private IlrSessionResponse response = null;
	private Gson gsonService;
	
	Logger log = Logger.getLogger(this.getClass());
	
	public Object getOutputParameter(String key) {
	    return this.response.getOutputParameters().get(key);
	}

	public boolean executeRule(String rulesetPath, Map<String,Object> inParam) {
		boolean r = false;
	    try {
		    // get a rulesession
		    if (this.sessionFactory == null) {
				 this.sessionFactory = new IlrPOJOSessionFactoryTest();
			}

		    // Create a session request object
		    IlrSessionRequest sessionRequest = sessionFactory.createRequest();
		    log.debug("Execute rulesetPath=" + rulesetPath);
		    System.out.println("Execute rulesetPath=" + rulesetPath);
		    sessionRequest.setForceUptodate(true);
		    sessionRequest.setRulesetPath(IlrPath.parsePath(rulesetPath));
		    // Enable trace to retrieve info on executed rules
		    
		    sessionRequest.setTraceEnabled(false);;//TeTe for test performance
		    
		    sessionRequest.getTraceFilter().setInfoAllFilters(true);
		    // Set the input parameters for the execution of the rules
		    Map<String,Object> inputParameters = sessionRequest.getInputParameters();
		    for(Map.Entry<String, Object> entry : inParam.entrySet()) {
		    	inputParameters.put(entry.getKey(), entry.getValue());
		    }
			IlrStatelessSessionTest session = sessionFactory.createStatelessSession();
		    // execute and get the response for this request
			System.out.println("Before execute");
		    this.response = session.execute(sessionRequest);
		    System.out.println("Rule executed");
		    r = true;
		} catch (IlrFormatException e) {
			e.printStackTrace();
		} catch (IlrSessionCreationException e) {
			e.printStackTrace();
		} catch (IlrSessionException e) {
			e.printStackTrace();
		}
	    
	    return r;
	}

	public Long getTotalRulesFired() {
	    return this.response.getRulesetExecutionTrace().getTotalRulesFired() ;
	}
	
	public List<String> getRulesFired() {
	    IlrExecutionTrace sessionTrace = this.response.getRulesetExecutionTrace();
	    List<String> rulesFired = new ArrayList<String>();
		if (sessionTrace!=null) {
			List<String> rules = getRulesExecuted (sessionTrace);
			rulesFired.addAll(rules);
		}
		return rulesFired;
	}

	/**
	 *
	 * @param trace
	 * @return the list of rules executed
	 */
	public List<String> getRulesExecuted (IlrExecutionTrace trace) {
		List<String> firedRuleBusinessNames = new ArrayList<String>  ();
		Map<String,IlrRuleInformation> allRules = trace.getRules();
		List<IlrExecutionEvent> executionEvents = trace.getExecutionEvents();
		if (executionEvents != null && allRules != null) {
		        String taskName = null;
			addRuleFiredBusinessNames(taskName, trace, executionEvents, firedRuleBusinessNames);
      }
		return firedRuleBusinessNames;
	}

	/**
	 * @param trace
	 * @param executionEvents
	 * @param firedRuleBusinessNames
	 */
	protected void addRuleFiredBusinessNames(String taskName, IlrExecutionTrace trace,
			List<IlrExecutionEvent> executionEvents,
			List<String> firedRuleBusinessNames) {
		Map<String, IlrRuleInformation> allRules = trace.getRules();
		if (executionEvents != null && allRules != null) {
			for (IlrExecutionEvent event : executionEvents) {
				if (event instanceof IlrRuleEvent) {
					String ruleName = allRules.get(event.getName()).getBusinessName();
					firedRuleBusinessNames.add(formatTrace(ruleName, taskName));
				} else {
					List<IlrExecutionEvent> subEvents = ((IlrTaskEvent) event).getSubExecutionEvents();
					IlrTaskEvent taskevent = (IlrTaskEvent)event;
					addRuleFiredBusinessNames(taskevent.getName(), trace, subEvents,	firedRuleBusinessNames);
				}
			}
		}
	}
	
    protected String formatTrace(String ruleName, String taskName) {
	String format 		= "Rule: \"{0}\" fired in rule task: \"{1}\"";
	Object[] arguments 	= {ruleName, taskName};
	return MessageFormat.format(format,arguments);
    }
    
    public String toJson(Object src){
    	return gsonService.toJson(src);
    }
    
    public <T> T fromJson(String json,Class<T> classOfT){
    	return gsonService.fromJson(json, classOfT);
    }
    
    //setter
    public void setGsonService(Gson gsonService) {
		this.gsonService = gsonService;
	}
    
}
