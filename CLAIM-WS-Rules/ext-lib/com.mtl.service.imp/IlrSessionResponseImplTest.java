package com.mtl.service.impl;

import ilog.rules.res.model.IlrFormatException;
import ilog.rules.res.model.IlrPath;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.IlrWarning;
import ilog.rules.res.session.ruleset.IlrExecutionTrace;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class IlrSessionResponseImplTest
  implements IlrSessionResponse
{
  private static final long serialVersionUID = 1L;
  private Map<String, Object> outputParameters = new HashMap();
  protected Map<String, Object> inputParameters = new HashMap();
  private String canonicalRsPathStr;
  private Serializable connectionId;
  private Serializable userData;
  private String interceptorClassNameUsed;
  private List<IlrWarning> warnings = new ArrayList();
  private String outputString;
  private Properties rulesetProperties;
  private String executionId;
  private IlrExecutionTrace executionTrace;

  public Map<String, Object> getInputParameters()
  {
    return this.inputParameters;
  }

  public Map<String, Object> getOutputParameters()
  {
    return this.outputParameters;
  }

  public Object getOutputParameter(String name) {
    return this.outputParameters.get(name);
  }

  public IlrPath getCanonicalRulesetPath()
  {
    IlrPath res = null;
    if (this.canonicalRsPathStr != null)
      try {
        res = IlrPath.parsePath(this.canonicalRsPathStr);
      }
      catch (IlrFormatException e)
      {
      }
    return res;
  }

  public Serializable getConnectionId() {
    return this.connectionId;
  }

  public void setConnectionId(Serializable connectionId) {
    this.connectionId = connectionId;
  }

  public Serializable getUserData() {
    return this.userData;
  }

  public void setUserData(Serializable userData) {
    this.userData = userData;
  }

  public List<IlrWarning> getWarnings() {
    return this.warnings;
  }

  public void setWarnings(List<IlrWarning> warnings) {
    this.warnings = warnings;
  }

  public String getInterceptorClassNameUsed() {
    return this.interceptorClassNameUsed;
  }

  public void setInterceptorClassNameUsed(String interceptorClassNameUsed) {
    this.interceptorClassNameUsed = interceptorClassNameUsed;
  }

  public String getRulesetExecutionOutput() {
    return this.outputString;
  }

  public void setOutputString(String outputString) {
    this.outputString = outputString;
  }

  public Properties getRulesetProperties() {
    return this.rulesetProperties;
  }

  public void setRulesetProperties(Properties rulesetProperties) {
    this.rulesetProperties = rulesetProperties;
  }

  public void setCanonicalRsPathStr(String canonicalRsPathStr) {
    this.canonicalRsPathStr = canonicalRsPathStr;
  }

  public String getExecutionId() {
    return this.executionId;
  }

  public void setExecutionId(String executionId) {
    this.executionId = executionId;
  }

  public IlrExecutionTrace getRulesetExecutionTrace() {
    return this.executionTrace;
  }

  public void setRulesetExecutionTrace(IlrExecutionTrace executionTrace) {
    this.executionTrace = executionTrace;
  }

  public void setOutputParameters(Map<String, Object> outputParameters) {
    this.outputParameters = outputParameters;
  }
}