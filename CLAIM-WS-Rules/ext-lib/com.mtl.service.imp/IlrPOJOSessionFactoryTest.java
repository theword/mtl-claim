package com.mtl.service.impl;

import ilog.rules.res.model.IlrPath;
import ilog.rules.res.session.IlrManagementSession;
import ilog.rules.res.session.IlrSessionCreationException;
import ilog.rules.res.session.IlrStatefulSession;
import ilog.rules.res.session.impl.pojo.IlrManagementSessionPOJO;
import ilog.rules.res.session.impl.pojo.IlrStatefulSessionPOJO;
import ilog.rules.res.session.impl.pojo.IlrStatelessSessionPOJO;
import ilog.rules.res.session.util.IlrJ2EEConnectionFactoryFinder;
import ilog.rules.res.xu.cci.IlrCCIClientFactory;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Map;

import javax.resource.cci.ConnectionFactory;

public class IlrPOJOSessionFactoryTest extends IlrSessionFactoryBaseTest
{
  private WeakReference<ConnectionFactory> connectionFactoryRef;
  private String xuConnectionFactoryJndiName = "java:comp/env/eis/XUConnectionFactory";
  private ClassLoader classLoader;
  protected transient WeakReference<IlrCCIClientFactory> clientFactoryRef = null;

  public IlrStatelessSessionTest createStatelessSession() throws IlrSessionCreationException
  {
    try {
      return new IlrStatelessSessionPOJOTest(this, getClientFactory(), getClassLoader()); 
      } 
    catch (Exception e) {
    	  throw new IlrSessionCreationException(e);
    }
  }

  public IlrStatefulSession createStatefulSession(IlrPath rulesetPath, Serializable userData, Map<String, Object> initParameters, boolean forceUptodate)
    throws IlrSessionCreationException
  {
    try
    {
    	return null;
     // return new IlrStatefulSessionPOJO(getClientFactory(), this, rulesetPath, userData, initParameters, forceUptodate);
    }
    catch (Exception e)
    {
    	throw new IlrSessionCreationException(e);
    }

  }

  public IlrManagementSession createManagementSession()
    throws IlrSessionCreationException
  {
    return new IlrManagementSessionPOJO(getClientFactory());
  }

  public ClassLoader getClassLoader()
  {
    if (this.classLoader == null) {
      return super.getClassLoader();
    }

    return this.classLoader;
  }

  public void setClassLoader(ClassLoader classLoader)
  {
    this.classLoader = classLoader;
  }

  public String getXuConnectionFactoryJndiName()
  {
    return this.xuConnectionFactoryJndiName;
  }

  public void setXuConnectionFactoryJndiName(String xuConnectionFactoryJndiName)
  {
    this.xuConnectionFactoryJndiName = xuConnectionFactoryJndiName;
  }

  public ConnectionFactory getConnectionFactory()
  {
    if (this.connectionFactoryRef != null) {
      return (ConnectionFactory)this.connectionFactoryRef.get();
    }
    return null;
  }

  public void setConnectionFactory(ConnectionFactory connectionFactory)
  {
    this.connectionFactoryRef = new WeakReference(connectionFactory);
  }

  protected synchronized IlrCCIClientFactory getClientFactory()
    throws IlrSessionCreationException
  {
    IlrCCIClientFactory clientFactory = null;
    if (this.clientFactoryRef != null) {
      clientFactory = (IlrCCIClientFactory)this.clientFactoryRef.get();
    }
    if (clientFactory == null)
    {
      ConnectionFactory connectionFactory = getConnectionFactory();
      if (connectionFactory != null)
        try {
          clientFactory = new IlrCCIClientFactory(connectionFactory);
        } catch (Exception e) {
          throw new IlrSessionCreationException(e);
        }
      else {
        try
        {
          connectionFactory = IlrJ2EEConnectionFactoryFinder.findConnectionFactory(this.xuConnectionFactoryJndiName);

          setConnectionFactory(connectionFactory);
          clientFactory = new IlrCCIClientFactory(connectionFactory);
        } catch (Exception e) {
          throw new IlrSessionCreationException(e);
        }
      }
      this.clientFactoryRef = new WeakReference(clientFactory);
    }
    return clientFactory;
  }
}