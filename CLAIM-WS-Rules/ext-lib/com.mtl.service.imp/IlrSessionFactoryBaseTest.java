package com.mtl.service.impl;

import ilog.rules.res.persistence.trace.IlrTraceDAO;
import ilog.rules.res.persistence.trace.IlrTraceDAOException;
import ilog.rules.res.persistence.trace.IlrTraceDAOFactory;
import ilog.rules.res.persistence.trace.impl.IlrTraceDAOFactoryProvider;
import ilog.rules.res.session.IlrSessionCreationException;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.impl.IlrSessionRequestImpl;
import ilog.rules.res.session.util.IlrClassLoaderUtil;
import ilog.rules.res.xu.cci.IlrCCIClientFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public abstract class IlrSessionFactoryBaseTest
  implements IlrSessionFactoryTest
{
  private boolean interceptorEnabled = false;
  private IlrTraceDAOFactory traceDAOFactory = null;
  public static final String DVS_DW_PROPERTIES = "dvs-dw.properties";
  public static final String DEFAULT_DVS_DW_PROPERTIES = "default-dvs-dw.properties";
  private final Object lock = new Object();

  public boolean isInterceptorEnabled() {
    return this.interceptorEnabled;
  }

  public void setInterceptorEnabled(boolean interceptorEnabled) {
    this.interceptorEnabled = interceptorEnabled;
  }

  public ClassLoader getClassLoader()
  {
    return IlrClassLoaderUtil.getThreadClassLoader();
  }

  public IlrSessionRequest createRequest() {
    IlrSessionRequestImpl requestImpl = new IlrSessionRequestImpl();
    requestImpl.setInterceptorEnabled(this.interceptorEnabled);
    return requestImpl;
  }

  protected abstract IlrCCIClientFactory getClientFactory()
    throws IlrSessionCreationException;

  public IlrTraceDAO getTraceDAO()
    throws IlrTraceDAOException
  {
    synchronized (this.lock)
    {
      if (this.traceDAOFactory == null) {
        try
        {
          this.traceDAOFactory = createTraceDAOFactory();
        } catch (Throwable t) {
          if ((t instanceof IlrTraceDAOException)) {
            throw ((IlrTraceDAOException)t);
          }
          throw new IlrTraceDAOException(t);
        }
      }
    }

    return this.traceDAOFactory.createDAO(); } 
  public IlrTraceDAOFactory createTraceDAOFactory() throws IlrTraceDAOException { Map configParams = new HashMap();
    ClassLoader classLoader = getClassLoader();
    Properties prop;
    Iterator i$;
    try { InputStream inStream = classLoader.getResourceAsStream("dvs-dw.properties");
      if (inStream == null) {
        inStream = classLoader.getResourceAsStream("/dvs-dw.properties");
      }
      if (inStream == null) {
        inStream = classLoader.getResourceAsStream("default-dvs-dw.properties");
      }
      if (inStream == null) {
        inStream = classLoader.getResourceAsStream("/default-dvs-dw.properties");
      }
      prop = new Properties();
      prop.load(inStream);
      configParams = new ConcurrentHashMap();

      for (i$ = prop.keySet().iterator(); i$.hasNext(); ) { Object obj = i$.next();
        String key = (String)obj;
        configParams.put(key, prop.getProperty(key));
      }
    }
    catch (Throwable t)
    {
    }
    return new IlrTraceDAOFactoryProvider().createTraceDaoFactory(configParams, classLoader);
  }
}