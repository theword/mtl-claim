package com.mtl.service.impl;

import ilog.rules.res.xu.cci.IlrResourceWarningListener;
import java.util.ArrayList;
import javax.resource.cci.ResourceWarning;

class IlrWarningListenerImplTest
  implements IlrResourceWarningListener
{
  protected ArrayList<ResourceWarning> warnings = new ArrayList();

  public void resourceWarningRaised(ResourceWarning warning) {
    this.warnings.add(warning);
  }

  public ArrayList<ResourceWarning> getWarnings() {
    return this.warnings;
  }

  public void clearWarnings() {
    this.warnings.clear();
  }
}