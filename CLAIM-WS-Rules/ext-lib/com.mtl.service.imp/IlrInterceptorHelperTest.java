package com.mtl.service.impl;

import ilog.rules.res.model.IlrPath;
import ilog.rules.res.model.IlrRuleAppInformation;
import ilog.rules.res.session.IlrSession;
import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionFactory;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.impl.IlrSessionResponseImpl;
import ilog.rules.res.session.impl.IlrStatefulSessionBase;
import ilog.rules.res.session.interceptor.IlrSessionInterceptorException;
import ilog.rules.res.session.util.IlrClassLoaderUtil;
import ilog.rules.res.session.util.IlrSessionLocalization;
import ilog.rules.res.util.classloader.IlrClassLoader;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class IlrInterceptorHelperTest
{
  private IlrInterceptorContextTest context;
  private List<IlrSessionInterceptorTest> interceptorChain;
  private IlrPath rsPath;

  public void initialize(IlrSessionRequest request, IlrSession session, IlrSessionFactoryTest sessionFactory)
    throws IlrSessionInterceptorException
  {
    this.rsPath = request.getRulesetPath();
    this.context = new IlrInterceptorContextTest(this.rsPath);
    this.context.setRequest(request);
    this.context.setSession(session);
    this.context.setSessionFactory(sessionFactory);
    initChain();
  }

  public void initialize(IlrPath rsPath, IlrSession session, IlrSessionFactoryTest sessionFactory)
    throws IlrSessionInterceptorException
  {
    this.rsPath = rsPath;
    this.context = new IlrInterceptorContextTest(this.rsPath);
    this.context.setSession(session);
    this.context.setSessionFactory(sessionFactory);
    initChain();
  }

  private void initChain() throws IlrSessionInterceptorException
  {
    this.interceptorChain = new ArrayList();

    IlrRuleAppInformation ruleAppInfo = this.context.getRuleAppInformation();
    if (ruleAppInfo == null) {
      throw new IlrSessionInterceptorException(IlrSessionLocalization.SESSION_INTERCEPTOR_RULEAPP_NOT_FOUND, new String[] { this.rsPath.toString() });
    }
    String interceptorClassProperty = ruleAppInfo.getProperties().getInterceptorClassName();
    String[] classNames = null;
    if (interceptorClassProperty != null) {
      classNames = interceptorClassProperty.split("[,;]");
    }
    if (classNames != null)
      for (String line : classNames)
        if ((!line.equals("")) && (!line.startsWith("#"))) {
          String interceptorClassName = line;

          ClassLoader classLoader = IlrClassLoaderUtil.getThreadClassLoader();
          Class clazz = IlrClassLoaderUtil.loadClassNoException(interceptorClassName, classLoader);
          if (clazz == null)
          {
            byte[] xomBytes = ruleAppInfo.getXOM();
            ClassLoader xomClassLoader = null;
            if ((xomBytes != null) && (xomBytes.length > 0)) {
              try {
                xomClassLoader = new IlrClassLoader(classLoader, xomBytes);
              } catch (IOException e) {
                throw new IlrSessionInterceptorException(e);
              }
            }
            if ((xomClassLoader == null) && 
              ((this.context.getSession() instanceof IlrStatefulSessionBase))) {
              try {
                xomClassLoader = ((IlrStatefulSessionBase)this.context.getSession()).getManagedXOMClassLoader();
              }
              catch (IlrSessionException e)
              {
              }
            }
            if (xomClassLoader != null) {
              try {
                clazz = xomClassLoader.loadClass(interceptorClassName);
              } catch (ClassNotFoundException e) {
                throw new IlrSessionInterceptorException(e);
              }
            }
          }
          if (clazz == null)
          {
            throw new IlrSessionInterceptorException(IlrSessionLocalization.SESSION_INTERCEPTOR_CLASS_NOT_FOUND, new String[] { interceptorClassName });
          }

          try
          {
            Object o = clazz.newInstance();
            IlrSessionInterceptorTest interceptor = (IlrSessionInterceptorTest)o;
            this.interceptorChain.add(interceptor);
          }
          catch (Exception e) {
            throw new IlrSessionInterceptorException(e);
          }
        }
  }

  public void invokeBeforeInterceptors()
    throws IlrSessionInterceptorException
  {
    for (IlrSessionInterceptorTest interceptor : this.interceptorChain)
      interceptor.beforeExecute(this.context);
  }

  public IlrSessionResponse invokeAfterInterceptors(IlrSessionResponse response)
    throws IlrSessionInterceptorException
  {
    this.context.setResponse(response);

    List reversedChain = new ArrayList(this.interceptorChain);
    Collections.reverse(reversedChain);
    StringBuffer interceptorClassNames = new StringBuffer();
    for (Iterator iterator = reversedChain.iterator(); iterator.hasNext(); ) {
      IlrSessionInterceptorTest interceptor = (IlrSessionInterceptorTest)iterator.next();
      interceptor.afterExecute(this.context);
      appendInterceptorClassName(interceptorClassNames, iterator, interceptor);
    }

    IlrSessionResponse responseFromContext = setInterceptorClassNamesInResponse(interceptorClassNames);
    return responseFromContext;
  }

  private IlrSessionResponse setInterceptorClassNamesInResponse(StringBuffer interceptorClassNames) {
    IlrSessionResponse responseFromContext = this.context.getResponse();
    if (interceptorClassNames.length() == 0)
      return responseFromContext;
    if ((responseFromContext instanceof IlrSessionResponseImpl)) {
      ((IlrSessionResponseImpl)responseFromContext).setInterceptorClassNameUsed(interceptorClassNames.toString());
    }

    return responseFromContext;
  }

  private void appendInterceptorClassName(StringBuffer interceptorClassNames, Iterator iterator, IlrSessionInterceptorTest interceptor)
  {
    interceptorClassNames.append(interceptor.getClass().getCanonicalName());
    if (iterator.hasNext())
      interceptorClassNames.append(";");
  }

  public IlrPath invokeTranformPathInterceptors(IlrPath path, Serializable userData, Map<String, Object> parameters)
    throws IlrSessionInterceptorException
  {
    IlrPath res = path;
    for (IlrSessionInterceptorTest interceptor : this.interceptorChain) {
      res = interceptor.transformRsPath(res, this.context.getRuleAppInformation(), userData, parameters, this.context.getSessionFactory());
    }
    return res;
  }

  public IlrInterceptorContextTest getContext() {
    return this.context;
  }
}