package com.mtl.service.impl;

import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrTraceFilter;
import ilog.rules.res.session.impl.IlrSessionResponseImpl;
import ilog.rules.res.session.ruleset.impl.IlrExecutionTraceHelper;
import ilog.rules.res.session.ruleset.impl.IlrExecutionTraceImpl;
import ilog.rules.res.session.util.IlrTraceHelper;
import ilog.rules.res.xu.cci.IlrCCIRuleEngineClient;
import ilog.rules.res.xu.cci.IlrConnectionId;
import ilog.rules.res.xu.cci.ruleset.IlrRulesetExecutionTrace;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.resource.ResourceException;

public class IlrSessionImplTest
{
  protected IlrSessionResponseImplTest execute(IlrCCIRuleEngineClient client, IlrSessionRequest request, IlrTraceHelperTest traceHelper)
    throws IlrSessionException
  {
	System.out.println("begin IlrSessionResponseImpl execute");
	
	System.out.println("1");
    IlrTraceFilter filter = request.getTraceFilter();
    System.out.println("2");
    IlrSessionResponseImplTest response = new IlrSessionResponseImplTest();
    System.out.println("3");
    Map inParams = request.getInputParameters();
    System.out.println("4");
    String taskName = request.getTaskName();
    System.out.println("5");
    boolean bomAccess = request.isBOMAccess();
    System.out.println("6");

    if (request.isTraceEnabled()) {
    	System.out.println("7");
      Properties filterProperties = traceFilterToXUFilterProperties(filter);
      System.out.println("8");
      int filterMask = traceFilterToFilterMask(filter);
      System.out.println("9");
      try {
    	  System.out.println("10");
        client.enableRulesetExecutionInformation(filterMask, filterProperties);
        System.out.println("11");
      } catch (ResourceException re) {
    	  System.out.println("12");
        throw new IlrSessionException(re);
      }
      System.out.println("13");
    }
    System.out.println("14");
    boolean askForNumRulesFired = !request.isTraceEnabled();
    System.out.println("15");
    IlrConnectionId connectionId;
    System.out.println("16");
    try {
    	System.out.println("17");
      Map rulesetProperties = client.getRulesetArchiveProperties();
      System.out.println("18");
      Map parametersOut = execute(client, rulesetProperties, taskName, inParams, bomAccess, askForNumRulesFired, traceHelper);
      System.out.println("19");
      response.getOutputParameters().putAll(parametersOut);
      System.out.println("20");
      response.setOutputString(client.getOutput());
      System.out.println("21");
      response.setUserData(request.getUserData());
      System.out.println("22");
      String canonicalRsPath = client.getCanonicalRulesetPath();
      System.out.println("23");
      response.setCanonicalRsPathStr(canonicalRsPath);
      System.out.println("24");
      Properties p = new Properties();
      System.out.println("25");
      p.putAll(rulesetProperties);
      System.out.println("26");
      response.setRulesetProperties(p);
      System.out.println("27");
      connectionId = client.getConnectionId();
      System.out.println("28");
      response.setConnectionId(connectionId);
      System.out.println("29");
    } catch (ResourceException e) {
    	System.out.println("30");
      throw new IlrSessionException(e);
    }
    System.out.println("31");
    System.out.println("trace status : >>" + request.isTraceEnabled() + "<<");
    if (request.isTraceEnabled()){
    	System.out.println("32");
      try
      {
    	  System.out.println("33");
    	  IlrRulesetExecutionTrace testTrace = client.getRulesetExecutionTrace();
    	  System.out.println("33.1");
    	  IlrTraceFilter tempFilter = request.getTraceFilter();
    	  System.out.println("33.2");
    	  IlrExecutionTraceImpl testHelper = new IlrExecutionTraceHelperTest().createSessionTrace(testTrace,tempFilter);
    	  System.out.println("33.3");
        response.setRulesetExecutionTrace(testHelper);
        System.out.println("34");
      }
      catch (Exception e)
      {
    	  System.out.println("35");
        throw new IlrSessionException(e);
      }
    }
    else {
    	System.out.println("36");
      response.setRulesetExecutionTrace(null);
    }
    System.out.println("37");
    String assignedId = request.getExecutionId();
    System.out.println("38");
    String actualId = assignedId;
    System.out.println("39");
    if (assignedId == null) {
    	System.out.println("40");
      actualId = connectionId.getUUID().toString();
      System.out.println("41");
    }
    System.out.println("42");
    response.setExecutionId(actualId);
    System.out.println("43");
    System.out.println("end IlrSessionResponseImpl execute");
    
    return response;
  }

  private Map<String, Object> execute(IlrCCIRuleEngineClient client, Map<String, String> rulesetProperties, String taskName, Map<String, Object> inputParameters, boolean bomParameters, boolean askForNumRulesFired, IlrTraceHelperTest traceHelper)
    throws ResourceException
  {
	  System.out.println("begin Map<String, Object> execute ");
    if (inputParameters != null) {
    	System.out.println("1");
      if (bomParameters){
    	  System.out.println("2");
        client.setParametersAsBOM(inputParameters);
        System.out.println("3");
      }
      else {
    	  System.out.println("4");
        client.setParameters(inputParameters);
        System.out.println("5");
      }
      System.out.println("6");
      if ((traceHelper != null) && (traceHelper.isBamEnabled()) && (traceHelper.isInfoInputParameters())) {
    	  System.out.println("7");
        Map serializedParams = new HashMap();
        System.out.println("8");
        String bomSupportEnabledProp = (String)rulesetProperties.get("ruleset.bom.enabled");
        System.out.println("9");
        if ((bomSupportEnabledProp == null) || (bomSupportEnabledProp.equals("false")))
        {
        	System.out.println("10");
          for (String k : inputParameters.keySet()) {
        	  System.out.println("11");
            Object v = inputParameters.get(k);
            System.out.println("12");
            if (v != null) {
            	System.out.println("13");
              String s = v.toString();
              serializedParams.put(k, s);
            }
          }
          System.out.println("14");
        }
        else
        {
        	System.out.println("15");
        	Map<String,Object> inParamsAsBOM;
          try
          {
        	  System.out.println("16");
            inParamsAsBOM = client.getParametersAsBOM((byte) 1, traceHelper.getInOutParamsSerializationFilters(), true);
            System.out.println("17");
            for (String k : inParamsAsBOM.keySet()) {
              Object v = inParamsAsBOM.get(k);
              serializedParams.put(k, (String)v);
            }
            System.out.println("18");
          }
          catch (ResourceException re)
          {
        	  System.out.println("19");
            for (String k : inputParameters.keySet()) {
              Object v = inputParameters.get(k);
              if (v != null) {
                String s = v.toString();
                serializedParams.put(k, s);
              }
            }
            System.out.println("20");
            try {
            	 System.out.println("21");
              String error = new IlrTraceHelper(client, null).printStackTrace(re);
              serializedParams.put("error", error);
            }
            catch (Exception ee) {
            	 System.out.println("22");
            }
          }
        }
        System.out.println("23");
        traceHelper.setSerializedInputParams(serializedParams);
        System.out.println("24");
      }
    }
    System.out.println("25");
    if (taskName == null){
    	 System.out.println("26");
      client.execute();
      System.out.println("27");
    }
    else {
    	 System.out.println("28");
      client.executeTask(taskName);
      System.out.println("29");
    }
    System.out.println("30");
    Map resultMap = bomParameters ? client.getParametersAsBOM((byte) 2, null, false) : client.getParameters((byte) 2);
    System.out.println("31");
    if (askForNumRulesFired) {
    	System.out.println("32");
      resultMap.put("ilog.rules.firedRulesCount", Integer.valueOf(client.getFiredRulesCount()));
    }
    System.out.println("33");
    System.out.println("end Map<String, Object> execute ");
    return resultMap;
  }

  protected int traceFilterToFilterMask(IlrTraceFilter filter)
  {
    int filterMask = 0;
    if (filter.isInfoInetAddress().booleanValue()) {
      filterMask |= 16384;
    }

    if (filter.isInfoRules().booleanValue()) {
      filterMask |= 4;
    }
    if (filter.isInfoTasks().booleanValue()) {
      filterMask |= 8;
    }

    if (filter.isInfoExecutionDuration().booleanValue()) {
      filterMask |= 256;
    }
    if (filter.isInfoTotalRulesNotFired().booleanValue()) {
      filterMask |= 16;
    }
    if (filter.isInfoTotalTasksExecuted().booleanValue()) {
      filterMask |= 1;
    }
    if (filter.isInfoTotalTasksNotExecuted().booleanValue()) {
      filterMask |= 32;
    }
    if (filter.isInfoExecutionEvents().booleanValue()) {
      filterMask |= 2;
      filterMask |= 2048;
      filterMask |= 4096;
    }

    if (filter.isInfoRulesNotFired().booleanValue())
    {
      filterMask |= 64;
    }
    if (filter.isInfoTasksNotExecuted().booleanValue()) {
      filterMask |= 128;
    }

    if ((!filter.isInfoExecutionEvents().booleanValue()) && (filter.isInfoTasksNotExecuted().booleanValue())) {
      filterMask |= 4096;
    }
    if ((!filter.isInfoExecutionEvents().booleanValue()) && (filter.isInfoRulesNotFired().booleanValue())) {
      filterMask |= 2048;
    }
    if (filter.isInfoWorkingMemory().booleanValue()) {
      filterMask |= 512;
    }
    if (filter.isInfoTotalRulesFired().booleanValue()) {
      filterMask |= 1024;
    }
    if (filter.isInfoSystemProperties().booleanValue()) {
      filterMask |= 8192;
    }

    return filterMask;
  }

  protected Properties traceFilterToXUFilterProperties(IlrTraceFilter filter) {
    Properties filterProperties = new Properties();

    if (filter.isInfoWorkingMemory().booleanValue()) {
      String wmFilter = filter.getWorkingMemoryFilter();
      if (wmFilter != null) {
        filterProperties.setProperty("WORKING_MEMORY_CLASS_NAMES", wmFilter);
      }
    }
    if (filter.isInfoBoundObjectByRule().booleanValue())
      filterProperties.setProperty("BOUND_OBJECTS", "true");
    else {
      filterProperties.setProperty("BOUND_OBJECTS", "false");
    }

    return filterProperties;
  }
}