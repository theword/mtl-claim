package com.mtl.service.impl;

import ilog.rules.res.session.IlrTraceFilter;
import ilog.rules.res.session.ruleset.impl.IlrExecutionTraceImpl;
import ilog.rules.res.session.ruleset.impl.IlrRuleEventImpl;
import ilog.rules.res.session.ruleset.impl.IlrRuleInformationImpl;
import ilog.rules.res.session.ruleset.impl.IlrTaskEventImpl;
import ilog.rules.res.session.ruleset.impl.IlrTaskInformationImpl;
import ilog.rules.res.xu.cci.ruleset.IlrRulesetExecutionTrace;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IlrExecutionTraceHelperTest
{
  private IlrExecutionTraceImpl rsTrace = null;
  private IlrRulesetExecutionTrace xuTrace = null;

  public IlrExecutionTraceImpl createSessionTrace(IlrRulesetExecutionTrace xuTrace, IlrTraceFilter filter) {
	  System.out.println("begin IlrExecutionTraceImpl createSessionTrace");
    this.rsTrace = new IlrExecutionTraceImpl();
    System.out.println("1");
    this.xuTrace = xuTrace;
    System.out.println("2");
    if (xuTrace.getExecutionDate() != null) this.rsTrace.setExecutionDate(xuTrace.getExecutionDate());
    System.out.println("3");
    if (xuTrace.getExecutionDuration() != null) this.rsTrace.setExecutionDuration(xuTrace.getExecutionDuration().longValue());
    System.out.println("4");
    if (xuTrace.getInetAddress() != null) this.rsTrace.setInetAddress(xuTrace.getInetAddress());
    System.out.println("5");
    if ((filter.isInfoSystemProperties().booleanValue()) && (xuTrace.getSystemProperties() != null)) this.rsTrace.setSystemProperties(xuTrace.getSystemProperties());
    System.out.println("6");
    if (xuTrace.getRulesNotFired() != null) initRulesNotFired();
    System.out.println("7");
    if (xuTrace.getTasksNotExecuted() != null) initTasksNotExecuted();
    System.out.println("8");
    if (xuTrace.getTotalRulesFired() != null) this.rsTrace.setTotalRulesFired(xuTrace.getTotalRulesFired().longValue());
    System.out.println("9");
    if (xuTrace.getTotalRulesNotFired() != null) this.rsTrace.setTotalRulesNotFired(xuTrace.getTotalRulesNotFired().longValue());
    System.out.println("10");
    if (xuTrace.getTotalTasksExecuted() != null) this.rsTrace.setTotalTasksExecuted(xuTrace.getTotalTasksExecuted().longValue());
    System.out.println("11");
    if (xuTrace.getTotalTasksNotExecuted() != null) this.rsTrace.setTotalTasksNotExecuted(xuTrace.getTotalTasksNotExecuted().longValue());
    System.out.println("12");
    if (filter.isInfoWorkingMemory().booleanValue()) this.rsTrace.setWorkingMemory(xuTrace.getWorkingMemory());
    System.out.println("13");
    if (xuTrace.getRules() != null) setRules(this.rsTrace, xuTrace.getRules());
    System.out.println("14");
    if (xuTrace.getTasks() != null) setTasks(this.rsTrace, xuTrace.getTasks());
    System.out.println("15");
    if (xuTrace.getExecutionEvents() != null) setExecutionEvents(this.rsTrace, xuTrace.getExecutionEvents());
    
    System.out.println("end IlrExecutionTraceImpl createSessionTrace");
    return this.rsTrace;
  }

  private void initRulesNotFired()
  {
    Set information = new HashSet();
    Map<String,ilog.rules.res.xu.cci.ruleset.IlrRuleInformation> ruleInformations = this.xuTrace.getRuleInformations();
    Set rulesNotFired = this.xuTrace.getRulesNotFired();
    for (Object ruleName : rulesNotFired) {
      information.add(createSessionRuleInfo((ilog.rules.res.xu.cci.ruleset.IlrRuleInformation)ruleInformations.get(ruleName.toString())));
    }
    this.rsTrace.setRulesNotFired(information);
  }

  private void initTasksNotExecuted() {
    Set information = new HashSet();
    Map taskInformations = this.xuTrace.getTaskInformations();
    Set tasksNotExecuted = this.xuTrace.getTasksNotExecuted();
    for (Object taskName : tasksNotExecuted) {
      information.add(createSessionTaskInfo((ilog.rules.res.xu.cci.ruleset.IlrTaskInformation)taskInformations.get(taskName.toString())));
    }
    this.rsTrace.setTasksNotExecuted(information);
  }

  private ilog.rules.res.session.ruleset.IlrRuleEvent createSessionRuleEvent(ilog.rules.res.xu.cci.ruleset.IlrRuleEvent xuEvent) {
    IlrRuleEventImpl event = new IlrRuleEventImpl(xuEvent.getName(), xuEvent.getStartDate(), xuEvent.getEndDate(), xuEvent.getPriority(), xuEvent.getObjects());

    if (this.xuTrace.getRuleInformations() != null)
      event.add(5, createSessionRuleInfo((ilog.rules.res.xu.cci.ruleset.IlrRuleInformation)this.xuTrace.getRuleInformations().get(xuEvent.getName())));
    return event;
  }

  private ilog.rules.res.session.ruleset.IlrTaskEvent createSessionTaskEvent(ilog.rules.res.xu.cci.ruleset.IlrTaskEvent xuEvent)
  {
    IlrTaskEventImpl rsTask = new IlrTaskEventImpl(xuEvent.getName(), xuEvent.getStartDate(), xuEvent.getEndDate());
    rsTask.add(3, createEventList(xuEvent.getSubExecutionEvents()));
    if (this.xuTrace.getTaskInformations() != null)
      rsTask.add(4, createSessionTaskInfo((ilog.rules.res.xu.cci.ruleset.IlrTaskInformation)this.xuTrace.getTaskInformations().get(xuEvent.getName())));
    return rsTask;
  }

  private ilog.rules.res.session.ruleset.IlrTaskInformation createSessionTaskInfo(ilog.rules.res.xu.cci.ruleset.IlrTaskInformation xuInfo) {
    return new IlrTaskInformationImpl(xuInfo.getName(), xuInfo.getBusinessName(), xuInfo.getProperties());
  }

  private ilog.rules.res.session.ruleset.IlrRuleInformation createSessionRuleInfo(ilog.rules.res.xu.cci.ruleset.IlrRuleInformation xuInfo) {
    return new IlrRuleInformationImpl(xuInfo.getName(), xuInfo.getBusinessName(), xuInfo.getProperties());
  }

  private void setRules(IlrExecutionTraceImpl rsTrace, Map<String, ilog.rules.res.xu.cci.ruleset.IlrRuleInformation> xuAllRules) {
    HashMap rsAllRules = new HashMap();

    for (Iterator i = xuAllRules.keySet().iterator(); i.hasNext(); ) {
      String key = (String)i.next();
      rsAllRules.put(key, createSessionRuleInfo((ilog.rules.res.xu.cci.ruleset.IlrRuleInformation)xuAllRules.get(key)));
    }
    rsTrace.setRules(rsAllRules);
  }

  private void setTasks(IlrExecutionTraceImpl rsTrace, Map<String, ilog.rules.res.xu.cci.ruleset.IlrTaskInformation> xuAllTasks) {
    HashMap rsAllTasks = new HashMap();

    for (Iterator i = xuAllTasks.keySet().iterator(); i.hasNext(); ) {
      String key = (String)i.next();
      rsAllTasks.put(key, createSessionTaskInfo((ilog.rules.res.xu.cci.ruleset.IlrTaskInformation)xuAllTasks.get(key)));
    }
    rsTrace.setTasks(rsAllTasks);
  }

  private void setExecutionEvents(IlrExecutionTraceImpl rsTrace, List<ilog.rules.res.xu.cci.ruleset.IlrExecutionEvent> xuEvents)
  {
    rsTrace.setExecutionEvents(createEventList(xuEvents));
  }

  private List<ilog.rules.res.session.ruleset.IlrExecutionEvent> createEventList(List<ilog.rules.res.xu.cci.ruleset.IlrExecutionEvent> xuEvents) {
    List executionEvents = new ArrayList();
    if (xuEvents != null)
    {
      for (ilog.rules.res.xu.cci.ruleset.IlrExecutionEvent event : xuEvents) {
        if ((event instanceof ilog.rules.res.xu.cci.ruleset.IlrRuleEvent)) {
          executionEvents.add(createSessionRuleEvent((ilog.rules.res.xu.cci.ruleset.IlrRuleEvent)event));
        }
        if ((event instanceof ilog.rules.res.xu.cci.ruleset.IlrTaskEvent)) {
          executionEvents.add(createSessionTaskEvent((ilog.rules.res.xu.cci.ruleset.IlrTaskEvent)event));
        }
      }

    }

    return executionEvents;
  }
}
