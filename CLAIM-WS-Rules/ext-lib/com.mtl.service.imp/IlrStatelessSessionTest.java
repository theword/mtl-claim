package com.mtl.service.impl;

import ilog.rules.res.session.IlrSession;
import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.async.IlrAsyncExecutionObserver;

public abstract interface IlrStatelessSessionTest extends IlrSession
{
  public abstract IlrSessionResponse execute(IlrSessionRequest paramIlrSessionRequest)
    throws IlrSessionException;

  public abstract void executeAsynchronous(IlrSessionRequest paramIlrSessionRequest, IlrAsyncExecutionObserver paramIlrAsyncExecutionObserver, long paramLong)
    throws IlrSessionException;
}