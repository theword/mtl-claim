package com.mtl.service.impl;

import ilog.rules.res.model.IlrPath;
import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.async.IlrAsyncExecutionObserver;
import ilog.rules.res.session.impl.IlrSessionWarningImpl;
import ilog.rules.res.session.util.IlrClassLoaderUtil;
import ilog.rules.res.xu.cci.IlrCCIClientFactory;
import ilog.rules.res.xu.cci.IlrCCIRuleEngineClient;

import java.util.Iterator;
import java.util.List;

import javax.resource.ResourceException;
import javax.resource.cci.ResourceWarning;

public class IlrStatelessSessionBaseTest extends IlrSessionImplTest
  implements IlrStatelessSessionTest
{
  protected IlrSessionFactoryBaseTest sessionFactory;
  protected IlrCCIClientFactory clientFactory;

  public IlrStatelessSessionBaseTest(IlrSessionFactoryBaseTest sessionFactory, IlrCCIClientFactory clientFactory)
  {
    this.sessionFactory = sessionFactory;
    this.clientFactory = clientFactory;
  }

  protected ClassLoader getClassLoader()
  {
    return IlrClassLoaderUtil.getThreadClassLoader();
  }

  public IlrSessionResponse execute(IlrSessionRequest request)
    throws IlrSessionException
  {
	  System.out.println("begin IlrSessionResponse");
    IlrSessionResponse response = null;
    IlrCCIRuleEngineClient client = null;
    IlrWarningListenerImplTest warningListener = new IlrWarningListenerImplTest();
    try
    {
      boolean interceptorEnabled = request.isInterceptorEnabled();
      IlrInterceptorHelperTest ih = null;
      if (interceptorEnabled) {
        ih = new IlrInterceptorHelperTest();
        ih.initialize(request, this, this.sessionFactory);

        IlrPath rsPath = ih.invokeTranformPathInterceptors(request.getRulesetPath(), request.getUserData(), request.getInputParameters());

        request.setRulesetPath(rsPath);

        ih.invokeBeforeInterceptors();

        request = ih.getContext().getRequest();
      }

      System.out.println("begin Rule");
      client = this.clientFactory.createRuleEngineClient(request.getRulesetPath(), getClassLoader(), null, request.getUserData(), "7.5.0.0");
      System.out.println("during 1 Rule");
      client.addResourceWarningListener(warningListener);
      System.out.println("during 2 Rule");
      if (request.isForceUptodate()) {
    	  System.out.println("during 2 Rule if");
        client.loadUptodateRuleset();
        System.out.println("during 2 Rule end if");
      }
      System.out.println("during 3 Rule");
      IlrTraceHelperTest traceHelper = new IlrTraceHelperTest(client, this.sessionFactory);
      System.out.println("traceHelper");
      if (traceHelper.isBamEnabled())
      {
    	  System.out.println("traceHelper if");
        traceHelper.prepareTrace(request);
        System.out.println("traceHelper end if");
      }
      System.out.println("before response ");
      response = execute(client, request, traceHelper);
      System.out.println("after response ");
      if (interceptorEnabled)
      {
    	  System.out.println("if interceptorEnabled");
        response = ih.invokeAfterInterceptors(response);
        System.out.println("end if interceptorEnabled");
      }
      System.out.println("end interceptorEnabled");
      if (traceHelper.isBamEnabled()) {
    	  System.out.println("traceHelper.isBamEnabled");
        traceHelper.persistTrace(request, response);
        System.out.println("persistTrace");
        traceHelper.filterTrace(request, response);
        System.out.println("filterTrace");
      }
      System.out.println("out traceHelper.isBamEnabled()");
    }
    catch (Exception e)
    {
    	System.out.println("catch");
      List sws;
      Iterator i$;
      ResourceWarning rw;
      System.out.println("end catch");
      throw new IlrSessionException(e);
     
    }
    finally {
    	System.out.println("finally");
      try {
    	  List sws;
        if (client != null) {
          client.close();
        }
        if (response != null) {
          sws = response.getWarnings();
          for (ResourceWarning rw : warningListener.getWarnings())
            sws.add(IlrSessionWarningImpl.createFromResourceWarning(rw));
        }
        System.out.println("finally 2");
      }
      catch (ResourceException e)
      {
    	  System.out.println("catch finally");
        List sws;
        throw new IlrSessionException(e);
      }
    }
    System.out.println("end IlrSessionResponse");
    return response;
  }

  public void executeAsynchronous(IlrSessionRequest request, IlrAsyncExecutionObserver observer, long timeout) throws IlrSessionException
  {
    IlrCCIRuleEngineClient client = null;
    try
    {
      client = this.clientFactory.createRuleEngineClient(request.getRulesetPath(), IlrClassLoaderUtil.getThreadClassLoader(), null, request.getUserData(), "7.5.0.0");

      if (request.isForceUptodate()) {
        client.loadUptodateRuleset();
      }

      if (request.getInputParameters() != null) {
        client.setParameters(request.getInputParameters());
      }
      //client.asynchronousExecute(new IlrInternalAsyncRulesetExecutionListener(observer, client));
    } catch (ResourceException e) {
      throw new IlrSessionException(e);
    } finally {
      if (client == null);
    }
  }
}
