/*
 * 
 */
package com.mtl.service.impl;

import ilog.rules.res.model.IlrFormatException;
import ilog.rules.res.model.IlrPath;
import ilog.rules.res.session.IlrPOJOSessionFactory;
import ilog.rules.res.session.IlrSessionCreationException;
import ilog.rules.res.session.IlrSessionException;
import ilog.rules.res.session.IlrSessionFactory;
import ilog.rules.res.session.IlrSessionRequest;
import ilog.rules.res.session.IlrSessionResponse;
import ilog.rules.res.session.IlrStatelessSession;
import ilog.rules.res.session.ruleset.IlrExecutionEvent;
import ilog.rules.res.session.ruleset.IlrExecutionTrace;
import ilog.rules.res.session.ruleset.IlrRuleEvent;
import ilog.rules.res.session.ruleset.IlrRuleInformation;
import ilog.rules.res.session.ruleset.IlrTaskEvent;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.mtl.service.RuleEngineService;


/**
 * The Class RuleEngineServiceImpl.
 */
public class RuleEngineServiceImpl implements RuleEngineService {
	
	/** The session factory. */
	private IlrSessionFactory sessionFactory = null;
	
	/** The response. */
	private IlrSessionResponse response = null;
	
	/** The gson service. */
	private Gson gsonService;
	
	/** The log. */
	Logger log = Logger.getLogger(this.getClass());
	
	/* (non-Javadoc)
	 * @see com.mtl.service.RuleEngineService#getOutputParameter(java.lang.String)
	 */
	public Object getOutputParameter(String key) {
	    return this.response.getOutputParameters().get(key);
	}

	/* (non-Javadoc)
	 * @see com.mtl.service.RuleEngineService#executeRule(java.lang.String, java.util.Map, java.lang.String)
	 */
	public boolean executeRule(String rulesetPath, Map<String,Object> inParam, String logRule) {
		boolean r = false;
		Logger log = Logger.getLogger(logRule);
	    try {
		    // get a rulesession
		    if (this.sessionFactory == null) {
				 this.sessionFactory = new IlrPOJOSessionFactory();
			}

		    // Create a session request object
		    IlrSessionRequest sessionRequest = sessionFactory.createRequest();
		    log.debug("Execute rulesetPath=" + rulesetPath);
		    sessionRequest.setForceUptodate(true);
		    sessionRequest.setRulesetPath(IlrPath.parsePath(rulesetPath));
		    // Enable trace to retrieve info on executed rules
		    sessionRequest.setTraceEnabled(false);
		    sessionRequest.getTraceFilter().setInfoAllFilters(true);
		    // Set the input parameters for the execution of the rules
		    Map<String,Object> inputParameters = sessionRequest.getInputParameters();
		    for(Map.Entry<String, Object> entry : inParam.entrySet()) {
		    	inputParameters.put(entry.getKey(), entry.getValue());
		    }
		    
			IlrStatelessSession session = sessionFactory.createStatelessSession();
		    // execute and get the response for this request
			log.debug("Before execute");
		    this.response = session.execute(sessionRequest);
		    log.debug("Rule executed");
		    
		    r = true;
		} catch (IlrFormatException e) {
			log.error("IlrFormatException:"+e.getMessage(),e);
		} catch (IlrSessionCreationException e) {
			log.error("IlrSessionCreationException:"+e.getMessage(),e);
		} catch (IlrSessionException e) {
			log.error("IlrSessionException:"+e.getMessage(),e);
		}
	    
	    return r;
	}

	/* (non-Javadoc)
	 * @see com.mtl.service.RuleEngineService#getTotalRulesFired()
	 */
	public Long getTotalRulesFired() {
	    return this.response.getRulesetExecutionTrace().getTotalRulesFired() ;
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.service.RuleEngineService#getRulesFired()
	 */
	public List<String> getRulesFired() {
	    IlrExecutionTrace sessionTrace = this.response.getRulesetExecutionTrace();
	    List<String> rulesFired = new ArrayList<String>();
		if (sessionTrace!=null) {
			List<String> rules = getRulesExecuted (sessionTrace);
			rulesFired.addAll(rules);
		}
		return rulesFired;
	}

	/**
	 * Gets the rules executed.
	 *
	 * @param trace the trace
	 * @return the list of rules executed
	 */
	public List<String> getRulesExecuted (IlrExecutionTrace trace) {
		List<String> firedRuleBusinessNames = new ArrayList<String>  ();
		Map<String,IlrRuleInformation> allRules = trace.getRules();
		List<IlrExecutionEvent> executionEvents = trace.getExecutionEvents();
		if (executionEvents != null && allRules != null) {
		        String taskName = null;
			addRuleFiredBusinessNames(taskName, trace, executionEvents, firedRuleBusinessNames);
      }
		return firedRuleBusinessNames;
	}

	/**
	 * Adds the rule fired business names.
	 *
	 * @param taskName the task name
	 * @param trace the trace
	 * @param executionEvents the execution events
	 * @param firedRuleBusinessNames the fired rule business names
	 */
	protected void addRuleFiredBusinessNames(String taskName, IlrExecutionTrace trace,
			List<IlrExecutionEvent> executionEvents,
			List<String> firedRuleBusinessNames) {
		Map<String, IlrRuleInformation> allRules = trace.getRules();
		if (executionEvents != null && allRules != null) {
			for (IlrExecutionEvent event : executionEvents) {
				if (event instanceof IlrRuleEvent) {
					String ruleName = allRules.get(event.getName()).getBusinessName();
					firedRuleBusinessNames.add(formatTrace(ruleName, taskName));
				} else {
					List<IlrExecutionEvent> subEvents = ((IlrTaskEvent) event).getSubExecutionEvents();
					IlrTaskEvent taskevent = (IlrTaskEvent)event;
					addRuleFiredBusinessNames(taskevent.getName(), trace, subEvents,	firedRuleBusinessNames);
				}
			}
		}
	}
	
    /**
     * Format trace.
     *
     * @param ruleName the rule name
     * @param taskName the task name
     * @return the string
     */
    protected String formatTrace(String ruleName, String taskName) {
	String format 		= "Rule: \"{0}\" fired in rule task: \"{1}\"";
	Object[] arguments 	= {ruleName, taskName};
	return MessageFormat.format(format,arguments);
    }
    
    /* (non-Javadoc)
     * @see com.mtl.service.RuleEngineService#toJson(java.lang.Object)
     */
    public String toJson(Object src){
    	return gsonService.toJson(src);
    }
    
    /* (non-Javadoc)
     * @see com.mtl.service.RuleEngineService#fromJson(java.lang.String, java.lang.Class)
     */
    public <T> T fromJson(String json,Class<T> classOfT){
    	return gsonService.fromJson(json, classOfT);
    }
    
    //setter
    /**
     * Sets the gson service.
     *
     * @param gsonService the new gson service
     */
    public void setGsonService(Gson gsonService) {
		this.gsonService = gsonService;
	}
    
}
