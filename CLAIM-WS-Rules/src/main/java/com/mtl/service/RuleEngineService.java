/*
 * 
 */

package com.mtl.service;

import ilog.rules.res.session.ruleset.IlrExecutionTrace;

import java.util.List;
import java.util.Map;

/**
 * Rule web service.
 *
 * @author TeTe
 */
public interface RuleEngineService {	
	
	/**
	 * Gets the output parameter.
	 *
	 * @param key the key
	 * @return the output parameter
	 */
	public Object getOutputParameter(String key);
	
	/**
	 * Execute rule.
	 *
	 * @param rulesetPath 	path of rule that execute
	 * @param inParam 	input parameter
	 * @param logRule 	log each rule
	 * @return 			return true if rule execute successful
	 */
	public boolean executeRule(String rulesetPath, Map<String,Object> inParam, String logRule);
	
	/**
	 * Gets the total rules fired.
	 *
	 * @return the total rules fired
	 */
	public Long getTotalRulesFired();
	
	/**
	 * Gets the rules fired.
	 *
	 * @return the rules fired
	 */
	public List<String> getRulesFired();
	
	/**
	 * Gets the rules executed.
	 *
	 * @param trace the trace
	 * @return the rules executed
	 */
	public List<String> getRulesExecuted (IlrExecutionTrace trace);
	
	/**
	 * To json.
	 *
	 * @param src the src
	 * @return the string
	 */
	public String toJson(Object src);
	
	/**
	 * From json.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param classOfT the class of t
	 * @return the t
	 */
	public <T> T fromJson(String json,Class<T> classOfT);
	
}
