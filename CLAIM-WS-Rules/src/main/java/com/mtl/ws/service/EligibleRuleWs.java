/*
 * 
 */
package com.mtl.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.mtl.ws.bean.RuleEligibleDto;
import com.mtl.ws.bean.RuleEligibleResponse;

/**
 * The Interface EligibleRuleService.
 */
@WebService(
		name = "EligibleRuleService"
		, targetNamespace = "http://www.claimrulews.eligible.ws.application.muangthai.co.th/"
	)
	@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface EligibleRuleWs {
	
	/**
	 * Execute claim eligible.
	 *
	 * @param request the request
	 * @return the rule dto
	 */
	public @WebResult(name="response") RuleEligibleResponse executeClaimEligible(@WebParam(name="request") RuleEligibleDto request);
}
