package com.mtl.ws.service;

import javax.xml.namespace.QName;

public interface WSClientService {
	
	public QName  getCashValueQName();
	public String getCashValueEndPoint();
	public QName  getFastPremiumPaidQName();
	public String getFastPremiumPaidEndPoint();
	public String getLinkClaimEndPoint();
	public QName  getLinkClaimQName();
	public String getReturnCashEndPoint();
	public QName  getReturnCashQName();
	public String getUSER_ID();
	public String getUSER_PASSWORD();
	public String getUsername();
	public String getPassword();
}
