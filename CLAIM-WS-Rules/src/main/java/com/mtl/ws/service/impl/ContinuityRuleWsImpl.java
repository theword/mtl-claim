package com.mtl.ws.service.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import th.co.muangthai.application.ws.linkclaim.www.HeaderLinkClaimRequest;
import th.co.muangthai.application.ws.linkclaim.www.HeaderLinkClaimResponse;
import th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType;
import th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_ServiceLocator;

import com.mtl.LogMonitor;
import com.mtl.constants.RuleWsConstants;
import com.mtl.constants.WSConstants;
import com.mtl.continuity.dto.ContinuityClaimCheckingDto;
import com.mtl.continuity.model.mtlclaim.Claim;
import com.mtl.continuity.model.mtlclaim.Diagnosis;
import com.mtl.continuity.model.xom.mtlclaim.ContinuityClaimCheckingModel;
import com.mtl.continuity.util.CalendarHelper;
import com.mtl.rules.service.DataCollectorClientService;
import com.mtl.service.RuleEngineService;
import com.mtl.utils.ApplicationContextUtils;
import com.mtl.utils.ResourceConfigManager;
import com.mtl.ws.bean.RuleContinuityDto;
import com.mtl.ws.bean.RuleContinuityResponse;
import com.mtl.ws.service.ContinuityRuleWs;
import com.mtl.ws.service.WSClientService;

@WebService(
	endpointInterface = "com.mtl.ws.service.ContinuityRuleWs"
	, targetNamespace="http://www.claimrulews.continuity.ws.application.muangthai.co.th/"
	, serviceName="ContinuityRuleService"
)
public class ContinuityRuleWsImpl implements ContinuityRuleWs{
	
	private String continuityClaim 		= RuleWsConstants.CONTINUITY_CLAIM;
	private Logger continuityClaimLog 	= Logger.getLogger(continuityClaim);
	private RuleEngineService ruleEngineService = (RuleEngineService)ApplicationContextUtils.getBean("ruleEngineService");
	private WSClientService wsClientService 	= (WSClientService)ApplicationContextUtils.getBean("wsClientService");
	private DataCollectorClientService dataCollectorClientService 	= (DataCollectorClientService)ApplicationContextUtils.getBean("dataCollectorClientService");
	private final String RULESETPATH_CONTINUITYCLAIM_CLAIM			= ResourceConfigManager.getProperty("claim.continuityclaim.ruleset.path");
	private String isUsedDataColl	= "1";
	private String isSetAddress		= "0";
	
	private Logger requestContinuityClaimLog 	= Logger.getLogger("requestContinuityClaim");
	private Logger responseContinuityClaimLog 	= Logger.getLogger("responseContinuityClaim");
	private Logger loadTestContinuityClaimLog 	= Logger.getLogger("loadTestContinuityClaimLog");
	private long startTime;
	private long endDataCollTime;
	private long endRuleTime;
	private String statusRule;
	
	private LogMonitor logMonitor;
	
	public @WebResult(name="response") RuleContinuityResponse executeClaimContinuityClaim(@WebParam(name="request") RuleContinuityDto request){
		continuityClaimLog.debug("::::::::::: start [ClaimRuleWsImpl.executeClaimContinuityClaim] ---------");
		startTime		 	= System.currentTimeMillis();
		String reason		= "";
		String json			= "";
		String jsonResponse	= "";
		logMonitor			= new LogMonitor();
		RuleContinuityResponse res 	= new RuleContinuityResponse();
		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(CalendarHelper.getCurrentDateTime().getTime());
		try{
			logMonitor.setSystemName(continuityClaim);
			logMonitor.setRequestTime(new Date());
			logMonitor.setClientNo(request.getContinuityClaimCheckingDto().getClientID());
			
			json = ruleEngineService.toJson(request.getContinuityClaimCheckingDto());
			continuityClaimLog.debug("::::: request.getJsonXom():"+json);
			requestContinuityClaimLog.debug("==== Request jsonXOM : "+json);
			res.setDetail(request.getIpAddress());
			ContinuityClaimCheckingModel model = doRuleClaimContinuityClaim(request);	
			
			if(model != null){
				if(model.getStatus() < 0){
					if(res.getErrors() == null)res.setErrors(new ArrayList<String>());
					res.getErrors().add("S01");
					res.setStatus(WSConstants.FAIL);
					statusRule = "fail";
				}else{
					res.setStatus(WSConstants.SUCCESS);
					res.setReason(reason);
					res.setContinuityClaimCheckingModel(model);
					statusRule = "success";
				}
				jsonResponse = ruleEngineService.toJson(model);
				continuityClaimLog.debug("response jsonXom:"+jsonResponse);
				responseContinuityClaimLog.debug("==== Response jsonXOM : "+jsonResponse);
			}else{
				res.setStatus(WSConstants.FAIL);
				statusRule = "fail";
			}			
		}catch (Exception e) {
			continuityClaimLog.error("ERROR:"+e,e);
			res.setStatus(WSConstants.FAIL);
			res.setReason("ERROR:"+e.getMessage());			
			e.printStackTrace();
			statusRule = "fail";
		}
		
		continuityClaimLog.debug("begin  test write log monitor");
		logMonitor.write();
		continuityClaimLog.debug("finish test write log monitor");
		
		res.setRespondDate(CalendarHelper.getCurrentDateTime().getTime());
		continuityClaimLog.debug("::::::::::: end Service [ClaimRuleWsImpl.executeClaimLinkClaim]");
		loadTestContinuityClaimLog.debug(";RuleLinkClaim;" + statusRule + ";" + (System.currentTimeMillis() - startTime) + ";" + endDataCollTime + ";" + endRuleTime);
		return res;
	}
	
	private ContinuityClaimCheckingModel doRuleClaimContinuityClaim(RuleContinuityDto request){
		validateDataColl(continuityClaim);
		ContinuityClaimCheckingModel continuityClaimCheckingModel 	= null;
		ContinuityClaimCheckingDto continuityClaimCheckingDto 		= request.getContinuityClaimCheckingDto(); 
		
		logMonitor.requestLog("collectContinuityClaimCheckingModel");
		continuityClaimCheckingModel = dataCollectorClientService.collectContinuityClaimCheckingModel(continuityClaimCheckingDto,request.getCallName());
		logMonitor.responseLog("collectContinuityClaimCheckingModel", null, null);
		endDataCollTime = System.currentTimeMillis() - startTime;
		if(continuityClaimCheckingModel!=null){		
			if(continuityClaimCheckingModel.getStatus()<0)return continuityClaimCheckingModel;
			Map<String, Object> in = new HashMap<String, Object>();
			in.put("continuityClaimCheckingModel", continuityClaimCheckingModel);
			
			long startRule 			= System.currentTimeMillis();
			logMonitor.requestLog("ruleContinuity");
			boolean executeResult 	= ruleEngineService.executeRule(RULESETPATH_CONTINUITYCLAIM_CLAIM, in, continuityClaim);
			logMonitor.responseLog("ruleContinuity", null, null);
			endRuleTime 			= System.currentTimeMillis() - startRule;
			
			continuityClaimLog.debug("execute success = " + executeResult);
			continuityClaimCheckingModel = (ContinuityClaimCheckingModel) ruleEngineService.getOutputParameter("continuityClaimCheckingModel");	
			
			if(!executeResult){
				continuityClaimCheckingModel.setStatus(-1);
				return continuityClaimCheckingModel;
			}else{
				if(continuityClaimCheckingModel.getPossibleContinuityClaims() != null && 
				   continuityClaimCheckingModel.getPossibleContinuityClaims().size()>0){
					
					if(continuityClaimCheckingModel.getAdmissionDate()!=null){
						List<String> referanceClaimList = new ArrayList<String>();
						for(Claim claim:continuityClaimCheckingModel.getPossibleContinuityClaims())
							referanceClaimList.add(claim.getReferenceClaimGroupID());
						List<String> diagnosisList = new ArrayList<String>();
						boolean check = true;
						for(Diagnosis diagnosis:continuityClaimCheckingDto.getRequestDiagnoses()){
							for(Claim claim:continuityClaimCheckingModel.getPossibleContinuityClaims()){
								if(claim.getRequestDiagnoses()!=null && claim.getRequestDiagnoses().size()>0){
									for(Diagnosis diagnosisCode:claim.getRequestDiagnoses()){
										if(diagnosisCode.getDiagnosisCode().equals(diagnosis.getDiagnosisCode())){
											diagnosisList.add(diagnosis.getDiagnosisCode());
											check = false;
											break;
										}
									}
								}
								if(!check)break;
							}
							if(!check)break;
						}
						List<String> links = getLinkClaimService(request,continuityClaimCheckingDto.getClientID(),diagnosisList,referanceClaimList);
						if(links==null){
							continuityClaimCheckingModel.setStatus(-1);
							return continuityClaimCheckingModel;
						}
						continuityClaimCheckingModel.setPossibleContinuityClaimList(links);
					}if(continuityClaimCheckingModel.getIncidentDate()!=null){
						if(continuityClaimCheckingModel.getPossibleContinuityClaimList() == null)
							continuityClaimCheckingModel.setPossibleContinuityClaimList(new ArrayList<String>());
						for(Claim claim:continuityClaimCheckingModel.getPossibleContinuityClaims())
							continuityClaimCheckingModel.getPossibleContinuityClaimList().add(claim.getReferenceClaimGroupID());
					}
				}
			}
		}else{
			continuityClaimCheckingModel = new ContinuityClaimCheckingModel(null, null, null, null, null, null);
			continuityClaimCheckingModel.setStatus(-1);
			continuityClaimLog.warn("Rule model is null");
		}		
		return continuityClaimCheckingModel;
	}	

	private void validateDataColl(String logRule){
		Logger log 		= Logger.getLogger(logRule);
		String isUsed 	= ResourceConfigManager.getProperty("rules.datacoll.isused");		
		if(WSConstants.DataColl.IS_USED_FALSE.equals(isUsed))this.isUsedDataColl=WSConstants.DataColl.IS_USED_FALSE;
		if(WSConstants.DataColl.IS_USED_TRUE.equals(this.isUsedDataColl)){			
			if(dataCollectorClientService!=null){
				String setAddress = ResourceConfigManager.getProperty("rules.datacoll.isSetAddress");
				if(WSConstants.IS_TRUE.equals(setAddress))this.isSetAddress=WSConstants.IS_TRUE;
				if(WSConstants.IS_TRUE.equals(this.isSetAddress)){
					String wsAddrerss = ResourceConfigManager.getProperty("rules.datacoll.ws.wsdl.address");
					String qname = ResourceConfigManager.getProperty("rules.datacoll.ws.qname");	
					log.debug("setServiceAddress,[wsAddrerss]="+wsAddrerss+",[qname]="+qname);
					try{
						if(StringUtils.isNotBlank(wsAddrerss)&&StringUtils.isNotBlank(qname))
							dataCollectorClientService.setServiceAddress(wsAddrerss, qname);
					}catch (Exception e) {
						log.error("ERROR while trying to setServiceAddress,[wsAddrerss]=" + wsAddrerss + ",[qname]=" + qname + ":" + e, e);
						this.isUsedDataColl=WSConstants.DataColl.IS_USED_FATAL;
					}
				}
			}else log.warn("ERROR dataCollectorClientService is null.");
		}	
	}
	
	/**/
	private List<String> getLinkClaimService(RuleContinuityDto request, String clientNumber, List<String> diagnosisList, List<String> referanceClaimList){
		continuityClaimLog.debug("call link claim is begining");
		String username = wsClientService.getUsername();
		String password = wsClientService.getPassword();
		String userID 	= wsClientService.getUSER_ID();
		String userPass = wsClientService.getUSER_PASSWORD();
		
		List<String> resultList = new ArrayList<String>();
		
		LinkClaimService_ServiceLocator locator = new LinkClaimService_ServiceLocator();
		QName linkClaimQname 					= wsClientService.getLinkClaimQName();
		String linkClaimEndPoint 				= wsClientService.getLinkClaimEndPoint();
		
		LinkClaimService_PortType linkClaim;
		try {
			locator.setLinkClaimWsImplPortEndpointAddress(linkClaimEndPoint);
			linkClaim = (LinkClaimService_PortType) locator.getPort(linkClaimQname, LinkClaimService_PortType.class);
			if(linkClaim!=null){
				((Stub) linkClaim).setTimeout(WSConstants.TIME_OUT);
				((Stub) linkClaim).setHeader("", username, userID);
				((Stub) linkClaim).setHeader("", password, userPass);
			}
			
			HeaderLinkClaimRequest requestService = new HeaderLinkClaimRequest();
			
			requestService.setServiceID(request.getServiceID());
			requestService.setCallName(request.getCallName());
			requestService.setReason("");
			requestService.setDetail("");
		
			String[] diagnosisArray = new String[diagnosisList.size()];
			String diagnosis		= "";
			for(int i=0;i<diagnosisList.size();i++){
				diagnosisArray[i] 	= diagnosisList.get(i);
				diagnosis			= diagnosis + diagnosisList.get(i) + ",";
			}
			String[] referanceClaimArray 	= new String[referanceClaimList.size()];
			String policyNumber				= "";
			for(int i=0;i<referanceClaimList.size();i++){
				referanceClaimArray[i] 	= referanceClaimList.get(i);
				policyNumber 			= policyNumber + referanceClaimList.get(i) + ",";
			}
			requestService.setClientNumber(clientNumber);
			requestService.setDiagnosisCodeList(diagnosisArray);
			requestService.setReferenceClaimList(referanceClaimArray);
			
			continuityClaimLog.debug("start  call link claim");
			logMonitor.requestLog("LinkClaimService", policyNumber, null, null, null, diagnosis);
			HeaderLinkClaimResponse response = linkClaim.getLinkClaim(requestService);
			logMonitor.responseLog("LinkClaimService", null, null);
			continuityClaimLog.debug("finish call link claim");
			
			if(response != null && response.getLinkClaimList() != null && response.getLinkClaimList().length > 0){
				for(String refNo:response.getLinkClaimList())resultList.add(refNo);
			}else continuityClaimLog.debug("link claim is null");
		} catch (ServiceException e) {
			e.printStackTrace();
			logMonitor.responseLog("LinkClaimService", null, e.getMessage());
			return null;
		} catch (RemoteException e) {
			e.printStackTrace();
			logMonitor.responseLog("LinkClaimService", null, e.getMessage());
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			logMonitor.responseLog("LinkClaimService", null, e.getMessage());
			return null;
		}
		continuityClaimLog.debug("call link claim is finish");
		return resultList;
	}
	/**/
}
