/*
 * 
 */
package com.mtl.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.mtl.ws.bean.RuleContinuityDto;
import com.mtl.ws.bean.RuleContinuityResponse;

/**
 * The Interface ContinuityRuleService.
 */
@WebService(
		name = "ContinuityRuleService"
		, targetNamespace = "http://www.claimrulews.continuity.ws.application.muangthai.co.th/"
	)
	@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ContinuityRuleWs {
	
	/**
	 * Execute claim continuity claim.
	 *
	 * @param request the request
	 * @return the rule dto
	 */
	public @WebResult(name="response") RuleContinuityResponse executeClaimContinuityClaim(@WebParam(name="request") RuleContinuityDto request);
}
