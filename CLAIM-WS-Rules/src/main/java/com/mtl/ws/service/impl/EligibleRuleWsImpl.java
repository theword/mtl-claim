package com.mtl.ws.service.impl;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererResponse;
import th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType;
import th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_ServiceLocator;
import th.co.muangthai.application.ws.cashvaluegatherer.www.HeaderCashValueGathererRequest;
import th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidResponse;
import th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType;
import th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_ServiceLocator;
import th.co.muangthai.application.ws.fastpremiumpaid.www.HeaderFastPremiumPaidRequest;
import th.co.muangthai.application.ws.fastpremiumpaid.www.HeaderFastPremiumPaidResponse;
import th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor;
import th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash;
import th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashRequest;
import th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashResponse;
import th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashServiceLocator;
import th.co.muangthai.application.ws.getcheckreturncash.www.GetRequestCheckReturnCash;
import th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash;

import com.mtl.LogMonitor;
import com.mtl.constants.RuleWsConstants;
import com.mtl.constants.WSConstants;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.enumerator.ClaimType;
import com.mtl.eligible.model.mtlclaim.Coverage;
import com.mtl.eligible.model.mtlclaim.Policy;
import com.mtl.eligible.model.mtlclaim.master.PolicyMs;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.eligible.util.CalendarHelper;
import com.mtl.rules.service.DataCollectorClientService;
import com.mtl.service.RuleEngineService;
import com.mtl.utils.ApplicationContextUtils;
import com.mtl.utils.DateUtils;
import com.mtl.utils.ResourceConfigManager;
import com.mtl.ws.bean.RuleEligibleDto;
import com.mtl.ws.bean.RuleEligibleResponse;
import com.mtl.ws.service.EligibleRuleWs;
import com.mtl.ws.service.WSClientService;

@WebService(
	endpointInterface = "com.mtl.ws.service.EligibleRuleWs"
	, targetNamespace="http://www.claimrulews.eligible.ws.application.muangthai.co.th/"
	, serviceName="EligibleRuleService"
)
public class EligibleRuleWsImpl implements EligibleRuleWs{
	
	private final Locale ENG_LOCALE	 		= new Locale("en", "EN");
	private WSClientService wsClientService = (WSClientService)ApplicationContextUtils.getBean("wsClientService");
	private String eligible 				= RuleWsConstants.ELIGIBLE;
	private Logger eligibleLog 				= Logger.getLogger(eligible);
	private RuleEngineService ruleEngineService 					= (RuleEngineService)ApplicationContextUtils.getBean("ruleEngineService");
	private DataCollectorClientService dataCollectorClientService 	= (DataCollectorClientService)ApplicationContextUtils.getBean("dataCollectorClientService");
	private final String RULESETPATH_ELIGIBLE1_CLAIM				= ResourceConfigManager.getProperty("claim.eligible-1.ruleset.path");
	private final String RULESETPATH_ELIGIBLE2_CLAIM				= ResourceConfigManager.getProperty("claim.eligible-2.ruleset.path");
	private String isUsedDataColl			= "1";
	private String isSetAddress				= "0";
	
	private String errorMessage 			= "";
	private Logger requestEligibleLog 		= Logger.getLogger(RuleWsConstants.REQUESTELIGIBLE);
	private Logger responseEligibleLog 		= Logger.getLogger(RuleWsConstants.RESPONSEELIGIBLE);
	private Logger loadTestEligibleLog 		= Logger.getLogger("loadTestEligibleLog");
	private long startTime;
	private long endDataCollTime1;
	private long endDataCollTime2;
	private long endRuleTime1;
	private long endRuleTime2;
	private String statusRule;

	private LogMonitor logMonitor;
	
	public @WebResult(name="response") RuleEligibleResponse executeClaimEligible(@WebParam(name="request") RuleEligibleDto request){
		eligibleLog.debug("::::::::::: start [ClaimRuleWsImpl.executeClaimEligible] ---------");
		startTime 			= System.currentTimeMillis();
		String reason		= "";
		String json			= "";
		String jsonResponse	= "";
		logMonitor			= new LogMonitor();
		RuleEligibleResponse res 	= new RuleEligibleResponse();
		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(CalendarHelper.getCurrentDateTime().getTime());
		try{
			logMonitor.setSystemName(eligible);
			logMonitor.setRequestTime(new Date());
			logMonitor.setPolicyNumber(request.getEligibleDto().getPolicyNumber());
			logMonitor.setClientNo(request.getEligibleDto().getClientID());
			logMonitor.setKeyOther(request.getEligibleDto().getClaimType().toString());
			
			json = ruleEngineService.toJson(request.getEligibleDto());
			eligibleLog.debug("::::: request.getJsonXom():"+json);
			requestEligibleLog.debug("==== Request jsonXOM : "+json);
			EligibleModel model = doRuleClaimEligible(request);			
			if(model!=null){
				if(model.getStatus()<0){
					if(res.getErrors() == null)res.setErrors(new ArrayList<String>());
					res.getErrors().add("S01");
					res.setStatus(WSConstants.FAIL);
					statusRule = "fail";
				}else{
					res.setStatus(WSConstants.SUCCESS);
					res.setReason(reason);
					res.setEligibleModel(model);
					statusRule = "success";
				}
				jsonResponse = ruleEngineService.toJson(model);
				eligibleLog.debug("response jsonXom:"+jsonResponse);
				responseEligibleLog.debug("==== Response jsonXOM : "+jsonResponse);
			}else{
				res.setStatus(WSConstants.FAIL);
				res.setReason(errorMessage);
				statusRule = "fail";
			}			
		}catch (Exception e) {
			eligibleLog.error("ERROR:"+e,e);
			res.setStatus(WSConstants.FAIL);
			res.setReason("ERROR:"+e.getMessage());		
			e.printStackTrace();
			statusRule = "fail";
		}
		
		eligibleLog.debug("begin  test write log monitor");
		logMonitor.write();
		eligibleLog.debug("finish test write log monitor");
		
		res.setRespondDate(CalendarHelper.getCurrentDateTime().getTime());
		eligibleLog.debug("::::::::::: end Service [ClaimRuleWsImpl.executeClaimEligible]");
		loadTestEligibleLog.debug(";RuleEligible;" + statusRule + ";" + (System.currentTimeMillis() - startTime) + ";" + endDataCollTime1 + ";" + endRuleTime1 + ";" + endDataCollTime2 + ";" + endRuleTime2);
		return res;
	}
	
	private EligibleModel doRuleClaimEligible(RuleEligibleDto request){
		validateDataColl(eligible);
		RuleEligibleResponse res = new RuleEligibleResponse();
		
		String username = wsClientService.getUsername();
		String password = wsClientService.getPassword();
		String userID 	= wsClientService.getUSER_ID();
		String userPass = wsClientService.getUSER_PASSWORD();
		
		EligibleModel 	eligibleModel 	= null;
		EligibleDto 	eligibleDto 	= request.getEligibleDto();
	
		logMonitor.requestLog("collectEligibleModel");
		eligibleModel = dataCollectorClientService.collectEligibleModel(eligibleDto,request.getCallName());
		logMonitor.responseLog("collectEligibleModel", null, null);
		
		HeaderFastPremiumPaidRequest requestService = new HeaderFastPremiumPaidRequest();
		
		Date date = null;
		if(eligibleDto.getClaimType() == ClaimType.ACCIDENT)date = eligibleDto.getIncidentDate();
		if(eligibleDto.getClaimType() == ClaimType.TPD && "T".equals(eligibleDto.getClaimCause()))date = eligibleDto.getDisabilityDate();
		if(eligibleDto.getClaimType() == ClaimType.TPD && "D".equals(eligibleDto.getClaimCause()))date = eligibleDto.getDeathDate();
		if(eligibleDto.getClaimType() == ClaimType.DEATH)date = eligibleDto.getDeathDate();
		if(eligibleDto.getClaimType() == ClaimType.HEALTH)date = eligibleDto.getAdmissionDate();
		
		List<PolicyFactor> factorList = new ArrayList<PolicyFactor>();
		if(eligibleModel == null || eligibleModel.getPolicies() == null)return null;
		if(eligibleModel.getStatus() < 0)return eligibleModel;
		
		String policyNumber = "";
		for(Policy policy:eligibleModel.getPolicies()){
			if(policy != null && (!policy.getPolicyNumber().isEmpty()) 
				&& (!policy.getPlanCode().isEmpty()) && policy.getIssuedDate() != null){
					PolicyFactor factor = new PolicyFactor();
					factor.setPolicyNumber(policy.getPolicyNumber());
					factor.setPlanCode(policy.getPlanCode());
					factor.setIssueDate(dateToString8digits(policy.getIssuedDate()));
					
					factorList.add(factor);
					
					policyNumber = policyNumber + policy.getPolicyNumber() + ",";
			}
		}
		PolicyFactor factorArray[] = new PolicyFactor[factorList.size()];
		for(int i=0;i<factorList.size();i++)factorArray[i] = factorList.get(i);
		requestService.setPolicyFactorList(factorArray);
		requestService.setIncidentDate(dateToString8digits(date));
		
		FastPremiumPaidService_ServiceLocator locator = new FastPremiumPaidService_ServiceLocator();
		QName fastQname 	= wsClientService.getFastPremiumPaidQName();
		String fastEndPoint = wsClientService.getFastPremiumPaidEndPoint();
		
		FastPremiumPaidService_PortType fast;
		try {
			locator.setFastPremiumPaidWsImplPortEndpointAddress(fastEndPoint);
			fast = (FastPremiumPaidService_PortType) locator.getPort(fastQname, FastPremiumPaidService_PortType.class);
			if(fast!=null){
				((Stub) fast).setTimeout(WSConstants.TIME_OUT);
				((Stub) fast).setHeader("", username, userID);
				((Stub) fast).setHeader("", password, userPass);
			}
			
			requestService.setServiceID(request.getServiceID());
			requestService.setCallName(request.getCallName());
			requestService.setReason("");
			requestService.setDetail("");
		
			eligibleLog.debug("start  call fast premium");
			logMonitor.requestLog("FastPremiumPaidService", policyNumber, null, null, null, requestService.getIncidentDate());
			HeaderFastPremiumPaidResponse response = fast.fastPremiumPaid(requestService);
			logMonitor.responseLog("FastPremiumPaidService", null, null);
			eligibleLog.debug("finish call fast premium");
			
			if(response!=null && response.getFastPremiumPaidResponseList() != null && response.getFastPremiumPaidResponseList().length > 0){
				for(Policy policy:eligibleModel.getPolicies()){
					for(FastPremiumPaidResponse fastResponse:response.getFastPremiumPaidResponseList()){
						if(policy.getPolicyNumber().equals(fastResponse.getPolicyNumber())){
							policy.setPaidDate(stringAS400ToDate(fastResponse.getOrgPaidDateBefore()));
							policy.setPaymentDate(stringAS400ToDate(fastResponse.getPaymentDateBefore()));
							policy.setModeOfPremium(fastResponse.getPaymentMode());
							policy.setPaidDateBack(stringAS400ToDate(fastResponse.getOrgPaidDateAfter()));
							policy.setPaymentDateBack(stringAS400ToDate(fastResponse.getPaymentDateAfter()));
							policy.setExpireDate(stringAS400ToDate(fastResponse.getExpireDate()));
							policy.setSixMonthAnnualFactor(fastResponse.getSixMonthAnnualFactor());
							policy.setQuarterlyFactor(fastResponse.getQuarterlyFactor());
							policy.setMonthlyFactor(fastResponse.getMonthlyFactor());
							eligibleLog.debug("policy   : >>" + policy.getPolicyNumber() + "<<");
							eligibleLog.debug("factor 1 : >>" + policy.getMonthlyFactor() + "<<");
							eligibleLog.debug("factor 3 : >>" + policy.getQuarterlyFactor() + "<<");
							eligibleLog.debug("factor 6 : >>" + policy.getSixMonthAnnualFactor() + "<<");
							eligibleLog.debug("mode     : >>" + policy.getModeOfPremium() + "<<");
							eligibleLog.debug("PaidDate     : >>" + policy.getPaidDate() + "<<");
						}
					}
				}
			}else{
				eligibleLog.debug("fast premium is null");
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			errorMessage = "Call FastPremiumService fail";
			eligibleLog.debug("Call FastPremiumService fail : " + e.getMessage());
			eligibleModel.setStatus(WSConstants.FAIL);
			logMonitor.responseLog("FastPremiumPaidService", null, e.getMessage());
			return eligibleModel; 
		} catch (RemoteException e) {
			e.printStackTrace();
			errorMessage = "Call FastPremiumService fail";
			eligibleLog.debug("Call FastPremiumService fail : " + e.getMessage());
			eligibleModel.setStatus(WSConstants.FAIL);
			logMonitor.responseLog("FastPremiumPaidService", null, e.getMessage());
			return eligibleModel; 
		} catch (Exception e) {
			e.printStackTrace();
			errorMessage = "Call FastPremiumService fail";
			eligibleLog.debug("Call FastPremiumService fail : " + e.getMessage());
			eligibleModel.setStatus(WSConstants.FAIL);
			logMonitor.responseLog("FastPremiumPaidService", null, e.getMessage());
			return eligibleModel; 
		}
		
		endDataCollTime1 = System.currentTimeMillis() - startTime;
		if(eligibleModel != null){			
			Map<String, Object> in 	= new HashMap<String, Object>();
			in.put("eligibleModel", eligibleModel);

			long startRule 			= System.currentTimeMillis();
			logMonitor.requestLog("ruleEligible1");
			boolean executeResult = ruleEngineService.executeRule(RULESETPATH_ELIGIBLE1_CLAIM, in, eligible);
			logMonitor.responseLog("ruleEligible1", null, null);
			endRuleTime1 = System.currentTimeMillis() - startRule;
			
			eligibleLog.debug("Eligible part1: execute success = " + executeResult);
			eligibleModel = (EligibleModel) ruleEngineService.getOutputParameter("eligibleModel");
			res.setDetail(request.getIpAddress());
			res.setReason("");
			res.setServiceID(request.getServiceID());
			res.setRequestedDate(request.getRequestedDate());
			res.setRespondDate(CalendarHelper.getCurrentDateTime().getTime());
			if(executeResult){
				long startTime2 = System.currentTimeMillis();
				res = new RuleEligibleResponse();
				res.setDetail(request.getIpAddress());
				res.setServiceID(request.getServiceID());
				res.setRequestedDate(CalendarHelper.getCurrentDateTime().getTime());
				
				QName cvQname 			= wsClientService.getCashValueQName();
				String cvEndPoint 		= wsClientService.getCashValueEndPoint();
				List<String> policies 	= new ArrayList<String>();
				try {
					for(Policy pol : eligibleModel.getPolicies()){
						if(pol.isReturnCash())policies.add(pol.getPolicyNumber());
						eligibleLog.debug("policiesList 	: " + policies);
						CashValueGathererService_ServiceLocator cvLocator = new CashValueGathererService_ServiceLocator();
						cvLocator.setCashValueGathererWsImplPortEndpointAddress(cvEndPoint);
						CashValueGathererService_PortType cv;
						cv = (CashValueGathererService_PortType) cvLocator.getPort(cvQname, CashValueGathererService_PortType.class);
						if(cv!=null){
							((Stub) cv).setTimeout(WSConstants.TIME_OUT);
							((Stub) cv).setHeader("", username, userID);
							((Stub) cv).setHeader("", password, userPass);
						}
						HeaderCashValueGathererRequest cvRequestService = new HeaderCashValueGathererRequest();
						eligibleLog.debug("getPolicyHistoryID 	: " + pol.getPolicyHistoryID());
						eligibleLog.debug("getPolicyNumber 		: " + pol.getPolicyNumber());
						eligibleLog.debug("getModeOfPremium	 	: " + pol.getModeOfPremium());
						
						if(pol.isOverDue() && pol.getPaidDate() != null && pol.getPolicyNumber() != null){
							
							eligibleLog.debug("pol paid Date : " + pol.getPaidDate());
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(pol.getPaidDate());
							calendar.add(Calendar.MONTH, pol.getModeOfPremium());
							eligibleLog.debug("get time : " + calendar.getTime());
							

							String cvDate 		= dateToString(calendar.getTime(),eligible);
//							String currentDate 	= dateToString(Calendar.getInstance().getTime(),eligible);
//							eligibleLog.debug("cvDate : " + cvDate + ", currentDate : " + currentDate);
							eligibleLog.debug("cvDate : " + cvDate);
							
							cvRequestService.setServiceID(request.getServiceID());
							cvRequestService.setCallName(request.getCallName());
							cvRequestService.setReason("");
							cvRequestService.setDetail("");
						
							cvRequestService.setPolicyNumber(pol.getPolicyNumber());
							cvRequestService.setGatherRequestDate(cvDate);
//							cvRequestService.setBusinessDate(currentDate);
							
							try{
								eligibleLog.debug("start  call cash value");
								logMonitor.requestLog("SOA-CLM-046_CASH-VALUE", pol.getPolicyNumber(), null, null, null, cvDate);
								CashValueGathererResponse response = cv.cashValueGatherer(cvRequestService);
								logMonitor.responseLog("SOA-CLM-046_CASH-VALUE", response.getStatusRespon(), response.getMessageRespon());
								eligibleLog.debug("finish call cash value");
								
								double cashValue = response.getCashValue();
								eligibleLog.debug("cv : " + cashValue);
								pol.setCashValue(cashValue);
							}
							catch(Exception ex){
								eligibleLog.error("Exception on CashValueGatherer");
								pol.setCashValue(0.0D);
								
								throw ex;
							}
						}
					}
				} catch (ServiceException e) {
					e.printStackTrace();
					errorMessage = "Call CashValueService fail";
					eligibleLog.debug("Call CashValueService fail : " + e.getMessage());
					eligibleModel.setStatus(WSConstants.FAIL);
					
					logMonitor.responseLog("SOA-CLM-046_CASH-VALUE", null, e.getMessage());
					return eligibleModel; 
				} catch (RemoteException e) {
					e.printStackTrace();
					errorMessage = "Call CashValueService fail";
					eligibleLog.debug("Call CashValueService fail : " + e.getMessage());
					eligibleModel.setStatus(WSConstants.FAIL);
					logMonitor.responseLog("SOA-CLM-046_CASH-VALUE", null, e.getMessage());
					return eligibleModel; 
				} catch (Exception e) {
					e.printStackTrace();
					errorMessage = "Call CashValueService fail";
					eligibleLog.debug("Call CashValueService fail : " + e.getMessage());
					eligibleModel.setStatus(WSConstants.FAIL);
					logMonitor.responseLog("SOA-CLM-046_CASH-VALUE", null, e.getMessage());
					return eligibleModel; 
				}
				
				List<GetResponseCheckReturnCash> returnCashResponse = callCheckReturnCash(request,policies,eligibleModel);
				
				if(returnCashResponse != null && returnCashResponse.size() > 0){
					for(Policy pol : eligibleModel.getPolicies()){
						if(pol.isReturnCash() && pol.getCoverages() != null && pol.getCoverages().size() > 0){
							for(GetResponseCheckReturnCash returnCash : returnCashResponse){
								if(returnCash.getPolicyNumber().equals(pol.getPolicyNumber())){
									for(Coverage coverage:pol.getCoverages()){
										if(coverage.isReturnCash()){
											eligibleLog.debug("coverage isReturnCash : "+coverage.getCoverageCode());
											coverage.setHasReturnCash(true);
											coverage.setReturnCashType(returnCash.getStatusCode());
											eligibleLog.debug("coverage ReturnCashType : "+coverage.getReturnCashType());
										}
									}
								}
							}
						}
					}
				}
				if(returnCashResponse == null){
					eligibleModel.setStatus(WSConstants.FAIL);
					return eligibleModel;
				}
				endDataCollTime2 	= System.currentTimeMillis() - startTime2;
				in 					= new HashMap<String, Object>();
				in.put("eligibleModel", eligibleModel);
				
				startRule 		= System.currentTimeMillis();
				logMonitor.requestLog("ruleEligible2");
				executeResult 	= ruleEngineService.executeRule(RULESETPATH_ELIGIBLE2_CLAIM, in, eligible);
				logMonitor.responseLog("ruleEligible2", null, null);
				endRuleTime2 	= System.currentTimeMillis() - startRule;
				
				eligibleLog.debug("Eligible part2: execute success = " + executeResult);
				eligibleModel = (EligibleModel) ruleEngineService.getOutputParameter("eligibleModel");
				
				eligibleLog.debug("----------------Map addTempPolicy-------------");				
				eligibleLog.debug("before Policies size " + eligibleModel.getPolicies().size());				
				//find remove tempPolicies  by ging
				if(eligibleModel.getTempPolicies() != null && eligibleModel.getTempPolicies().size() > 0){	
					Iterator<String> tempPolicyItr = eligibleModel.getTempPolicies().iterator();
					String tempPolicy;
					while(tempPolicyItr.hasNext()){
						tempPolicy = tempPolicyItr.next();
						for(Policy policy:eligibleModel.getPolicies()){
							if(tempPolicy.equals(policy.getPolicyNumber())){
								tempPolicyItr.remove();
								break;
							}
						}
					}	
				}
				
				if(eligibleModel.getTempPolicies()!=null && eligibleModel.getTempPolicies().size()>0){
				//eligibleLog.debug("Before TempPolicies size : " +eligibleModel.getTempPolicies().size());	
					for(int i=0; i<eligibleModel.getTempPolicies().size();i++){
						String tempdelete = eligibleModel.getTempPolicies().get(i).trim();
//						if(eligibleModel.getPolicyType().equals("claim")){
							if(tempdelete.length()==10 && !tempdelete.startsWith("9")){
								eligibleModel.getTempPolicies().remove(i);
								i--;
							}
//						}else{
							if(tempdelete.length()==8 ||(tempdelete.length()!=8 && tempdelete.startsWith("9"))){
								eligibleModel.getTempPolicies().remove(i);
								i--;
							}
						}
//					}
				}
				//add tempPolicy  by ging
				String tempPolicyGroupType;
				if(eligibleModel.getTempPolicies() != null && eligibleModel.getTempPolicies().size() > 0){
				eligibleLog.debug("TempPolicies size : " + eligibleModel.getTempPolicies().size());
					for(String policy:eligibleModel.getTempPolicies()){
						String temp = policy.trim();
						if(temp.length()==10 && !temp.startsWith("9"))tempPolicyGroupType = "Ordinary";
						else tempPolicyGroupType = "IL";
						com.mtl.eligible.model.mtlclaim.master.PolicyMs policyMs = new PolicyMs(temp,tempPolicyGroupType);
						policyMs.setClaim(false);
						com.mtl.eligible.model.mtlclaim.Policy updatePolicy = new com.mtl.eligible.model.mtlclaim.Policy(policyMs, null);
						eligibleModel.getPolicies().add(updatePolicy);
					}
				}
				eligibleLog.debug("Policies size " + eligibleModel.getPolicies().size());	
				eligibleLog.debug("---------------------------------");
				//clear List TempPolicies		
				if(eligibleModel.getTempPolicies() != null && eligibleModel.getTempPolicies().size() > 0)eligibleModel.getTempPolicies().clear();				
				if(executeResult)res.setRespondDate(CalendarHelper.getCurrentDateTime().getTime());
				else{
					eligibleModel.setStatus(WSConstants.FAIL);
					eligibleLog.warn("Rule model is null");
				}
			}else{
				eligibleModel.setStatus(WSConstants.FAIL);
				return eligibleModel; 
			}
		}
		return eligibleModel;
	}

	private List<GetResponseCheckReturnCash> callCheckReturnCash(RuleEligibleDto ruleEligibleDto, List<String> policies,EligibleModel eligibleModel){
		EligibleDto eligibleDto = ruleEligibleDto.getEligibleDto();
		String requestDate = DateUtils.formatDate(((eligibleDto.getClaimType().equals(ClaimType.ACCIDENT))?eligibleDto.getIncidentDate()
							:(eligibleDto.getClaimType().equals(ClaimType.DEATH))?eligibleDto.getDeathDate()
							:(eligibleDto.getClaimType().equals(ClaimType.TPD) && "T".equals(eligibleDto.getClaimCause()))?eligibleDto.getDisabilityDate()
							:(eligibleDto.getClaimType().equals(ClaimType.TPD) && "D".equals(eligibleDto.getClaimCause()))?eligibleDto.getDeathDate()
							:(eligibleDto.getClaimType().equals(ClaimType.HEALTH))?eligibleDto.getAdmissionDate():null), DateUtils.getYyyyMMdd());
		if(policies != null && policies.size() > 0 && requestDate != null){
			try {
				GetCheckReturnCashServiceLocator returnCashLocator = new GetCheckReturnCashServiceLocator();
				QName returnCashQname = wsClientService.getReturnCashQName();
				String returnCashEndPoint = wsClientService.getReturnCashEndPoint();
				String username = wsClientService.getUsername();
				String password = wsClientService.getPassword();
				String userID 	= wsClientService.getUSER_ID();
				String userPass = wsClientService.getUSER_PASSWORD();
				
				returnCashLocator.setGetCheckReturnCashWsImplPortEndpointAddress(returnCashEndPoint);
				GetCheckReturnCash returnCash = (GetCheckReturnCash) returnCashLocator.getPort(returnCashQname, GetCheckReturnCash.class);
				if(returnCash!=null){
					((Stub) returnCash).setTimeout(WSConstants.TIME_OUT);
					((Stub) returnCash).setHeader("", username, userID);
					((Stub) returnCash).setHeader("", password, userPass);
				}
				
				GetRequestCheckReturnCash headerRequest = new GetRequestCheckReturnCash();
				
				String[] str 		= new String[policies.size()];
				String policyNumber	= "";
				for(int i=0;i<policies.size();i++){
					str[i] = policies.get(i);
					policyNumber = policyNumber + policies.get(i) + ",";
				}
				
				headerRequest.setVH_POLICY(str);
				headerRequest.setVH_DISB_TYPE("NCB");
				
				GetCheckReturnCashRequest request = new GetCheckReturnCashRequest();
				request.setServiceID(ruleEligibleDto.getServiceID());
				request.setCallName("LOF4CLM");
				request.setReason("call");
				request.setStatus(1);
				request.setDetail("");
				request.setGetRequestCheckReturnCash(headerRequest);
				
				eligibleLog.debug("start  call return cash");
				logMonitor.requestLog("GetCheckReturnCashService", policyNumber, null, null, null, "NCB");
				GetCheckReturnCashResponse response = returnCash.getCheckReturnCash(request);
				logMonitor.responseLog("GetCheckReturnCashService", null, null);
				eligibleLog.debug("finish call return cash");
				
				Date issueDate = new Date();
				Date requestDateNew = stringAS400ToDate(requestDate);
				
				List<GetResponseCheckReturnCash> checkReturnCashs = new ArrayList<GetResponseCheckReturnCash>();
				if(response!=null && response.getGetResponseCheckReturnCashList()!=null && response.getGetResponseCheckReturnCashList().length>0){
					eligibleLog.debug("response data return cash : " + response.getGetResponseCheckReturnCashList().length);	
					for(GetResponseCheckReturnCash cash : response.getGetResponseCheckReturnCashList()){
						eligibleLog.debug("policyNumber : >>" + cash.getPolicyNumber() + "<<");
						eligibleLog.debug("statusCode   : >>" + cash.getStatusCode() + "<<");
						eligibleLog.debug("voucherDate  : >>" + cash.getVoucherDate() + "<<");
						eligibleLog.debug("----------------------------------------------------");
						
						for(Policy policy:eligibleModel.getPolicies()){
							if(policy != null && policy.getIssuedDate() != null && policy.isReturnCash()){
								if(cash.getPolicyNumber().equals(policy.getPolicyNumber())){
									eligibleLog.debug("PolicyNumber getPolicy:"+policy.getPolicyNumber());
									eligibleLog.debug("policyNumber getReturnCash: "+ cash.getPolicyNumber());
									issueDate=policy.getIssuedDate();
									eligibleLog.debug("issueDate  befor: >>" + issueDate + "<<");
								}							
							}
						}
						List<Date>checkVoucherDate 	= new ArrayList<Date>();
						String voucherDate 			= DateUtils.formatDate(DateUtils.convertStringToDate(DateUtils.getDATETIME_PATTERN(), cash.getVoucherDate()), DateUtils.getYyyyMMdd());				
						checkVoucherDate.add(stringAS400ToDate(voucherDate));
						boolean checkFlag 			= false;
						checkFlag 					= checkReturnCash(issueDate,requestDateNew,checkVoucherDate,3);
						eligibleLog.debug("checkFlag for returnCash:"+checkFlag);	
						if(checkFlag)checkReturnCashs.add(cash);
					}
					eligibleLog.debug("response data return cash : " + checkReturnCashs.size());
					return checkReturnCashs;
				}
				eligibleLog.debug("response data return cash is null");
				return checkReturnCashs;
			} catch (ServiceException e) {
				e.printStackTrace();
				errorMessage = "Call CheckReturnCash fail";
				eligibleLog.debug("Call CheckReturnCash fail : " + e.getMessage());
				logMonitor.responseLog("GetCheckReturnCashService", null, e.getMessage());
				return null;
			} catch (RemoteException e) {
				e.printStackTrace();
				errorMessage = "Call CheckReturnCash fail";
				eligibleLog.debug("Call CheckReturnCash fail : " + e.getMessage());
				logMonitor.responseLog("GetCheckReturnCashService", null, e.getMessage());
				return null;
			} catch(ParseException e){
				e.printStackTrace();
				errorMessage = "convert Date fail";
				eligibleLog.debug("convert Date fail : " + e.getMessage());
				logMonitor.responseLog("GetCheckReturnCashService", null, e.getMessage());
				return null;
			}catch(Exception e){
				e.printStackTrace();
				errorMessage = "Call CheckReturnCash fail";
				eligibleLog.debug("Call CheckReturnCash fail : " + e.getMessage());
				logMonitor.responseLog("GetCheckReturnCashService", null, e.getMessage());
				return null;
			}
		}
		return new ArrayList<GetResponseCheckReturnCash>();
	}
	
	private void validateDataColl(String logRule){
		Logger log 		= Logger.getLogger(logRule);
		String isUsed 	= ResourceConfigManager.getProperty("rules.datacoll.isused");		
		if(WSConstants.DataColl.IS_USED_FALSE.equals(isUsed))this.isUsedDataColl=WSConstants.DataColl.IS_USED_FALSE;
		if(WSConstants.DataColl.IS_USED_TRUE.equals(this.isUsedDataColl)){			
			if(dataCollectorClientService != null){
				String setAddress = ResourceConfigManager.getProperty("rules.datacoll.isSetAddress");
				if(WSConstants.IS_TRUE.equals(setAddress))this.isSetAddress=WSConstants.IS_TRUE;
				if(WSConstants.IS_TRUE.equals(this.isSetAddress)){
					String wsAddrerss 	= ResourceConfigManager.getProperty("rules.datacoll.ws.wsdl.address");
					String qname 		= ResourceConfigManager.getProperty("rules.datacoll.ws.qname");	
					log.debug("setServiceAddress,[wsAddrerss]=" + wsAddrerss + ",[qname]=" + qname);
					try{
						if(StringUtils.isNotBlank(wsAddrerss)&&StringUtils.isNotBlank(qname))
							dataCollectorClientService.setServiceAddress(wsAddrerss, qname);
					}catch (Exception e) {
						log.error("ERROR while trying to setServiceAddress,[wsAddrerss]=" + wsAddrerss + ",[qname]=" + qname + ":" + e, e);
						this.isUsedDataColl = WSConstants.DataColl.IS_USED_FATAL;
					}
				}
			}else log.warn("ERROR dataCollectorClientService is null.");
		}	
	}
	
	private String dateToString(Date date,String logRule){
		Logger log = Logger.getLogger(logRule);
		log.debug("date : " + date);
		if(date == null){
			log.debug("date is null");
			return null;
		}
		final Locale ENG_LOCALE = new Locale("en", "EN");
		SimpleDateFormat sf 	= new SimpleDateFormat("dd/MM/yyyy", ENG_LOCALE);
		String temp 			= sf.format(date);
		log.debug("temp : " + temp);
		return temp;
	}

	private String dateToString8digits(Date date){
		if(date == null || date.equals("null"))return null;
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd", ENG_LOCALE);
		String temp = sf.format(date);
		return temp;
	}
	
	private Date stringAS400ToDate(String date){
		if(date == null || date.isEmpty())return null;
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd", ENG_LOCALE);
		try {
			return sf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Boolean checkReturnCash(Date issueDate,Date requestDate,List<Date>voucherDateList,int yearPeriod){
		eligibleLog.debug("-----------Call CheckFlagReturnCash----------- ");
		eligibleLog.debug("requestDate checkReturnCash : "+requestDate);		
		if(requestDate.getTime() < issueDate.getTime()){
			eligibleLog.debug("Call checkReturnCash fail : requestDate < issueDate");
			return false;
		}
		Calendar temp 		= Calendar.getInstance();
		Calendar temp1 		= Calendar.getInstance();
		Date issueDateMax 	= new Date(issueDate.getTime());
		boolean foud = false;
			while(!foud){
				issueDate = issueDateMax;
				temp.setTime(issueDate);
				temp.add(Calendar.YEAR, yearPeriod);
				issueDateMax = temp.getTime();
					if(issueDate.getTime() <= requestDate.getTime() && requestDate.getTime() <= issueDateMax.getTime()){
						temp1.setTime(issueDate);
						temp1.add(Calendar.DATE, 1);
						issueDate = temp1.getTime();
						foud = true;
						
					}
			}
			for(Date voucherDate:voucherDateList){
				eligibleLog.debug("issueDate after    : " + issueDate);
				eligibleLog.debug("issueDateMax after : " + issueDateMax);
				eligibleLog.debug("voucherDateList checkReturnCash : " + voucherDate);
				if(issueDate.getTime() <= voucherDate.getTime() && voucherDate.getTime() <= issueDateMax.getTime())return true;
			}
		return false;
	}
}
