package com.mtl.ws.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mtl.LogMonitor;
import com.mtl.benefitAnalysis.dto.BenefitAnalysisDto;
import com.mtl.benefitAnalysis.model.mtlclaim.Claim;
import com.mtl.benefitAnalysis.model.mtlclaim.ClaimRemain;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisModel;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisRuleModel;
import com.mtl.benefitAnalysis.util.CalendarHelper;
import com.mtl.constants.RuleWsConstants;
import com.mtl.constants.WSConstants;
import com.mtl.rules.service.DataCollectorClientService;
import com.mtl.service.RuleEngineService;
import com.mtl.utils.ApplicationContextUtils;
import com.mtl.utils.ResourceConfigManager;
import com.mtl.ws.bean.RuleBenefitDto;
import com.mtl.ws.bean.RuleBenefitResponse;
import com.mtl.ws.service.BenefitRuleWs;

@WebService(
	endpointInterface = "com.mtl.ws.service.BenefitRuleWs"
	, targetNamespace="http://www.claimrulews.benefitanalysis.ws.application.muangthai.co.th/"
	, serviceName="BenefitRuleService"
)
public class BenefitRuleWsImpl implements BenefitRuleWs{
	
	private String benefit 		= RuleWsConstants.BENEFIT;
	private Logger benefitLog 	= Logger.getLogger(benefit);
	
	private RuleEngineService ruleEngineService 					= (RuleEngineService)ApplicationContextUtils.getBean("ruleEngineService");
	private DataCollectorClientService dataCollectorClientService 	= (DataCollectorClientService)ApplicationContextUtils.getBean("dataCollectorClientService");
	private final String RULESETPATH_BENEFITANALYSIS_CLAIM			= ResourceConfigManager.getProperty("claim.benefitanalysis-1.ruleset.path");
	private String isUsedDataColl	= "1";
	private String isSetAddress		= "0";
	
	private Logger requestBenefitLog 	= Logger.getLogger("requestBenefit");
	private Logger responseBenefitLog 	= Logger.getLogger("responseBenefit");
	private Logger loadTestBenefitLog 	= Logger.getLogger("loadTestBenefitLog");
	private long startTime;
	private long endDataCollTime;
	private long endRuleTime;
	private String statusRule;
	private LogMonitor logMonitor;

	public @WebResult(name = "response")
	RuleBenefitResponse executeClaimBenefit(@WebParam(name = "request") RuleBenefitDto request) {
		benefitLog.debug("::::::::::: start [ClaimRuleWsImpl.executeClaimBenefit]---------");
		startTime = System.currentTimeMillis();
		String reason = "";
		String json = "";
		String jsonResponse = "";
		logMonitor = new LogMonitor();
		RuleBenefitResponse res = new RuleBenefitResponse();
		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(CalendarHelper.getCurrentDateTime().getTime());
		try {
			logMonitor.setSystemName(benefit);
			logMonitor.setRequestTime(new Date());
			String policyNumber = "";
			for (Claim claim : request.getBenefitAnalysisDto().getCurrentClaims())
				policyNumber = policyNumber + claim.getPolicyNumber() + ",";
			logMonitor.setPolicyNumber(policyNumber);

			json = ruleEngineService.toJson(request.getBenefitAnalysisDto());
			benefitLog.debug("::::: executeClaimBenefit request.getJsonXom():" + json);
			requestBenefitLog.debug("==== Request jsonXOM : " + json);
			// setResponseDtoDetail(res,benefit);
			res.setDetail(request.getIpAddress());
			BenefitAnalysisModel model = doRuleClaimBenefit(request.getBenefitAnalysisDto(), request.getCallName());
			if (model != null) {
				if (model.getStatus() < 0) {
					if (res.getErrors() == null)
						res.setErrors(new ArrayList<String>());
					res.getErrors().add("S01");
					res.setStatus(WSConstants.FAIL);
					statusRule = "fail";
				}
				else {
					res.setStatus(WSConstants.SUCCESS);
					res.setReason(reason);
					res.setBenefitAnalysisModel(model);
					statusRule = "success";
					// res.setJsonXom(ruleEngineService.toJson(model));
				}
				jsonResponse = ruleEngineService.toJson(model);
				benefitLog.debug("::::: executeClaimBenefit response jsonXom:" + jsonResponse);
				responseBenefitLog.debug("==== Response jsonXOM : " + jsonResponse);
			}
			else {
				res.setStatus(WSConstants.FAIL);
				statusRule = "fail";
			}
		}
		catch (Exception e) {
			benefitLog.error("ERROR:" + e, e);
			res.setStatus(WSConstants.FAIL);
			res.setReason("ERROR:" + e.getMessage());
			e.printStackTrace();
			statusRule = "fail";
		}

		benefitLog.debug("begin  test write log monitor");
		logMonitor.write();
		benefitLog.debug("finish test write log monitor");

		res.setRespondDate(CalendarHelper.getCurrentDateTime().getTime());

		/**
		 * benefitLog.debug("==== begin benefitAnalysis Auditing Log ====");
		 * try{ if(StringUtils.isNotBlank(jsonResponse)){
		 * auditLogService.log(json, jsonResponse,
		 * getAuditLogDtoByRuleDto(res,RULES_CLAIM_ACTION_BEN
		 * ,RULESETPATH_BENEFITANALYSIS_CLAIM,benefit)); }else{
		 * auditLogService.log(json, res.getReason(),
		 * getAuditLogDtoByRuleDto(res
		 * ,RULES_CLAIM_ACTION_BEN,RULESETPATH_BENEFITANALYSIS_CLAIM,benefit));
		 * } benefitLog.debug("==== finish benefitAnalysis Auditing Log ====");
		 * }catch (Exception e) {
		 * benefitLog.debug("Could not log to db.serviceId:"
		 * +res.getServiceID()); } /
		 **/
		benefitLog.debug("::::::::::: end Service [ClaimRuleWsImpl.executeClaimBenefit]");
		loadTestBenefitLog.debug(";RuleBenefit;" + statusRule + ";" + (System.currentTimeMillis() - startTime) + ";" + endDataCollTime + ";"
				+ endRuleTime);
		return res;
	}

	private BenefitAnalysisModel doRuleClaimBenefit(BenefitAnalysisDto benefitAnalysisDto,String callName){
		validateDataColl(benefit);
		BenefitAnalysisModel benefitAnalysisModel 	= null;
		BenefitAnalysisModel tempModel	 			= null;
		
		logMonitor.requestLog("collectBenefitAnalysisModel");
		tempModel 		= dataCollectorClientService.collectBenefitAnalysisModel(benefitAnalysisDto,callName);
		logMonitor.responseLog("collectBenefitAnalysisModel", null, null);
		
		endDataCollTime = System.currentTimeMillis() - startTime;
		if(tempModel != null){
			if(tempModel.getStatus()<0){
				benefitAnalysisModel = tempModel;
				benefitAnalysisModel.setStatus(tempModel.getStatus());
				return benefitAnalysisModel;
			}
			Map<String, Object> in = new HashMap<String, Object>();
			BenefitAnalysisRuleModel benefitAnalysisRuleModel = new BenefitAnalysisRuleModel();
			benefitAnalysisRuleModel.setBenefitGroups(tempModel.getBenefitGroups());
			benefitAnalysisRuleModel.setCurrentClaims(tempModel.getCurrentClaims());
			benefitAnalysisRuleModel.setFixedClaims(tempModel.getFixedClaims());
			benefitAnalysisRuleModel.setClaimRemain(new ClaimRemain());
			benefitAnalysisRuleModel.setClaimNumbers(tempModel.getClaimNumbers());
			
			in.put("benefitAnalysisRuleModel", benefitAnalysisRuleModel);
			
			long startRule = System.currentTimeMillis();
			logMonitor.requestLog("ruleBenefit");
			boolean executeResult = ruleEngineService.executeRule(RULESETPATH_BENEFITANALYSIS_CLAIM, in, benefit);
			logMonitor.responseLog("ruleBenefit", null, null);
			endRuleTime = System.currentTimeMillis() - startRule;
			
			benefitLog.debug("execute test success = " + executeResult);
			benefitAnalysisRuleModel = (BenefitAnalysisRuleModel) ruleEngineService.getOutputParameter("benefitAnalysisRuleModel");	
			
			if(executeResult){
				if(benefitAnalysisRuleModel==null){
					benefitLog.debug("Returned test rule model is null");
					benefitAnalysisModel = null;
				}
				benefitAnalysisModel = tempModel; 
				benefitAnalysisModel.setBenefitGroups(benefitAnalysisRuleModel.getBenefitGroups());
				benefitAnalysisModel.setCurrentClaims(benefitAnalysisRuleModel.getCurrentClaims());
				benefitAnalysisModel.setFixedClaims(benefitAnalysisRuleModel.getFixedClaims());
			}else benefitAnalysisModel = null;
		}else benefitLog.debug("tempModel is null");
		return benefitAnalysisModel;
	}
	
	private void validateDataColl(String logRule){
		Logger log = Logger.getLogger(logRule);
		String isUsed = ResourceConfigManager.getProperty("rules.datacoll.isused");		
		if(WSConstants.DataColl.IS_USED_FALSE.equals(isUsed))this.isUsedDataColl = WSConstants.DataColl.IS_USED_FALSE;
		if(WSConstants.DataColl.IS_USED_TRUE.equals(this.isUsedDataColl)){			
			if(dataCollectorClientService != null){
				String setAddress = ResourceConfigManager.getProperty("rules.datacoll.isSetAddress");
				if(WSConstants.IS_TRUE.equals(setAddress))this.isSetAddress = WSConstants.IS_TRUE;
				if(WSConstants.IS_TRUE.equals(this.isSetAddress)){
					String wsAddrerss 	= ResourceConfigManager.getProperty("rules.datacoll.ws.wsdl.address");
					String qname 		= ResourceConfigManager.getProperty("rules.datacoll.ws.qname");	
					log.debug("setServiceAddress,[wsAddrerss]="+wsAddrerss+",[qname]="+qname);
					try{
						if(StringUtils.isNotBlank(wsAddrerss)&&StringUtils.isNotBlank(qname))
							dataCollectorClientService.setServiceAddress(wsAddrerss, qname);
					}catch (Exception e) {
						log.error("ERROR while trying to setServiceAddress,[wsAddrerss]=" + wsAddrerss + ",[qname]=" + qname+":" + e, e);
						this.isUsedDataColl=WSConstants.DataColl.IS_USED_FATAL;
					}				
				}				
			}else log.warn("ERROR dataCollectorClientService is null.");
		}	
	}
}
