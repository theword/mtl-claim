/*
 * 
 */
package com.mtl.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.mtl.ws.bean.RuleHospitalBlacklistDto;
import com.mtl.ws.bean.RuleHospitalBlacklistResponse;

/**
 * The Interface HospitalRuleService.
 */
@WebService(
		name = "HospitalRuleService"
		, targetNamespace = "http://www.claimrulews.hospital.ws.application.muangthai.co.th/"
	)
	@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface HospitalRuleWs {
	
	/**
	 * Execute claim hospital blacklist.
	 *
	 * @param request the request
	 * @return the rule dto
	 */
	public @WebResult(name="response") RuleHospitalBlacklistResponse executeClaimHospitalBlacklist(@WebParam(name="request") RuleHospitalBlacklistDto request);
	
}
