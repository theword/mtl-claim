package com.mtl.ws.service.impl;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.mtl.utils.ResourceConfigManager;
import com.mtl.ws.service.WSClientService;

public class WSClientServiceImpl implements WSClientService {
	Logger log = Logger.getLogger(this.getClass());
	
	private QName cashValueQName;
	private String cashValueEndPoint;
	
	private QName fastPremiumPaidQName;
	private String fastPremiumPaidEndPoint;
	
	private String linkClaimEndPoint;
	private QName linkClaimQName;
	
	private String returnCashEndPoint;
	private QName returnCashQName;
	
	private String USER_ID;
	private String USER_PASSWORD;
	
	static private final String USERNAME = "username";
	static private final String PASSWORD = "password";
	
	public void init(){
		try {
			
			this.cashValueQName = new QName(ResourceConfigManager.getProperty("cashValueGather.qname"), ResourceConfigManager.getProperty("cashValueGather.servicename"));
			this.cashValueEndPoint = ResourceConfigManager.getProperty("cashValueGather.endpoint");
			
			this.fastPremiumPaidQName = new QName(ResourceConfigManager.getProperty("fastPremiumPaid.qname"), ResourceConfigManager.getProperty("fastPremiumPaid.servicename"));
			this.fastPremiumPaidEndPoint = ResourceConfigManager.getProperty("fastPremiumPaid.endpoint");
			
			this.linkClaimEndPoint = ResourceConfigManager.getProperty("linkClaim.endpoint");
			this.linkClaimQName = new QName(ResourceConfigManager.getProperty("linkClaim.qname"), ResourceConfigManager.getProperty("linkClaim.servicename"));
			
			this.returnCashEndPoint = ResourceConfigManager.getProperty("returnCash.endpoint");
			this.returnCashQName = new QName(ResourceConfigManager.getProperty("returnCash.qname"), ResourceConfigManager.getProperty("returnCash.servicename"));
			
			this.USER_ID = ResourceConfigManager.getProperty("soa.authen.username");
			this.USER_PASSWORD = ResourceConfigManager.getProperty("soa.authen.password");
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error When Initialize WebService");
		}
	}
	
	public QName getCashValueQName() {
		return cashValueQName;
	}

	public String getCashValueEndPoint() {
		return cashValueEndPoint;
	}

	public QName getFastPremiumPaidQName() {
		return fastPremiumPaidQName;
	}

	public String getFastPremiumPaidEndPoint() {
		return fastPremiumPaidEndPoint;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public String getUSER_PASSWORD() {
		return USER_PASSWORD;
	}

	public String getUsername() {
		return USERNAME;
	}

	public String getPassword() {
		return PASSWORD;
	}
	
	public String getLinkClaimEndPoint() {
		return linkClaimEndPoint;
	}

	public void setLinkClaimEndPoint(String linkClaimEndPoint) {
		this.linkClaimEndPoint = linkClaimEndPoint;
	}

	public QName getLinkClaimQName() {
		return linkClaimQName;
	}

	public void setLinkClaimQName(QName linkClaimQName) {
		this.linkClaimQName = linkClaimQName;
	}

	public String getReturnCashEndPoint() {
		return returnCashEndPoint;
	}

	public QName getReturnCashQName() {
		return returnCashQName;
	}

}
