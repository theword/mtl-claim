/*
 * 
 */
package com.mtl.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.mtl.ws.bean.RuleBenefitDto;
import com.mtl.ws.bean.RuleBenefitResponse;

/**
 * The Interface BenefitRuleService.
 */
@WebService(
		name = "BenefitRuleService"
		, targetNamespace = "http://www.claimrulews.benefitanalysis.ws.application.muangthai.co.th/"
	)
	@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface BenefitRuleWs {
	
	/**
	 * Execute claim benefit.
	 *
	 * @param request the request
	 * @return the rule dto
	 */
	public @WebResult(name="response") RuleBenefitResponse executeClaimBenefit(@WebParam(name="request") RuleBenefitDto request);
	
}
