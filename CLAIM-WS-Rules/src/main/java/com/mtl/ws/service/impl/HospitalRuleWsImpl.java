package com.mtl.ws.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mtl.LogMonitor;
import com.mtl.constants.RuleWsConstants;
import com.mtl.constants.WSConstants;
import com.mtl.hospital.dto.HospitalBlacklistDto;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;
import com.mtl.hospital.util.CalendarHelper;
import com.mtl.rules.service.DataCollectorClientService;
import com.mtl.service.RuleEngineService;
import com.mtl.utils.ApplicationContextUtils;
import com.mtl.utils.ResourceConfigManager;
import com.mtl.ws.bean.RuleHospitalBlacklistDto;
import com.mtl.ws.bean.RuleHospitalBlacklistResponse;
import com.mtl.ws.service.HospitalRuleWs;

@WebService(
	endpointInterface = "com.mtl.ws.service.HospitalRuleWs"
	, targetNamespace="http://www.claimrulews.hospital.ws.application.muangthai.co.th/"
	, serviceName="HospitalRuleService"
)
public class HospitalRuleWsImpl implements HospitalRuleWs{
	
	private String hospitalBlackList 			= RuleWsConstants.HOSPITAL_BLACK_LIST;
	private Logger hospitalBlackListLog 		= Logger.getLogger(hospitalBlackList);
	private RuleEngineService ruleEngineService = (RuleEngineService)ApplicationContextUtils.getBean("ruleEngineService");
	private DataCollectorClientService dataCollectorClientService 	= (DataCollectorClientService)ApplicationContextUtils.getBean("dataCollectorClientService");
	private final String RULESETPATH_HOSPITAL_CLAIM					= ResourceConfigManager.getProperty("claim.hospitalblacklist.ruleset.path");
	private String isUsedDataColl	= "1";
	private String isSetAddress		= "0";
	
	private Logger requestHospitalBlackListLog 	= Logger.getLogger("requestHospitalBlackList");
	private Logger responseHospitalBlackListLog = Logger.getLogger("responseHospitalBlackList");
	private Logger loadTestHospitalLog 			= Logger.getLogger("loadTestHospitalLog");
	private long startTime;
	private long endDataCollTime;
	private long endRuleTime;
	private String statusRule;

	private LogMonitor logMonitor;
	
	public @WebResult(name="response") RuleHospitalBlacklistResponse executeClaimHospitalBlacklist(@WebParam(name="request") RuleHospitalBlacklistDto request){
		hospitalBlackListLog.debug("::::::::::: start [ClaimRuleWsImpl.executeClaimHospitalBlacklist]---------");
		startTime 			= System.currentTimeMillis();
		String reason		= "";
		String json			= "";
		String jsonResponse	= "";
		logMonitor			= new LogMonitor();
		RuleHospitalBlacklistResponse res = new RuleHospitalBlacklistResponse();
		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(CalendarHelper.getCurrentDateTime().getTime());
		try{
			logMonitor.setSystemName(hospitalBlackList);
			logMonitor.setRequestTime(new Date());
			logMonitor.setKeyOther(request.getHospitalBlacklistDto().getHospitalCode());
			
			json = ruleEngineService.toJson(request.getHospitalBlacklistDto());
			hospitalBlackListLog.debug("::::: request.getJsonXom(): "+json);
			requestHospitalBlackListLog.debug("==== Request jsonXOM : "+json);
			res.setDetail(request.getIpAddress());
			HospitalBlacklistModel model = doRuleClaimHospitalBlacklist(request.getHospitalBlacklistDto(),request.getCallName());			
			if(model!=null){
				if(model.getStatus()<0){
					if(res.getErrors()==null)res.setErrors(new ArrayList<String>());
					res.getErrors().add("S01");
					res.setStatus(WSConstants.FAIL);
					statusRule = "fail";
				}else{
					res.setStatus(WSConstants.SUCCESS);
					res.setReason(reason);
					res.setHospitalBlacklistModel(model);
					statusRule = "success";
				}
				jsonResponse = ruleEngineService.toJson(model);
				hospitalBlackListLog.debug("response jsonXom:"+jsonResponse);
				responseHospitalBlackListLog.debug("==== Response jsonXOM : "+jsonResponse);
			}else{
				res.setStatus(WSConstants.FAIL);
				statusRule = "fail";
			}			
		}catch (Exception e) {
			hospitalBlackListLog.error("ERROR:"+e,e);
			res.setStatus(WSConstants.FAIL);
			res.setReason("ERROR:"+e.getMessage());		
			e.printStackTrace();
			statusRule = "fail";
		}

		hospitalBlackListLog.debug("begin  test write log monitor");
		logMonitor.write();
		hospitalBlackListLog.debug("finish test write log monitor");
		
		res.setRespondDate(CalendarHelper.getCurrentDateTime().getTime());
		hospitalBlackListLog.debug("::::::::::: end Service [ClaimRuleWsImpl.executeClaimHospitalBlacklist]");
		loadTestHospitalLog.debug(";RuleHospitalBlacklist;" + statusRule + ";" + (System.currentTimeMillis() - startTime) + ";" + endDataCollTime + ";" + endRuleTime);
		return res;
	}
	
	private HospitalBlacklistModel doRuleClaimHospitalBlacklist(HospitalBlacklistDto hospitalBlacklistDto,String callName){
		validateDataColl(hospitalBlackList);
		HospitalBlacklistModel hospitalBlacklistModel = dataCollectorClientService.collectHospitalBlacklistModel(hospitalBlacklistDto,callName);
		endDataCollTime = System.currentTimeMillis() - startTime;
		if(hospitalBlacklistModel!=null){
			if(hospitalBlacklistModel.getStatus()<0)return hospitalBlacklistModel;
			
			Map<String, Object> in = new HashMap<String, Object>();
			in.put("hospitalBlacklistModel", hospitalBlacklistModel);
			hospitalBlackListLog.debug("hospitalBlacklistModel.getHospitalCode():"+hospitalBlacklistModel.getHospital().getHospital().getHospitalCode());
			
			long startRule = System.currentTimeMillis();
			logMonitor.requestLog("ruleHospital");
			boolean executeResult = ruleEngineService.executeRule(RULESETPATH_HOSPITAL_CLAIM, in, hospitalBlackList);
			logMonitor.responseLog("ruleHospital", null, null);
			endRuleTime = System.currentTimeMillis() - startRule;
			
			hospitalBlackListLog.debug("execute success = " + executeResult);
			hospitalBlacklistModel = (HospitalBlacklistModel) ruleEngineService.getOutputParameter("hospitalBlacklistModel");
			
			if(!executeResult){
				hospitalBlacklistModel = new HospitalBlacklistModel();
				hospitalBlacklistModel.setStatus(WSConstants.FAIL);
			}
		}else{
			hospitalBlackListLog.warn("Rule model is null");
			hospitalBlacklistModel = new HospitalBlacklistModel();
			hospitalBlacklistModel.setStatus(WSConstants.FAIL);
		}		
		return hospitalBlacklistModel;
	}
	
	private void validateDataColl(String logRule){
		Logger log 		= Logger.getLogger(logRule);
		String isUsed 	= ResourceConfigManager.getProperty("rules.datacoll.isused");		
		if(WSConstants.DataColl.IS_USED_FALSE.equals(isUsed))this.isUsedDataColl = WSConstants.DataColl.IS_USED_FALSE;
		if(WSConstants.DataColl.IS_USED_TRUE.equals(this.isUsedDataColl)){			
			if(dataCollectorClientService != null){
				String setAddress = ResourceConfigManager.getProperty("rules.datacoll.isSetAddress");
				if(WSConstants.IS_TRUE.equals(setAddress))this.isSetAddress = WSConstants.IS_TRUE;
				if(WSConstants.IS_TRUE.equals(this.isSetAddress)){
					String wsAddrerss 	= ResourceConfigManager.getProperty("rules.datacoll.ws.wsdl.address");
					String qname 		= ResourceConfigManager.getProperty("rules.datacoll.ws.qname");	
					log.debug("setServiceAddress,[wsAddrerss]=" + wsAddrerss + ",[qname]=" + qname);
					try{
						if(StringUtils.isNotBlank(wsAddrerss)&&StringUtils.isNotBlank(qname))
							dataCollectorClientService.setServiceAddress(wsAddrerss, qname);
					}catch (Exception e) {
						log.error("ERROR while trying to setServiceAddress,[wsAddrerss]=" + wsAddrerss + ",[qname]=" + qname + ":" + e, e);
						this.isUsedDataColl=WSConstants.DataColl.IS_USED_FATAL;
					}				
				}				
			}else log.warn("ERROR dataCollectorClientService is null.");
		}	
	}
}
