/*
 * 
 */
package com.mtl.ws.bean;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.ws.common.DateTimeAdapter;


/**
 * The Class RuleDto for request service.
 */
@XmlRootElement(name="RuleDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleDto {
	
	/** The service id. */
	@XmlElement(name="ServiceID", required=false)
	private String serviceID;
	
	/** The call name. */
	@XmlElement(name="CallName", required=true)
	private String callName;
	
	/** The IP address. */
	@XmlElement(name="IPAddress", required=true)
	private String ipAddress;
	
	/** The requested date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name="RequestedDate")
	private Date requestedDate;
	
	/** The respond date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name="RespondDate")
	private Date respondDate;
	
	/** The status. */
	@XmlElement(name="Status", required=false)
	private Integer status;
	
	/** The reason. */
	@XmlElement(name="Reason", required=false)
	private String reason;
	
	/** The reason important. */
	@XmlElement(name="ReasonImportant", required=false)
	private String reasonImportant;
	
	/** The detail. */
	@XmlElement(name="Detail", required=false)
	private String detail;
	
	/** The json xom. */
	@XmlElement(name="JsonXom", required=true)
	private String jsonXom;
	
	/** The is incomplete model. */
	@XmlElement(name="IsIncompleteModel", required=true)
	private String isIncompleteModel;

	/**
	 * Gets the service id.
	 *
	 * @return the service id
	 */
	public String getServiceID() {
		return serviceID;
	}
	
	/**
	 * Sets the service id.
	 *
	 * @param serviceID the new service id
	 */
	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}
	
	/**
	 * Gets the call name.
	 *
	 * @return the call name
	 */
	public String getCallName() {
		return callName;
	}
	
	/**
	 * Sets the call name.
	 *
	 * @param callName the new call name
	 */
	public void setCallName(String callName) {
		this.callName = callName;
	}
	
	/**
	 * Gets the requested date.
	 *
	 * @return the requested date
	 */
	public Date getRequestedDate() {
		return requestedDate;
	}
	
	/**
	 * Sets the requested date.
	 *
	 * @param requestedDate the new requested date
	 */
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	
	/**
	 * Gets the respond date.
	 *
	 * @return the respond date
	 */
	public Date getRespondDate() {
		return respondDate;
	}
	
	/**
	 * Sets the respond date.
	 *
	 * @param respondDate the new respond date
	 */
	public void setRespondDate(Date respondDate) {
		this.respondDate = respondDate;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	/**
	 * Gets the reason important.
	 *
	 * @return the reason important
	 */
	public String getReasonImportant() {
		return reasonImportant;
	}
	
	/**
	 * Sets the reason important.
	 *
	 * @param reasonImportant the new reason important
	 */
	public void setReasonImportant(String reasonImportant) {
		this.reasonImportant = reasonImportant;
	}
	
	/**
	 * Gets the reason.
	 *
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	
	/**
	 * Sets the reason.
	 *
	 * @param reason the new reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	/**
	 * Gets the detail.
	 *
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}
	
	/**
	 * Sets the detail.
	 *
	 * @param detail the new detail
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	/**
	 * Gets the json xom.
	 *
	 * @return the json xom
	 */
	public String getJsonXom() {
		return jsonXom;
	}
	
	/**
	 * Sets the json xom.
	 *
	 * @param jsonXom the new json xom
	 */
	public void setJsonXom(String jsonXom) {
		this.jsonXom = jsonXom;
	}
	
	/**
	 * Gets the checks if is incomplete model.
	 *
	 * @return the checks if is incomplete model
	 */
	public String getIsIncompleteModel() {
		return isIncompleteModel;
	}
	
	/**
	 * Sets the checks if is incomplete model.
	 *
	 * @param isIncompleteModel the new checks if is incomplete model
	 */
	public void setIsIncompleteModel(String isIncompleteModel) {
		this.isIncompleteModel = isIncompleteModel;
	}

	/**
	 * Gets the ip address.
	 *
	 * @return the ip address
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the new ip address
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
	
}
