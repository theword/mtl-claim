/*
 * 
 */
package com.mtl.ws.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The Class DateAdapter.
 */
public class DateAdapter extends XmlAdapter<String, Date> {

	/** The format. */
	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	/* (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Date date) throws Exception {
		return format.format(date);
	}

	/* (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Date unmarshal(String dateStr) throws Exception {
		return format.parse(dateStr);
	}

}
