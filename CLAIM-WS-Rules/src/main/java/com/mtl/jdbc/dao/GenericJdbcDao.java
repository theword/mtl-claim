/*
 * 
 */
package com.mtl.jdbc.dao;

import org.springframework.jdbc.core.JdbcTemplate;


/**
 * The Interface GenericJdbcDao.
 */
public interface GenericJdbcDao {

	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public JdbcTemplate getTemplate();
	
}
