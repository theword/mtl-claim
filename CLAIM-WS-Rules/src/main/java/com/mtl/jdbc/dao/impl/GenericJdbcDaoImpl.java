/*
 * 
 */
package com.mtl.jdbc.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.mtl.jdbc.dao.GenericJdbcDao;


/**
 * The Class GenericJdbcDaoImpl.
 */
@Repository
public class GenericJdbcDaoImpl extends JdbcDaoSupport implements GenericJdbcDao{

	/* (non-Javadoc)
	 * @see com.mtl.jdbc.dao.GenericJdbcDao#getTemplate()
	 */
	public JdbcTemplate getTemplate(){
		return this.getJdbcTemplate();
	}
	
}
