package com.mtl.constants;

public class WSConstants {
	
	public static final String IS_FALSE = "0";
	public static final String IS_TRUE 	= "1";
	public static final int SUCCESS 	= 0;
	public static final int FAIL 		= -1;
	public static int TIME_OUT 			= 1200000;

	public class DataColl{
		public static final String IS_USED_FALSE 	= "0";
		public static final String IS_USED_TRUE 	= "1";		
		public static final String IS_USED_FATAL 	= "2";
	} 
}
