/*
 * 
 */
package com.mtl.constants;

/**
 * The Class RuleWsConstants.
 */
public class RuleWsConstants {

	/** The Constant BUNDLE_KEY. */
	public static final String BUNDLE_KEY = "ApplicationResources";
	
	/** The Constant HOSPITAL_BLACK_LIST. */
	public static final String HOSPITAL_BLACK_LIST = "hospitalBlackList";
	
	/** The Constant ELIGIBLE. */
	public static final String ELIGIBLE = "eligible";
	
	/** The Constant BENEFIT. */
	public static final String BENEFIT = "benefit";
	
	/** The Constant CONTINUITY_CLAIM. */
	public static final String CONTINUITY_CLAIM = "continuityClaim";
	
	/** The Constant UNDERWRITE. */
	public static final String UNDERWRITE = "underwrite";
	
	/** The Constant ELIGIBLE. */
	public static final String REQUESTELIGIBLE = "requestEligible";
	
	/** The Constant ELIGIBLE. */
	public static final String RESPONSEELIGIBLE = "responseEligible";
	
	/**
	 * The Class ExecuteResponseType.
	 */
	public class ExecuteResponseType{
		
		/** The Constant STOP. */
		public static final String STOP = "1";
		
		/** The Constant CONTINUE. */
		public static final String CONTINUE = "2";
	} 
	
	/**
	 * The Class OrderValueType.
	 */
	public class OrderValueType{
		
		/** The Constant VOUCHER. */
		public static final String VOUCHER = "VCHRNO";
	}
	
}
