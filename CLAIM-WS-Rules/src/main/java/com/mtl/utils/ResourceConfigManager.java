/*
 * 
 */
package com.mtl.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2547
 * </p>
 * <p>
 * Company: Motif Technology Co., Ltd.
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class ResourceConfigManager {

	private final static Logger log = Logger.getLogger(ResourceConfigManager.class);
	private static Properties prop = null;

	protected final static String CONFIG_FILE = "rule-config.properties";

	static {
		// load configuration data
		try {
			InputStream in = getConfigInputStream();
			prop = new Properties();
			prop.load(in);
		}
		catch (Exception e) {
			log.error("Can't load property file " + CONFIG_FILE, e);
		} // end try
	}

	public static String getProperty(String name) {
		return (String) prop.get(name);
	}

	public static Properties getProperties() {
		return prop;
	}

	private static InputStream getConfigInputStream() {
		InputStream in = null;
		try {
//			PropertiesUtils propUtils = (PropertiesUtils) ApplicationContextUtils.getBean("propertiesUtils");
//			String path = propUtils.getPathRuleConfig();
			
			File propertyFile = new File("/data/conf/clm-rulews/rule-config.properties");
//			File propertyFile = new File("/data/conf/ete/clm-rulews/rule-config.properties");
			
//			File propertyFile = new File(path);
			
			log.info("propertyFile:" + propertyFile);
			if (propertyFile != null && propertyFile.isFile()) {
				log.info("Use config file:" + propertyFile);
				in = new FileInputStream(propertyFile);
			}
			else {
				log.info("Not found config file.Use default config instread.");
				in = ResourceConfigManager.class.getClassLoader().getResourceAsStream(CONFIG_FILE);
			}

		}
		catch (Exception e) {
			log.error("Can't load property file " + CONFIG_FILE, e);
		} // end try
		return in;
	}

}
