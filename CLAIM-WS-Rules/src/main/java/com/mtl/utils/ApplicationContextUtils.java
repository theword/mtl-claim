/*
 * 
 */
package com.mtl.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The Class ApplicationContextUtils.
 */
public class ApplicationContextUtils implements ApplicationContextAware {

	/** The ctx. */
	private static ApplicationContext ctx;
	
	/**
	 * Gets the bean.
	 *
	 * @param name the name
	 * @return the bean
	 */
	public static Object getBean(String name)
	{
		if(ctx==null){
			initialize();
		}
		
		return ctx.getBean(name);
	}

	/**
	 * Initialize.
	 */
	protected static void initialize(){
		
		ctx = new ClassPathXmlApplicationContext(
				new String[]{
						"applicationContext-service.xml"
						, "applicationContext-service-datacoll-client.xml"
//						, "applicationContext-aop-brmlog.xml"
//						, "applicationContext-dao-brmlog.xml"
//						, "applicationContext-jdbc-dao-brmlog.xml"
//						, "applicationContext-resource-brmlog.xml"
//						, "applicationContext-service-brmlog.xml"
						});
	}

	 
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	public void setApplicationContext(ApplicationContext appCtx)
			throws BeansException {
		this.ctx = appCtx;
	}

}
