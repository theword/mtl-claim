/*
 * 
 */
package com.mtl.utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * The Class BeanUtils.
 */
public class BeanUtils {

	/**
	 * Checks if is empty.
	 *
	 * @param st the st
	 * @return true, if is empty
	 */
	public static boolean isEmpty(String st){
		boolean b = true;
		if(st!=null && st.trim().length()>0){
			b = false;
		}
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param st the st
	 * @return true, if is empty
	 */
	public static boolean isEmpty(StringBuilder st){
		boolean b = true;
		if(st!=null && st.toString().trim().length()>0){
			b = false;
		}
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param ls the ls
	 * @return true, if is empty
	 */
	public static boolean isEmpty(List<?> ls ){
		boolean b = true;
		if(ls!=null && !ls.isEmpty()){
			b = false;
		}
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param map the map
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Map<?, ?> map){
		boolean b = true;
		if(map!=null && !map.isEmpty()){
			b = false;
		}
		return(b);
	}

	/**
	 * Checks if is empty.
	 *
	 * @param integer the integer
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Integer integer){
		boolean b = isNull(integer);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param byt the byt
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Byte byt){
		boolean b = isNull(byt);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param s the s
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Short s){
		boolean b = isNull(s);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param l the l
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Long l){
		boolean b = isNull(l);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param c the c
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Character c){
		boolean b = isNull(c);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param f the f
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Float f){
		boolean b = isNull(f);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param d the d
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Double d){
		boolean b = isNull(d);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param num the num
	 * @return true, if is empty
	 */
	public static boolean isEmpty(Number num){
		boolean b = isNull(num);
		return (b);

	}

	/**
	 * Checks if is empty.
	 *
	 * @param d the d
	 * @return true, if is empty
	 */
	public static boolean isEmpty(BigDecimal d){
		boolean b = isNull(d);
		return (b);

	}

	/**
	 * Checks if is null.
	 *
	 * @param obj the obj
	 * @return true, if is null
	 */
	public static boolean isNull(Object obj){
		boolean b = true;
		if(obj != null){
			b = false;
		}
		return (b);
	}

	/**
	 * Gets the default value if null.
	 *
	 * @param value the value
	 * @param defaultValue the default value
	 * @return the default value if null
	 */
	public static Object getDefaultValueIfNull(Object value, Object defaultValue){
		Object result = defaultValue;
		if(value != null){
			result = value;
		}
		return(result);
	}

	/**
	 * Checks if is not empty.
	 *
	 * @param st the st
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(String st){
		boolean b = true;
		if(st ==null || st.trim().length()==0){
			b = false;
		}
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param ls the ls
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(List<?> ls ){
		boolean b = false;
		if(ls!=null && !ls.isEmpty()){
			b = true;
		}
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param map the map
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Map<?,?> map){
		boolean b = false;
		if(map!=null && !map.isEmpty()){
			b = true;
		}
		return(b);
	}

	/**
	 * Checks if is not empty.
	 *
	 * @param integer the integer
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Integer integer){
		boolean b = isNotNull(integer);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param byt the byt
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Byte byt){
		boolean b = isNotNull(byt);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param s the s
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Short s){
		boolean b = isNotNull(s);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param l the l
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Long l){
		boolean b = isNotNull(l);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param c the c
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Character c){
		boolean b = isNotNull(c);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param f the f
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Float f){
		boolean b = isNotNull(f);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param d the d
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Double d){
		boolean b = isNotNull(d);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param num the num
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Number num){
		boolean b = isNotNull(num);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param d the d
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(BigDecimal d){
		boolean b = isNotNull(d);
		return (b);

	}

	/**
	 * Checks if is not empty.
	 *
	 * @param obj the obj
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(Object obj){
		boolean b = false;
		if(obj != null){
			b = true;
		}
		return (b);
	}

	/**
	 * Checks if is not null.
	 *
	 * @param obj the obj
	 * @return true, if is not null
	 */
	public static boolean isNotNull(Object obj){
		boolean b = false;
		if(obj != null){
			b = true;
		}
		return (b);
	}
	
	
	/**
	 * Populate.
	 *
	 * @param bean the bean
	 * @param properties the properties
	 */
	@SuppressWarnings("rawtypes")
	public static void populate(Object bean, Map properties)
	{
    	try
    	{
        	org.apache.commons.beanutils.BeanUtils.populate(bean, properties);
    	}
    	catch (Exception e)
    	{
    		throw new IllegalStateException("Unable to populate properties [" + properties + "] to bean " + bean.getClass(), e);
    	}
	}
	
}
