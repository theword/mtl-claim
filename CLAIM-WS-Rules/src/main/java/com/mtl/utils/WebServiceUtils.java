/*
 * 
 */
package com.mtl.utils;

import org.apache.log4j.Logger;

import com.mtl.ws.bean.RuleDto;


/**
 * The Class WebServiceUtils.
 */
public class WebServiceUtils {
	
	/** The logger. */
	private static Logger logger = Logger.getLogger(WebServiceUtils.class);
	
	/**
	 * Prints the log header.
	 *
	 * @param header the header
	 */
	public static void printLogHeader(RuleDto header){
		logger.info("$");
		logger.info("$");
		logger.info("____________ HEADER ___________");
		logger.info(">>>> ServiceID :: '" + header.getServiceID() + "'");
		logger.info(">>>> CallName :: '" + header.getCallName()+ "'");
		logger.info(">>>> RequestDate :: '" + header.getRequestedDate()+ "'");
		logger.info(">>>> RespondDate :: '" + header.getRespondDate()+ "'");
		logger.info(">>>> Status :: '" + header.getStatus()+ "'");
		logger.info(">>>> Reason :: '" + header.getReason()+ "'");
		logger.info("$");
		logger.info("$");
	}
	
}
