/*
 * 
 */
package com.mtl.utils;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400ConnectionPool;
import com.ibm.as400.access.AS400Exception;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.ConnectionPoolException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import com.ibm.as400.access.QSYSObjectPathName;

/**
 * The Class CashValueGatherer.
 */
public class CashValueGatherer {
	
	/** The log. */
	Logger log = Logger.getLogger(this.getClass());
	
	/** The _as400 pool. */
	static AS400ConnectionPool _as400Pool = null;
	
	/** The constant system. */
	/* host name UAT mtldevl.muangthai.co.th */
	/* host name Production mtlife.muangthai.co.th */
	final String CONSTANT_SYSTEM = "mtlife.muangthai.co.th";
	
	/** The constant user. */
	/* user name UAT 		CAPZPBR1 */
	/* host name Production CAPZPBR1 */
	final String CONSTANT_USER = "CAPZPBR1";
	
	/** The constant password. */
	/* password UAT 		BRMS2013 */
	/* password Production 	brmscm56 */
	final String CONSTANT_PASSWORD = "brmscm56";
	
	/** The constant service. */
	final int CONSTANT_SERVICE = AS400.COMMAND;
	
	/** The constant max connections. */
	final int CONSTANT_MAX_CONNECTIONS = 3;

	/** The eng locale. */
	private final Locale ENG_LOCALE = new Locale("en", "EN");
	
	/**
	 * Instantiates a new cash value gatherer.
	 */
	public CashValueGatherer() {
		try {
			_as400Pool = new AS400ConnectionPool();
			_as400Pool.setMaxConnections(CONSTANT_MAX_CONNECTIONS);
			_as400Pool.fill(CONSTANT_SYSTEM, CONSTANT_USER, CONSTANT_PASSWORD,
					CONSTANT_SERVICE, CONSTANT_MAX_CONNECTIONS);
		} catch (Exception _exception) {
			_exception.printStackTrace(System.err);
		}
	}

	/**
	 * Gather.
	 *
	 * @param requestDate the request date
	 * @param policyNumber the policy number
	 * @return the double
	 */
	public double gather(Date requestDate, String policyNumber) {
		log.debug("start [CashValueGatherer.gather]");
		double cashValue = 0;
		AS400 _as400 = null;
		try {
			_as400 = _as400Pool.getConnection(CONSTANT_SYSTEM, CONSTANT_USER,
					CONSTANT_PASSWORD, CONSTANT_SERVICE);
			CommandCall _command = new CommandCall(_as400);
			_command
					.setCommand("CHGLIBL LIBL(CP3FSUP CP3SMDL  CP3FPRD CP3FPRD2  CP3SPRD CP3OFIX CP3OMDL  MTDTALIB MTPGMLIB MTCAPLIB QTEMP QGPL CP3OPRD MTINFLIB MTQRYLIB JDFOBJ91 JMTACOM CP3SFIX GRP36F MTNMMLIB)");
			if (!_command.run()) {
				AS400Message[] _messageList = _command.getMessageList();
				throw new AS400Exception(_messageList);
			}
			ProgramCall _program = new ProgramCall(_as400);
			/* UAT new QSYSObjectPathName("PGAEY","M9182BAT", "PGM");*/
			QSYSObjectPathName _programObject = new QSYSObjectPathName("CP3OMDL","M9182BAT", "PGM");
			String _programPath = _programObject.getPath();
			ProgramParameter[] _parameterList = new ProgramParameter[3];
			AS400Text policy = new AS400Text(10);
			AS400Text csv_date = new AS400Text(8);
			AS400Text csv_amt = new AS400Text(15);
			
			String date = covertDate(requestDate);
			if (date == null) {
				log.debug("Invalid Request Date");
				return 0;
			}
			log.debug("Input Date: " + date);
			log.debug("Input Policy Number: " + policyNumber);
			_parameterList[0] = new ProgramParameter(policy.toBytes(policyNumber));
			_parameterList[1] = new ProgramParameter(csv_date.toBytes(date));
			_parameterList[2] = new ProgramParameter(15);
			_program.setProgram(_programPath, _parameterList);
			if (!_program.run()) {
				AS400Message[] _messageList = _program.getMessageList();
				throw new AS400Exception(_messageList);
			}
			log.debug("Call Program to get Cash Value");
			cashValue = Double.parseDouble(csv_amt.toObject(_parameterList[2].getOutputData()).toString());
			log.debug("Retrieved Cash Value: " + csv_amt.toObject(_parameterList[2].getOutputData()).toString());
			log.debug("                    : " + cashValue);
		} catch (AS400Exception e) {
			log.error(e.getMessage());
			System.err.println("java -cp .;jt400.jar M54ClmBat");
			e.printStackTrace(System.err);
		} catch (PropertyVetoException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} catch (AS400SecurityException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} catch (ErrorCompletingRequestException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} catch (ObjectDoesNotExistException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} catch (ConnectionPoolException e) {
			log.error(e.getMessage());
			e.printStackTrace(System.err);
		} finally {
			if (_as400 != null) {
				_as400Pool.returnConnectionToPool(_as400);
			}
			if (_as400Pool != null) {
				_as400Pool.close();
			}
		}
		log.debug("end [CashValueGatherer.gather]");
		return cashValue;
	}

	/**
	 * Covert date.
	 *
	 * @param date the date
	 * @return the string
	 */
	private String covertDate(Date date) {
		log.debug("start [CashValueGatherer.covertDate]");
		log.debug("Date Input: "+date.toString());
		SimpleDateFormat sp = new SimpleDateFormat("yyyyMMdd" , ENG_LOCALE);
		try {
			log.debug("end [CashValueGatherer.covertDate]");
			String output = sp.format(date);
			log.debug(output);
			return output;
		} catch (Exception e) {
			log.error("Date format exception catched");
			log.error("end [CashValueGatherer.covertDate]");
			return null;
		}
		
	}
}
