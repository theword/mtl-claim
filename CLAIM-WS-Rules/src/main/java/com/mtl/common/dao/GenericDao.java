/*
 * 
 */
package com.mtl.common.dao;

import java.io.Serializable;
import java.util.List;

/**
 * The Interface GenericDao.
 *
 * @param <T> the generic type
 * @param <PK> the generic type
 */
public interface GenericDao<T, PK extends Serializable> {

	/**
	 * Gets the.
	 *
	 * @param id the id
	 * @return the t
	 */
	public abstract T get(PK id);
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public abstract List<T> getAll();
	
	/**
	 * Save.
	 *
	 * @param entity the entity
	 * @return the pk
	 */
	public abstract PK save(T entity);
	
	/**
	 * Delete.
	 *
	 * @param entity the entity
	 */
	public abstract void delete(T entity);
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public abstract void delete(PK id);
	
	/**
	 * Exists.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public abstract boolean exists(PK id);
	
	/**
	 * Delete.
	 *
	 * @param ids the ids
	 */
	public abstract void delete(PK[] ids);
	
	/**
	 * Save or update.
	 *
	 * @param entity the entity
	 */
	public abstract void saveOrUpdate(T entity);
	
}
