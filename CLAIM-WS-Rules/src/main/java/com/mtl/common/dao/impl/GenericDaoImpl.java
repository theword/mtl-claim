/*
 * 
 */
package com.mtl.common.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.mtl.common.dao.GenericDao;



/**
 * The Class GenericDaoImpl.
 *
 * @param <T> the generic type
 * @param <PK> the generic type
 */
public class GenericDaoImpl<T, PK extends Serializable> extends HibernateDaoSupport implements GenericDao<T, PK> {

	/** The log. */
	protected Logger log = Logger.getLogger(getClass());
	
	/** The persistent class. */
	protected Class<T> persistentClass;

	/**
	 * Instantiates a new generic dao impl.
	 */
	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	/**
	 * Creates the criteria.
	 *
	 * @return the criteria
	 */
	public Criteria createCriteria(){
		return this.getSession().createCriteria(persistentClass);
	}
	//*** for use getHibernateTemplate of Spring manage -> control 1 Session, 1 Connection ***
	/**
	 * Creates the detached criteria.
	 *
	 * @return the detached criteria
	 */
	public DetachedCriteria createDetachedCriteria(){
		return DetachedCriteria.forClass(persistentClass);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#get(java.io.Serializable)
	 */
	public T get(PK id) {
		T entity = (T) this.getHibernateTemplate().get(this.persistentClass, id);

		if (entity == null) {
			String msg = "Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...";
			log.warn(msg);
		}

		return entity;
	}

	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#getAll()
	 */
	public List<T> getAll() {

		return this.getHibernateTemplate().loadAll(persistentClass);
	}

	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#save(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public PK save(T entity) {
		return (PK)this.getHibernateTemplate().save(entity);
	}
	
	/**
	 * Update.
	 *
	 * @param entity the entity
	 */
	public void update(T entity) {
		this.getHibernateTemplate().update(entity);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#saveOrUpdate(java.lang.Object)
	 */
	public void saveOrUpdate(T entity){
		this.getHibernateTemplate().saveOrUpdate(entity);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#delete(java.lang.Object)
	 */
	public void delete(T entity) {
		this.getHibernateTemplate().delete(entity);
	}

	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#delete(java.io.Serializable)
	 */
	public void delete(PK id) {
		this.getHibernateTemplate().delete(this.get(id));
	}

	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#exists(java.io.Serializable)
	 */
	public boolean exists(PK id) {
		T entity = (T) this.get(id);

		if (entity == null) {
			return false;
		} else {
			return true;
		}
	}

	/* (non-Javadoc)
	 * @see com.mtl.common.dao.GenericDao#delete(PK[])
	 */
	public void delete(PK[] ids) {
		if (ids != null) {
			for (PK id : ids) {
				delete(id);
			}
		}
	}

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public List<T> findAll() {
		return find("from " + persistentClass.getName());
	}
	
	/**
	 * Find.
	 *
	 * @param hql the hql
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<T> find(String hql){
		return this.getSession().createQuery(hql).list();
	}
}