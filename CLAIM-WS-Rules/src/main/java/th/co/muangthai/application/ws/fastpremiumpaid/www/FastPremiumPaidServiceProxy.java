package th.co.muangthai.application.ws.fastpremiumpaid.www;

public class FastPremiumPaidServiceProxy implements th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType fastPremiumPaidService_PortType = null;
  
  public FastPremiumPaidServiceProxy() {
    _initFastPremiumPaidServiceProxy();
  }
  
  public FastPremiumPaidServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initFastPremiumPaidServiceProxy();
  }
  
  private void _initFastPremiumPaidServiceProxy() {
    try {
      fastPremiumPaidService_PortType = (new th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_ServiceLocator()).getFastPremiumPaidWsImplPort();
      if (fastPremiumPaidService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)fastPremiumPaidService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)fastPremiumPaidService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (fastPremiumPaidService_PortType != null)
      ((javax.xml.rpc.Stub)fastPremiumPaidService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType getFastPremiumPaidService_PortType() {
    if (fastPremiumPaidService_PortType == null)
      _initFastPremiumPaidServiceProxy();
    return fastPremiumPaidService_PortType;
  }
  
  public th.co.muangthai.application.ws.fastpremiumpaid.www.HeaderFastPremiumPaidResponse fastPremiumPaid(th.co.muangthai.application.ws.fastpremiumpaid.www.HeaderFastPremiumPaidRequest request) throws java.rmi.RemoteException{
	  if (fastPremiumPaidService_PortType == null)
      _initFastPremiumPaidServiceProxy();
    return fastPremiumPaidService_PortType.fastPremiumPaid(request);
  }
  
  
}