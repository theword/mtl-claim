/**
 * FastPremiumPaidService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.fastpremiumpaid.www;

public class FastPremiumPaidService_ServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_Service {

    public FastPremiumPaidService_ServiceLocator() {
    }


    public FastPremiumPaidService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FastPremiumPaidService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FastPremiumPaidWsImplPort
    private java.lang.String FastPremiumPaidWsImplPort_address = "http://MTOWOSBU1:8011/MTL-SOA/CLM/BRMSCLM/FastPremiumPaidService/ProxyService/FastPremiumPaidServicePS";

    public java.lang.String getFastPremiumPaidWsImplPortAddress() {
        return FastPremiumPaidWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FastPremiumPaidWsImplPortWSDDServiceName = "FastPremiumPaidWsImplPort";

    public java.lang.String getFastPremiumPaidWsImplPortWSDDServiceName() {
        return FastPremiumPaidWsImplPortWSDDServiceName;
    }

    public void setFastPremiumPaidWsImplPortWSDDServiceName(java.lang.String name) {
        FastPremiumPaidWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType getFastPremiumPaidWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FastPremiumPaidWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFastPremiumPaidWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType getFastPremiumPaidWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getFastPremiumPaidWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFastPremiumPaidWsImplPortEndpointAddress(java.lang.String address) {
        FastPremiumPaidWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.fastpremiumpaid.www.FastPremiumPaidWsImplPortBindingStub(new java.net.URL(FastPremiumPaidWsImplPort_address), this);
                _stub.setPortName(getFastPremiumPaidWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FastPremiumPaidWsImplPort".equals(inputPortName)) {
            return getFastPremiumPaidWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.fastpremiumpaid.ws.application.muangthai.co.th/", "FastPremiumPaidService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.fastpremiumpaid.ws.application.muangthai.co.th/", "FastPremiumPaidWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FastPremiumPaidWsImplPort".equals(portName)) {
            setFastPremiumPaidWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
