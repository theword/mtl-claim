/**
 * HeaderFastPremiumPaidRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.fastpremiumpaid.www;

public class HeaderFastPremiumPaidRequest  extends th.co.muangthai.application.ws.fastpremiumpaid.www.HeaderXml  implements java.io.Serializable {
    private java.lang.String incidentDate;

    private th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor[] policyFactorList;

    public HeaderFastPremiumPaidRequest() {
    }

    public HeaderFastPremiumPaidRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String incidentDate,
           th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor[] policyFactorList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.incidentDate = incidentDate;
        this.policyFactorList = policyFactorList;
    }


    /**
     * Gets the incidentDate value for this HeaderFastPremiumPaidRequest.
     * 
     * @return incidentDate
     */
    public java.lang.String getIncidentDate() {
        return incidentDate;
    }


    /**
     * Sets the incidentDate value for this HeaderFastPremiumPaidRequest.
     * 
     * @param incidentDate
     */
    public void setIncidentDate(java.lang.String incidentDate) {
        this.incidentDate = incidentDate;
    }


    /**
     * Gets the policyFactorList value for this HeaderFastPremiumPaidRequest.
     * 
     * @return policyFactorList
     */
    public th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor[] getPolicyFactorList() {
        return policyFactorList;
    }


    /**
     * Sets the policyFactorList value for this HeaderFastPremiumPaidRequest.
     * 
     * @param policyFactorList
     */
    public void setPolicyFactorList(th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor[] policyFactorList) {
        this.policyFactorList = policyFactorList;
    }

    public th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor getPolicyFactorList(int i) {
        return this.policyFactorList[i];
    }

    public void setPolicyFactorList(int i, th.co.muangthai.application.ws.fastpremiumpaid.www.PolicyFactor _value) {
        this.policyFactorList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderFastPremiumPaidRequest)) return false;
        HeaderFastPremiumPaidRequest other = (HeaderFastPremiumPaidRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.incidentDate==null && other.getIncidentDate()==null) || 
             (this.incidentDate!=null &&
              this.incidentDate.equals(other.getIncidentDate()))) &&
            ((this.policyFactorList==null && other.getPolicyFactorList()==null) || 
             (this.policyFactorList!=null &&
              java.util.Arrays.equals(this.policyFactorList, other.getPolicyFactorList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getIncidentDate() != null) {
            _hashCode += getIncidentDate().hashCode();
        }
        if (getPolicyFactorList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPolicyFactorList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPolicyFactorList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderFastPremiumPaidRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.fastpremiumpaid.ws.application.muangthai.co.th/", "headerFastPremiumPaidRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IncidentDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyFactorList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyFactorList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.fastpremiumpaid.ws.application.muangthai.co.th/", "policyFactor"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
