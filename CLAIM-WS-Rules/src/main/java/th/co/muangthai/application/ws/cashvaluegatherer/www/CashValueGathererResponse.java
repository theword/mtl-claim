/**
 * CashValueGathererResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.cashvaluegatherer.www;

public class CashValueGathererResponse  extends th.co.muangthai.application.ws.cashvaluegatherer.www.HeaderXml  implements java.io.Serializable {
    private double cashValue;

    private java.lang.String statusRespon;

    private java.lang.String messageRespon;

    public CashValueGathererResponse() {
    }

    public CashValueGathererResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           double cashValue,
           java.lang.String statusRespon,
           java.lang.String messageRespon) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.cashValue = cashValue;
        this.statusRespon = statusRespon;
        this.messageRespon = messageRespon;
    }


    /**
     * Gets the cashValue value for this CashValueGathererResponse.
     * 
     * @return cashValue
     */
    public double getCashValue() {
        return cashValue;
    }


    /**
     * Sets the cashValue value for this CashValueGathererResponse.
     * 
     * @param cashValue
     */
    public void setCashValue(double cashValue) {
        this.cashValue = cashValue;
    }


    /**
     * Gets the statusRespon value for this CashValueGathererResponse.
     * 
     * @return statusRespon
     */
    public java.lang.String getStatusRespon() {
        return statusRespon;
    }


    /**
     * Sets the statusRespon value for this CashValueGathererResponse.
     * 
     * @param statusRespon
     */
    public void setStatusRespon(java.lang.String statusRespon) {
        this.statusRespon = statusRespon;
    }


    /**
     * Gets the messageRespon value for this CashValueGathererResponse.
     * 
     * @return messageRespon
     */
    public java.lang.String getMessageRespon() {
        return messageRespon;
    }


    /**
     * Sets the messageRespon value for this CashValueGathererResponse.
     * 
     * @param messageRespon
     */
    public void setMessageRespon(java.lang.String messageRespon) {
        this.messageRespon = messageRespon;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CashValueGathererResponse)) return false;
        CashValueGathererResponse other = (CashValueGathererResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.cashValue == other.getCashValue() &&
            ((this.statusRespon==null && other.getStatusRespon()==null) || 
             (this.statusRespon!=null &&
              this.statusRespon.equals(other.getStatusRespon()))) &&
            ((this.messageRespon==null && other.getMessageRespon()==null) || 
             (this.messageRespon!=null &&
              this.messageRespon.equals(other.getMessageRespon())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Double(getCashValue()).hashCode();
        if (getStatusRespon() != null) {
            _hashCode += getStatusRespon().hashCode();
        }
        if (getMessageRespon() != null) {
            _hashCode += getMessageRespon().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CashValueGathererResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.cashvaluegatherer.ws.application.muangthai.co.th/", "cashValueGathererResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CashValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusRespon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatusRespon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageRespon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MessageRespon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
