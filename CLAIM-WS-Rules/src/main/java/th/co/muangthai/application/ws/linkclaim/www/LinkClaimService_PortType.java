/**
 * LinkClaimService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.linkclaim.www;

public interface LinkClaimService_PortType extends java.rmi.Remote {
    public th.co.muangthai.application.ws.linkclaim.www.HeaderLinkClaimResponse getLinkClaim(th.co.muangthai.application.ws.linkclaim.www.HeaderLinkClaimRequest request) throws java.rmi.RemoteException;
}
