/**
 * CashValueGathererService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.cashvaluegatherer.www;

public interface CashValueGathererService_PortType extends java.rmi.Remote {
    public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererResponse cashValueGatherer(th.co.muangthai.application.ws.cashvaluegatherer.www.HeaderCashValueGathererRequest request) throws java.rmi.RemoteException;
}
