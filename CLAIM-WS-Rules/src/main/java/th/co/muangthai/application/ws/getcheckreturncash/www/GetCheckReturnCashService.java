/**
 * GetCheckReturnCashService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcheckreturncash.www;

public interface GetCheckReturnCashService extends javax.xml.rpc.Service {
    public java.lang.String getGetCheckReturnCashWsImplPortAddress();

    public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash getGetCheckReturnCashWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash getGetCheckReturnCashWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
