/**
 * GetCheckReturnCashResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcheckreturncash.www;

public class GetCheckReturnCashResponse  extends th.co.muangthai.application.ws.getcheckreturncash.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash[] getResponseCheckReturnCashList;

    public GetCheckReturnCashResponse() {
    }

    public GetCheckReturnCashResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash[] getResponseCheckReturnCashList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.getResponseCheckReturnCashList = getResponseCheckReturnCashList;
    }


    /**
     * Gets the getResponseCheckReturnCashList value for this GetCheckReturnCashResponse.
     * 
     * @return getResponseCheckReturnCashList
     */
    public th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash[] getGetResponseCheckReturnCashList() {
        return getResponseCheckReturnCashList;
    }


    /**
     * Sets the getResponseCheckReturnCashList value for this GetCheckReturnCashResponse.
     * 
     * @param getResponseCheckReturnCashList
     */
    public void setGetResponseCheckReturnCashList(th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash[] getResponseCheckReturnCashList) {
        this.getResponseCheckReturnCashList = getResponseCheckReturnCashList;
    }

    public th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash getGetResponseCheckReturnCashList(int i) {
        return this.getResponseCheckReturnCashList[i];
    }

    public void setGetResponseCheckReturnCashList(int i, th.co.muangthai.application.ws.getcheckreturncash.www.GetResponseCheckReturnCash _value) {
        this.getResponseCheckReturnCashList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCheckReturnCashResponse)) return false;
        GetCheckReturnCashResponse other = (GetCheckReturnCashResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.getResponseCheckReturnCashList==null && other.getGetResponseCheckReturnCashList()==null) || 
             (this.getResponseCheckReturnCashList!=null &&
              java.util.Arrays.equals(this.getResponseCheckReturnCashList, other.getGetResponseCheckReturnCashList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGetResponseCheckReturnCashList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetResponseCheckReturnCashList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetResponseCheckReturnCashList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCheckReturnCashResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "getCheckReturnCashResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getResponseCheckReturnCashList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getResponseCheckReturnCashList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "getResponseCheckReturnCash"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
