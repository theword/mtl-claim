package th.co.muangthai.application.ws.getcheckreturncash.www;

public class GetCheckReturnCashProxy implements th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash getCheckReturnCash = null;
  
  public GetCheckReturnCashProxy() {
    _initGetCheckReturnCashProxy();
  }
  
  public GetCheckReturnCashProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetCheckReturnCashProxy();
  }
  
  private void _initGetCheckReturnCashProxy() {
    try {
      getCheckReturnCash = (new th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashServiceLocator()).getGetCheckReturnCashWsImplPort();
      if (getCheckReturnCash != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getCheckReturnCash)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getCheckReturnCash)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getCheckReturnCash != null)
      ((javax.xml.rpc.Stub)getCheckReturnCash)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash getGetCheckReturnCash() {
    if (getCheckReturnCash == null)
      _initGetCheckReturnCashProxy();
    return getCheckReturnCash;
  }
  
  public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashResponse getCheckReturnCash(th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashRequest request) throws java.rmi.RemoteException{
    if (getCheckReturnCash == null)
      _initGetCheckReturnCashProxy();
    return getCheckReturnCash.getCheckReturnCash(request);
  }
  
  
}