/**
 * CashValueGathererService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.cashvaluegatherer.www;

public interface CashValueGathererService_Service extends javax.xml.rpc.Service {
    public java.lang.String getCashValueGathererWsImplPortAddress();

    public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType getCashValueGathererWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType getCashValueGathererWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
