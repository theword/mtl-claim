/**
 * GetRequestCheckReturnCash.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcheckreturncash.www;

public class GetRequestCheckReturnCash  implements java.io.Serializable {
    private java.lang.String[] VH_POLICY;

    private java.lang.String VH_DISB_TYPE;

    public GetRequestCheckReturnCash() {
    }

    public GetRequestCheckReturnCash(
           java.lang.String[] VH_POLICY,
           java.lang.String VH_DISB_TYPE) {
           this.VH_POLICY = VH_POLICY;
           this.VH_DISB_TYPE = VH_DISB_TYPE;
    }


    /**
     * Gets the VH_POLICY value for this GetRequestCheckReturnCash.
     * 
     * @return VH_POLICY
     */
    public java.lang.String[] getVH_POLICY() {
        return VH_POLICY;
    }


    /**
     * Sets the VH_POLICY value for this GetRequestCheckReturnCash.
     * 
     * @param VH_POLICY
     */
    public void setVH_POLICY(java.lang.String[] VH_POLICY) {
        this.VH_POLICY = VH_POLICY;
    }

    public java.lang.String getVH_POLICY(int i) {
        return this.VH_POLICY[i];
    }

    public void setVH_POLICY(int i, java.lang.String _value) {
        this.VH_POLICY[i] = _value;
    }


    /**
     * Gets the VH_DISB_TYPE value for this GetRequestCheckReturnCash.
     * 
     * @return VH_DISB_TYPE
     */
    public java.lang.String getVH_DISB_TYPE() {
        return VH_DISB_TYPE;
    }


    /**
     * Sets the VH_DISB_TYPE value for this GetRequestCheckReturnCash.
     * 
     * @param VH_DISB_TYPE
     */
    public void setVH_DISB_TYPE(java.lang.String VH_DISB_TYPE) {
        this.VH_DISB_TYPE = VH_DISB_TYPE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRequestCheckReturnCash)) return false;
        GetRequestCheckReturnCash other = (GetRequestCheckReturnCash) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.VH_POLICY==null && other.getVH_POLICY()==null) || 
             (this.VH_POLICY!=null &&
              java.util.Arrays.equals(this.VH_POLICY, other.getVH_POLICY()))) &&
            ((this.VH_DISB_TYPE==null && other.getVH_DISB_TYPE()==null) || 
             (this.VH_DISB_TYPE!=null &&
              this.VH_DISB_TYPE.equals(other.getVH_DISB_TYPE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVH_POLICY() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVH_POLICY());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVH_POLICY(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getVH_DISB_TYPE() != null) {
            _hashCode += getVH_DISB_TYPE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRequestCheckReturnCash.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "getRequestCheckReturnCash"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VH_POLICY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VH_POLICY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("VH_DISB_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VH_DISB_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
