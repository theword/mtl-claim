/**
 * HeaderLinkClaimRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.linkclaim.www;

public class HeaderLinkClaimRequest  extends th.co.muangthai.application.ws.linkclaim.www.HeaderXml  implements java.io.Serializable {
    private java.lang.String clientNumber;

    private java.lang.String[] diagnosisCodeList;

    private java.lang.String[] referenceClaimList;

    public HeaderLinkClaimRequest() {
    }

    public HeaderLinkClaimRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String clientNumber,
           java.lang.String[] diagnosisCodeList,
           java.lang.String[] referenceClaimList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.clientNumber = clientNumber;
        this.diagnosisCodeList = diagnosisCodeList;
        this.referenceClaimList = referenceClaimList;
    }


    /**
     * Gets the clientNumber value for this HeaderLinkClaimRequest.
     * 
     * @return clientNumber
     */
    public java.lang.String getClientNumber() {
        return clientNumber;
    }


    /**
     * Sets the clientNumber value for this HeaderLinkClaimRequest.
     * 
     * @param clientNumber
     */
    public void setClientNumber(java.lang.String clientNumber) {
        this.clientNumber = clientNumber;
    }


    /**
     * Gets the diagnosisCodeList value for this HeaderLinkClaimRequest.
     * 
     * @return diagnosisCodeList
     */
    public java.lang.String[] getDiagnosisCodeList() {
        return diagnosisCodeList;
    }


    /**
     * Sets the diagnosisCodeList value for this HeaderLinkClaimRequest.
     * 
     * @param diagnosisCodeList
     */
    public void setDiagnosisCodeList(java.lang.String[] diagnosisCodeList) {
        this.diagnosisCodeList = diagnosisCodeList;
    }

    public java.lang.String getDiagnosisCodeList(int i) {
        return this.diagnosisCodeList[i];
    }

    public void setDiagnosisCodeList(int i, java.lang.String _value) {
        this.diagnosisCodeList[i] = _value;
    }


    /**
     * Gets the referenceClaimList value for this HeaderLinkClaimRequest.
     * 
     * @return referenceClaimList
     */
    public java.lang.String[] getReferenceClaimList() {
        return referenceClaimList;
    }


    /**
     * Sets the referenceClaimList value for this HeaderLinkClaimRequest.
     * 
     * @param referenceClaimList
     */
    public void setReferenceClaimList(java.lang.String[] referenceClaimList) {
        this.referenceClaimList = referenceClaimList;
    }

    public java.lang.String getReferenceClaimList(int i) {
        return this.referenceClaimList[i];
    }

    public void setReferenceClaimList(int i, java.lang.String _value) {
        this.referenceClaimList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderLinkClaimRequest)) return false;
        HeaderLinkClaimRequest other = (HeaderLinkClaimRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.clientNumber==null && other.getClientNumber()==null) || 
             (this.clientNumber!=null &&
              this.clientNumber.equals(other.getClientNumber()))) &&
            ((this.diagnosisCodeList==null && other.getDiagnosisCodeList()==null) || 
             (this.diagnosisCodeList!=null &&
              java.util.Arrays.equals(this.diagnosisCodeList, other.getDiagnosisCodeList()))) &&
            ((this.referenceClaimList==null && other.getReferenceClaimList()==null) || 
             (this.referenceClaimList!=null &&
              java.util.Arrays.equals(this.referenceClaimList, other.getReferenceClaimList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClientNumber() != null) {
            _hashCode += getClientNumber().hashCode();
        }
        if (getDiagnosisCodeList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDiagnosisCodeList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDiagnosisCodeList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReferenceClaimList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferenceClaimList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferenceClaimList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderLinkClaimRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.linkclaim.ws.application.muangthai.co.th/", "headerLinkClaimRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diagnosisCodeList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diagnosisCodeList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceClaimList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "referenceClaimList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
