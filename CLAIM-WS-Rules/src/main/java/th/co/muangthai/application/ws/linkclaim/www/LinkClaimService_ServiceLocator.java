/**
 * LinkClaimService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.linkclaim.www;

public class LinkClaimService_ServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_Service {

    public LinkClaimService_ServiceLocator() {
    }


    public LinkClaimService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LinkClaimService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LinkClaimWsImplPort
    private java.lang.String LinkClaimWsImplPort_address = "http://MTOWOSBU1:8011/MTL-SOA/CLM/BRMSCLM/LinkClaimService/ProxyService/LinkClaimServicePS";

    public java.lang.String getLinkClaimWsImplPortAddress() {
        return LinkClaimWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LinkClaimWsImplPortWSDDServiceName = "LinkClaimWsImplPort";

    public java.lang.String getLinkClaimWsImplPortWSDDServiceName() {
        return LinkClaimWsImplPortWSDDServiceName;
    }

    public void setLinkClaimWsImplPortWSDDServiceName(java.lang.String name) {
        LinkClaimWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType getLinkClaimWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LinkClaimWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLinkClaimWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType getLinkClaimWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.linkclaim.www.LinkClaimWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.linkclaim.www.LinkClaimWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getLinkClaimWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLinkClaimWsImplPortEndpointAddress(java.lang.String address) {
        LinkClaimWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.linkclaim.www.LinkClaimWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.linkclaim.www.LinkClaimWsImplPortBindingStub(new java.net.URL(LinkClaimWsImplPort_address), this);
                _stub.setPortName(getLinkClaimWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LinkClaimWsImplPort".equals(inputPortName)) {
            return getLinkClaimWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.linkclaim.ws.application.muangthai.co.th/", "LinkClaimService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.linkclaim.ws.application.muangthai.co.th/", "LinkClaimWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LinkClaimWsImplPort".equals(portName)) {
            setLinkClaimWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
