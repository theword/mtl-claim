/**
 * FastPremiumPaidResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.fastpremiumpaid.www;

public class FastPremiumPaidResponse  implements java.io.Serializable {
    private java.lang.String policyNumber;

    private java.lang.String orgPaidDateBefore;

    private java.lang.String orgPaidDateAfter;

    private java.lang.String paymentDateBefore;

    private java.lang.String paymentDateAfter;

    private java.lang.String expireDate;

    private int paymentMode;

    private java.lang.Double sixMonthAnnualFactor;

    private java.lang.Double quarterlyFactor;

    private java.lang.Double monthlyFactor;

    public FastPremiumPaidResponse() {
    }

    public FastPremiumPaidResponse(
           java.lang.String policyNumber,
           java.lang.String orgPaidDateBefore,
           java.lang.String orgPaidDateAfter,
           java.lang.String paymentDateBefore,
           java.lang.String paymentDateAfter,
           java.lang.String expireDate,
           int paymentMode,
           java.lang.Double sixMonthAnnualFactor,
           java.lang.Double quarterlyFactor,
           java.lang.Double monthlyFactor) {
           this.policyNumber = policyNumber;
           this.orgPaidDateBefore = orgPaidDateBefore;
           this.orgPaidDateAfter = orgPaidDateAfter;
           this.paymentDateBefore = paymentDateBefore;
           this.paymentDateAfter = paymentDateAfter;
           this.expireDate = expireDate;
           this.paymentMode = paymentMode;
           this.sixMonthAnnualFactor = sixMonthAnnualFactor;
           this.quarterlyFactor = quarterlyFactor;
           this.monthlyFactor = monthlyFactor;
    }


    /**
     * Gets the policyNumber value for this FastPremiumPaidResponse.
     * 
     * @return policyNumber
     */
    public java.lang.String getPolicyNumber() {
        return policyNumber;
    }


    /**
     * Sets the policyNumber value for this FastPremiumPaidResponse.
     * 
     * @param policyNumber
     */
    public void setPolicyNumber(java.lang.String policyNumber) {
        this.policyNumber = policyNumber;
    }


    /**
     * Gets the orgPaidDateBefore value for this FastPremiumPaidResponse.
     * 
     * @return orgPaidDateBefore
     */
    public java.lang.String getOrgPaidDateBefore() {
        return orgPaidDateBefore;
    }


    /**
     * Sets the orgPaidDateBefore value for this FastPremiumPaidResponse.
     * 
     * @param orgPaidDateBefore
     */
    public void setOrgPaidDateBefore(java.lang.String orgPaidDateBefore) {
        this.orgPaidDateBefore = orgPaidDateBefore;
    }


    /**
     * Gets the orgPaidDateAfter value for this FastPremiumPaidResponse.
     * 
     * @return orgPaidDateAfter
     */
    public java.lang.String getOrgPaidDateAfter() {
        return orgPaidDateAfter;
    }


    /**
     * Sets the orgPaidDateAfter value for this FastPremiumPaidResponse.
     * 
     * @param orgPaidDateAfter
     */
    public void setOrgPaidDateAfter(java.lang.String orgPaidDateAfter) {
        this.orgPaidDateAfter = orgPaidDateAfter;
    }


    /**
     * Gets the paymentDateBefore value for this FastPremiumPaidResponse.
     * 
     * @return paymentDateBefore
     */
    public java.lang.String getPaymentDateBefore() {
        return paymentDateBefore;
    }


    /**
     * Sets the paymentDateBefore value for this FastPremiumPaidResponse.
     * 
     * @param paymentDateBefore
     */
    public void setPaymentDateBefore(java.lang.String paymentDateBefore) {
        this.paymentDateBefore = paymentDateBefore;
    }


    /**
     * Gets the paymentDateAfter value for this FastPremiumPaidResponse.
     * 
     * @return paymentDateAfter
     */
    public java.lang.String getPaymentDateAfter() {
        return paymentDateAfter;
    }


    /**
     * Sets the paymentDateAfter value for this FastPremiumPaidResponse.
     * 
     * @param paymentDateAfter
     */
    public void setPaymentDateAfter(java.lang.String paymentDateAfter) {
        this.paymentDateAfter = paymentDateAfter;
    }


    /**
     * Gets the expireDate value for this FastPremiumPaidResponse.
     * 
     * @return expireDate
     */
    public java.lang.String getExpireDate() {
        return expireDate;
    }


    /**
     * Sets the expireDate value for this FastPremiumPaidResponse.
     * 
     * @param expireDate
     */
    public void setExpireDate(java.lang.String expireDate) {
        this.expireDate = expireDate;
    }


    /**
     * Gets the paymentMode value for this FastPremiumPaidResponse.
     * 
     * @return paymentMode
     */
    public int getPaymentMode() {
        return paymentMode;
    }


    /**
     * Sets the paymentMode value for this FastPremiumPaidResponse.
     * 
     * @param paymentMode
     */
    public void setPaymentMode(int paymentMode) {
        this.paymentMode = paymentMode;
    }


    /**
     * Gets the sixMonthAnnualFactor value for this FastPremiumPaidResponse.
     * 
     * @return sixMonthAnnualFactor
     */
    public java.lang.Double getSixMonthAnnualFactor() {
        return sixMonthAnnualFactor;
    }


    /**
     * Sets the sixMonthAnnualFactor value for this FastPremiumPaidResponse.
     * 
     * @param sixMonthAnnualFactor
     */
    public void setSixMonthAnnualFactor(java.lang.Double sixMonthAnnualFactor) {
        this.sixMonthAnnualFactor = sixMonthAnnualFactor;
    }


    /**
     * Gets the quarterlyFactor value for this FastPremiumPaidResponse.
     * 
     * @return quarterlyFactor
     */
    public java.lang.Double getQuarterlyFactor() {
        return quarterlyFactor;
    }


    /**
     * Sets the quarterlyFactor value for this FastPremiumPaidResponse.
     * 
     * @param quarterlyFactor
     */
    public void setQuarterlyFactor(java.lang.Double quarterlyFactor) {
        this.quarterlyFactor = quarterlyFactor;
    }


    /**
     * Gets the monthlyFactor value for this FastPremiumPaidResponse.
     * 
     * @return monthlyFactor
     */
    public java.lang.Double getMonthlyFactor() {
        return monthlyFactor;
    }


    /**
     * Sets the monthlyFactor value for this FastPremiumPaidResponse.
     * 
     * @param monthlyFactor
     */
    public void setMonthlyFactor(java.lang.Double monthlyFactor) {
        this.monthlyFactor = monthlyFactor;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FastPremiumPaidResponse)) return false;
        FastPremiumPaidResponse other = (FastPremiumPaidResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.policyNumber==null && other.getPolicyNumber()==null) || 
             (this.policyNumber!=null &&
              this.policyNumber.equals(other.getPolicyNumber()))) &&
            ((this.orgPaidDateBefore==null && other.getOrgPaidDateBefore()==null) || 
             (this.orgPaidDateBefore!=null &&
              this.orgPaidDateBefore.equals(other.getOrgPaidDateBefore()))) &&
            ((this.orgPaidDateAfter==null && other.getOrgPaidDateAfter()==null) || 
             (this.orgPaidDateAfter!=null &&
              this.orgPaidDateAfter.equals(other.getOrgPaidDateAfter()))) &&
            ((this.paymentDateBefore==null && other.getPaymentDateBefore()==null) || 
             (this.paymentDateBefore!=null &&
              this.paymentDateBefore.equals(other.getPaymentDateBefore()))) &&
            ((this.paymentDateAfter==null && other.getPaymentDateAfter()==null) || 
             (this.paymentDateAfter!=null &&
              this.paymentDateAfter.equals(other.getPaymentDateAfter()))) &&
            ((this.expireDate==null && other.getExpireDate()==null) || 
             (this.expireDate!=null &&
              this.expireDate.equals(other.getExpireDate()))) &&
            this.paymentMode == other.getPaymentMode() &&
            ((this.sixMonthAnnualFactor==null && other.getSixMonthAnnualFactor()==null) || 
             (this.sixMonthAnnualFactor!=null &&
              this.sixMonthAnnualFactor.equals(other.getSixMonthAnnualFactor()))) &&
            ((this.quarterlyFactor==null && other.getQuarterlyFactor()==null) || 
             (this.quarterlyFactor!=null &&
              this.quarterlyFactor.equals(other.getQuarterlyFactor()))) &&
            ((this.monthlyFactor==null && other.getMonthlyFactor()==null) || 
             (this.monthlyFactor!=null &&
              this.monthlyFactor.equals(other.getMonthlyFactor())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPolicyNumber() != null) {
            _hashCode += getPolicyNumber().hashCode();
        }
        if (getOrgPaidDateBefore() != null) {
            _hashCode += getOrgPaidDateBefore().hashCode();
        }
        if (getOrgPaidDateAfter() != null) {
            _hashCode += getOrgPaidDateAfter().hashCode();
        }
        if (getPaymentDateBefore() != null) {
            _hashCode += getPaymentDateBefore().hashCode();
        }
        if (getPaymentDateAfter() != null) {
            _hashCode += getPaymentDateAfter().hashCode();
        }
        if (getExpireDate() != null) {
            _hashCode += getExpireDate().hashCode();
        }
        _hashCode += getPaymentMode();
        if (getSixMonthAnnualFactor() != null) {
            _hashCode += getSixMonthAnnualFactor().hashCode();
        }
        if (getQuarterlyFactor() != null) {
            _hashCode += getQuarterlyFactor().hashCode();
        }
        if (getMonthlyFactor() != null) {
            _hashCode += getMonthlyFactor().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FastPremiumPaidResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.fastpremiumpaid.ws.application.muangthai.co.th/", "fastPremiumPaidResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgPaidDateBefore");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OrgPaidDateBefore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgPaidDateAfter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OrgPaidDateAfter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentDateBefore");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PaymentDateBefore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentDateAfter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PaymentDateAfter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expireDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ExpireDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PaymentMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sixMonthAnnualFactor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SixMonthAnnualFactor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quarterlyFactor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "QuarterlyFactor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monthlyFactor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MonthlyFactor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
