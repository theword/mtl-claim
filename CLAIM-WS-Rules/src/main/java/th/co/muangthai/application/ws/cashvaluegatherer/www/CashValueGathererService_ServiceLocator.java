/**
 * CashValueGathererService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.cashvaluegatherer.www;

public class CashValueGathererService_ServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_Service {

    public CashValueGathererService_ServiceLocator() {
    }


    public CashValueGathererService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CashValueGathererService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CashValueGathererWsImplPort
    private java.lang.String CashValueGathererWsImplPort_address = "http://mtowosbp2.muangthai.co.th:8011/MTL-SOA/CLM/BRMSCLM/CashValueGathererService/ProxyService/CashValueGathererServicePS";

    public java.lang.String getCashValueGathererWsImplPortAddress() {
        return CashValueGathererWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CashValueGathererWsImplPortWSDDServiceName = "CashValueGathererWsImplPort";

    public java.lang.String getCashValueGathererWsImplPortWSDDServiceName() {
        return CashValueGathererWsImplPortWSDDServiceName;
    }

    public void setCashValueGathererWsImplPortWSDDServiceName(java.lang.String name) {
        CashValueGathererWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType getCashValueGathererWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CashValueGathererWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCashValueGathererWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType getCashValueGathererWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getCashValueGathererWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCashValueGathererWsImplPortEndpointAddress(java.lang.String address) {
        CashValueGathererWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererWsImplPortBindingStub(new java.net.URL(CashValueGathererWsImplPort_address), this);
                _stub.setPortName(getCashValueGathererWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CashValueGathererWsImplPort".equals(inputPortName)) {
            return getCashValueGathererWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.cashvaluegatherer.ws.application.muangthai.co.th/", "CashValueGathererService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.cashvaluegatherer.ws.application.muangthai.co.th/", "CashValueGathererWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CashValueGathererWsImplPort".equals(portName)) {
            setCashValueGathererWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
