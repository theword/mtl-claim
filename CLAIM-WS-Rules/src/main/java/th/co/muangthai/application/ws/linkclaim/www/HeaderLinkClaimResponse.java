/**
 * HeaderLinkClaimResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.linkclaim.www;

public class HeaderLinkClaimResponse  extends th.co.muangthai.application.ws.linkclaim.www.HeaderXml  implements java.io.Serializable {
    private java.lang.String[] linkClaimList;

    public HeaderLinkClaimResponse() {
    }

    public HeaderLinkClaimResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String[] linkClaimList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.linkClaimList = linkClaimList;
    }


    /**
     * Gets the linkClaimList value for this HeaderLinkClaimResponse.
     * 
     * @return linkClaimList
     */
    public java.lang.String[] getLinkClaimList() {
        return linkClaimList;
    }


    /**
     * Sets the linkClaimList value for this HeaderLinkClaimResponse.
     * 
     * @param linkClaimList
     */
    public void setLinkClaimList(java.lang.String[] linkClaimList) {
        this.linkClaimList = linkClaimList;
    }

    public java.lang.String getLinkClaimList(int i) {
        return this.linkClaimList[i];
    }

    public void setLinkClaimList(int i, java.lang.String _value) {
        this.linkClaimList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderLinkClaimResponse)) return false;
        HeaderLinkClaimResponse other = (HeaderLinkClaimResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.linkClaimList==null && other.getLinkClaimList()==null) || 
             (this.linkClaimList!=null &&
              java.util.Arrays.equals(this.linkClaimList, other.getLinkClaimList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLinkClaimList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLinkClaimList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLinkClaimList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderLinkClaimResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.linkclaim.ws.application.muangthai.co.th/", "headerLinkClaimResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("linkClaimList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "linkClaimList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
