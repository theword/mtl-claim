/**
 * GetCheckReturnCash.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcheckreturncash.www;

public interface GetCheckReturnCash extends java.rmi.Remote {
    public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashResponse getCheckReturnCash(th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashRequest request) throws java.rmi.RemoteException;
}
