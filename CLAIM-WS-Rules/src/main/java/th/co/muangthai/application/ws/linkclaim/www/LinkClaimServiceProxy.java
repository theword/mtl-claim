package th.co.muangthai.application.ws.linkclaim.www;

public class LinkClaimServiceProxy implements th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType linkClaimService_PortType = null;
  
  public LinkClaimServiceProxy() {
    _initLinkClaimServiceProxy();
  }
  
  public LinkClaimServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initLinkClaimServiceProxy();
  }
  
  private void _initLinkClaimServiceProxy() {
    try {
      linkClaimService_PortType = (new th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_ServiceLocator()).getLinkClaimWsImplPort();
      if (linkClaimService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)linkClaimService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)linkClaimService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (linkClaimService_PortType != null)
      ((javax.xml.rpc.Stub)linkClaimService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.linkclaim.www.LinkClaimService_PortType getLinkClaimService_PortType() {
    if (linkClaimService_PortType == null)
      _initLinkClaimServiceProxy();
    return linkClaimService_PortType;
  }
  
  public th.co.muangthai.application.ws.linkclaim.www.HeaderLinkClaimResponse getLinkClaim(th.co.muangthai.application.ws.linkclaim.www.HeaderLinkClaimRequest request) throws java.rmi.RemoteException{
    if (linkClaimService_PortType == null)
      _initLinkClaimServiceProxy();
    return linkClaimService_PortType.getLinkClaim(request);
  }
  
  
}