/**
 * GetCheckReturnCashServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcheckreturncash.www;

public class GetCheckReturnCashServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashService {

    public GetCheckReturnCashServiceLocator() {
    }


    public GetCheckReturnCashServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetCheckReturnCashServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetCheckReturnCashWsImplPort
    private java.lang.String GetCheckReturnCashWsImplPort_address = "http://uat-lof4clm.muangthai.co.th:9103/GetCheckReturnCash/GetCheckReturnCashService";

    public java.lang.String getGetCheckReturnCashWsImplPortAddress() {
        return GetCheckReturnCashWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetCheckReturnCashWsImplPortWSDDServiceName = "GetCheckReturnCashWsImplPort";

    public java.lang.String getGetCheckReturnCashWsImplPortWSDDServiceName() {
        return GetCheckReturnCashWsImplPortWSDDServiceName;
    }

    public void setGetCheckReturnCashWsImplPortWSDDServiceName(java.lang.String name) {
        GetCheckReturnCashWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash getGetCheckReturnCashWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetCheckReturnCashWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetCheckReturnCashWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash getGetCheckReturnCashWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getGetCheckReturnCashWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetCheckReturnCashWsImplPortEndpointAddress(java.lang.String address) {
        GetCheckReturnCashWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCash.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getcheckreturncash.www.GetCheckReturnCashWsImplPortBindingStub(new java.net.URL(GetCheckReturnCashWsImplPort_address), this);
                _stub.setPortName(getGetCheckReturnCashWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetCheckReturnCashWsImplPort".equals(inputPortName)) {
            return getGetCheckReturnCashWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "GetCheckReturnCashService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "GetCheckReturnCashWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetCheckReturnCashWsImplPort".equals(portName)) {
            setGetCheckReturnCashWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
