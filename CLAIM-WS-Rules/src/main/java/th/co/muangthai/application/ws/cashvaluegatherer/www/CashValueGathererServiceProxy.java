package th.co.muangthai.application.ws.cashvaluegatherer.www;

public class CashValueGathererServiceProxy implements th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType cashValueGathererService_PortType = null;
  
  public CashValueGathererServiceProxy() {
    _initCashValueGathererServiceProxy();
  }
  
  public CashValueGathererServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initCashValueGathererServiceProxy();
  }
  
  private void _initCashValueGathererServiceProxy() {
    try {
      cashValueGathererService_PortType = (new th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_ServiceLocator()).getCashValueGathererWsImplPort();
      if (cashValueGathererService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)cashValueGathererService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)cashValueGathererService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (cashValueGathererService_PortType != null)
      ((javax.xml.rpc.Stub)cashValueGathererService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererService_PortType getCashValueGathererService_PortType() {
    if (cashValueGathererService_PortType == null)
      _initCashValueGathererServiceProxy();
    return cashValueGathererService_PortType;
  }
  
  public th.co.muangthai.application.ws.cashvaluegatherer.www.CashValueGathererResponse cashValueGatherer(th.co.muangthai.application.ws.cashvaluegatherer.www.HeaderCashValueGathererRequest request) throws java.rmi.RemoteException{
    if (cashValueGathererService_PortType == null)
      _initCashValueGathererServiceProxy();
    return cashValueGathererService_PortType.cashValueGatherer(request);
  }
  
  
}