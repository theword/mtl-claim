/**
 * GetCheckReturnCashRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcheckreturncash.www;

public class GetCheckReturnCashRequest  extends th.co.muangthai.application.ws.getcheckreturncash.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getcheckreturncash.www.GetRequestCheckReturnCash getRequestCheckReturnCash;

    public GetCheckReturnCashRequest() {
    }

    public GetCheckReturnCashRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getcheckreturncash.www.GetRequestCheckReturnCash getRequestCheckReturnCash) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.getRequestCheckReturnCash = getRequestCheckReturnCash;
    }


    /**
     * Gets the getRequestCheckReturnCash value for this GetCheckReturnCashRequest.
     * 
     * @return getRequestCheckReturnCash
     */
    public th.co.muangthai.application.ws.getcheckreturncash.www.GetRequestCheckReturnCash getGetRequestCheckReturnCash() {
        return getRequestCheckReturnCash;
    }


    /**
     * Sets the getRequestCheckReturnCash value for this GetCheckReturnCashRequest.
     * 
     * @param getRequestCheckReturnCash
     */
    public void setGetRequestCheckReturnCash(th.co.muangthai.application.ws.getcheckreturncash.www.GetRequestCheckReturnCash getRequestCheckReturnCash) {
        this.getRequestCheckReturnCash = getRequestCheckReturnCash;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCheckReturnCashRequest)) return false;
        GetCheckReturnCashRequest other = (GetCheckReturnCashRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.getRequestCheckReturnCash==null && other.getGetRequestCheckReturnCash()==null) || 
             (this.getRequestCheckReturnCash!=null &&
              this.getRequestCheckReturnCash.equals(other.getGetRequestCheckReturnCash())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGetRequestCheckReturnCash() != null) {
            _hashCode += getGetRequestCheckReturnCash().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCheckReturnCashRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "getCheckReturnCashRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getRequestCheckReturnCash");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getRequestCheckReturnCash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getcheckreturncash.ws.application.muangthai.co.th/", "getRequestCheckReturnCash"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
