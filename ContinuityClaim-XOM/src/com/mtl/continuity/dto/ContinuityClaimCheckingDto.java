/*
 * 
 */
package com.mtl.continuity.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.continuity.model.enumerator.ClaimType;
import com.mtl.continuity.model.mtlclaim.Diagnosis;
import com.mtl.continuity.util.DateTimeAdapter;

/**
 * The Class ContinuityClaimCheckingDto.
 */
@XmlRootElement(name = "ContinuityClaimCheckingDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContinuityClaimCheckingDto {
	
	/** The client id. */
	@XmlElement(name = "clientID", required = false)
	private String clientID; 
	
	/** The request diagnoses. */
	@XmlElement(name = "requestDiagnoses", required = false)
	private List<Diagnosis> requestDiagnoses; 
	
	/** The claim type. */
	@XmlElement(name = "claimType", required = false)
	private ClaimType claimType; 
	
	/** The admission date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "admissionDate", required = false)
	private Date admissionDate; 
	
	/** The incident date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "incidentDate", required = false)
	private Date incidentDate;
	
	/** The discharge date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "dischargeDate", required = false)
	private Date dischargeDate;
	
	/**
	 * Instantiates a new continuity claim checking dto.
	 */
	public ContinuityClaimCheckingDto(){
		
	}
	
	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public String getClientID() {
		return clientID;
	}
	
	/**
	 * Sets the client id.
	 *
	 * @param clientID the new client id
	 */
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	/**
	 * Gets the request diagnoses.
	 *
	 * @return the request diagnoses
	 */
	public List<Diagnosis> getRequestDiagnoses() {
		return requestDiagnoses;
	}
	
	/**
	 * Sets the request diagnoses.
	 *
	 * @param requestDiagnoses the new request diagnoses
	 */
	public void setRequestDiagnoses(List<Diagnosis> requestDiagnoses) {
		this.requestDiagnoses = requestDiagnoses;
	}
	
	/**
	 * Gets the claim type.
	 *
	 * @return the claim type
	 */
	public ClaimType getClaimType() {
		return claimType;
	}
	
	/**
	 * Sets the claim type.
	 *
	 * @param claimType the new claim type
	 */
	public void setClaimType(ClaimType claimType) {
		this.claimType = claimType;
	}
	
	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate() {
		return admissionDate;
	}
	
	/**
	 * Sets the admission date.
	 *
	 * @param admissionDate the new admission date
	 */
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}
	
	/**
	 * Gets the incident date.
	 *
	 * @return the incident date
	 */
	public Date getIncidentDate() {
		return incidentDate;
	}
	
	/**
	 * Sets the incident date.
	 *
	 * @param incidentDate the new incident date
	 */
	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}
	
	/**
	 * Gets the discharge date.
	 *
	 * @return the discharge date
	 */
	public Date getDischargeDate() {
		return dischargeDate;
	}
	
	/**
	 * Sets the discharge date.
	 *
	 * @param dischargeDate the new discharge date
	 */
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	
}
