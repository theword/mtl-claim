/*
 * 
 */
package com.mtl.continuity.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The Class DateTimeAdapter.
 */
public class DateTimeAdapter extends XmlAdapter<String, Date> {

	/** The format. */
	private SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a" , new Locale("en", "EN"));
	
	/* (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public String marshal(Date date) throws Exception {
		return format.format(date);
	}

	/* (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Date unmarshal(String dateStr) throws Exception {
		return format.parse(dateStr);
	}

}
