/*
 * 
 */
package com.mtl.continuity.model.mtlclaim;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.continuity.model.enumerator.BenefitMode;
import com.mtl.continuity.model.enumerator.CalculateOption;
import com.mtl.continuity.model.enumerator.LinkMode;
import com.mtl.continuity.model.mtlclaim.master.BenefitMs;
import com.mtl.continuity.model.templates.IMasterModel;
import com.mtl.continuity.model.templates.ITransactionModel;


/**
 * The Class Benefit.
 */
@XmlRootElement(name = "Benefit")
@XmlAccessorType(XmlAccessType.FIELD)
public class Benefit implements ITransactionModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -129158006345184531L;

	/** The benefit. */
	@XmlElement(name = "benefit", required = false)
	private BenefitMs benefit;
	
	/** The available amount. */
	@XmlElement(name = "availableAmount", required = false)
	private double availableAmount;
	
	@XmlElement(name = "maxAvailableAmount", required = false)
	private double maxAvailableAmount;
	
	/** The available days. */
	@XmlElement(name = "availableDays", required = false)
	private int availableDays;
	
	/** The available percent. */
	@XmlElement(name = "availablePercent", required = false)
	private double availablePercent;
	
	/** The available days intial. */
	@XmlElement(name = "availableDaysIntial", required = false)
	private int availableDaysIntial;
	
	/** The billed days for excess. */
	@XmlElement(name = "billedDaysForExcess", required = false)
	private List<String> billedDaysForExcess;//date for calculate excess
	
	/** The billed amounts. */
	@XmlElement(name = "billedAmounts", required = false)
	private List<String> billedAmounts;//amount to claim
	
	/** The billed days. */
	@XmlElement(name = "billedDays", required = false)
	private List<String> billedDays;//date to claim
	
	/** The billed percent. */
	@XmlElement(name = "billedPercent", required = false)
	private double billedPercent;
	
	/** The claim amounts. */
	@XmlElement(name = "claimAmounts", required = false)
	private List<String> claimAmounts;//amounts to available claim pay
	
	/** The claim days. */
	@XmlElement(name = "claimDays", required = false)
	private List<String> claimDays;//date to available claim use
	
	/** The claim percents. */
	@XmlElement(name = "claimPercents", required = false)
	private List<Double> claimPercents;
	
	@XmlElement(name = "claimTotalAmounts", required = false)
	private List<String> claimTotalAmounts;
	
	/** The claim percent. */
	@XmlElement(name = "claimPercent", required = false)
	private double claimPercent;
	
	/** The excess amounts. */
	@XmlElement(name = "excessAmounts", required = false)
	private List<String> excessAmounts;
	
	/** The continuity claim amount. */
	@XmlElement(name = "continuityClaimAmount", required = false)
	private double continuityClaimAmount;
	
	/** The continuity claim day. */
	@XmlElement(name = "continuityClaimDay", required = false)
	private int continuityClaimDay;
	
	/** The continuity claim percent. */
	@XmlElement(name = "continuityClaimPercent", required = false)
	private float continuityClaimPercent;
	
	@XmlElement(name = "continuityDeDuctAmount", required = false)
	private double continuityDeDuctAmount;
	
//	@XmlElement(name = "benefitGroupClaimAmounts", required = false)
//	private Map<String, List<String>> benefitGroupClaimAmounts;
//	
//	@XmlElement(name = "benefitGroupClaimDays", required = false)
//	private Map<String, List<String>> benefitGroupClaimDays;
	
	/** The benefit group max claim percent. */
	@XmlElement(name = "benefitGroupMaxClaimPercent", required = false)
	private double benefitGroupMaxClaimPercent;
	
	/** The benefit group claim key. */
	@XmlElement(name = "benefitGroupClaimKey", required = false)
	private short benefitGroupClaimKey;
	
	//for case fixed claim
	/** The fixed amounts. */
	@XmlElement(name = "fixedAmounts", required = false)
	private List<String> fixedAmounts;//fixed amount to claim
	
	/** The fixed days. */
	@XmlElement(name = "fixedDays", required = false)
	private List<String> fixedDays;//fixed date to claim
	
	/** The fixed percents. */
	@XmlElement(name = "fixedPercents", required = false)
	private List<Double> fixedPercents;//fixed percent to claim
	
	/** The fixed percent. */
	@XmlElement(name = "fixedPercent", required = false)
	private double fixedPercent;//fixed percent to claim
	
	/** The is fixed. */
	@XmlElement(name = "isFixed", required = false)
	private boolean isFixed;
	
	/** The is fixed calculated. */
	@XmlElement(name = "isFixedCalculated", required = false)
	private boolean isFixedCalculated;
	
	//add By TeTe for BenefitAnalysisRule-1 25/06/2013
	/** The benefit priority. */
	@XmlElement(name = "benefitPriority", required = false)
	private int benefitPriority;//for order by benefit
	
	/** The request. */
	@XmlElement(name = "request", required = false)
	List<ClaimRemain> request;		//claim request
	
	/** The claim. */
	@XmlElement(name = "claim", required = false)
	List<ClaimRemain> claim;		//available claim
	
	/** The remain. */
	@XmlElement(name = "remain", required = false)
	List<ClaimRemain> remain;		//claim remain
	
	//for case D Duct
	/** The is d duct type. */
	@XmlElement(name = "isDDuctType", required = false)
	private boolean isDDuctType;
	
	/** The d duct fixed amounts. */
	@XmlElement(name = "dDuctFixedAmounts", required = false)
	private List<String> dDuctFixedAmounts;//d duct fixed amount to claim
	
	/** The d duct fixed days. */
	@XmlElement(name = "dDuctFixedDays", required = false)
	private List<String> dDuctFixedDays;//d duct fixed date to claim
	
	/** The d duct fixed percent. */
	@XmlElement(name = "dDuctFixedPercent", required = false)
	private double 		 dDuctFixedPercent;//d duct fixed percent to claim
	
	/** The d duct fixed to tal real. */
	@XmlElement(name = "dDuctFixedToTalReal", required = false)
	private List<String> dDuctFixedToTalReal;
	
	/** The fixed input. */
	@XmlElement(name = "fixedInput", required = false)
	private List<String> fixedInput;
	
	/** The fixed output. */
	@XmlElement(name = "fixedOutput", required = false)
	private List<String> fixedOutput;
	
	//split from
	/** The is own split. */
	@XmlElement(name = "isOwnSplit", required = false)
	private List<String> isOwnSplit;
	
	//for map claim
	/** The map benefit index. */
	@XmlElement(name = "mapBenefitIndex", required = false)
	private List<Integer> mapBenefitIndex;
	
	//for RS0 Special case
	/** The is benefit special case r s0. */
	@XmlElement(name = "isBenefitSpecialCaseRS0", required = false)
	private boolean isBenefitSpecialCaseRS0;
	
	//for RBO Special
	/** The sender rbo. */
	@XmlElement(name = "senderRBO", required = false)
	private boolean senderRBO;
	
	/** The receiver rbo. */
	@XmlElement(name = "receiverRBO", required = false)
	private boolean receiverRBO;
	
	/** The RB type. */
	@XmlElement(name = "RBType", required = false)
	private boolean RBType;
	
	/** The ICU type. */
	@XmlElement(name = "ICUType", required = false)
	private boolean ICUType;
	
	//for RBH Special
	/** The RBH group transfer. */
	@XmlElement(name = "RBHGroupTransfer", required = false)
	private boolean RBHGroupTransfer;
	
	@XmlElement(name = "dDuctIndex", required = false)
	private int dDuctIndex;
	@XmlElement(name = "fixedIndex", required = false)
	private int fixedIndex;
	
	@XmlElement(name = "handicappedMultiplier", required = false)
	private int handicappedMultiplier;
	@XmlElement(name = "handicappedType", required = false)
	private boolean handicappedType;
	@XmlElement(name = "handicappedShareDay", required = false)
	private boolean handicappedShareDay;
	@XmlElement(name = "lostFlag", required = false)
	private boolean lostFlag;
	@XmlElement(name = "rhlFlag", required = false)
	private boolean rhlFlag;
	
	
	@XmlElement(name = "disabledCase", required = false)
	private boolean disabledCase;
	@XmlElement(name = "deformedCase", required = false)
	private boolean deformedCase;
	@XmlElement(name = "lockPAByYearCase", required = false)
	private boolean lockPAByYearCase;
	
	@XmlElement(name = "sumContinuityPercent", required = false)
	private double sumContinuityPercent;
	@XmlElement(name = "sumContinuityAmount", required = false)
	private double sumContinuityAmount;
	
	@XmlElement(name = "medicalExpenseSender", required = false)
	private boolean medicalExpenseSender;
	@XmlElement(name = "medicalExpenseReceiver", required = false)
	private boolean medicalExpenseReceiver;
	
	@XmlElement(name = "benefitIndex", required = false)
	private int benefitIndex;
	@XmlElement(name = "groupSumBenefit", required = false)
	private boolean groupSumBenefit;
	
	@XmlElement(name = "motorCycleFlag", required = false)
	private boolean motorCycleFlag = false;
	@XmlElement(name = "motorCyclePercent", required = false)
	private double motorCyclePercent = 0.0;
	
	@XmlElement(name = "deDuctList", required = false)
	private List<DeDuct> deDuctList = new ArrayList<DeDuct>();
	
	public Benefit() {
		setBilledAmounts(new ArrayList<String>());
		setBilledPercent(0);
		setBilledDays(new ArrayList<String>());
		setClaimAmounts(new ArrayList<String>());
		setClaimTotalAmounts(new ArrayList<String>());
		setClaimPercent(0);
		setClaimDays(new ArrayList<String>());
		setClaimPercents(new ArrayList<Double>());
		setMapBenefitIndex(new ArrayList<Integer>());
		setFixedAmounts(new ArrayList<String>());
		setFixedPercent(0);
		setFixedDays(new ArrayList<String>());
		setFixedPercents(new ArrayList<Double>());
		setBilledDaysForExcess(new ArrayList<String>());
		setExcessAmounts(new ArrayList<String>());
		setFixedOutput(new ArrayList<String>());
		setFixedInput(new ArrayList<String>());
		setIsOwnSplit(new ArrayList<String>());
		setdDuctFixedAmounts(new ArrayList<String>());
		setdDuctFixedPercent(0);
		setdDuctFixedDays(new ArrayList<String>());
		setdDuctFixedToTalReal(new ArrayList<String>());
		setSenderRBO(false);
		setReceiverRBO(false);
		setHandicappedType(false);
		setHandicappedMultiplier(0);
		setSumContinuityPercent(0.0);
		setSumContinuityAmount(0.0);
		
//		benefitGroupClaimAmounts = new HashMap<String, List<String>>();
//		benefitGroupClaimDays 	= new HashMap<String, List<String>>();
		benefitGroupMaxClaimPercent = 0;
		continuityClaimAmount 	= 0;
		continuityClaimDay 		= 0;
		continuityClaimPercent 	= 0;
		continuityDeDuctAmount 	= 0;
		benefitGroupClaimKey 	= 0;
		maxAvailableAmount   	= 0;
		request 				= new ArrayList<ClaimRemain>();
		claim 					= new ArrayList<ClaimRemain>();
		remain 					= new ArrayList<ClaimRemain>();
		isDDuctType 			= false;
		isBenefitSpecialCaseRS0 = false;
		isFixed					= false;
		benefit 				= new BenefitMs();
		RBType					= false;
		ICUType 				= false;
		RBHGroupTransfer 		= false;
		lostFlag 				= false;
		dDuctIndex 				= 0;
		fixedIndex 				= 0;
		handicappedShareDay 	= false;
		deformedCase 			= false;
		disabledCase 			= false;
		lockPAByYearCase 		= false;
		medicalExpenseSender	= false;
		medicalExpenseReceiver	= false;
		benefitIndex			= 0;
		groupSumBenefit			= false;
		rhlFlag					= false;
	}
	
	/**
	 * Instantiates a new benefit.
	 *
	 * @param benefit the benefit
	 */
	public Benefit(BenefitMs benefit) {
		this();
		setMasterModel(benefit);
		setAvailableAmount(benefit.getMaxClaimAmount());
		setAvailableDays(benefit.getMaxPayDays());
		setAvailableDaysIntial(benefit.getMaxPayDays());
		setAvailablePercent(benefit.getPercent());
		setIsOwnSplit(new ArrayList<String>());
	}
	
	/**
	 * Reset to cal.
	 */
	public void resetToCal(){
		setBilledAmounts(new ArrayList<String>());
		setBilledPercent(0);
		setBilledDays(new ArrayList<String>());
		setBilledDaysForExcess(new ArrayList<String>());
		
		setClaimAmounts(new ArrayList<String>());
		setClaimPercent(0);
		setClaimDays(new ArrayList<String>());
		setExcessAmounts(new ArrayList<String>());
		setFixedOutput(new ArrayList<String>());
		setIsOwnSplit(new ArrayList<String>());
		
		setAvailableDays(availableDaysIntial - continuityClaimDay);
		
		setAvailableAmount(maxAvailableAmount);
		
		//unfixed();
		fixedUncalculated();
		request = new ArrayList<ClaimRemain>();
		claim = new ArrayList<ClaimRemain>();
		remain = new ArrayList<ClaimRemain>();
		
		setdDuctIndex(0);
		setFixedIndex(0);
		
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#getMasterModel()
	 */
	@Override
	public BenefitMs getMasterModel() {
		return benefit;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#setMasterModel(com.mtl.model.templates.IMasterModel)
	 */
	@Override
	public void setMasterModel(IMasterModel masterModel) {
		benefit = (BenefitMs)masterModel;
	}

	/**
	 * Sets the claim amount.
	 *
	 * @param claimAmount the new claim amount
	 */
	public void setClaimAmount(double claimAmount) {
		if(claimAmounts.isEmpty()){
			claimAmounts.add(claimAmount+"");
			return;
		}
		this.claimAmounts.set(0, claimAmount+"");
	}

	/**
	 * Gets the claim amount.
	 *
	 * @return the claim amount
	 */
	public double getClaimAmount() {
		if(claimAmounts.isEmpty()){
			return 0;
		}
		return Double.parseDouble(claimAmounts.get(0));
	}
	
	public void setClaimTotalAmount(double claimTotalAmount) {
		if(claimTotalAmounts.isEmpty()){
			claimTotalAmounts.add(claimTotalAmount+"");
			return;
		}
		this.claimTotalAmounts.set(0, claimTotalAmount+"");
	}
	
	/**
	 * Sets the claim percent.
	 *
	 * @param claimPercent the new claim percent
	 */
	public void setClaimPercent(double claimPercent) {
		this.claimPercent = claimPercent;
	}

	/**
	 * Gets the claim percent.
	 *
	 * @return the claim percent
	 */
	public double getClaimPercent() {
		return claimPercent;
	}
	
	/**
	 * Sets the claim day.
	 *
	 * @param claimDay the new claim day
	 */
	public void setClaimDay(int claimDay) {
		if(claimDays.isEmpty()){
			claimDays.add(claimDay+"");
			return;
		}
		this.claimDays.set(0, claimDay+"");
	}

	/**
	 * Gets the claim day.
	 *
	 * @return the claim day
	 */
	public int getClaimDay() {
		if(claimDays.isEmpty()){
			return 0;
		}
		return Integer.parseInt(claimDays.get(0));
	}

	/**
	 * Sets the billed amount.
	 *
	 * @param billedAmount the new billed amount
	 */
	public void setBilledAmount(double billedAmount) {
		if(billedAmounts.isEmpty()){
			billedAmounts.add(billedAmount+"");
			return;
		}
		this.billedAmounts.set(0, billedAmount+"");
	}

	/**
	 * Gets the billed amount.
	 *
	 * @return the billed amount
	 */
	public double getBilledAmount() {
		if(billedAmounts.isEmpty()){
			return 0;
		}
		return Double.parseDouble(billedAmounts.get(0));
	}
	
	/**
	 * Sets the billed percent.
	 *
	 * @param billedPercent the new billed percent
	 */
	public void setBilledPercent(double billedPercent) {
		this.billedPercent = billedPercent;
	}

	/**
	 * Gets the billed percent.
	 *
	 * @return the billed percent
	 */
	public double getBilledPercent() {
		return billedPercent;
	}
	
	/**
	 * Sets the billed day.
	 *
	 * @param billedDay the new billed day
	 */
	public void setBilledDay(int billedDay) {
		if(billedDays.isEmpty()){
			billedDays.add(billedDay+"");
			return;
		}
		this.billedDays.set(0, billedDay+"");
	}

	/**
	 * Gets the billed day.
	 *
	 * @return the billed day
	 */
	public int getBilledDay() {
		if(billedDays.isEmpty()){
			return 0;
		}
		return Integer.parseInt(billedDays.get(0));
	}
	
	/**
	 * Sets the excess amount.
	 *
	 * @param excessAmount the new excess amount
	 */
	public void setExcessAmount(double excessAmount) {
		if(excessAmount < 0){
			excessAmount = 0;
		}
		if(excessAmounts.isEmpty()){
			excessAmounts.add(excessAmount+"");
			return;
		}
		this.excessAmounts.set(0, excessAmount+"");
	}

	/**
	 * Gets the excess amount.
	 *
	 * @return the excess amount
	 */
	public double getExcessAmount() {
		if(excessAmounts.isEmpty()){
			return 0;
		}
		return Double.parseDouble(excessAmounts.get(0));
	}

	/**
	 * Sets the available amount.
	 *
	 * @param maxClaimAmount the new available amount
	 */
	public void setAvailableAmount(double maxClaimAmount) {
		this.availableAmount = maxClaimAmount;
	}

	/**
	 * Gets the available amount.
	 *
	 * @return the available amount
	 */
	public double getAvailableAmount() {
		return availableAmount;
	}

	/**
	 * Sets the available days.
	 *
	 * @param availableDays the new available days
	 */
	public void setAvailableDays(int availableDays) {
		this.availableDays = availableDays;
	}

	/**
	 * Gets the available days.
	 *
	 * @return the available days
	 */
	public int getAvailableDays() {
		return availableDays;
	}
	
	/**
	 * Gets the benefit group.
	 *
	 * @return the benefit group
	 */
	public String getBenefitGroup(){
		return benefit.getBenefitGroup();
	}
	
	/**
	 * Gets the benefit code.
	 *
	 * @return the benefit code
	 */
	public String getBenefitCode(){
		return benefit.getCode();
	}
	
	/**
	 * Gets the max pay days.
	 *
	 * @return the max pay days
	 */
	public int getMaxPayDays(){
		return benefit.getMaxPayDays();
	}
	
	/**
	 * Gets the max claim amount.
	 *
	 * @return the max claim amount
	 */
	public double getMaxClaimAmount(){
		return benefit.getMaxClaimAmount();
	}
	
	/**
	 * Gets the percent.
	 *
	 * @return the percent
	 */
	public double getPercent(){
		return benefit.getPercent();
	}
	
	/**
	 * Gets the sum insured.
	 *
	 * @return the sum insured
	 */
	public double getSumInsured(){
		return benefit.getSumInsured();
	}
	
	/**
	 * Sets the sum insured.
	 *
	 * @param value the new sum insured
	 */
	public void setSumInsured(double value){
		benefit.setSumInsured(value);
	}
	
	/**
	 * Gets the benefit mode.
	 *
	 * @return the benefit mode
	 */
	public BenefitMode getBenefitMode(){
		return benefit.getBenefitMode();
	}
	
	/**
	 * Gets the link mode.
	 *
	 * @return the link mode
	 */
	public LinkMode getLinkMode(){
		return benefit.getLinkMode();
	}

	/**
	 * Sets the billed amounts.
	 *
	 * @param billedAmounts the new billed amounts
	 */
	public void setBilledAmounts(List<String> billedAmounts) {
		this.billedAmounts = billedAmounts;
	}
	
	/**
	 * Adds the billed amounts.
	 *
	 * @param billedAmounts the billed amounts
	 */
	public void addBilledAmounts(List<String> billedAmounts) {
		for(String billedAmount : billedAmounts){
			billedAmounts.add(billedAmount);
		}
	}

	/**
	 * Gets the billed amounts.
	 *
	 * @return the billed amounts
	 */
	public List<String> getBilledAmounts() {
		return billedAmounts;
	}
	
	/**
	 * Adds the billed amount.
	 *
	 * @param billedAmount the billed amount
	 */
	public void addBilledAmount(String billedAmount){
		this.billedAmounts.add(billedAmount);
	}

	/**
	 * Sets the billed days.
	 *
	 * @param billedDays the new billed days
	 */
	public void setBilledDays(List<String> billedDays) {
		this.billedDays = billedDays;
	}
	
	/**
	 * Adds the billed days.
	 *
	 * @param billedDays the billed days
	 */
	public void addBilledDays(List<String> billedDays){
		for(String billedDay : billedDays){
			this.billedDays.add(billedDay);
		}
	}

	/**
	 * Gets the billed days.
	 *
	 * @return the billed days
	 */
	public List<String> getBilledDays() {
		return billedDays;
	}
	
	/**
	 * Adds the billed day.
	 *
	 * @param billDay the bill day
	 */
	public void addBilledDay(String billDay){
		this.billedDays.add(billDay);
	}

	/**
	 * Sets the claim amounts.
	 *
	 * @param claimAmounts the new claim amounts
	 */
	public void setClaimAmounts(List<String> claimAmounts) {
		this.claimAmounts = claimAmounts;
	}

	/**
	 * Gets the claim amounts.
	 *
	 * @return the claim amounts
	 */
	public List<String> getClaimAmounts() {
		return claimAmounts;
	}

	/**
	 * Sets the claim days.
	 *
	 * @param claimDays the new claim days
	 */
	public void setClaimDays(List<String> claimDays) {
		this.claimDays = claimDays;
	}

	/**
	 * Gets the claim days.
	 *
	 * @return the claim days
	 */
	public List<String> getClaimDays() {
		return claimDays;
	}

	/**
	 * Sets the excess amounts.
	 *
	 * @param excessAmounts the new excess amounts
	 */
	public void setExcessAmounts(List<String> excessAmounts) {
		this.excessAmounts = excessAmounts;
	}

	/**
	 * Gets the excess amounts.
	 *
	 * @return the excess amounts
	 */
	public List<String> getExcessAmounts() {
		ArrayList<String> excessAmounts = new ArrayList<String>();
		for(int i = 0; i<billedAmounts.size(); i++){
			excessAmounts.add(
					Double.parseDouble(billedAmounts.get(i)) * Integer.parseInt(billedDays.get(i)) - 
					Double.parseDouble(claimAmounts.get(i)) * Integer.parseInt(claimDays.get(i))+"");
		}
		return excessAmounts;
	}
	
	/**
	 * Sets the continuity claim percent.
	 *
	 * @param continuityClaimPercent the new continuity claim percent
	 */
	public void setContinuityClaimPercent(float continuityClaimPercent) {
		this.continuityClaimPercent = continuityClaimPercent;
	}

	/**
	 * Gets the continuity claim percent.
	 *
	 * @return the continuity claim percent
	 */
	public float getContinuityClaimPercent() {
		return continuityClaimPercent;
	}

	/**
	 * Sets the continuity claim day.
	 *
	 * @param continuityClaimDay the new continuity claim day
	 */
	public void setContinuityClaimDay(int continuityClaimDay) {
		this.continuityClaimDay = continuityClaimDay;
	}

	/**
	 * Gets the continuity claim day.
	 *
	 * @return the continuity claim day
	 */
	public int getContinuityClaimDay() {
		return continuityClaimDay;
	}

	/**
	 * Sets the continuity claim amount.
	 *
	 * @param continuityClaimAmount the new continuity claim amount
	 */
	public void setContinuityClaimAmount(double continuityClaimAmount) {
		this.continuityClaimAmount = continuityClaimAmount;
	}

	/**
	 * Gets the continuity claim amount.
	 *
	 * @return the continuity claim amount
	 */
	public double getContinuityClaimAmount() {
		return continuityClaimAmount;
	}

//	public Map<String, List<String>> getBenefitGroupClaimAmounts() {
//		return benefitGroupClaimAmounts;
//	}
//
//	public Map<String, List<String>> getBenefitGroupClaimDays() {
//		return benefitGroupClaimDays;
//	}
//	
	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public double getTotalAmount(){
		double totalAmount = 0;
		for(String amount : getTotalAmounts()){
			totalAmount += Double.parseDouble(amount);
		}
		return totalAmount;
	}
	
	/**
	 * Gets the total amounts.
	 *
	 * @return the total amounts
	 */
	public List<String> getTotalAmounts() {
		List<String> totals = new ArrayList<String>();
		/**
		for(int i = 0; i < claimAmounts.size(); i++){
			double total = 0;
			if(getBenefitMode() == BenefitMode.WEEKLY || 
					getBenefitMode() == BenefitMode.DAILY ){
				total = Double.parseDouble(claimAmounts.get(i)) * Integer.parseInt(claimDays.get(i));
			}else{
				total = Double.parseDouble(claimAmounts.get(i));
			}
			/**/
		//edit for lof4clm issue double many floating 21/03/2014
		for(int i = 0; i < claimTotalAmounts.size(); i++){
			totals.add(claimTotalAmounts.get(i)+"");
		}
		return totals;
	}
	
	public List<String> getTotalClaimAmounts() {
		List<String> totals = new ArrayList<String>();
		for(int i = 0; i < claimAmounts.size(); i++){
			double total = 0;
			if(getBenefitMode() == BenefitMode.WEEKLY || 
					getBenefitMode() == BenefitMode.DAILY ){
				total = Double.parseDouble(claimAmounts.get(i)) * Integer.parseInt(claimDays.get(i));
			}else{
				total = Double.parseDouble(claimAmounts.get(i));
			}
			totals.add(total + "");
		}
		return totals;
	}
	
	public double getTotalClaimAmount(){
		double totalAmount = 0;
		for(String amount : getTotalClaimAmounts()){
			totalAmount += Double.parseDouble(amount);
		}
		return totalAmount;
	}
	/**
	 * Gets the total d duct amount.
	 *
	 * @return the total d duct amount
	 */
	public double getTotalDDuctAmount(){
		double totalAmount = 0;
		for(String amount : getTotalDDuctAmounts()){
			totalAmount += Double.parseDouble(amount);
		}
		return totalAmount;
	}
	
	/**
	 * Gets the total d duct amounts.
	 *
	 * @return the total d duct amounts
	 */
	public List<String> getTotalDDuctAmounts() {
		List<String> totals = new ArrayList<String>();
		for(int i = 0; i < dDuctFixedAmounts.size(); i++){
			double total = 0;
			if(getBenefitMode() == BenefitMode.WEEKLY || 
					getBenefitMode() == BenefitMode.DAILY ){
				total = Double.parseDouble(dDuctFixedAmounts.get(i)) * Integer.parseInt(dDuctFixedDays.get(i));
			}else{
				total = Double.parseDouble(dDuctFixedAmounts.get(i));
			}
			totals.add(total+"");
		}
		return totals;
	}
	
	/**
	 * Gets the total billed amount.
	 *
	 * @return the total billed amount
	 */
	public double getTotalBilledAmount(){
		double totalAmount = 0;
		for(String amount : getTotalBilledAmounts()){
			totalAmount += Double.parseDouble(amount);
		}
		return totalAmount;
	}
	
	/**
	 * Gets the total billed amounts.
	 *
	 * @return the total billed amounts
	 */
	public List<String> getTotalBilledAmounts() {
		List<String> totals = new ArrayList<String>();
		for(int i = 0; i < billedAmounts.size(); i++){
			double total = 0;
			if(getBenefitMode() == BenefitMode.WEEKLY || getBenefitMode() == BenefitMode.DAILY ){
				total = Double.parseDouble(billedAmounts.get(i)) * Integer.parseInt(billedDays.get(i));
			}else{
				total = Double.parseDouble(billedAmounts.get(i));
			}
			totals.add(total+"");
		}
		return totals;
	}
	
	/**
	 * Gets the total billed days.
	 *
	 * @return the total billed days
	 */
	public int getTotalBilledDays(){
		int days = 0;
		for(String day : billedDays){
			days += Integer.parseInt(day);
		}
		return days;
	}
	//Special Methods
	
	/**
	 * Clear excess amounts.
	 */
	public void clearExcessAmounts(){
		for(int i = 0; i < excessAmounts.size(); i++){
			excessAmounts.set(i, "0");
		}
	}
	
	/**
	 * Gets the total used days.
	 *
	 * @return the total used days
	 */
	public int getTotalUsedDays(){
		int days = 0;
		for(String day : claimDays){
			days += Integer.parseInt(day);
		}
		return days;
	}

	/**
	 * Fixed.
	 */
	public void fixed(){
		setFixed(true);
	}
	
	/**
	 * Unfixed.
	 */
	public void unfixed(){
		setFixed(false);
	}
	
	/**
	 * Fixed calculated.
	 */
	public void fixedCalculated(){
		setFixedCalculated(true);
	}
	
	/**
	 * Fixed uncalculated.
	 */
	public void fixedUncalculated(){
		setFixedCalculated(false);
	}
	
	/**
	 * Sets the fixed.
	 *
	 * @param isFixed the new fixed
	 */
	private void setFixed(boolean isFixed) {
		this.isFixed = isFixed;
	}

	/**
	 * Checks if is fixed.
	 *
	 * @return true, if is fixed
	 */
	public boolean isFixed() {
		return isFixed;
	}

	/**
	 * Checks if is fixed calculated.
	 *
	 * @return true, if is fixed calculated
	 */
	public boolean isFixedCalculated() {
		return isFixedCalculated;
	}
	
	/**
	 * Sets the fixed calculated.
	 *
	 * @param isFixedCalculated the new fixed calculated
	 */
	public void setFixedCalculated(boolean isFixedCalculated) {
		this.isFixedCalculated = isFixedCalculated;
	}
	
	/**
	 * Sets the benefit group max claim percent.
	 *
	 * @param benefitGroupMaxClaimPercent the new benefit group max claim percent
	 */
	public void setBenefitGroupMaxClaimPercent(double benefitGroupMaxClaimPercent) {
		this.benefitGroupMaxClaimPercent = benefitGroupMaxClaimPercent;
	}

	/**
	 * Gets the benefit group max claim percent.
	 *
	 * @return the benefit group max claim percent
	 */
	public double getBenefitGroupMaxClaimPercent() {
		return benefitGroupMaxClaimPercent;
	}
	
	/**
	 * Clean daily claim amounts.
	 */
	public void cleanDailyClaimAmounts(){
		if(getBenefitMode()==BenefitMode.DAILY ||
				getBenefitMode()==BenefitMode.WEEKLY ){
			for(int i = 0; i<claimAmounts.size(); i++){
				if(Short.parseShort(claimDays.get(i))<=0 || 
						Double.parseDouble(claimAmounts.get(i))<=0){
					claimAmounts.remove(i);
					claimDays.remove(i);
					benefitGroupClaimKey--;
				}
			}
		}
	}
	
//	public int getUnableClaimDays(String groupKey){
//		short unableClaimDays = 0;
//		List<String> myClaimAmounts = benefitGroupClaimAmounts.get(groupKey);
//		List<String> myClaimDays = benefitGroupClaimDays.get(groupKey);
//		for(int i = 0; i<myClaimAmounts.size(); i++){
//			if(Double.parseDouble(myClaimAmounts.get(i))<=0){
//				unableClaimDays+=Short.parseShort(myClaimDays.get(i));
//			}
//		}
//		return unableClaimDays;
//	}
	
	/**
	 * Adds the claim amounts to benefit group.
	 *
	 * @param groupKey the group key
	 */
	public void addClaimAmountsToBenefitGroup(String groupKey){
		ArrayList<String> myClaimAmounts = new ArrayList<String>();
		ArrayList<String> myClaimDays = new ArrayList<String>();
		while( benefitGroupClaimKey < claimAmounts.size()){
			myClaimAmounts.add(claimAmounts.get(benefitGroupClaimKey));
			myClaimDays.add(claimDays.get(benefitGroupClaimKey));
			benefitGroupClaimKey++;
		}
		if(claimPercent > benefitGroupMaxClaimPercent){
			benefitGroupMaxClaimPercent = claimPercent;
		}
//		benefitGroupClaimAmounts.put(groupKey, myClaimAmounts);
//		benefitGroupClaimDays.put(groupKey, myClaimDays);
	}
	
	/**
	 * Prints the fixed input.
	 */
	public void printFixedInput(){
		System.out.println("-------------------fixed input---------------------");
		if(fixedInput != null && fixedInput.size() > 0){
			System.out.print("[");
			for(String index:fixedInput){
				System.out.print(index + ",");
			}
			System.out.print("]\n");
		}else{
			System.out.println("\t\tnull");
		}
		System.out.println("-------------------fixed input---------------------");
	}

	public void addBilledDeDuct(double amount){
		DeDuct deDuct = new DeDuct();
		deDuct.setBilledDeDuct(amount);
		deDuctList.add(deDuct);
	}
	
	public double getTotalClaimDeDuct(){
		double sum = 0.0;
		for(DeDuct deDuct:deDuctList)sum = sum + deDuct.getClaimDeDuct();
		return sum;
	}
	
	/**
	 * Gets the benefit priority.
	 *
	 * @return the benefit priority
	 */
	public int getBenefitPriority() {
		return benefitPriority;
	}

	/**
	 * Sets the benefit priority.
	 *
	 * @param benefitPriority the new benefit priority
	 */
	public void setBenefitPriority(int benefitPriority) {
		this.benefitPriority = benefitPriority;
	}

	/**
	 * Gets the remain.
	 *
	 * @return the remain
	 */
	public List<ClaimRemain> getRemain() {
		return remain;
	}

	/**
	 * Sets the remain.
	 *
	 * @param remain the new remain
	 */
	public void setRemain(List<ClaimRemain> remain) {
		this.remain = remain;
	}

	/**
	 * Gets the request.
	 *
	 * @return the request
	 */
	public List<ClaimRemain> getRequest() {
		return request;
	}

	/**
	 * Sets the request.
	 *
	 * @param request the new request
	 */
	public void setRequest(List<ClaimRemain> request) {
		this.request = request;
	}

	/**
	 * Gets the claim.
	 *
	 * @return the claim
	 */
	public List<ClaimRemain> getClaim() {
		return claim;
	}

	/**
	 * Sets the claim.
	 *
	 * @param claim the new claim
	 */
	public void setClaim(List<ClaimRemain> claim) {
		this.claim = claim;
	}

	/**
	 * Gets the fixed amounts.
	 *
	 * @return the fixed amounts
	 */
	public List<String> getFixedAmounts() {
		return fixedAmounts;
	}

	/**
	 * Sets the fixed amounts.
	 *
	 * @param fixedAmounts the new fixed amounts
	 */
	public void setFixedAmounts(List<String> fixedAmounts) {
		this.fixedAmounts = fixedAmounts;
	}

	/**
	 * Gets the fixed days.
	 *
	 * @return the fixed days
	 */
	public List<String> getFixedDays() {
		return fixedDays;
	}

	/**
	 * Sets the fixed days.
	 *
	 * @param fixedDays the new fixed days
	 */
	public void setFixedDays(List<String> fixedDays) {
		this.fixedDays = fixedDays;
	}

	/**
	 * Gets the fixed percent.
	 *
	 * @return the fixed percent
	 */
	public double getFixedPercent() {
		return fixedPercent;
	}

	/**
	 * Sets the fixed percent.
	 *
	 * @param fixedPercent the new fixed percent
	 */
	public void setFixedPercent(double fixedPercent) {
		this.fixedPercent = fixedPercent;
	}

	/**
	 * Gets the map benefit index.
	 *
	 * @return the map benefit index
	 */
	public List<Integer> getMapBenefitIndex() {
		return mapBenefitIndex;
	}

	/**
	 * Sets the map benefit index.
	 *
	 * @param mapBenefitIndex the new map benefit index
	 */
	public void setMapBenefitIndex(List<Integer> mapBenefitIndex) {
		this.mapBenefitIndex = mapBenefitIndex;
	}

	/**
	 * Gets the available percent.
	 *
	 * @return the available percent
	 */
	public double getAvailablePercent() {
		return availablePercent;
	}

	/**
	 * Sets the available percent.
	 *
	 * @param availablePercent the new available percent
	 */
	public void setAvailablePercent(double availablePercent) {
		this.availablePercent = availablePercent;
	}

	/**
	 * Gets the d duct fixed amounts.
	 *
	 * @return the d duct fixed amounts
	 */
	public List<String> getdDuctFixedAmounts() {
		return dDuctFixedAmounts;
	}

	/**
	 * Sets the d duct fixed amounts.
	 *
	 * @param dDuctFixedAmounts the new d duct fixed amounts
	 */
	public void setdDuctFixedAmounts(List<String> dDuctFixedAmounts) {
		this.dDuctFixedAmounts = dDuctFixedAmounts;
	}

	/**
	 * Gets the d duct fixed days.
	 *
	 * @return the d duct fixed days
	 */
	public List<String> getdDuctFixedDays() {
		return dDuctFixedDays;
	}

	/**
	 * Sets the d duct fixed days.
	 *
	 * @param dDuctFixedDays the new d duct fixed days
	 */
	public void setdDuctFixedDays(List<String> dDuctFixedDays) {
		this.dDuctFixedDays = dDuctFixedDays;
	}

	/**
	 * Gets the d duct fixed percent.
	 *
	 * @return the d duct fixed percent
	 */
	public double getdDuctFixedPercent() {
		return dDuctFixedPercent;
	}

	/**
	 * Sets the d duct fixed percent.
	 *
	 * @param dDuctFixedPercent the new d duct fixed percent
	 */
	public void setdDuctFixedPercent(double dDuctFixedPercent) {
		this.dDuctFixedPercent = dDuctFixedPercent;
	}

	/**
	 * Gets the fixed input.
	 *
	 * @return the fixed input
	 */
	public List<String> getFixedInput() {
		return fixedInput;
	}

	/**
	 * Sets the fixed input.
	 *
	 * @param fixedInput the new fixed input
	 */
	public void setFixedInput(List<String> fixedInput) {
		this.fixedInput = fixedInput;
	}

	/**
	 * Gets the fixed output.
	 *
	 * @return the fixed output
	 */
	public List<String> getFixedOutput() {
		return fixedOutput;
	}

	/**
	 * Sets the fixed output.
	 *
	 * @param fixedOutput the new fixed output
	 */
	public void setFixedOutput(List<String> fixedOutput) {
		this.fixedOutput = fixedOutput;
	}
	
	/**
	 * Gets the checks if is own split.
	 *
	 * @return the checks if is own split
	 */
	public List<String> getIsOwnSplit() {
		return isOwnSplit;
	}

	/**
	 * Sets the checks if is own split.
	 *
	 * @param isOwnSplit the new checks if is own split
	 */
	public void setIsOwnSplit(List<String> isOwnSplit) {
		this.isOwnSplit = isOwnSplit;
	}

	/**
	 * Gets the fixed percents.
	 *
	 * @return the fixed percents
	 */
	public List<Double> getFixedPercents() {
		return fixedPercents;
	}

	/**
	 * Sets the fixed percents.
	 *
	 * @param fixedPercents the new fixed percents
	 */
	public void setFixedPercents(List<Double> fixedPercents) {
		this.fixedPercents = fixedPercents;
	}

	/**
	 * Gets the claim percents.
	 *
	 * @return the claim percents
	 */
	public List<Double> getClaimPercents() {
		return claimPercents;
	}

	/**
	 * Sets the claim percents.
	 *
	 * @param claimPercents the new claim percents
	 */
	public void setClaimPercents(List<Double> claimPercents) {
		this.claimPercents = claimPercents;
	}
	
	/**
	 * Gets the calculate option.
	 *
	 * @return the calculate option
	 */
	public CalculateOption getCalculateOption() {
		return benefit.getCalculateOption();
	}

	/**
	 * Gets the benefit group short name.
	 *
	 * @return the benefit group short name
	 */
	public String getBenefitGroupShortName() {
		return benefit.getBenefitGroupShortName();
	}

	/**
	 * Checks if is d duct type.
	 *
	 * @return true, if is d duct type
	 */
	public boolean isDDuctType() {
		return isDDuctType;
	}

	/**
	 * Sets the d duct type.
	 *
	 * @param isDDuctType the new d duct type
	 */
	public void setDDuctType(boolean isDDuctType) {
		this.isDDuctType = isDDuctType;
	}

	/**
	 * Gets the d duct fixed to tal real.
	 *
	 * @return the d duct fixed to tal real
	 */
	public List<String> getdDuctFixedToTalReal() {
		return dDuctFixedToTalReal;
	}

	/**
	 * Sets the d duct fixed to tal real.
	 *
	 * @param dDuctFixedToTalReal the new d duct fixed to tal real
	 */
	public void setdDuctFixedToTalReal(List<String> dDuctFixedToTalReal) {
		this.dDuctFixedToTalReal = dDuctFixedToTalReal;
	}

	/**
	 * Gets the available days intial.
	 *
	 * @return the available days intial
	 */
	public int getAvailableDaysIntial() {
		return availableDaysIntial;
	}

	/**
	 * Sets the available days intial.
	 *
	 * @param availableDaysIntial the new available days intial
	 */
	public void setAvailableDaysIntial(int availableDaysIntial) {
		this.availableDaysIntial = availableDaysIntial;
	}

	/**
	 * Checks if is benefit special case r s0.
	 *
	 * @return true, if is benefit special case r s0
	 */
	public boolean isBenefitSpecialCaseRS0() {
		return isBenefitSpecialCaseRS0;
	}

	/**
	 * Sets the benefit special case r s0.
	 *
	 * @param isBenefitSpecialCaseRS0 the new benefit special case r s0
	 */
	public void setBenefitSpecialCaseRS0(boolean isBenefitSpecialCaseRS0) {
		this.isBenefitSpecialCaseRS0 = isBenefitSpecialCaseRS0;
	}

	/**
	 * Checks if is sender rbo.
	 *
	 * @return true, if is sender rbo
	 */
	public boolean isSenderRBO() {
		return senderRBO;
	}

	/**
	 * Sets the sender rbo.
	 *
	 * @param senderRBO the new sender rbo
	 */
	public void setSenderRBO(boolean senderRBO) {
		this.senderRBO = senderRBO;
	}

	/**
	 * Checks if is receiver rbo.
	 *
	 * @return true, if is receiver rbo
	 */
	public boolean isReceiverRBO() {
		return receiverRBO;
	}

	/**
	 * Sets the receiver rbo.
	 *
	 * @param receiverRBO the new receiver rbo
	 */
	public void setReceiverRBO(boolean receiverRBO) {
		this.receiverRBO = receiverRBO;
	}

	/**
	 * Gets the billed days for excess.
	 *
	 * @return the billed days for excess
	 */
	public List<String> getBilledDaysForExcess() {
		return billedDaysForExcess;
	}

	/**
	 * Sets the billed days for excess.
	 *
	 * @param billedDaysForExcess the new billed days for excess
	 */
	public void setBilledDaysForExcess(List<String> billedDaysForExcess) {
		this.billedDaysForExcess = billedDaysForExcess;
	}

	/**
	 * Checks if is rB type.
	 *
	 * @return true, if is rB type
	 */
	public boolean isRBType() {
		return RBType;
	}

	/**
	 * Sets the rB type.
	 *
	 * @param rBType the new rB type
	 */
	public void setRBType(boolean rBType) {
		RBType = rBType;
	}

	/**
	 * Checks if is iCU type.
	 *
	 * @return true, if is iCU type
	 */
	public boolean isICUType() {
		return ICUType;
	}

	/**
	 * Sets the iCU type.
	 *
	 * @param iCUType the new iCU type
	 */
	public void setICUType(boolean iCUType) {
		ICUType = iCUType;
	}

	/**
	 * Checks if is rBH group transfer.
	 *
	 * @return true, if is rBH group transfer
	 */
	public boolean isRBHGroupTransfer() {
		return RBHGroupTransfer;
	}

	/**
	 * Sets the rBH group transfer.
	 *
	 * @param rBHGroupTransfer the new rBH group transfer
	 */
	public void setRBHGroupTransfer(boolean rBHGroupTransfer) {
		RBHGroupTransfer = rBHGroupTransfer;
	}

	public int getdDuctIndex() {
		return dDuctIndex;
	}

	public void setdDuctIndex(int dDuctIndex) {
		this.dDuctIndex = dDuctIndex;
	}

	public int getFixedIndex() {
		return fixedIndex;
	}

	public void setFixedIndex(int fixedIndex) {
		this.fixedIndex = fixedIndex;
	}
	//End Special Methods

	public boolean isHandicappedType() {
		return handicappedType;
	}

	public void setHandicappedType(boolean handicappedType) {
		this.handicappedType = handicappedType;
	}

	public int getHandicappedMultiplier() {
		return handicappedMultiplier;
	}

	public void setHandicappedMultiplier(int handicappedMultiplier) {
		this.handicappedMultiplier = handicappedMultiplier;
	}

	public boolean isLostFlag() {
		return lostFlag;
	}

	public void setLostFlag(boolean lostFlag) {
		this.lostFlag = lostFlag;
	}

	public double getContinuityDeDuctAmount() {
		return continuityDeDuctAmount;
	}

	public void setContinuityDeDuctAmount(double continuityDeDuctAmount) {
		this.continuityDeDuctAmount = continuityDeDuctAmount;
	}

	public boolean isHandicappedShareDay() {
		return handicappedShareDay;
	}

	public void setHandicappedShareDay(boolean handicappedShareDay) {
		this.handicappedShareDay = handicappedShareDay;
	}

	public List<String> getClaimTotalAmounts() {
		return claimTotalAmounts;
	}

	public void setClaimTotalAmounts(List<String> claimTotalAmounts) {
		this.claimTotalAmounts = claimTotalAmounts;
	}

	public boolean isDisabledCase() {
		return disabledCase;
	}

	public void setDisabledCase(boolean disabledCase) {
		this.disabledCase = disabledCase;
	}

	public boolean isDeformedCase() {
		return deformedCase;
	}

	public void setDeformedCase(boolean deformedCase) {
		this.deformedCase = deformedCase;
	}

	public double getMaxAvailableAmount() {
		return maxAvailableAmount;
	}

	public void setMaxAvailableAmount(double maxAvailableAmount) {
		this.maxAvailableAmount = maxAvailableAmount;
	}

	public boolean isLockPAByYearCase() {
		return lockPAByYearCase;
	}

	public void setLockPAByYearCase(boolean lockPAByYearCase) {
		this.lockPAByYearCase = lockPAByYearCase;
	}

	public double getSumContinuityPercent() {
		return sumContinuityPercent;
	}

	public void setSumContinuityPercent(double sumContinuityPercent) {
		this.sumContinuityPercent = sumContinuityPercent;
	}

	public double getSumContinuityAmount() {
		return sumContinuityAmount;
	}

	public void setSumContinuityAmount(double sumContinuityAmount) {
		this.sumContinuityAmount = sumContinuityAmount;
	}

	public boolean isMedicalExpenseSender() {
		return medicalExpenseSender;
	}

	public void setMedicalExpenseSender(boolean medicalExpenseSender) {
		this.medicalExpenseSender = medicalExpenseSender;
	}

	public boolean isMedicalExpenseReceiver() {
		return medicalExpenseReceiver;
	}

	public void setMedicalExpenseReceiver(boolean medicalExpenseReceiver) {
		this.medicalExpenseReceiver = medicalExpenseReceiver;
	}

	public int getBenefitIndex() {
		return benefitIndex;
	}

	public void setBenefitIndex(int benefitIndex) {
		this.benefitIndex = benefitIndex;
	}

	public boolean isGroupSumBenefit() {
		return groupSumBenefit;
	}

	public void setGroupSumBenefit(boolean groupSumBenefit) {
		this.groupSumBenefit = groupSumBenefit;
	}

	public boolean isRhlFlag() {
		return rhlFlag;
	}

	public void setRhlFlag(boolean rhlFlag) {
		this.rhlFlag = rhlFlag;
	}

	public boolean isMotorCycleFlag() {
		return motorCycleFlag;
	}

	public void setMotorCycleFlag(boolean motorCycleFlag) {
		this.motorCycleFlag = motorCycleFlag;
	}

	public double getMotorCyclePercent() {
		return motorCyclePercent;
	}

	public void setMotorCyclePercent(double motorCyclePercent) {
		this.motorCyclePercent = motorCyclePercent;
	}

	public List<DeDuct> getDeDuctList() {
		return deDuctList;
	}

	public void setDeDuctList(List<DeDuct> deDuctList) {
		this.deDuctList = deDuctList;
	}

}
