/*
 * 
 */
package com.mtl.continuity.model.mtlclaim.master;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.continuity.model.enumerator.BenefitMode;
import com.mtl.continuity.model.enumerator.CalculateOption;
import com.mtl.continuity.model.enumerator.LinkMode;
import com.mtl.continuity.model.templates.IMasterModel;


/**
 * The Class BenefitMs.
 */
@XmlRootElement(name = "BenefitMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class BenefitMs implements IMasterModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4200143235406037482L;
	
	/** The benefit code. */
	@XmlElement(name = "benefitCode", required = false)
	private String benefitCode;
	
	/** The benefit group. */
	@XmlElement(name = "benefitGroup", required = false)
	private String benefitGroup;
	
	/** The benefit group short name. */
	@XmlElement(name = "benefitGroupShortName", required = false)
	private String benefitGroupShortName;
	
	/** The sum insured. */
	@XmlElement(name = "sumInsured", required = false)
	private double sumInsured;
	
	/** The percent. */
	@XmlElement(name = "percent", required = false)
	private double percent;
	
	/** The max claim amount. */
	@XmlElement(name = "maxClaimAmount", required = false)
	private double maxClaimAmount;
	
	/** The max pay days. */
	@XmlElement(name = "maxPayDays", required = false)
	private int maxPayDays;
	
	/** The benefit mode. */
	@XmlElement(name = "benefitMode", required = false)
	private BenefitMode benefitMode;
	
	/** The link mode. */
	@XmlElement(name = "linkMode", required = false)
	private LinkMode linkMode;
	
	/** The calculate option. */
	@XmlElement(name = "calculateOption", required = false)
	private CalculateOption calculateOption;
	
	/**
	 * Instantiates a new benefit ms.
	 */
	public BenefitMs() {
		benefitMode = null;
		linkMode = null;
		calculateOption = null;
	}
	
	/**
	 * Instantiates a new benefit ms.
	 *
	 * @param benefitCode the benefit code
	 * @param benefitGroup the benefit group
	 * @param sumInsured the sum insured
	 * @param percent the percent
	 * @param maxClaimAmount the max claim amount
	 * @param maxClaimDays the max claim days
	 * @param benefitMode the benefit mode
	 * @param linkMode the link mode
	 */
	public BenefitMs(String benefitCode, String benefitGroup, double sumInsured, double percent, 
			double maxClaimAmount, int maxClaimDays, BenefitMode benefitMode, LinkMode linkMode) {
		this();
		setCode(benefitCode);
		setBenefitGroup(benefitGroup);
		setSumInsured(sumInsured);
		setPercent(percent);
		setMaxClaimAmount(maxClaimAmount);
		setMaxPayDays(maxClaimDays);
		setBenefitMode(benefitMode);
		setLinkMode(linkMode);
	}

	/**
	 * Instantiates a new benefit ms.
	 *
	 * @param benefitCode the benefit code
	 * @param benefitGroup the benefit group
	 * @param sumInsured the sum insured
	 * @param percent the percent
	 * @param maxClaimAmount the max claim amount
	 * @param maxClaimDays the max claim days
	 * @param benefitMode the benefit mode
	 * @param linkMode the link mode
	 * @param benefitGroupShortName the benefit group short name
	 * @param calculateOption the calculate option
	 */
	public BenefitMs(String benefitCode, String benefitGroup, double sumInsured, double percent, 
			double maxClaimAmount, int maxClaimDays, BenefitMode benefitMode, LinkMode linkMode,
			String benefitGroupShortName,CalculateOption calculateOption) {
		this();
		setCode(benefitCode);
		setBenefitGroup(benefitGroup);
		setSumInsured(sumInsured);
		setPercent(percent);
		setMaxClaimAmount(maxClaimAmount);
		setMaxPayDays(maxClaimDays);
		setBenefitMode(benefitMode);
		setLinkMode(linkMode);
		setBenefitGroupShortName(benefitGroupShortName);
		setCalculateOption(calculateOption);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#getCode()
	 */
	@Override
	public String getCode() {
		return benefitCode;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.benefitCode = code;
	}

	/**
	 * Sets the sum insured.
	 *
	 * @param sumInsured the new sum insured
	 */
	public void setSumInsured(double sumInsured) {
		this.sumInsured = sumInsured;
	}

	/**
	 * Gets the sum insured.
	 *
	 * @return the sum insured
	 */
	public double getSumInsured() {
		return sumInsured;
	}

	/**
	 * Sets the max pay days.
	 *
	 * @param maxPayDays the new max pay days
	 */
	public void setMaxPayDays(int maxPayDays) {
		this.maxPayDays = maxPayDays;
	}

	/**
	 * Gets the max pay days.
	 *
	 * @return the max pay days
	 */
	public int getMaxPayDays() {
		return maxPayDays;
	}

	/**
	 * Sets the percent.
	 *
	 * @param percent the new percent
	 */
	public void setPercent(double percent) {
		this.percent = percent;
	}

	/**
	 * Gets the percent.
	 *
	 * @return the percent
	 */
	public double getPercent() {
		return percent;
	}

	/**
	 * Sets the benefit group.
	 *
	 * @param benefitGroup the new benefit group
	 */
	public void setBenefitGroup(String benefitGroup) {
		this.benefitGroup = benefitGroup;
	}

	/**
	 * Gets the benefit group.
	 *
	 * @return the benefit group
	 */
	public String getBenefitGroup() {
		return benefitGroup;
	}

	/**
	 * Sets the benefit mode.
	 *
	 * @param benefitMode the new benefit mode
	 */
	public void setBenefitMode(BenefitMode benefitMode) {
		this.benefitMode = benefitMode;
	}

	/**
	 * Gets the benefit mode.
	 *
	 * @return the benefit mode
	 */
	public BenefitMode getBenefitMode() {
		return benefitMode;
	}

	/**
	 * Sets the link mode.
	 *
	 * @param linkMode the new link mode
	 */
	public void setLinkMode(LinkMode linkMode) {
		this.linkMode = linkMode;
	}

	/**
	 * Gets the link mode.
	 *
	 * @return the link mode
	 */
	public LinkMode getLinkMode() {
		return linkMode;
	}

	/**
	 * Sets the max claim amount.
	 *
	 * @param maxClaimAmount the new max claim amount
	 */
	public void setMaxClaimAmount(double maxClaimAmount) {
		this.maxClaimAmount = maxClaimAmount;
	}

	/**
	 * Gets the max claim amount.
	 *
	 * @return the max claim amount
	 */
	public double getMaxClaimAmount() {
		return maxClaimAmount;
	}

	/**
	 * Sets the calculate option.
	 *
	 * @param calculateOption the new calculate option
	 */
	public void setCalculateOption(CalculateOption calculateOption) {
		this.calculateOption = calculateOption;
	}

	/**
	 * Gets the calculate option.
	 *
	 * @return the calculate option
	 */
	public CalculateOption getCalculateOption() {
		return calculateOption;
	}

	/**
	 * Gets the benefit group short name.
	 *
	 * @return the benefit group short name
	 */
	public String getBenefitGroupShortName() {
		return benefitGroupShortName;
	}

	/**
	 * Sets the benefit group short name.
	 *
	 * @param benefitGroupShortName the new benefit group short name
	 */
	public void setBenefitGroupShortName(String benefitGroupShortName) {
		this.benefitGroupShortName = benefitGroupShortName;
	}
}
