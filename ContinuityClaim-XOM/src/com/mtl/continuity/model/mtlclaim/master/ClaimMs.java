/*
 * 
 */
package com.mtl.continuity.model.mtlclaim.master;

import java.io.Serializable;
import java.util.Date;

import com.mtl.continuity.model.enumerator.ClaimType;
import com.mtl.continuity.model.templates.IMasterModel;
import com.mtl.continuity.util.DateTimeAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlRootElement(name = "ClaimMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ClaimMs implements IMasterModel, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4468478668462328051L;
	
	@XmlElement(name = "claimID", required = false)
	private String claimID;
	
	@XmlElement(name = "claimType", required = false)
	private ClaimType claimType;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "admissionDate", required = false)
	private Date admissionDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "incidentDate", required = false)
	private Date incidentDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "dischargeDate", required = false)
	private Date dischargeDate;

	/**
	 * Instantiates a new claim ms.
	 */
	public ClaimMs() {
		claimType = null;
		admissionDate = null;
		incidentDate = null;
		dischargeDate = null;
	}
	
	/**
	 * Instantiates a new claim ms.
	 *
	 * @param claimID the claim id
	 * @param claimType the claim type
	 * @param admissionDate the admission date
	 * @param incidentDate the incident date
	 * @param dischargeDate the discharge date
	 */
	public ClaimMs(String claimID, ClaimType claimType, Date admissionDate, 
			Date incidentDate, Date dischargeDate) {
		this();
		setCode(claimID);
		setClaimType(claimType);
		setAdmissionDate(admissionDate);
		setIncidentDate(incidentDate);
		setDischargeDate(dischargeDate);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#getCode()
	 */
	@Override
	public String getCode() {
		return claimID;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		claimID = code;
	}

	/**
	 * Sets the claim type.
	 *
	 * @param claimType the new claim type
	 */
	public void setClaimType(ClaimType claimType) {
		this.claimType = claimType;
	}

	/**
	 * Gets the claim type.
	 *
	 * @return the claim type
	 */
	public ClaimType getClaimType() {
		return claimType;
	}

	/**
	 * Sets the admission date.
	 *
	 * @param admissionDate the new admission date
	 */
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate() {
		return admissionDate;
	}

	/**
	 * Sets the incident date.
	 *
	 * @param incidentDate the new incident date
	 */
	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	 * Gets the incident date.
	 *
	 * @return the incident date
	 */
	public Date getIncidentDate() {
		return incidentDate;
	}

	/**
	 * Sets the discharge date.
	 *
	 * @param dischargeDate the new discharge date
	 */
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	/**
	 * Gets the discharge date.
	 *
	 * @return the discharge date
	 */
	public Date getDischargeDate() {
		return dischargeDate;
	}
	
}
