/*
 * 
 */
package com.mtl.continuity.model.mtlclaim.master;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.continuity.model.templates.IMasterModel;


@XmlRootElement(name = "DiagnosisMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class DiagnosisMs implements IMasterModel, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8267325661545905789L;
	
	@XmlElement(name = "diagnosisCode", required = false)
	private String diagnosisCode;
	
	@XmlElement(name = "exclusionIndicator", required = false)
	private Integer exclusionIndicator;
	
	/**
	 * Instantiates a new diagnosis ms.
	 */
	private DiagnosisMs(){
		
	}
	
	/**
	 * Instantiates a new diagnosis ms.
	 *
	 * @param diagnosisCode the diagnosis code
	 */
	public DiagnosisMs(String diagnosisCode){
		this();
		setCode(diagnosisCode);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#getCode()
	 */
	@Override
	public String getCode() {
		return diagnosisCode;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.diagnosisCode = code;
	}

	public Integer getExclusionIndicator() {
		return exclusionIndicator;
	}

	public void setExclusionIndicator(Integer exclusionIndicator) {
		this.exclusionIndicator = exclusionIndicator;
	}
	
}
