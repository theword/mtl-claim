/*
 * 
 */
package com.mtl.continuity.model.mtlclaim;

import java.io.Serializable;

/**
 * The Class ClaimTransfer.
 */
public class ClaimTransfer implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5664844717164088111L;
	
	
	/** The benefit group code. */
	private String benefitGroupCode;
	
	/** The claim amount. */
	private double claimAmount;
	
	/** The claim day. */
	private int	   claimDay;
	
	/** The claim percent. */
	private float  claimPercent;
	
	/** The used. */
	private boolean used;
	
	//get set method
	/**
	 * Gets the benefit group code.
	 *
	 * @return the benefit group code
	 */
	public String getBenefitGroupCode() {
		return benefitGroupCode;
	}
	
	/**
	 * Sets the benefit group code.
	 *
	 * @param benefitGroupCode the new benefit group code
	 */
	public void setBenefitGroupCode(String benefitGroupCode) {
		this.benefitGroupCode = benefitGroupCode;
	}
	
	/**
	 * Gets the claim amount.
	 *
	 * @return the claim amount
	 */
	public double getClaimAmount() {
		return claimAmount;
	}
	
	/**
	 * Sets the claim amount.
	 *
	 * @param claimAmount the new claim amount
	 */
	public void setClaimAmount(double claimAmount) {
		this.claimAmount = claimAmount;
	}
	
	/**
	 * Gets the claim day.
	 *
	 * @return the claim day
	 */
	public int getClaimDay() {
		return claimDay;
	}
	
	/**
	 * Sets the claim day.
	 *
	 * @param claimDay the new claim day
	 */
	public void setClaimDay(int claimDay) {
		this.claimDay = claimDay;
	}
	
	/**
	 * Gets the claim percent.
	 *
	 * @return the claim percent
	 */
	public float getClaimPercent() {
		return claimPercent;
	}
	
	/**
	 * Sets the claim percent.
	 *
	 * @param claimPercent the new claim percent
	 */
	public void setClaimPercent(float claimPercent) {
		this.claimPercent = claimPercent;
	}
	
	/**
	 * Checks if is used.
	 *
	 * @return true, if is used
	 */
	public boolean isUsed() {
		return used;
	}
	
	/**
	 * Sets the used.
	 *
	 * @param used the new used
	 */
	public void setUsed(boolean used) {
		this.used = used;
	}
	
	
}
