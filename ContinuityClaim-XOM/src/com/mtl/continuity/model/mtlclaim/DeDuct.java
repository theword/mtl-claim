/*
 * 
 */
package com.mtl.continuity.model.mtlclaim;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The Class DeDuct.
 */
@XmlRootElement(name = "DeDuct")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeDuct implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1662496267220213359L;
	@XmlElement(name = "claimDeDuct", 	required = false)private double claimDeDuct = 0.0;
	@XmlElement(name = "billedDeDuct", 	required = false)private double billedDeDuct = 0.0;
	
	private boolean calculateDeDuct = false;
	private double claimAmountDeDuct = 0.0;
	private Integer claimDayDeDuct;
	
	public double getClaimDeDuct() {
		return claimDeDuct;
	}
	public void setClaimDeDuct(double claimDeDuct) {
		this.claimDeDuct = claimDeDuct;
	}
	public double getBilledDeDuct() {
		return billedDeDuct;
	}
	public void setBilledDeDuct(double billedDeDuct) {
		this.billedDeDuct = billedDeDuct;
	}
	public boolean isCalculateDeDuct() {
		return calculateDeDuct;
	}
	public void setCalculateDeDuct(boolean calculateDeDuct) {
		this.calculateDeDuct = calculateDeDuct;
	}
	public double getClaimAmountDeDuct() {
		return claimAmountDeDuct;
	}
	public void setClaimAmountDeDuct(double claimAmountDeDuct) {
		this.claimAmountDeDuct = claimAmountDeDuct;
	}
	public Integer getClaimDayDeDuct() {
		return claimDayDeDuct;
	}
	public void setClaimDayDeDuct(Integer claimDayDeDuct) {
		this.claimDayDeDuct = claimDayDeDuct;
	}
	
}
