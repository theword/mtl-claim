/*
 * 
 */
package com.mtl.continuity.model.mtlclaim;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class ClaimRemain.
 */
@XmlRootElement(name = "ClaimRemain")
@XmlAccessorType(XmlAccessType.FIELD)
public class ClaimRemain implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8966919120258184170L;
	
	/** The has remain. */
	@XmlElement(name = "hasRemain", required = false)
	private boolean hasRemain;
	
	/** The has day. */
	@XmlElement(name = "hasDay", required = false)
	private boolean hasDay;
	
	/** The has amount. */
	@XmlElement(name = "hasAmount", required = false)
	private boolean hasAmount;
	
	/** The day remain. */
	@XmlElement(name = "dayRemain", required = false)
	private int dayRemain;
	
	/** The amount remain. */
	@XmlElement(name = "amountRemain", required = false)
	private double amountRemain;
	
	/** The map index. */
	@XmlElement(name = "mapIndex", required = false)
	private int mapIndex;
	
	/** The is own split. */
	@XmlElement(name = "isOwnSplit", required = false)
	private String isOwnSplit;
	
	/** The from icu. */
	@XmlElement(name = "fromICU", required = false)
	private boolean fromICU;
	
	/** The transfer flag. */
	@XmlElement(name = "transferFlag", required = false)
	private boolean transferFlag;
	
	/** The remain type. */
	@XmlElement(name = "remainType", required = false)
	private char remainType;//D = Day,A = Amount
	//initial value
	
	/**
	 * Instantiates a new claim remain.
	 */
	public ClaimRemain(){
		hasRemain 	 = false;
		hasDay	  	 = false;
		hasAmount 	 = false;
		dayRemain 	 = 0;
		amountRemain = 0.00;
		mapIndex 	 = 0;
		isOwnSplit 	 = "T";
		fromICU		 = false;
		transferFlag = false;
		remainType	 = 'D';
	}
	
	/**
	 * Sets the reset claim remain.
	 */
	public void setResetClaimRemain(){
		hasRemain 	 = false;
		hasDay	  	 = false;
		hasAmount 	 = false;
		dayRemain 	 = 0;
		amountRemain = 0.00;
		mapIndex 	 = 0;
		isOwnSplit 	 = "T";
		fromICU		 = false;
		transferFlag = false;
		remainType	 = 'D';
	}

	//method
	
	
	
	
	//get set method
	/**
	 * Checks if is checks for remain.
	 *
	 * @return true, if is checks for remain
	 */
	public boolean isHasRemain() {
		return hasRemain;
	}

	/**
	 * Sets the checks for remain.
	 *
	 * @param hasRemain the new checks for remain
	 */
	public void setHasRemain(boolean hasRemain) {
		this.hasRemain = hasRemain;
	}

	/**
	 * Checks if is checks for day.
	 *
	 * @return true, if is checks for day
	 */
	public boolean isHasDay() {
		return hasDay;
	}

	/**
	 * Sets the checks for day.
	 *
	 * @param hasDay the new checks for day
	 */
	public void setHasDay(boolean hasDay) {
		this.hasDay = hasDay;
	}

	/**
	 * Checks if is checks for amount.
	 *
	 * @return true, if is checks for amount
	 */
	public boolean isHasAmount() {
		return hasAmount;
	}

	/**
	 * Sets the checks for amount.
	 *
	 * @param hasAmount the new checks for amount
	 */
	public void setHasAmount(boolean hasAmount) {
		this.hasAmount = hasAmount;
	}

	/**
	 * Gets the day remain.
	 *
	 * @return the day remain
	 */
	public int getDayRemain() {
		return dayRemain;
	}

	/**
	 * Sets the day remain.
	 *
	 * @param dayRemain the new day remain
	 */
	public void setDayRemain(int dayRemain) {
		this.dayRemain = dayRemain;
	}

	/**
	 * Gets the amount remain.
	 *
	 * @return the amount remain
	 */
	public double getAmountRemain() {
		return amountRemain;
	}

	/**
	 * Sets the amount remain.
	 *
	 * @param amountRemain the new amount remain
	 */
	public void setAmountRemain(double amountRemain) {
		this.amountRemain = amountRemain;
	}

	/**
	 * Gets the map index.
	 *
	 * @return the map index
	 */
	public int getMapIndex() {
		return mapIndex;
	}

	/**
	 * Sets the map index.
	 *
	 * @param mapIndex the new map index
	 */
	public void setMapIndex(int mapIndex) {
		this.mapIndex = mapIndex;
	}

	/**
	 * Gets the checks if is own split.
	 *
	 * @return the checks if is own split
	 */
	public String getIsOwnSplit() {
		return isOwnSplit;
	}

	/**
	 * Sets the checks if is own split.
	 *
	 * @param isOwnSplit the new checks if is own split
	 */
	public void setIsOwnSplit(String isOwnSplit) {
		this.isOwnSplit = isOwnSplit;
	}

	/**
	 * Checks if is from icu.
	 *
	 * @return true, if is from icu
	 */
	public boolean isFromICU() {
		return fromICU;
	}

	/**
	 * Sets the from icu.
	 *
	 * @param fromICU the new from icu
	 */
	public void setFromICU(boolean fromICU) {
		this.fromICU = fromICU;
	}

	/**
	 * Checks if is transfer flag.
	 *
	 * @return true, if is transfer flag
	 */
	public boolean isTransferFlag() {
		return transferFlag;
	}

	/**
	 * Sets the transfer flag.
	 *
	 * @param transferFlag the new transfer flag
	 */
	public void setTransferFlag(boolean transferFlag) {
		this.transferFlag = transferFlag;
	}

	/**
	 * Gets the remain type.
	 *
	 * @return the remain type
	 */
	public char getRemainType() {
		return remainType;
	}

	/**
	 * Sets the remain type.
	 *
	 * @param remainType the new remain type
	 */
	public void setRemainType(char remainType) {
		this.remainType = remainType;
	}


}
