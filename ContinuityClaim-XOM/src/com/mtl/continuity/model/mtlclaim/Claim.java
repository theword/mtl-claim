/*
 * 
 */
package com.mtl.continuity.model.mtlclaim;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mtl.continuity.model.enumerator.ClaimType;
import com.mtl.continuity.model.mtlclaim.master.ClaimMs;
import com.mtl.continuity.model.templates.IMasterModel;
import com.mtl.continuity.model.templates.ITransactionModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Claim")
@XmlAccessorType(XmlAccessType.FIELD)
public class Claim implements ITransactionModel, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1106469135915611629L;
	
	@XmlElement(name = "claim", required = false)
	private ClaimMs claim;
	
	/** The policy number. */
	@XmlElement(name = "policyNumber", required = false)
	private String policyNumber;
	
	/** The coverage. */
	@XmlElement(name = "coverage", required = false)
	private Coverage coverage;
	
	/** The benefits. */
	@XmlElement(name = "benefits", required = false)
	private List<Benefit> benefits;
	
	/** The request diagnoses. */
	@XmlElement(name = "requestDiagnoses", required = false)
	private List<Diagnosis> requestDiagnoses;
	
	/** The reference claim group id. */
	@XmlElement(name = "referenceClaimGroupID", required = false)
	private String referenceClaimGroupID;
	
	/** The is active. */
	@XmlElement(name = "isActive", required = false)
	private boolean isActive;
	
	/** The current benefit index. */
	@XmlElement(name = "currentBenefitIndex", required = false)
	private int currentBenefitIndex;
	
	/** The used day. */
	@XmlElement(name = "usedDay", required = false)
	private int usedDay;

	//add By TeTe for BenefitAnalysisRule-1 25/06/2013
	/** The coverage priority. */
	@XmlElement(name = "coveragePriority", required = false)
	private int coveragePriority;//for order by coverage
	
	/** The sum coverage claim d duct. */
	@XmlElement(name = "sumCoverageClaimDDuct", required = false)
	private double sumCoverageClaimDDuct;
	
	/** The lumsum. */
	@XmlElement(name = "lumsum", required = false)
	private boolean lumsum;
	
	//shareDay continue for ICU & RB
	//set day ICU + RB limit 120
	/** The limit ic uand rb. */
	@XmlElement(name = "limitICUandRB", required = false)
	private int limitICUandRB;
	
	/** The max limit ic uand rb. */
	@XmlElement(name = "maxLimitICUandRB", required = false)
	private int maxLimitICUandRB;
	//for RB and ICU shareDay
	/** The continuity share day. */
	@XmlElement(name = "continuityShareDay", required = false)
	private int continuityShareDay;
	//for RS0 Special
	/** The is coverage special case r s0. */
	@XmlElement(name = "isCoverageSpecialCaseRS0", required = false)
	private boolean isCoverageSpecialCaseRS0;
	
	//case RB0
	/** The is rbo. */
	@XmlElement(name = "isRBO", required = false)
	private boolean isRBO;
	
	/** The max cover. */
	@XmlElement(name = "maxCover", required = false)
	private int maxCover;
	
	/** The max special. */
	@XmlElement(name = "maxSpecial", required = false)
	private int maxSpecial;
	
	/** The day cover. */
	@XmlElement(name = "dayCover", required = false)
	private int dayCover;
	
	/** The day special. */
	@XmlElement(name = "daySpecial", required = false)
	private int daySpecial;
	
	/** The transfer day. */
	@XmlElement(name = "transferDay", required = false)
	private List<Integer> transferDay;
	
	/** The IC uactive. */
	@XmlElement(name = "ICUactive", required = false)
	private boolean ICUactive;
	
	/** The no icu type. */
	@XmlElement(name = "noICUType", required = false)
	private boolean noICUType;
	
	/** The claim index. */
	@XmlElement(name = "claimIndex", required = false)
	private int claimIndex;
	
	/** The is rop. */
	@XmlElement(name = "isROP", required = false)
	private boolean isROP;
	
	/** The is rhl. */
	@XmlElement(name = "isRHL", required = false)
	private boolean isRHL;
	
	//for case claim HB-accident no calculate HB
	/** The is rbg. */
	@XmlElement(name = "isRBG", required = false)
	private boolean isRBG;
	
	/** RHM */
	@XmlElement(name = "isDiamondCare", required = false)
	private boolean isDiamondCare;
	
	@XmlElement(name = "healthReturnCash", required = false)
	private boolean healthReturnCash;
	
	@XmlElement(name = "maxHandicapDay", required = false)
	private int maxHandicapDay;
	@XmlElement(name = "claimHandicapDay", required = false)
	private int claimHandicapDay;
	
	@XmlElement(name = "maxRHLAmount", required = false)
	private double maxRHLAmount;
	@XmlElement(name = "availableRHLAmount", required = false)
	private double availableRHLAmount;
	
	@XmlElement(name = "maxYearRHLAmount", required = false)
	private double maxYearRHLAmount;
	@XmlElement(name = "availableYearRHLAmount", required = false)
	private double availableYearRHLAmount;
	
	@XmlElement(name = "handicapeCalculated", required = false)
	private boolean handicapeCalculated;
	@XmlElement(name = "calculatedMedicalExpense", required = false)
	private boolean calculatedMedicalExpense;
	
	/** for sum cal benefit de duct type before cal this coverage de duct type*/
	@XmlElement(name = "totalClaimDeDuct", required = false)
	private double totalClaimDeDuct;
	
	/** for sum de duct remain after subtract continuous claim*/
	@XmlElement(name = "totalRemainDeDuct", required = false)
	private double totalRemainDeDuct;
	/**
	 * Instantiates a new claim.
	 */
	public Claim() {
		activate();
		usedDay 					= 0;
		currentBenefitIndex 		= 0;
		sumCoverageClaimDDuct 		= 0.00;
		lumsum 						= false;
		continuityShareDay 			= 0;
		limitICUandRB 				= 0;
		maxLimitICUandRB 			= 0;
		isCoverageSpecialCaseRS0 	= false;
		isRBO 						= false;
		maxCover 					= 0;
		maxSpecial	 				= 0;
		dayCover 					= 0;
		daySpecial 					= 0;
		transferDay 				= new ArrayList<Integer>();
		claim 						= new ClaimMs();
		coverage 					= new Coverage();
		benefits 					= new ArrayList<Benefit>();
		requestDiagnoses 			= new ArrayList<Diagnosis>();
		ICUactive 					= true;
		noICUType 					= false;
		claimIndex					= -1;
		isROP	   					= false;
		isRHL						= false;
		isRBG						= false;
		healthReturnCash 			= false;
		maxHandicapDay				= 0;
		claimHandicapDay			= 0;
		handicapeCalculated			= false;
		calculatedMedicalExpense	= false;
		totalClaimDeDuct			= 0.0;
		totalRemainDeDuct			= 0.0;
		maxRHLAmount				= 0.0;
		availableRHLAmount			= 0.0;
		maxYearRHLAmount			= 0.0;
		availableYearRHLAmount		= 0.0;
		isDiamondCare				= false;
	}
	
	/**
	 * This constructor should be used in case of benefit analysis claim construction.
	 *
	 * @param claim master model of this Claim
	 * @param policyNumber chosen by user from the Assessment page
	 * @param coverage chosen by user from the Assessment page
	 * @param referenceClaimGroupID the reference claim group id
	 * @param benefits list of Benefits that belong to the coverageCode
	 * @param requestDiagnoses request diagnosis of this claim
	 */
	public Claim(ClaimMs claim, String policyNumber, Coverage coverage, 
			String referenceClaimGroupID, List<Benefit> benefits, List<Diagnosis> requestDiagnoses) {
		this();
		setMasterModel(claim);
		setPolicyNumber(policyNumber);
		setCoverage(coverage);
		setReferenceClaimGroupID(referenceClaimGroupID);
		setBenefits(benefits);
		setRequestDiagnoses(requestDiagnoses);
		setSumCoverageClaimDDuct(0.00);
	}
	
	/**
	 * Reset to cal.
	 */
	public void resetToCal(){
		sumCoverageClaimDDuct = 0.00;
		claimHandicapDay	  = 0;
		limitICUandRB = maxLimitICUandRB - continuityShareDay;
		dayCover = maxCover;
		daySpecial = maxSpecial;
		transferDay = new ArrayList<Integer>();
		ICUactive = true;
		handicapeCalculated = false;
		calculatedMedicalExpense = false;
		availableRHLAmount = maxRHLAmount;
		availableYearRHLAmount = maxYearRHLAmount;
		coverage.setMaxAvailableAmount(coverage.getMaxAmount());
		coverage.setAvailableGroupSumBenefitAmount(coverage.getMaxGroupSumBenefitAmount());
		for(Benefit benefit:benefits){
			benefit.resetToCal();
		}
	}
	//add by TeTe for use in Rule=Claim-BenefitAnalysis-1 25/06/2013
	
		//return benefits at benefitIndex
		/**
		 * Gets the current benefit.
		 *
		 * @return the current benefit
		 */
		public Benefit getCurrentBenefit(){
			if(benefits != null && currentBenefitIndex < benefits.size() && benefits.get(currentBenefitIndex) != null)
				return benefits.get(currentBenefitIndex);
			return null;
		}
		//check null of benefits at benefitIndex
		/**
		 * Checks for current benefit.
		 *
		 * @return true, if successful
		 */
		public boolean hasCurrentBenefit(){
			if(benefits != null && currentBenefitIndex < benefits.size() && benefits.get(currentBenefitIndex) != null)
				return true;
			return false;
		}
		//set benefit at benefitIndex with value from Rule-Claim-BenefitAnalysis-1
		/**
		 * Sets the benefit.
		 *
		 * @param benefit the new benefit
		 */
		public void setBenefit(Benefit benefit){
			benefits.set(currentBenefitIndex, benefit);
		}
		
		/**
		 * Next benefit.
		 */
		public void nextBenefit(){
			currentBenefitIndex++;
		}
		
		/**
		 * Adds the sum coverage claim d duct.
		 *
		 * @param claimAmount the claim amount
		 */
		public void addSumCoverageClaimDDuct(double claimAmount){
			sumCoverageClaimDDuct = sumCoverageClaimDDuct + claimAmount;
		}
		
		/**
		 * Adds the continuity share day.
		 *
		 * @param continuityDay the continuity day
		 */
		public void addContinuityShareDay(int continuityDay){
			continuityShareDay = continuityShareDay + continuityDay;
		}
		
		/**
		 * Calculate share day ic uand rb.
		 *
		 * @param claimDay the claim day
		 */
		public void calculateShareDayICUandRB(int claimDay){
			limitICUandRB = limitICUandRB - claimDay;
		}
		
		/**
		 * Gets the total continuity claim day.
		 *
		 * @return the total continuity claim day
		 */
		public int getTotalContinuityClaimDay(){
			int total = 0;
			if(benefits != null && benefits.size() > 0){
				for(Benefit benefit:benefits){
					total += benefit.getContinuityClaimDay();
				}
			}
			return total;
		}
		
		public double getTotalContinuityClaimCaseDisabled(){
			double total = 0;
			if(benefits != null && benefits.size() > 0){
				for(Benefit benefit:benefits){
					if(benefit.isDisabledCase())
						total += benefit.getContinuityClaimAmount();
				}
			}
			return total;
		}
		
		public double getTotalContinuityRHL(){
			double total = 0;
			if(benefits != null && benefits.size() > 0){
				for(Benefit benefit:benefits){
					if(benefit.isRhlFlag())
						total += benefit.getContinuityClaimAmount();
				}
			}
			return total;
		}
		
		/**
		 * Gets the total continuity claim day sender.
		 *
		 * @return the total continuity claim day sender
		 */
		public int getTotalContinuityClaimDaySender(){
			int total = 0;
			if(benefits != null && benefits.size() > 0){
				for(Benefit benefit:benefits){
					if(benefit.isSenderRBO())
						total += benefit.getContinuityClaimDay();
				}
			}
			return total;
		}
	//end add by TeTe for use in Rule=Claim-BenefitAnalysis-1
	
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#getMasterModel()
	 */
	@Override
	public ClaimMs getMasterModel() {
		return claim;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#setMasterModel(com.mtl.model.templates.IMasterModel)
	 */
	@Override
	public void setMasterModel(IMasterModel masterModel) {
		claim = (ClaimMs)masterModel;
	}
	
	/**
	 * Sets the reference claim group id.
	 *
	 * @param referenceClaimGroupID the new reference claim group id
	 */
	public void setReferenceClaimGroupID(String referenceClaimGroupID) {
		this.referenceClaimGroupID = referenceClaimGroupID;
	}

	/**
	 * Gets the reference claim group id.
	 *
	 * @return the reference claim group id
	 */
	public String getReferenceClaimGroupID() {
		return referenceClaimGroupID;
	}
	
	/**
	 * Sets the request diagnoses.
	 *
	 * @param requestDiagnoses the new request diagnoses
	 */
	public void setRequestDiagnoses(List<Diagnosis> requestDiagnoses) {
		this.requestDiagnoses = requestDiagnoses;
	}

	/**
	 * Gets the request diagnoses.
	 *
	 * @return the request diagnoses
	 */
	public List<Diagnosis> getRequestDiagnoses() {
		return requestDiagnoses;
	}
	
	/**
	 * Sets the policy number.
	 *
	 * @param policyNumber the new policy number
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * Gets the policy number.
	 *
	 * @return the policy number
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}
	
	/**
	 * Sets the benefits.
	 *
	 * @param benefits the new benefits
	 */
	public void setBenefits(List<Benefit> benefits) {
		this.benefits = benefits;
	}

	/**
	 * Gets the benefits.
	 *
	 * @return the benefits
	 */
	public List<Benefit> getBenefits() {
		return benefits;
	}
	
	/**
	 * Gets the claim id.
	 *
	 * @return the claim id
	 */
	public String getClaimID(){
		return claim.getCode();
	}
	
	/**
	 * Gets the claim type.
	 *
	 * @return the claim type
	 */
	public ClaimType getClaimType(){
		return claim.getClaimType();
	}
	
	/**
	 * Gets the incident date.
	 *
	 * @return the incident date
	 */
	public Date getIncidentDate(){
		return claim.getIncidentDate();
	}
	
	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate(){
		return claim.getAdmissionDate();
	}
	
	/**
	 * Gets the discharge date.
	 *
	 * @return the discharge date
	 */
	public Date getDischargeDate(){
		return claim.getDischargeDate();
	}

	//Rules used methods
	
	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	private void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}
	
	/**
	 * Activate.
	 */
	public void activate(){
		this.setActive(true);
	}
	
	/**
	 * Deactivate.
	 */
	public void deactivate(){
		this.setActive(false);
	}
	
	/**
	 * Checks for next fixed benefit.
	 *
	 * @return true, if successful
	 */
	public boolean hasNextFixedBenefit(){
		if(benefits == null){
			return false;
		}
		while(currentBenefitIndex < benefits.size()){
			if(!benefits.get(currentBenefitIndex).isFixed() ||
					benefits.get(currentBenefitIndex).isFixedCalculated()){
				currentBenefitIndex++;
				continue;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks for next benefit.
	 *
	 * @return true, if successful
	 */
	public boolean hasNextBenefit(){
		if(benefits == null){
			return false;
		}
		while(currentBenefitIndex < benefits.size()){
			if(benefits.get(currentBenefitIndex).isFixed()){
				currentBenefitIndex++;
				continue;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the total continuity amount d duct.
	 *
	 * @return the total continuity amount d duct
	 */
	public double getTotalContinuityAmountDDuct(){
		double total = 0.00;
		if(benefits != null && benefits.size()>0){
			for(Benefit benefit:benefits){
				if(benefit.isDDuctType()){
					total += benefit.getContinuityClaimAmount();
				}
			}
		}
		return total;
	}
	
	public double getTotalContinuityDDuct(){
		double total = 0.00;
		if(benefits != null && benefits.size()>0){
			for(Benefit benefit:benefits){
				if(benefit.isDDuctType()){
					total += benefit.getContinuityDeDuctAmount();
				}
			}
		}
		return total;
	}
	
	/**
	 * Sets the current benefit.
	 *
	 * @param currentBenefit the new current benefit
	 */
	public void setCurrentBenefit(Benefit currentBenefit){
		benefits.set(currentBenefitIndex-1, currentBenefit);
	}
	
	/**
	 * Gets the next benefit.
	 *
	 * @return the next benefit
	 */
	public Benefit getNextBenefit(){
		return benefits.get(currentBenefitIndex++);
	}
	
	/**
	 * Reset benefit index.
	 */
	public void resetBenefitIndex(){
		currentBenefitIndex = 0;
	}
	
	/**
	 * Gets the used day.
	 *
	 * @return the used day
	 */
	public int getUsedDay(){
		return usedDay;
	}
	
	/**
	 * Adds the used day.
	 *
	 * @param daySpent the day spent
	 */
	public void addUsedDay(int daySpent){
		usedDay += daySpent;
	}
	
	public double getBilledDeDuct(){
		double result = 0.00;
		if(benefits != null && benefits.size()>0){
			for(Benefit benefit:benefits){
				if(benefit.isDDuctType())
					for(DeDuct deDuct:benefit.getDeDuctList())result = result + deDuct.getBilledDeDuct();
			}
		}
		return result;
	}
	
	public int totalContinuityHandicappedShareDay(){
		int sum = 0;
		if(benefits != null){
			for(Benefit benefit:benefits){
				if(benefit.isHandicappedShareDay()){
					sum = sum + benefit.getContinuityClaimDay();
				}
			}
		}
		return sum;
	}
	
	/**
	 * Gets the first diagnosis.
	 *
	 * @return the first diagnosis
	 */
	public Diagnosis getFirstDiagnosis(){
		return requestDiagnoses.get(0);
	}

	/**
	 * Sets the coverage.
	 *
	 * @param coverage the new coverage
	 */
	public void setCoverage(Coverage coverage) {
		this.coverage = coverage;
	}

	/**
	 * Gets the coverage.
	 *
	 * @return the coverage
	 */
	public Coverage getCoverage() {
		return coverage;
	}

	/**
	 * Gets the coverage priority.
	 *
	 * @return the coverage priority
	 */
	public int getCoveragePriority() {
		return coveragePriority;
	}

	/**
	 * Sets the coverage priority.
	 *
	 * @param coveragePriority the new coverage priority
	 */
	public void setCoveragePriority(int coveragePriority) {
		this.coveragePriority = coveragePriority;
	}

	/**
	 * Sets the current benefit index.
	 *
	 * @param currentBenefitIndex the new current benefit index
	 */
	public void setCurrentBenefitIndex(int currentBenefitIndex) {
		this.currentBenefitIndex = currentBenefitIndex;
	}
	
	/**
	 * Gets the current benefit index.
	 *
	 * @return the current benefit index
	 */
	public int getCurrentBenefitIndex(){
		return currentBenefitIndex;
	}
	//End rules used methods

	/**
	 * Gets the sum coverage claim d duct.
	 *
	 * @return the sum coverage claim d duct
	 */
	public double getSumCoverageClaimDDuct() {
		return sumCoverageClaimDDuct;
	}

	/**
	 * Sets the sum coverage claim d duct.
	 *
	 * @param sumCoverageClaimDDuct the new sum coverage claim d duct
	 */
	public void setSumCoverageClaimDDuct(double sumCoverageClaimDDuct) {
		this.sumCoverageClaimDDuct = sumCoverageClaimDDuct;
	}

	/**
	 * Checks if is lumsum.
	 *
	 * @return true, if is lumsum
	 */
	public boolean isLumsum() {
		return lumsum;
	}

	/**
	 * Sets the lumsum.
	 *
	 * @param lumsum the new lumsum
	 */
	public void setLumsum(boolean lumsum) {
		this.lumsum = lumsum;
	}

	/**
	 * Gets the limit ic uand rb.
	 *
	 * @return the limit ic uand rb
	 */
	public int getLimitICUandRB() {
		return limitICUandRB;
	}

	/**
	 * Sets the limit ic uand rb.
	 *
	 * @param limitICUandRB the new limit ic uand rb
	 */
	public void setLimitICUandRB(int limitICUandRB) {
		this.limitICUandRB = limitICUandRB;
	}

	/**
	 * Gets the continuity share day.
	 *
	 * @return the continuity share day
	 */
	public int getContinuityShareDay() {
		return continuityShareDay;
	}

	/**
	 * Sets the continuity share day.
	 *
	 * @param continuityShareDay the new continuity share day
	 */
	public void setContinuityShareDay(int continuityShareDay) {
		this.continuityShareDay = continuityShareDay;
	}

	/**
	 * Gets the max limit ic uand rb.
	 *
	 * @return the max limit ic uand rb
	 */
	public int getMaxLimitICUandRB() {
		return maxLimitICUandRB;
	}

	/**
	 * Sets the max limit ic uand rb.
	 *
	 * @param maxLimitICUandRB the new max limit ic uand rb
	 */
	public void setMaxLimitICUandRB(int maxLimitICUandRB) {
		this.maxLimitICUandRB = maxLimitICUandRB;
	}

	/**
	 * Checks if is coverage special case r s0.
	 *
	 * @return true, if is coverage special case r s0
	 */
	public boolean isCoverageSpecialCaseRS0() {
		return isCoverageSpecialCaseRS0;
	}

	/**
	 * Sets the coverage special case r s0.
	 *
	 * @param isCoverageSpecialCaseRS0 the new coverage special case r s0
	 */
	public void setCoverageSpecialCaseRS0(boolean isCoverageSpecialCaseRS0) {
		this.isCoverageSpecialCaseRS0 = isCoverageSpecialCaseRS0;
	}

	/**
	 * Checks if is rbo.
	 *
	 * @return true, if is rbo
	 */
	public boolean isRBO() {
		return isRBO;
	}

	/**
	 * Sets the rbo.
	 *
	 * @param isRBO the new rbo
	 */
	public void setRBO(boolean isRBO) {
		this.isRBO = isRBO;
	}

	/**
	 * Gets the max cover.
	 *
	 * @return the max cover
	 */
	public int getMaxCover() {
		return maxCover;
	}

	/**
	 * Sets the max cover.
	 *
	 * @param maxCover the new max cover
	 */
	public void setMaxCover(int maxCover) {
		this.maxCover = maxCover;
	}

	/**
	 * Gets the max special.
	 *
	 * @return the max special
	 */
	public int getMaxSpecial() {
		return maxSpecial;
	}

	/**
	 * Sets the max special.
	 *
	 * @param maxSpecial the new max special
	 */
	public void setMaxSpecial(int maxSpecial) {
		this.maxSpecial = maxSpecial;
	}

	/**
	 * Gets the day cover.
	 *
	 * @return the day cover
	 */
	public int getDayCover() {
		return dayCover;
	}

	/**
	 * Sets the day cover.
	 *
	 * @param dayCover the new day cover
	 */
	public void setDayCover(int dayCover) {
		if(dayCover<0)dayCover=0;
		this.dayCover = dayCover;
	}

	/**
	 * Gets the day special.
	 *
	 * @return the day special
	 */
	public int getDaySpecial() {
		return daySpecial;
	}

	/**
	 * Sets the day special.
	 *
	 * @param daySpecial the new day special
	 */
	public void setDaySpecial(int daySpecial) {
		if(daySpecial<0)daySpecial=0;
		this.daySpecial = daySpecial;
	}

	/**
	 * Gets the transfer day.
	 *
	 * @return the transfer day
	 */
	public List<Integer> getTransferDay() {
		return transferDay;
	}

	/**
	 * Sets the transfer day.
	 *
	 * @param transferDay the new transfer day
	 */
	public void setTransferDay(List<Integer> transferDay) {
		this.transferDay = transferDay;
	}
	
	/**
	 * Check transfer day.
	 *
	 * @return true, if successful
	 */
	public boolean checkTransferDay(){
		if(transferDay != null && transferDay.size() > 0)
			return true;
		return false;
	}
	
	/**
	 * Adds the transfer day.
	 *
	 * @param transferDay the transfer day
	 */
	public void addTransferDay(int transferDay) {
		if(this.transferDay == null)
			this.transferDay = new ArrayList<Integer>();
		this.transferDay.add(transferDay);
	}

	/**
	 * Checks if is iC uactive.
	 *
	 * @return true, if is iC uactive
	 */
	public boolean isICUactive() {
		return ICUactive;
	}

	/**
	 * Sets the iC uactive.
	 *
	 * @param iCUactive the new iC uactive
	 */
	public void setICUactive(boolean iCUactive) {
		ICUactive = iCUactive;
	}

	/**
	 * Checks if is no icu type.
	 *
	 * @return true, if is no icu type
	 */
	public boolean isNoICUType() {
		return noICUType;
	}

	/**
	 * Sets the no icu type.
	 *
	 * @param noICUType the new no icu type
	 */
	public void setNoICUType(boolean noICUType) {
		this.noICUType = noICUType;
	}

	/**
	 * Gets the claim index.
	 *
	 * @return the claim index
	 */
	public int getClaimIndex() {
		return claimIndex;
	}

	/**
	 * Sets the claim index.
	 *
	 * @param claimIndex the new claim index
	 */
	public void setClaimIndex(int claimIndex) {
		this.claimIndex = claimIndex;
	}

	/**
	 * Checks if is rop.
	 *
	 * @return true, if is rop
	 */
	public boolean isROP() {
		return isROP;
	}

	/**
	 * Sets the rop.
	 *
	 * @param isROP the new rop
	 */
	public void setROP(boolean isROP) {
		this.isROP = isROP;
	}

	/**
	 * Checks if is rhl.
	 *
	 * @return true, if is rhl
	 */
	public boolean isRHL() {
		return isRHL;
	}

	/**
	 * Sets the rhl.
	 *
	 * @param isRHL the new rhl
	 */
	public void setRHL(boolean isRHL) {
		this.isRHL = isRHL;
	}

	/**
	 * Checks if is rbh.
	 *
	 * @return true, if is rbh
	 */
	public boolean isRBG() {
		return isRBG;
	}

	/**
	 * Sets the rbh.
	 *
	 * @param isRBH the new rbh
	 */
	public void setRBG(boolean isRBG) {
		this.isRBG = isRBG;
	}

	public boolean isHealthReturnCash() {
		return healthReturnCash;
	}

	public void setHealthReturnCash(boolean healthReturnCash) {
		this.healthReturnCash = healthReturnCash;
	}

	public int getMaxHandicapDay() {
		return maxHandicapDay;
	}

	public void setMaxHandicapDay(int maxHandicapDay) {
		this.maxHandicapDay = maxHandicapDay;
	}

	public int getClaimHandicapDay() {
		return claimHandicapDay;
	}

	public void setClaimHandicapDay(int claimHandicapDay) {
		this.claimHandicapDay = claimHandicapDay;
	}

	public boolean isHandicapeCalculated() {
		return handicapeCalculated;
	}

	public void setHandicapeCalculated(boolean handicapeCalculated) {
		this.handicapeCalculated = handicapeCalculated;
	}

	public boolean isCalculatedMedicalExpense() {
		return calculatedMedicalExpense;
	}

	public void setCalculatedMedicalExpense(boolean calculatedMedicalExpense) {
		this.calculatedMedicalExpense = calculatedMedicalExpense;
	}
	
	public void orderBenefit() {
	    if(benefits != null && benefits.size() > 0){
		    int size = benefits.size();
		    quicksort(0, size-1);
		    int i = 0;
		    for(Benefit benefit:benefits){
		    	benefit.setBenefitIndex(i);
		    	i++;
		    }
	    }
	 }
	
	private void quicksort(int low, int high) {
		   int i = low, j = high;
		   
		   int pivot = benefits.get(low + (high-low)/2).getBenefitPriority();
		   while(i <= j){
			   while(benefits.get(i).getBenefitPriority() < pivot){
				   i++;
			   }
			   while(benefits.get(j).getBenefitPriority() > pivot){
				   j--;
			   }
			   if(i <= j){
				   swap(i, j);
				   i++;
				   j--;
			   }//if(i <= j)
		   }//while(i <= j)
		   
		   if (low < j) quicksort(low, j);
		   if (i < high)quicksort(i, high);
		 }
		 //swap currentClaim
		private void swap(int i, int j) {
		    Benefit temp = benefits.get(i);
		    benefits.set(i, benefits.get(j));
		    benefits.set(j, temp);
		 }

		public double getTotalClaimDeDuct() {
			return totalClaimDeDuct;
		}

		public void setTotalClaimDeDuct(double totalClaimDeDuct) {
			this.totalClaimDeDuct = totalClaimDeDuct;
		}

		public double getTotalRemainDeDuct() {
			return totalRemainDeDuct;
		}

		public void setTotalRemainDeDuct(double totalRemainDeDuct) {
			this.totalRemainDeDuct = totalRemainDeDuct;
		}

		public double getMaxRHLAmount() {
			return maxRHLAmount;
		}

		public void setMaxRHLAmount(double maxRHLAmount) {
			if(maxRHLAmount<0.0)
				maxRHLAmount = 0.0;
			this.maxRHLAmount = maxRHLAmount;
		}

		public double getAvailableRHLAmount() {
			return availableRHLAmount;
		}

		public void setAvailableRHLAmount(double availableRHLAmount) {
			if(availableRHLAmount<0.0)
				availableRHLAmount = 0.0;
			this.availableRHLAmount = availableRHLAmount;
		}

		public boolean isDiamondCare() {
			return isDiamondCare;
		}

		public void setDiamondCare(boolean isDiamondCare) {
			this.isDiamondCare = isDiamondCare;
		}

		public double getMaxYearRHLAmount() {
			return maxYearRHLAmount;
		}

		public void setMaxYearRHLAmount(double maxYearRHLAmount) {
			if(maxYearRHLAmount<0.0)
				maxYearRHLAmount = 0.0;
			this.maxYearRHLAmount = maxYearRHLAmount;
		}

		public double getAvailableYearRHLAmount() {
			return availableYearRHLAmount;
		}

		public void setAvailableYearRHLAmount(double availableYearRHLAmount) {
			if(availableYearRHLAmount<0.0)
				availableYearRHLAmount = 0.0;
			this.availableYearRHLAmount = availableYearRHLAmount;
		}
	
}
