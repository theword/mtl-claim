/*
 * 
 */
package com.mtl.continuity.model.xom.mtlclaim;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.continuity.model.RuleMessage;
import com.mtl.continuity.model.RuleModel;
import com.mtl.continuity.model.enumerator.ClaimType;
import com.mtl.continuity.model.mtlclaim.Claim;
import com.mtl.continuity.model.mtlclaim.Diagnosis;
import com.mtl.continuity.util.DateTimeAdapter;
import com.mtl.continuity.util.ModelUtility;


/**
 * The Class ContinuityClaimCheckingModel.
 */
@XmlRootElement(name = "ContinuityClaimCheckingModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContinuityClaimCheckingModel extends RuleModel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6302099543167181907L;
	
	/** The request diagnoses. */
	@XmlElement(name = "requestDiagnoses", required = false)
	private List<Diagnosis> requestDiagnoses;
	
	@XmlElement(name = "currentIndexRequestDiagnosisCode", required = false)
	private int currentIndexRequestDiagnosisCode;
	
	/** The claim type. */
	@XmlElement(name = "claimType", required = false)
	private ClaimType claimType;
	
	/** The admission date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "admissionDate", required = false)
	private Date admissionDate;
	
	/** The incident date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "incidentDate", required = false)
	private Date incidentDate;
	
	/** The discharge date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "dischargeDate", required = false)
	private Date dischargeDate;
	
	/** The related claims. */
	@XmlElement(name = "relatedClaims", required = false)
	private List<Claim> relatedClaims;
	
	/** The possible continuity claims. */
	@XmlElement(name = "possibleContinuityClaims", required = false)
	private List<Claim> possibleContinuityClaims;
	
	@XmlElement(name = "possibleContinuityClaimList", required = false)
	private List<String> possibleContinuityClaimList;
	
	private int status = 0;
	
	/**
	 * Instantiates a new continuity claim checking model.
	 */
	private ContinuityClaimCheckingModel() {
		super();
		relatedClaims = new ArrayList<Claim>();
		possibleContinuityClaims = new ArrayList<Claim>();
		possibleContinuityClaimList = new ArrayList<String>();
		currentIndexRequestDiagnosisCode = 0;
	}
	
	/**
	 * Instantiates a new continuity claim checking model.
	 *
	 * @param requestDiagnoses the request diagnoses
	 * @param relatedClaims the related claims
	 * @param claimType the claim type
	 * @param admissionDate the admission date
	 * @param incidentDate the incident date
	 * @param dischargeDate the discharge date
	 */
	public ContinuityClaimCheckingModel(List<Diagnosis> requestDiagnoses,  List<Claim> relatedClaims,
			ClaimType claimType, Date admissionDate, Date incidentDate, Date dischargeDate) {
		this();
		setRequestDiagnoses(requestDiagnoses);
		setRelatedClaims(relatedClaims);
		setClaimType(claimType);
		setAdmissionDate(admissionDate);
		setIncidentDate(incidentDate);
		setDischargeDate(dischargeDate);
	}

	/**
	 * Sets the request diagnoses.
	 *
	 * @param requestDiagnoses the new request diagnoses
	 */
	public void setRequestDiagnoses(List<Diagnosis> requestDiagnoses) {
		this.requestDiagnoses = requestDiagnoses;
	}

	/**
	 * Gets the request diagnoses.
	 *
	 * @return the request diagnoses
	 */
	public List<Diagnosis> getRequestDiagnoses() {
		return requestDiagnoses;
	}
	
	/**
	 * Gets the first diagnosis.
	 *
	 * @return the first diagnosis
	 */
	public Diagnosis getFirstDiagnosis(){
		return requestDiagnoses.get(0);
	}
	
	/**
	 * Sets the admission date.
	 *
	 * @param admissionDate the new admission date
	 */
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate() {
		return admissionDate;
	}

	/**
	 * Sets the incident date.
	 *
	 * @param incidentDate the new incident date
	 */
	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	 * Gets the incident date.
	 *
	 * @return the incident date
	 */
	public Date getIncidentDate() {
		return incidentDate;
	}

	/**
	 * Sets the discharge date.
	 *
	 * @param dischargeDate the new discharge date
	 */
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	/**
	 * Gets the discharge date.
	 *
	 * @return the discharge date
	 */
	public Date getDischargeDate() {
		return dischargeDate;
	}

	/**
	 * Sets the claim type.
	 *
	 * @param claimType the new claim type
	 */
	public void setClaimType(ClaimType claimType) {
		this.claimType = claimType;
	}

	/**
	 * Gets the claim type.
	 *
	 * @return the claim type
	 */
	public ClaimType getClaimType() {
		return claimType;
	}

	/**
	 * Sets the related claims.
	 *
	 * @param relatedClaims the new related claims
	 */
	public void setRelatedClaims(List<Claim> relatedClaims) {
		this.relatedClaims = relatedClaims;
	}

	/**
	 * Gets the related claims.
	 *
	 * @return the related claims
	 */
	public List<Claim> getRelatedClaims() {
		return relatedClaims;
	}
	
	//Local Methods
	
	/**
	 * Gets the possible continuity claim size.
	 *
	 * @return the possible continuity claim size
	 */
	public int getPossibleContinuityClaimSize(){
		return possibleContinuityClaims.size();
	}
	
	/**
	 * Gets the possible continuity claims.
	 *
	 * @return the possible continuity claims
	 */
	public List<Claim> getPossibleContinuityClaims(){
		return possibleContinuityClaims;
	}
	
	/**
	 * Adds the possible continuity claim.
	 *
	 * @param continuityClaim the continuity claim
	 */
	public void addPossibleContinuityClaim(Claim continuityClaim){
		if(possibleContinuityClaims.isEmpty()){
			System.out.println("1st Add Possible Continuity Claim - Claim ID: " + continuityClaim.getClaimID());
			possibleContinuityClaims.add(continuityClaim);
		}else{
			if(getIncidentDate() != null && continuityClaim.getIncidentDate() != null){
				if(getIncidentDate().compareTo(continuityClaim.getIncidentDate()) == 0){
					possibleContinuityClaims.add(continuityClaim);
					System.out.println("Add Possible Continuity Claim IncidentDate - Claim ID: " + continuityClaim.getClaimID());
					return;
				}
			}
			int index = 0;
			while(index < possibleContinuityClaims.size()){
				Claim claim = possibleContinuityClaims.get(index);
				if(claim.getDischargeDate() != null && getAdmissionDate() != null
					&& continuityClaim.getAdmissionDate() != null && getDischargeDate() != null)
				{
					if(continuityClaim.getPolicyNumber().equals(claim.getPolicyNumber()) && 
					   continuityClaim.getCoverage().getCoverageCode().equals(claim.getCoverage().getCoverageCode()) &&
					   continuityClaim.getCoverage().getTrailerNumber().equals(claim.getCoverage().getTrailerNumber()))
					{
						int diffAdmissionDate 	= ModelUtility.differentDays(claim.getDischargeDate(), getAdmissionDate());
						int diffDischange		= ModelUtility.differentDays(continuityClaim.getAdmissionDate(), getDischargeDate());
						
						if(diffAdmissionDate > diffDischange){
							System.out.println("Replacing Add Possible Continuity Claim - Claim ID: " + continuityClaim.getClaimID());
							possibleContinuityClaims.remove(index);
							possibleContinuityClaims.add(continuityClaim);
						}
						return;
					}
				}
				index++;
			}//while
			System.out.println("Add Possible Continuity Claim - Claim ID: " + continuityClaim.getClaimID());
			possibleContinuityClaims.add(continuityClaim);
		}
	}
	
	/**
	 * Gets the all rule messages.
	 *
	 * @return the all rule messages
	 */
	public List<RuleMessage> getAllRuleMessages(){
		List<RuleMessage> ruleMessages = new ArrayList<RuleMessage>();
		ruleMessages.addAll(getRuleMesssages());
		return ruleMessages;
	}

	public List<String> getPossibleContinuityClaimList() {
		return possibleContinuityClaimList;
	}

	public void setPossibleContinuityClaimList(
			List<String> possibleContinuityClaimList) {
		this.possibleContinuityClaimList = possibleContinuityClaimList;
	}
	
	public String getCurrentRequestDiagnosisCode(){
		if(requestDiagnoses!=null&&requestDiagnoses.size()>currentIndexRequestDiagnosisCode){
			return requestDiagnoses.get(currentIndexRequestDiagnosisCode).getDiagnosisCode();
		}
		return null;
	}
	
	public boolean checkCurrentRequestDiagnosisCodeList(){
		if(requestDiagnoses!=null&&requestDiagnoses.size()>currentIndexRequestDiagnosisCode){
			return true;
		}
		return false;
	}
	
	public void nextIndexRequestDiagnosisCode(){
		currentIndexRequestDiagnosisCode = currentIndexRequestDiagnosisCode + 1;
	}
	
	public void resetIndexRequestDiagnosisCode(){
		currentIndexRequestDiagnosisCode = 0;
	}

	public int getCurrentIndexRequestDiagnosisCode() {
		return currentIndexRequestDiagnosisCode;
	}

	public void setCurrentIndexRequestDiagnosisCode(
			int currentIndexRequestDiagnosisCode) {
		this.currentIndexRequestDiagnosisCode = currentIndexRequestDiagnosisCode;
	}
	//End Local Methods

	public void setPossibleContinuityClaims(List<Claim> possibleContinuityClaims) {
		this.possibleContinuityClaims = possibleContinuityClaims;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
