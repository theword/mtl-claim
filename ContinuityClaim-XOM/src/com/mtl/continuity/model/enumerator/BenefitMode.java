/*
 * 
 */
package com.mtl.continuity.model.enumerator;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Enum BenefitMode.
 */
@XmlRootElement(name = "BenefitMode")
@XmlAccessorType(XmlAccessType.FIELD)
public enum BenefitMode {
	
	/** The amount. */
	AMOUNT, 
 /** The percent. */
 PERCENT, 
 /** The daily. */
 DAILY, 
 /** The weekly. */
 WEEKLY;
	
	/** The state map. */
	private static Map<String, BenefitMode> stateMap = new HashMap<String, BenefitMode>();
	
	static {
		stateMap.put("A", AMOUNT);
		stateMap.put("P", PERCENT);
		stateMap.put("D", DAILY);
		stateMap.put("W", WEEKLY);
	}
	
	/**
	 * Lookup.
	 *
	 * @param key the key
	 * @return the benefit mode
	 */
	public static BenefitMode lookup(String key){
		if(key == null)
			return AMOUNT;
		return stateMap.get(key);
	}
	
}
	