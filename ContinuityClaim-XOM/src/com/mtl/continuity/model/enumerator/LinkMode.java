/*
 * 
 */
package com.mtl.continuity.model.enumerator;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Enum LinkMode.
 */
@XmlRootElement(name = "LinkMode")
@XmlAccessorType(XmlAccessType.FIELD)
public enum LinkMode {
	
	/** The disability. */
	DISABILITY, 
 /** The yearly. */
 YEARLY;

	/** The state map. */
	private static Map<String, LinkMode> stateMap = new HashMap<String, LinkMode>();
	
	static {
		stateMap.put("1", DISABILITY);
		stateMap.put("2", YEARLY);
	}
	
	/**
	 * Lookup.
	 *
	 * @param key the key
	 * @return the link mode
	 */
	public static LinkMode lookup(String key){
		if(key == null)
			return DISABILITY;
		return stateMap.get(key);
	}
}
