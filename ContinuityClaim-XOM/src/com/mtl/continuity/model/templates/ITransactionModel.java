/*
 * 
 */
package com.mtl.continuity.model.templates;

/**
 * The Interface ITransactionModel.
 */
public interface ITransactionModel{
	
	/**
	 * Gets the master model.
	 *
	 * @return the master model
	 */
	public IMasterModel getMasterModel();

	/**
	 * Sets the master model.
	 *
	 * @param masterModel the new master model
	 */
	public void setMasterModel(IMasterModel masterModel);
	
}
