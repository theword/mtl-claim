/*
 * 
 */
package com.mtl.eligible.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.eligible.model.enumerator.ClaimSource;
import com.mtl.eligible.model.enumerator.ClaimType;
import com.mtl.eligible.model.mtlclaim.Diagnosis;
import com.mtl.eligible.util.DateTimeAdapter;

@XmlRootElement(name = "EligibleDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class EligibleDto  {
	
	@XmlElement(name = "clientID", required = false)
	private String clientID;
	
	@XmlElement(name = "policyNumber", required = false)
	private String policyNumber;
	
	@XmlElement(name = "requestDiagnoses", required = false)
	private List<Diagnosis> requestDiagnoses;
	
	@XmlElement(name = "claimType", required = false)
	private ClaimType claimType;
	
	@XmlElement(name = "claimSource", required = false)
	private ClaimSource claimSource;
	
	@XmlElement(name = "hospitalCode", required = false)
	private String hospitalCode;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "admissionDate", required = false)
	private Date admissionDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "incidentDate", required = false)
	private Date incidentDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "dischargeDate", required = false)
	private Date dischargeDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "disabilityDate", required = false)
	private Date disabilityDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "deathDate", required = false)
	private Date deathDate;
	
	@XmlElement(name = "causeStatusCode", required = false)
	private String causeStatusCode;
	
	@XmlElement(name = "causeStatusSubCode", required = false)
	private String causeStatusSubCode;
	
	@XmlElement(name = "dayCaseStatus", required = false)
	private char dayCaseStatus; 
	
	@XmlElement(name = "claimCause", required = false)
	private String claimCause;

	@XmlElement(name = "codeICD9", required = false)
	private String codeICD9;
	
	@XmlElement(name = "dotorLicense", required = false)
	private String dotorLicense;
	/**
	 * Instantiates a new eligible dto.
	 */
	public EligibleDto(){
		
	}
	
	/**
	 * Sets the client id.
	 *
	 * @param clientID the new client id
	 */
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public String getClientID() {
		return clientID;
	}
	
	/**
	 * Gets the request diagnoses.
	 *
	 * @return the request diagnoses
	 */
	public List<Diagnosis> getRequestDiagnoses() {
		return requestDiagnoses;
	}
	
	/**
	 * Sets the request diagnoses.
	 *
	 * @param requestDiagnoses the new request diagnoses
	 */
	public void setRequestDiagnoses(List<Diagnosis> requestDiagnoses) {
		this.requestDiagnoses = requestDiagnoses;
	}
	
	/**
	 * Gets the claim type.
	 *
	 * @return the claim type
	 */
	public ClaimType getClaimType() {
		return claimType;
	}
	
	/**
	 * Sets the claim type.
	 *
	 * @param claimType the new claim type
	 */
	public void setClaimType(ClaimType claimType) {
		this.claimType = claimType;
	}
	
	/**
	 * Gets the claim source.
	 *
	 * @return the claim source
	 */
	public ClaimSource getClaimSource() {
		return claimSource;
	}
	
	/**
	 * Sets the claim source.
	 *
	 * @param claimSource the new claim source
	 */
	public void setClaimSource(ClaimSource claimSource) {
		this.claimSource = claimSource;
	}
	
	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate() {
		return admissionDate;
	}
	
	/**
	 * Sets the admission date.
	 *
	 * @param admissionDate the new admission date
	 */
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}
	
	/**
	 * Gets the incident date.
	 *
	 * @return the incident date
	 */
	public Date getIncidentDate() {
		return incidentDate;
	}
	
	/**
	 * Sets the incident date.
	 *
	 * @param incidentDate the new incident date
	 */
	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}
	
	/**
	 * Gets the discharge date.
	 *
	 * @return the discharge date
	 */
	public Date getDischargeDate() {
		return dischargeDate;
	}
	
	/**
	 * Sets the discharge date.
	 *
	 * @param dischargeDate the new discharge date
	 */
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	/**
	 * Gets the policy number.
	 *
	 * @return the policy number
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * Sets the policy number.
	 *
	 * @param policyNumber the new policy number
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public char getDayCaseStatus() {
		return dayCaseStatus;
	}

	public void setDayCaseStatus(char dayCaseStatus) {
		this.dayCaseStatus = dayCaseStatus;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public Date getDisabilityDate() {
		return disabilityDate;
	}

	public void setDisabilityDate(Date disabilityDate) {
		this.disabilityDate = disabilityDate;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public String getCauseStatusCode() {
		return causeStatusCode;
	}

	public void setCauseStatusCode(String causeStatusCode) {
		this.causeStatusCode = causeStatusCode;
	}

	public String getCauseStatusSubCode() {
		return causeStatusSubCode;
	}

	public void setCauseStatusSubCode(String causeStatusSubCode) {
		this.causeStatusSubCode = causeStatusSubCode;
	}

	public String getClaimCause() {
		return claimCause;
	}

	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}

	public String getCodeICD9() {
		return codeICD9;
	}

	public void setCodeICD9(String codeICD9) {
		this.codeICD9 = codeICD9;
	}

	public String getDotorLicense() {
		return dotorLicense;
	}

	public void setDotorLicense(String dotorLicense) {
		this.dotorLicense = dotorLicense;
	}

}
