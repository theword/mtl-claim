/*
 * 
 */
package com.mtl.eligible.model.xom.mtlclaim;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.eligible.model.RuleMessage;
import com.mtl.eligible.model.RuleModel;
import com.mtl.eligible.model.enumerator.ClaimSource;
import com.mtl.eligible.model.enumerator.ClaimType;
import com.mtl.eligible.model.mtlclaim.Coverage;
import com.mtl.eligible.model.mtlclaim.Diagnosis;
import com.mtl.eligible.model.mtlclaim.Policy;
import com.mtl.eligible.util.DateTimeAdapter;
import com.mtl.eligible.util.ModelUtility;


@XmlRootElement(name = "EligibleModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class EligibleModel extends RuleModel{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -256473091636266309L;
	
	/** The request diagnoses. */
	@XmlElement(name = "requestDiagnoses", required = false)
	private List<Diagnosis> requestDiagnoses;
	
	@XmlElement(name = "policies", required = false)
	private List<Policy> policies;
	
	@XmlElement(name = "tempPolicies", required = false)
	private List<String> tempPolicies;
	
	@XmlElement(name = "clientID", required = false)
	private String clientID;
	
	/** The claim type. */
	@XmlElement(name = "claimType", required = false)
	private ClaimType claimType;
	
	/** The claim source. */
	@XmlElement(name = "claimSource", required = false)
	private ClaimSource claimSource;
	
	/** The admission date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "admissionDate", required = false)
	private Date admissionDate;
	
	/** The incident date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "incidentDate", required = false)
	private Date incidentDate;
	
	/** The discharge date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "dischargeDate", required = false)
	private Date dischargeDate;
	
	/** The policy type. */
	@XmlElement(name = "policyType", required = false)
	private String policyType;
	
	/** The current rule state. */
	@XmlElement(name = "currentRuleState", required = false)
	private String currentRuleState;
	
	/** The policy index. */
	@XmlElement(name = "policyIndex", required = false)
	private int policyIndex;
	
	/** The request diagnosis index. */
	@XmlElement(name = "requestDiagnosisIndex", required = false)
	private int requestDiagnosisIndex;
	
	@XmlElement(name = "dayCaseStatus", required = false)
	private char dayCaseStatus;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "disabilityDate", required = false)
	private Date disabilityDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "deathDate", required = false)
	private Date deathDate;
	
	private int status = 0;
	
	@XmlElement(name = "causeStatusCode", required = false)
	private String causeStatusCode;
	
	@XmlElement(name = "causeStatusSubCode", required = false)
	private String causeStatusSubCode;
	
	@XmlElement(name = "claimCause", required = false)
	private String claimCause;

	@XmlElement(name = "codeICD9", required = false)
	private String codeICD9;
	
	@XmlElement(name = "dotorLicense", required = false)
	private String dotorLicense;
	
	public Date getDisabilityDate() {
		return disabilityDate;
	}

	public void setDisabilityDate(Date disabilityDate) {
		this.disabilityDate = disabilityDate;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	/**
	 * Instantiates a new eligible model.
	 */
	private EligibleModel() {
		super();
	}
	
	/**
	 * Instantiates a new eligible model.
	 *
	 * @param requestDiagnoses the request diagnoses
	 * @param policies the policies
	 * @param claimType the claim type
	 * @param claimSource the claim source
	 * @param admissionDate the admission date
	 * @param incidentDate the incident date
	 * @param dischargeDate the discharge date
	 * @param policyType the policy type
	 */
	public EligibleModel(List<Diagnosis> requestDiagnoses, List<Policy> policies, 
			ClaimType claimType, ClaimSource claimSource, Date admissionDate, Date incidentDate, Date dischargeDate,
			Date deathDate, Date disabilityDate,String policyType,String causeStatusCode,String causeStatusSubCode,
			String claimCause,String codeICD9,String dotorLicense){
		this();
		setRequestDiagnoses(requestDiagnoses);
		setPolicies(policies);
		setClaimSource(claimSource);
		setClaimType(claimType);
		setAdmissionDate(admissionDate);
		setDischargeDate(dischargeDate);
		setIncidentDate(incidentDate);
		setPolicyType(policyType);
		setPolicyIndex(0);
		setRequestDiagnosisIndex(0);
		setDayCaseStatus('N');
		setDeathDate(deathDate);
		setDisabilityDate(disabilityDate);
		setCauseStatusCode(causeStatusCode);
		setCauseStatusSubCode(causeStatusSubCode);
		setClaimCause(claimCause);
		setCodeICD9(codeICD9);
		setDotorLicense(dotorLicense);
	}

	//TeTe change for eligible rule 19/07/2013
	/**
	 * Gets the current policy.
	 *
	 * @return the current policy
	 */
	public Policy getCurrentPolicy(){
//		System.out.println("getCurrentPolicy : policyIndex : " + policyIndex);
		if(policies != null && policies.size() > policyIndex){
//			System.out.println("can get policy");
			return policies.get(policyIndex);
		}
//		System.out.println("can not get policy");
		return null;
	}
	
	/**
	 * Have next policy.
	 *
	 * @return true, if successful
	 */
	public boolean haveNextPolicy(){
//		System.out.println("haveNextPolicy : policyIndex : " + policyIndex);
		if(policies != null && policies.size() > policyIndex+1 && policies.get(policyIndex+1) != null){
			policyIndex++;
//			System.out.println("haveNextPolicy true : policyIndex : " + policyIndex);
			return true;
		}
//		System.out.println("haveNextPolicy false : policyIndex : " + policyIndex);
		return false;
	}
	
	public int countOverDue(Date request,Date paidDate,int mode, Date issueDate){
		int result = 0;
		Date graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(paidDate, mode),issueDate);
		while(request.compareTo(graceDate)>=0){
			graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(graceDate, mode),issueDate);
			result++;
		}
		return result;
	}
	
	public int countGraceOverDue(Date request,Date graceDate,int mode, Date issueDate){
		int result = 0;
		while(request.compareTo(graceDate)>=0){
			graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(graceDate, mode),issueDate);
			result++;
		}
		return result;
	}
	
	public boolean checkCVGracePeriod(Date request,Date paidDate){
		Date graceDate = ModelUtility.addDate(paidDate, 31);
		if(request.after(graceDate)){
			return true;
		}
		return false;
	}
	
	public boolean findDiagnosisCode(String code){
		if(requestDiagnoses != null && requestDiagnoses.size()>0){
			for(Diagnosis diagnosis:requestDiagnoses){
				if(diagnosis.getDiagnosisCode().trim().equals(code))
					return true;
			}
		}
		return false;
	}
	//ging add 31-03-2016
	public boolean checkDiagnosisCause (List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosisList,String diagnosisCause){
		if(diagnosisList == null)
			return false;
		for(com.mtl.eligible.model.mtlclaim.Diagnosis diagnosis:diagnosisList){
			if(diagnosis.getDiagnosisCause() != null && diagnosisCause != null){
				if(diagnosis.getDiagnosisCause().equals(diagnosisCause)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Sets the current policy.
	 *
	 * @param policy the new current policy
	 */
	public void setCurrentPolicy(Policy policy){
//		System.out.println("in setCurrentPolicy");
		if(policies != null && policies.size() > policyIndex){
			policies.set(policyIndex, policy);
//			System.out.println("set successful at " + policyIndex);
			return;
		}
//		System.out.println("can not set at " + policyIndex);
	}
	
	//TeTe change for eligible rule 31/07/2013
	/**
	 * Gets the current request diagnosis.
	 *
	 * @return the current request diagnosis
	 */
	public Diagnosis getCurrentRequestDiagnosis(){
//		System.out.println("getCurrentRequestDiagnosis : requestDiagnosisIndex : " + requestDiagnosisIndex);
		if(requestDiagnoses != null && requestDiagnoses.size() > requestDiagnosisIndex){
//			System.out.println("can get diagnosis");
			return requestDiagnoses.get(requestDiagnosisIndex);
		}
//		System.out.println("can not get diagnosis");
		return null;
	}
	
	
	
	
	/**
	 * Have next request diagnosis.
	 *
	 * @return true, if successful
	 */
	public boolean haveNextRequestDiagnosis(){
//		System.out.println("haveNextRequestDiagnosis : requestDiagnosisIndex : " + requestDiagnosisIndex);
		if(requestDiagnoses != null && requestDiagnoses.size() > requestDiagnosisIndex+1 && requestDiagnoses.get(requestDiagnosisIndex+1) != null){
			requestDiagnosisIndex++;
//			System.out.println("haveNextRequestDiagnosis true : requestDiagnosisIndex : " + requestDiagnosisIndex);
			return true;
		}
//		System.out.println("haveNextRequestDiagnosis false : requestDiagnosisIndex : " + requestDiagnosisIndex);
		return false;
	}
	
	/**
	 * Sets the current request diagnosis.
	 *
	 * @param diagnosis the new current request diagnosis
	 */
	public void setCurrentRequestDiagnosis(Diagnosis diagnosis){
//		System.out.println("in setCurrentRequestDiagnosis");
		if(requestDiagnoses != null && requestDiagnoses.size() > requestDiagnosisIndex){
			requestDiagnoses.set(requestDiagnosisIndex, diagnosis);
//			System.out.println("set successful at " + requestDiagnosisIndex);
			return;
		}
//		System.out.println("can not set at " + requestDiagnosisIndex);
	}
	
	//check diagnosis between
	/**
	 * Check diagnosis between.
	 *
	 * @param diagnosis1 the diagnosis1
	 * @param diagnosis2 the diagnosis2
	 * @return true, if successful
	 */
	public boolean checkDiagnosisBetween(String diagnosis1,String diagnosis2){
		if(requestDiagnoses != null && requestDiagnoses.size() > 0){
			for(Diagnosis diagnosis:requestDiagnoses){
				String temp = diagnosis.getDiagnosisCode();
				if(temp.compareTo(diagnosis1) >= 0 && temp.compareTo(diagnosis2) <= 0){
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * Sets the policies.
	 *
	 * @param policies the new policies
	 */
	public void setPolicies(List<Policy> policies) {
		this.policies = policies;
	}

	/**
	 * Gets the policies.
	 *
	 * @return the policies
	 */
	public List<Policy> getPolicies() {
		return policies;
	}

	/**
	 * Sets the claim type.
	 *
	 * @param claimType the new claim type
	 */
	public void setClaimType(ClaimType claimType) {
		this.claimType = claimType;
	}

	/**
	 * Gets the claim type.
	 *
	 * @return the claim type
	 */
	public ClaimType getClaimType() {
		return claimType;
	}
	
	/**
	 * Sets the request diagnoses.
	 *
	 * @param requestDiagnoses the new request diagnoses
	 */
	public void setRequestDiagnoses(List<Diagnosis> requestDiagnoses) {
		this.requestDiagnoses = requestDiagnoses;
	}

	/**
	 * Gets the request diagnoses.
	 *
	 * @return the request diagnoses
	 */
	public List<Diagnosis> getRequestDiagnoses() {
		return requestDiagnoses;
	}

	/**
	 * Sets the admission date.
	 *
	 * @param admissionDate the new admission date
	 */
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate() {
		return admissionDate;
	}

	/**
	 * Sets the incident date.
	 *
	 * @param incidentDate the new incident date
	 */
	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	 * Gets the incident date.
	 *
	 * @return the incident date
	 */
	public Date getIncidentDate() {
		return incidentDate;
	}

	/**
	 * Sets the discharge date.
	 *
	 * @param dischargeDate the new discharge date
	 */
	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	/**
	 * Gets the discharge date.
	 *
	 * @return the discharge date
	 */
	public Date getDischargeDate() {
		return dischargeDate;
	}

	/**
	 * Sets the claim source.
	 *
	 * @param claimSource the new claim source
	 */
	public void setClaimSource(ClaimSource claimSource) {
		this.claimSource = claimSource;
	}

	/**
	 * Gets the claim source.
	 *
	 * @return the claim source
	 */
	public ClaimSource getClaimSource() {
		return claimSource;
	}
	
	/**
	 * Gets the all rule messages.
	 *
	 * @return the all rule messages
	 */
	public List<RuleMessage> getAllRuleMessages(){
		List<RuleMessage> ruleMessages = new ArrayList<RuleMessage>();
		ruleMessages.addAll(getRuleMesssages());
		if(getPolicies() != null){
			for(Policy pol : getPolicies()){
				ruleMessages.add(pol.getMessage());
				if(pol.getCoverages() != null){
					for(Coverage cov : pol.getCoverages()){
						ruleMessages.add(cov.getMessage());
					}
				}
			}
		}
		return ruleMessages;
	}

	/**
	 * Gets the policy type.
	 *
	 * @return the policy type
	 */
	public String getPolicyType() {
		return policyType;
	}

	/**
	 * Sets the policy type.
	 *
	 * @param policyType the new policy type
	 */
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	/**
	 * Gets the current rule state.
	 *
	 * @return the current rule state
	 */
	public String getCurrentRuleState() {
		return currentRuleState;
	}

	/**
	 * Sets the current rule state.
	 *
	 * @param currentRuleState the new current rule state
	 */
	public void setCurrentRuleState(String currentRuleState) {
		System.out.println("currentRuleState : "+currentRuleState);
		this.currentRuleState = currentRuleState;
	}

	/**
	 * Gets the policy index.
	 *
	 * @return the policy index
	 */
	public int getPolicyIndex() {
		return policyIndex;
	}

	/**
	 * Sets the policy index.
	 *
	 * @param policyIndex the new policy index
	 */
	public void setPolicyIndex(int policyIndex) {
		this.policyIndex = policyIndex;
	}

	/**
	 * Gets the request diagnosis index.
	 *
	 * @return the request diagnosis index
	 */
	public int getRequestDiagnosisIndex() {
		return requestDiagnosisIndex;
	}

	/**
	 * Sets the request diagnosis index.
	 *
	 * @param requestDiagnosisIndex the new request diagnosis index
	 */
	public void setRequestDiagnosisIndex(int requestDiagnosisIndex) {
		this.requestDiagnosisIndex = requestDiagnosisIndex;
	}

	public char getDayCaseStatus() {
		return dayCaseStatus;
	}

	public void setDayCaseStatus(char dayCaseStatus) {
		this.dayCaseStatus = dayCaseStatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCauseStatusCode() {
		return causeStatusCode;
	}

	public void setCauseStatusCode(String causeStatusCode) {
		this.causeStatusCode = causeStatusCode;
	}

	public String getCauseStatusSubCode() {
		return causeStatusSubCode;
	}

	public void setCauseStatusSubCode(String causeStatusSubCode) {
		this.causeStatusSubCode = causeStatusSubCode;
	}

	public String getClaimCause() {
		return claimCause;
	}

	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}

	public String getCodeICD9() {
		return codeICD9;
	}

	public void setCodeICD9(String codeICD9) {
		this.codeICD9 = codeICD9;
	}

	public String getDotorLicense() {
		return dotorLicense;
	}

	public void setDotorLicense(String dotorLicense) {
		this.dotorLicense = dotorLicense;
	}
	
	public List<String> getTempPolicies() {
		return tempPolicies;
	}

	public void setTempPolicies(List<String> tempPolicies) {
		this.tempPolicies = tempPolicies;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public void prepareCoveragesReinDate(Policy policy){
		Date masterReinDate = null;
		for(Coverage coverage : policy.getCoverages()){
			if("01".equals(coverage.getTrailerNumber())){
				if(coverage.getMasterModel() != null){
					masterReinDate = coverage.getMasterModel().getReInStatementDate();
				}
				break;
			}
		}
		System.out.println("masterReinDate = " + masterReinDate);
		
		Date reinDate;
		for(Coverage coverage : policy.getCoverages()){
			if(masterReinDate != null){
				reinDate = coverage.getMasterModel().getReInStatementDate();
				
				System.out.println("reinDate = " + reinDate);
				if(reinDate != null){
					if(reinDate.before(masterReinDate)){
						coverage.getMasterModel().setTempDate(masterReinDate);
					}
					else{
						coverage.getMasterModel().setTempDate(reinDate);
					}
				}
				else{
					coverage.getMasterModel().setTempDate(masterReinDate);
				}
			}
			else{
				coverage.getMasterModel().setTempDate(coverage.getMasterModel().getIssuedDate());
			}
			
			System.out.println("coverage.getMasterModel().getTempDate() = " + coverage.getMasterModel().getTempDate());
		}
	}
}
