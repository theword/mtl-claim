/*
 * 
 */
package com.mtl.eligible.model.templates;

/**
 * The Interface IMasterModel.
 */
public interface IMasterModel{
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode();

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code);
	
}
