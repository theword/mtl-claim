/*
 * 
 */
package com.mtl.eligible.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "RuleModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleModel implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5859018784688543833L;
	
	@XmlElement(name = "ruleMessages", required = false)
	protected List<RuleMessage> ruleMessages;
	
	@XmlElement(name = "modelStatus", required = false)
	private boolean modelStatus;
	
	/**
	 * Instantiates a new rule model.
	 */
	public RuleModel() {
		ruleMessages = new ArrayList<RuleMessage>();
		modelStatus = true;
	}
	
	/**
	 * Gets the rule messsages.
	 *
	 * @return the rule messsages
	 */
	public List<RuleMessage> getRuleMesssages() {
		return ruleMessages;
	}
	
	/**
	 * Sets the rule messsages.
	 *
	 * @param ruleMessages the new rule messsages
	 */
	public void setRuleMesssages(List<RuleMessage> ruleMessages) {
		this.ruleMessages = ruleMessages;
	}
	
	/**
	 * Gets the model status.
	 *
	 * @return the model status
	 */
	public boolean getModelStatus() {
		return modelStatus;
	}
	
	/**
	 * Sets the model status.
	 *
	 * @param modelStatus the new model status
	 */
	private void setModelStatus(boolean modelStatus) {
		this.modelStatus = modelStatus;
	}
	
	/**
	 * Approve.
	 */
	public void approve(){
		setModelStatus(true);
	}
	
	/**
	 * Reject.
	 */
	public void reject(){
		setModelStatus(false);
	}
	
	/**
	 * Adds the rule message code.
	 *
	 * @param messageCode the message code
	 */
	public void addRuleMessageCode(String messageCode){
		ruleMessages.add(new RuleMessage(messageCode));
	}
	
	/**
	 * Adds the rule message code desc.
	 *
	 * @param messageCode the message code
	 * @param descCode the desc code
	 */
	public void addRuleMessageCodeDesc(String messageCode,String descCode){
		ruleMessages.add(new RuleMessage(messageCode,descCode));
	}
	
	/**
	 * Sort rule message.
	 */
	public void sortRuleMessage() {
	    if (ruleMessages ==null || ruleMessages.size()==0){
	    	return;
	    }
	    short sortIndex = (short) ruleMessages.size();
	    quicksort((short)0, (short)(sortIndex - 1));
	 }

	/**
	 * Quicksort.
	 *
	 * @param low the low
	 * @param high the high
	 */
	private void quicksort(short low, short high) {
		short i = low, j = high;
		short pivot = ruleMessages.get(low + (high-low)/2).getPriority();

		while (i <= j) {
			while (ruleMessages.get(i).getPriority() < pivot) {
				i++;
			}
			while (ruleMessages.get(i).getPriority() > pivot) {
				j--;
			}
			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}
		// Recursion
		if (low < j)
			quicksort(low, j);
		if (i < high)
			quicksort(i, high);
	}

	/**
	 * Exchange.
	 *
	 * @param i the i
	 * @param j the j
	 */
	private void exchange(short i, short j) {
		RuleMessage temp = ruleMessages.get(i);
		ruleMessages.set(i, ruleMessages.get(j));
		ruleMessages.set(j, temp);
	}
}
