/*
 * 
 */
package com.mtl.eligible.model.mtlclaim;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.eligible.model.RuleMessage;
import com.mtl.eligible.model.mtlclaim.master.PolicyMs;
import com.mtl.eligible.model.templates.IMasterModel;
import com.mtl.eligible.model.templates.ITransactionModel;
import com.mtl.eligible.util.DateTimeAdapter;
import com.mtl.eligible.util.ModelUtility;


@XmlRootElement(name = "Policy")
@XmlAccessorType(XmlAccessType.FIELD)
public class Policy implements ITransactionModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6777265510365081557L;
	
	@XmlElement(name = "policy", required = false)
	private PolicyMs policy;
	
	@XmlElement(name = "coverages", required = false)
	private List<Coverage> coverages;
	
	@XmlElement(name = "isActive", required = false)
	private boolean isActive;
	
	/** The is over due. */
	private boolean isOverDue;
	
	@XmlElement(name = "cashValue", required = false)
	private double cashValue;
	
	@XmlElement(name = "message", required = false)
	private RuleMessage message;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "paidDate", required = false)
	private Date paidDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "paymentDate", required = false)
	private Date paymentDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "gracePeriod", required = false)
	private Date gracePeriod;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "paidDateBack", required = false)
	private Date paidDateBack;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "paymentDateBack", required = false)
	private Date paymentDateBack;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "expireDate", required = false)
	private Date expireDate;
	
	/** The coverage index. */
	private int coverageIndex;
	
	@XmlElement(name = "overDueMonth", required = false)
	private int overDueMonth;
	
	private int enoughOverDueMonth;
	
	@XmlElement(name = "tempCV", required = false)
	private Double tempCV;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "tempPaidDate", required = false)
	private Date tempPaidDate;
	
	@XmlElement(name = "sixMonthAnnualFactor", required = false)
	private Double sixMonthAnnualFactor;
	
	@XmlElement(name = "quarterlyFactor", required = false)
	private Double quarterlyFactor;
	
	@XmlElement(name = "monthlyFactor", required = false)
	private Double monthlyFactor;
	

	private String tempPolicyStatus = "";
	private String tempPolicySubCodeStatus = "";
	
	@XmlElement(name = "clientType", required = false)
	private String clientType;

	private boolean returnCash = false;
	

	/**
	 * Instantiates a new policy.
	 */
	private Policy() {
		this.activate();
		this.setMessage("");
		this.coverageIndex 		= 0;
		this.paidDate 			= null;
		this.paymentDate 		= null;
		this.gracePeriod 		= null;
		this.paidDateBack 		= null;
		this.paymentDateBack 	= null;
		this.expireDate 		= null;
		this.overDueMonth 		= 0;
		this.enoughOverDueMonth = 0;
		this.tempCV				= 0.0;
		this.tempPaidDate		= null;
		this.sixMonthAnnualFactor = 0.0;
		this.quarterlyFactor	= 0.0;
		this.monthlyFactor		= 0.0;
	}
	
	/**
	 * Instantiates a new policy.
	 *
	 * @param policy the policy
	 * @param coverages the coverages
	 */
	public Policy(PolicyMs policy, List<Coverage> coverages) {
		this();
		setMasterModel(policy);
		setCoverages(coverages);
		setCoverageIndex(0);
	}

	public int enoughOverDue(Date request,Date paidDate,int mode,Double model, Date issueDate){
		int result = 0;
		Date graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(paidDate, mode),issueDate);
		while(request.compareTo(graceDate)>=0){
			if(tempCV > model){
				result++;
				tempCV = tempCV - model;
				graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(graceDate, mode),issueDate);
			}else{
				return result;
			}
		}
		
		return result;
	}
	
	public double getModelPremium(int mode){
		double  tempModal  = 0.0;
//		if(coverages!=null && coverages.size()>0){
//			boolean isTralier1 = false;
			double  tempFactor = 0.0;
			if(mode==1){
				if(getMonthlyFactor()>1){
					tempFactor = 1.00/getMonthlyFactor();
				}else{
					tempFactor = getMonthlyFactor();
				}
			}else{
				if(mode==3){
					tempFactor = getQuarterlyFactor();
				}else{
					if(mode==6){
						tempFactor = getSixMonthAnnualFactor();
					}else{
						tempFactor = 1;//Year
					}
				}
			}
			
//			for(Coverage coverage:coverages){
//				if(coverage.getTrailerNumber().equals("01"))isTralier1 = true;
//				tempModal = tempModal + (coverage.getLifeAnnualPremium()*tempFactor);
//			}
//			if(!isTralier1){
//				tempModal = tempModal + (policy.getModalPremium()*tempFactor);
//			}
			tempModal = policy.getLifeAnnualPremium()*tempFactor;
//		}
		return tempModal;
	}
	
	public int enoughGraceOverDue(Date request,Date grace,int mode,Double model, Date issueDate){
		int result = 0;
		Date graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(grace, mode),issueDate);
		while(tempCV > model){
			result++;
			tempCV = tempCV - model;
			if(request.compareTo(graceDate)>=0){
				
			}else{
				return result;
			}
			graceDate = ModelUtility.getMaxDate(ModelUtility.addMonth(graceDate, mode),issueDate);
		}
		return result;
	}
	
	//TeTe change for eligible rule 19/07/2013
	/**
	 * Gets the current coverage.
	 *
	 * @return the current coverage
	 */
	public Coverage getCurrentCoverage(){
		if(coverages != null && coverages.size() > coverageIndex){
			return coverages.get(coverageIndex);
		}
		return null;
	}
	
	/**
	 * Have next coverage.
	 *
	 * @return true, if successful
	 */
	public boolean haveNextCoverage(){
		if(coverages != null && coverages.size() > coverageIndex+1 && coverages.get(coverageIndex+1) != null){
			coverageIndex++;
			return true;
		}
		return false;
	}
	
	/**
	 * Sets the current coverage.
	 *
	 * @param coverage the new current coverage
	 */
	public void setCurrentCoverage(Coverage coverage){
		if(coverages != null && coverages.size() > coverageIndex){
			coverages.set(coverageIndex, coverage);
			return;
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#getMasterModel()
	 */
	@Override
	public PolicyMs getMasterModel() {
		return policy;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#setMasterModel(com.mtl.model.templates.IMasterModel)
	 */
	@Override
	public void setMasterModel(IMasterModel masterModel) {
		policy = (PolicyMs)masterModel;
	}
	
	/**
	 * Sets the coverages.
	 *
	 * @param coverages the new coverages
	 */
	public void setCoverages(List<Coverage> coverages) {
		this.coverages = coverages;
	}

	/**
	 * Gets the coverages.
	 *
	 * @return the coverages
	 */
	public List<Coverage> getCoverages() {
		return coverages;
	}
	
	/**
	 * Sets the active.
	 *
	 * @param isValid the new active
	 */
	private void setActive(boolean isValid) {
		this.isActive = isValid;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}
	
	/**
	 * Activate.
	 */
	public void activate(){
		this.setActive(true);
	}
	
	/**
	 * Deactivate.
	 */
	public void deactivate(){
		this.setActive(false);
	}

	/**
	 * Sets the message map code.
	 *
	 * @param messageMapCode the new message map code
	 */
	public void setMessageMapCode(String messageMapCode) {
		message.setMessageMapCode(messageMapCode);
	}
	
	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		if(this.message == null){
			System.out.println("this.message is null" + message);
			this.message = new RuleMessage(message);
		}
		else{
			System.out.println("this.message is not null : " + message);
			this.message.setRuleMessage(message);
		}
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public RuleMessage getMessage() {
		return message;
	}
	
	/**
	 * Gets the policy number.
	 *
	 * @return the policy number
	 */
	public String getPolicyNumber(){
		return policy.getCode();
	}
	
	/**
	 * Gets the plan code.
	 *
	 * @return the plan code
	 */
	public String getPlanCode(){
		return policy.getPlanCode();
	}
	
	/**
	 * Gets the policy status.
	 *
	 * @return the policy status
	 */
	public String getPolicyStatus(){
		return policy.getPolicyStatus();
	}
	
	/**
	 * Gets the billed to date.
	 *
	 * @return the billed to date
	 */
	public Date getBilledToDate(){
		return policy.getBilledToDate();
	}
	
	/**
	 * Gets the issued date.
	 *
	 * @return the issued date
	 */
	public Date getIssuedDate(){
		return policy.getIssuedDate();
	}
	
	/**
	 * Gets the paid to date.
	 *
	 * @return the paid to date
	 */
	public Date getPaidToDate(){
		return policy.getPaidToDate();
	}

	/**
	 * Gets the policy history id.
	 *
	 * @return the policy history id
	 */
	public String getPolicyHistoryID() {
		return policy.getPolicyHistoryID();
	}
	
	/**
	 * Gets the modal premium.
	 *
	 * @return the modal premium
	 */
	public double getModalPremium() {
		return policy.getModalPremium();
	}
	
	/**
	 * Gets the mode of premium.
	 *
	 * @return the mode of premium
	 */
	public int getModeOfPremium() {
		return policy.getModeOfPremium();
	}

	/**
	 * Sets the over due.
	 *
	 * @param isOverDue the new over due
	 */
	public void setOverDue(boolean isOverDue) {
		this.isOverDue = isOverDue;
	}
	
	/**
	 * Gets the policy status sub code.
	 *
	 * @return the policy status sub code
	 */
	public String getPolicyStatusSubCode(){
		return policy.getPolicyStatusSubCode();
	}

	/**
	 * Checks if is over due.
	 *
	 * @return true, if is over due
	 */
	public boolean isOverDue() {
		return isOverDue;
	}

	/**
	 * Sets the cash value.
	 *
	 * @param cashValue the new cash value
	 */
	public void setCashValue(double cashValue) {
		this.cashValue = cashValue;
	}

	/**
	 * Gets the cash value.
	 *
	 * @return the cash value
	 */
	public double getCashValue() {
		return cashValue;
	}

	/**
	 * Gets the coverage index.
	 *
	 * @return the coverage index
	 */
	public int getCoverageIndex() {
		return coverageIndex;
	}

	/**
	 * Sets the coverage index.
	 *
	 * @param coverageIndex the new coverage index
	 */
	public void setCoverageIndex(int coverageIndex) {
		this.coverageIndex = coverageIndex;
	}

	/**
	 * Gets the paid date.
	 *
	 * @return the paid date
	 */
	public Date getPaidDate() {
		return paidDate;
	}

	/**
	 * Sets the paid date.
	 *
	 * @param paidDate the new paid date
	 */
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * Gets the grace period.
	 *
	 * @return the grace period
	 */
	public Date getGracePeriod() {
		return gracePeriod;
	}

	/**
	 * Sets the grace period.
	 *
	 * @param gracePeriod the new grace period
	 */
	public void setGracePeriod(Date gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	/**
	 * Gets the paid date back.
	 *
	 * @return the paid date back
	 */
	public Date getPaidDateBack() {
		return paidDateBack;
	}

	/**
	 * Sets the paid date back.
	 *
	 * @param paidDateBack the new paid date back
	 */
	public void setPaidDateBack(Date paidDateBack) {
		this.paidDateBack = paidDateBack;
	}

	/**
	 * Gets the payment date back.
	 *
	 * @return the payment date back
	 */
	public Date getPaymentDateBack() {
		return paymentDateBack;
	}

	/**
	 * Sets the payment date back.
	 *
	 * @param paymentDateBack the new payment date back
	 */
	public void setPaymentDateBack(Date paymentDateBack) {
		this.paymentDateBack = paymentDateBack;
	}

	/**
	 * Gets the expire date.
	 *
	 * @return the expire date
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the expire date.
	 *
	 * @param expireDate the new expire date
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public int getOverDueMonth() {
		return overDueMonth;
	}

	public void setOverDueMonth(int overDueMonth) {
		this.overDueMonth = overDueMonth;
	}

	public int getEnoughOverDueMonth() {
		return enoughOverDueMonth;
	}

	public void setEnoughOverDueMonth(int enoughOverDueMonth) {
		this.enoughOverDueMonth = enoughOverDueMonth;
	}

	public Double getTempCV() {
		return tempCV;
	}

	public void setTempCV(Double tempCV) {
		this.tempCV = tempCV;
	}

	public Date getTempPaidDate() {
		return tempPaidDate;
	}

	public void setTempPaidDate(Date tempPaidDate) {
		this.tempPaidDate = tempPaidDate;
	}

	public Double getSixMonthAnnualFactor() {
		return sixMonthAnnualFactor;
	}

	public void setSixMonthAnnualFactor(Double sixMonthAnnualFactor) {
		this.sixMonthAnnualFactor = sixMonthAnnualFactor;
	}

	public Double getQuarterlyFactor() {
		return quarterlyFactor;
	}

	public void setQuarterlyFactor(Double quarterlyFactor) {
		this.quarterlyFactor = quarterlyFactor;
	}

	public Double getMonthlyFactor() {
		return monthlyFactor;
	}

	public void setMonthlyFactor(Double monthlyFactor) {
		this.monthlyFactor = monthlyFactor;
	}
	
	public void setModeOfPremium(int modeOfPremium){
		this.policy.setModeOfPremium(modeOfPremium);
	}
	
	public Date getFirstCoverageMaturityDate(){
		if(coverages!=null && coverages.size()>0)
			return coverages.get(0).getMaturityDate();
		return null;
	}
	
	public void setModelCoverage(char model){
		if(coverages!=null && coverages.size()>0){
			for(Coverage coverage: coverages){
				if(coverage.isActive()){
					coverage.setModelStatus(model);
				}
			}
		}
	}
	
	//ging
	public String checkTempPolicySubCodeStatus(String tempPolicySubCodeStatus){
		if(tempPolicySubCodeStatus == null || tempPolicySubCodeStatus .equals("") || 
				tempPolicySubCodeStatus.equals("null")){
			return "0";
		}
		return tempPolicySubCodeStatus;
	}
	
	public void setModalPremium(double modalPremium) {
		policy.setModalPremium(modalPremium);
	}

	public String getTempPolicyStatus() {
		return tempPolicyStatus;
	}

	public void setTempPolicyStatus(String tempPolicyStatus) {
		this.tempPolicyStatus = tempPolicyStatus;
	}
	
	public String getTempPolicySubCodeStatus() {
		return tempPolicySubCodeStatus;
	}

	public void setTempPolicySubCodeStatus(String tempPolicySubCodeStatus) {
		this.tempPolicySubCodeStatus = tempPolicySubCodeStatus;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public boolean isReturnCash() {
		return returnCash;
	}

	public void setReturnCash(boolean returnCash) {
		this.returnCash = returnCash;
	}



}
