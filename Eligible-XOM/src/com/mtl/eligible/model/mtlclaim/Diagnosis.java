/*
 * 
 */
package com.mtl.eligible.model.mtlclaim;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.eligible.model.mtlclaim.master.DiagnosisMs;
import com.mtl.eligible.model.templates.IMasterModel;
import com.mtl.eligible.model.templates.ITransactionModel;


@XmlRootElement(name = "Diagnosis")
@XmlAccessorType(XmlAccessType.FIELD)
public class Diagnosis implements ITransactionModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3392686715307001879L;
	
	@XmlElement(name = "diagnosis", required = false)
	private DiagnosisMs diagnosis;
	
	/**
	 * Instantiates a new diagnosis.
	 */
	public Diagnosis(){
		diagnosis = new DiagnosisMs(null, null);
	}
	
	/**
	 * Instantiates a new diagnosis.
	 *
	 * @param diagnosis the diagnosis
	 */
	public Diagnosis(DiagnosisMs diagnosis){
		this();
		setMasterModel(diagnosis);
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#getMasterModel()
	 */
	@Override
	public DiagnosisMs getMasterModel() {
		return diagnosis;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#setMasterModel(com.mtl.model.templates.IMasterModel)
	 */
	@Override
	public void setMasterModel(IMasterModel masterModel) {
		diagnosis = (DiagnosisMs)masterModel;
	}
	
	/**
	 * Gets the diagnosis code.
	 *
	 * @return the diagnosis code
	 */
	public String getDiagnosisCode(){
		return diagnosis.getCode();
	}
	
	public String getDiagnosisCause(){
		return diagnosis.getDiagnosisCause();
	}
	
	public Integer getExclusionIndicator() {
		return diagnosis.getExclusionIndicator();
	}
	
}
