/*
 * 
 */
package com.mtl.eligible.model.mtlclaim;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.eligible.model.RuleMessage;
import com.mtl.eligible.model.mtlclaim.master.CoverageMs;
import com.mtl.eligible.model.templates.IMasterModel;
import com.mtl.eligible.model.templates.ITransactionModel;



@XmlRootElement(name = "Coverage")
@XmlAccessorType(XmlAccessType.FIELD)
public class Coverage implements ITransactionModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1093082356160063065L;
	
	@XmlElement(name = "coverage", required = false)
	private CoverageMs coverage;
	
	@XmlElement(name = "excludedDiagnoses", required = false)
	private List<Diagnosis> excludedDiagnoses;
	
	private List<Diagnosis> causeDiagnosis;
	
	@XmlElement(name = "isActive", required = false)
	private boolean isActive;
	
	@XmlElement(name = "isValid", required = false)
	private boolean isValid;
	
	@XmlElement(name = "modelStatus", required = false)
	private char modelStatus;
	
	@XmlElement(name = "message", required = false)
	private RuleMessage message;
	
	/** The special status. */
	@XmlElement(name = "specialStatus", required = false)
	private int specialStatus;
	
	/** The current diagnosis index. */
	@XmlElement(name = "currentDiagnosisIndex", required = false)
	private int currentDiagnosisIndex;
	
	@XmlElement(name = "yimKwangYimChang", required = false)
	private boolean yimKwangYimChang;
	
	@XmlElement(name = "temporaryDisablement", required = false)
	private boolean temporaryDisablement;
	
	@XmlElement(name = "motorcycle", required = false)
	private boolean motorcycle;
	
	@XmlElement(name = "maxGroupSumBenefitAmount", required = false)
	private double maxGroupSumBenefitAmount;
	
	@XmlElement(name = "availableGroupSumBenefitAmount", required = false)
	private double availableGroupSumBenefitAmount;
	
	@XmlElement(name = "groupSumBenefit", required = false)
	private boolean groupSumBenefit;
	
	@XmlElement(name = "wpM", required = false)
	private double wpM;
	
	@XmlElement(name = "clientType", required = false)
	private String clientType;
	
	private String tempCoverageStatus = "";
	
	@XmlElement(name = "clientClass", required = false)
	private String clientClass = "";
	
	private boolean returnCash 		= false;
	private boolean hasReturnCash 	= false;
	private String returnCashType 	= "";
	
	//add by Ging 2016-03-04
/*	private String classOfBusiness;
	private String supplementaryBenefit;
	private String typeInsurance;
	private String uwGroupKey;
	private String newUwGroupKey;
	*/
	/**
	 * Instantiates a new coverage.
	 */
	public Coverage() {
		this.activate();
		this.approve();
		this.setMessage("");
		this.setModelStatus('E');
		this.specialStatus = 0;
		this.coverage = new CoverageMs();
		this.yimKwangYimChang = false;
		this.temporaryDisablement = false;
		this.motorcycle = false;
		this.groupSumBenefit = false;
		this.maxGroupSumBenefitAmount = 0.0;
		this.availableGroupSumBenefitAmount = 0.0;
		
	}
	
	/**
	 * Instantiates a new coverage.
	 *
	 * @param coverage the coverage
	 * @param excludedDiagnoses the excluded diagnoses
	 */
	public Coverage(CoverageMs coverage, List<Diagnosis> excludedDiagnoses) {
		this();
		setCurrentDiagnosisIndex(0);
		setMasterModel(coverage);
		setExcludedDiagnoses(excludedDiagnoses);
	}
	
	//TeTe change for eligible rule 19/07/2013
	/**
	 * Gets the current diagnosis.
	 *
	 * @return the current diagnosis
	 */
	public Diagnosis getCurrentDiagnosis(){
		//System.out.println("getCurrentDiagnosis : currentDiagnosisIndex : " + currentDiagnosisIndex);
		if(excludedDiagnoses != null && excludedDiagnoses.size() > currentDiagnosisIndex){
			//System.out.println("can get diagnosis");
			return excludedDiagnoses.get(currentDiagnosisIndex);
		}
		//System.out.println("can not get diagnosis");
		return null;
	}
	
	/**
	 * Have next diagnosis.
	 *
	 * @return true, if successful
	 */
	public boolean haveNextDiagnosis(){
		//System.out.println("haveNextDiagnosis : currentDiagnosisIndex : " + currentDiagnosisIndex);
		if(excludedDiagnoses != null && excludedDiagnoses.size() > currentDiagnosisIndex+1 && excludedDiagnoses.get(currentDiagnosisIndex+1) != null){
			currentDiagnosisIndex++;
			//System.out.println("haveNextDiagnosis true : currentDiagnosisIndex : " + currentDiagnosisIndex);
			return true;
		}
		//System.out.println("haveNextDiagnosis false : currentDiagnosisIndex : " + currentDiagnosisIndex);
		return false;
	}
	
	/**
	 * Sets the current diagnosis.
	 *
	 * @param diagnosis the new current diagnosis
	 */
	public void setCurrentDiagnosis(Diagnosis diagnosis){
		//System.out.println("in setCurrentDiagnosis");
		if(excludedDiagnoses != null && excludedDiagnoses.size() > currentDiagnosisIndex){
			excludedDiagnoses.set(currentDiagnosisIndex, diagnosis);
			//System.out.println("set successful at " + currentDiagnosisIndex);
			return;
		}
		//System.out.println("can not set at " + currentDiagnosisIndex);
	}
	
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#getMasterModel()
	 */
	@Override
	public CoverageMs getMasterModel() {
		return coverage;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#setMasterModel(com.mtl.model.templates.IMasterModel)
	 */
	@Override
	public void setMasterModel(IMasterModel masterModel) {
		coverage = (CoverageMs)masterModel;
	}
	
	/**
	 * Gets the coverage history id.
	 *
	 * @return the coverage history id
	 */
	public String getCoverageHistoryID() {
		return coverage.getCoverageHistoryID();
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	private void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}
	
	/**
	 * Activate.
	 */
	public void activate(){
		this.setActive(true);
	}
	
	/**
	 * Deactivate.
	 */
	public void deactivate(){
		this.setActive(false);
	}
	
	/**
	 * Sets the valid.
	 *
	 * @param isValid the new valid
	 */
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	/**
	 * Checks if is valid.
	 *
	 * @return true, if is valid
	 */
	public boolean isValid() {
		return isValid;
	}
	
	/**
	 * Approve.
	 */
	public void approve(){
		this.setValid(true);
	}
	
	/**
	 * Reject.
	 */
	public void reject(){
		this.setValid(false);
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = new RuleMessage(message);
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public RuleMessage getMessage() {
		return message;
	}
	
	/**
	 * Gets the coverage code.
	 *
	 * @return the coverage code
	 */
	public String getCoverageCode(){
		return coverage.getCode();
	}
	
	/**
	 * Gets the coverage status.
	 *
	 * @return the coverage status
	 */
	public String getCoverageStatus(){
		return coverage.getCoverageStatus();
	}
	
	/**
	 * Gets the issued date.
	 *
	 * @return the issued date
	 */
	public Date getIssuedDate(){
		return coverage.getIssuedDate();
	}
	
	/**
	 * Gets the plan group code.
	 *
	 * @return the plan group code
	 */
	public String getPlanGroupCode(){
		return coverage.getPlanGroupCode();
	}
	
	/**
	 * Gets the effective date.
	 *
	 * @return the effective date
	 */
	public Date getEffectiveDate(){
		return coverage.getEffectiveDate();
	}
	
	/**
	 * Gets the trailer number.
	 *
	 * @return the trailer number
	 */
	public String getTrailerNumber(){
		return coverage.getTrailerNumber();
	}
	
	/**
	 * Gets the face amount.
	 *
	 * @return the face amount
	 */
	public double getFaceAmount(){
		return coverage.getFaceAmount();
	}

	/**
	 * Sets the excluded diagnoses.
	 *
	 * @param excludedDiagnoses the new excluded diagnoses
	 */
	public void setExcludedDiagnoses(List<Diagnosis> excludedDiagnoses) {
		this.excludedDiagnoses = excludedDiagnoses;
	}

	/**
	 * Gets the excluded diagnoses.
	 *
	 * @return the excluded diagnoses
	 */
	public List<Diagnosis> getExcludedDiagnoses() {
		return excludedDiagnoses;
	}

	/**
	 * Sets the model status.
	 *
	 * @param modelStatus the new model status
	 */
	public void setModelStatus(char modelStatus) {
		this.modelStatus = modelStatus;
	}

	/**
	 * Gets the model status.
	 *
	 * @return the model status
	 */
	public char getModelStatus() {
		return modelStatus;
	}
	
	/**
	 * Gets the max amount.
	 *
	 * @return the max amount
	 */
	public double getMaxAmount(){
		return coverage.getMaxAmount();
	}
	
	/**
	 * Sets the max amount.
	 *
	 * @param maxAmount the new max amount
	 */
	public void setMaxAmount(double maxAmount){
		coverage.setMaxAmount(maxAmount);
	}
	
	/**
	 * Calculate max amount.
	 *
	 * @param maxAmount the max amount
	 */
	public void calculateMaxAmount(double maxAmount){
		if(maxAmount > 0)
			coverage.setMaxAmount(coverage.getMaxAmount() - maxAmount);
	}
	
	/**
	 * Gets the max available amount.
	 *
	 * @return the max available amount
	 */
	public double getMaxAvailableAmount(){
		return coverage.getMaxAvailableAmount();
	}
	
	/**
	 * Sets the max available amount.
	 *
	 * @param maxAvailableAmount the new max available amount
	 */
	public void setMaxAvailableAmount(double maxAvailableAmount){
		coverage.setMaxAvailableAmount(maxAvailableAmount);
	}
	
	
	/**
	 * Checks if is cov d duct type.
	 *
	 * @return true, if is cov d duct type
	 */
	public boolean isCovDDuctType(){
		return coverage.isCovDDuctType();
	}
	
	/**
	 * Sets the cov d duct type.
	 *
	 * @param covDDuctType the new cov d duct type
	 */
	public void setCovDDuctType(boolean covDDuctType){
		coverage.setCovDDuctType(covDDuctType);
	}
	
	/**
	 * Gets the cov d duct amount.
	 *
	 * @return the cov d duct amount
	 */
	public double getCovDDuctAmount(){
		return coverage.getCovDDuctAmount();
	}
	
	/**
	 * Sets the cov d duct amount.
	 *
	 * @param covDDuctAmount the new cov d duct amount
	 */
	public void setCovDDuctAmount(double covDDuctAmount){
		coverage.setCovDDuctAmount(covDDuctAmount);
	}
	
	/**
	 * Checks if is cov d duct cal.
	 *
	 * @return true, if is cov d duct cal
	 */
	public boolean isCovDDuctCal(){
		return coverage.isCovDDuctCal();
	}
	
	/**
	 * Sets the cov d duct cal.
	 *
	 * @param covDDuctCal the new cov d duct cal
	 */
	public void setCovDDuctCal(boolean covDDuctCal){
		coverage.setCovDDuctCal(covDDuctCal);
	}

	/**
	 * Gets the current diagnosis index.
	 *
	 * @return the current diagnosis index
	 */
	public int getCurrentDiagnosisIndex() {
		return currentDiagnosisIndex;
	}

	/**
	 * Sets the current diagnosis index.
	 *
	 * @param currentDiagnosisIndex the new current diagnosis index
	 */
	public void setCurrentDiagnosisIndex(int currentDiagnosisIndex) {
		this.currentDiagnosisIndex = currentDiagnosisIndex;
	}
	
	/**
	 * Gets the coverage group code.
	 *
	 * @return the coverage group code
	 */
	public String getCoverageGroupCode(){
		return coverage.getCoverageGroupCode();
	}
	
	/**
	 * Gets the coverage status sub code.
	 *
	 * @return the coverage status sub code
	 */
	public String getCoverageStatusSubCode(){
		return coverage.getCoverageStatusSubCode();
	}
	
	/**
	 * Sets the coverage status sub code.
	 *
	 * @param coverageStatusSubCode the new coverage status sub code
	 */
	public void setCoverageStatusSubCode(String coverageStatusSubCode){
		this.coverage.setCoverageStatusSubCode(coverageStatusSubCode);
	}
	
	/**
	 * Gets the plan name.
	 *
	 * @return the plan name
	 */
	public String getPlanName() {
		return coverage.getPlanName();
	}
	
	/**
	 * Gets the message map code.
	 *
	 * @return the message map code
	 */
	public String getMessageMapCode() {
		return message.getMessageMapCode();
	}

	/**
	 * Sets the message map code.
	 *
	 * @param messageMapCode the new message map code
	 */
	public void setMessageMapCode(String messageMapCode) {
		message.setMessageMapCode(messageMapCode);
	}

	/**
	 * Gets the special status.
	 *
	 * @return the special status
	 */
	public int getSpecialStatus() {
		return specialStatus;
	}

	/**
	 * Sets the special status.
	 *
	 * @param specialStatus the new special status
	 */
	public void setSpecialStatus(int specialStatus) {
		this.specialStatus = specialStatus;
	}
	
	public Date getMaturityDate(){
		return this.coverage.getMaturityDate();
	}

	public boolean isYimKwangYimChang() {
		return yimKwangYimChang;
	}

	public void setYimKwangYimChang(boolean yimKwangYimChang) {
		this.yimKwangYimChang = yimKwangYimChang;
	}

	public boolean isTemporaryDisablement() {
		return temporaryDisablement;
	}

	public void setTemporaryDisablement(boolean temporaryDisablement) {
		this.temporaryDisablement = temporaryDisablement;
	}

	public boolean isMotorcycle() {
		return motorcycle;
	}

	public void setMotorcycle(boolean motorcycle) {
		this.motorcycle = motorcycle;
	}
	
	public double getLifeAnnualPremium() {
		return coverage.getLifeAnnualPremium();
	}

	public double getAvailableGroupSumBenefitAmount() {
		return availableGroupSumBenefitAmount;
	}

	public void setAvailableGroupSumBenefitAmount(double availableGroupSumBenefitAmount) {
		this.availableGroupSumBenefitAmount = availableGroupSumBenefitAmount;
	}

	public boolean isGroupSumBenefit() {
		return groupSumBenefit;
	}

	public void setGroupSumBenefit(boolean groupSumBenefit) {
		this.groupSumBenefit = groupSumBenefit;
	}

	public double getMaxGroupSumBenefitAmount() {
		return maxGroupSumBenefitAmount;
	}

	public void setMaxGroupSumBenefitAmount(double maxGroupSumBenefitAmount) {
		this.maxGroupSumBenefitAmount = maxGroupSumBenefitAmount;
	}

	public double getClaimYearAmountRHM(){
		return this.coverage.getClaimYearAmountRHM();
	}
	
	public double getClaimYearAmountRHL() {
		return this.coverage.getClaimYearAmountRHL();
	}

	public CoverageMs getCoverage() {
		return coverage;
	}

	public void setCoverage(CoverageMs coverage) {
		this.coverage = coverage;
	}

	public String getTempCoverageStatus() {
		return tempCoverageStatus;
	}

	public void setTempCoverageStatus(String tempCoverageStatus) {
		this.tempCoverageStatus = tempCoverageStatus;
	}

	public double getWpM() {
		return wpM;
	}

	public void setWpM(double wpM) {
		this.wpM = wpM;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getClientClass() {
		return clientClass;
	}

	public void setClientClass(String clientClass) {
		this.clientClass = clientClass;
	}

	public boolean isReturnCash() {
		return returnCash;
	}

	public void setReturnCash(boolean returnCash) {
		this.returnCash = returnCash;
	}

	public boolean isHasReturnCash() {
		return hasReturnCash;
	}

	public void setHasReturnCash(boolean hasReturnCash) {
		this.hasReturnCash = hasReturnCash;
	}

	public String getReturnCashType() {
		return returnCashType;
	}

	public void setReturnCashType(String returnCashType) {
		this.returnCashType = returnCashType;
	}
	
	public Integer getWaitingDays(){
		return this.coverage.getWaitingDays();
	}

	public String getClassOfBusiness() {
		return coverage.getClassOfBusiness();
	}
	
	public String getSupplementaryBenefit() {
		return coverage.getSupplementaryBenefit();
	}
	
	public String getTypeInsurance() {
		return coverage.getTypeInsurance();
	}
	
	public String getUwGroupKey() {
		return coverage.getUwGroupKey();
	}
	
	public String getNewUwGroupKey() {
		return coverage.getNewUwGroupKey();
	}

	public List<Diagnosis> getCauseDiagnosis() {
		return causeDiagnosis;
	}

	public void setCauseDiagnosis(List<Diagnosis> causeDiagnosis) {
		this.causeDiagnosis = causeDiagnosis;
	}
	
}
