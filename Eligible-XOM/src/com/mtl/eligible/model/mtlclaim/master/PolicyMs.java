/*
 * 
 */
package com.mtl.eligible.model.mtlclaim.master;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.eligible.model.templates.IMasterModel;
import com.mtl.eligible.util.DateTimeAdapter;

@XmlRootElement(name = "PolicyMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class PolicyMs implements IMasterModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8902542876406317829L;
	
	@XmlElement(name = "policyHistoryID", required = false)
	private String policyHistoryID;
	
	@XmlElement(name = "policyNumber", required = false)
	private String policyNumber;
	
	@XmlElement(name = "policyStatus", required = false)
	private String policyStatus;
	
	@XmlElement(name = "planCode", required = false)
	private String planCode;
	
	@XmlElement(name = "policyStatusSubCode", required = false)
	private String policyStatusSubCode;
	
	@XmlElement(name = "modalPremium", required = false)
	private double modalPremium;
	
	@XmlElement(name = "modeOfPremium", required = false)
	private int modeOfPremium;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "billedToDate", required = false)
	private Date billedToDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "paidToDate", required = false)
	private Date paidToDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "issuedDate", required = false)
	private Date issuedDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "reInStatementDate", required = false)
	private Date reInStatementDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "tempDate", required = false)
	private Date tempDate;
	
	@XmlElement(name = "policyCurrentStatusCode", required = false)
	private String policyCurrentStatusCode;
	
	@XmlElement(name = "policyCurrentSubStatusCode", required = false)
	private String policyCurrentSubStatusCode;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "poDateStatusChange", required = false)
	private Date poDateStatusChange;
	
	@XmlElement(name = "policyComment", required = false)
	private String policyComment;
	
	@XmlElement(name = "agentBranch", required = false)
	private String agentBranch;
	
	@XmlElement(name = "agentCode", required = false)
	private String agentCode;
	
	@XmlElement(name = "agentPlacement", required = false)
	private String agentPlacement;
	
	@XmlElement(name = "paymentMode", required = false)
	private String paymentMode;
	
	@XmlElement(name = "mailingAddress", required = false)
	private String mailingAddress;
	
	@XmlElement(name = "city", required = false)
	private String city;
	
	@XmlElement(name = "postalCode", required = false)
	private String postalCode;
	
	@XmlElement(name = "bankIndicator", required = false)
	private Integer bankIndicator;
	
	@XmlElement(name = "bankAccountNumber", required = false)
	private String bankAccountNumber;
	
	@XmlElement(name = "addressIndicator", required = false)
	private Integer addressIndicator;
	
	@XmlElement(name = "ownerClientNumber", required = false)
	private String ownerClientNumber;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "approvedDate", required = false)
	private Date approvedDate;
	
	@XmlElement(name = "relationship", required = false)
	private String relationship;
	
	@XmlElement(name = "voidingPeriod", required = false)
	private String voidingPeriod;
	
	@XmlElement(name = "planDesc", required = false)
	private String planDesc;
	
	@XmlElement(name = "lifeAnnualPremium", required = false)
	private double lifeAnnualPremium;
	
	@XmlElement(name = "policyGroupType", required = false)
	private String policyGroupType;
	
	@XmlElement(name = "isClaim", required = false)
	private boolean isClaim;
	
	@XmlElement(name = "oldStatusCode", required = false)
	private String oldStatusCode;
	
	public String getPolicyGroupType() {
		return policyGroupType;
	}

	public void setPolicyGroupType(String policyGroupType) {
		this.policyGroupType = policyGroupType;
	}

	/**
	 * Instantiates a new policy ms.
	 */
	private PolicyMs() {
		
	}
	
	/**
	 * Instantiates a new policy ms.
	 *
	 * @param policyHistoryID the policy history id
	 * @param policyNumber the policy number
	 * @param policyStatus the policy status
	 * @param planCode the plan code
	 * @param modalPremium the modal premium
	 * @param modeOfPremium the mode of premium
	 * @param issuedDate the issued date
	 * @param billedToDate the billed to date
	 * @param paidToDate the paid to date
	 */
	public PolicyMs(String policyHistoryID, String policyNumber, String policyStatus, String planCode, 
			double modalPremium, int modeOfPremium, Date issuedDate, Date billedToDate, Date paidToDate, Date reInStatementDate) {
		this();
		setPolicyHistoryID(policyHistoryID);
		setCode(policyNumber);
		setPolicyStatus(policyStatus);
		setPlanCode(planCode);
		setModalPremium(modalPremium);
		setModeOfPremium(modeOfPremium);
		setIssuedDate(issuedDate);
		setBilledToDate(billedToDate);
		setPaidToDate(paidToDate);
		setReInStatementDate(reInStatementDate);
	}
	
	/**
	 * Instantiates a new policy ms.
	 *
	 * @param policyHistoryID the policy history id
	 * @param policyNumber the policy number
	 * @param policyStatus the policy status
	 * @param planCode the plan code
	 * @param modalPremium the modal premium
	 * @param modeOfPremium the mode of premium
	 * @param issuedDate the issued date
	 * @param billedToDate the billed to date
	 * @param paidToDate the paid to date
	 * @param policyStatusSubCode the policy status sub code
	 */
	public PolicyMs(String policyHistoryID, String policyNumber, String policyStatus, String planCode, 
			double modalPremium, int modeOfPremium, Date issuedDate, Date reInStatementDate, Date billedToDate, Date paidToDate,
			String policyStatusSubCode) {
		this();
		setPolicyHistoryID(policyHistoryID);
		setCode(policyNumber);
		setPolicyStatus(policyStatus);
		setPlanCode(planCode);
		setModalPremium(modalPremium);
		setModeOfPremium(modeOfPremium);
		setIssuedDate(issuedDate);
		setReInStatementDate(reInStatementDate);
		setBilledToDate(billedToDate);
		setPaidToDate(paidToDate);
		setPolicyStatusSubCode(policyStatusSubCode != null && 
							  !policyStatusSubCode.trim().equalsIgnoreCase("null") && 
							  !policyStatusSubCode.trim().equals("")
							  ?policyStatusSubCode.trim():"");
	}

	public PolicyMs(String policyNumber,String policyGroupType ) {
		this();
		setCode(policyNumber);
		setPolicyGroupType(policyGroupType);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#getCode()
	 */
	@Override
	public String getCode() {
		return policyNumber;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.policyNumber = code;
	}

	/**
	 * Sets the policy status.
	 *
	 * @param policyStatus the new policy status
	 */
	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	/**
	 * Gets the policy status.
	 *
	 * @return the policy status
	 */
	public String getPolicyStatus() {
		return policyStatus;
	}

	/**
	 * Sets the issued date.
	 *
	 * @param issuedDate the new issued date
	 */
	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	/**
	 * Gets the issued date.
	 *
	 * @return the issued date
	 */
	public Date getIssuedDate() {
		return issuedDate;
	}

	/**
	 * Sets the paid to date.
	 *
	 * @param paidToDate the new paid to date
	 */
	public void setPaidToDate(Date paidToDate) {
		this.paidToDate = paidToDate;
	}

	/**
	 * Gets the paid to date.
	 *
	 * @return the paid to date
	 */
	public Date getPaidToDate() {
		return paidToDate;
	}

	/**
	 * Sets the billed to date.
	 *
	 * @param billedToDate the new billed to date
	 */
	public void setBilledToDate(Date billedToDate) {
		this.billedToDate = billedToDate;
	}

	/**
	 * Gets the billed to date.
	 *
	 * @return the billed to date
	 */
	public Date getBilledToDate() {
		return billedToDate;
	}

	/**
	 * Sets the plan code.
	 *
	 * @param planCode the new plan code
	 */
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	/**
	 * Gets the plan code.
	 *
	 * @return the plan code
	 */
	public String getPlanCode() {
		return planCode;
	}

	/**
	 * Sets the policy history id.
	 *
	 * @param policyHistoryID the new policy history id
	 */
	public void setPolicyHistoryID(String policyHistoryID) {
		this.policyHistoryID = policyHistoryID;
	}

	/**
	 * Gets the policy history id.
	 *
	 * @return the policy history id
	 */
	public String getPolicyHistoryID() {
		return policyHistoryID;
	}

	/**
	 * Sets the modal premium.
	 *
	 * @param modalPremium the new modal premium
	 */
	public void setModalPremium(double modalPremium) {
		this.modalPremium = modalPremium;
	}

	/**
	 * Gets the modal premium.
	 *
	 * @return the modal premium
	 */
	public double getModalPremium() {
		return modalPremium;
	}

	/**
	 * Sets the mode of premium.
	 *
	 * @param modeOfPremium the new mode of premium
	 */
	public void setModeOfPremium(int modeOfPremium) {
		this.modeOfPremium = modeOfPremium;
	}

	/**
	 * Gets the mode of premium.
	 *
	 * @return the mode of premium
	 */
	public int getModeOfPremium() {
		return modeOfPremium;
	}

	/**
	 * Gets the policy status sub code.
	 *
	 * @return the policy status sub code
	 */
	public String getPolicyStatusSubCode() {
		return policyStatusSubCode;
	}

	/**
	 * Sets the policy status sub code.
	 *
	 * @param policyStatusSubCode the new policy status sub code
	 */
	public void setPolicyStatusSubCode(String policyStatusSubCode) {
		this.policyStatusSubCode = policyStatusSubCode;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Date getReInStatementDate() {
		return reInStatementDate;
	}

	public void setReInStatementDate(Date reInStatementDate) {
		this.reInStatementDate = reInStatementDate;
	}

	public String getPolicyCurrentStatusCode() {
		return policyCurrentStatusCode;
	}

	public void setPolicyCurrentStatusCode(String policyCurrentStatusCode) {
		this.policyCurrentStatusCode = policyCurrentStatusCode;
	}

	public String getPolicyCurrentSubStatusCode() {
		return policyCurrentSubStatusCode;
	}

	public void setPolicyCurrentSubStatusCode(String policyCurrentSubStatusCode) {
		this.policyCurrentSubStatusCode = policyCurrentSubStatusCode;
	}

	public String getPolicyComment() {
		return policyComment;
	}

	public void setPolicyComment(String policyComment) {
		this.policyComment = policyComment;
	}

	public String getAgentBranch() {
		return agentBranch;
	}

	public void setAgentBranch(String agentBranch) {
		this.agentBranch = agentBranch;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentPlacement() {
		return agentPlacement;
	}

	public void setAgentPlacement(String agentPlacement) {
		this.agentPlacement = agentPlacement;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Integer getBankIndicator() {
		return bankIndicator;
	}

	public void setBankIndicator(Integer bankIndicator) {
		this.bankIndicator = bankIndicator;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public Integer getAddressIndicator() {
		return addressIndicator;
	}

	public void setAddressIndicator(Integer addressIndicator) {
		this.addressIndicator = addressIndicator;
	}

	public String getOwnerClientNumber() {
		return ownerClientNumber;
	}

	public void setOwnerClientNumber(String ownerClientNumber) {
		this.ownerClientNumber = ownerClientNumber;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getVoidingPeriod() {
		return voidingPeriod;
	}

	public void setVoidingPeriod(String voidingPeriod) {
		this.voidingPeriod = voidingPeriod;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public Date getPoDateStatusChange() {
		return poDateStatusChange;
	}

	public void setPoDateStatusChange(Date poDateStatusChange) {
		this.poDateStatusChange = poDateStatusChange;
	}

	public double getLifeAnnualPremium() {
		return lifeAnnualPremium;
	}

	public void setLifeAnnualPremium(double lifeAnnualPremium) {
		this.lifeAnnualPremium = lifeAnnualPremium;
	}

	public Date getTempDate() {
		return tempDate;
	}

	public void setTempDate(Date tempDate) {
		this.tempDate = tempDate;
	}

	public boolean isClaim() {
		return isClaim;
	}

	public void setClaim(boolean isClaim) {
		this.isClaim = isClaim;
	}

	public String getOldStatusCode() {
		return oldStatusCode;
	}

	public void setOldStatusCode(String oldStatusCode) {
		this.oldStatusCode = oldStatusCode;
	}
	
}
