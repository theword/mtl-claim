/*
 * 
 */
package com.mtl.eligible.model.mtlclaim.master;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.eligible.model.templates.IMasterModel;
import com.mtl.eligible.util.DateTimeAdapter;


@XmlRootElement(name = "CoverageMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoverageMs implements IMasterModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5755295338171820621L;
	
	@XmlElement(name = "coverageHistoryID", required = false)
	private String coverageHistoryID;
	
	@XmlElement(name = "coverageCode", required = false)
	private String coverageCode;
	
	@XmlElement(name = "coverageStatus", required = false)
	private String coverageStatus;
	
	/** The coverage status sub code. */
	@XmlElement(name = "coverageStatusSubCode", required = false)
	private String coverageStatusSubCode;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "issuedDate", required = false)
	private Date issuedDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "reInStatementDate", required = false)
	private Date reInStatementDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "tempDate", required = false)
	private Date tempDate;
	
	/** The effective date. */
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "effectiveDate", required = false)
	private Date effectiveDate;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "maturityDate", required = false)
	private Date maturityDate;
	
	@XmlElement(name = "planGroupCode", required = false)
	private String planGroupCode;
	
	/** The plan name. */
	@XmlElement(name = "planName", required = false)
	private String planName;
	
	@XmlElement(name = "trailerNumber", required = false)
	private String trailerNumber;
	
	@XmlElement(name = "faceAmount", required = false)
	private double faceAmount;
	
	/** The coverage group code. */
	@XmlElement(name = "coverageGroupCode", required = false)
	private String coverageGroupCode;
	
	//max amount claim for coverage
	/** The max amount. */
	@XmlElement(name = "maxAmount", required = false)
	private double maxAmount;
	
	/** The max available amount. */
	@XmlElement(name = "maxAvailableAmount", required = false)
	private double maxAvailableAmount;
	//d duct type
	/** The cov d duct type. */
	@XmlElement(name = "covDDuctType", required = false)
	private boolean covDDuctType;
	
	/** The cov d duct amount. */
	@XmlElement(name = "covDDuctAmount", required = false)
	private double	covDDuctAmount;
	
	/** The cov d duct cal. */
	@XmlElement(name = "covDDuctCal", required = false)
	private boolean covDDuctCal;
	
	@XmlElement(name = "lifeAnnualPremium", required = false)
	private double lifeAnnualPremium;
	
	@XmlElement(name = "claimYearAmountRHM", required = false)
	private double claimYearAmountRHM;
	
	@XmlElement(name = "claimYearAmountRHL", required = false)
	private double claimYearAmountRHL;
	
	@XmlElement(name = "coverageCurrentStatusCode", required = false)
	private String coverageCurrentStatusCode;
	
	@XmlElement(name = "coverageCurrentStatusSubCode", required = false)
	private String coverageCurrentStatusSubCode;

	@XmlElement(name = "sumInsured", required = false)
	private BigDecimal sumInsured;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "dateStatusChange", required = false)
	private Date dateStatusChange;
	
	@XmlElement(name = "voidingPeriod", required = false)
	private String voidingPeriod;
	
	@XmlElement(name = "waitingDays", required = false)
	private Integer waitingDays = 0;
	
	@XmlElement(name = "classOfBusiness", required = false)
	private String classOfBusiness;
	
	@XmlElement(name = "supplementaryBenefit", required = false)
	private String supplementaryBenefit;
	
	@XmlElement(name = "typeInsurance", required = false)
	private String typeInsurance;
	
	@XmlElement(name = "uwGroupKey", required = false)
	private String uwGroupKey;
	
	@XmlElement(name = "newUwGroupKey", required = false)
	private String newUwGroupKey;
	
	@XmlElement(name = "coverageNumber", required = false)
	private String coverageNumber;
	
	public String getCoverageNumber() {
		return coverageNumber;
	}

	public void setCoverageNumber(String coverageNumber) {
		this.coverageNumber = coverageNumber;
	}

	/**
	 * Instantiates a new coverage ms.
	 */
	public CoverageMs() {
		issuedDate    = null;
		effectiveDate = null;
		maturityDate  = null;
		dateStatusChange= null;
		lifeAnnualPremium = 0.0;
		claimYearAmountRHM= 0.0;
		claimYearAmountRHL= 0.0;
	}
	
	/**
	 * Instantiates a new coverage ms.
	 *
	 * @param coverageHistoryID the coverage history id
	 * @param coverageCode the coverage code
	 * @param coverageStatus the coverage status
	 * @param issuedDate the issued date
	 * @param planGroupCode the plan group code
	 * @param effectiveDate the effective date
	 * @param trailerNumber the trailer number
	 * @param faceAmount the face amount
	 */
	public CoverageMs(String coverageHistoryID, String coverageCode, String coverageStatus, 
			Date issuedDate, String planGroupCode, Date effectiveDate, String trailerNumber, double faceAmount) {
		this();
		setCoverageHistoryID(coverageHistoryID);
		setCode(coverageCode);
		setCoverageStatus(coverageStatus);
		setIssuedDate(issuedDate);
		setPlanGroupCode(planGroupCode);
		setEffectiveDate(effectiveDate);
		setTrailerNumber(trailerNumber);
		setFaceAmount(faceAmount);
		
		setCovDDuctType(false);
		setCovDDuctAmount(0.00);
		setCovDDuctCal(false);
	}

	/**
	 * Instantiates a new coverage ms.
	 *
	 * @param coverageHistoryID the coverage history id
	 * @param coverageCode the coverage code
	 * @param coverageStatus the coverage status
	 * @param issuedDate the issued date
	 * @param planGroupCode the plan group code
	 * @param effectiveDate the effective date
	 * @param trailerNumber the trailer number
	 * @param faceAmount the face amount
	 * @param coverageGroupCode the coverage group code
	 * @param coverageStatusSubCode the coverage status sub code
	 * @param planName the plan name
	 */
	public CoverageMs(String coverageHistoryID, String coverageCode, String coverageStatus, 
			Date issuedDate, String planGroupCode, Date effectiveDate, String trailerNumber, double faceAmount,
			String coverageGroupCode,String coverageStatusSubCode,String planName) {
		this();
		setCoverageHistoryID(coverageHistoryID);
		setCode(coverageCode);
		setCoverageStatus(coverageStatus);
		setIssuedDate(issuedDate);
		setPlanGroupCode(planGroupCode);
		setEffectiveDate(effectiveDate);
		setTrailerNumber(trailerNumber);
		setFaceAmount(faceAmount);
		
		setCovDDuctType(false);
		setCovDDuctAmount(0.00);
		setCovDDuctCal(false);
		
		setCoverageGroupCode(coverageGroupCode);
		setCoverageStatusSubCode(coverageStatusSubCode);
		setPlanName(planName);

		
		
	}
	
	public CoverageMs(String coverageHistoryID, String coverageCode, String coverageStatus, 
			Date issuedDate, String planGroupCode, Date effectiveDate, String trailerNumber, double faceAmount,
			String coverageGroupCode,String coverageStatusSubCode,String planName,Date maturityDate,double lifeAnnualPremium) {
		this();
		setCoverageHistoryID(coverageHistoryID);
		setCode(coverageCode);
		setCoverageStatus(coverageStatus);
		setIssuedDate(issuedDate);
		setPlanGroupCode(planGroupCode);
		setEffectiveDate(effectiveDate);
		setTrailerNumber(trailerNumber);
		setFaceAmount(faceAmount);
		
		setCovDDuctType(false);
		setCovDDuctAmount(0.00);
		setCovDDuctCal(false);
		
		setCoverageGroupCode(coverageGroupCode);
		setCoverageStatusSubCode(coverageStatusSubCode);
		setPlanName(planName);
		setMaturityDate(maturityDate);
		setLifeAnnualPremium(lifeAnnualPremium);
		//ging 16-03-2016
		/*setClassOfBusiness(classOfBusiness);
		setSupplementaryBenefit(supplementaryBenefit);
		setTypeInsurance(typeInsurance);
		setUwGroupKey(uwGroupKey);
		setNewUwGroupKey(newUwGroupKey);*/
	}
	
	/**
	 * Instantiates a new coverage ms.
	 *
	 * @param coverageHistoryID the coverage history id
	 * @param coverageCode the coverage code
	 * @param coverageStatus the coverage status
	 * @param issuedDate the issued date
	 * @param planGroupCode the plan group code
	 * @param effectiveDate the effective date
	 * @param trailerNumber the trailer number
	 * @param faceAmount the face amount
	 * @param maxAmount the max amount
	 */
	public CoverageMs(String coverageHistoryID, String coverageCode, String coverageStatus, Date issuedDate, 
			String planGroupCode, Date effectiveDate, String trailerNumber, double faceAmount, double maxAmount) {
		this();
		setCoverageHistoryID(coverageHistoryID);
		setCode(coverageCode);
		setCoverageStatus(coverageStatus);
		setIssuedDate(issuedDate);
		setPlanGroupCode(planGroupCode);
		setEffectiveDate(effectiveDate);
		setTrailerNumber(trailerNumber);
		setFaceAmount(faceAmount);
		
		setMaxAmount(maxAmount);
		setMaxAvailableAmount(maxAmount);
		setCovDDuctType(false);
		setCovDDuctAmount(0.00);
		setCovDDuctCal(false);
	}
	
	/**
	 * Instantiates a new coverage ms.
	 *
	 * @param coverageHistoryID the coverage history id
	 * @param coverageCode the coverage code
	 * @param coverageStatus the coverage status
	 * @param issuedDate the issued date
	 * @param planGroupCode the plan group code
	 * @param effectiveDate the effective date
	 * @param trailerNumber the trailer number
	 * @param faceAmount the face amount
	 * @param maxAmount the max amount
	 * @param coverageGroupCode the coverage group code
	 */
	public CoverageMs(String coverageHistoryID, String coverageCode, String coverageStatus, Date issuedDate, 
			String planGroupCode, Date effectiveDate, String trailerNumber, double faceAmount, double maxAmount,
			String coverageGroupCode) {
		this();
		setCoverageHistoryID(coverageHistoryID);
		setCode(coverageCode);
		setCoverageStatus(coverageStatus);
		setIssuedDate(issuedDate);
		setPlanGroupCode(planGroupCode);
		setEffectiveDate(effectiveDate);
		setTrailerNumber(trailerNumber);
		setFaceAmount(faceAmount);
		
		setMaxAmount(maxAmount);
		setMaxAvailableAmount(maxAmount);
		setCovDDuctType(false);
		setCovDDuctAmount(0.00);
		setCovDDuctCal(false);

		setCoverageGroupCode(coverageGroupCode);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#getCode()
	 */
	@Override
	public String getCode() {
		return coverageCode;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.coverageCode = code;
	}

	/**
	 * Sets the coverage status.
	 *
	 * @param coverageStatus the new coverage status
	 */
	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}

	/**
	 * Gets the coverage status.
	 *
	 * @return the coverage status
	 */
	public String getCoverageStatus() {
		return coverageStatus;
	}
	
	/**
	 * Sets the issued date.
	 *
	 * @param issuedDate the new issued date
	 */
	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	/**
	 * Gets the issued date.
	 *
	 * @return the issued date
	 */
	public Date getIssuedDate() {
		return issuedDate;
	}

	/**
	 * Sets the plan group code.
	 *
	 * @param planGroupCode the new plan group code
	 */
	public void setPlanGroupCode(String planGroupCode) {
		this.planGroupCode = planGroupCode;
	}

	/**
	 * Gets the plan group code.
	 *
	 * @return the plan group code
	 */
	public String getPlanGroupCode() {
		return planGroupCode;
	}

	/**
	 * Sets the coverage history id.
	 *
	 * @param coverageHistoryID the new coverage history id
	 */
	public void setCoverageHistoryID(String coverageHistoryID) {
		this.coverageHistoryID = coverageHistoryID;
	}

	/**
	 * Gets the coverage history id.
	 *
	 * @return the coverage history id
	 */
	public String getCoverageHistoryID() {
		return coverageHistoryID;
	}

	/**
	 * Sets the trailer number.
	 *
	 * @param trailerNumber the new trailer number
	 */
	public void setTrailerNumber(String trailerNumber) {
		this.trailerNumber = trailerNumber;
	}

	/**
	 * Gets the trailer number.
	 *
	 * @return the trailer number
	 */
	public String getTrailerNumber() {
		return trailerNumber;
	}

	/**
	 * Sets the face amount.
	 *
	 * @param faceAmount the new face amount
	 */
	public void setFaceAmount(double faceAmount) {
		this.faceAmount = faceAmount;
	}

	/**
	 * Gets the face amount.
	 *
	 * @return the face amount
	 */
	public double getFaceAmount() {
		return faceAmount;
	}

	/**
	 * Sets the effective date.
	 *
	 * @param effectiveDate the new effective date
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * Gets the effective date.
	 *
	 * @return the effective date
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * Gets the max amount.
	 *
	 * @return the max amount
	 */
	public double getMaxAmount() {
		return maxAmount;
	}

	/**
	 * Sets the max amount.
	 *
	 * @param maxAmount the new max amount
	 */
	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}

	/**
	 * Gets the max available amount.
	 *
	 * @return the max available amount
	 */
	public double getMaxAvailableAmount() {
		return maxAvailableAmount;
	}

	/**
	 * Sets the max available amount.
	 *
	 * @param maxAvailableAmount the new max available amount
	 */
	public void setMaxAvailableAmount(double maxAvailableAmount) {
		this.maxAvailableAmount = maxAvailableAmount;
	}

	/**
	 * Checks if is cov d duct type.
	 *
	 * @return true, if is cov d duct type
	 */
	public boolean isCovDDuctType() {
		return covDDuctType;
	}

	/**
	 * Sets the cov d duct type.
	 *
	 * @param covDDuctType the new cov d duct type
	 */
	public void setCovDDuctType(boolean covDDuctType) {
		this.covDDuctType = covDDuctType;
	}

	/**
	 * Gets the cov d duct amount.
	 *
	 * @return the cov d duct amount
	 */
	public double getCovDDuctAmount() {
		return covDDuctAmount;
	}

	/**
	 * Sets the cov d duct amount.
	 *
	 * @param covDDuctAmount the new cov d duct amount
	 */
	public void setCovDDuctAmount(double covDDuctAmount) {
		this.covDDuctAmount = covDDuctAmount;
	}

	/**
	 * Checks if is cov d duct cal.
	 *
	 * @return true, if is cov d duct cal
	 */
	public boolean isCovDDuctCal() {
		return covDDuctCal;
	}

	/**
	 * Sets the cov d duct cal.
	 *
	 * @param covDDuctCal the new cov d duct cal
	 */
	public void setCovDDuctCal(boolean covDDuctCal) {
		this.covDDuctCal = covDDuctCal;
	}

	/**
	 * Gets the coverage group code.
	 *
	 * @return the coverage group code
	 */
	public String getCoverageGroupCode() {
		return coverageGroupCode;
	}

	/**
	 * Sets the coverage group code.
	 *
	 * @param coverageGroupCode the new coverage group code
	 */
	public void setCoverageGroupCode(String coverageGroupCode) {
		this.coverageGroupCode = coverageGroupCode;
	}

	/**
	 * Gets the coverage status sub code.
	 *
	 * @return the coverage status sub code
	 */
	public String getCoverageStatusSubCode() {
		return coverageStatusSubCode;
	}

	/**
	 * Sets the coverage status sub code.
	 *
	 * @param coverageStatusSubCode the new coverage status sub code
	 */
	public void setCoverageStatusSubCode(String coverageStatusSubCode) {
		this.coverageStatusSubCode = (coverageStatusSubCode != null && 
									   !coverageStatusSubCode.trim().equalsIgnoreCase("null") && 
									   !coverageStatusSubCode.trim().equals("")
									   ?coverageStatusSubCode.trim():"");
	}

	/**
	 * Gets the plan name.
	 *
	 * @return the plan name
	 */
	public String getPlanName() {
		return planName;
	}

	/**
	 * Sets the plan name.
	 *
	 * @param planName the new plan name
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public double getLifeAnnualPremium() {
		return lifeAnnualPremium;
	}

	public void setLifeAnnualPremium(double lifeAnnualPremium) {
		this.lifeAnnualPremium = lifeAnnualPremium;
	}

	public double getClaimYearAmountRHM() {
		return claimYearAmountRHM;
	}

	public void setClaimYearAmountRHM(double claimYearAmountRHM) {
		this.claimYearAmountRHM = claimYearAmountRHM;
	}

	public double getClaimYearAmountRHL() {
		return claimYearAmountRHL;
	}

	public void setClaimYearAmountRHL(double claimYearAmountRHL) {
		this.claimYearAmountRHL = claimYearAmountRHL;
	}

	public String getCoverageCode() {
		return coverageCode;
	}

	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	public String getCoverageCurrentStatusCode() {
		return coverageCurrentStatusCode;
	}

	public void setCoverageCurrentStatusCode(String coverageCurrentStatusCode) {
		this.coverageCurrentStatusCode = coverageCurrentStatusCode;
	}
	
	

	public String getCoverageCurrentStatusSubCode() {
		return coverageCurrentStatusSubCode;
	}

	public void setCoverageCurrentStatusSubCode(String coverageCurrentStatusSubCode) {
		this.coverageCurrentStatusSubCode = coverageCurrentStatusSubCode;
	}

	public BigDecimal getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(BigDecimal sumInsured) {
		this.sumInsured = sumInsured;
	}

	public Date getDateStatusChange() {
		return dateStatusChange;
	}

	public void setDateStatusChange(Date dateStatusChange) {
		this.dateStatusChange = dateStatusChange;
	}

	public Date getReInStatementDate() {
		return reInStatementDate;
	}

	public void setReInStatementDate(Date reInStatementDate) {
		this.reInStatementDate = reInStatementDate;
	}

	public String getVoidingPeriod() {
		return voidingPeriod;
	}

	public void setVoidingPeriod(String voidingPeriod) {
		this.voidingPeriod = voidingPeriod;
	}

	public Integer getWaitingDays() {
		return waitingDays;
	}

	public void setWaitingDays(Integer waitingDays) {
		this.waitingDays = waitingDays;
	}

	public String getClassOfBusiness() {
		return classOfBusiness;
	}

	public void setClassOfBusiness(String classOfBusiness) {
		this.classOfBusiness = classOfBusiness;
	}

	public String getSupplementaryBenefit() {
		return supplementaryBenefit;
	}

	public void setSupplementaryBenefit(String supplementaryBenefit) {
		this.supplementaryBenefit = supplementaryBenefit;
	}

	public String getTypeInsurance() {
		return typeInsurance;
	}

	public void setTypeInsurance(String typeInsurance) {
		this.typeInsurance = typeInsurance;
	}

	public String getUwGroupKey() {
		return uwGroupKey;
	}

	public void setUwGroupKey(String uwGroupKey) {
		this.uwGroupKey = uwGroupKey;
	}

	public String getNewUwGroupKey() {
		return newUwGroupKey;
	}

	public void setNewUwGroupKey(String newUwGroupKey) {
		this.newUwGroupKey = newUwGroupKey;
	}

	public Date getTempDate() {
		return tempDate;
	}

	public void setTempDate(Date tempDate) {
		this.tempDate = tempDate;
	}
	
}
