package com.mtl.eligible.util;

import java.lang.reflect.Field;

public final class Utils {
	/**
	 * Require org.apache.commons.lang3.reflect.FieldUtils
	 * 
	 * @param obj
	 *            any Object Class
	 * @return Object Class name and all declared field and value as json Object
	 *         String
	 */
	public static String printAll(Object obj) {
		if (obj == null) {
			return "null";
		}

		StringBuilder str = new StringBuilder();
		String newLine = System.getProperty("line.separator");

		str.append(obj.getClass().getName());
		str.append("{");
		str.append(newLine);

		// determine fields declared in this class only (no fields of
		// superclass)
		Field[] fields = obj.getClass().getDeclaredFields();

		// print field names paired with their values
		for (Field field : fields) {
			str.append("\t");
			try {
				str.append("\"").append(field.getName()).append("\"");
				str.append(" : ");
				// requires access to private field:
				if (!field.isAccessible()) {
					field.setAccessible(true);
				}

				if (field.get(obj) != null) {
					str.append("\"").append(field.get(obj)).append("\"");
				}
				else {
					str.append("null");
				}
			}
			catch (IllegalAccessException ex) {
				ex.printStackTrace(System.err);
			}
			str.append(newLine);
		}
		str.append("}");

		return str.toString();
	}

}