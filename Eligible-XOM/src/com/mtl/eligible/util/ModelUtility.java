/*
 * 
 */
package com.mtl.eligible.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * The Class ModelUtility.
 */
public class ModelUtility{
	
	/** The Constant millesecPerDay. */
	private final static int millesecPerDay = 86400000;
	
	/** The Constant ENG_LOCALE. */
	private final static Locale ENG_LOCALE = new Locale("en", "EN");
	
	/**
	 * Instantiates a new model utility.
	 */
	public ModelUtility(){
		
	}
	
	/**
	 * Adds the date.
	 *
	 * @param date the date
	 * @param day the day
	 * @return the date
	 */
	public static Date addDate(Date date, int day){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, day);
		return cal.getTime();
	}
	
	/**
	 * Adds the month.
	 *
	 * @param date the date
	 * @param month the month
	 * @return the date
	 */
	public static Date addMonth(Date date, int month){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		return cal.getTime();
	}
	
	/**
	 * Adds the year.
	 *
	 * @param date the date
	 * @param year the year
	 * @return the date
	 */
	public static Date addYear(Date date, int year){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.YEAR, year);
		return cal.getTime();
	}
	
	/**
	 * Different days.
	 *
	 * @param oldDate the old date
	 * @param newDate the new date
	 * @return the int
	 */
	public static int differentDays(Date oldDate, Date newDate){
		long differentDate = newDate.getTime() - oldDate.getTime();
		differentDate /= millesecPerDay;
		return Math.abs((int)differentDate);
	}
	
	/**
	 * Different date.
	 *
	 * @param oldDate the old date
	 * @param newDate the new date
	 * @return the int
	 */
	public static int differentDate(Date oldDate, Date newDate){
		long differentDate = newDate.getTime() - oldDate.getTime();
		differentDate /= millesecPerDay;
		return (int)differentDate;
	}
	
	/**
	 * Max.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the double
	 */
	public static double max(double a, double b){
		return Math.max(a, b);
	}
	
	/**
	 * Min.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the double
	 */
	public static double min(double a, double b){
		return Math.min(a, b);
	}
	
	/**
	 * Floor.
	 *
	 * @param a the a
	 * @return the double
	 */
	public static double floor(double a){
		return Math.floor(a);
	}
	
	/**
	 * Encode model.
	 *
	 * @param model the model
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String encodeModel(Object model) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(model);
        oos.close();
        return new String( Base64Coder.encode( baos.toByteArray() ) );
	}
	
	/**
	 * Decode model.
	 *
	 * @param stringModel the string model
	 * @return the object
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static Object decodeModel(String stringModel) throws IOException, ClassNotFoundException {
		byte [] data = Base64Coder.decode(stringModel);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o  = (Object) ois.readObject();
        ois.close();
        return o;
	}
	
	/**
	 * Prints the out.
	 *
	 * @param text the text
	 */
	public static void printOut(String text){
		System.out.println(text);
	}
	
	public static void printObj(Object o){
		System.out.println(String.valueOf(o));
	}
	
	public static String toUpperCase(String str){
		return str != null ? str.toUpperCase() : null;
	}
	
	public static String toLowerCase(String str){
		return str != null ? str.toLowerCase() : null;
	}
	
	/**
	 * Between.
	 *
	 * @param start the start
	 * @param end the end
	 * @param sample the sample
	 * @return true, if successful
	 */
	public static boolean between(String start, String end, String sample){
		return sample.compareTo(start) >= 0 && sample.compareTo(end) <= 0;
	}
	
	/**
	 * Gets the date to string.
	 *
	 * @param date the date
	 * @return the date to string
	 */
	public static String getDateToString(Date date){
		if(date == null){
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy", ENG_LOCALE);
		return sf.format(date);
	}
	
	@SuppressWarnings("deprecation")
	public static int getDiffMonth(Date beginDate,Date afterDate){
		int diffMonth = 0;
		if(beginDate != null && afterDate != null){
			int diffYear = afterDate.getYear() - beginDate.getYear();
			diffMonth = diffYear * 12 + afterDate.getMonth() - beginDate.getMonth();
			int diffDay = afterDate.getDay() - beginDate.getDay();
			if(diffDay > 0){
				diffMonth = diffMonth + 1;
			}
		}
		return diffMonth;
	}
	
	@SuppressWarnings("deprecation")
	public static Date getMaxDate(Date requestDate, Date issueDate){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(requestDate);
		int maxDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		if(issueDate.getDate() > requestDate.getDate()){
			if(issueDate.getDate() > maxDate)
				requestDate.setDate(maxDate);
			else
				requestDate.setDate(issueDate.getDate());
		}
		
		return requestDate;
	}
	
	public static boolean isOneOf(String source,String target){
		//return true if the target is one of the source.
		boolean result=false;
		if(source==null||source.length()==0||"".equals(source)){
			result = false;
		}else{
			source = ","+source+",";
			target = ","+target+",";
			int i = source.indexOf(target);
			if(i!=-1){
				result=true;
			}
		}		
		return result;
	}
	
}
