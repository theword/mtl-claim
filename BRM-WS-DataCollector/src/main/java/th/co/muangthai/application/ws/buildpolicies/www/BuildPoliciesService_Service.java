/**
 * BuildPoliciesService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildpolicies.www;

public interface BuildPoliciesService_Service extends javax.xml.rpc.Service {
    public java.lang.String getBuildPoliciesWsImplPortAddress();

    public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType getBuildPoliciesWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType getBuildPoliciesWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
