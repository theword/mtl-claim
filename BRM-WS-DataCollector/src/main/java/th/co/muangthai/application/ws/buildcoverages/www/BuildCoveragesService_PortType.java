/**
 * BuildCoveragesService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildcoverages.www;

public interface BuildCoveragesService_PortType extends java.rmi.Remote {
    public th.co.muangthai.application.ws.buildcoverages.www.HeaderBuildCoveragesResponse buildCoverages(th.co.muangthai.application.ws.buildcoverages.www.HeaderBuildCoveragesRequest request) throws java.rmi.RemoteException;
}
