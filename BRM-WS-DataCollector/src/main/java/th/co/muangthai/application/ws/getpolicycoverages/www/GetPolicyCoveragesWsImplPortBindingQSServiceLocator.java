/**
 * GetPolicyCoveragesWsImplPortBindingQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicycoverages.www;

public class GetPolicyCoveragesWsImplPortBindingQSServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingQSService {

/**
 * OSB Service
 */

    public GetPolicyCoveragesWsImplPortBindingQSServiceLocator() {
    }


    public GetPolicyCoveragesWsImplPortBindingQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetPolicyCoveragesWsImplPortBindingQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetPolicyCoveragesWsImplPortBindingQSPort
    private java.lang.String GetPolicyCoveragesWsImplPortBindingQSPort_address = "http://uat-osb.muangthai.co.th:9606/MTLSOA_CLM085_GetPolicyCoveragesServicePS/proxy-service/MTLSOA_CLM085_GetPolicyCoveragesPS";

    public java.lang.String getGetPolicyCoveragesWsImplPortBindingQSPortAddress() {
        return GetPolicyCoveragesWsImplPortBindingQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName = "GetPolicyCoveragesWsImplPortBindingQSPort";

    public java.lang.String getGetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName() {
        return GetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName;
    }

    public void setGetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName(java.lang.String name) {
        GetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService getGetPolicyCoveragesWsImplPortBindingQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetPolicyCoveragesWsImplPortBindingQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetPolicyCoveragesWsImplPortBindingQSPort(endpoint);
    }

    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService getGetPolicyCoveragesWsImplPortBindingQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getGetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetPolicyCoveragesWsImplPortBindingQSPortEndpointAddress(java.lang.String address) {
        GetPolicyCoveragesWsImplPortBindingQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingStub(new java.net.URL(GetPolicyCoveragesWsImplPortBindingQSPort_address), this);
                _stub.setPortName(getGetPolicyCoveragesWsImplPortBindingQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetPolicyCoveragesWsImplPortBindingQSPort".equals(inputPortName)) {
            return getGetPolicyCoveragesWsImplPortBindingQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.getpolicycoverages.ws.application.muangthai.co.th/", "GetPolicyCoveragesWsImplPortBindingQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.getpolicycoverages.ws.application.muangthai.co.th/", "GetPolicyCoveragesWsImplPortBindingQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetPolicyCoveragesWsImplPortBindingQSPort".equals(portName)) {
            setGetPolicyCoveragesWsImplPortBindingQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
