package th.co.muangthai.application.ws.buildpolicies.www;

public class BuildPoliciesServiceProxy implements th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType buildPoliciesService_PortType = null;
  
  public BuildPoliciesServiceProxy() {
    _initBuildPoliciesServiceProxy();
  }
  
  public BuildPoliciesServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initBuildPoliciesServiceProxy();
  }
  
  private void _initBuildPoliciesServiceProxy() {
    try {
      buildPoliciesService_PortType = (new th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_ServiceLocator()).getBuildPoliciesWsImplPort();
      if (buildPoliciesService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)buildPoliciesService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)buildPoliciesService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (buildPoliciesService_PortType != null)
      ((javax.xml.rpc.Stub)buildPoliciesService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType getBuildPoliciesService_PortType() {
    if (buildPoliciesService_PortType == null)
      _initBuildPoliciesServiceProxy();
    return buildPoliciesService_PortType;
  }
  
  public th.co.muangthai.application.ws.buildpolicies.www.HeaderBuildPoliciesResponse buildPolicies(th.co.muangthai.application.ws.buildpolicies.www.HeaderBuildPoliciesRequest request) throws java.rmi.RemoteException{
    if (buildPoliciesService_PortType == null)
      _initBuildPoliciesServiceProxy();
    return buildPoliciesService_PortType.buildPolicies(request);
  }
  
  
}