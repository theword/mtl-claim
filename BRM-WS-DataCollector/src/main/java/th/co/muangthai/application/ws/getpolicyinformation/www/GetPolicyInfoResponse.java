/**
 * GetPolicyInfoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInfoResponse  extends th.co.muangthai.application.ws.getpolicyinformation.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy[] getPolicyInfoResponsePolicyList;

    public GetPolicyInfoResponse() {
    }

    public GetPolicyInfoResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy[] getPolicyInfoResponsePolicyList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.getPolicyInfoResponsePolicyList = getPolicyInfoResponsePolicyList;
    }


    /**
     * Gets the getPolicyInfoResponsePolicyList value for this GetPolicyInfoResponse.
     * 
     * @return getPolicyInfoResponsePolicyList
     */
    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy[] getGetPolicyInfoResponsePolicyList() {
        return getPolicyInfoResponsePolicyList;
    }


    /**
     * Sets the getPolicyInfoResponsePolicyList value for this GetPolicyInfoResponse.
     * 
     * @param getPolicyInfoResponsePolicyList
     */
    public void setGetPolicyInfoResponsePolicyList(th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy[] getPolicyInfoResponsePolicyList) {
        this.getPolicyInfoResponsePolicyList = getPolicyInfoResponsePolicyList;
    }

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy getGetPolicyInfoResponsePolicyList(int i) {
        return this.getPolicyInfoResponsePolicyList[i];
    }

    public void setGetPolicyInfoResponsePolicyList(int i, th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy _value) {
        this.getPolicyInfoResponsePolicyList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyInfoResponse)) return false;
        GetPolicyInfoResponse other = (GetPolicyInfoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.getPolicyInfoResponsePolicyList==null && other.getGetPolicyInfoResponsePolicyList()==null) || 
             (this.getPolicyInfoResponsePolicyList!=null &&
              java.util.Arrays.equals(this.getPolicyInfoResponsePolicyList, other.getGetPolicyInfoResponsePolicyList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGetPolicyInfoResponsePolicyList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyInfoResponsePolicyList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyInfoResponsePolicyList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyInfoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyInfoResponsePolicyList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getPolicyInfoResponsePolicyList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponsePolicy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
