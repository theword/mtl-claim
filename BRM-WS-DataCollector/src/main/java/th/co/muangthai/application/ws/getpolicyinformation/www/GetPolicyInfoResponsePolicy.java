/**
 * GetPolicyInfoResponsePolicy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInfoResponsePolicy  implements java.io.Serializable {
    private java.lang.String policyNumber;

    private java.lang.String clientNumber;

    private java.lang.String planCode;

    private java.lang.String issueDate;

    private java.lang.String reInStatementDate;

    private java.lang.String POCurrentStatusCode;

    private java.lang.String poCurrentSubStatusCode;

    private java.lang.String poDateStatusChange;

    private java.lang.String policyComment;

    private java.lang.String modeOfPremium;

    private java.lang.String agentBranch;

    private java.lang.String agentCode;

    private java.lang.String agentPlacement;

    private java.lang.String paymentMode;

    private java.lang.String mailingAddress;

    private java.lang.String city;

    private java.lang.String postalCode;

    private java.lang.String addressCode;

    private java.lang.String bankIndicator;

    private java.lang.String bankClientNumber;

    private java.lang.String bankAccountNumber;

    private java.lang.String addressIndicator;

    private java.lang.String ownerClientNumber;

    private java.lang.String addressClientNumber;

    private java.lang.String approvedDate;

    private java.lang.String relationship;

    private java.lang.String voidingPeriod;

    private java.lang.String planDesc;

    private java.lang.String bankAccountName;

    private th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage[] getPolicyInfoResponseCoverageList;

    public GetPolicyInfoResponsePolicy() {
    }

    public GetPolicyInfoResponsePolicy(
           java.lang.String policyNumber,
           java.lang.String clientNumber,
           java.lang.String planCode,
           java.lang.String issueDate,
           java.lang.String reInStatementDate,
           java.lang.String POCurrentStatusCode,
           java.lang.String poCurrentSubStatusCode,
           java.lang.String poDateStatusChange,
           java.lang.String policyComment,
           java.lang.String modeOfPremium,
           java.lang.String agentBranch,
           java.lang.String agentCode,
           java.lang.String agentPlacement,
           java.lang.String paymentMode,
           java.lang.String mailingAddress,
           java.lang.String city,
           java.lang.String postalCode,
           java.lang.String addressCode,
           java.lang.String bankIndicator,
           java.lang.String bankClientNumber,
           java.lang.String bankAccountNumber,
           java.lang.String addressIndicator,
           java.lang.String ownerClientNumber,
           java.lang.String addressClientNumber,
           java.lang.String approvedDate,
           java.lang.String relationship,
           java.lang.String voidingPeriod,
           java.lang.String planDesc,
           java.lang.String bankAccountName,
           th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage[] getPolicyInfoResponseCoverageList) {
           this.policyNumber = policyNumber;
           this.clientNumber = clientNumber;
           this.planCode = planCode;
           this.issueDate = issueDate;
           this.reInStatementDate = reInStatementDate;
           this.POCurrentStatusCode = POCurrentStatusCode;
           this.poCurrentSubStatusCode = poCurrentSubStatusCode;
           this.poDateStatusChange = poDateStatusChange;
           this.policyComment = policyComment;
           this.modeOfPremium = modeOfPremium;
           this.agentBranch = agentBranch;
           this.agentCode = agentCode;
           this.agentPlacement = agentPlacement;
           this.paymentMode = paymentMode;
           this.mailingAddress = mailingAddress;
           this.city = city;
           this.postalCode = postalCode;
           this.addressCode = addressCode;
           this.bankIndicator = bankIndicator;
           this.bankClientNumber = bankClientNumber;
           this.bankAccountNumber = bankAccountNumber;
           this.addressIndicator = addressIndicator;
           this.ownerClientNumber = ownerClientNumber;
           this.addressClientNumber = addressClientNumber;
           this.approvedDate = approvedDate;
           this.relationship = relationship;
           this.voidingPeriod = voidingPeriod;
           this.planDesc = planDesc;
           this.bankAccountName = bankAccountName;
           this.getPolicyInfoResponseCoverageList = getPolicyInfoResponseCoverageList;
    }


    /**
     * Gets the policyNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @return policyNumber
     */
    public java.lang.String getPolicyNumber() {
        return policyNumber;
    }


    /**
     * Sets the policyNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @param policyNumber
     */
    public void setPolicyNumber(java.lang.String policyNumber) {
        this.policyNumber = policyNumber;
    }


    /**
     * Gets the clientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @return clientNumber
     */
    public java.lang.String getClientNumber() {
        return clientNumber;
    }


    /**
     * Sets the clientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @param clientNumber
     */
    public void setClientNumber(java.lang.String clientNumber) {
        this.clientNumber = clientNumber;
    }


    /**
     * Gets the planCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return planCode
     */
    public java.lang.String getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String planCode) {
        this.planCode = planCode;
    }


    /**
     * Gets the issueDate value for this GetPolicyInfoResponsePolicy.
     * 
     * @return issueDate
     */
    public java.lang.String getIssueDate() {
        return issueDate;
    }


    /**
     * Sets the issueDate value for this GetPolicyInfoResponsePolicy.
     * 
     * @param issueDate
     */
    public void setIssueDate(java.lang.String issueDate) {
        this.issueDate = issueDate;
    }


    /**
     * Gets the reInStatementDate value for this GetPolicyInfoResponsePolicy.
     * 
     * @return reInStatementDate
     */
    public java.lang.String getReInStatementDate() {
        return reInStatementDate;
    }


    /**
     * Sets the reInStatementDate value for this GetPolicyInfoResponsePolicy.
     * 
     * @param reInStatementDate
     */
    public void setReInStatementDate(java.lang.String reInStatementDate) {
        this.reInStatementDate = reInStatementDate;
    }


    /**
     * Gets the POCurrentStatusCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return POCurrentStatusCode
     */
    public java.lang.String getPOCurrentStatusCode() {
        return POCurrentStatusCode;
    }


    /**
     * Sets the POCurrentStatusCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param POCurrentStatusCode
     */
    public void setPOCurrentStatusCode(java.lang.String POCurrentStatusCode) {
        this.POCurrentStatusCode = POCurrentStatusCode;
    }


    /**
     * Gets the poCurrentSubStatusCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return poCurrentSubStatusCode
     */
    public java.lang.String getPoCurrentSubStatusCode() {
        return poCurrentSubStatusCode;
    }


    /**
     * Sets the poCurrentSubStatusCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param poCurrentSubStatusCode
     */
    public void setPoCurrentSubStatusCode(java.lang.String poCurrentSubStatusCode) {
        this.poCurrentSubStatusCode = poCurrentSubStatusCode;
    }


    /**
     * Gets the poDateStatusChange value for this GetPolicyInfoResponsePolicy.
     * 
     * @return poDateStatusChange
     */
    public java.lang.String getPoDateStatusChange() {
        return poDateStatusChange;
    }


    /**
     * Sets the poDateStatusChange value for this GetPolicyInfoResponsePolicy.
     * 
     * @param poDateStatusChange
     */
    public void setPoDateStatusChange(java.lang.String poDateStatusChange) {
        this.poDateStatusChange = poDateStatusChange;
    }


    /**
     * Gets the policyComment value for this GetPolicyInfoResponsePolicy.
     * 
     * @return policyComment
     */
    public java.lang.String getPolicyComment() {
        return policyComment;
    }


    /**
     * Sets the policyComment value for this GetPolicyInfoResponsePolicy.
     * 
     * @param policyComment
     */
    public void setPolicyComment(java.lang.String policyComment) {
        this.policyComment = policyComment;
    }


    /**
     * Gets the modeOfPremium value for this GetPolicyInfoResponsePolicy.
     * 
     * @return modeOfPremium
     */
    public java.lang.String getModeOfPremium() {
        return modeOfPremium;
    }


    /**
     * Sets the modeOfPremium value for this GetPolicyInfoResponsePolicy.
     * 
     * @param modeOfPremium
     */
    public void setModeOfPremium(java.lang.String modeOfPremium) {
        this.modeOfPremium = modeOfPremium;
    }


    /**
     * Gets the agentBranch value for this GetPolicyInfoResponsePolicy.
     * 
     * @return agentBranch
     */
    public java.lang.String getAgentBranch() {
        return agentBranch;
    }


    /**
     * Sets the agentBranch value for this GetPolicyInfoResponsePolicy.
     * 
     * @param agentBranch
     */
    public void setAgentBranch(java.lang.String agentBranch) {
        this.agentBranch = agentBranch;
    }


    /**
     * Gets the agentCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return agentCode
     */
    public java.lang.String getAgentCode() {
        return agentCode;
    }


    /**
     * Sets the agentCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param agentCode
     */
    public void setAgentCode(java.lang.String agentCode) {
        this.agentCode = agentCode;
    }


    /**
     * Gets the agentPlacement value for this GetPolicyInfoResponsePolicy.
     * 
     * @return agentPlacement
     */
    public java.lang.String getAgentPlacement() {
        return agentPlacement;
    }


    /**
     * Sets the agentPlacement value for this GetPolicyInfoResponsePolicy.
     * 
     * @param agentPlacement
     */
    public void setAgentPlacement(java.lang.String agentPlacement) {
        this.agentPlacement = agentPlacement;
    }


    /**
     * Gets the paymentMode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return paymentMode
     */
    public java.lang.String getPaymentMode() {
        return paymentMode;
    }


    /**
     * Sets the paymentMode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param paymentMode
     */
    public void setPaymentMode(java.lang.String paymentMode) {
        this.paymentMode = paymentMode;
    }


    /**
     * Gets the mailingAddress value for this GetPolicyInfoResponsePolicy.
     * 
     * @return mailingAddress
     */
    public java.lang.String getMailingAddress() {
        return mailingAddress;
    }


    /**
     * Sets the mailingAddress value for this GetPolicyInfoResponsePolicy.
     * 
     * @param mailingAddress
     */
    public void setMailingAddress(java.lang.String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }


    /**
     * Gets the city value for this GetPolicyInfoResponsePolicy.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this GetPolicyInfoResponsePolicy.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the postalCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return postalCode
     */
    public java.lang.String getPostalCode() {
        return postalCode;
    }


    /**
     * Sets the postalCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param postalCode
     */
    public void setPostalCode(java.lang.String postalCode) {
        this.postalCode = postalCode;
    }


    /**
     * Gets the addressCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @return addressCode
     */
    public java.lang.String getAddressCode() {
        return addressCode;
    }


    /**
     * Sets the addressCode value for this GetPolicyInfoResponsePolicy.
     * 
     * @param addressCode
     */
    public void setAddressCode(java.lang.String addressCode) {
        this.addressCode = addressCode;
    }


    /**
     * Gets the bankIndicator value for this GetPolicyInfoResponsePolicy.
     * 
     * @return bankIndicator
     */
    public java.lang.String getBankIndicator() {
        return bankIndicator;
    }


    /**
     * Sets the bankIndicator value for this GetPolicyInfoResponsePolicy.
     * 
     * @param bankIndicator
     */
    public void setBankIndicator(java.lang.String bankIndicator) {
        this.bankIndicator = bankIndicator;
    }


    /**
     * Gets the bankClientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @return bankClientNumber
     */
    public java.lang.String getBankClientNumber() {
        return bankClientNumber;
    }


    /**
     * Sets the bankClientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @param bankClientNumber
     */
    public void setBankClientNumber(java.lang.String bankClientNumber) {
        this.bankClientNumber = bankClientNumber;
    }


    /**
     * Gets the bankAccountNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @return bankAccountNumber
     */
    public java.lang.String getBankAccountNumber() {
        return bankAccountNumber;
    }


    /**
     * Sets the bankAccountNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @param bankAccountNumber
     */
    public void setBankAccountNumber(java.lang.String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }


    /**
     * Gets the addressIndicator value for this GetPolicyInfoResponsePolicy.
     * 
     * @return addressIndicator
     */
    public java.lang.String getAddressIndicator() {
        return addressIndicator;
    }


    /**
     * Sets the addressIndicator value for this GetPolicyInfoResponsePolicy.
     * 
     * @param addressIndicator
     */
    public void setAddressIndicator(java.lang.String addressIndicator) {
        this.addressIndicator = addressIndicator;
    }


    /**
     * Gets the ownerClientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @return ownerClientNumber
     */
    public java.lang.String getOwnerClientNumber() {
        return ownerClientNumber;
    }


    /**
     * Sets the ownerClientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @param ownerClientNumber
     */
    public void setOwnerClientNumber(java.lang.String ownerClientNumber) {
        this.ownerClientNumber = ownerClientNumber;
    }


    /**
     * Gets the addressClientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @return addressClientNumber
     */
    public java.lang.String getAddressClientNumber() {
        return addressClientNumber;
    }


    /**
     * Sets the addressClientNumber value for this GetPolicyInfoResponsePolicy.
     * 
     * @param addressClientNumber
     */
    public void setAddressClientNumber(java.lang.String addressClientNumber) {
        this.addressClientNumber = addressClientNumber;
    }


    /**
     * Gets the approvedDate value for this GetPolicyInfoResponsePolicy.
     * 
     * @return approvedDate
     */
    public java.lang.String getApprovedDate() {
        return approvedDate;
    }


    /**
     * Sets the approvedDate value for this GetPolicyInfoResponsePolicy.
     * 
     * @param approvedDate
     */
    public void setApprovedDate(java.lang.String approvedDate) {
        this.approvedDate = approvedDate;
    }


    /**
     * Gets the relationship value for this GetPolicyInfoResponsePolicy.
     * 
     * @return relationship
     */
    public java.lang.String getRelationship() {
        return relationship;
    }


    /**
     * Sets the relationship value for this GetPolicyInfoResponsePolicy.
     * 
     * @param relationship
     */
    public void setRelationship(java.lang.String relationship) {
        this.relationship = relationship;
    }


    /**
     * Gets the voidingPeriod value for this GetPolicyInfoResponsePolicy.
     * 
     * @return voidingPeriod
     */
    public java.lang.String getVoidingPeriod() {
        return voidingPeriod;
    }


    /**
     * Sets the voidingPeriod value for this GetPolicyInfoResponsePolicy.
     * 
     * @param voidingPeriod
     */
    public void setVoidingPeriod(java.lang.String voidingPeriod) {
        this.voidingPeriod = voidingPeriod;
    }


    /**
     * Gets the planDesc value for this GetPolicyInfoResponsePolicy.
     * 
     * @return planDesc
     */
    public java.lang.String getPlanDesc() {
        return planDesc;
    }


    /**
     * Sets the planDesc value for this GetPolicyInfoResponsePolicy.
     * 
     * @param planDesc
     */
    public void setPlanDesc(java.lang.String planDesc) {
        this.planDesc = planDesc;
    }


    /**
     * Gets the bankAccountName value for this GetPolicyInfoResponsePolicy.
     * 
     * @return bankAccountName
     */
    public java.lang.String getBankAccountName() {
        return bankAccountName;
    }


    /**
     * Sets the bankAccountName value for this GetPolicyInfoResponsePolicy.
     * 
     * @param bankAccountName
     */
    public void setBankAccountName(java.lang.String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }


    /**
     * Gets the getPolicyInfoResponseCoverageList value for this GetPolicyInfoResponsePolicy.
     * 
     * @return getPolicyInfoResponseCoverageList
     */
    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage[] getGetPolicyInfoResponseCoverageList() {
        return getPolicyInfoResponseCoverageList;
    }


    /**
     * Sets the getPolicyInfoResponseCoverageList value for this GetPolicyInfoResponsePolicy.
     * 
     * @param getPolicyInfoResponseCoverageList
     */
    public void setGetPolicyInfoResponseCoverageList(th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage[] getPolicyInfoResponseCoverageList) {
        this.getPolicyInfoResponseCoverageList = getPolicyInfoResponseCoverageList;
    }

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage getGetPolicyInfoResponseCoverageList(int i) {
        return this.getPolicyInfoResponseCoverageList[i];
    }

    public void setGetPolicyInfoResponseCoverageList(int i, th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage _value) {
        this.getPolicyInfoResponseCoverageList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyInfoResponsePolicy)) return false;
        GetPolicyInfoResponsePolicy other = (GetPolicyInfoResponsePolicy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.policyNumber==null && other.getPolicyNumber()==null) || 
             (this.policyNumber!=null &&
              this.policyNumber.equals(other.getPolicyNumber()))) &&
            ((this.clientNumber==null && other.getClientNumber()==null) || 
             (this.clientNumber!=null &&
              this.clientNumber.equals(other.getClientNumber()))) &&
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              this.planCode.equals(other.getPlanCode()))) &&
            ((this.issueDate==null && other.getIssueDate()==null) || 
             (this.issueDate!=null &&
              this.issueDate.equals(other.getIssueDate()))) &&
            ((this.reInStatementDate==null && other.getReInStatementDate()==null) || 
             (this.reInStatementDate!=null &&
              this.reInStatementDate.equals(other.getReInStatementDate()))) &&
            ((this.POCurrentStatusCode==null && other.getPOCurrentStatusCode()==null) || 
             (this.POCurrentStatusCode!=null &&
              this.POCurrentStatusCode.equals(other.getPOCurrentStatusCode()))) &&
            ((this.poCurrentSubStatusCode==null && other.getPoCurrentSubStatusCode()==null) || 
             (this.poCurrentSubStatusCode!=null &&
              this.poCurrentSubStatusCode.equals(other.getPoCurrentSubStatusCode()))) &&
            ((this.poDateStatusChange==null && other.getPoDateStatusChange()==null) || 
             (this.poDateStatusChange!=null &&
              this.poDateStatusChange.equals(other.getPoDateStatusChange()))) &&
            ((this.policyComment==null && other.getPolicyComment()==null) || 
             (this.policyComment!=null &&
              this.policyComment.equals(other.getPolicyComment()))) &&
            ((this.modeOfPremium==null && other.getModeOfPremium()==null) || 
             (this.modeOfPremium!=null &&
              this.modeOfPremium.equals(other.getModeOfPremium()))) &&
            ((this.agentBranch==null && other.getAgentBranch()==null) || 
             (this.agentBranch!=null &&
              this.agentBranch.equals(other.getAgentBranch()))) &&
            ((this.agentCode==null && other.getAgentCode()==null) || 
             (this.agentCode!=null &&
              this.agentCode.equals(other.getAgentCode()))) &&
            ((this.agentPlacement==null && other.getAgentPlacement()==null) || 
             (this.agentPlacement!=null &&
              this.agentPlacement.equals(other.getAgentPlacement()))) &&
            ((this.paymentMode==null && other.getPaymentMode()==null) || 
             (this.paymentMode!=null &&
              this.paymentMode.equals(other.getPaymentMode()))) &&
            ((this.mailingAddress==null && other.getMailingAddress()==null) || 
             (this.mailingAddress!=null &&
              this.mailingAddress.equals(other.getMailingAddress()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.postalCode==null && other.getPostalCode()==null) || 
             (this.postalCode!=null &&
              this.postalCode.equals(other.getPostalCode()))) &&
            ((this.addressCode==null && other.getAddressCode()==null) || 
             (this.addressCode!=null &&
              this.addressCode.equals(other.getAddressCode()))) &&
            ((this.bankIndicator==null && other.getBankIndicator()==null) || 
             (this.bankIndicator!=null &&
              this.bankIndicator.equals(other.getBankIndicator()))) &&
            ((this.bankClientNumber==null && other.getBankClientNumber()==null) || 
             (this.bankClientNumber!=null &&
              this.bankClientNumber.equals(other.getBankClientNumber()))) &&
            ((this.bankAccountNumber==null && other.getBankAccountNumber()==null) || 
             (this.bankAccountNumber!=null &&
              this.bankAccountNumber.equals(other.getBankAccountNumber()))) &&
            ((this.addressIndicator==null && other.getAddressIndicator()==null) || 
             (this.addressIndicator!=null &&
              this.addressIndicator.equals(other.getAddressIndicator()))) &&
            ((this.ownerClientNumber==null && other.getOwnerClientNumber()==null) || 
             (this.ownerClientNumber!=null &&
              this.ownerClientNumber.equals(other.getOwnerClientNumber()))) &&
            ((this.addressClientNumber==null && other.getAddressClientNumber()==null) || 
             (this.addressClientNumber!=null &&
              this.addressClientNumber.equals(other.getAddressClientNumber()))) &&
            ((this.approvedDate==null && other.getApprovedDate()==null) || 
             (this.approvedDate!=null &&
              this.approvedDate.equals(other.getApprovedDate()))) &&
            ((this.relationship==null && other.getRelationship()==null) || 
             (this.relationship!=null &&
              this.relationship.equals(other.getRelationship()))) &&
            ((this.voidingPeriod==null && other.getVoidingPeriod()==null) || 
             (this.voidingPeriod!=null &&
              this.voidingPeriod.equals(other.getVoidingPeriod()))) &&
            ((this.planDesc==null && other.getPlanDesc()==null) || 
             (this.planDesc!=null &&
              this.planDesc.equals(other.getPlanDesc()))) &&
            ((this.bankAccountName==null && other.getBankAccountName()==null) || 
             (this.bankAccountName!=null &&
              this.bankAccountName.equals(other.getBankAccountName()))) &&
            ((this.getPolicyInfoResponseCoverageList==null && other.getGetPolicyInfoResponseCoverageList()==null) || 
             (this.getPolicyInfoResponseCoverageList!=null &&
              java.util.Arrays.equals(this.getPolicyInfoResponseCoverageList, other.getGetPolicyInfoResponseCoverageList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPolicyNumber() != null) {
            _hashCode += getPolicyNumber().hashCode();
        }
        if (getClientNumber() != null) {
            _hashCode += getClientNumber().hashCode();
        }
        if (getPlanCode() != null) {
            _hashCode += getPlanCode().hashCode();
        }
        if (getIssueDate() != null) {
            _hashCode += getIssueDate().hashCode();
        }
        if (getReInStatementDate() != null) {
            _hashCode += getReInStatementDate().hashCode();
        }
        if (getPOCurrentStatusCode() != null) {
            _hashCode += getPOCurrentStatusCode().hashCode();
        }
        if (getPoCurrentSubStatusCode() != null) {
            _hashCode += getPoCurrentSubStatusCode().hashCode();
        }
        if (getPoDateStatusChange() != null) {
            _hashCode += getPoDateStatusChange().hashCode();
        }
        if (getPolicyComment() != null) {
            _hashCode += getPolicyComment().hashCode();
        }
        if (getModeOfPremium() != null) {
            _hashCode += getModeOfPremium().hashCode();
        }
        if (getAgentBranch() != null) {
            _hashCode += getAgentBranch().hashCode();
        }
        if (getAgentCode() != null) {
            _hashCode += getAgentCode().hashCode();
        }
        if (getAgentPlacement() != null) {
            _hashCode += getAgentPlacement().hashCode();
        }
        if (getPaymentMode() != null) {
            _hashCode += getPaymentMode().hashCode();
        }
        if (getMailingAddress() != null) {
            _hashCode += getMailingAddress().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getPostalCode() != null) {
            _hashCode += getPostalCode().hashCode();
        }
        if (getAddressCode() != null) {
            _hashCode += getAddressCode().hashCode();
        }
        if (getBankIndicator() != null) {
            _hashCode += getBankIndicator().hashCode();
        }
        if (getBankClientNumber() != null) {
            _hashCode += getBankClientNumber().hashCode();
        }
        if (getBankAccountNumber() != null) {
            _hashCode += getBankAccountNumber().hashCode();
        }
        if (getAddressIndicator() != null) {
            _hashCode += getAddressIndicator().hashCode();
        }
        if (getOwnerClientNumber() != null) {
            _hashCode += getOwnerClientNumber().hashCode();
        }
        if (getAddressClientNumber() != null) {
            _hashCode += getAddressClientNumber().hashCode();
        }
        if (getApprovedDate() != null) {
            _hashCode += getApprovedDate().hashCode();
        }
        if (getRelationship() != null) {
            _hashCode += getRelationship().hashCode();
        }
        if (getVoidingPeriod() != null) {
            _hashCode += getVoidingPeriod().hashCode();
        }
        if (getPlanDesc() != null) {
            _hashCode += getPlanDesc().hashCode();
        }
        if (getBankAccountName() != null) {
            _hashCode += getBankAccountName().hashCode();
        }
        if (getGetPolicyInfoResponseCoverageList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyInfoResponseCoverageList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyInfoResponseCoverageList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyInfoResponsePolicy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponsePolicy"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ClientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PlanCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IssueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reInStatementDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReInStatementDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POCurrentStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "POCurrentStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("poCurrentSubStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "poCurrentSubStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("poDateStatusChange");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PoDateStatusChange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyComment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyComment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modeOfPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ModeOfPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AgentBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentPlacement");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AgentPlacement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PaymentMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mailingAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MailingAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AddressCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankClientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankClientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AddressIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerClientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OwnerClientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressClientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AddressClientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ApprovedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationship");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Relationship"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voidingPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VoidingPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PlanDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAccountName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankAccountName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyInfoResponseCoverageList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getPolicyInfoResponseCoverageList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponseCoverage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
