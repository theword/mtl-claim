/**
 * BuildPoliciesService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildpolicies.www;

public interface BuildPoliciesService_PortType extends java.rmi.Remote {
    public th.co.muangthai.application.ws.buildpolicies.www.HeaderBuildPoliciesResponse buildPolicies(th.co.muangthai.application.ws.buildpolicies.www.HeaderBuildPoliciesRequest request) throws java.rmi.RemoteException;
}
