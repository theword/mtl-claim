/**
 * GetPolicyByClientService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicybyclient.www;

public class GetPolicyByClientService_ServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_Service {

/**
 * OSB Service
 */

    public GetPolicyByClientService_ServiceLocator() {
    }


    public GetPolicyByClientService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetPolicyByClientService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetPolicyByClientWsImplPort
    private java.lang.String GetPolicyByClientWsImplPort_address = "http://uat-osb.muangthai.co.th:9606/MTLSOA_CLM084_GetPolicyByClientServicePS/proxy-service/MTLSOA_CLM084_GetPolicyByClientServicePS";

    public java.lang.String getGetPolicyByClientWsImplPortAddress() {
        return GetPolicyByClientWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetPolicyByClientWsImplPortWSDDServiceName = "GetPolicyByClientWsImplPort";

    public java.lang.String getGetPolicyByClientWsImplPortWSDDServiceName() {
        return GetPolicyByClientWsImplPortWSDDServiceName;
    }

    public void setGetPolicyByClientWsImplPortWSDDServiceName(java.lang.String name) {
        GetPolicyByClientWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType getGetPolicyByClientWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetPolicyByClientWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetPolicyByClientWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType getGetPolicyByClientWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getGetPolicyByClientWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetPolicyByClientWsImplPortEndpointAddress(java.lang.String address) {
        GetPolicyByClientWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientWsImplPortBindingStub(new java.net.URL(GetPolicyByClientWsImplPort_address), this);
                _stub.setPortName(getGetPolicyByClientWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetPolicyByClientWsImplPort".equals(inputPortName)) {
            return getGetPolicyByClientWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.getpolicybyclient.ws.application.muangthai.co.th/", "GetPolicyByClientService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.getpolicybyclient.ws.application.muangthai.co.th/", "GetPolicyByClientWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetPolicyByClientWsImplPort".equals(portName)) {
            setGetPolicyByClientWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
