/**
 * GetCoverageRelationService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcoveragerelation.www;

public interface GetCoverageRelationService_PortType extends java.rmi.Remote {
    public th.co.muangthai.application.ws.getcoveragerelation.www.HeaderGetCoverageRelationResponse getCoverageRelation(th.co.muangthai.application.ws.getcoveragerelation.www.HeaderGetCoverageRelationRequest request) throws java.rmi.RemoteException;
}
