/**
 * GetPolicyCoveragesResponseList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicycoverages.www;

public class GetPolicyCoveragesResponseList  implements java.io.Serializable {
    private java.lang.String policyNumber;

    private java.lang.String policyStatus;

    private java.lang.String planCode;

    private double modalPremium;

    private int modeOfPremium;

    private java.lang.String issuedDate;

    private java.lang.String billedToDate;

    private java.lang.String paidToDate;

    private java.lang.String coverageCode;

    private java.lang.String coverageStatus;

    private java.lang.String coverageIssuedDate;

    private java.lang.String trailerNumber;

    private double faceAmount;

    public GetPolicyCoveragesResponseList() {
    }

    public GetPolicyCoveragesResponseList(
           java.lang.String policyNumber,
           java.lang.String policyStatus,
           java.lang.String planCode,
           double modalPremium,
           int modeOfPremium,
           java.lang.String issuedDate,
           java.lang.String billedToDate,
           java.lang.String paidToDate,
           java.lang.String coverageCode,
           java.lang.String coverageStatus,
           java.lang.String coverageIssuedDate,
           java.lang.String trailerNumber,
           double faceAmount) {
           this.policyNumber = policyNumber;
           this.policyStatus = policyStatus;
           this.planCode = planCode;
           this.modalPremium = modalPremium;
           this.modeOfPremium = modeOfPremium;
           this.issuedDate = issuedDate;
           this.billedToDate = billedToDate;
           this.paidToDate = paidToDate;
           this.coverageCode = coverageCode;
           this.coverageStatus = coverageStatus;
           this.coverageIssuedDate = coverageIssuedDate;
           this.trailerNumber = trailerNumber;
           this.faceAmount = faceAmount;
    }


    /**
     * Gets the policyNumber value for this GetPolicyCoveragesResponseList.
     * 
     * @return policyNumber
     */
    public java.lang.String getPolicyNumber() {
        return policyNumber;
    }


    /**
     * Sets the policyNumber value for this GetPolicyCoveragesResponseList.
     * 
     * @param policyNumber
     */
    public void setPolicyNumber(java.lang.String policyNumber) {
        this.policyNumber = policyNumber;
    }


    /**
     * Gets the policyStatus value for this GetPolicyCoveragesResponseList.
     * 
     * @return policyStatus
     */
    public java.lang.String getPolicyStatus() {
        return policyStatus;
    }


    /**
     * Sets the policyStatus value for this GetPolicyCoveragesResponseList.
     * 
     * @param policyStatus
     */
    public void setPolicyStatus(java.lang.String policyStatus) {
        this.policyStatus = policyStatus;
    }


    /**
     * Gets the planCode value for this GetPolicyCoveragesResponseList.
     * 
     * @return planCode
     */
    public java.lang.String getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this GetPolicyCoveragesResponseList.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String planCode) {
        this.planCode = planCode;
    }


    /**
     * Gets the modalPremium value for this GetPolicyCoveragesResponseList.
     * 
     * @return modalPremium
     */
    public double getModalPremium() {
        return modalPremium;
    }


    /**
     * Sets the modalPremium value for this GetPolicyCoveragesResponseList.
     * 
     * @param modalPremium
     */
    public void setModalPremium(double modalPremium) {
        this.modalPremium = modalPremium;
    }


    /**
     * Gets the modeOfPremium value for this GetPolicyCoveragesResponseList.
     * 
     * @return modeOfPremium
     */
    public int getModeOfPremium() {
        return modeOfPremium;
    }


    /**
     * Sets the modeOfPremium value for this GetPolicyCoveragesResponseList.
     * 
     * @param modeOfPremium
     */
    public void setModeOfPremium(int modeOfPremium) {
        this.modeOfPremium = modeOfPremium;
    }


    /**
     * Gets the issuedDate value for this GetPolicyCoveragesResponseList.
     * 
     * @return issuedDate
     */
    public java.lang.String getIssuedDate() {
        return issuedDate;
    }


    /**
     * Sets the issuedDate value for this GetPolicyCoveragesResponseList.
     * 
     * @param issuedDate
     */
    public void setIssuedDate(java.lang.String issuedDate) {
        this.issuedDate = issuedDate;
    }


    /**
     * Gets the billedToDate value for this GetPolicyCoveragesResponseList.
     * 
     * @return billedToDate
     */
    public java.lang.String getBilledToDate() {
        return billedToDate;
    }


    /**
     * Sets the billedToDate value for this GetPolicyCoveragesResponseList.
     * 
     * @param billedToDate
     */
    public void setBilledToDate(java.lang.String billedToDate) {
        this.billedToDate = billedToDate;
    }


    /**
     * Gets the paidToDate value for this GetPolicyCoveragesResponseList.
     * 
     * @return paidToDate
     */
    public java.lang.String getPaidToDate() {
        return paidToDate;
    }


    /**
     * Sets the paidToDate value for this GetPolicyCoveragesResponseList.
     * 
     * @param paidToDate
     */
    public void setPaidToDate(java.lang.String paidToDate) {
        this.paidToDate = paidToDate;
    }


    /**
     * Gets the coverageCode value for this GetPolicyCoveragesResponseList.
     * 
     * @return coverageCode
     */
    public java.lang.String getCoverageCode() {
        return coverageCode;
    }


    /**
     * Sets the coverageCode value for this GetPolicyCoveragesResponseList.
     * 
     * @param coverageCode
     */
    public void setCoverageCode(java.lang.String coverageCode) {
        this.coverageCode = coverageCode;
    }


    /**
     * Gets the coverageStatus value for this GetPolicyCoveragesResponseList.
     * 
     * @return coverageStatus
     */
    public java.lang.String getCoverageStatus() {
        return coverageStatus;
    }


    /**
     * Sets the coverageStatus value for this GetPolicyCoveragesResponseList.
     * 
     * @param coverageStatus
     */
    public void setCoverageStatus(java.lang.String coverageStatus) {
        this.coverageStatus = coverageStatus;
    }


    /**
     * Gets the coverageIssuedDate value for this GetPolicyCoveragesResponseList.
     * 
     * @return coverageIssuedDate
     */
    public java.lang.String getCoverageIssuedDate() {
        return coverageIssuedDate;
    }


    /**
     * Sets the coverageIssuedDate value for this GetPolicyCoveragesResponseList.
     * 
     * @param coverageIssuedDate
     */
    public void setCoverageIssuedDate(java.lang.String coverageIssuedDate) {
        this.coverageIssuedDate = coverageIssuedDate;
    }


    /**
     * Gets the trailerNumber value for this GetPolicyCoveragesResponseList.
     * 
     * @return trailerNumber
     */
    public java.lang.String getTrailerNumber() {
        return trailerNumber;
    }


    /**
     * Sets the trailerNumber value for this GetPolicyCoveragesResponseList.
     * 
     * @param trailerNumber
     */
    public void setTrailerNumber(java.lang.String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }


    /**
     * Gets the faceAmount value for this GetPolicyCoveragesResponseList.
     * 
     * @return faceAmount
     */
    public double getFaceAmount() {
        return faceAmount;
    }


    /**
     * Sets the faceAmount value for this GetPolicyCoveragesResponseList.
     * 
     * @param faceAmount
     */
    public void setFaceAmount(double faceAmount) {
        this.faceAmount = faceAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyCoveragesResponseList)) return false;
        GetPolicyCoveragesResponseList other = (GetPolicyCoveragesResponseList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.policyNumber==null && other.getPolicyNumber()==null) || 
             (this.policyNumber!=null &&
              this.policyNumber.equals(other.getPolicyNumber()))) &&
            ((this.policyStatus==null && other.getPolicyStatus()==null) || 
             (this.policyStatus!=null &&
              this.policyStatus.equals(other.getPolicyStatus()))) &&
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              this.planCode.equals(other.getPlanCode()))) &&
            this.modalPremium == other.getModalPremium() &&
            this.modeOfPremium == other.getModeOfPremium() &&
            ((this.issuedDate==null && other.getIssuedDate()==null) || 
             (this.issuedDate!=null &&
              this.issuedDate.equals(other.getIssuedDate()))) &&
            ((this.billedToDate==null && other.getBilledToDate()==null) || 
             (this.billedToDate!=null &&
              this.billedToDate.equals(other.getBilledToDate()))) &&
            ((this.paidToDate==null && other.getPaidToDate()==null) || 
             (this.paidToDate!=null &&
              this.paidToDate.equals(other.getPaidToDate()))) &&
            ((this.coverageCode==null && other.getCoverageCode()==null) || 
             (this.coverageCode!=null &&
              this.coverageCode.equals(other.getCoverageCode()))) &&
            ((this.coverageStatus==null && other.getCoverageStatus()==null) || 
             (this.coverageStatus!=null &&
              this.coverageStatus.equals(other.getCoverageStatus()))) &&
            ((this.coverageIssuedDate==null && other.getCoverageIssuedDate()==null) || 
             (this.coverageIssuedDate!=null &&
              this.coverageIssuedDate.equals(other.getCoverageIssuedDate()))) &&
            ((this.trailerNumber==null && other.getTrailerNumber()==null) || 
             (this.trailerNumber!=null &&
              this.trailerNumber.equals(other.getTrailerNumber()))) &&
            this.faceAmount == other.getFaceAmount();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPolicyNumber() != null) {
            _hashCode += getPolicyNumber().hashCode();
        }
        if (getPolicyStatus() != null) {
            _hashCode += getPolicyStatus().hashCode();
        }
        if (getPlanCode() != null) {
            _hashCode += getPlanCode().hashCode();
        }
        _hashCode += new Double(getModalPremium()).hashCode();
        _hashCode += getModeOfPremium();
        if (getIssuedDate() != null) {
            _hashCode += getIssuedDate().hashCode();
        }
        if (getBilledToDate() != null) {
            _hashCode += getBilledToDate().hashCode();
        }
        if (getPaidToDate() != null) {
            _hashCode += getPaidToDate().hashCode();
        }
        if (getCoverageCode() != null) {
            _hashCode += getCoverageCode().hashCode();
        }
        if (getCoverageStatus() != null) {
            _hashCode += getCoverageStatus().hashCode();
        }
        if (getCoverageIssuedDate() != null) {
            _hashCode += getCoverageIssuedDate().hashCode();
        }
        if (getTrailerNumber() != null) {
            _hashCode += getTrailerNumber().hashCode();
        }
        _hashCode += new Double(getFaceAmount()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyCoveragesResponseList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicycoverages.ws.application.muangthai.co.th/", "getPolicyCoveragesResponseList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PlanCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modalPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ModalPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modeOfPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ModeOfPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billedToDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BilledToDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paidToDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PaidToDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageIssuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageIssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trailerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TrailerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FaceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
