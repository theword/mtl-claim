/**
 * GetCoverageRelationService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcoveragerelation.www;

public class GetCoverageRelationService_ServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_Service {

/**
 * OSB Service
 */

    public GetCoverageRelationService_ServiceLocator() {
    }


    public GetCoverageRelationService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetCoverageRelationService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetCoverageRelationWsImplPort
    private java.lang.String GetCoverageRelationWsImplPort_address = "http://uat-osb.muangthai.co.th:9606/MTLSOA_CLM083_GetCoverageRelationServicePS/proxy-service/MTLSOA_CLM083_GetCoverageRelationServicePS";

    public java.lang.String getGetCoverageRelationWsImplPortAddress() {
        return GetCoverageRelationWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetCoverageRelationWsImplPortWSDDServiceName = "GetCoverageRelationWsImplPort";

    public java.lang.String getGetCoverageRelationWsImplPortWSDDServiceName() {
        return GetCoverageRelationWsImplPortWSDDServiceName;
    }

    public void setGetCoverageRelationWsImplPortWSDDServiceName(java.lang.String name) {
        GetCoverageRelationWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType getGetCoverageRelationWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetCoverageRelationWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetCoverageRelationWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType getGetCoverageRelationWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getGetCoverageRelationWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetCoverageRelationWsImplPortEndpointAddress(java.lang.String address) {
        GetCoverageRelationWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationWsImplPortBindingStub(new java.net.URL(GetCoverageRelationWsImplPort_address), this);
                _stub.setPortName(getGetCoverageRelationWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetCoverageRelationWsImplPort".equals(inputPortName)) {
            return getGetCoverageRelationWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.getcoveragerelation.ws.application.muangthai.co.th/", "GetCoverageRelationService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.getcoveragerelation.ws.application.muangthai.co.th/", "GetCoverageRelationWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetCoverageRelationWsImplPort".equals(portName)) {
            setGetCoverageRelationWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
