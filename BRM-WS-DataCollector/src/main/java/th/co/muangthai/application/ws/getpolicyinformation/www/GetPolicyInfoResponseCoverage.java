/**
 * GetPolicyInfoResponseCoverage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInfoResponseCoverage  implements java.io.Serializable {
    private java.lang.String coverageCode;

    private java.lang.String coverageNumber;

    private java.lang.String issuedDate;

    private java.lang.String maturityDate;

    private java.lang.String coCurrentStatusCode;

    private java.lang.String coCurrentStatusSubCode;

    private java.lang.String coDateStatusChange;

    private java.math.BigDecimal faceAmount;

    private java.lang.String trailerNumber;

    private java.math.BigDecimal sumInsured;

    private java.math.BigDecimal lifeAnnualPremium;

    private java.lang.String coPolicyNumber;

    private java.lang.String exhibitReinDate;

    private java.math.BigDecimal wpMultiplier;

    private java.lang.String voidingPeriod;

    private th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion[] getPolicyInfoResponseExclusion;

    public GetPolicyInfoResponseCoverage() {
    }

    public GetPolicyInfoResponseCoverage(
           java.lang.String coverageCode,
           java.lang.String coverageNumber,
           java.lang.String issuedDate,
           java.lang.String maturityDate,
           java.lang.String coCurrentStatusCode,
           java.lang.String coCurrentStatusSubCode,
           java.lang.String coDateStatusChange,
           java.math.BigDecimal faceAmount,
           java.lang.String trailerNumber,
           java.math.BigDecimal sumInsured,
           java.math.BigDecimal lifeAnnualPremium,
           java.lang.String coPolicyNumber,
           java.lang.String exhibitReinDate,
           java.math.BigDecimal wpMultiplier,
           java.lang.String voidingPeriod,
           th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion[] getPolicyInfoResponseExclusion) {
           this.coverageCode = coverageCode;
           this.coverageNumber = coverageNumber;
           this.issuedDate = issuedDate;
           this.maturityDate = maturityDate;
           this.coCurrentStatusCode = coCurrentStatusCode;
           this.coCurrentStatusSubCode = coCurrentStatusSubCode;
           this.coDateStatusChange = coDateStatusChange;
           this.faceAmount = faceAmount;
           this.trailerNumber = trailerNumber;
           this.sumInsured = sumInsured;
           this.lifeAnnualPremium = lifeAnnualPremium;
           this.coPolicyNumber = coPolicyNumber;
           this.exhibitReinDate = exhibitReinDate;
           this.wpMultiplier = wpMultiplier;
           this.voidingPeriod = voidingPeriod;
           this.getPolicyInfoResponseExclusion = getPolicyInfoResponseExclusion;
    }


    /**
     * Gets the coverageCode value for this GetPolicyInfoResponseCoverage.
     * 
     * @return coverageCode
     */
    public java.lang.String getCoverageCode() {
        return coverageCode;
    }


    /**
     * Sets the coverageCode value for this GetPolicyInfoResponseCoverage.
     * 
     * @param coverageCode
     */
    public void setCoverageCode(java.lang.String coverageCode) {
        this.coverageCode = coverageCode;
    }


    /**
     * Gets the coverageNumber value for this GetPolicyInfoResponseCoverage.
     * 
     * @return coverageNumber
     */
    public java.lang.String getCoverageNumber() {
        return coverageNumber;
    }


    /**
     * Sets the coverageNumber value for this GetPolicyInfoResponseCoverage.
     * 
     * @param coverageNumber
     */
    public void setCoverageNumber(java.lang.String coverageNumber) {
        this.coverageNumber = coverageNumber;
    }


    /**
     * Gets the issuedDate value for this GetPolicyInfoResponseCoverage.
     * 
     * @return issuedDate
     */
    public java.lang.String getIssuedDate() {
        return issuedDate;
    }


    /**
     * Sets the issuedDate value for this GetPolicyInfoResponseCoverage.
     * 
     * @param issuedDate
     */
    public void setIssuedDate(java.lang.String issuedDate) {
        this.issuedDate = issuedDate;
    }


    /**
     * Gets the maturityDate value for this GetPolicyInfoResponseCoverage.
     * 
     * @return maturityDate
     */
    public java.lang.String getMaturityDate() {
        return maturityDate;
    }


    /**
     * Sets the maturityDate value for this GetPolicyInfoResponseCoverage.
     * 
     * @param maturityDate
     */
    public void setMaturityDate(java.lang.String maturityDate) {
        this.maturityDate = maturityDate;
    }


    /**
     * Gets the coCurrentStatusCode value for this GetPolicyInfoResponseCoverage.
     * 
     * @return coCurrentStatusCode
     */
    public java.lang.String getCoCurrentStatusCode() {
        return coCurrentStatusCode;
    }


    /**
     * Sets the coCurrentStatusCode value for this GetPolicyInfoResponseCoverage.
     * 
     * @param coCurrentStatusCode
     */
    public void setCoCurrentStatusCode(java.lang.String coCurrentStatusCode) {
        this.coCurrentStatusCode = coCurrentStatusCode;
    }


    /**
     * Gets the coCurrentStatusSubCode value for this GetPolicyInfoResponseCoverage.
     * 
     * @return coCurrentStatusSubCode
     */
    public java.lang.String getCoCurrentStatusSubCode() {
        return coCurrentStatusSubCode;
    }


    /**
     * Sets the coCurrentStatusSubCode value for this GetPolicyInfoResponseCoverage.
     * 
     * @param coCurrentStatusSubCode
     */
    public void setCoCurrentStatusSubCode(java.lang.String coCurrentStatusSubCode) {
        this.coCurrentStatusSubCode = coCurrentStatusSubCode;
    }


    /**
     * Gets the coDateStatusChange value for this GetPolicyInfoResponseCoverage.
     * 
     * @return coDateStatusChange
     */
    public java.lang.String getCoDateStatusChange() {
        return coDateStatusChange;
    }


    /**
     * Sets the coDateStatusChange value for this GetPolicyInfoResponseCoverage.
     * 
     * @param coDateStatusChange
     */
    public void setCoDateStatusChange(java.lang.String coDateStatusChange) {
        this.coDateStatusChange = coDateStatusChange;
    }


    /**
     * Gets the faceAmount value for this GetPolicyInfoResponseCoverage.
     * 
     * @return faceAmount
     */
    public java.math.BigDecimal getFaceAmount() {
        return faceAmount;
    }


    /**
     * Sets the faceAmount value for this GetPolicyInfoResponseCoverage.
     * 
     * @param faceAmount
     */
    public void setFaceAmount(java.math.BigDecimal faceAmount) {
        this.faceAmount = faceAmount;
    }


    /**
     * Gets the trailerNumber value for this GetPolicyInfoResponseCoverage.
     * 
     * @return trailerNumber
     */
    public java.lang.String getTrailerNumber() {
        return trailerNumber;
    }


    /**
     * Sets the trailerNumber value for this GetPolicyInfoResponseCoverage.
     * 
     * @param trailerNumber
     */
    public void setTrailerNumber(java.lang.String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }


    /**
     * Gets the sumInsured value for this GetPolicyInfoResponseCoverage.
     * 
     * @return sumInsured
     */
    public java.math.BigDecimal getSumInsured() {
        return sumInsured;
    }


    /**
     * Sets the sumInsured value for this GetPolicyInfoResponseCoverage.
     * 
     * @param sumInsured
     */
    public void setSumInsured(java.math.BigDecimal sumInsured) {
        this.sumInsured = sumInsured;
    }


    /**
     * Gets the lifeAnnualPremium value for this GetPolicyInfoResponseCoverage.
     * 
     * @return lifeAnnualPremium
     */
    public java.math.BigDecimal getLifeAnnualPremium() {
        return lifeAnnualPremium;
    }


    /**
     * Sets the lifeAnnualPremium value for this GetPolicyInfoResponseCoverage.
     * 
     * @param lifeAnnualPremium
     */
    public void setLifeAnnualPremium(java.math.BigDecimal lifeAnnualPremium) {
        this.lifeAnnualPremium = lifeAnnualPremium;
    }


    /**
     * Gets the coPolicyNumber value for this GetPolicyInfoResponseCoverage.
     * 
     * @return coPolicyNumber
     */
    public java.lang.String getCoPolicyNumber() {
        return coPolicyNumber;
    }


    /**
     * Sets the coPolicyNumber value for this GetPolicyInfoResponseCoverage.
     * 
     * @param coPolicyNumber
     */
    public void setCoPolicyNumber(java.lang.String coPolicyNumber) {
        this.coPolicyNumber = coPolicyNumber;
    }


    /**
     * Gets the exhibitReinDate value for this GetPolicyInfoResponseCoverage.
     * 
     * @return exhibitReinDate
     */
    public java.lang.String getExhibitReinDate() {
        return exhibitReinDate;
    }


    /**
     * Sets the exhibitReinDate value for this GetPolicyInfoResponseCoverage.
     * 
     * @param exhibitReinDate
     */
    public void setExhibitReinDate(java.lang.String exhibitReinDate) {
        this.exhibitReinDate = exhibitReinDate;
    }


    /**
     * Gets the wpMultiplier value for this GetPolicyInfoResponseCoverage.
     * 
     * @return wpMultiplier
     */
    public java.math.BigDecimal getWpMultiplier() {
        return wpMultiplier;
    }


    /**
     * Sets the wpMultiplier value for this GetPolicyInfoResponseCoverage.
     * 
     * @param wpMultiplier
     */
    public void setWpMultiplier(java.math.BigDecimal wpMultiplier) {
        this.wpMultiplier = wpMultiplier;
    }


    /**
     * Gets the voidingPeriod value for this GetPolicyInfoResponseCoverage.
     * 
     * @return voidingPeriod
     */
    public java.lang.String getVoidingPeriod() {
        return voidingPeriod;
    }


    /**
     * Sets the voidingPeriod value for this GetPolicyInfoResponseCoverage.
     * 
     * @param voidingPeriod
     */
    public void setVoidingPeriod(java.lang.String voidingPeriod) {
        this.voidingPeriod = voidingPeriod;
    }


    /**
     * Gets the getPolicyInfoResponseExclusion value for this GetPolicyInfoResponseCoverage.
     * 
     * @return getPolicyInfoResponseExclusion
     */
    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion[] getGetPolicyInfoResponseExclusion() {
        return getPolicyInfoResponseExclusion;
    }


    /**
     * Sets the getPolicyInfoResponseExclusion value for this GetPolicyInfoResponseCoverage.
     * 
     * @param getPolicyInfoResponseExclusion
     */
    public void setGetPolicyInfoResponseExclusion(th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion[] getPolicyInfoResponseExclusion) {
        this.getPolicyInfoResponseExclusion = getPolicyInfoResponseExclusion;
    }

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion getGetPolicyInfoResponseExclusion(int i) {
        return this.getPolicyInfoResponseExclusion[i];
    }

    public void setGetPolicyInfoResponseExclusion(int i, th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion _value) {
        this.getPolicyInfoResponseExclusion[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyInfoResponseCoverage)) return false;
        GetPolicyInfoResponseCoverage other = (GetPolicyInfoResponseCoverage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.coverageCode==null && other.getCoverageCode()==null) || 
             (this.coverageCode!=null &&
              this.coverageCode.equals(other.getCoverageCode()))) &&
            ((this.coverageNumber==null && other.getCoverageNumber()==null) || 
             (this.coverageNumber!=null &&
              this.coverageNumber.equals(other.getCoverageNumber()))) &&
            ((this.issuedDate==null && other.getIssuedDate()==null) || 
             (this.issuedDate!=null &&
              this.issuedDate.equals(other.getIssuedDate()))) &&
            ((this.maturityDate==null && other.getMaturityDate()==null) || 
             (this.maturityDate!=null &&
              this.maturityDate.equals(other.getMaturityDate()))) &&
            ((this.coCurrentStatusCode==null && other.getCoCurrentStatusCode()==null) || 
             (this.coCurrentStatusCode!=null &&
              this.coCurrentStatusCode.equals(other.getCoCurrentStatusCode()))) &&
            ((this.coCurrentStatusSubCode==null && other.getCoCurrentStatusSubCode()==null) || 
             (this.coCurrentStatusSubCode!=null &&
              this.coCurrentStatusSubCode.equals(other.getCoCurrentStatusSubCode()))) &&
            ((this.coDateStatusChange==null && other.getCoDateStatusChange()==null) || 
             (this.coDateStatusChange!=null &&
              this.coDateStatusChange.equals(other.getCoDateStatusChange()))) &&
            ((this.faceAmount==null && other.getFaceAmount()==null) || 
             (this.faceAmount!=null &&
              this.faceAmount.equals(other.getFaceAmount()))) &&
            ((this.trailerNumber==null && other.getTrailerNumber()==null) || 
             (this.trailerNumber!=null &&
              this.trailerNumber.equals(other.getTrailerNumber()))) &&
            ((this.sumInsured==null && other.getSumInsured()==null) || 
             (this.sumInsured!=null &&
              this.sumInsured.equals(other.getSumInsured()))) &&
            ((this.lifeAnnualPremium==null && other.getLifeAnnualPremium()==null) || 
             (this.lifeAnnualPremium!=null &&
              this.lifeAnnualPremium.equals(other.getLifeAnnualPremium()))) &&
            ((this.coPolicyNumber==null && other.getCoPolicyNumber()==null) || 
             (this.coPolicyNumber!=null &&
              this.coPolicyNumber.equals(other.getCoPolicyNumber()))) &&
            ((this.exhibitReinDate==null && other.getExhibitReinDate()==null) || 
             (this.exhibitReinDate!=null &&
              this.exhibitReinDate.equals(other.getExhibitReinDate()))) &&
            ((this.wpMultiplier==null && other.getWpMultiplier()==null) || 
             (this.wpMultiplier!=null &&
              this.wpMultiplier.equals(other.getWpMultiplier()))) &&
            ((this.voidingPeriod==null && other.getVoidingPeriod()==null) || 
             (this.voidingPeriod!=null &&
              this.voidingPeriod.equals(other.getVoidingPeriod()))) &&
            ((this.getPolicyInfoResponseExclusion==null && other.getGetPolicyInfoResponseExclusion()==null) || 
             (this.getPolicyInfoResponseExclusion!=null &&
              java.util.Arrays.equals(this.getPolicyInfoResponseExclusion, other.getGetPolicyInfoResponseExclusion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCoverageCode() != null) {
            _hashCode += getCoverageCode().hashCode();
        }
        if (getCoverageNumber() != null) {
            _hashCode += getCoverageNumber().hashCode();
        }
        if (getIssuedDate() != null) {
            _hashCode += getIssuedDate().hashCode();
        }
        if (getMaturityDate() != null) {
            _hashCode += getMaturityDate().hashCode();
        }
        if (getCoCurrentStatusCode() != null) {
            _hashCode += getCoCurrentStatusCode().hashCode();
        }
        if (getCoCurrentStatusSubCode() != null) {
            _hashCode += getCoCurrentStatusSubCode().hashCode();
        }
        if (getCoDateStatusChange() != null) {
            _hashCode += getCoDateStatusChange().hashCode();
        }
        if (getFaceAmount() != null) {
            _hashCode += getFaceAmount().hashCode();
        }
        if (getTrailerNumber() != null) {
            _hashCode += getTrailerNumber().hashCode();
        }
        if (getSumInsured() != null) {
            _hashCode += getSumInsured().hashCode();
        }
        if (getLifeAnnualPremium() != null) {
            _hashCode += getLifeAnnualPremium().hashCode();
        }
        if (getCoPolicyNumber() != null) {
            _hashCode += getCoPolicyNumber().hashCode();
        }
        if (getExhibitReinDate() != null) {
            _hashCode += getExhibitReinDate().hashCode();
        }
        if (getWpMultiplier() != null) {
            _hashCode += getWpMultiplier().hashCode();
        }
        if (getVoidingPeriod() != null) {
            _hashCode += getVoidingPeriod().hashCode();
        }
        if (getGetPolicyInfoResponseExclusion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyInfoResponseExclusion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyInfoResponseExclusion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyInfoResponseCoverage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponseCoverage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maturityDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MaturityDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coCurrentStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoCurrentStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coCurrentStatusSubCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoCurrentStatusSubCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coDateStatusChange");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoDateStatusChange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FaceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trailerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TrailerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sumInsured");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SumInsured"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifeAnnualPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LifeAnnualPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coPolicyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoPolicyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exhibitReinDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ExhibitReinDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("wpMultiplier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WpMultiplier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voidingPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VoidingPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyInfoResponseExclusion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getPolicyInfoResponseExclusion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponseExclusion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
