/**
 * HeaderBuildCoveragesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildcoverages.www;

public class HeaderBuildCoveragesResponse  extends th.co.muangthai.application.ws.buildcoverages.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesResponse[] buildCoveragesResponseList;

    public HeaderBuildCoveragesResponse() {
    }

    public HeaderBuildCoveragesResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesResponse[] buildCoveragesResponseList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.buildCoveragesResponseList = buildCoveragesResponseList;
    }


    /**
     * Gets the buildCoveragesResponseList value for this HeaderBuildCoveragesResponse.
     * 
     * @return buildCoveragesResponseList
     */
    public th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesResponse[] getBuildCoveragesResponseList() {
        return buildCoveragesResponseList;
    }


    /**
     * Sets the buildCoveragesResponseList value for this HeaderBuildCoveragesResponse.
     * 
     * @param buildCoveragesResponseList
     */
    public void setBuildCoveragesResponseList(th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesResponse[] buildCoveragesResponseList) {
        this.buildCoveragesResponseList = buildCoveragesResponseList;
    }

    public th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesResponse getBuildCoveragesResponseList(int i) {
        return this.buildCoveragesResponseList[i];
    }

    public void setBuildCoveragesResponseList(int i, th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesResponse _value) {
        this.buildCoveragesResponseList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderBuildCoveragesResponse)) return false;
        HeaderBuildCoveragesResponse other = (HeaderBuildCoveragesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.buildCoveragesResponseList==null && other.getBuildCoveragesResponseList()==null) || 
             (this.buildCoveragesResponseList!=null &&
              java.util.Arrays.equals(this.buildCoveragesResponseList, other.getBuildCoveragesResponseList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBuildCoveragesResponseList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBuildCoveragesResponseList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBuildCoveragesResponseList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderBuildCoveragesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.buildcoverages.ws.application.muangthai.co.th/", "headerBuildCoveragesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildCoveragesResponseList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BuildCoveragesResponseList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.buildcoverages.ws.application.muangthai.co.th/", "buildCoveragesResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
