/**
 * GetPolicyInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public interface GetPolicyInformation extends java.rmi.Remote {
    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponse getPolicyInformation(th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequest request) throws java.rmi.RemoteException;
}
