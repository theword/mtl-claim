/**
 * BuildCoveragesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildcoverages.www;

public class BuildCoveragesResponse  implements java.io.Serializable {
    private java.lang.String appNumber;

    private java.lang.String coverageCode;

    private java.lang.String coverageStatus;

    private java.lang.String issuedDate;

    private java.lang.String trailerNumber;

    private double faceAmount;

    public BuildCoveragesResponse() {
    }

    public BuildCoveragesResponse(
           java.lang.String appNumber,
           java.lang.String coverageCode,
           java.lang.String coverageStatus,
           java.lang.String issuedDate,
           java.lang.String trailerNumber,
           double faceAmount) {
           this.appNumber = appNumber;
           this.coverageCode = coverageCode;
           this.coverageStatus = coverageStatus;
           this.issuedDate = issuedDate;
           this.trailerNumber = trailerNumber;
           this.faceAmount = faceAmount;
    }


    /**
     * Gets the appNumber value for this BuildCoveragesResponse.
     * 
     * @return appNumber
     */
    public java.lang.String getAppNumber() {
        return appNumber;
    }


    /**
     * Sets the appNumber value for this BuildCoveragesResponse.
     * 
     * @param appNumber
     */
    public void setAppNumber(java.lang.String appNumber) {
        this.appNumber = appNumber;
    }


    /**
     * Gets the coverageCode value for this BuildCoveragesResponse.
     * 
     * @return coverageCode
     */
    public java.lang.String getCoverageCode() {
        return coverageCode;
    }


    /**
     * Sets the coverageCode value for this BuildCoveragesResponse.
     * 
     * @param coverageCode
     */
    public void setCoverageCode(java.lang.String coverageCode) {
        this.coverageCode = coverageCode;
    }


    /**
     * Gets the coverageStatus value for this BuildCoveragesResponse.
     * 
     * @return coverageStatus
     */
    public java.lang.String getCoverageStatus() {
        return coverageStatus;
    }


    /**
     * Sets the coverageStatus value for this BuildCoveragesResponse.
     * 
     * @param coverageStatus
     */
    public void setCoverageStatus(java.lang.String coverageStatus) {
        this.coverageStatus = coverageStatus;
    }


    /**
     * Gets the issuedDate value for this BuildCoveragesResponse.
     * 
     * @return issuedDate
     */
    public java.lang.String getIssuedDate() {
        return issuedDate;
    }


    /**
     * Sets the issuedDate value for this BuildCoveragesResponse.
     * 
     * @param issuedDate
     */
    public void setIssuedDate(java.lang.String issuedDate) {
        this.issuedDate = issuedDate;
    }


    /**
     * Gets the trailerNumber value for this BuildCoveragesResponse.
     * 
     * @return trailerNumber
     */
    public java.lang.String getTrailerNumber() {
        return trailerNumber;
    }


    /**
     * Sets the trailerNumber value for this BuildCoveragesResponse.
     * 
     * @param trailerNumber
     */
    public void setTrailerNumber(java.lang.String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }


    /**
     * Gets the faceAmount value for this BuildCoveragesResponse.
     * 
     * @return faceAmount
     */
    public double getFaceAmount() {
        return faceAmount;
    }


    /**
     * Sets the faceAmount value for this BuildCoveragesResponse.
     * 
     * @param faceAmount
     */
    public void setFaceAmount(double faceAmount) {
        this.faceAmount = faceAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BuildCoveragesResponse)) return false;
        BuildCoveragesResponse other = (BuildCoveragesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.appNumber==null && other.getAppNumber()==null) || 
             (this.appNumber!=null &&
              this.appNumber.equals(other.getAppNumber()))) &&
            ((this.coverageCode==null && other.getCoverageCode()==null) || 
             (this.coverageCode!=null &&
              this.coverageCode.equals(other.getCoverageCode()))) &&
            ((this.coverageStatus==null && other.getCoverageStatus()==null) || 
             (this.coverageStatus!=null &&
              this.coverageStatus.equals(other.getCoverageStatus()))) &&
            ((this.issuedDate==null && other.getIssuedDate()==null) || 
             (this.issuedDate!=null &&
              this.issuedDate.equals(other.getIssuedDate()))) &&
            ((this.trailerNumber==null && other.getTrailerNumber()==null) || 
             (this.trailerNumber!=null &&
              this.trailerNumber.equals(other.getTrailerNumber()))) &&
            this.faceAmount == other.getFaceAmount();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAppNumber() != null) {
            _hashCode += getAppNumber().hashCode();
        }
        if (getCoverageCode() != null) {
            _hashCode += getCoverageCode().hashCode();
        }
        if (getCoverageStatus() != null) {
            _hashCode += getCoverageStatus().hashCode();
        }
        if (getIssuedDate() != null) {
            _hashCode += getIssuedDate().hashCode();
        }
        if (getTrailerNumber() != null) {
            _hashCode += getTrailerNumber().hashCode();
        }
        _hashCode += new Double(getFaceAmount()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BuildCoveragesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.buildcoverages.ws.application.muangthai.co.th/", "buildCoveragesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("appNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AppNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CoverageStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trailerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TrailerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FaceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
