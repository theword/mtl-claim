/**
 * GetPolicyInformationServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInformationServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationService {

    public GetPolicyInformationServiceLocator() {
    }


    public GetPolicyInformationServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetPolicyInformationServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetPolicyInformationWsImplPort
    private java.lang.String GetPolicyInformationWsImplPort_address = "http://uat-lof4clm-minor.muangthai.co.th:9624/MTL_CLAIM_WS_UAT/GetPolicyInformationService";

    public java.lang.String getGetPolicyInformationWsImplPortAddress() {
        return GetPolicyInformationWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetPolicyInformationWsImplPortWSDDServiceName = "GetPolicyInformationWsImplPort";

    public java.lang.String getGetPolicyInformationWsImplPortWSDDServiceName() {
        return GetPolicyInformationWsImplPortWSDDServiceName;
    }

    public void setGetPolicyInformationWsImplPortWSDDServiceName(java.lang.String name) {
        GetPolicyInformationWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation getGetPolicyInformationWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetPolicyInformationWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetPolicyInformationWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation getGetPolicyInformationWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getGetPolicyInformationWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetPolicyInformationWsImplPortEndpointAddress(java.lang.String address) {
        GetPolicyInformationWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationWsImplPortBindingStub(new java.net.URL(GetPolicyInformationWsImplPort_address), this);
                _stub.setPortName(getGetPolicyInformationWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetPolicyInformationWsImplPort".equals(inputPortName)) {
            return getGetPolicyInformationWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "GetPolicyInformationService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "GetPolicyInformationWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetPolicyInformationWsImplPort".equals(portName)) {
            setGetPolicyInformationWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
