/**
 * GetCoverageRelationService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcoveragerelation.www;

public interface GetCoverageRelationService_Service extends javax.xml.rpc.Service {

/**
 * OSB Service
 */
    public java.lang.String getGetCoverageRelationWsImplPortAddress();

    public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType getGetCoverageRelationWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType getGetCoverageRelationWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
