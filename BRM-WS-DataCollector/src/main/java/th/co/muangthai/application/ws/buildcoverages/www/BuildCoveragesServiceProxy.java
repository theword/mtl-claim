package th.co.muangthai.application.ws.buildcoverages.www;

public class BuildCoveragesServiceProxy implements th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesService_PortType buildCoveragesService_PortType = null;
  
  public BuildCoveragesServiceProxy() {
    _initBuildCoveragesServiceProxy();
  }
  
  public BuildCoveragesServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initBuildCoveragesServiceProxy();
  }
  
  private void _initBuildCoveragesServiceProxy() {
    try {
      buildCoveragesService_PortType = (new th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesService_ServiceLocator()).getBuildCoveragesWsImplPort();
      if (buildCoveragesService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)buildCoveragesService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)buildCoveragesService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (buildCoveragesService_PortType != null)
      ((javax.xml.rpc.Stub)buildCoveragesService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesService_PortType getBuildCoveragesService_PortType() {
    if (buildCoveragesService_PortType == null)
      _initBuildCoveragesServiceProxy();
    return buildCoveragesService_PortType;
  }
  
  public th.co.muangthai.application.ws.buildcoverages.www.HeaderBuildCoveragesResponse buildCoverages(th.co.muangthai.application.ws.buildcoverages.www.HeaderBuildCoveragesRequest request) throws java.rmi.RemoteException{
    if (buildCoveragesService_PortType == null)
      _initBuildCoveragesServiceProxy();
    return buildCoveragesService_PortType.buildCoverages(request);
  }
  
  
}