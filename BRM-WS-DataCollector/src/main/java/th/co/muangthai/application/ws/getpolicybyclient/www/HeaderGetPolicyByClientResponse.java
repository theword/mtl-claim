/**
 * HeaderGetPolicyByClientResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicybyclient.www;

public class HeaderGetPolicyByClientResponse  extends th.co.muangthai.application.ws.getpolicybyclient.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse[] getPolicyByClientResponses;

    public HeaderGetPolicyByClientResponse() {
    }

    public HeaderGetPolicyByClientResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse[] getPolicyByClientResponses) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.getPolicyByClientResponses = getPolicyByClientResponses;
    }


    /**
     * Gets the getPolicyByClientResponses value for this HeaderGetPolicyByClientResponse.
     * 
     * @return getPolicyByClientResponses
     */
    public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse[] getGetPolicyByClientResponses() {
        return getPolicyByClientResponses;
    }


    /**
     * Sets the getPolicyByClientResponses value for this HeaderGetPolicyByClientResponse.
     * 
     * @param getPolicyByClientResponses
     */
    public void setGetPolicyByClientResponses(th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse[] getPolicyByClientResponses) {
        this.getPolicyByClientResponses = getPolicyByClientResponses;
    }

    public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse getGetPolicyByClientResponses(int i) {
        return this.getPolicyByClientResponses[i];
    }

    public void setGetPolicyByClientResponses(int i, th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse _value) {
        this.getPolicyByClientResponses[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderGetPolicyByClientResponse)) return false;
        HeaderGetPolicyByClientResponse other = (HeaderGetPolicyByClientResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.getPolicyByClientResponses==null && other.getGetPolicyByClientResponses()==null) || 
             (this.getPolicyByClientResponses!=null &&
              java.util.Arrays.equals(this.getPolicyByClientResponses, other.getGetPolicyByClientResponses())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGetPolicyByClientResponses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyByClientResponses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyByClientResponses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderGetPolicyByClientResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicybyclient.ws.application.muangthai.co.th/", "headerGetPolicyByClientResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyByClientResponses");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getPolicyByClientResponses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getpolicybyclient.ws.application.muangthai.co.th/", "getPolicyByClientResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
