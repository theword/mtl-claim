package th.co.muangthai.application.ws.getpolicybyclient.www;

public class GetPolicyByClientServiceProxy implements th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType getPolicyByClientService_PortType = null;
  
  public GetPolicyByClientServiceProxy() {
    _initGetPolicyByClientServiceProxy();
  }
  
  public GetPolicyByClientServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetPolicyByClientServiceProxy();
  }
  
  private void _initGetPolicyByClientServiceProxy() {
    try {
      getPolicyByClientService_PortType = (new th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_ServiceLocator()).getGetPolicyByClientWsImplPort();
      if (getPolicyByClientService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getPolicyByClientService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getPolicyByClientService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getPolicyByClientService_PortType != null)
      ((javax.xml.rpc.Stub)getPolicyByClientService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType getGetPolicyByClientService_PortType() {
    if (getPolicyByClientService_PortType == null)
      _initGetPolicyByClientServiceProxy();
    return getPolicyByClientService_PortType;
  }
  
  public th.co.muangthai.application.ws.getpolicybyclient.www.HeaderGetPolicyByClientResponse getPolicyByClient(th.co.muangthai.application.ws.getpolicybyclient.www.HeaderGetPolicyByClientRequest request) throws java.rmi.RemoteException{
    if (getPolicyByClientService_PortType == null)
      _initGetPolicyByClientServiceProxy();
    return getPolicyByClientService_PortType.getPolicyByClient(request);
  }
  
  
}