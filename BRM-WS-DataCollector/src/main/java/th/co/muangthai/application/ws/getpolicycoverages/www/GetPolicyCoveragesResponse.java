/**
 * GetPolicyCoveragesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicycoverages.www;

public class GetPolicyCoveragesResponse  extends th.co.muangthai.application.ws.getpolicycoverages.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList[] getPolicyCoveragesResponseList;

    public GetPolicyCoveragesResponse() {
    }

    public GetPolicyCoveragesResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList[] getPolicyCoveragesResponseList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.getPolicyCoveragesResponseList = getPolicyCoveragesResponseList;
    }


    /**
     * Gets the getPolicyCoveragesResponseList value for this GetPolicyCoveragesResponse.
     * 
     * @return getPolicyCoveragesResponseList
     */
    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList[] getGetPolicyCoveragesResponseList() {
        return getPolicyCoveragesResponseList;
    }


    /**
     * Sets the getPolicyCoveragesResponseList value for this GetPolicyCoveragesResponse.
     * 
     * @param getPolicyCoveragesResponseList
     */
    public void setGetPolicyCoveragesResponseList(th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList[] getPolicyCoveragesResponseList) {
        this.getPolicyCoveragesResponseList = getPolicyCoveragesResponseList;
    }

    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList getGetPolicyCoveragesResponseList(int i) {
        return this.getPolicyCoveragesResponseList[i];
    }

    public void setGetPolicyCoveragesResponseList(int i, th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList _value) {
        this.getPolicyCoveragesResponseList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyCoveragesResponse)) return false;
        GetPolicyCoveragesResponse other = (GetPolicyCoveragesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.getPolicyCoveragesResponseList==null && other.getGetPolicyCoveragesResponseList()==null) || 
             (this.getPolicyCoveragesResponseList!=null &&
              java.util.Arrays.equals(this.getPolicyCoveragesResponseList, other.getGetPolicyCoveragesResponseList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGetPolicyCoveragesResponseList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyCoveragesResponseList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyCoveragesResponseList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyCoveragesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicycoverages.ws.application.muangthai.co.th/", "getPolicyCoveragesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyCoveragesResponseList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GetPolicyCoveragesResponseList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getpolicycoverages.ws.application.muangthai.co.th/", "getPolicyCoveragesResponseList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
