/**
 * BuildPoliciesService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildpolicies.www;

public class BuildPoliciesService_ServiceLocator extends org.apache.axis.client.Service implements th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_Service {

    public BuildPoliciesService_ServiceLocator() {
    }


    public BuildPoliciesService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BuildPoliciesService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BuildPoliciesWsImplPort
    private java.lang.String BuildPoliciesWsImplPort_address = "http://MTOWOSBU2:8011/MTL-SOA/CLM/BRMSCLM/BuildPoliciesService/ProxyService/BuildPoliciesServicePS";

    public java.lang.String getBuildPoliciesWsImplPortAddress() {
        return BuildPoliciesWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BuildPoliciesWsImplPortWSDDServiceName = "BuildPoliciesWsImplPort";

    public java.lang.String getBuildPoliciesWsImplPortWSDDServiceName() {
        return BuildPoliciesWsImplPortWSDDServiceName;
    }

    public void setBuildPoliciesWsImplPortWSDDServiceName(java.lang.String name) {
        BuildPoliciesWsImplPortWSDDServiceName = name;
    }

    public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType getBuildPoliciesWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BuildPoliciesWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBuildPoliciesWsImplPort(endpoint);
    }

    public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType getBuildPoliciesWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getBuildPoliciesWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBuildPoliciesWsImplPortEndpointAddress(java.lang.String address) {
        BuildPoliciesWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesWsImplPortBindingStub _stub = new th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesWsImplPortBindingStub(new java.net.URL(BuildPoliciesWsImplPort_address), this);
                _stub.setPortName(getBuildPoliciesWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BuildPoliciesWsImplPort".equals(inputPortName)) {
            return getBuildPoliciesWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.buildpolicies.ws.application.muangthai.co.th/", "BuildPoliciesService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.buildpolicies.ws.application.muangthai.co.th/", "BuildPoliciesWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BuildPoliciesWsImplPort".equals(portName)) {
            setBuildPoliciesWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
