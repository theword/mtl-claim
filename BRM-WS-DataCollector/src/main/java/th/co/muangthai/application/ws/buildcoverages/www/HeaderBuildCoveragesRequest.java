/**
 * HeaderBuildCoveragesRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildcoverages.www;

public class HeaderBuildCoveragesRequest  extends th.co.muangthai.application.ws.buildcoverages.www.HeaderXml  implements java.io.Serializable {
    private java.lang.String appNumber;

    public HeaderBuildCoveragesRequest() {
    }

    public HeaderBuildCoveragesRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String appNumber) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.appNumber = appNumber;
    }


    /**
     * Gets the appNumber value for this HeaderBuildCoveragesRequest.
     * 
     * @return appNumber
     */
    public java.lang.String getAppNumber() {
        return appNumber;
    }


    /**
     * Sets the appNumber value for this HeaderBuildCoveragesRequest.
     * 
     * @param appNumber
     */
    public void setAppNumber(java.lang.String appNumber) {
        this.appNumber = appNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderBuildCoveragesRequest)) return false;
        HeaderBuildCoveragesRequest other = (HeaderBuildCoveragesRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.appNumber==null && other.getAppNumber()==null) || 
             (this.appNumber!=null &&
              this.appNumber.equals(other.getAppNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAppNumber() != null) {
            _hashCode += getAppNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderBuildCoveragesRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.buildcoverages.ws.application.muangthai.co.th/", "headerBuildCoveragesRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("appNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AppNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
