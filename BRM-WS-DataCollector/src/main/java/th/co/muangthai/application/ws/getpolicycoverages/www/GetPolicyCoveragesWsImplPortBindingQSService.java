/**
 * GetPolicyCoveragesWsImplPortBindingQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicycoverages.www;

public interface GetPolicyCoveragesWsImplPortBindingQSService extends javax.xml.rpc.Service {

/**
 * OSB Service
 */
    public java.lang.String getGetPolicyCoveragesWsImplPortBindingQSPortAddress();

    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService getGetPolicyCoveragesWsImplPortBindingQSPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService getGetPolicyCoveragesWsImplPortBindingQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
