/**
 * HeaderGetCoverageRelationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcoveragerelation.www;

public class HeaderGetCoverageRelationResponse  extends th.co.muangthai.application.ws.getcoveragerelation.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse[] coverageRelationResponseList;

    public HeaderGetCoverageRelationResponse() {
    }

    public HeaderGetCoverageRelationResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse[] coverageRelationResponseList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.coverageRelationResponseList = coverageRelationResponseList;
    }


    /**
     * Gets the coverageRelationResponseList value for this HeaderGetCoverageRelationResponse.
     * 
     * @return coverageRelationResponseList
     */
    public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse[] getCoverageRelationResponseList() {
        return coverageRelationResponseList;
    }


    /**
     * Sets the coverageRelationResponseList value for this HeaderGetCoverageRelationResponse.
     * 
     * @param coverageRelationResponseList
     */
    public void setCoverageRelationResponseList(th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse[] coverageRelationResponseList) {
        this.coverageRelationResponseList = coverageRelationResponseList;
    }

    public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse getCoverageRelationResponseList(int i) {
        return this.coverageRelationResponseList[i];
    }

    public void setCoverageRelationResponseList(int i, th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse _value) {
        this.coverageRelationResponseList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderGetCoverageRelationResponse)) return false;
        HeaderGetCoverageRelationResponse other = (HeaderGetCoverageRelationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.coverageRelationResponseList==null && other.getCoverageRelationResponseList()==null) || 
             (this.coverageRelationResponseList!=null &&
              java.util.Arrays.equals(this.coverageRelationResponseList, other.getCoverageRelationResponseList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCoverageRelationResponseList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCoverageRelationResponseList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCoverageRelationResponseList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderGetCoverageRelationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getcoveragerelation.ws.application.muangthai.co.th/", "headerGetCoverageRelationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageRelationResponseList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coverageRelationResponseList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getcoveragerelation.ws.application.muangthai.co.th/", "getCoverageRelationResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
