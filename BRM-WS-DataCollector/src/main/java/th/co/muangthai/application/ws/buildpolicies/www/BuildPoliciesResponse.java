/**
 * BuildPoliciesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildpolicies.www;

public class BuildPoliciesResponse  implements java.io.Serializable {
    private java.lang.String policyHistoryID;

    private java.lang.String policyNumber;

    private java.lang.String policyStatus;

    private java.lang.String planCode;

    private double modalPremium;

    private int modeOfPremium;

    private java.lang.String issuedDate;

    private java.lang.String billedToDate;

    private java.lang.String paidToDate;

    public BuildPoliciesResponse() {
    }

    public BuildPoliciesResponse(
           java.lang.String policyHistoryID,
           java.lang.String policyNumber,
           java.lang.String policyStatus,
           java.lang.String planCode,
           double modalPremium,
           int modeOfPremium,
           java.lang.String issuedDate,
           java.lang.String billedToDate,
           java.lang.String paidToDate) {
           this.policyHistoryID = policyHistoryID;
           this.policyNumber = policyNumber;
           this.policyStatus = policyStatus;
           this.planCode = planCode;
           this.modalPremium = modalPremium;
           this.modeOfPremium = modeOfPremium;
           this.issuedDate = issuedDate;
           this.billedToDate = billedToDate;
           this.paidToDate = paidToDate;
    }


    /**
     * Gets the policyHistoryID value for this BuildPoliciesResponse.
     * 
     * @return policyHistoryID
     */
    public java.lang.String getPolicyHistoryID() {
        return policyHistoryID;
    }


    /**
     * Sets the policyHistoryID value for this BuildPoliciesResponse.
     * 
     * @param policyHistoryID
     */
    public void setPolicyHistoryID(java.lang.String policyHistoryID) {
        this.policyHistoryID = policyHistoryID;
    }


    /**
     * Gets the policyNumber value for this BuildPoliciesResponse.
     * 
     * @return policyNumber
     */
    public java.lang.String getPolicyNumber() {
        return policyNumber;
    }


    /**
     * Sets the policyNumber value for this BuildPoliciesResponse.
     * 
     * @param policyNumber
     */
    public void setPolicyNumber(java.lang.String policyNumber) {
        this.policyNumber = policyNumber;
    }


    /**
     * Gets the policyStatus value for this BuildPoliciesResponse.
     * 
     * @return policyStatus
     */
    public java.lang.String getPolicyStatus() {
        return policyStatus;
    }


    /**
     * Sets the policyStatus value for this BuildPoliciesResponse.
     * 
     * @param policyStatus
     */
    public void setPolicyStatus(java.lang.String policyStatus) {
        this.policyStatus = policyStatus;
    }


    /**
     * Gets the planCode value for this BuildPoliciesResponse.
     * 
     * @return planCode
     */
    public java.lang.String getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this BuildPoliciesResponse.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String planCode) {
        this.planCode = planCode;
    }


    /**
     * Gets the modalPremium value for this BuildPoliciesResponse.
     * 
     * @return modalPremium
     */
    public double getModalPremium() {
        return modalPremium;
    }


    /**
     * Sets the modalPremium value for this BuildPoliciesResponse.
     * 
     * @param modalPremium
     */
    public void setModalPremium(double modalPremium) {
        this.modalPremium = modalPremium;
    }


    /**
     * Gets the modeOfPremium value for this BuildPoliciesResponse.
     * 
     * @return modeOfPremium
     */
    public int getModeOfPremium() {
        return modeOfPremium;
    }


    /**
     * Sets the modeOfPremium value for this BuildPoliciesResponse.
     * 
     * @param modeOfPremium
     */
    public void setModeOfPremium(int modeOfPremium) {
        this.modeOfPremium = modeOfPremium;
    }


    /**
     * Gets the issuedDate value for this BuildPoliciesResponse.
     * 
     * @return issuedDate
     */
    public java.lang.String getIssuedDate() {
        return issuedDate;
    }


    /**
     * Sets the issuedDate value for this BuildPoliciesResponse.
     * 
     * @param issuedDate
     */
    public void setIssuedDate(java.lang.String issuedDate) {
        this.issuedDate = issuedDate;
    }


    /**
     * Gets the billedToDate value for this BuildPoliciesResponse.
     * 
     * @return billedToDate
     */
    public java.lang.String getBilledToDate() {
        return billedToDate;
    }


    /**
     * Sets the billedToDate value for this BuildPoliciesResponse.
     * 
     * @param billedToDate
     */
    public void setBilledToDate(java.lang.String billedToDate) {
        this.billedToDate = billedToDate;
    }


    /**
     * Gets the paidToDate value for this BuildPoliciesResponse.
     * 
     * @return paidToDate
     */
    public java.lang.String getPaidToDate() {
        return paidToDate;
    }


    /**
     * Sets the paidToDate value for this BuildPoliciesResponse.
     * 
     * @param paidToDate
     */
    public void setPaidToDate(java.lang.String paidToDate) {
        this.paidToDate = paidToDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BuildPoliciesResponse)) return false;
        BuildPoliciesResponse other = (BuildPoliciesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.policyHistoryID==null && other.getPolicyHistoryID()==null) || 
             (this.policyHistoryID!=null &&
              this.policyHistoryID.equals(other.getPolicyHistoryID()))) &&
            ((this.policyNumber==null && other.getPolicyNumber()==null) || 
             (this.policyNumber!=null &&
              this.policyNumber.equals(other.getPolicyNumber()))) &&
            ((this.policyStatus==null && other.getPolicyStatus()==null) || 
             (this.policyStatus!=null &&
              this.policyStatus.equals(other.getPolicyStatus()))) &&
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              this.planCode.equals(other.getPlanCode()))) &&
            this.modalPremium == other.getModalPremium() &&
            this.modeOfPremium == other.getModeOfPremium() &&
            ((this.issuedDate==null && other.getIssuedDate()==null) || 
             (this.issuedDate!=null &&
              this.issuedDate.equals(other.getIssuedDate()))) &&
            ((this.billedToDate==null && other.getBilledToDate()==null) || 
             (this.billedToDate!=null &&
              this.billedToDate.equals(other.getBilledToDate()))) &&
            ((this.paidToDate==null && other.getPaidToDate()==null) || 
             (this.paidToDate!=null &&
              this.paidToDate.equals(other.getPaidToDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPolicyHistoryID() != null) {
            _hashCode += getPolicyHistoryID().hashCode();
        }
        if (getPolicyNumber() != null) {
            _hashCode += getPolicyNumber().hashCode();
        }
        if (getPolicyStatus() != null) {
            _hashCode += getPolicyStatus().hashCode();
        }
        if (getPlanCode() != null) {
            _hashCode += getPlanCode().hashCode();
        }
        _hashCode += new Double(getModalPremium()).hashCode();
        _hashCode += getModeOfPremium();
        if (getIssuedDate() != null) {
            _hashCode += getIssuedDate().hashCode();
        }
        if (getBilledToDate() != null) {
            _hashCode += getBilledToDate().hashCode();
        }
        if (getPaidToDate() != null) {
            _hashCode += getPaidToDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BuildPoliciesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.buildpolicies.ws.application.muangthai.co.th/", "buildPoliciesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyHistoryID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyHistoryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PolicyStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PlanCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modalPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ModalPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modeOfPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ModeOfPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billedToDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BilledToDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paidToDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PaidToDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
