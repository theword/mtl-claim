/**
 * BuildCoveragesService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildcoverages.www;

public interface BuildCoveragesService_Service extends javax.xml.rpc.Service {
    public java.lang.String getBuildCoveragesWsImplPortAddress();

    public th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesService_PortType getBuildCoveragesWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.buildcoverages.www.BuildCoveragesService_PortType getBuildCoveragesWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
