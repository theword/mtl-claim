/**
 * GetPolicyInfoResponseExclusion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInfoResponseExclusion  implements java.io.Serializable {
    private java.lang.Integer exclusionIndicator;

    private java.lang.String exclusionCode;

    public GetPolicyInfoResponseExclusion() {
    }

    public GetPolicyInfoResponseExclusion(
           java.lang.Integer exclusionIndicator,
           java.lang.String exclusionCode) {
           this.exclusionIndicator = exclusionIndicator;
           this.exclusionCode = exclusionCode;
    }


    /**
     * Gets the exclusionIndicator value for this GetPolicyInfoResponseExclusion.
     * 
     * @return exclusionIndicator
     */
    public java.lang.Integer getExclusionIndicator() {
        return exclusionIndicator;
    }


    /**
     * Sets the exclusionIndicator value for this GetPolicyInfoResponseExclusion.
     * 
     * @param exclusionIndicator
     */
    public void setExclusionIndicator(java.lang.Integer exclusionIndicator) {
        this.exclusionIndicator = exclusionIndicator;
    }


    /**
     * Gets the exclusionCode value for this GetPolicyInfoResponseExclusion.
     * 
     * @return exclusionCode
     */
    public java.lang.String getExclusionCode() {
        return exclusionCode;
    }


    /**
     * Sets the exclusionCode value for this GetPolicyInfoResponseExclusion.
     * 
     * @param exclusionCode
     */
    public void setExclusionCode(java.lang.String exclusionCode) {
        this.exclusionCode = exclusionCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyInfoResponseExclusion)) return false;
        GetPolicyInfoResponseExclusion other = (GetPolicyInfoResponseExclusion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.exclusionIndicator==null && other.getExclusionIndicator()==null) || 
             (this.exclusionIndicator!=null &&
              this.exclusionIndicator.equals(other.getExclusionIndicator()))) &&
            ((this.exclusionCode==null && other.getExclusionCode()==null) || 
             (this.exclusionCode!=null &&
              this.exclusionCode.equals(other.getExclusionCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExclusionIndicator() != null) {
            _hashCode += getExclusionIndicator().hashCode();
        }
        if (getExclusionCode() != null) {
            _hashCode += getExclusionCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyInfoResponseExclusion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoResponseExclusion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exclusionIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ExclusionIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exclusionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ExclusionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
