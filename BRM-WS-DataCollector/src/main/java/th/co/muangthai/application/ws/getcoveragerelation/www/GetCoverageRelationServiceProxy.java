package th.co.muangthai.application.ws.getcoveragerelation.www;

public class GetCoverageRelationServiceProxy implements th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType getCoverageRelationService_PortType = null;
  
  public GetCoverageRelationServiceProxy() {
    _initGetCoverageRelationServiceProxy();
  }
  
  public GetCoverageRelationServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetCoverageRelationServiceProxy();
  }
  
  private void _initGetCoverageRelationServiceProxy() {
    try {
      getCoverageRelationService_PortType = (new th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_ServiceLocator()).getGetCoverageRelationWsImplPort();
      if (getCoverageRelationService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getCoverageRelationService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getCoverageRelationService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getCoverageRelationService_PortType != null)
      ((javax.xml.rpc.Stub)getCoverageRelationService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType getGetCoverageRelationService_PortType() {
    if (getCoverageRelationService_PortType == null)
      _initGetCoverageRelationServiceProxy();
    return getCoverageRelationService_PortType;
  }
  
  public th.co.muangthai.application.ws.getcoveragerelation.www.HeaderGetCoverageRelationResponse getCoverageRelation(th.co.muangthai.application.ws.getcoveragerelation.www.HeaderGetCoverageRelationRequest request) throws java.rmi.RemoteException{
    if (getCoverageRelationService_PortType == null)
      _initGetCoverageRelationServiceProxy();
    return getCoverageRelationService_PortType.getCoverageRelation(request);
  }
  
  
}