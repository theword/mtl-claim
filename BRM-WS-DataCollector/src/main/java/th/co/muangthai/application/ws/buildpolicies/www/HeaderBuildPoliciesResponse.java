/**
 * HeaderBuildPoliciesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.buildpolicies.www;

public class HeaderBuildPoliciesResponse  extends th.co.muangthai.application.ws.buildpolicies.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesResponse[] buildPoliciesResponseList;

    public HeaderBuildPoliciesResponse() {
    }

    public HeaderBuildPoliciesResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesResponse[] buildPoliciesResponseList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.buildPoliciesResponseList = buildPoliciesResponseList;
    }


    /**
     * Gets the buildPoliciesResponseList value for this HeaderBuildPoliciesResponse.
     * 
     * @return buildPoliciesResponseList
     */
    public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesResponse[] getBuildPoliciesResponseList() {
        return buildPoliciesResponseList;
    }


    /**
     * Sets the buildPoliciesResponseList value for this HeaderBuildPoliciesResponse.
     * 
     * @param buildPoliciesResponseList
     */
    public void setBuildPoliciesResponseList(th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesResponse[] buildPoliciesResponseList) {
        this.buildPoliciesResponseList = buildPoliciesResponseList;
    }

    public th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesResponse getBuildPoliciesResponseList(int i) {
        return this.buildPoliciesResponseList[i];
    }

    public void setBuildPoliciesResponseList(int i, th.co.muangthai.application.ws.buildpolicies.www.BuildPoliciesResponse _value) {
        this.buildPoliciesResponseList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderBuildPoliciesResponse)) return false;
        HeaderBuildPoliciesResponse other = (HeaderBuildPoliciesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.buildPoliciesResponseList==null && other.getBuildPoliciesResponseList()==null) || 
             (this.buildPoliciesResponseList!=null &&
              java.util.Arrays.equals(this.buildPoliciesResponseList, other.getBuildPoliciesResponseList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBuildPoliciesResponseList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBuildPoliciesResponseList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBuildPoliciesResponseList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderBuildPoliciesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.buildpolicies.ws.application.muangthai.co.th/", "headerBuildPoliciesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildPoliciesResponseList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BuildPoliciesResponseList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.buildpolicies.ws.application.muangthai.co.th/", "buildPoliciesResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
