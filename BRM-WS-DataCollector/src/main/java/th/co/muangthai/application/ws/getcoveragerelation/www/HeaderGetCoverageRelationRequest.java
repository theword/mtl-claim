/**
 * HeaderGetCoverageRelationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getcoveragerelation.www;

public class HeaderGetCoverageRelationRequest  extends th.co.muangthai.application.ws.getcoveragerelation.www.HeaderXml  implements java.io.Serializable {
    private java.lang.String clientNumber;

    private java.lang.String policyNumber;

    public HeaderGetCoverageRelationRequest() {
    }

    public HeaderGetCoverageRelationRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String clientNumber,
           java.lang.String policyNumber) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.clientNumber = clientNumber;
        this.policyNumber = policyNumber;
    }


    /**
     * Gets the clientNumber value for this HeaderGetCoverageRelationRequest.
     * 
     * @return clientNumber
     */
    public java.lang.String getClientNumber() {
        return clientNumber;
    }


    /**
     * Sets the clientNumber value for this HeaderGetCoverageRelationRequest.
     * 
     * @param clientNumber
     */
    public void setClientNumber(java.lang.String clientNumber) {
        this.clientNumber = clientNumber;
    }


    /**
     * Gets the policyNumber value for this HeaderGetCoverageRelationRequest.
     * 
     * @return policyNumber
     */
    public java.lang.String getPolicyNumber() {
        return policyNumber;
    }


    /**
     * Sets the policyNumber value for this HeaderGetCoverageRelationRequest.
     * 
     * @param policyNumber
     */
    public void setPolicyNumber(java.lang.String policyNumber) {
        this.policyNumber = policyNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderGetCoverageRelationRequest)) return false;
        HeaderGetCoverageRelationRequest other = (HeaderGetCoverageRelationRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.clientNumber==null && other.getClientNumber()==null) || 
             (this.clientNumber!=null &&
              this.clientNumber.equals(other.getClientNumber()))) &&
            ((this.policyNumber==null && other.getPolicyNumber()==null) || 
             (this.policyNumber!=null &&
              this.policyNumber.equals(other.getPolicyNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClientNumber() != null) {
            _hashCode += getClientNumber().hashCode();
        }
        if (getPolicyNumber() != null) {
            _hashCode += getPolicyNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderGetCoverageRelationRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getcoveragerelation.ws.application.muangthai.co.th/", "headerGetCoverageRelationRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "policyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
