/**
 * GetPolicyByClientService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicybyclient.www;

public interface GetPolicyByClientService_Service extends javax.xml.rpc.Service {

/**
 * OSB Service
 */
    public java.lang.String getGetPolicyByClientWsImplPortAddress();

    public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType getGetPolicyByClientWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType getGetPolicyByClientWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
