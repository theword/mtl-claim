/**
 * GetPolicyInfoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInfoRequest  extends th.co.muangthai.application.ws.getpolicyinformation.www.HeaderXml  implements java.io.Serializable {
    private th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy[] getPolicyInfoRequestList;

    public GetPolicyInfoRequest() {
    }

    public GetPolicyInfoRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy[] getPolicyInfoRequestList) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.getPolicyInfoRequestList = getPolicyInfoRequestList;
    }


    /**
     * Gets the getPolicyInfoRequestList value for this GetPolicyInfoRequest.
     * 
     * @return getPolicyInfoRequestList
     */
    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy[] getGetPolicyInfoRequestList() {
        return getPolicyInfoRequestList;
    }


    /**
     * Sets the getPolicyInfoRequestList value for this GetPolicyInfoRequest.
     * 
     * @param getPolicyInfoRequestList
     */
    public void setGetPolicyInfoRequestList(th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy[] getPolicyInfoRequestList) {
        this.getPolicyInfoRequestList = getPolicyInfoRequestList;
    }

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy getGetPolicyInfoRequestList(int i) {
        return this.getPolicyInfoRequestList[i];
    }

    public void setGetPolicyInfoRequestList(int i, th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy _value) {
        this.getPolicyInfoRequestList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyInfoRequest)) return false;
        GetPolicyInfoRequest other = (GetPolicyInfoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.getPolicyInfoRequestList==null && other.getGetPolicyInfoRequestList()==null) || 
             (this.getPolicyInfoRequestList!=null &&
              java.util.Arrays.equals(this.getPolicyInfoRequestList, other.getGetPolicyInfoRequestList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGetPolicyInfoRequestList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyInfoRequestList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyInfoRequestList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyInfoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyInfoRequestList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "getPolicyInfoRequestList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.getpolicyinformation.ws.application.muangthai.co.th/", "getPolicyInfoRequestPolicy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
