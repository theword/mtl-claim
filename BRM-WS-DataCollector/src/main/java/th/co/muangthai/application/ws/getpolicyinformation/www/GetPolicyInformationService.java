/**
 * GetPolicyInformationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicyinformation.www;

public interface GetPolicyInformationService extends javax.xml.rpc.Service {
    public java.lang.String getGetPolicyInformationWsImplPortAddress();

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation getGetPolicyInformationWsImplPort() throws javax.xml.rpc.ServiceException;

    public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation getGetPolicyInformationWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
