/**
 * GetPolicyCoveragesService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package th.co.muangthai.application.ws.getpolicycoverages.www;

public interface GetPolicyCoveragesService extends java.rmi.Remote {
    public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponse getPolicyCoverages(th.co.muangthai.application.ws.getpolicycoverages.www.HeaderGetPolicyCoveragesRequest request) throws java.rmi.RemoteException;
}
