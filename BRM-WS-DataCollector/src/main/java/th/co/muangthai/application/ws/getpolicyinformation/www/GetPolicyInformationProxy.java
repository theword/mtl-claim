package th.co.muangthai.application.ws.getpolicyinformation.www;

public class GetPolicyInformationProxy implements th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation getPolicyInformation = null;
  
  public GetPolicyInformationProxy() {
    _initGetPolicyInformationProxy();
  }
  
  public GetPolicyInformationProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetPolicyInformationProxy();
  }
  
  private void _initGetPolicyInformationProxy() {
    try {
      getPolicyInformation = (new th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationServiceLocator()).getGetPolicyInformationWsImplPort();
      if (getPolicyInformation != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getPolicyInformation)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getPolicyInformation)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getPolicyInformation != null)
      ((javax.xml.rpc.Stub)getPolicyInformation)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation getGetPolicyInformation() {
    if (getPolicyInformation == null)
      _initGetPolicyInformationProxy();
    return getPolicyInformation;
  }
  
  public th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponse getPolicyInformation(th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequest request) throws java.rmi.RemoteException{
    if (getPolicyInformation == null)
      _initGetPolicyInformationProxy();
    return getPolicyInformation.getPolicyInformation(request);
  }
  
  
}