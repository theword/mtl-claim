package th.co.muangthai.application.ws.getpolicycoverages.www;

public class GetPolicyCoveragesServiceProxy implements th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService {
  private String _endpoint = null;
  private th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService getPolicyCoveragesService = null;
  
  public GetPolicyCoveragesServiceProxy() {
    _initGetPolicyCoveragesServiceProxy();
  }
  
  public GetPolicyCoveragesServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetPolicyCoveragesServiceProxy();
  }
  
  private void _initGetPolicyCoveragesServiceProxy() {
    try {
      getPolicyCoveragesService = (new th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingQSServiceLocator()).getGetPolicyCoveragesWsImplPortBindingQSPort();
      if (getPolicyCoveragesService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getPolicyCoveragesService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getPolicyCoveragesService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getPolicyCoveragesService != null)
      ((javax.xml.rpc.Stub)getPolicyCoveragesService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService getGetPolicyCoveragesService() {
    if (getPolicyCoveragesService == null)
      _initGetPolicyCoveragesServiceProxy();
    return getPolicyCoveragesService;
  }
  
  public th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponse getPolicyCoverages(th.co.muangthai.application.ws.getpolicycoverages.www.HeaderGetPolicyCoveragesRequest request) throws java.rmi.RemoteException{
    if (getPolicyCoveragesService == null)
      _initGetPolicyCoveragesServiceProxy();
    return getPolicyCoveragesService.getPolicyCoverages(request);
  }
  
  
}