/*
 * 
 */
package com.mtl.jdbc.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


/**
 * The Interface GenericJdbcDao.
 */
public interface GenericJdbcDao {

	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public JdbcTemplate getTemplate();
	
	/**
	 * Gets the named parameter jdbc template.
	 *
	 * @return the named parameter jdbc template
	 */
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate();
}
