/*
 * 
 */
package com.mtl.dto;

import java.util.Date;

/**
 * The Class DiagnosisDto.
 */
public class DiagnosisDto {
	
	/** The claim number. */
	private String 	claimNumber;
	
	/** The diagnosis code. */
	private String	diagnosisCode;
	
	/** The admission date. */
	private Date	admissionDate;
	
	/** The current. */
	private boolean current;
	
	/**
	 * Instantiates a new diagnosis dto.
	 *
	 * @param claimNumber the claim number
	 * @param diagnosisCode the diagnosis code
	 * @param admissionDate the admission date
	 */
	public DiagnosisDto(String claimNumber, String diagnosisCode, Date admissionDate){
		this.claimNumber 	= claimNumber;
		this.diagnosisCode 	= diagnosisCode;
		this.admissionDate 	= admissionDate;
		this.current		= false;
	}
	
	/**
	 * Gets the claim number.
	 *
	 * @return the claim number
	 */
	public String getClaimNumber() {
		return claimNumber;
	}
	
	/**
	 * Sets the claim number.
	 *
	 * @param claimNumber the new claim number
	 */
	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}
	
	/**
	 * Gets the diagnosis code.
	 *
	 * @return the diagnosis code
	 */
	public String getDiagnosisCode() {
		return diagnosisCode;
	}
	
	/**
	 * Sets the diagnosis code.
	 *
	 * @param diagnosisCode the new diagnosis code
	 */
	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}
	
	/**
	 * Gets the admission date.
	 *
	 * @return the admission date
	 */
	public Date getAdmissionDate() {
		return admissionDate;
	}
	
	/**
	 * Sets the admission date.
	 *
	 * @param admissionDate the new admission date
	 */
	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	/**
	 * Checks if is current.
	 *
	 * @return true, if is current
	 */
	public boolean isCurrent() {
		return current;
	}

	/**
	 * Sets the current.
	 *
	 * @param current the new current
	 */
	public void setCurrent(boolean current) {
		this.current = current;
	}
	
	
	
}
