/*
 * 
 */
package com.mtl.dto;

import java.util.Date;

/**
 * The Class PolicyDto.
 */
public class PolicyDto {
	
	/** The policy number. */
	private String 	policyNumber;
	
	/** The paid date. */
	private Date	paidDate;
	
	/** The payment date. */
	private	Date	paymentDate;
	
	/** The expire date. */
	private Date	expireDate;
	
	private int 	paymentMode;
	
	/**
	 * Instantiates a new policy dto.
	 *
	 * @param policyNumber the policy number
	 * @param paidDate the paid date
	 * @param paymentDate the payment date
	 */
	public PolicyDto(String policyNumber,Date paidDate,Date paymentDate,int paymentMode){
		this.policyNumber 	= policyNumber;
		this.paidDate		= paidDate;
		this.paymentDate	= paymentDate;
		this.paymentMode	= paymentMode;
	}
	
	public PolicyDto(String policyNumber,Date paidDate,Date paymentDate){
		this.policyNumber 	= policyNumber;
		this.paidDate		= paidDate;
		this.paymentDate	= paymentDate;
	}

	/**
	 * Instantiates a new policy dto.
	 *
	 * @param policyNumber the policy number
	 * @param expireDate the expire date
	 */
	public PolicyDto(String policyNumber,Date expireDate){
		this.policyNumber 	= policyNumber;
		this.expireDate		= expireDate;
	}
	
	/**
	 * Gets the policy number.
	 *
	 * @return the policy number
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * Sets the policy number.
	 *
	 * @param policyNumber the new policy number
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * Gets the paid date.
	 *
	 * @return the paid date
	 */
	public Date getPaidDate() {
		return paidDate;
	}

	/**
	 * Sets the paid date.
	 *
	 * @param paidDate the new paid date
	 */
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * Gets the expire date.
	 *
	 * @return the expire date
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * Sets the expire date.
	 *
	 * @param expireDate the new expire date
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public int getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(int paymentMode) {
		this.paymentMode = paymentMode;
	}

}
