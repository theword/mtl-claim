package com.mtl.dto;

public class SumContinuity {
	private double sumPercent;
	private double sumAmount;
	
	public SumContinuity(double sumPercent, double sumAmount){
		this.sumPercent = sumPercent;
		this.sumAmount = sumAmount;
	}
	
	public double getSumPercent() {
		return sumPercent;
	}
	public void setSumPercent(double sumPercent) {
		this.sumPercent = sumPercent;
	}
	public double getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(double sumAmount) {
		this.sumAmount = sumAmount;
	}
	
	
}
