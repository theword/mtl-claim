package com.mtl.dto;

public class PolicyFactor {
	
	private Double sixMonthAnnualFactor;
	private Double quarterlyFactor;
	private Double monthlyFactor;
	
	public PolicyFactor(){
	
	}
	
	public PolicyFactor(Double sixMonthAnnualFactor, Double quarterlyFactor, Double monthlyFactor){
		setSixMonthAnnualFactor(sixMonthAnnualFactor);
		setQuarterlyFactor(quarterlyFactor);
		setMonthlyFactor(monthlyFactor);
	}

	public Double getSixMonthAnnualFactor() {
		return sixMonthAnnualFactor;
	}

	public void setSixMonthAnnualFactor(Double sixMonthAnnualFactor) {
		this.sixMonthAnnualFactor = sixMonthAnnualFactor;
	}

	public Double getQuarterlyFactor() {
		return quarterlyFactor;
	}

	public void setQuarterlyFactor(Double quarterlyFactor) {
		this.quarterlyFactor = quarterlyFactor;
	}

	public Double getMonthlyFactor() {
		return monthlyFactor;
	}

	public void setMonthlyFactor(Double monthlyFactor) {
		this.monthlyFactor = monthlyFactor;
	}


}
