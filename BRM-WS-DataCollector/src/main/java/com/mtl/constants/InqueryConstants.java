/*
 * 
 */
package com.mtl.constants;

/**
 * The Class InqueryConstants.
 */
public class InqueryConstants {
	
	/** The Constant PARAM_BLANK. */
	public static final String PARAM_BLANK = "''";
	
	/** The Constant PARAM_TBL_BLANK. */
	public static final String PARAM_TBL_BLANK = "('',0)";
	
	/** The Constant ILIFE_SCHEMA. */
	public static final String ILIFE_SCHEMA = "MTA";
	
	/**
	 * The Class UWSchema.
	 */
	public static class UWSchema{
		
		/** The Constant CP3FPRD. */
		public static final String CP3FPRD 	 = "CP3FPRD"; 
		
		/** The Constant CP3FSUP. */
		public static final String CP3FSUP   = "CP3FSUP"; 		
	}
	
}
