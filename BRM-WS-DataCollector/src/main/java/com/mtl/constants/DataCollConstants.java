/*
 * 
 */
package com.mtl.constants;

/**
 * The Class DataCollConstants.
 */
public class DataCollConstants {

	/** The Constant BUNDLE_KEY. */
	public static final String BUNDLE_KEY = "ApplicationResources";
	
	/**
	 * The Class ValidateType.
	 */
	public class ValidateType{
		
		/** The Constant NONE. */
		public static final String NONE = "0";
		
		/** The Constant VALIDATE_ALL. */
		public static final String VALIDATE_ALL = "1";
		
		/** The Constant VALIDATE_PARTIAL. */
		public static final String VALIDATE_PARTIAL = "2";
	}
}
