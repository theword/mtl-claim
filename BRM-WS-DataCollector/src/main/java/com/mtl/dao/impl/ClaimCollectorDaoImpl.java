package com.mtl.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mtl.LogMonitor;
import com.mtl.continuity.model.mtlclaim.Coverage;
import com.mtl.dao.ClaimCollectorDao;
import com.mtl.dto.DiagnosisDto;
import com.mtl.dto.SumContinuity;
import com.mtl.eligible.model.mtlclaim.master.DiagnosisMs;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;
import com.mtl.ws.bean.PlanDto;
import com.mtl.ws.service.impl.WSClientServiceImpl;

public class ClaimCollectorDaoImpl implements ClaimCollectorDao {

	private Logger log = Logger.getLogger(this.getClass());

	private final Locale ENG_LOCALE = new Locale("en", "EN");
	private WSClientServiceImpl wsClientService = new WSClientServiceImpl();

	private final int SUCCESS = 0;
	private int status;

	 private final String schema = "LPOS.";
//	private final String schema = "";

	private LogMonitor logMonitor;

	public void setLogMonitor(LogMonitor logMonitor) {
		this.logMonitor = logMonitor;
	}

	public com.mtl.benefitAnalysis.model.mtlclaim.Coverage buildCoverageFromCoverageHistory(
			final com.mtl.benefitAnalysis.model.mtlclaim.Coverage coverage, final String clientNumber, final String policyNumber,
			final Date requestDate, final List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- Build coverage from coverage history called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Build coverage from coverage history called: " + e);
			return null;
		}
		StringBuilder sql = new StringBuilder();

		final String coverageCode = coverage.getCoverageCode();

		if (coverageCode != null && !policyNumber.startsWith("9")) {// &&
																	// policyNumber.length()!=8){
			sql.append(" SELECT TB2.PLAN_CODE, ");
			sql.append(" TB3.PLAN_GROUP_TYPE, ");
			sql.append(" TB2.EFFECTIVE_DATE, ");
			sql.append(" TB2.MAXIMUM_LIMIT, ");
			sql.append(" TB3.PLAN_GROUP_CODE ");
			sql.append(" FROM CLM_PLAN_TB TB2 ");
			sql.append(" INNER JOIN CLM_PLAN_GROUP_TB TB3 ON TB2.PLAN_GROUP_CODE = TB3.PLAN_GROUP_CODE ");
			sql.append(" WHERE TB2.PLAN_CODE = '" + coverageCode + "'");
			sql.append(" AND TB2.IS_DELETED = 0");
			sql.append(" AND TB3.IS_DELETED = 0");

			log.debug("SQL Code: " + sql.toString());

			try {
				logMonitor.requestLog("buildCoverageFromCoverageHistory", policyNumber, clientNumber, null, null, coverageCode);
				ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
				logMonitor.responseLog("buildCoverageFromCoverageHistory", null, null);
				while (rs.next()) {
					String coverageStatus = coverage.getCoverageStatus();
					String trailerNumber = coverage.getTrailerNumber();
					Double faceAmount = coverage.getFaceAmount();
					Date issuedDate = coverage.getIssuedDate();
					Date maturityDate = coverage.getMaturityDate();
					BigDecimal sumInsured = coverage.getCoverage().getSumInsured();
					com.mtl.benefitAnalysis.model.mtlclaim.master.CoverageMs coverageMs = new com.mtl.benefitAnalysis.model.mtlclaim.master.CoverageMs(
							"", coverageCode, coverageStatus, issuedDate, rs.getString("PLAN_GROUP_TYPE"),
							stringToDate(rs.getString("EFFECTIVE_DATE")), trailerNumber, faceAmount, rs.getDouble("MAXIMUM_LIMIT"),
							rs.getString("PLAN_GROUP_CODE"));
					coverageMs.setMaturityDate(maturityDate);
					coverageMs.setClaimYearAmountRHM(maxSumInsuredRHM(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							claimNumbers, lof4claimConnection));
					coverageMs.setClaimYearAmountRHL(maxSumInsuredRHL(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					coverageMs.setClaimYearAmountRHG(maxSumInsuredRHG(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));// add
																								// by
																								// ging
																								// 2016-02-15
					/** 20160919 Add ClaimYearAmountRHH */
					coverageMs.setClaimYearAmountRHH(maxSumInsuredRHH(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					if ("RHE".equals(rs.getString("PLAN_GROUP_CODE")) || "LHE".equals(rs.getString("PLAN_GROUP_CODE"))) {
						/** 20180417 Add ClaimYearAmountRHE */
						coverageMs.setClaimYearAmountRHE(maxSumInsuredRHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						// jame
						coverageMs.setClaimYearALM01RHE(claimYearALM01RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearCHUP1RHE(claimYearCHUP1RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearVAC01RHE(claimYearVAC01RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearDET01RHE(claimYearDET01RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearEYE01RHE(claimYearEYE01RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearOPE01RHE(claimYearOPE01RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearESP50RHE(claimYearESP50RHE(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearAmountTRN(maxSumInsuredTRN(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setMaxLife(maxLife(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setMaxTimesBenefit(maxTimesBenefit(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setMaxTimeSP1(maxTimeSP1(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearAmountMEQ(maxSumInsuredEMQ(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					}

					if ("RHP".equals(rs.getString("PLAN_GROUP_CODE"))) {
						coverageMs.setClaimYearAmountRHP(maxSumInsuredRHP(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearAmountGen2(maxSumInsuredGen2(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, rs.getString("PLAN_GROUP_CODE"), lof4claimConnection));
						coverageMs.setClaimYearAmountGen3(maxSumInsuredGen3(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
						coverageMs.setClaimYearAmountTRN(maxSumInsuredTRN(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
								coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					}

					coverageMs.setMaxFourTimesBenefit(maxFourTimesBenefit(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					coverageMs.setClaimYearMilkyWAY(claimYearMilkyWAY(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					coverageMs.setClaimYearAmountOD(maxSumInsuredOD(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					coverageMs.setCoverageApprovedTotal(coverageApprovedTotal(coverageCode, clientNumber, policyNumber, issuedDate, requestDate,
							coverageMs.getTrailerNumber(), claimNumbers, lof4claimConnection));
					coverageMs.setSumInsured(sumInsured);// add by TeTe
															// 2015-12-09
															// receive for app
					com.mtl.benefitAnalysis.model.mtlclaim.Coverage rec = new com.mtl.benefitAnalysis.model.mtlclaim.Coverage(coverageMs, null);

					try {
						lof4claimStatement.close();
					}
					catch (Exception e) {
						log.error("[Error Closed statment] buildCoverageFromCoverageHistory : ", e);
						return null;
					}
					return rec;
				}
			}
			catch (SQLException e) {
				log.error("[Error] buildCoverageFromCoverageHistory : ", e);
				logMonitor.responseLog("buildCoverageFromCoverageHistory", null, e.getMessage());
				try {
					lof4claimStatement.close();
				}
				catch (Exception ex) {
					log.error("[Error Closed statment] buildCoverageFromCoverageHistory : ", ex);
					return null;
				}
			}
		}
		else {
			log.debug("case ILife");

			sql.append(" SELECT ");
			sql.append(" TB3.PLAN_GROUP_TYPE, ");
			sql.append(" TB2.EFFECTIVE_DATE, ");
			sql.append(" TB2.MAXIMUM_LIMIT, ");
			sql.append(" TB3.PLAN_GROUP_CODE ");
			sql.append(" FROM CLM_PLAN_TB TB2 ");
			sql.append(" INNER JOIN CLM_PLAN_GROUP_TB TB3 ");
			sql.append(" ON TB2.PLAN_GROUP_CODE   = TB3.PLAN_GROUP_CODE ");
			sql.append(" WHERE TB2.PLAN_CODE = ('" + coverage.getCoverageCode() + "') ");
			sql.append(" AND TB3.PLAN_GROUP_TYPE IN ('1','2','3','4','5','6','7') ");
			sql.append(" AND TB2.IS_DELETED = 0");
			sql.append(" AND TB3.IS_DELETED = 0");

			log.debug("case ILife SQL Code: " + sql.toString());
			try {
				logMonitor.requestLog("buildCoverageFromCoverageHistory_ILife", policyNumber, clientNumber, null, null, coverage.getCoverageCode());
				ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
				logMonitor.responseLog("buildCoverageFromCoverageHistory_ILife", null, null);
				while (rs.next()) {
					Date issuedDate = coverage.getIssuedDate();
					com.mtl.benefitAnalysis.model.mtlclaim.Coverage rec = new com.mtl.benefitAnalysis.model.mtlclaim.Coverage(
							new com.mtl.benefitAnalysis.model.mtlclaim.master.CoverageMs(coverage.getCoverageHistoryID(), coverage.getCoverageCode(),
									"", issuedDate, rs.getString("PLAN_GROUP_TYPE"), stringToDate(rs.getString("EFFECTIVE_DATE")),
									coverage.getTrailerNumber(), coverage.getFaceAmount(), rs.getDouble("MAXIMUM_LIMIT"),
									rs.getString("PLAN_GROUP_CODE")), null);
					try {
						lof4claimStatement.close();
					}
					catch (Exception e) {
						log.error("[Error Closed statment] buildCoverageFromCoverageHistory : ", e);
						return null;
					}
					return rec;
				}
			}
			catch (SQLException e) {
				log.error("[Error Web Service] getLOF4ClaimStep : ", e);
				logMonitor.responseLog("buildCoverageFromCoverageHistory_ILife", null, e.getMessage());
				try {
					lof4claimStatement.close();
				}
				catch (Exception ex) {
					log.error("[Error Closed statment] buildCoverageFromCoverageHistory_ILife : ", ex);
					return null;
				}
			}
		}
		return coverage;
	}

	private double retrieveModalPremium(String coverageHistoryID, Connection lof4claimConnection) {
		log.debug("--- Retrieve Modal Premium called ---");

		wsClientService.init();
		String collectorType = wsClientService.getCollectorType();

		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement RetrieveModalPremiumcalled: " + e);
		}
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append("MODAL_PREMIUM ");
		sql.append("FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_POLICY_TR ");
		sql.append("WHERE POLICY_HISTORY_ID = ( ");
		sql.append("  SELECT POLICY_HISTORY_ID ");
		sql.append("  FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_COVERAGE_TR ");
		sql.append("  WHERE COVERAGE_HISTORY_ID = " + coverageHistoryID + "");
		sql.append(") ");
		log.debug("SQL Code: " + sql.toString());

		List<Double> doubles = new ArrayList<Double>();
		try {
			logMonitor.requestLog("retrieveModalPremium", null, null, null, null, coverageHistoryID);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("retrieveModalPremium", null, null);
			while (rs.next()) {
				Double result = rs.getDouble("MODAL_PREMIUM");
				doubles.add(result);
			}
		}
		catch (SQLException e) {
			log.error("[Error Web Service] getLOF4ClaimStep : ", e);
			logMonitor.responseLog("retrieveModalPremium", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] Retrieve Modal Premium called : ", e);
		}
		return doubles.size() > 0 ? doubles.get(0).doubleValue() : 0;
	}

	public List<com.mtl.benefitAnalysis.model.mtlclaim.Benefit> buildBenefitsForClaim(com.mtl.benefitAnalysis.model.mtlclaim.Claim claim,
			String clientNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- Build benefits from coverage code called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Buildbenefitsfromcoveragecodecalled: " + e);
		}
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT TB1.BENEFIT_CODE ");
		sql.append(",TB2.BENEFIT_GROUP_CODE ");
		sql.append(",CASE WHEN TB1.SUM_INSURED > 0 THEN TB1.SUM_INSURED ELSE TB2.COVER_AMOUNT END AS SUM_INSURED ");
		sql.append(",TB1.MAX_PAY_DAY ");
		sql.append(",TB1.MAXIMUM_LIMIT ");
		sql.append(",TB1.BENEFIT_MODE ");
		sql.append(",TB1.PERCENT ");
		sql.append(",TB1.LINK_MODE ");
		sql.append(",TB3.BENEFIT_GROUP_SHORT_NAME ");
		sql.append(",TB3.CALCULATE_OPTION ");
		sql.append(",TB1.GN_EVENT_PNTR ");
		sql.append("FROM CLM_PLAN_BENEFIT_TB TB1 ");
		sql.append("INNER JOIN CLM_BENEFIT_TB TB2 ON TB1.BENEFIT_CODE = TB2.BENEFIT_CODE ");
		sql.append("INNER JOIN CLM_BENEFIT_GROUP_TB TB3 ON TB2.BENEFIT_GROUP_CODE = TB3.BENEFIT_GROUP_CODE ");
		sql.append("WHERE PLAN_CODE = '" + claim.getCoverage().getCoverageCode() + "' ");
		sql.append(" AND TB1.IS_DELETED = 0 ");
		sql.append(" AND TB2.IS_DELETED = 0 ");
		sql.append(" AND TB3.IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		final com.mtl.benefitAnalysis.model.mtlclaim.Claim finalClaim = claim;
		final String finalCoverageCode = finalClaim.getCoverage().getCoverageCode();
		final String finalTrailerNumber = finalClaim.getCoverage().getTrailerNumber();
		final String finalCilentNumber = clientNumber;
		final Date finalIssueDate = finalClaim.getCoverage().getIssuedDate();
		final Date finalRequestDate = (finalClaim.getClaimType() == com.mtl.benefitAnalysis.model.enumerator.ClaimType.ACCIDENT) ? finalClaim
				.getIncidentDate() : null;
		List<com.mtl.benefitAnalysis.model.mtlclaim.Benefit> resultList = new ArrayList<com.mtl.benefitAnalysis.model.mtlclaim.Benefit>();
		try {
			logMonitor.requestLog("buildBenefitsForClaim", null, clientNumber, null, null, claim.getCoverage().getCoverageCode());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildBenefitsForClaim", null, null);

			while (rs.next()) {
				double sumInsured = 0;
				if (finalClaim.getCoverage().getCoverageCode().equals("RDD701")) {
					if (com.mtl.benefitAnalysis.util.ModelUtility.addYear((Date) (finalClaim.getCoverage().getIssuedDate().clone()), 2).after(
							finalClaim.getAdmissionDate()))
						//sumInsured = retrieveModalPremium(finalClaim.getCoverage().getCoverageHistoryID(), lof4posConnection) * 1.02;
						sumInsured = retrieveModalPremium(finalClaim.getCoverage().getCoverageHistoryID(), lof4claimConnection) * 1.02;
					else
						sumInsured = finalClaim.getCoverage().getFaceAmount();
				}
				else
					sumInsured = rs.getDouble("SUM_INSURED");
				com.mtl.benefitAnalysis.model.mtlclaim.Benefit rec = new com.mtl.benefitAnalysis.model.mtlclaim.Benefit(
						new com.mtl.benefitAnalysis.model.mtlclaim.master.BenefitMs(rs.getString("BENEFIT_CODE"), rs.getString("BENEFIT_GROUP_CODE"),
								sumInsured, rs.getDouble("PERCENT"), rs.getDouble("MAXIMUM_LIMIT"), rs.getInt("MAX_PAY_DAY"),
								com.mtl.benefitAnalysis.model.enumerator.BenefitMode.lookup(rs.getString("BENEFIT_MODE")),
								com.mtl.benefitAnalysis.model.enumerator.LinkMode.lookup(rs.getString("LINK_MODE")),
								rs.getString("BENEFIT_GROUP_SHORT_NAME"), com.mtl.benefitAnalysis.model.enumerator.CalculateOption.lookup(rs
										.getString("CALCULATE_OPTION"))));
				rec.getMasterModel().setPlanEvent(rs.getString("GN_EVENT_PNTR"));// add
																					// by
																					// TeTe
																					// 2015-12-09
																					// for
																					// plan
																					// event
				if (finalClaim.getClaimType() == com.mtl.benefitAnalysis.model.enumerator.ClaimType.ACCIDENT) {
					SumContinuity sumContinuity = getSumContinuity(finalCilentNumber, finalCoverageCode, finalTrailerNumber, rec.getBenefitCode(),
							finalIssueDate, finalRequestDate, lof4claimConnection);
					rec.setSumContinuityPercent(sumContinuity.getSumPercent());
					rec.setSumContinuityAmount(sumContinuity.getSumAmount());
				}
				resultList.add(rec);
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found benefitMs");
			logMonitor.responseLog("buildBenefitsForClaim", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] Buildbenefitsfromcoveragecodecalled : ", e);
		}
		return resultList;
	}

	private com.mtl.benefitAnalysis.model.mtlclaim.master.BenefitMs buildBenefitMs(String benefitCode, Connection lof4claimConnection) {
		log.debug("--- Build benefit ms called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Buildbenefitmscalled: " + e);
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" BENEFIT_CODE, ");
		sql.append(" ( SELECT ");
		sql.append("  BENEFIT_GROUP_CODE ");
		sql.append("  FROM CLM_BENEFIT_TB ");
		sql.append("  WHERE BENEFIT_CODE = '" + benefitCode + "'");
		sql.append("  AND IS_DELETED = 0 ");
		sql.append(" ) ");
		sql.append(" AS BENEFIT_GROUP_CODE, ");
		sql.append(" SUM_INSURED, ");
		sql.append(" MAX_PAY_DAY, ");
		sql.append(" MAXIMUM_LIMIT, ");
		sql.append(" BENEFIT_MODE, ");
		sql.append(" PERCENT, ");
		sql.append(" LINK_MODE ");
		sql.append(" FROM CLM_PLAN_BENEFIT_TB ");
		sql.append(" WHERE BENEFIT_CODE = '" + benefitCode + "' ");
		sql.append(" AND IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("buildBenefitMs", null, null, null, null, benefitCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildBenefitMs", null, null);
			while (rs.next()) {
				com.mtl.benefitAnalysis.model.mtlclaim.master.BenefitMs rec = new com.mtl.benefitAnalysis.model.mtlclaim.master.BenefitMs(
						rs.getString("BENEFIT_CODE"), rs.getString("BENEFIT_GROUP_CODE"), rs.getDouble("SUM_INSURED"), rs.getDouble("PERCENT"),
						rs.getDouble("MAXIMUM_LIMIT"), rs.getInt("MAX_PAY_DAY"), com.mtl.benefitAnalysis.model.enumerator.BenefitMode.lookup(rs
								.getString("BENEFIT_MODE")), com.mtl.benefitAnalysis.model.enumerator.LinkMode.lookup(rs.getString("LINK_MODE")));
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] Buildbenefitmscalled : ", e);
				}
				return rec;
			}
		}
		catch (Exception e) {
			log.debug("not found benefitMs");
			log.error("[Error] : ", e);
			logMonitor.responseLog("buildBenefitMs", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] Buildbenefitmscalled : ", e);
		}
		return null;
	}

	private com.mtl.continuity.model.mtlclaim.master.BenefitMs buildBenefitMsRulecontinuity(String benefitCode, Connection lof4claimConnection) {
		log.debug("--- Build benefit ms continuity called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Buildbenefitmscontinuitycalled: " + e);
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" BENEFIT_CODE, ");
		sql.append(" ( SELECT ");
		sql.append("  BENEFIT_GROUP_CODE ");
		sql.append("  FROM CLM_BENEFIT_TB ");
		sql.append("  WHERE BENEFIT_CODE = '" + benefitCode + "'");
		sql.append("  AND IS_DELETED = 0 ");
		sql.append(" ) ");
		sql.append(" AS BENEFIT_GROUP_CODE, ");
		sql.append(" SUM_INSURED, ");
		sql.append(" MAX_PAY_DAY, ");
		sql.append(" MAXIMUM_LIMIT, ");
		sql.append(" BENEFIT_MODE, ");
		sql.append(" PERCENT, ");
		sql.append(" LINK_MODE ");
		sql.append(" FROM CLM_PLAN_BENEFIT_TB ");
		sql.append(" WHERE BENEFIT_CODE = '" + benefitCode + "' ");
		sql.append(" AND IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("buildBenefitMsRulecontinuity", null, null, null, null, benefitCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildBenefitMsRulecontinuity", null, null);
			while (rs.next()) {
				com.mtl.continuity.model.mtlclaim.master.BenefitMs rec = new com.mtl.continuity.model.mtlclaim.master.BenefitMs(
						rs.getString("BENEFIT_CODE"), rs.getString("BENEFIT_GROUP_CODE"), rs.getDouble("SUM_INSURED"), rs.getDouble("PERCENT"),
						rs.getDouble("MAXIMUM_LIMIT"), rs.getInt("MAX_PAY_DAY"), com.mtl.continuity.model.enumerator.BenefitMode.lookup(rs
								.getString("BENEFIT_MODE")), com.mtl.continuity.model.enumerator.LinkMode.lookup(rs.getString("LINK_MODE")));
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] Buildbenefitmscontinuitycalled : ", e);
				}
				return rec;
			}
		}
		catch (Exception e) {
			log.debug("not found benefitMs");
			log.error("[Error]: ", e);
			logMonitor.responseLog("buildBenefitMsRulecontinuity", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] Buildbenefitmscontinuitycalled : ", e);
		}
		return null;
	}

	private List<com.mtl.benefitAnalysis.model.mtlclaim.Benefit> buildBenefits(String claimID, Connection lof4claimConnection) {
		log.debug("--- Build benefits called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Buildbenefitscalled: " + e);
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" BENEFIT_CODE, ");
		sql.append(" BILLED_PERCENT, ");
		sql.append(" BILLED_DAY, ");
		sql.append(" BILLED_AMOUNT, ");
		sql.append(" APPROVED_PERCENT, ");
		sql.append(" APPROVED_DAY, ");
		sql.append(" APPROVED_AMOUNT, ");
		sql.append(" APPROVED_TOTAL, ");
		sql.append(" BILLED_DEDUCT, ");
		sql.append(" APPROVED_DEDUCT ");
		sql.append(" FROM CLM_CLAIM_BENEFIT_TR ");
		sql.append(" WHERE CLAIM_NUMBER = '" + claimID + "' ");
		sql.append(" AND IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		List<com.mtl.benefitAnalysis.model.mtlclaim.Benefit> resultList = new ArrayList<com.mtl.benefitAnalysis.model.mtlclaim.Benefit>();
		try {
			logMonitor.requestLog("buildBenefits", null, null, null, null, claimID);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildBenefits", null, null);
			while (rs.next()) {
				com.mtl.benefitAnalysis.model.mtlclaim.master.BenefitMs benefitMs = buildBenefitMs(rs.getString("BENEFIT_CODE"), lof4claimConnection);
				if (benefitMs != null) {
					com.mtl.benefitAnalysis.model.mtlclaim.Benefit rec = new com.mtl.benefitAnalysis.model.mtlclaim.Benefit(benefitMs);
					rec.setBilledPercent(rs.getDouble("BILLED_PERCENT"));
					rec.setBilledAmount(rs.getDouble("BILLED_AMOUNT"));
					rec.setBilledDay(rs.getInt("BILLED_DAY"));
					rec.setClaimAmount(rs.getDouble("APPROVED_AMOUNT"));
					rec.setClaimDay(rs.getInt("APPROVED_DAY"));
					rec.setClaimPercent(rs.getDouble("APPROVED_PERCENT"));
					rec.setClaimTotalAmount(rs.getDouble("APPROVED_TOTAL"));

					com.mtl.benefitAnalysis.model.mtlclaim.DeDuct deDuct = new com.mtl.benefitAnalysis.model.mtlclaim.DeDuct();
					deDuct.setBilledDeDuct(rs.getDouble("BILLED_DEDUCT"));
					deDuct.setClaimDeDuct(rs.getDouble("APPROVED_DEDUCT"));
					rec.getDeDuctList().add(deDuct);

					resultList.add(rec);
				}
				else {
					try {
						lof4claimStatement.close();
					}
					catch (Exception e) {
						log.error("[Error Closed statment] Buildbenefitscalled : ", e);
					}
					return null;
				}
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found Buildbenefitscalled");
			logMonitor.responseLog("buildBenefits", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] Buildbenefitscalled : ", e);
		}
		return resultList;
	}

	private List<com.mtl.continuity.model.mtlclaim.Benefit> buildBenefitsRuleContinuity(String claimID, Connection lof4claimConnection) {
		log.debug("--- Build buildBenefitsRuleContinuity called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement BuildbuildBenefitsRuleContinuitycalled: " + e);
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" BENEFIT_CODE, ");
		sql.append(" BILLED_PERCENT, ");
		sql.append(" BILLED_DAY, ");
		sql.append(" BILLED_AMOUNT, ");
		sql.append(" APPROVED_PERCENT, ");
		sql.append(" APPROVED_DAY, ");
		sql.append(" APPROVED_AMOUNT, ");
		sql.append(" APPROVED_TOTAL, ");
		sql.append(" BILLED_DEDUCT, ");
		sql.append(" APPROVED_DEDUCT ");
		sql.append(" FROM CLM_CLAIM_BENEFIT_TR ");
		sql.append(" WHERE CLAIM_NUMBER = '" + claimID + "' ");
		sql.append(" AND IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		List<com.mtl.continuity.model.mtlclaim.Benefit> resultList = new ArrayList<com.mtl.continuity.model.mtlclaim.Benefit>();
		try {
			logMonitor.requestLog("buildBenefitsRuleContinuity", null, null, null, null, claimID);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildBenefitsRuleContinuity", null, null);
			while (rs.next()) {
				com.mtl.continuity.model.mtlclaim.master.BenefitMs benefitMs = buildBenefitMsRulecontinuity(rs.getString("BENEFIT_CODE"),
						lof4claimConnection);
				if (benefitMs != null) {
					com.mtl.continuity.model.mtlclaim.Benefit rec = new com.mtl.continuity.model.mtlclaim.Benefit(benefitMs);
					rec.setBilledPercent(rs.getDouble("BILLED_PERCENT"));
					rec.setBilledAmount(rs.getDouble("BILLED_AMOUNT"));
					rec.setBilledDay(rs.getInt("BILLED_DAY"));
					rec.setClaimAmount(rs.getDouble("APPROVED_AMOUNT"));
					rec.setClaimDay(rs.getInt("APPROVED_DAY"));
					rec.setClaimPercent(rs.getDouble("APPROVED_PERCENT"));
					rec.setClaimTotalAmount(rs.getDouble("APPROVED_TOTAL"));

					com.mtl.continuity.model.mtlclaim.DeDuct deDuct = new com.mtl.continuity.model.mtlclaim.DeDuct();
					deDuct.setBilledDeDuct(rs.getDouble("BILLED_DEDUCT"));
					deDuct.setClaimDeDuct(rs.getDouble("APPROVED_DEDUCT"));
					rec.getDeDuctList().add(deDuct);

					resultList.add(rec);
				}
				else {
					try {
						lof4claimStatement.close();
					}
					catch (Exception e) {
						log.error("[Error Closed statment] buildBenefitsRuleContinuity : ", e);
					}
					return null;
				}
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found buildBenefitsRuleContinuity");
			logMonitor.responseLog("buildBenefitsRuleContinuity", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] buildBenefitsRuleContinuity : ", e);
		}
		return resultList;
	}

	private List<com.mtl.benefitAnalysis.model.mtlclaim.Diagnosis> buildClaimedDiagnoses(String claimID, Connection lof4claimConnection,
			String callName) {
		log.debug("--- Build claim diagnoses called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement BuildClaimDiagnosesCalled: " + e);
		}

		StringBuilder sql = new StringBuilder();

		if ("OECLAIM".equalsIgnoreCase(callName)) {
			sql.append(" SELECT DISTINCT DIAGNOSIS_CODE, CLAIM_DIAGNOSIS_ID ");
			sql.append(" FROM CLM_CLAIM_DIAGNOSIS_TR ");
			sql.append(" WHERE CLAIM_NUMBER = '" + claimID + "' ");
			sql.append(" AND IS_DELETED = 0 ");
			sql.append(" AND ROWNUM = 1 ");
			sql.append(" ORDER BY CLAIM_DIAGNOSIS_ID ");
		}
		else {
			sql.append(" SELECT DISTINCT ");
			sql.append("  DIAGNOSIS_CODE ");
			sql.append(" FROM CLM_CLAIM_DIAGNOSIS_TR ");
			sql.append(" WHERE CLAIM_NUMBER = '" + claimID + "' ");
			sql.append(" AND IS_DELETED = 0 ");
		}

		log.debug("SQL Code: " + sql.toString());

		List<com.mtl.benefitAnalysis.model.mtlclaim.Diagnosis> resultList = new ArrayList<com.mtl.benefitAnalysis.model.mtlclaim.Diagnosis>();
		try {
			logMonitor.requestLog("buildClaimedDiagnoses", null, null, null, null, claimID);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildClaimedDiagnoses", null, null);
			while (rs.next()) {
				com.mtl.benefitAnalysis.model.mtlclaim.Diagnosis rec = new com.mtl.benefitAnalysis.model.mtlclaim.Diagnosis(
						new com.mtl.benefitAnalysis.model.mtlclaim.master.DiagnosisMs(rs.getString("DIAGNOSIS_CODE")));
				resultList.add(rec);
			}
		}
		catch (Exception e) {
			log.error("error : " + e.getMessage());
			log.debug("not found buildClaimedDiagnoses");
			logMonitor.responseLog("buildClaimedDiagnoses", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] buildClaimedDiagnoses : ", e);
		}
		return resultList;
	}

	private List<com.mtl.continuity.model.mtlclaim.Diagnosis> buildClaimedDiagnosesRuleContinuity(String claimID, Connection lof4claimConnection,
			String callName) {
		log.debug("--- Build claim diagnoses continuity called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Buildclaimdiagnosescontinuitycalled: " + e);
		}
		StringBuilder sql = new StringBuilder();

		if ("OECLAIM".equalsIgnoreCase(callName)) {
			sql.append(" SELECT DISTINCT DIAGNOSIS_CODE, CLAIM_DIAGNOSIS_ID ");
			sql.append(" FROM CLM_CLAIM_DIAGNOSIS_TR ");
			sql.append(" WHERE CLAIM_NUMBER = '" + claimID + "' ");
			sql.append(" AND IS_DELETED = 0 ");
			sql.append(" AND ROWNUM = 1 ");
			sql.append(" ORDER BY CLAIM_DIAGNOSIS_ID ");
		}
		else {
			sql.append(" SELECT DISTINCT ");
			sql.append("  DIAGNOSIS_CODE ");
			sql.append(" FROM CLM_CLAIM_DIAGNOSIS_TR ");
			sql.append(" WHERE CLAIM_NUMBER = '" + claimID + "' ");
			sql.append(" AND IS_DELETED = 0 ");
		}

		log.debug("SQL Code: " + sql.toString());

		List<com.mtl.continuity.model.mtlclaim.Diagnosis> resultList = new ArrayList<com.mtl.continuity.model.mtlclaim.Diagnosis>();
		try {
			logMonitor.requestLog("buildClaimedDiagnosesRuleContinuity", null, null, null, null, claimID);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildClaimedDiagnosesRuleContinuity", null, null);
			while (rs.next()) {
				com.mtl.continuity.model.mtlclaim.Diagnosis rec = new com.mtl.continuity.model.mtlclaim.Diagnosis(
						new com.mtl.continuity.model.mtlclaim.master.DiagnosisMs(rs.getString("DIAGNOSIS_CODE")));
				resultList.add(rec);
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found buildClaimedDiagnosesRuleContinuity");
			logMonitor.responseLog("buildClaimedDiagnosesRuleContinuity", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] buildClaimedDiagnosesRuleContinuity : ", e);
		}
		return resultList;
	}

	// revert qury to 1.6 by Ging by P'M 11-5-2016
	public List<com.mtl.continuity.model.mtlclaim.Claim> buildRelatedClaims(String clientId, com.mtl.continuity.model.enumerator.ClaimType claimType,
			Date admissionDate, Date incidentDate, Date dischargeDate, List<com.mtl.continuity.model.mtlclaim.Diagnosis> requestDiagnosis,
			String callName, Connection lof4claimConnection) {
		log.debug("--- Build related claims called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement Build related claims called: " + e);
		}
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT TB1.CLAIM_NUMBER, ");
		sql.append(" TB1.POLICY_NUMBER, ");
		sql.append(" TB1.COVERAGE_CODE, ");
		sql.append(" TB1.TRAILER_NUMBER, ");
		sql.append(" (SELECT PLAN_GROUP_TYPE ");
		sql.append(" FROM CLM_PLAN_GROUP_TB ");
		sql.append(" WHERE PLAN_GROUP_CODE = ");
		sql.append(" 	( ");
		sql.append(" 	 SELECT PLAN_GROUP_CODE FROM CLM_PLAN_TB ");
		sql.append("	 WHERE PLAN_CODE = TB1.COVERAGE_CODE ");
		sql.append(" 	 AND IS_DELETED = 0 ");
		sql.append(" 	) ");
		sql.append(" AND IS_DELETED = 0 ");
		sql.append(" ) AS PLAN_GROUP_TYPE, ");
		sql.append(" TB1.LINK_CLAIM_NUMBER, ");
		sql.append(" TB1.CLAIM_TYPE, ");
		sql.append(" TB1.ADMISSION_DATE, ");
		sql.append(" TB1.INCIDENT_DATE, ");
		sql.append(" TB1.DISCHARGE_DATE ");
		sql.append(" FROM CLM_CLAIM_HEADER_TR TB1 ");
		sql.append(" INNER JOIN clm_client_tb TB2 ON TB1.CLIENT_NUMBER = TB2.CLIENT_NUMBER ");

		if ("OECLAIM".equalsIgnoreCase(callName)) {
			sql.append(" WHERE TB1.CLAIM_STATUS  in ('04','05','06','07','11') ");
		}
		else {
			sql.append(" WHERE TB1.CLAIM_STATUS  in ('06','07','11') ");
		}

		sql.append(" AND TB1.IS_DELETED = 0 ");
		sql.append(" AND TB2.IS_DELETED = 0 ");

		if (StringUtils.isNotBlank(callName) && callName.equalsIgnoreCase("ILIFE") || StringUtils.isNotBlank(callName)
				&& callName.equalsIgnoreCase("LOF4CLM_IL")) {
			sql.append(" AND TB2.ILIFE_CUST_ID = '" + clientId + "' AND ");
		}
		else {
			sql.append(" AND TB2.CAPSIL_CLIENT_NUMBER = '" + clientId + "' AND ");
		}

		String diagnosisCode = "";
		if (requestDiagnosis != null && requestDiagnosis.size() > 0) {
			if ("OECLAIM".equalsIgnoreCase(callName)) {
				diagnosisCode = "'" + requestDiagnosis.get(0).getDiagnosisCode() + "'";
			}
			else {
				for (com.mtl.continuity.model.mtlclaim.Diagnosis diagnosis : requestDiagnosis)
					diagnosisCode = diagnosisCode + "'" + diagnosis.getDiagnosisCode() + "',";
				diagnosisCode = diagnosisCode.substring(0, diagnosisCode.length() - 1);
			}
		}

		String tempIncidentDate = "";

		if (incidentDate != null) {
			tempIncidentDate = dateToString(incidentDate);
		}

		sql.append(" (( ");
		sql.append(" TB1.claim_number in (SELECT distinct diag.claim_number FROM clm_claim_diagnosis_tr diag ");
		sql.append(" WHERE diag.diagnosis_code in (" + diagnosisCode + ") ");
		sql.append(" AND diag.IS_DELETED = 0 ) )");
		sql.append(" OR TB1.INCIDENT_DATE = TO_DATE('" + tempIncidentDate + "', 'DD-MM-YYYY') )");
		sql.append(" AND TB1.IS_DELETED = 0 ");// add by TeTe with Tua
												// 16/12/2013

		log.debug("SQL Code: " + sql.toString());
		List<com.mtl.continuity.model.mtlclaim.Claim> resultList = new ArrayList<com.mtl.continuity.model.mtlclaim.Claim>();
		try {
			logMonitor.requestLog("buildRelatedClaims", null, clientId, null, null, diagnosisCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildRelatedClaims", null, null);
			while (rs.next()) {
				com.mtl.continuity.model.mtlclaim.Claim rec = new com.mtl.continuity.model.mtlclaim.Claim(
						new com.mtl.continuity.model.mtlclaim.master.ClaimMs(rs.getString("CLAIM_NUMBER"),
								com.mtl.continuity.model.enumerator.ClaimType.lookup(rs.getString("CLAIM_TYPE")),
								stringToDate(rs.getString("ADMISSION_DATE")), stringToDate(rs.getString("INCIDENT_DATE")),
								stringToDate(rs.getString("DISCHARGE_DATE"))), rs.getString("POLICY_NUMBER"), new Coverage(
								new com.mtl.continuity.model.mtlclaim.master.CoverageMs(null, rs.getString("COVERAGE_CODE"), null, null,
										rs.getString("PLAN_GROUP_TYPE"), null, rs.getString("TRAILER_NUMBER"), 0), null),
						rs.getString("LINK_CLAIM_NUMBER"), buildBenefitsRuleContinuity(rs.getString("CLAIM_NUMBER"), lof4claimConnection),
						buildClaimedDiagnosesRuleContinuity(rs.getString("CLAIM_NUMBER"), lof4claimConnection, callName));
				resultList.add(rec);
			}
		}
		catch (Exception e) {
			log.error("error : " + e.getMessage());
			log.debug("not found buildRelatedClaims");
			logMonitor.responseLog("buildRelatedClaims", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] buildRelatedClaims : ", e);
		}
		return resultList;
	}

	public List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> buildContinuityClaim(
			List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> continuityClaims, Connection lof4claimConnection, String callName) {
		log.debug("--- Build continuity claim called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement BuildContinuityClaimCalled: " + e);
		}

		String claimNumber = "";
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append("  CLAIM_NUMBER, ");
		sql.append("  POLICY_NUMBER, ");
		sql.append("  COVERAGE_CODE, ");
		sql.append(" TRAILER_NUMBER, ");
		sql.append("  LINK_CLAIM_NUMBER, ");
		sql.append("  CLAIM_TYPE, ");
		sql.append("  ADMISSION_DATE, ");
		sql.append("  INCIDENT_DATE, ");
		sql.append("  DISCHARGE_DATE ");
		sql.append(" FROM CLM_CLAIM_HEADER_TR ");

		if ("OECLAIM".equalsIgnoreCase(callName)) {
			sql.append(" WHERE CLAIM_STATUS in ('04','05','06','07','11') ");
		}
		else {
			sql.append(" WHERE CLAIM_STATUS in ('06','07','11') ");
		}

		sql.append(" AND IS_DELETED = 0 ");
		sql.append(" AND LINK_CLAIM_NUMBER IN ( ");
		for (int i = 0; i < continuityClaims.size(); i++) {
			sql.append("'" + continuityClaims.get(i).getReferenceClaimGroupID() + "'");
			claimNumber = claimNumber + continuityClaims.get(i).getReferenceClaimGroupID();
			if (i < continuityClaims.size() - 1) {
				sql.append(", ");
			}
			else {
				sql.append(" ");
			}
		}
		sql.append(")");
		log.debug("SQL Code: " + sql.toString());
		List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> resultList = new ArrayList<com.mtl.benefitAnalysis.model.mtlclaim.Claim>();
		try {
			logMonitor.requestLog("buildContinuityClaim", null, null, null, null, claimNumber);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildContinuityClaim", null, null);
			while (rs.next()) {
				com.mtl.benefitAnalysis.model.mtlclaim.Claim rec = new com.mtl.benefitAnalysis.model.mtlclaim.Claim(
						new com.mtl.benefitAnalysis.model.mtlclaim.master.ClaimMs(rs.getString("CLAIM_NUMBER"),
								com.mtl.benefitAnalysis.model.enumerator.ClaimType.lookup(rs.getString("CLAIM_TYPE")),
								stringToDate(rs.getString("ADMISSION_DATE")), stringToDate(rs.getString("INCIDENT_DATE")),
								stringToDate(rs.getString("DISCHARGE_DATE"))), rs.getString("POLICY_NUMBER"),
						new com.mtl.benefitAnalysis.model.mtlclaim.Coverage(new com.mtl.benefitAnalysis.model.mtlclaim.master.CoverageMs(null, rs
								.getString("COVERAGE_CODE"), null, null, null, null, rs.getString("TRAILER_NUMBER"), 0), null),
						rs.getString("LINK_CLAIM_NUMBER"), buildBenefits(rs.getString("CLAIM_NUMBER"), lof4claimConnection), buildClaimedDiagnoses(
								rs.getString("CLAIM_NUMBER"), lof4claimConnection, callName));
				resultList.add(rec);
			}
		}
		catch (Exception e) {
			log.error("error : " + e.getMessage());
			log.debug("not found buildContinuityClaim");
			logMonitor.responseLog("buildContinuityClaim", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] buildContinuityClaim : ", e);
		}
		return resultList;
	}

	public PlanDto getPlanDto(String planCode, String callName, com.mtl.eligible.model.enumerator.ClaimType claimType, Connection lof4claimConnection) {
		log.debug("--- get plan group type called ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement get plan group type called: " + e);
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" tb1.PLAN_CODE ");
		sql.append(" ,tb2.PLAN_GROUP_TYPE ");
		sql.append(" ,tb2.PLAN_GROUP_CODE ");
		sql.append(" ,tb1.PLAN_NAME ");
		sql.append(" ,TB1.CLASS_OF_BUSINESS ");
		sql.append(" ,TB1.SUPPLEMENTARY_BENEFIT ");
		sql.append(" ,TB1.TYPE_INSURANCE ");
		sql.append(" ,TB1.UW_GROUP_KEY ");
		sql.append(" ,TB1.NEW_UW_GROUP_KEY ");
		sql.append(" FROM CLM_PLAN_TB TB1 ");
		sql.append(" INNER JOIN CLM_PLAN_GROUP_TB TB2 ");
		sql.append(" ON TB1.PLAN_GROUP_CODE = TB2.PLAN_GROUP_CODE ");
		sql.append(" AND TB2.IS_DELETED = 0 ");
		sql.append(" WHERE TB1.PLAN_CODE = '").append(planCode).append("'");
		sql.append(" AND TB1.IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("getPlanDto", null, null, null, null, planCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("getPlanDto", null, null);
			while (rs.next()) {
				String planGroupCode = rs.getString("PLAN_GROUP_CODE");
				String planGroupType = rs.getString("PLAN_GROUP_TYPE");
				String planName = rs.getString("PLAN_NAME");
				// add by Ging requirement from P'M for map query 2016-03-03
				String classOfBusiness = rs.getString("CLASS_OF_BUSINESS");
				String supplementaryBenefit = rs.getString("SUPPLEMENTARY_BENEFIT");
				String typeInsurance = rs.getString("TYPE_INSURANCE");
				String uwGroupKey = rs.getString("UW_GROUP_KEY");
				String newUwGroupKey = rs.getString("NEW_UW_GROUP_KEY");

				planGroupCode = StringUtils.isNotBlank(planGroupCode) ? planGroupCode : "";
				planGroupType = StringUtils.isNotBlank(planGroupType) ? planGroupType : "";
				planName = StringUtils.isNotBlank(planName) ? planName : "";
				classOfBusiness = StringUtils.isNotBlank(classOfBusiness) ? classOfBusiness : "";
				supplementaryBenefit = StringUtils.isNotBlank(supplementaryBenefit) ? supplementaryBenefit : "";
				typeInsurance = StringUtils.isNotBlank(typeInsurance) ? typeInsurance : "";
				uwGroupKey = StringUtils.isNotBlank(uwGroupKey) ? uwGroupKey : "";
				newUwGroupKey = StringUtils.isNotBlank(newUwGroupKey) ? newUwGroupKey : "";

				PlanDto planDto = new PlanDto();
				planDto.setPlanGroupType(planGroupType);
				planDto.setPlanGroupCode(planGroupCode);
				planDto.setPlanName(planName);
				planDto.setClassOfBusiness(classOfBusiness);
				planDto.setSupplementaryBenefit(supplementaryBenefit);
				planDto.setTypeInsurance(typeInsurance);
				planDto.setUwGroupKey(uwGroupKey);
				planDto.setNewUwGroupKey(newUwGroupKey);

				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] getPlanDto : ", e);
				}
				return planDto;
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found getPlanDto");
			logMonitor.responseLog("getPlanDto", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] getPlanDto : ", e);
		}
		return null;
	}

	public void getCoveragesPlanCode(com.mtl.eligible.model.mtlclaim.Coverage coverage, Connection lof4claimConnection) {
		log.debug("--- getCoveragesPlanCode : " + coverage.getCoverageCode());
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement coveragesPlanCode called: " + e);
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT PG.PLAN_GROUP_TYPE, ");
		sql.append(" PB.EFFECTIVE_DATE, ");
		sql.append(" PB.PLAN_GROUP_CODE, ");
		sql.append(" PB.PLAN_NAME, ");
		sql.append(" PB.WAITING_DAYS ");
		sql.append(" FROM CLM_PLAN_TB PB ");
		sql.append(" INNER JOIN CLM_PLAN_GROUP_TB PG ON PB.PLAN_GROUP_CODE = PG.PLAN_GROUP_CODE ");
		sql.append(" AND PG.IS_DELETED = 0 ");
		sql.append(" WHERE PB.PLAN_CODE  = '").append(coverage.getCoverageCode()).append("'");
		sql.append(" AND PB.IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("getCoveragesPlanCode", null, null, null, null, coverage.getCoverageCode());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("getCoveragesPlanCode", null, null);
			while (rs.next()) {
				coverage.getMasterModel().setPlanGroupCode(rs.getString("PLAN_GROUP_TYPE"));
				coverage.getMasterModel().setEffectiveDate(stringToDate(rs.getString("EFFECTIVE_DATE")));
				coverage.getMasterModel().setCoverageGroupCode(rs.getString("PLAN_GROUP_CODE"));
				coverage.getMasterModel().setPlanName(rs.getString("PLAN_NAME"));
				coverage.getMasterModel().setWaitingDays(rs.getInt("WAITING_DAYS"));
				break;
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found getCoveragesPlanCode");
			logMonitor.responseLog("getCoveragesPlanCode", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception ex) {
				log.error("[Error Closed statment] getCoveragesPlanCode called : ", ex);
				return;
			}
			return;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] getCoveragesPlanCode called : ", e);
			return;
		}
	}

	// ging 29-03-2016 add parameter diagnosisList
	private List<com.mtl.eligible.model.mtlclaim.Coverage> updateCoverages(String policyHistoryID,
			List<com.mtl.eligible.model.mtlclaim.Coverage> as400CoverageList, String callName, com.mtl.eligible.model.enumerator.ClaimType claimType,
			final List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosisList, Connection lof4claimConnection, Connection lof4posConnection) {
		log.debug("--- update coverages called ---");

		wsClientService.init();
		String collectorType = wsClientService.getCollectorType();

		Statement lof4claimStatement = null;
		Statement lof4posStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
			lof4posStatement = lof4posConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4ClaimStatement update coverages called: " + e);
		}
		StringBuilder sql = new StringBuilder();
		if (StringUtils.isNotBlank(policyHistoryID)) {
			sql.append(" SELECT TB1.COVERAGE_HISTORY_ID, ");
			sql.append(" TB1.PLAN_CODE, ");
			sql.append(" TB1.STATUS_CODE, ");
			sql.append(" TB1.ISSUE_DATE, ");
			sql.append(" TB1.TRAILER_NUMBER, ");
			sql.append(" TB1.FACE_AMOUNT, ");
			sql.append(" TB1.SUM_INSURED, ");
			sql.append(" TB1.STATUS_SUB_CODE, ");
			sql.append(" TB1.MATURITY_DATE, ");
			sql.append(" ( Select Sum(Life_Annual_Premium) From ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "")
					.append("CLM_HIS_COVERAGE_TR ");
			sql.append(" Where Policy_History_Id = '").append(policyHistoryID).append("' ) as LIFE_ANNUAL_PREMIUM ");
			sql.append(" FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_COVERAGE_TR TB1 ");
			sql.append(" WHERE TB1.POLICY_HISTORY_ID  = ").append(policyHistoryID);

			log.debug("SQL Code: " + sql.toString());
			List<com.mtl.eligible.model.mtlclaim.Coverage> list = new ArrayList<com.mtl.eligible.model.mtlclaim.Coverage>();
			try {
				logMonitor.requestLog("updateCoverages", policyHistoryID, null, null, null, null);
				ResultSet rs = lof4posStatement.executeQuery(sql.toString());
//				ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
				logMonitor.responseLog("updateCoverages", null, null);
				while (rs.next()) {
					com.mtl.eligible.model.mtlclaim.Coverage rec = new com.mtl.eligible.model.mtlclaim.Coverage();
					com.mtl.eligible.model.mtlclaim.master.CoverageMs coverageMs = new com.mtl.eligible.model.mtlclaim.master.CoverageMs(
							rs.getString("COVERAGE_HISTORY_ID"), rs.getString("PLAN_CODE"), rs.getString("STATUS_CODE"),
							stringToDate(rs.getString("ISSUE_DATE")), null, null, rs.getString("TRAILER_NUMBER"), rs.getDouble("FACE_AMOUNT"), null,
							rs.getString("STATUS_SUB_CODE"), null, stringToDate(rs.getString("MATURITY_DATE")), rs.getDouble("LIFE_ANNUAL_PREMIUM"));
					coverageMs.setSumInsured(new BigDecimal(rs.getString("SUM_INSURED")));
					rec.setCauseDiagnosis(getDiagnosis(diagnosisList, lof4claimConnection));// ging
																							// 29-03-2016
					rec.setMasterModel(coverageMs);
					getCoveragesPlanCode(rec, lof4claimConnection);
					list.add(rec);
				}
			}
			catch (Exception e) {
				log.error("error : ", e);
				log.debug("not found updateCoverages");
				logMonitor.responseLog("updateCoverages", null, e.getMessage());
			}
			log.debug("listCoverage.size() - " + list.size());
			boolean isChange = false;
			double lifeAnnualPremium = 0.0d;
			Date tempReinDate = null;
			// check ReinDate by kuay
			for (com.mtl.eligible.model.mtlclaim.Coverage temp400Coverage : as400CoverageList) { // LOF
																									// -
																									// change
																									// from
																									// coverage
																									// to
																									// lofCoverage
				if (temp400Coverage.getTrailerNumber().equals("01") && temp400Coverage.getMasterModel().getReInStatementDate() != null)
					tempReinDate = temp400Coverage.getMasterModel().getReInStatementDate();
			}
			log.debug("tempReinDate:" + tempReinDate);
			for (com.mtl.eligible.model.mtlclaim.Coverage lofCoverage : list) { // LOF
																				// -
																				// change
																				// from
																				// coverage
																				// to
																				// lofCoverage
				for (com.mtl.eligible.model.mtlclaim.Coverage as400Coverage : as400CoverageList) { // 400
																									// -
																									// change
																									// from
																									// request
																									// to
																									// as400Coverage
					// DEBUG 6/8/2015 FILM
					if (lofCoverage.getTrailerNumber().equals(as400Coverage.getTrailerNumber())) {
						as400Coverage.getMasterModel().setIssuedDate(lofCoverage.getIssuedDate());

						as400Coverage.getMasterModel().setMaturityDate(lofCoverage.getMaturityDate());
						as400Coverage.getMasterModel().setCoverageStatus(lofCoverage.getCoverageStatus());
						as400Coverage.setTempCoverageStatus(lofCoverage.getCoverageStatus());
						as400Coverage.getMasterModel().setFaceAmount(lofCoverage.getFaceAmount());
						as400Coverage.getMasterModel().setTrailerNumber(lofCoverage.getTrailerNumber());
						as400Coverage.getMasterModel().setCoverageHistoryID(lofCoverage.getCoverageHistoryID());
						as400Coverage.getMasterModel().setCoverageGroupCode(lofCoverage.getCoverageGroupCode());
						as400Coverage.getMasterModel().setPlanName(lofCoverage.getPlanName());
						as400Coverage.getMasterModel().setPlanGroupCode(lofCoverage.getPlanGroupCode());
						as400Coverage.getMasterModel().setSumInsured(lofCoverage.getMasterModel().getSumInsured());
						as400Coverage.getMasterModel().setLifeAnnualPremium(lofCoverage.getMasterModel().getLifeAnnualPremium());
						as400Coverage.getMasterModel().setWaitingDays(lofCoverage.getWaitingDays());
						as400Coverage.setCauseDiagnosis(lofCoverage.getCauseDiagnosis());
						as400Coverage.getMasterModel().setCoverageStatusSubCode(lofCoverage.getCoverageStatusSubCode());// new
																														// 3-6-2016
						if (!isChange)
							lifeAnnualPremium = lofCoverage.getMasterModel().getLifeAnnualPremium();
						if (StringUtils.isNotBlank(lofCoverage.getCoverageCode()))
							as400Coverage.getMasterModel().setCode(lofCoverage.getCoverageCode());
						isChange = true;
					}
				}
			}

			if (isChange) {
				for (com.mtl.eligible.model.mtlclaim.Coverage as400Coverage : as400CoverageList)
					as400Coverage.getMasterModel().setLifeAnnualPremium(lifeAnnualPremium);
			}
			else {
				for (com.mtl.eligible.model.mtlclaim.Coverage as400Coverage : as400CoverageList)
					lifeAnnualPremium += as400Coverage.getMasterModel().getLifeAnnualPremium();
				for (com.mtl.eligible.model.mtlclaim.Coverage as400Coverage : as400CoverageList)
					as400Coverage.getMasterModel().setLifeAnnualPremium(lifeAnnualPremium);
			}
		}

		log.debug("as400CoverageList.size() - " + as400CoverageList.size());

		for (com.mtl.eligible.model.mtlclaim.Coverage as400Coverage : as400CoverageList) {
			if (StringUtils.isBlank(as400Coverage.getCoverageStatus()))
				as400Coverage.getMasterModel().setCoverageStatus("");
			if (StringUtils.isBlank(as400Coverage.getMasterModel().getCoverageHistoryID())) {
				log.debug("getCoverageCurrentStatusCode as400Coverage : " + as400Coverage.getMasterModel().getCoverageCurrentStatusCode());
				as400Coverage.setTempCoverageStatus(as400Coverage.getMasterModel().getCoverageCurrentStatusCode());
			}
			if (StringUtils.isBlank(as400Coverage.getCoverageStatusSubCode())) {
				as400Coverage.getMasterModel().setCoverageStatusSubCode("");
				log.debug("CoverageStatusSubCode : " + as400Coverage.getMasterModel().getCoverageStatusSubCode());
			}
			log.debug("CoverageCode :" + as400Coverage.getCoverageCode());
			log.debug("TempCoverageStatus Final:" + as400Coverage.getTempCoverageStatus());
			log.debug("CoverageStatusSubCode Final: " + as400Coverage.getCoverageStatusSubCode());

			if (StringUtils.isBlank(as400Coverage.getPlanGroupCode()) || StringUtils.isBlank(as400Coverage.getCoverageGroupCode())
					|| StringUtils.isBlank(as400Coverage.getClassOfBusiness()) || StringUtils.isBlank(as400Coverage.getSupplementaryBenefit())
					|| StringUtils.isBlank(as400Coverage.getTypeInsurance()) || StringUtils.isBlank(as400Coverage.getUwGroupKey())
					|| StringUtils.isBlank(as400Coverage.getNewUwGroupKey())) {
				PlanDto planDto = getPlanDto(as400Coverage.getCoverageCode(), callName, claimType, lof4claimConnection);
				if (planDto != null) {
					if (StringUtils.isBlank(as400Coverage.getPlanGroupCode()))
						as400Coverage.getMasterModel().setPlanGroupCode(planDto.getPlanGroupType());
					if (StringUtils.isBlank(as400Coverage.getCoverageGroupCode()))
						as400Coverage.getMasterModel().setCoverageGroupCode(planDto.getPlanGroupCode());
					if (StringUtils.isBlank(as400Coverage.getPlanName()))
						as400Coverage.getMasterModel().setPlanName(planDto.getPlanName());
					if (StringUtils.isBlank(as400Coverage.getClassOfBusiness()))
						as400Coverage.getMasterModel().setClassOfBusiness(planDto.getClassOfBusiness());
					if (StringUtils.isBlank(as400Coverage.getSupplementaryBenefit()))
						as400Coverage.getMasterModel().setSupplementaryBenefit(planDto.getSupplementaryBenefit());
					if (StringUtils.isBlank(as400Coverage.getUwGroupKey()))
						as400Coverage.getMasterModel().setUwGroupKey(planDto.getUwGroupKey());
					if (StringUtils.isBlank(as400Coverage.getNewUwGroupKey()))
						as400Coverage.getMasterModel().setNewUwGroupKey(planDto.getNewUwGroupKey());
					if (StringUtils.isBlank(as400Coverage.getTypeInsurance()))
						as400Coverage.getMasterModel().setTypeInsurance(planDto.getTypeInsurance());
				}
				else
					as400Coverage.getCoverage().setWaitingDays(0);
			}
		}
		try {
			lof4claimStatement.close();
			lof4posStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] update coverages called : ", e);
		}
		return as400CoverageList;
	}

	public com.mtl.eligible.model.mtlclaim.Policy updatePolicy(Date requestDate, final List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosis,
			com.mtl.eligible.model.mtlclaim.Policy policy, String callName, com.mtl.eligible.model.enumerator.ClaimType claimType,
			Connection lof4claimConnection, Connection lof4posConnection) {
		log.debug("--- update policy called ---");
		Statement lof4claimStatement = null;
		Statement lof4posStatement = null;

		wsClientService.init();
		String collectorType = wsClientService.getCollectorType();

		try {
			lof4claimStatement = lof4claimConnection.createStatement();
			lof4posStatement = lof4posConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement update policy called : ", e);
		}

		StringBuilder sql = new StringBuilder();

//		sql.append("SELECT TMP_MAIN.POLICY_HISTORY_ID ");
//		sql.append(" ,TMP_MAIN.POLICY_NUMBER ");
//		sql.append(" ,STATUS_CODE ");
//		sql.append(" ,TMP_SUB.PREVIOUS_STATUS_CODE ");
//		sql.append(" ,BASIC_PLAN_CODE ");
//		sql.append(" ,ISSUE_DATE ");
//		sql.append(" ,TMP_SUB2.EXHIBIT_REIN_DATE ");
//		sql.append(" ,PAID_TO_DATE ");
//		sql.append(" ,BILLED_TO_DATE ");
//		sql.append(" ,MODAL_PREMIUM ");
//		sql.append(" ,MODE_OF_PREMIUM ");
//		sql.append(" ,POLICY_STATUS_SUB_CODE ");
//		sql.append(" FROM ");
//		sql.append(" ( ");
//		sql.append(" SELECT ");
//		sql.append(" PO_HIS1.UPDATED_DATE ");
//		sql.append(" ,PO_HIS1.POLICY_NUMBER ");
//		sql.append(" ,PO_HIS1.POLICY_HISTORY_ID ");
//		sql.append(" ,PO_HIS1.STATUS_CODE ");
//		sql.append(" ,PO_HIS1.BASIC_PLAN_CODE ");
//		sql.append(" ,PO_HIS1.ISSUE_DATE ");
//		sql.append(" ,CO_HIS1.TRAILER_NUMBER ");
//		sql.append(" ,PO_HIS1.PAID_TO_DATE ");
//		sql.append(" ,PO_HIS1.BILLED_TO_DATE ");
//		sql.append(" ,PO_HIS1.POLICY_STATUS_SUB_CODE ");
//		sql.append(" ,PO_HIS1.MODE_OF_PREMIUM ");
//		sql.append(" ,CASE ");
//		sql.append(" WHEN CO_HIS1.TRAILER_NUMBER      = '01' ");
//		sql.append(" AND CO_HIS1.LIFE_ANNUAL_PREMIUM IS NOT NULL ");
//		sql.append(" THEN CO_HIS1.LIFE_ANNUAL_PREMIUM ");
//		sql.append(" ELSE NULL ");
//		sql.append(" END AS MODAL_PREMIUM ");
//		sql.append(" ,ROW_NUMBER() OVER(PARTITION BY PO_HIS1.POLICY_NUMBER ORDER BY PO_HIS1.UPDATED_DATE DESC,CO_HIS1.TRAILER_NUMBER) MAXROW ");
//		sql.append(" FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_POLICY_TR PO_HIS1 ");
//		sql.append(" INNER JOIN ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_COVERAGE_TR CO_HIS1 ");
//		sql.append(" ON PO_HIS1.POLICY_HISTORY_ID = CO_HIS1.POLICY_HISTORY_ID ");
//		sql.append(" WHERE PO_HIS1.POLICY_NUMBER  = '" + policy.getPolicyNumber() + "' ");
//		sql.append(" AND PO_HIS1.UPDATED_DATE <= TO_DATE('" + dateToString(requestDate) + "', 'DD-MM-YYYY') ");
//		sql.append(" ) TMP_MAIN ");
//		sql.append(" LEFT JOIN ");
//		sql.append(" ( ");
//		sql.append(" SELECT CO_HIS2.PREVIOUS_STATUS_CODE, PO_HIS2.POLICY_HISTORY_ID ");
//		sql.append(" FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_COVERAGE_TR CO_HIS2 ");
//		sql.append(" INNER JOIN ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_POLICY_TR PO_HIS2 ");
//		sql.append(" ON PO_HIS2.POLICY_HISTORY_ID = CO_HIS2.POLICY_HISTORY_ID ");
//		sql.append(" AND PO_HIS2.POLICY_NUMBER  =  '" + policy.getPolicyNumber() + "' ");
//		sql.append(" WHERE CO_HIS2.TRAILER_NUMBER = '01' ");
//		sql.append(" AND CO_HIS2.EXHIBIT_REIN_DATE IS NOT NULL ");
//		sql.append(" ORDER BY PO_HIS2.UPDATED_DATE DESC) TMP_SUB ");
//		sql.append(" ON TMP_SUB.POLICY_HISTORY_ID = TMP_MAIN.POLICY_HISTORY_ID ");
//		sql.append(" LEFT JOIN ");
//		sql.append(" ( ");
//		sql.append(" SELECT EXHIBIT_REIN_DATE,  POLICY_NUMBER ");
//		sql.append(" FROM ");
//		sql.append(" ( ");
//		sql.append(" SELECT ");
//		sql.append(" CO_HIS3.PREVIOUS_STATUS_CODE ");
//		sql.append(" ,PO_HIS3.POLICY_HISTORY_ID ");
//		sql.append(" ,CO_HIS3.EXHIBIT_REIN_DATE ");
//		sql.append(" ,PO_HIS3.POLICY_NUMBER ");
//		sql.append(" ,ROW_NUMBER() OVER(PARTITION BY PO_HIS3.POLICY_NUMBER ORDER BY PO_HIS3.UPDATED_DATE DESC) FETCHFIRSTROW ");
//		sql.append(" FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_COVERAGE_TR CO_HIS3 ");
//		sql.append(" INNER JOIN ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_POLICY_TR PO_HIS3 ");
//		sql.append(" ON PO_HIS3.POLICY_HISTORY_ID = CO_HIS3.POLICY_HISTORY_ID ");
//		sql.append(" AND PO_HIS3.POLICY_NUMBER  =  '" + policy.getPolicyNumber() + "' ");
//		sql.append(" WHERE CO_HIS3.TRAILER_NUMBER = '01' ");
//		sql.append(" AND CO_HIS3.EXHIBIT_REIN_DATE IS NOT NULL ");
//		sql.append(" ORDER BY PO_HIS3.UPDATED_DATE DESC) ");
//		sql.append(" WHERE FETCHFIRSTROW                  = 1) ");
//		sql.append(" TMP_SUB2 ON TMP_SUB2.POLICY_NUMBER = TMP_MAIN.POLICY_NUMBER ");
//		sql.append(" WHERE MAXROW = 1 ");
//		sql.append(" ORDER BY ISSUE_DATE , ");
//		sql.append(" TMP_MAIN.POLICY_NUMBER ");
		
		sql.append(" WITH ");
		sql.append(" TEMP_FIRST  AS ");
		sql.append(" ( ");
		sql.append(" SELECT ");
		sql.append(" DPO_POLICY_NUMBER AS POLICY_NUMBER, ");
		sql.append(" MIN(DPO_AS_OF_DATE) AS FIRST_RECORD, ");
		sql.append(" TO_DATE('").append(dateToString(requestDate)).append("', 'DD-MM-YYYY') AS CLAIM_DATE  ");
		sql.append(" FROM ");
		sql.append(" LPOS.DAT_POLICY  ");
		sql.append(" WHERE ");
		sql.append(" DPO_POLICY_NUMBER = '").append(policy.getPolicyNumber()).append("'  ");
		sql.append(" GROUP BY ");
		sql.append(" DPO_POLICY_NUMBER  ");
		sql.append(" ) ");
		sql.append(" , ");
		sql.append(" PO_HIS AS ");
		sql.append(" ( ");
		sql.append(" SELECT ");
		sql.append(" ROW_NUMBER() OVER(PARTITION BY HP.DPO_POLICY_NUMBER  ");
		sql.append(" ORDER BY ");
		sql.append(" HP.DPO_AS_OF_DATE DESC) AS ROWNUMBER, ");
		sql.append(" CASE  ");
		sql.append(" WHEN HP.DPO_PA_RENEW_DATE IS NOT NULL AND ");
		sql.append(" TO_CHAR(HP.DPO_PA_RENEW_DATE,'yyyyMMdd') <> '30000101'  ");
		sql.append(" THEN HP.DPO_PA_RENEW_DATE  ");
		sql.append(" ELSE HP.DPO_ISSUE_DATE  ");
		sql.append(" END FIX_ISSUE, ");
		sql.append(" HP.DPO_ID                                                    AS   POLICY_HISTORY_ID, ");
		sql.append(" HP.DPO_AS_OF_DATE                                  AS  UPDATED_DATE, ");
		sql.append(" HP.DPO_POLICY_NUMBER                          AS   POLICY_NUMBER, ");
		sql.append(" HP.DPO_STATUS_CODE                              AS   STATUS_CODE, ");
		sql.append(" HP.DPO_STATUS_SUB_CODE                          AS  POLICY_STATUS_SUB_CODE, ");
		sql.append(" HP.DPO_MODX                                                  AS  MODE_OF_PREMIUM, ");
		sql.append(" HP.DPO_MODE_PREMIUM                                AS  MODAL_PREMIUM, ");
		sql.append(" HP.DPO_BILLED_TO_DATE                               AS  BILLED_TO_DATE, ");
		sql.append(" HP.DPO_PAID_TO_DATE                                   AS  PAID_TO_DATE, ");
		sql.append(" HP.DPO_BASIC_PLAN_CODE                           AS  BASIC_PLAN_CODE, ");
		sql.append(" HP.DPO_OLD_STATUS_CODE                          AS  PREVIOUS_STATUS_CODE, ");
		sql.append(" T.CLAIM_DATE, ");
		sql.append(" HC.DCO_TRAILER_NUMBER                             AS  TRAILER_NUMBER, ");
		sql.append(" HC.DCO_LIFE_ANNUAL_PREMIUM                   AS CO_MODAL_PREMIUM, ");
		sql.append(" CASE  ");
		sql.append(" WHEN HC.DCO_EXHIBIT_REINSTATED_DATE IS NOT NULL ");
		sql.append(" THEN HC.DCO_OLD_STATUS_CODE  ");
		sql.append(" ELSE NULL  ");
		sql.append(" END CO_PREVIOUS_STATUS_CODE, ");
		sql.append(" HC.DCO_EXHIBIT_REINSTATED_DATE                                AS EXHIBIT_REIN_DATE  ");
		sql.append(" FROM ");
		sql.append(" TEMP_FIRST T  ");
		sql.append(" INNER JOIN LPOS.DAT_POLICY HP  ");
		sql.append(" ON (T.POLICY_NUMBER = HP.DPO_POLICY_NUMBER AND ");
		sql.append(" HP.DPO_AS_OF_DATE <= ( ");
		sql.append(" CASE  ");
		sql.append(" WHEN T.FIRST_RECORD > T.CLAIM_DATE  ");
		sql.append(" THEN T.FIRST_RECORD  ");
		sql.append(" ELSE T.CLAIM_DATE  ");
		sql.append(" END) )  ");
		sql.append(" LEFT JOIN LPOS.DAT_COVERAGE HC  ");
		sql.append(" ON (HP.DPO_ID = HC.DPO_ID AND ");
		sql.append(" HC.DPO_ID = '01')  ");
		sql.append(" ) ");
		sql.append(" , ");
		sql.append(" PO_REIN     AS ");
		sql.append(" (	SELECT ");
		sql.append(" POLICY_NUMBER, ");
		sql.append(" EXHIBIT_REIN_DATE  ");
		sql.append(" FROM ");
		sql.append(" PO_HIS  ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM = 1 AND ");
		sql.append(" EXHIBIT_REIN_DATE IS NOT NULL  ");
		sql.append(" ) ");
		sql.append(" , ");
		sql.append(" PO_RENEW    AS ");
		sql.append(" (	SELECT ");
		sql.append(" CASE  ");
		sql.append(" WHEN PH.FIX_ISSUE > PH.UPDATED_DATE AND ");
		sql.append(" PH.FIX_ISSUE > PH.CLAIM_DATE  ");
		sql.append(" THEN 1  ");
		sql.append(" ELSE 0  ");
		sql.append(" END BEFORE_RENEW, ");
		sql.append(" PH.*  ");
		sql.append(" FROM ");
		sql.append(" PO_HIS PH  ");
		sql.append(" ) ");
		sql.append(" , ");
		sql.append(" PO_DATA     AS ");
		sql.append(" (	SELECT ");
		sql.append(" ROWNUM AS ROW_YAER, ");
		sql.append(" PR.*  ");
		sql.append(" FROM ");
		sql.append(" PO_RENEW PR ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= 2 AND ");
		sql.append(" ( PR.ROWNUMBER = 1 OR ");
		sql.append(" (PR.ROWNUMBER > 1 AND ");
		sql.append(" PR.BEFORE_RENEW = 1 AND ");
		sql.append(" PR.FIX_ISSUE <> (	SELECT ");
		sql.append(" PRT.FIX_ISSUE  ");
		sql.append(" FROM ");
		sql.append(" PO_RENEW PRT  ");
		sql.append(" WHERE ");
		sql.append(" PRT.ROWNUMBER = 1 ");
		sql.append(" ) ");
		sql.append(" ) OR ");
		sql.append(" (PR.ROWNUMBER > 1 AND ");
		sql.append(" PR.BEFORE_RENEW = 0) )  ");
		sql.append(" ) ");
		sql.append(" , ");
		sql.append(" PO_DATA_ROW AS ");
		sql.append(" (	SELECT ");
		sql.append(" CASE  ");
		sql.append(" WHEN (	SELECT ");
		sql.append(" COUNT(*) AS ROW_DATA  ");
		sql.append(" FROM ");
		sql.append(" PO_DATA  ");
		sql.append(" ) ");
		sql.append(" = 1  ");
		sql.append(" THEN 1  ");
		sql.append(" ELSE ( ");
		sql.append(" CASE  ");
		sql.append(" WHEN PDT.BEFORE_RENEW = 0  ");
		sql.append(" THEN 1  ");
		sql.append(" ELSE 2  ");
		sql.append(" END) ");
		sql.append(" END YEAR_ROW  ");
		sql.append(" FROM ");
		sql.append(" PO_RENEW PDT  ");
		sql.append(" WHERE ");
		sql.append(" PDT.ROWNUMBER = 1  ");
		sql.append(" ) ");
		sql.append(" SELECT ");
		sql.append(" PD.POLICY_HISTORY_ID, ");
		sql.append(" PD.POLICY_NUMBER, ");
		sql.append(" PD.STATUS_CODE, ");
		sql.append(" PD.PREVIOUS_STATUS_CODE, ");
		sql.append(" PD.BASIC_PLAN_CODE, ");
		sql.append(" PD.FIX_ISSUE AS ISSUE_DATE, ");
		sql.append(" RE.EXHIBIT_REIN_DATE, ");
		sql.append(" PD.PAID_TO_DATE, ");
		sql.append(" PD.BILLED_TO_DATE, ");
		sql.append(" PD.MODAL_PREMIUM, ");
		sql.append(" PD.MODE_OF_PREMIUM, ");
		sql.append(" PD.POLICY_STATUS_SUB_CODE  ");
		sql.append(" FROM ");
		sql.append(" PO_DATA PD  ");
		sql.append(" LEFT JOIN PO_REIN RE  ");
		sql.append(" ON (PD.POLICY_NUMBER = RE.POLICY_NUMBER)  ");
		sql.append(" WHERE ");
		sql.append(" PD.ROW_YAER = (	SELECT ");
		sql.append(" YEAR_ROW  ");
		sql.append(" FROM ");
		sql.append(" PO_DATA_ROW ");
		sql.append(" ) ");
//		sql.append(" ; ");

		log.debug("SQL Code: " + sql.toString());
		final Date finalRequestedDate = requestDate;
		com.mtl.eligible.model.mtlclaim.Policy policyResponse = null;

		try {
			logMonitor.requestLog("updatePolicy", policy.getPolicyNumber(), null, null, null, dateToString(requestDate));
			ResultSet rs = lof4posStatement.executeQuery(sql.toString());
//			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("updatePolicy", null, null);

			while (rs.next()) {
				policyResponse = new com.mtl.eligible.model.mtlclaim.Policy(new com.mtl.eligible.model.mtlclaim.master.PolicyMs(
						rs.getString("POLICY_HISTORY_ID"), rs.getString("POLICY_NUMBER"), rs.getString("STATUS_CODE"),
						rs.getString("BASIC_PLAN_CODE"), rs.getDouble("MODAL_PREMIUM"), rs.getInt("MODE_OF_PREMIUM"),
						stringToDate(rs.getString("ISSUE_DATE")), stringToDate(rs.getString("EXHIBIT_REIN_DATE")),
						stringToDate(rs.getString("BILLED_TO_DATE")), stringToDate(rs.getString("PAID_TO_DATE")),
						((rs.getString("POLICY_STATUS_SUB_CODE") != null) ? rs.getString("POLICY_STATUS_SUB_CODE") : "")), null);
				policyResponse.getMasterModel().setLifeAnnualPremium(rs.getDouble("MODAL_PREMIUM"));
				policyResponse.getMasterModel().setOldStatusCode(
						rs.getString("PREVIOUS_STATUS_CODE") != null ? rs.getString("PREVIOUS_STATUS_CODE") : "");
				log.debug("b4 policyHistory : " + policyResponse.getMasterModel().getPolicyHistoryID());
				log.debug("OldStatusCode: " + policyResponse.getMasterModel().getOldStatusCode());
			}

			if (policyResponse == null) {
				sql = new StringBuilder();
				sql.append("SELECT TMP_MAIN.POLICY_HISTORY_ID ");
				sql.append(" ,TMP_MAIN.POLICY_NUMBER ");
				sql.append(" ,STATUS_CODE ");
				sql.append(" ,TMP_SUB.PREVIOUS_STATUS_CODE ");
				sql.append(" ,BASIC_PLAN_CODE ");
				sql.append(" ,ISSUE_DATE ");
				sql.append(" ,PAID_TO_DATE ");
				sql.append(" ,BILLED_TO_DATE ");
				sql.append(" ,MODAL_PREMIUM ");
				sql.append(" ,MODE_OF_PREMIUM ");
				sql.append(" ,POLICY_STATUS_SUB_CODE ");
				sql.append(" FROM ");
				sql.append(" ( ");
				sql.append(" SELECT ");
				sql.append(" PO_HIS1.UPDATED_DATE ");
				sql.append(" ,PO_HIS1.POLICY_NUMBER ");
				sql.append(" ,PO_HIS1.POLICY_HISTORY_ID ");
				sql.append(" ,PO_HIS1.STATUS_CODE ");
				sql.append(" ,PO_HIS1.BASIC_PLAN_CODE ");
				sql.append(" ,PO_HIS1.ISSUE_DATE ");
				sql.append(" ,CO_HIS1.TRAILER_NUMBER ");
				sql.append(" ,PO_HIS1.PAID_TO_DATE ");
				sql.append(" ,PO_HIS1.BILLED_TO_DATE ");
				sql.append(" ,PO_HIS1.POLICY_STATUS_SUB_CODE ");
				sql.append(" ,PO_HIS1.MODE_OF_PREMIUM ");
				sql.append(" ,CASE ");
				sql.append(" WHEN CO_HIS1.TRAILER_NUMBER      = '01' ");
				sql.append(" AND CO_HIS1.LIFE_ANNUAL_PREMIUM IS NOT NULL ");
				sql.append(" THEN CO_HIS1.LIFE_ANNUAL_PREMIUM ");
				sql.append(" ELSE NULL ");
				sql.append(" END AS MODAL_PREMIUM ");
				sql.append(" ,ROW_NUMBER() OVER(PARTITION BY PO_HIS1.POLICY_NUMBER ORDER BY PO_HIS1.UPDATED_DATE ,CO_HIS1.TRAILER_NUMBER) MAXROW ");
				sql.append(" FROM ").append(schema).append("CLM_HIS_POLICY_TR PO_HIS1 ");
				sql.append(" INNER JOIN ").append(schema).append("CLM_HIS_COVERAGE_TR CO_HIS1 ");
				sql.append(" ON PO_HIS1.POLICY_HISTORY_ID = CO_HIS1.POLICY_HISTORY_ID ");
				sql.append(" WHERE PO_HIS1.POLICY_NUMBER  = '" + policy.getPolicyNumber() + "' ");
				sql.append(" ) TMP_MAIN ");
				sql.append(" LEFT JOIN ");
				sql.append(" ( ");
				sql.append(" SELECT CO_HIS2.PREVIOUS_STATUS_CODE, PO_HIS2.POLICY_HISTORY_ID ");
				sql.append(" FROM ").append(schema).append("CLM_HIS_COVERAGE_TR CO_HIS2 ");
				sql.append(" INNER JOIN ").append(schema).append("CLM_HIS_POLICY_TR PO_HIS2 ");
				sql.append(" ON PO_HIS2.POLICY_HISTORY_ID = CO_HIS2.POLICY_HISTORY_ID ");
				sql.append(" AND PO_HIS2.POLICY_NUMBER  =  '" + policy.getPolicyNumber() + "' ");
				sql.append(" WHERE CO_HIS2.TRAILER_NUMBER = '01' ");
				sql.append(" AND CO_HIS2.EXHIBIT_REIN_DATE IS NOT NULL ");
				sql.append(" ORDER BY PO_HIS2.UPDATED_DATE DESC) TMP_SUB ");
				sql.append(" ON TMP_SUB.POLICY_HISTORY_ID = TMP_MAIN.POLICY_HISTORY_ID ");
				sql.append(" WHERE MAXROW = 1 ");
				sql.append(" ORDER BY ISSUE_DATE , ");
				sql.append(" TMP_MAIN.POLICY_NUMBER ");

				//ResultSet rs2 = lof4posStatement.executeQuery(sql.toString());
				ResultSet rs2 = lof4claimStatement.executeQuery(sql.toString());

				while (rs2.next()) {
					policyResponse = new com.mtl.eligible.model.mtlclaim.Policy(new com.mtl.eligible.model.mtlclaim.master.PolicyMs(
							rs2.getString("POLICY_HISTORY_ID"), rs2.getString("POLICY_NUMBER"), rs2.getString("STATUS_CODE"),
							rs2.getString("BASIC_PLAN_CODE"), rs2.getDouble("MODAL_PREMIUM"), rs2.getInt("MODE_OF_PREMIUM"),
							stringToDate(rs2.getString("ISSUE_DATE")), null, stringToDate(rs2.getString("BILLED_TO_DATE")),
							stringToDate(rs2.getString("PAID_TO_DATE")),
							((rs2.getString("POLICY_STATUS_SUB_CODE") != null) ? rs2.getString("POLICY_STATUS_SUB_CODE") : "")), null);
					policyResponse.getMasterModel().setLifeAnnualPremium(rs2.getDouble("MODAL_PREMIUM"));
					policyResponse.getMasterModel().setOldStatusCode(
							rs2.getString("PREVIOUS_STATUS_CODE") != null ? rs2.getString("PREVIOUS_STATUS_CODE") : "");
					log.debug("b4 policyHistory : " + policyResponse.getMasterModel().getPolicyHistoryID());
					log.debug("OldStatusCode: " + policyResponse.getMasterModel().getOldStatusCode());
				}
			}
		}
		catch (Exception e) {
			log.debug("no data at LOF4CLM");
			log.error("Error : ", e);
			logMonitor.responseLog("updatePolicy", null, e.getMessage());
		}

		String policyHistory = "";
		if (policyResponse != null && StringUtils.isNotBlank(policyResponse.getPolicyHistoryID()))
			policyHistory = policyResponse.getPolicyHistoryID();

		policy.getMasterModel().setPolicyHistoryID(policyHistory);
		log.debug("b4 policyHistory : " + policyHistory);
		log.debug("b4 policy.getCoverages().size() - " + policy.getCoverages().size());
		policy.setCoverages(updateCoverages(policyHistory, policy.getCoverages(), callName, claimType, diagnosis, lof4claimConnection, lof4posConnection));
		if (policyResponse != null) {
			if (StringUtils.isNotBlank(policyResponse.getPolicyHistoryID())) {
				if (policy.getCoverages() != null && policy.getCoverages().size() > 0) {
					for (com.mtl.eligible.model.mtlclaim.Coverage coverage : policy.getCoverages()) {
						if (coverage.getCoverageGroupCode() != null && "ROP".equalsIgnoreCase(coverage.getCoverageGroupCode()) || "ROR".equalsIgnoreCase(coverage.getCoverageGroupCode())) {
//							log.debug("checkPossibleROP.getCoverageHistoryID >> : " + coverage.getCoverageHistoryID());
//							log.debug("checkPossibleROP.getCoverageCode >> : " + coverage.getCoverageCode());
//							log.debug("checkPossibleROP.getPolicyNumber >> : " + policy.getPolicyNumber());
//							log.debug("checkPossibleROP.finalRequestedDate >> : " + finalRequestedDate);
							int statusCode = checkPossibleROP(coverage.getCoverageHistoryID(), coverage.getCoverageCode(), policy.getPolicyNumber(),
									finalRequestedDate, diagnosis, lof4claimStatement);
							log.debug("checkPossibleROP : " + statusCode);
							// 0 normal
							// 1 1 times per day
							// 2 7 times per diagnosis
							// 3 30 times per year
							// 4 unknown
							coverage.setSpecialStatus(statusCode);
							log.debug("SpecialStatus : " + coverage.getSpecialStatus());
							// log.debug("coverage code : >>" +
							// coverage.getCoverageCode() + "<<");
							// log.debug("status   code : >>" + statusCode +
							// "<<");
							if (statusCode == 0) {

							}
							else {
								coverage.deactivate();
								coverage.setModelStatus('E');
							}
						}
						if (coverage.getCoverageGroupCode() != null && coverage.getCoverageGroupCode().equalsIgnoreCase("RTT")) {
							int statusCode = checkPossibleRTT(coverage.getCoverageCode(), policy.getPolicyNumber(), finalRequestedDate,
									lof4claimStatement);
							log.debug("checkPossibleRTT : " + statusCode);
							// 0 normal
							// 5 6 times per day
							coverage.setSpecialStatus(statusCode);
							log.debug("SpecialStatus : " + coverage.getSpecialStatus());
							if (statusCode != 0)
								coverage.setModelStatus('C');
						}
					}
				}
			}
			policy.getMasterModel().setBilledToDate(policyResponse.getBilledToDate());
			policy.getMasterModel().setPaidToDate(policyResponse.getPaidToDate());
			policy.getMasterModel().setIssuedDate(policyResponse.getIssuedDate());
			policy.getMasterModel().setPolicyStatus(policyResponse.getPolicyStatus());
			policy.setTempPolicyStatus(policyResponse.getPolicyStatus());
			policy.setTempPolicySubCodeStatus(policyResponse.getPolicyStatusSubCode());
			policy.getMasterModel().setPolicyStatusSubCode(policyResponse.getMasterModel().getPolicyStatusSubCode());
			policy.getMasterModel().setModeOfPremium(policyResponse.getModeOfPremium());
			policy.getMasterModel().setLifeAnnualPremium(policyResponse.getMasterModel().getLifeAnnualPremium());
			policy.getMasterModel().setOldStatusCode(policyResponse.getMasterModel().getOldStatusCode());

			//if (collectorType.equalsIgnoreCase("cisx"))
			//	policy.getMasterModel().setReInStatementDate(policyResponse.getMasterModel().getReInStatementDate());

			log.debug("PolicyPlanCode : >>" + policy.getMasterModel().getPlanCode() + "<<");
			log.debug("PolicyStatusCode : >>" + policy.getMasterModel().getPolicyStatus() + "<<");
			log.debug("TempPolicyCodeStatus : >>" + policy.getTempPolicyStatus() + "<<");
			log.debug("PolicyStatusSubCode : >>" + policy.getMasterModel().getPolicyStatusSubCode() + "<<");
			log.debug("TempPolicySubCodeStatus : >>" + policy.getTempPolicySubCodeStatus() + "<<");
		}
		if (StringUtils.isBlank(policy.getPolicyStatus()))
			policy.getMasterModel().setPolicyStatus("");
		if (StringUtils.isBlank(policy.getPolicyStatusSubCode()))
			policy.getMasterModel().setPolicyStatusSubCode("");
		double tempLifeAnnual = 0.0;
		if (policy.getCoverages() != null && policy.getCoverages().size() > 0)
			tempLifeAnnual = policy.getCoverages().get(0).getLifeAnnualPremium();
		policy.getMasterModel().setLifeAnnualPremium(tempLifeAnnual);

		if (StringUtils.isBlank(policy.getMasterModel().getPolicyHistoryID())) {
			if (StringUtils.isBlank(policy.getTempPolicyStatus())) {
				if (StringUtils.isBlank(policy.getMasterModel().getPolicyCurrentStatusCode())) {
					policy.setTempPolicyStatus("");
					log.debug("TempPolicyStatus Final");
				}
				else
					policy.setTempPolicyStatus(policy.getMasterModel().getPolicyCurrentStatusCode());
				log.debug("TempPolicyStatus Final : >>" + policy.getTempPolicyStatus() + "<<");
			}

			if (StringUtils.isBlank(policy.getTempPolicySubCodeStatus())) {
				if (StringUtils.isBlank(policy.getMasterModel().getPolicyCurrentSubStatusCode())) {
					policy.setTempPolicySubCodeStatus("");
					log.debug("TempPolicySubCodeStatus Final");
				}
				else
					policy.setTempPolicySubCodeStatus(policy.getMasterModel().getPolicyCurrentSubStatusCode());
				log.debug("TempPolicySubCodeStatus Final : >>" + policy.getTempPolicySubCodeStatus() + "<<");
			}
		}
		try {
			lof4claimStatement.close();
			lof4posStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] update policy called : ", e);
		}
		return policy;
	}

	private String dateToString(Date date) {
		if (date == null)
			return null;
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy", ENG_LOCALE);
		return sf.format(date);
	}

	private Date stringToDate(String date) {
		if (date == null || date.equals("null"))
			return null;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", ENG_LOCALE);
		try {
			return sf.parse(date);
		}
		catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private int checkPossibleROP(String coverageHistoryID, String coverageCode, String policyNumber, Date requestDate,
			List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosis, Statement lof4claimStatement) {
		log.debug("--------------------checkPossibleROP----------------");
		log.debug("inFunc_checkPossibleROP.getCoverageHistoryID >> : " + coverageHistoryID);
		log.debug("inFunc_checkPossibleROP.getCoverageCode >> : " + coverageCode);
		log.debug("inFunc_checkPossibleROP.getPolicyNumber >> : " + policyNumber);
		log.debug("inFunc_checkPossibleROP.finalRequestedDate >> : " + requestDate);

		wsClientService.init();
		String collectorType = wsClientService.getCollectorType();

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT ");
			sql.append(" * ");
			sql.append(" FROM CLM_CLAIM_HEADER_TR ");
			sql.append(" WHERE ");
			sql.append(" COVERAGE_CODE = '" + coverageCode + "' AND ");
			sql.append(" POLICY_NUMBER = '" + policyNumber + "' AND ");
			sql.append(" ADMISSION_DATE = TO_DATE('" + dateToString(requestDate) + "', 'DD-MM-YYYY') AND ");
			sql.append(" CLAIM_STATUS = '06' AND ");
			sql.append(" IS_DELETED = 0 ");
			log.debug("checkPossibleROP 1 :" + sql.toString());

			logMonitor.requestLog("checkPossibleROP_1", policyNumber, null, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("checkPossibleROP_1", null, null);
			if (rs.next()) {
				return 1;
			}
			else {
				sql = new StringBuilder();
				sql.append("SELECT ");
				sql.append(" ISSUE_DATE ");
				sql.append("FROM ").append("cisx".equalsIgnoreCase(collectorType) ? schema : "").append("CLM_HIS_COVERAGE_TR ");
				sql.append("WHERE ");
				sql.append("COVERAGE_HISTORY_ID = '" + coverageHistoryID + "' AND ");
				sql.append("ISSUE_DATE < TO_DATE('" + dateToString(requestDate) + "', 'DD-MM-YYYY') AND ");
				sql.append("MATURITY_DATE >= TO_DATE('" + dateToString(requestDate) + "', 'DD-MM-YYYY') ");

				log.debug("checkPossibleROP 2 :" + sql.toString());

				logMonitor.requestLog("checkPossibleROP_2", policyNumber, null, null, null, coverageHistoryID);
				//rs = lof4posStatement.executeQuery(sql.toString());
				rs = lof4claimStatement.executeQuery(sql.toString());
				
				logMonitor.responseLog("checkPossibleROP_2", null, null);
				
				if (rs.next()) {
					Date issueDate = stringToDate(rs.getString("ISSUE_DATE"));
					log.debug("-----------------------------------------------------------");
					log.debug("issue 	date : >>" + dateToString(issueDate) + "<<");
					log.debug("request 	date : >>" + dateToString(requestDate) + "<<");

					Calendar cStart = Calendar.getInstance();
					cStart.setTime(issueDate);
					Calendar cEnd = Calendar.getInstance();
					cEnd.setTime(issueDate);
					Calendar cCompare = Calendar.getInstance();
					cCompare.setTime(requestDate);

					cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
					cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
					cEnd.add(Calendar.DATE, -1);

					log.debug("-----------------------------------------------------------");
					log.debug("1 start date : >>" + dateToString(cStart.getTime()) + "<<");
					log.debug("1 end   date : >>" + dateToString(cEnd.getTime()) + "<<");

					if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
						cStart.add(Calendar.YEAR, -1);
						cEnd.add(Calendar.YEAR, -1);
					}
					if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
						cStart.add(Calendar.YEAR, 1);
						cEnd.add(Calendar.YEAR, 1);
					}

					String startDate = dateToString(cStart.getTime());
					String endDate = dateToString(cEnd.getTime());

					log.debug("-----------------------------------------------------------");
					log.debug("2 start  date : >>" + startDate + "<<");
					log.debug("2 end    date : >>" + endDate + "<<");
					log.debug("-----------------------------------------------------------");

					sql = new StringBuilder();
					sql.append(" SELECT ");
					sql.append(" COUNT(1) as COUNTING ");
					sql.append(" FROM CLM_CLAIM_HEADER_TR ");
					sql.append(" WHERE ");
					sql.append(" COVERAGE_CODE = '" + coverageCode + "' AND ");
					sql.append(" POLICY_NUMBER = '" + policyNumber + "' AND ");
					sql.append(" CLAIM_STATUS = '06' AND ");
					sql.append(" IS_DELETED = 0 AND ");
					sql.append(" ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY') AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");

					log.debug("checkPossibleROP 3 :" + sql.toString());

					logMonitor.requestLog("checkPossibleROP_3", policyNumber, null, null, null, coverageCode);
					rs = lof4claimStatement.executeQuery(sql.toString());
					logMonitor.responseLog("checkPossibleROP_3", null, null);

					if (rs.next()) {
						int count_3 = rs.getInt("COUNTING");
						System.out.println("count case 3 ROP : " + count_3);
						if (count_3 >= 30)
							return 3;
					}
					if (diagnosis != null && diagnosis.size() > 0) {
						for (com.mtl.eligible.model.mtlclaim.Diagnosis diagnosis2 : diagnosis) {
							sql = new StringBuilder();

							sql.append(" SELECT CL.CLAIM_NUMBER as CLAIM_NUMBER, ");
							sql.append(" DI.DIAGNOSIS_CODE as DIAGNOSIS_CODE, ");
							sql.append(" CL.ADMISSION_DATE as ADMISSION_DATE ");
							sql.append(" FROM CLM_CLAIM_DIAGNOSIS_TR DI ");
							sql.append(" INNER JOIN CLM_CLAIM_HEADER_TR CL ");
							sql.append(" ON CL.CLAIM_NUMBER     = DI.CLAIM_NUMBER ");
							sql.append(" WHERE CL.COVERAGE_CODE = '" + coverageCode + "' ");
							sql.append(" AND DI.DIAGNOSIS_CODE  = '" + diagnosis2.getDiagnosisCode() + "' ");
							sql.append(" AND ADMISSION_DATE     IS NOT NULL ");
							sql.append(" AND CL.POLICY_NUMBER   = '" + policyNumber + "' ");
							sql.append(" AND CL.CLAIM_STATUS    = '06' ");
							sql.append(" AND DI.IS_DELETED = 0 ");
							sql.append(" AND CL.IS_DELETED = 0 ");
							sql.append(" ORDER BY CL.ADMISSION_DATE ");

							log.debug("checkPossibleROP 4 :" + sql.toString());
							List<DiagnosisDto> diagnosisDtoList = new ArrayList<DiagnosisDto>();

							logMonitor.requestLog("checkPossibleROP_4", policyNumber, null, null, null, coverageCode);
							rs = lof4claimStatement.executeQuery(sql.toString());
							logMonitor.responseLog("checkPossibleROP_4", null, null);

							while (rs.next()) {
								DiagnosisDto rec = new DiagnosisDto(rs.getString("CLAIM_NUMBER"), rs.getString("DIAGNOSIS_CODE"),
										stringToDate(rs.getString("ADMISSION_DATE")));
								diagnosisDtoList.add(rec);
							}
							log.debug("diagnosisDtoList size : " + (diagnosisDtoList != null ? diagnosisDtoList.size() : "null"));

							if (diagnosisDtoList != null) {
								boolean t = true;
								List<DiagnosisDto> diagnosisDto = new ArrayList<DiagnosisDto>();
								for (int i = 0; i < diagnosisDtoList.size(); i++) {
									if (diagnosisDtoList.get(i).getAdmissionDate().before(requestDate))
										diagnosisDto.add(diagnosisDtoList.get(i));
									else {
										t = false;
										DiagnosisDto temp = new DiagnosisDto("", diagnosis2.getDiagnosisCode(), requestDate);
										temp.setCurrent(true);
										diagnosisDto.add(temp);
										for (int j = i; j < diagnosisDtoList.size(); j++)
											diagnosisDto.add(diagnosisDtoList.get(j));
										break;
									}
								}
								if (t) {
									DiagnosisDto temp = new DiagnosisDto("", diagnosis2.getDiagnosisCode(), requestDate);
									temp.setCurrent(true);
									diagnosisDto.add(temp);
								}
								log.debug("diagnosisDto size : " + diagnosisDto.size());
								for (DiagnosisDto dto : diagnosisDto)
									log.debug("admissionDate : " + dto.getAdmissionDate());
								if (!checkDateCaseROP(diagnosisDto))
									return 2;
							}
						}
					}
					return 0;
				}
				else
					return 0;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("checkPossibleROP_1", null, e.getMessage());
			logMonitor.responseLog("checkPossibleROP_2", null, e.getMessage());
			logMonitor.responseLog("checkPossibleROP_3", null, e.getMessage());
			logMonitor.responseLog("checkPossibleROP_4", null, e.getMessage());
			return 0;
		}
	}

	public com.mtl.eligible.model.mtlclaim.Coverage buildCoverageFromLOF4CLM(final String covCode, final String covStatus, final Date issueDate,
			Connection lof4claimConnection) {
		log.debug("--- Build coverage from LOF4CLM called ---");
		log.debug("Coverage_Code : " + covCode);
		com.mtl.eligible.model.mtlclaim.Coverage result = null;
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement buildCoverageFromLOF4CLM : " + e);
			return null;
		}

		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT ");
		sql.append(" TB3.PLAN_GROUP_TYPE, ");
		sql.append(" TB2.EFFECTIVE_DATE, ");
		sql.append(" TB2.MAXIMUM_LIMIT, ");
		sql.append(" TB3.PLAN_GROUP_CODE, ");
		sql.append(" TB2.PLAN_NAME ");
		sql.append(" FROM CLM_PLAN_TB TB2 ");
		sql.append(" INNER JOIN CLM_PLAN_GROUP_TB TB3 ");
		sql.append(" ON TB2.PLAN_GROUP_CODE   = TB3.PLAN_GROUP_CODE ");
		sql.append(" WHERE TRIM(TB2.PLAN_CODE) = ('" + covCode + "') ");
		sql.append(" AND TB2.IS_DELETED = 0 ");
		sql.append(" AND TB3.IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("buildCoverageFromLOF4CLM", null, null, null, null, covCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildCoverageFromLOF4CLM", null, null);
			while (rs.next()) {
				result = new com.mtl.eligible.model.mtlclaim.Coverage(new com.mtl.eligible.model.mtlclaim.master.CoverageMs("", covCode, covStatus,
						issueDate, rs.getString("PLAN_GROUP_TYPE"), stringToDate(rs.getString("EFFECTIVE_DATE")), "00", 0.0,
						rs.getString("PLAN_GROUP_CODE"), "", rs.getString("PLAN_NAME")), new ArrayList<com.mtl.eligible.model.mtlclaim.Diagnosis>());
				break;
			}
		}
		catch (Exception e) {
			log.debug("not found plan code : >>" + covCode + "<<");
			logMonitor.responseLog("buildCoverageFromLOF4CLM", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception ex) {
				log.error("[Error Closed statment] buildCoverageFromLOF4CLM : ", ex);
				return null;
			}
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception ex) {
			log.error("[Error Closed statment] buildCoverageFromLOF4CLM : ", ex);
			return null;
		}
		return result;
	}
	
	public List<com.mtl.eligible.model.mtlclaim.Coverage> buildCoverageListFromLOF4CLM(List<com.mtl.eligible.model.mtlclaim.Coverage> coverageList, Connection lof4claimConnection){
		log.debug("Start = buildCoverageListFromLOF4CLM =");
		List<com.mtl.eligible.model.mtlclaim.Coverage> resultList = new ArrayList<com.mtl.eligible.model.mtlclaim.Coverage>();
		
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement buildCoverageFromLOF4CLM : " + e);
			return null;
		}
		
		String coverageCodeStr = "";
		for(com.mtl.eligible.model.mtlclaim.Coverage coverage : coverageList){
			coverageCodeStr += (",'" + coverage.getCoverageCode() + "'");
		}
		if(coverageCodeStr.length() > 0){
			coverageCodeStr = coverageCodeStr.substring(1);
		}
		log.debug("coverageCodeStr = " + coverageCodeStr);
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" TB2.PLAN_CODE, ");
		sql.append(" TB3.PLAN_GROUP_TYPE, ");
		sql.append(" TB2.EFFECTIVE_DATE, ");
		sql.append(" TB2.MAXIMUM_LIMIT, ");
		sql.append(" TB3.PLAN_GROUP_CODE, ");
		sql.append(" TB2.PLAN_NAME ");
		sql.append(" FROM CLM_PLAN_TB TB2 ");
		sql.append(" INNER JOIN CLM_PLAN_GROUP_TB TB3 ");
		sql.append(" ON TB2.PLAN_GROUP_CODE   = TB3.PLAN_GROUP_CODE ");
		sql.append(" WHERE TRIM(TB2.PLAN_CODE) in (" + coverageCodeStr + ") ");
		sql.append(" AND TB2.IS_DELETED = 0 ");
		sql.append(" AND TB3.IS_DELETED = 0 ");
		
		log.debug("SQL Code: " + sql.toString());
		
		try{
			com.mtl.eligible.model.mtlclaim.Coverage result;
			
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			while(rs.next()){
				String rsPlanCode = rs.getString("PLAN_CODE");
				if(rsPlanCode != null){
					rsPlanCode = rsPlanCode.trim();
					for(com.mtl.eligible.model.mtlclaim.Coverage coverage : coverageList){
						if(rsPlanCode.equals(coverage.getCoverageCode())){
							result = new com.mtl.eligible.model.mtlclaim.Coverage(new com.mtl.eligible.model.mtlclaim.master.CoverageMs("", coverage.getCoverageCode(), coverage.getCoverageStatus(),
									coverage.getIssuedDate(), rs.getString("PLAN_GROUP_TYPE"), stringToDate(rs.getString("EFFECTIVE_DATE")), "00", 0.0,
									rs.getString("PLAN_GROUP_CODE"), "", rs.getString("PLAN_NAME")), new ArrayList<com.mtl.eligible.model.mtlclaim.Diagnosis>());
							
							resultList.add(result);
							
							break;
						}
					}
				}
			}
		}
		catch (Exception e) {
			log.debug("not found plan code : >>" + coverageCodeStr + "<<");
			logMonitor.responseLog("buildCoverageFromLOF4CLM", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception ex) {
				log.error("[Error Closed statment] buildCoverageFromLOF4CLM : ", ex);
				return null;
			}
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception ex) {
			log.error("[Error Closed statment] buildCoverageFromLOF4CLM : ", ex);
			return null;
		}
		
		log.debug("End = buildCoverageListFromLOF4CLM =");
		return resultList;
	}

	private boolean checkDateCaseROP(List<DiagnosisDto> diagnosisDtoList) {
		int count = 1;
		boolean check = false;
		if (diagnosisDtoList != null && diagnosisDtoList.size() > 7) {
			log.debug("checkDateCaseROP diagnosisDtoList size : " + diagnosisDtoList.size());
			for (int i = 0; i < diagnosisDtoList.size() - 1; i++) {
				if (checkDateBetween(diagnosisDtoList.get(i).getAdmissionDate(), diagnosisDtoList.get(i + 1).getAdmissionDate())) {
					if (diagnosisDtoList.get(i).isCurrent() || diagnosisDtoList.get(i + 1).isCurrent())
						check = true;
					count++;
					if (check && count > 7) {
						log.debug("count in loop : " + count);
						return false;
					}
				}
				else {
					check = false;
					count = 1;
				}
			}
			log.debug("count : " + count);
			if (check && count > 7)
				return false;
		}
		return true;
	}

	private boolean checkDateBetween(Date startDate, Date endDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.DATE, 14);

		Date tempDate = c.getTime();
		return endDate.compareTo(tempDate) <= 0;
	}

	private SumContinuity getSumContinuity(String clientNumber, String coverageCode, String trailerNumber, String benefitCode, Date issueDate,
			Date requestDate, Connection lof4claimConnection) {
		log.debug("--- get sum continuity ---");
		log.debug("issueDate : " + issueDate);
		log.debug("requestDate : " + requestDate);
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement getSumContinuity: " + e);
		}
		// find time of issue date
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		// end find time of issue date

		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT ");
		sql.append(" SUM(TB2.APPROVED_PERCENT) AS SUM_PERCENT ");
		sql.append(" ,SUM(TB2.APPROVED_AMOUNT) AS SUM_AMOUNT ");
		sql.append(" FROM CLM_CLAIM_HEADER_TR TB1 ");
		sql.append(" INNER JOIN CLM_CLAIM_BENEFIT_TR TB2  ");
		sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
		sql.append(" INNER JOIN CLM_CLIENT_TB TB3 ");
		sql.append(" ON TB1.CLIENT_NUMBER = TB3.CLIENT_NUMBER ");
		sql.append(" WHERE TB3.CAPSIL_CLIENT_NUMBER = '" + clientNumber + "' ");
		sql.append(" AND TB1.CLAIM_TYPE = 'A' ");
		sql.append(" AND TB1.COVERAGE_CODE = '" + coverageCode + "' ");
		sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
		sql.append(" AND BENEFIT_CODE = '" + benefitCode + "' ");
		sql.append(" AND TB1.INCIDENT_DATE between TO_DATE('" + startDate + "', 'dd-MM-yyyy') ");
		sql.append(" AND TO_DATE('" + endDate + "', 'dd-MM-yyyy') ");
		sql.append(" AND TB1.IS_DELETED = 0 ");
		sql.append(" AND TB2.IS_DELETED = 0 ");
		sql.append(" AND TB3.IS_DELETED = 0 ");

		log.debug("SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("getSumContinuity", null, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("getSumContinuity", null, null);
			while (rs.next()) {
				double sumPercent = rs.getDouble("SUM_PERCENT");
				double sumAmount = rs.getDouble("SUM_AMOUNT");

				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] getSumContinuity : ", e);
				}
				return new SumContinuity(sumPercent, sumAmount);
			}
		}
		catch (Exception e) {
			log.error("error : ", e);
			log.debug("not found buildBenefitsRuleContinuity");
			logMonitor.responseLog("getSumContinuity", null, e.getMessage());
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] getSumContinuity : ", e);
		}
		return null;
	}

	private int checkPossibleRTT(String coverageCode, String policyNumber, Date requestDate, Statement lof4claimStatement) {
		log.debug("--------------------checkPossibleRTT----------------");
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT ");
			sql.append(" COUNT(1) as COUNTING ");
			sql.append(" FROM CLM_CLAIM_HEADER_TR ");
			sql.append(" WHERE ");
			sql.append(" COVERAGE_CODE = '" + coverageCode + "' AND ");
			sql.append(" POLICY_NUMBER = '" + policyNumber + "' AND ");
			sql.append(" INCIDENT_DATE = TO_DATE('" + dateToString(requestDate) + "', 'DD-MM-YYYY') AND ");
			sql.append(" CLAIM_STATUS = '06' AND ");
			sql.append(" IS_DELETED = 0 ");
			log.debug("checkPossibleRTT 1 :" + sql.toString());

			logMonitor.requestLog("checkPossibleRTT", policyNumber, null, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("checkPossibleRTT", null, null);
			if (rs.next()) {
				int count = rs.getInt("COUNTING");
				System.out.println("count case RTT : " + count);
				if (count >= 6)
					return 5;
			}
			return 0;
		}
		catch (Exception e) {
			logMonitor.responseLog("checkPossibleRTT", null, e.getMessage());
			return 0;
		}
	}

	private double maxSumInsuredRHM(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured RHM ---");
		log.debug("coverageCode:" + coverageCode + " clientNumber:" + clientNumber + " policyNumber:" + policyNumber + " issueDate:" + issueDate
				+ " requestDate:" + requestDate);
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredRHM: " + e);
		}

		// find time of issue date
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);
		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL) is null ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL) end  AS SUMMAX_RHM ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" on TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER not in (" + temp + ") ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_STATUS in ('04','05','06','07','11') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredRHM :" + sql.toString());

			logMonitor.requestLog("maxSumInsuredRHM", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredRHM", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_RHM");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredRHM : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredRHM", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredRHM : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredRHM : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredRHL(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured RHL ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredRHL: " + e);
		}
		// find time of issue date
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL) is null ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL) end  AS SUMMAX_RHL ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" on TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER not in (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS in ('04','05','06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE Like 'LIC%%' ");
			sql.append(" OR TB2.BENEFIT_CODE Like 'LDV%%' ");
			sql.append(" OR TB2.BENEFIT_CODE Like 'LSF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE Like 'LAF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE Like 'LOR%%' ");
			sql.append(" OR TB2.BENEFIT_CODE Like 'LGE%%') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredRHL :" + sql.toString());

			logMonitor.requestLog("maxSumInsuredRHL", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredRHL", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_RHL");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredRHL : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredRHL", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredRHL : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredRHL : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredRHG(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {// add
																								// by
																								// ging
																								// 2016-02-15
		log.debug("--- max Sum Insured RHG ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredRHG: " + e);
		}
		// find time of issue date
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_RHG ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE '%DV%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%SC%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%SF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%AF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%GE%%') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredRHG :" + sql.toString());

			logMonitor.requestLog("maxSumInsuredRHG", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredRHG", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_RHG");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredRHG : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredRHG", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredRHG : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredRHG : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredRHH(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured RHH ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredRHH: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_RHH ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE '%RB%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%IC%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%HM%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%DV%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%OP%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%AM%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%GA%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%GE%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%SF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%ET%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%OD%%') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredRHH :" + sql.toString());

			logMonitor.requestLog("maxSumInsuredRHH", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredRHH", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_RHH");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredRHH : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredRHH", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredRHH : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredRHH : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredRHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured RHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredRHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_RHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.benefit_code in (select benefit_code from clm_plan_benefit_tb where plan_code like 'RHE%' or plan_code like 'LHE%') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredRHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_RHE");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredRHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredRHH : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredRHH : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredOD(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured OD ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredOD: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_OD ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE '%OD%%') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredOD :" + sql.toString());

			logMonitor.requestLog("maxSumInsuredOD", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredOD", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_OD");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredROD : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredOD", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredOD : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredOD : ", e);
		}
		return 0.0;
	}

	private double maxTimesBenefit(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- maxTimesBenefit ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxTimesBenefit: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";
			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT COUNT( DISTINCT tb1.job_number) AS MAX_TIMES ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.claim_number = TB2.claim_number ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number         = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code      = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number        = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT    IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS        IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER       = '" + trailerNumber + "' ");
			sql.append(" AND TB2.benefit_code        = 'OPE01' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY') AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxTimesBenefit :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("MAX_TIMES");
				System.out.println("maxTimesBenefit : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxTimesBenefit : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxTimesBenefit : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxTimesBenefit : ", e);
		}
		return 0.0;
	}

	private double maxFourTimesBenefit(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- maxFourTimesBenefit ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxFourTimesBenefit: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";
			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT COUNT( DISTINCT tb1.job_number) AS MAX_TIMES ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.claim_number = TB2.claim_number ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number         = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code      = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number        = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT    IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS        IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER       = '" + trailerNumber + "' ");
			sql.append(" AND TB2.benefit_code        = 'DM206' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxTimesBenefit :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("MAX_TIMES");
				System.out.println("maxFourTimesBenefit : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxFourTimesBenefit : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxFourTimesBenefit : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxFourTimesBenefit : ", e);
		}
		return 0.0;
	}

	private double maxTimeSP1(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate, String trailerNumber,
			List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- maxTimeSP1 ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxTimeSP1: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";
			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_day)IS NULL THEN 0 ELSE SUM(tb2.approved_day) END AS SUMDAY_MAXLIFE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number         = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code      = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number        = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT    IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS        IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER       = '" + trailerNumber + "' ");
			sql.append(" AND TB2.benefit_code        like 'ESP%' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY') AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxTimesBenefit :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMDAY_MAXLIFE");
				System.out.println("maxTimeSP1 : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxTimeSP1 : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxTimesBenefit : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxTimesBenefit : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredRHP(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured RHP ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredRHP: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_RHP ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE '%RB%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%IC%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%GE%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%SF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%AF%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%OR%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%DV%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%EN%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%PD%%' ");
			sql.append(" OR TB2.BENEFIT_CODE LIKE '%RN%%') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredRHP :" + sql.toString());
			logMonitor.requestLog("maxSumInsuredRHP", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredRHP", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_RHP");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredRHP : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredRHP", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredRHP : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredRHP : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredGen2(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, String coverageGroupCode, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured Gen%2 ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredGen2: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_GEN2 ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			if (coverageGroupCode.equals("RHP"))
				sql.append(" AND (TB2.BENEFIT_CODE LIKE 'GEN%2') ");
			else
				if (coverageGroupCode.equals("RDE"))
					sql.append(" AND (TB2.BENEFIT_CODE = 'DM102') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredGen2 :" + sql.toString());
			logMonitor.requestLog("maxSumInsuredGen2", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredGen2", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_GEN2");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredGen2 : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredGen2", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredGen2 : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredGen2 : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredGen3(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured Gen%3 ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredGen3: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_GEN3 ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE 'GEN%3') ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredGen%3 :" + sql.toString());
			logMonitor.requestLog("maxSumInsuredGen3", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredGen3", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_GEN3");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredGen3 : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredGen3", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredGen3 : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredGen3 : ", e);
		}
		return 0.0;
	}

	// jame
	private double claimYearALM01RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear ALM01 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearALM01RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'ALM01' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearALM01RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearALM01RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear SumInsuredRHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearALM01RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearALM01RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearCHUP1RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear CHUP1 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearCHUP1RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'CHUP1' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearCHUP1RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearCHUP1RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearCHUP1RHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearCHUP1RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearCHUP1RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearVAC01RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear VAC01 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearVAC01RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'VAC01' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearVAC01RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearVAC01RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearVAC01RHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearVAC01RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearVAC01RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearDET01RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear DET01 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearDET01RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'DET01' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearDET01RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearDET01RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearDET01RHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearDET01RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearDET01RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearEYE01RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear EYE01 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearEYE01RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'EYE01' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearEYE01RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearEYE01RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearEYE01RHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearEYE01RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearEYE01RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearOPE01RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear OPE01 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearOPE01RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'OPE01' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearOPE01RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearOPE01RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearOPE01RHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearOPE01RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearOPE01RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearESP50RHE(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear ESP50 SumInsuredRHE ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearESP50RHE: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearRHE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'ESP50' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearESP50RHE :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearRHE");
				System.out.println("claimYearESP50RHE : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearESP50RHE : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearESP50RHE : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearESP50RHE : ", e);
		}
		return 0.0;
	}

	private double claimYearMilkyWAY(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- claimYear MilkyWAY ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement claimYear claimYearMilkyWAY: " + e);
		}
		Calendar cStart = Calendar.getInstance();
		cStart.setTime(issueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(issueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(requestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		String startDate = dateToString(cStart.getTime());
		String endDate = dateToString(cEnd.getTime());
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_claimYearMilkyWAY ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE like 'DM21%' ");
			sql.append(" AND TB1.ADMISSION_DATE BETWEEN TO_DATE('" + startDate + "', 'DD-MM-YYYY')  ");
			sql.append(" AND TO_DATE('" + endDate + "', 'DD-MM-YYYY') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("claimYear claimYearMilkyWAY :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_claimYearMilkyWAY");
				System.out.println("claimYearMilkyWAY : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] claimYear claimYearMilkyWAY : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] claimYear claimYearMilkyWAY : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] claimYear claimYearMilkyWAY : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredTRN(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured TRN ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredTRN: " + e);
		}

		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_TRN ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE 'TRN%%') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredTRN :" + sql.toString());

			logMonitor.requestLog("maxSumInsuredTRN", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("maxSumInsuredTRN", null, null);
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_TRN");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredTRN : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("maxSumInsuredTRN", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredTRN : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredTRN : ", e);
		}
		return 0.0;
	}

	private double maxSumInsuredEMQ(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- maxSumInsuredEMQ ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxSumInsuredEMQ: " + e);
		}

		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_MEQ ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND (TB2.BENEFIT_CODE LIKE 'EMQ%') ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredTRN :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_MEQ");
				System.out.println("sumInsured : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredEMQ : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredEMQ : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredEMQ : ", e);
		}
		return 0.0;
	}

	private double maxLife(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate, String trailerNumber,
			List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- max Sum Insured maxLife ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxLife: " + e);
		}

		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS SUMMAX_LIFE ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'EPSY1' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("maxSumInsuredmaxLife :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double sumInsured = rs.getDouble("SUMMAX_LIFE");
				System.out.println("sumInsured maxLife : " + sumInsured);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxSumInsuredmaxLife : ", e);
				}
				return sumInsured;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxSumInsuredmaxLife : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxSumInsuredmaxLife : ", e);
		}
		return 0.0;
	}

	private double benefitRDDAmount(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			String benefitCode, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- approvedAmount benefitGroupRDD Amount ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement benefitRDDAmount: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS approvedAmount ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = '" + benefitCode + "' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" group by TB2.BENEFIT_CODE ");

			log.debug("benefitRDDAmount :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double approvedAmount = rs.getDouble("approvedAmount");
				System.out.println("approvedAmount : " + approvedAmount);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] benefitRDDAmount : ", e);
				}
				return approvedAmount;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] benefitRDDAmount : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] benefitRDDAmount : ", e);
		}
		return 0.0;
	}

	private double maxPercentRDP(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, String benefitCode, Connection lof4claimConnection) {
		log.debug("--- maxPercentDRP ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement maxPercentDRP: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_percent)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_percent)END AS approvedPERCENT ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			if (benefitCode.equals("C2501") || benefitCode.equals("CAA01")) {
				sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%01' ");
			}
			else
				if (benefitCode.equals("C2502") || benefitCode.equals("CAA02")) {
					sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%02' ");
				}
				else
					if (benefitCode.equals("CAA03")) {
						sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%03' ");
					}
					else
						if (benefitCode.equals("C2504") || benefitCode.equals("C5004") || benefitCode.equals("CAA04")) {
							sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%04' ");
						}
						else
							if (benefitCode.equals("C2505") || benefitCode.equals("C5005") || benefitCode.equals("CAA05")) {
								sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%05' ");
							}
							else
								if (benefitCode.equals("C2506") || benefitCode.equals("CAA06") || benefitCode.equals("C5006")) {
									sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%06' ");
								}
								else
									if (benefitCode.equals("C2507") || benefitCode.equals("CAA07") || benefitCode.equals("C5007")) {
										sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%07' ");
									}
									else
										if (benefitCode.equals("CAA08") || benefitCode.equals("C2508")) {
											sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%08' ");
										}
										else
											if (benefitCode.equals("C2509") || benefitCode.equals("CAA09") || benefitCode.equals("C5009")) {
												sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%09' ");
											}
											else
												if (benefitCode.equals("C2510") || benefitCode.equals("CAA10") || benefitCode.equals("C5010")) {
													sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%10' ");
												}
												else
													if (benefitCode.equals("C2511") || benefitCode.equals("CAA11") || benefitCode.equals("C5011")) {
														sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%11' ");
													}
													else
														if (benefitCode.equals("C2512") || benefitCode.equals("CAA12") || benefitCode.equals("C5012")) {
															sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%12' ");
														}
														else
															if (benefitCode.equals("C2513") || benefitCode.equals("CAA13")
																	|| benefitCode.equals("C5013")) {
																sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%13' ");
															}
															else
																if (benefitCode.equals("C2514") || benefitCode.equals("CAA14")
																		|| benefitCode.equals("C5014")) {
																	sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%14' ");
																}
																else
																	if (benefitCode.equals("CAA15") || benefitCode.equals("C5015")
																			|| benefitCode.equals("C2515")) {
																		sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%15' ");
																	}
																	else
																		if (benefitCode.equals("CAA16")) {
																			sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%16' ");
																		}
																		else
																			if (benefitCode.equals("CAA17") || benefitCode.equals("C2517")) {
																				sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%17' ");
																			}
																			else
																				if (benefitCode.equals("C2518") || benefitCode.equals("CAA18")
																						|| benefitCode.equals("C5018")) {
																					sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%18' ");
																				}
																				else
																					if (benefitCode.equals("C2519") || benefitCode.equals("CAA19")
																							|| benefitCode.equals("C5019")) {
																						sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%19' ");
																					}
																					else
																						if (benefitCode.equals("CAA20")
																								|| benefitCode.equals("C2520")) {
																							sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%20' ");
																						}
																						else
																							if (benefitCode.equals("C2521")
																									|| benefitCode.equals("CAA21")
																									|| benefitCode.equals("C5021")) {
																								sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%21' ");
																							}
																							else
																								if (benefitCode.equals("CAA22")) {
																									sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%22' ");
																								}
																								else
																									if (benefitCode.equals("C2523")
																											|| benefitCode.equals("CAA23")
																											|| benefitCode.equals("C5023")) {
																										sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%23' ");
																									}
																									else
																										if (benefitCode.equals("CAA24")) {
																											sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%24' ");
																										}
																										else
																											if (benefitCode.equals("CAA25")
																													|| benefitCode.equals("C2525")) {
																												sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%25' ");
																											}
																											else
																												if (benefitCode.equals("C2526")
																														|| benefitCode
																																.equals("CAA26")
																														|| benefitCode
																																.equals("C5026")) {
																													sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%26' ");
																												}
																												else
																													if (benefitCode.equals("CAA27")
																															|| benefitCode
																																	.equals("C2527")) {
																														sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%27' ");
																													}
																													else
																														if (benefitCode
																																.equals("CAA28")
																																|| benefitCode
																																		.equals("C2528")) {
																															sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%28' ");
																														}
																														else
																															if (benefitCode
																																	.equals("CAA29")
																																	|| benefitCode
																																			.equals("C2529")) {
																																sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%29' ");
																															}
																															else
																																if (benefitCode
																																		.equals("CAA30")
																																		|| benefitCode
																																				.equals("C2530")) {
																																	sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%30' ");
																																}
																																else
																																	if (benefitCode
																																			.equals("CAA31")
																																			|| benefitCode
																																					.equals("C2531")) {
																																		sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%31' ");
																																	}
																																	else
																																		if (benefitCode
																																				.equals("CAA32")) {
																																			sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%32' ");
																																		}
																																		else
																																			if (benefitCode
																																					.equals("CAA33")) {
																																				sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%33' ");
																																			}
																																			else
																																				if (benefitCode
																																						.equals("CAA34")) {
																																					sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%34' ");
																																				}
																																				else
																																					if (benefitCode
																																							.equals("C2535")
																																							|| benefitCode
																																									.equals("CAA35")
																																							|| benefitCode
																																									.equals("C5035")) {
																																						sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%35' ");
																																					}
																																					else
																																						if (benefitCode
																																								.equals("CAA36")) {
																																							sql.append(" AND TB2.BENEFIT_CODE LIKE 'C%%36' ");
																																						}
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");

			log.debug("maxPercentRDP :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double approvedPERCENT = rs.getDouble("approvedPERCENT");
				System.out.println("approvedPERCENT : " + approvedPERCENT);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] maxPercentDRP : ", e);
				}
				return approvedPERCENT;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] maxPercentDRP : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] maxPercentDRP : ", e);
		}
		return 0.0;
	}

	private double benefitHLD12(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- approvedAmount benefitHLD12 ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement benefitHLD12: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS approvedAmount ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'HL130' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" group by TB2.BENEFIT_CODE ");

			log.debug("benefitRDDAmount :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double approvedAmount = rs.getDouble("approvedAmount");
				System.out.println("approvedAmount : " + approvedAmount);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] benefitHLD12 : ", e);
				}
				return approvedAmount;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] benefitHLD12 : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] benefitHLD12 : ", e);
		}
		return 0.0;
	}

	private double benefitHLD1(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- approvedAmount benefitHLD1 ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement benefitHLD1: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS approvedAmount ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND TB2.BENEFIT_CODE = 'HLD11'");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" group by TB2.BENEFIT_CODE ");

			log.debug("benefitHLD1 :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double approvedAmount = rs.getDouble("approvedAmount");
				System.out.println("approvedAmount : " + approvedAmount);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] benefitHLD1 : ", e);
				}
				return approvedAmount;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] benefitHLD1 : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] benefitHLD1 : ", e);
		}
		return 0.0;
	}

	private Date tempIssueDate(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, String benefitGroup, String coverageGroupCode, Connection lof4claimConnection) {
		log.debug("--- tempIssueDate ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement tempIssueDate: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT TB1.COVERAGE_ISSUED_DATE as tempIssueDate ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			if (coverageGroupCode.equals("RDK")) {
				sql.append(" AND TB2.BENEFIT_CODE = 'DDRK'");
			}
			if ((coverageGroupCode.equals("RCA")) && (benefitGroup.equals("HB10") || benefitGroup.equals("HB11") || benefitGroup.equals("HB12"))) {
				sql.append(" AND (TB2.BENEFIT_CODE = 'CA085'");
				sql.append(" OR TB2.BENEFIT_CODE = 'CA100') ");
			}
			sql.append(" AND TB2.APPROVED_PERCENT <> '0' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("tempIssueDate :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				Date tempIssueDate = stringToDate(rs.getString("tempIssueDate"));
				System.out.println("tempIssueDate : " + tempIssueDate);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] tempIssueDate : ", e);
				}
				return tempIssueDate;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] tempIssueDate : ", a);
			}
			return null;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] tempIssueDate : ", e);
		}
		return null;
	}

	private Date tempRequestDate(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, String benefitGroup, String coverageGroupCode, Connection lof4claimConnection) {
		log.debug("--- tempRequestDate ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement tempRequestDate: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT tb1.admission_date as tempRequestDate ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			if (coverageGroupCode.equals("RDK")) {
				sql.append(" AND TB2.BENEFIT_CODE = 'DDRK'");
			}
			else
				if ((coverageGroupCode.equals("RCP") || coverageGroupCode.equals("RCA"))
						&& (benefitGroup.equals("CPR15") || benefitGroup.equals("CICA1") || benefitGroup.equals("CICA2"))) {
					sql.append(" AND TB2.BENEFIT_CODE = 'CA015'");
				}
				else
					if ((coverageGroupCode.equals("RCA"))
							&& (benefitGroup.equals("HB10") || benefitGroup.equals("HB11") || benefitGroup.equals("HB12"))) {
						sql.append(" AND (TB2.BENEFIT_CODE = 'CA085' ");
						sql.append(" OR TB2.BENEFIT_CODE = 'CA100') ");
					}
					else
						if (coverageGroupCode.equals("RDM")) {
							sql.append(" AND (TB2.BENEFIT_CODE        = 'DDRA' ");
							sql.append(" OR TB2.BENEFIT_CODE          = 'DDRB' ");
							sql.append(" OR TB2.BENEFIT_CODE          = 'DDRC' ");
							sql.append(" OR TB2.BENEFIT_CODE          = 'DDRD') ");
						}
						else
							if (coverageGroupCode.equals("RDE") && (benefitGroup.equals("DM102"))) {
								sql.append(" AND TB2.BENEFIT_CODE = 'DM102'");
							}
							else
								if (coverageGroupCode.equals("RDE") && (benefitGroup.equals("DM104"))) {
									sql.append(" AND TB2.BENEFIT_CODE = 'DM103'");
								}
			sql.append(" AND TB2.APPROVED_PERCENT <> '0' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			log.debug("tempRequestDate :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				Date tempRequestDate = stringToDate(rs.getString("tempRequestDate"));
				System.out.println("tempRequestDate : " + tempRequestDate);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] tempRequestDate : ", e);
				}
				return tempRequestDate;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] tempRequestDate : ", a);
			}
			return null;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] tempRequestDate : ", e);
		}
		return null;
	}

	private Date findTempStartDate(Date tempIssueDate, Date tempRequestDate) {
		// find time of issue date
		if (tempIssueDate == null || tempRequestDate == null)
			return null;

		Calendar cStart = Calendar.getInstance();
		cStart.setTime(tempIssueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(tempIssueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(tempRequestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		Date startDate = cStart.getTime();
		return startDate;
	}

	private Date findTempEndDate(Date tempIssueDate, Date tempRequestDate) {
		// find time of issue date
		if (tempIssueDate == null || tempRequestDate == null)
			return null;

		Calendar cStart = Calendar.getInstance();
		cStart.setTime(tempIssueDate);
		Calendar cEnd = Calendar.getInstance();
		cEnd.setTime(tempIssueDate);
		Calendar cCompare = Calendar.getInstance();
		cCompare.setTime(tempRequestDate);

		cStart.set(Calendar.YEAR, cCompare.get(Calendar.YEAR));
		cEnd.set(Calendar.YEAR, cCompare.get(Calendar.YEAR) + 1);
		cEnd.add(Calendar.DATE, -1);
		if (cStart.getTime().compareTo(cCompare.getTime()) > 0) {
			cStart.add(Calendar.YEAR, -1);
			cEnd.add(Calendar.YEAR, -1);
		}
		if (cEnd.getTime().compareTo(cCompare.getTime()) < 0) {
			cStart.add(Calendar.YEAR, 1);
			cEnd.add(Calendar.YEAR, 1);
		}
		Date endDate = cEnd.getTime();
		return endDate;
	}

	private double benefitEarlyStage(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, String benefitGroupCode, Connection lof4claimConnection) {
		log.debug("--- approvedAmount benefitEarlyStage ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement benefitEarlyStage: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS approvedAmount ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			if (benefitGroupCode.equals("CPR15")) {
				sql.append(" AND TB2.BENEFIT_CODE = 'CA015'");
			}
			else
				if (benefitGroupCode.equals("DM104")) {
					sql.append(" AND TB2.BENEFIT_CODE = 'DM103'");
				}
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" group by TB2.BENEFIT_CODE ");

			log.debug("benefitEarlyStage :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double approvedAmount = rs.getDouble("approvedAmount");
				System.out.println("approvedAmount : " + approvedAmount);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] benefitEarlyStage : ", e);
				}
				return approvedAmount;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] benefitEarlyStage : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] benefitEarlyStage : ", e);
		}
		return 0.0;
	}

	private double benefitLateStage(String coverageCode, String clientNumber, String policyNumber, Date issueDate, String trailerNumber,
			List<String> claimNumbers, String benefitGroupCode, String coverageGroupCode, Connection lof4claimConnection) {
		log.debug("--- approvedAmount benefitLateStage ---");
		// find time of issue date
		Statement lof4claimStatement = null;
		try {

			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement benefitLateStage: " + e);
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'" + "";
			for (String i : claimNumbers) {
				temp = temp + i + "','";

			}
			temp = temp.substring(0, temp.length() - 2);
		}

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS approvedAmount ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
			sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
			sql.append(" INNER JOIN clm_client_tb TB3 ");
			sql.append(" ON TB1.client_number = tb3.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			if (benefitGroupCode.equals("CICA2")) {
				sql.append(" AND TB2.BENEFIT_CODE = 'CA100'");
			}
			if (coverageGroupCode.equals("RDP")) {
				sql.append(" AND TB2.BENEFIT_CODE LIKE 'CAA%%' ");
			}
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" AND tb3.is_deleted = '0' ");
			sql.append(" group by TB2.BENEFIT_CODE ");

			log.debug("benefitLateStage :" + sql.toString());
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			if (rs.next()) {
				double approvedAmount = rs.getDouble("approvedAmount");
				System.out.println("approvedAmount : " + approvedAmount);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] benefitLateStage : ", e);
				}
				return approvedAmount;
			}
		}
		catch (Exception e) {
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] benefitLateStage : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] benefitLateStage : ", e);
		}
		return 0.0;
	}

	/**/
	/**
	 * private double benefitMiddleStage(String coverageCode, String
	 * clientNumber, String policyNumber, Date issueDate, String
	 * trailerNumber,List<String> claimNumbers,String
	 * benefitGroupCode,Connection lof4claimConnection) {
	 * log.debug("--- approvedAmount benefitMiddleStage ---"); //find time of
	 * issue date Statement lof4claimStatement = null; try {
	 * 
	 * lof4claimStatement = lof4claimConnection.createStatement(); } catch
	 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace();
	 * setStatus(-1); log.debug(
	 * "Error Cannot createStatement lof4claimStatement benefitMiddleStage: " +
	 * e); } String temp = "' '"; if(claimNumbers!=null &&
	 * claimNumbers.size()>0){ temp = "'"+""; for(String i:claimNumbers){ temp =
	 * temp+i+"','";
	 * 
	 * } temp=temp.substring(0,temp.length()-2); }
	 * 
	 * try { StringBuilder sql = new StringBuilder();
	 * sql.append(" SELECT CASE WHEN SUM(tb2.approved_TOTAL)IS NULL ");
	 * sql.append(" THEN 0 ELSE SUM(tb2.approved_TOTAL)END AS approvedAmount ");
	 * sql.append(" FROM clm_claim_header_tr TB1 ");
	 * sql.append(" INNER JOIN clm_claim_benefit_tr TB2 ");
	 * sql.append(" ON TB1.CLAIM_NUMBER = TB2.CLAIM_NUMBER ");
	 * sql.append(" INNER JOIN clm_client_tb TB3 ");
	 * sql.append(" ON TB1.client_number = tb3.client_number ");
	 * sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
	 * sql.append(" AND tb3.capsil_client_number = '" + clientNumber + "' ");
	 * sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
	 * sql.append(" AND TB1.CLAIM_NUMBER NOT IN ("+ temp +") ");
	 * sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
	 * sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
	 * if(benefitGroupCode.equals("C2504")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5004' "); } else
	 * if(benefitGroupCode.equals("C2505")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5005' "); } else
	 * if(benefitGroupCode.equals("C2506")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5006' "); } else
	 * if(benefitGroupCode.equals("C2507")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5007' "); } else
	 * if(benefitGroupCode.equals("C2509")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5009' "); } else
	 * if(benefitGroupCode.equals("C2510")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5010' "); } else
	 * if(benefitGroupCode.equals("C2511")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5011' "); } else
	 * if(benefitGroupCode.equals("C2512")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5012' "); } else
	 * if(benefitGroupCode.equals("C2513")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5013' "); } else
	 * if(benefitGroupCode.equals("C2514")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5014' "); } else
	 * if(benefitGroupCode.equals("C2515")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5015' "); } else
	 * if(benefitGroupCode.equals("C2518")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5018' "); } else
	 * if(benefitGroupCode.equals("C2519")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5019' "); } else
	 * if(benefitGroupCode.equals("C2521")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5021' "); } else
	 * if(benefitGroupCode.equals("C2523")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5023' "); } else
	 * if(benefitGroupCode.equals("C2526")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5026' "); } else
	 * if(benefitGroupCode.equals("C2535")){
	 * sql.append(" AND TB2.BENEFIT_CODE = 'C5035'	 "); }
	 * sql.append(" AND tb1.is_deleted = '0' ");
	 * sql.append(" AND tb2.is_deleted = '0' ");
	 * sql.append(" AND tb3.is_deleted = '0' ");
	 * sql.append(" group by TB2.BENEFIT_CODE ");
	 * 
	 * log.debug("benefitMiddleStage :" + sql.toString()); ResultSet rs =
	 * lof4claimStatement.executeQuery(sql.toString()); if(rs.next()){ double
	 * approvedAmount = rs.getDouble("approvedAmount");
	 * System.out.println("approvedAmount : " + approvedAmount); try{
	 * lof4claimStatement.close(); } catch (Exception e) {
	 * log.error("[Error Closed statment] benefitMiddleStage : " , e); } return
	 * approvedAmount; } } catch(Exception e){ try{ lof4claimStatement.close();
	 * } catch (Exception a) {
	 * log.error("[Error Closed statment] benefitMiddleStage : " , a); } return
	 * 0.0; } try{ lof4claimStatement.close(); } catch (Exception e) {
	 * log.error("[Error Closed statment] benefitMiddleStage : " , e); } return
	 * 0.0; } /
	 **/
	private double coverageApprovedTotal(String coverageCode, String clientNumber, String policyNumber, Date issueDate, Date requestDate,
			String trailerNumber, List<String> claimNumbers, Connection lof4claimConnection) {
		log.debug("--- coverageApprovedTotal ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement coverageApprovedTotal: " + e);
			return 0.0;
		}
		String temp = "' '";
		if (claimNumbers != null && claimNumbers.size() > 0) {
			temp = "'";
			for (String i : claimNumbers)
				temp = temp + i + "','";
			temp = temp.substring(0, temp.length() - 2);
		}
		try {
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT CASE WHEN SUM(tb1.TOTAL_CLAIM_AMT)IS NULL ");
			sql.append(" THEN 0 ELSE SUM(tb1.TOTAL_CLAIM_AMT)END AS approvedAmount ");
			sql.append(" FROM clm_claim_header_tr TB1 ");
			sql.append(" INNER JOIN clm_client_tb TB2 ");
			sql.append(" ON TB1.client_number = TB2.client_number ");
			sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			sql.append(" AND tb2.capsil_client_number = '" + clientNumber + "' ");
			sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			sql.append(" AND TB1.CLAIM_NUMBER NOT IN (" + temp + ") ");
			sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			sql.append(" AND tb1.is_deleted = '0' ");
			sql.append(" AND tb2.is_deleted = '0' ");
			sql.append(" ORDER BY tb1.client_number ");

			// if(coverageCode.contains("RHE04") ||
			// coverageCode.contains("RHE03")){
			// sql.append(" SELECT CASE WHEN SUM(tb3.approved_TOTAL)IS NULL ");
			// sql.append(" THEN 0 ELSE SUM(tb3.approved_TOTAL)END AS approvedAmountPSY ");
			// sql.append(" FROM clm_claim_header_tr TB1 ");
			// sql.append(" INNER JOIN clm_client_tb TB2 ");
			// sql.append(" ON TB1.client_number = TB2.client_number ");
			// sql.append(" INNER JOIN clm_claim_benefit_tr TB3 ");
			// sql.append(" ON TB1.CLAIM_NUMBER = TB3.CLAIM_NUMBER ");
			// sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			// sql.append(" AND tb2.capsil_client_number = '" + clientNumber +
			// "' ");
			// sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			// sql.append(" AND TB1.CLAIM_NUMBER NOT IN ("+ temp +") ");
			// sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			// sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			// sql.append(" AND TB3.BENEFIT_CODE = 'EPSY1' ");
			// sql.append(" AND tb1.is_deleted = '0' ");
			// sql.append(" AND tb2.is_deleted = '0' ");
			// sql.append(" AND tb3.is_deleted = '0' ");
			// sql.append(" ORDER BY tb1.client_number ");
			// }else{
			// sql.append(" SELECT CASE WHEN SUM(tb1.TOTAL_CLAIM_AMT)IS NULL ");
			// sql.append(" THEN 0 ELSE SUM(tb1.TOTAL_CLAIM_AMT)END AS approvedAmount ");
			// sql.append(" FROM clm_claim_header_tr TB1 ");
			// sql.append(" INNER JOIN clm_client_tb TB2 ");
			// sql.append(" ON TB1.client_number = TB2.client_number ");
			// sql.append(" WHERE tb1.coverage_code = '" + coverageCode + "' ");
			// sql.append(" AND tb2.capsil_client_number = '" + clientNumber +
			// "' ");
			// sql.append(" AND tb1.policy_number = '" + policyNumber + "' ");
			// sql.append(" AND TB1.CLAIM_NUMBER NOT IN ("+ temp +") ");
			// sql.append(" AND TB1.CLAIM_STATUS IN ('06','07','11') ");
			// sql.append(" AND TB1.TRAILER_NUMBER = '" + trailerNumber + "' ");
			// sql.append(" AND tb1.is_deleted = '0' ");
			// sql.append(" AND tb2.is_deleted = '0' ");
			// sql.append(" ORDER BY tb1.client_number ");
			//
			// }

			log.debug("coverageApprovedTotal :" + sql.toString());

			logMonitor.requestLog("coverageApprovedTotal", policyNumber, clientNumber, null, null, coverageCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("coverageApprovedTotal", null, null);
			if (rs.next()) {
				double approvedAmount = rs.getDouble("approvedAmount");
				System.out.println("approvedAmount : " + approvedAmount);
				try {
					lof4claimStatement.close();
				}
				catch (Exception e) {
					log.error("[Error Closed statment] coverageApprovedTotal : ", e);
				}
				return approvedAmount;
			}
		}
		catch (Exception e) {
			logMonitor.responseLog("coverageApprovedTotal", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] coverageApprovedTotal : ", a);
			}
			return 0.0;
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] coverageApprovedTotal : ", e);
		}
		return 0.0;
	}

	public HospitalBlacklistModel buildHospitalClaim(String hospitalCode, Connection lof4claimConnection) {
		log.debug("--- buildHospitalClaim ---");
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement buildHospitalClaim : " + e);
			return null;
		}

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT TB1.HOSPITAL_CODE ");
		sql.append(" ,TB1.NAME AS HOSPITAL_NAME ");
		sql.append(" ,TB1.IS_BLACK_LIST ");
		sql.append(" ,TB2.PA AS IS_CONTACT ");
		sql.append(" FROM CLM_HOSPITAL_TB TB1 ");
		sql.append(" LEFT JOIN CLM_HOSPITAL_CONTRACT_TB TB2 ");
		sql.append(" ON TB1.CONTRACT_CODE = TB2.CONTRACT_CODE ");
		sql.append(" where TB1.HOSPITAL_CODE = '" + hospitalCode + "' ");

		log.debug("case buildHospitalClaim SQL Code: " + sql.toString());
		try {
			logMonitor.requestLog("buildHospitalClaim", null, null, null, null, hospitalCode);
			ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
			logMonitor.responseLog("buildHospitalClaim", null, null);
			while (rs.next()) {
				HospitalBlacklistModel result = new HospitalBlacklistModel();
				result.getHospital().getHospital().setHospitalCode(rs.getString("HOSPITAL_CODE"));
				result.getHospital().getHospital().setHospitalName(rs.getString("HOSPITAL_NAME"));
				result.getHospital()
						.getHospital()
						.putHospitalStatus(com.mtl.hospital.util.ModelUtility.CONTACT_PA,
								(StringUtils.isNotBlank(rs.getString("IS_CONTACT")) ? rs.getString("IS_CONTACT") : "N"));
				result.getHospital()
						.getHospital()
						.putHospitalStatus(com.mtl.hospital.util.ModelUtility.BLACKLIST,
								(StringUtils.isNotBlank(rs.getString("IS_BLACK_LIST")) ? rs.getString("IS_BLACK_LIST") : "N"));
				log.debug("Result HospitalBlacklistModel : " + result.toString());// read
																					// log
																					// result
																					// ging
																					// 24-02-2016
				return result;
			}
		}
		catch (Exception e) {
			log.error("error : " + e.getMessage());
			log.debug("not found buildHospitalClaim");
			logMonitor.responseLog("buildHospitalClaim", null, e.getMessage());
			try {
				lof4claimStatement.close();
			}
			catch (Exception a) {
				log.error("[Error Closed statment] buildHospitalClaim : ", a);
				return null;
			}
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] buildHospitalClaim : ", e);
			return null;
		}
		return null;
	}

	// ging 28-03-2016
	private List<com.mtl.eligible.model.mtlclaim.Diagnosis> getDiagnosis(final List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosisList,
			Connection lof4claimConnection) {
		Statement lof4claimStatement = null;
		try {
			lof4claimStatement = lof4claimConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot createStatement lof4claimStatement update coverages called: " + e);
		}
		StringBuilder sql = new StringBuilder();
		String diagnosisCode = "";
		List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosisListResult = new ArrayList<com.mtl.eligible.model.mtlclaim.Diagnosis>();

		if (diagnosisList != null && diagnosisList.size() > 0) {
			for (com.mtl.eligible.model.mtlclaim.Diagnosis diagnosis : diagnosisList)
				diagnosisCode = diagnosisCode + "'" + diagnosis.getDiagnosisCode() + "',";
			diagnosisCode = diagnosisCode.substring(0, diagnosisCode.length() - 1);

			log.debug("--- Build getdDiagnosis called ---");
			sql.append(" SELECT DIAGNOSIS_CODE,DIAGNOSIS_CAUSE  ");
			sql.append(" FROM CLM_DIAGNOSIS_TB ");
			sql.append(" WHERE DIAGNOSIS_CODE IN (" + diagnosisCode + ") ");

			log.debug("getdDiagnosis sql :" + sql.toString());
			try {
				logMonitor.requestLog("getDiagnosis", null, null, null, null, diagnosisCode);
				ResultSet rs = lof4claimStatement.executeQuery(sql.toString());
				logMonitor.responseLog("getDiagnosis", null, null);
				while (rs.next()) {
					com.mtl.eligible.model.mtlclaim.Diagnosis rec = new com.mtl.eligible.model.mtlclaim.Diagnosis();
					String code = rs.getString("DIAGNOSIS_CODE");
					String diagnosisCause = rs.getString("DIAGNOSIS_CAUSE");

					code = StringUtils.isNotBlank(code) ? code : "";
					diagnosisCause = StringUtils.isNotBlank(diagnosisCause) ? diagnosisCause : "";

					com.mtl.eligible.model.mtlclaim.master.DiagnosisMs diagnosisMs = new DiagnosisMs(code, diagnosisCause);
					diagnosisMs.setCode(code);
					diagnosisMs.setDiagnosisCause(diagnosisCause);

					rec.setMasterModel(diagnosisMs);
					diagnosisListResult.add(rec);
				}

			}
			catch (Exception e) {
				log.debug("getDiagnosis error :" + e.getMessage());
				logMonitor.responseLog("getDiagnosis", null, e.getMessage());
				try {
					lof4claimStatement.close();
				}
				catch (Exception a) {
					log.error("[Error Closed statment] Build getdDiagnosis called : ", a);
				}
				return null;
			}
		}
		try {
			lof4claimStatement.close();
		}
		catch (Exception e) {
			log.error("[Error Closed statment] Build getdDiagnosis called : ", e);
		}
		return diagnosisListResult;
	}

	public WSClientServiceImpl getWsClientService() {
		return wsClientService;
	}

	public void setWsClientService(WSClientServiceImpl wsClientService) {
		this.wsClientService = wsClientService;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
