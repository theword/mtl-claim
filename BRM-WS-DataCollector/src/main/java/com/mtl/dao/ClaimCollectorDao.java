package com.mtl.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import com.mtl.LogMonitor;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;
import com.mtl.ws.bean.PlanDto;

public interface ClaimCollectorDao {
	
	public List<com.mtl.benefitAnalysis.model.mtlclaim.Benefit> buildBenefitsForClaim(com.mtl.benefitAnalysis.model.mtlclaim.Claim claim, String clientNumber,List<String> claimNumbers,Connection lof4claimConnection);
	public List<com.mtl.continuity.model.mtlclaim.Claim> buildRelatedClaims(String clientId, com.mtl.continuity.model.enumerator.ClaimType claimType, 
			Date admissionDate, Date incidentDate, Date dischargeDate, List<com.mtl.continuity.model.mtlclaim.Diagnosis> requestDiagnosis,String callName,Connection lof4claimConnection);
	public List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> buildContinuityClaim(List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> continuityClaims,Connection lof4claimConnection, String callName);
	public com.mtl.benefitAnalysis.model.mtlclaim.Coverage buildCoverageFromCoverageHistory(com.mtl.benefitAnalysis.model.mtlclaim.Coverage coverage,String clientNumber, String policyNumber, Date requestDate, List<String> claimNumbers,Connection lof4claimConnection);
	public com.mtl.eligible.model.mtlclaim.Coverage buildCoverageFromLOF4CLM(final String covCode,final String covStatus,final Date issueDate,Connection lof4claimConnection);
	public com.mtl.eligible.model.mtlclaim.Policy updatePolicy(Date requestDate, final List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosis,com.mtl.eligible.model.mtlclaim.Policy policy,String callName,com.mtl.eligible.model.enumerator.ClaimType claimType, Connection lof4claimConnection, Connection lof4posConnection);
	public HospitalBlacklistModel buildHospitalClaim(String hospitalCode,Connection lof4claimConnection);
	public void setLogMonitor(LogMonitor logMonitor);
	public void getCoveragesPlanCode(com.mtl.eligible.model.mtlclaim.Coverage coverage, Connection lof4claimConnection);
	public PlanDto getPlanDto(String planCode, String callName, com.mtl.eligible.model.enumerator.ClaimType claimType, Connection lof4claimConnection);

	public List<com.mtl.eligible.model.mtlclaim.Coverage> buildCoverageListFromLOF4CLM(List<com.mtl.eligible.model.mtlclaim.Coverage> coverageList, Connection lof4claimConnection);
}
