/*
 * 
 */
package com.mtl.utils;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The Class ApplicationContextUtils.
 */
public class ApplicationContextUtils implements ApplicationContextAware {

	/** The ctx. */
	private static ApplicationContext ctx;
	//private static Logger log = Logger.getLogger(ApplicationContextUtils.class);
	
	/**
	 * Gets the bean.
	 *
	 * @param name the name
	 * @return the bean
	 */
	public static Object getBean(String name)
	{
		if(ctx==null){
			initialize();
		}
		
		return ctx.getBean(name);
	}

	/**
	 * Initialize.
	 */
	protected static void initialize(){
		
		ctx = new ClassPathXmlApplicationContext(
				new String[]{
						"applicationContext-aop.xml",
						"applicationContext-resource.xml"
						, "applicationContext-jdbc-dao.xml"
						, "applicationContext-dao.xml"
						, "applicationContext-service.xml"});
	}

	 
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	public void setApplicationContext(ApplicationContext appCtx)
			throws BeansException {
		this.ctx = appCtx;
	}

}
