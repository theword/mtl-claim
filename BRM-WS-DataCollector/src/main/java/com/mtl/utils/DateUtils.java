/*
 * 
 */
package com.mtl.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import com.mtl.constants.DataCollConstants;


/**
 * The Class DateUtils.
 */
public class DateUtils {

    /** The log. */
    private static Log log = LogFactory.getLog(DateUtils.class);
    
    /** The Constant THAI_LOCALE. */
    private static final Locale THAI_LOCALE = new Locale("th", "TH");
    
    /** The time pattern. */
    private static String timePattern = "HH:mm";
    
    /** The hhmm time pattern. */
    private static String hhmmTimePattern = "HH:mm";
    
    /** The hhmmss time pattern. */
    private static String hhmmssTimePattern = "HH:mm:ss";
    
    /** The hhmmss s time pattern. */
    private static String hhmmssSTimePattern = "HH:mm:ss.S";
    
    /** The dd m myyyy pattern. */
    private static String ddMMyyyyPattern = "ddMMyyyy";
    
    /** The dd m myyyy with slash pattern. */
    private static String ddMMyyyyWithSlashPattern = "dd/MM/yyyy";
    
    /** The yyyy m mdd. */
    private static String yyyyMMdd="yyyyMMdd"; 
    
    /** The Constant MILLISECONDS_PER_DAY. */
    public static final long MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;


    /**
     * Return default datePattern (yyyyMMdd HHmmss) if date.format not exists
     * @return a string representing the date pattern on the UI
     */
    public static String getDatePattern() {
        Locale locale = LocaleContextHolder.getLocale();
        String defaultDatePattern;
        try {
            defaultDatePattern = ResourceBundle.getBundle(DataCollConstants.BUNDLE_KEY, locale)
                .getString("date.format");
        } catch (MissingResourceException mse) {
            defaultDatePattern = "yyyyMMdd HHmmss";
        }
        
        return defaultDatePattern;
    }
    
    /**
     * Gets the date time pattern.
     *
     * @return the date time pattern
     */
    public static String getDateTimePattern() {
        return DateUtils.getDatePattern() + " " + hhmmssTimePattern;
    }

    /**
     * This method attempts to convert an Oracle-formatted date
     * in the form dd-MMM-yyyy to dd/MM/yyyy.
     *
     * @param aDate date from database as a string
     * @return formatted string for the ui
     */
    public static String getDate(Date aDate) {
        SimpleDateFormat df;
        String returnValue = "";

        if (aDate != null) {
            df = new SimpleDateFormat(getDatePattern());
            returnValue = df.format(aDate);
        }

        return (returnValue);
    }

    /**
     * This method generates a string representation of a date/time
     * in the format you specify on input.
     *
     * @param aMask the date pattern the string is in
     * @param strDate a string representation of a date
     * @return a converted Date object
     * @throws ParseException the parse exception
     * @see java.text.SimpleDateFormat
     */
    public static Date convertStringToDate(String aMask, String strDate)
      throws ParseException {
        SimpleDateFormat df;
        Date date;
        df = new SimpleDateFormat(aMask);

        if (log.isDebugEnabled()) {
            log.debug("converting '" + strDate + "' to date with mask '"
                      + aMask + "'");
        }

        try {
            date = df.parse(strDate);
        } catch (ParseException pe) {
            //log.error("ParseException: " + pe);
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());
        }

        return (date);
    }

    /**
     * This method returns the current time in the format:
     * HH:mm.
     *
     * @param theTime the current time
     * @return the current date/time
     */
    public static String getTimeNow(Date theTime) {
        return getDateTime(hhmmssTimePattern, theTime);
    }

    /**
     * This method returns the current date in the format: dd/MM/yyyy.
     *
     * @return the current date
     * @throws ParseException the parse exception
     */
    public static Calendar getToday() throws ParseException {
        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat(getDatePattern());

        // This seems like quite a hack (date -> string -> date),
        // but it works ;-)
        String todayAsString = df.format(today);
        Calendar cal = new GregorianCalendar();
        cal.setTime(convertStringToDate(todayAsString));

        return cal;
    }

    /**
     * This method generates a string representation of a date's date/time
     * in the format you specify on input.
     *
     * @param aMask the date pattern the string is in
     * @param aDate a date object
     * @return a formatted string representation of the date
     * @see java.text.SimpleDateFormat
     */
    public static String getDateTime(String aMask, Date aDate) {
        SimpleDateFormat df = null;
        String returnValue = "";

        if (aDate == null) {
            log.error("aDate is null!");
        } else {
            df = new SimpleDateFormat(aMask);
            returnValue = df.format(aDate);
        }

        return (returnValue);
    }

    /**
     * This method generates a string representation of a date based
     * on the System Property 'dateFormat'
     * in the format you specify on input.
     *
     * @param aDate A date to convert
     * @return a string representation of the date
     */
    public static String convertDateToString(Date aDate) {
        return getDateTime(getDatePattern(), aDate);
    }

    /**
     * This method converts a String to a date using the datePattern.
     *
     * @param strDate the date to convert (in format dd/MM/yyyy)
     * @return a date object
     * @throws ParseException the parse exception
     */
    public static Date convertStringToDate(String strDate)
      throws ParseException {
        Date aDate = null;

        try {
            if (log.isDebugEnabled()) {
                log.debug("converting date with pattern: " + getDatePattern());
            }

            aDate = convertStringToDate(getDatePattern(), strDate);
        } catch (ParseException pe) {
            log.error("Could not convert '" + strDate
                      + "' to a date, throwing exception");
            pe.printStackTrace();
            throw new ParseException(pe.getMessage(),
                                     pe.getErrorOffset());
                    
        }

        return aDate;
    }
    
    /**
     * Format date.
     *
     * @param date the date
     * @param pattern the pattern
     * @return the string
     */
    public static String formatDate(Date date, String pattern){
    	SimpleDateFormat dateFormate =new  SimpleDateFormat(pattern); 
    	return dateFormate.format(date);
    }
	
    /**
     * Compare.
     *
     * @param date1 the date1
     * @param date2 the date2
     * @return the int
     */
    public static int compare(Date date1, Date date2) {
		return date1.compareTo(date2);
	}

	/**
	 * Before.
	 *
	 * @param date1 the date1
	 * @param date2 the date2
	 * @return true, if successful
	 */
	public static boolean before(Date date1, Date date2) {
		return date1.compareTo(date2) < 0;
	}

	/**
	 * After.
	 *
	 * @param date1 the date1
	 * @param date2 the date2
	 * @return true, if successful
	 */
	public static boolean after(Date date1, Date date2) {
		return date1.compareTo(date2) > 0;
	}

	/**
	 * Between.
	 *
	 * @param date the date
	 * @param start the start
	 * @param end the end
	 * @return true, if successful
	 */
	public static boolean between(Date date, Date start, Date end) {
		if (before(date, start)) {
			return false;
		} else if (after(date, end)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Diff date.
	 *
	 * @param date1 the date1
	 * @param date2 the date2
	 * @return the int
	 */
	public static int diffDate(Date date1, Date date2) {
		long millisecond1 = date1.getTime();
		long millisecond2 = date2.getTime();
		long diffMilliseconds = millisecond2 - millisecond1;
		return (int) (diffMilliseconds / MILLISECONDS_PER_DAY);
	}
	
	/**
	 * Gets the tomorrow date.
	 *
	 * @return the tomorrow date
	 */
	public static Date getTomorrowDate() {
		Calendar calendar = Calendar.getInstance();
		DateUtils.removeTime(calendar);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		return calendar.getTime();
	}

	/**
	 * Gets the yesterday date.
	 *
	 * @return the yesterday date
	 */
	public static Date getYesterdayDate() {
		Calendar calendar = Calendar.getInstance();
		DateUtils.removeTime(calendar);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		return calendar.getTime();
	}

	/**
	 * Removes the time.
	 *
	 * @param calendar the calendar
	 */
	private static void removeTime(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}
}
