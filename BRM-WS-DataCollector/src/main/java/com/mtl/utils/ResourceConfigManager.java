/*
 * 
 */
package com.mtl.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2547
 * </p>
 * <p>
 * Company: Motif Technology Co., Ltd.
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */

public class ResourceConfigManager {

	/** The Constant log. */
	private final static Logger log = Logger.getLogger(ResourceConfigManager.class);

	/** The prop. */
	private static Properties prop = null;

	/** The Constant CONFIG_FILE. */
	protected final static String CONFIG_FILE = "data-collector-config.properties";
	protected final static String JDBC_FILE = "jdbc.properties";

	static {
		// load configuration data
		try {
			// InputStream
			// in=ResourceConfigManager.class.getClassLoader().getResourceAsStream(CONFIG_FILE);
			InputStream in = getConfigInputStream();

			prop = new Properties();
			prop.load(in);
		}
		catch (Exception e) {
			log.error("Can't load property file " + CONFIG_FILE, e);
			// log.error("Can't load property file " + JDBC_FILE, e);
		} // end try
	}

	/**
	 * Gets the property.
	 * 
	 * @param name
	 *            the name
	 * @return the property
	 */
	public static String getProperty(String name) {
		return (String) prop.get(name);
	}

	/**
	 * Gets the properties.
	 * 
	 * @return the properties
	 */
	public static Properties getProperties() {
		return prop;
	}

	private static InputStream getConfigInputStream() {
		InputStream in = null;
		try {
//			PropertiesUtils propUtils = (PropertiesUtils) ApplicationContextUtils.getBean("propertiesUtil");
//			String path = propUtils.getPathRuleConfig();
			
			File propertyFile = new File("/data/conf/clm-datacollector/" + CONFIG_FILE);
//			File propertyFile = new File("/data/conf/ete/clm-datacollector/"+CONFIG_FILE);
			
//			File propertyFile = new File(path + CONFIG_FILE);
			
			log.info("propertyFile:" + propertyFile);
			if (propertyFile != null && propertyFile.isFile()) {
				log.info("Use config file:" + propertyFile);
				in = new FileInputStream(propertyFile);
			}
			else {
				log.info("Not found config file.Use default config instread.");
				in = ResourceConfigManager.class.getClassLoader().getResourceAsStream(JDBC_FILE);
			}

		}
		catch (Exception e) {
			log.error("Can't load property file " + JDBC_FILE, e);
		} // end try
		return in;
	}
}
