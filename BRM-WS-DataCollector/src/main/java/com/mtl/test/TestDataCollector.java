/*
 * 
 */
package com.mtl.test;

import com.mtl.jdbc.dao.GenericJdbcDao;
import com.mtl.utils.ApplicationContextUtils;

/**
 * The Class TestDataCollector.
 */
public class TestDataCollector {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		System.out.println("========  START TestDataCollector  =========");
		TestDataCollector test = new TestDataCollector();
		System.out.println("------test orcl-----");
		try{
			test.testOrcl();
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("------test Db2-----");
		try{
			test.testDb2();
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("========  END TestDataCollector  =========");
	}
	
	/**
	 * Test orcl.
	 */
	public void testOrcl(){
//		GenericJdbcDao genericJdbcDaoClaim = (GenericJdbcDao)ApplicationContextUtils.getBean("genericJdbcDaoClaim");
//		genericJdbcDaoClaim.getTemplate().execute("select 1 from dual");
	}
	
	/**
	 * Test db2.
	 */
	public void testDb2(){
//		GenericJdbcDao genericJdbcDaoUnderwrite = (GenericJdbcDao)ApplicationContextUtils.getBean("genericJdbcDaoUnderwrite");
//		genericJdbcDaoUnderwrite.getTemplate().execute("select 1 from SYSIBM.SYSDUMMY1");				
	}

}
