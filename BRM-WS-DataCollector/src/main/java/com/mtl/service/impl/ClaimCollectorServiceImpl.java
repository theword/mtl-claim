package com.mtl.service.impl;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationResponse;
import th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_PortType;
import th.co.muangthai.application.ws.getcoveragerelation.www.GetCoverageRelationService_ServiceLocator;
import th.co.muangthai.application.ws.getcoveragerelation.www.HeaderGetCoverageRelationRequest;
import th.co.muangthai.application.ws.getcoveragerelation.www.HeaderGetCoverageRelationResponse;
import th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse;
import th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType;
import th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_ServiceLocator;
import th.co.muangthai.application.ws.getpolicybyclient.www.HeaderGetPolicyByClientRequest;
import th.co.muangthai.application.ws.getpolicybyclient.www.HeaderGetPolicyByClientResponse;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequest;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoRequestPolicy;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponse;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseCoverage;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponseExclusion;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInfoResponsePolicy;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformation;
import th.co.muangthai.application.ws.getpolicyinformation.www.GetPolicyInformationServiceLocator;

import com.mtl.LogMonitor;
import com.mtl.benefitAnalysis.dto.BenefitAnalysisDto;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisModel;
import com.mtl.constants.WSConstants;
import com.mtl.continuity.dto.ContinuityClaimCheckingDto;
import com.mtl.continuity.model.xom.mtlclaim.ContinuityClaimCheckingModel;
import com.mtl.dao.ClaimCollectorDao;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.hospital.dto.HospitalBlacklistDto;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;
import com.mtl.service.ClaimCollectorService;
import com.mtl.ws.bean.PlanDto;
import com.mtl.ws.service.impl.WSClientServiceImpl;


public class ClaimCollectorServiceImpl implements ClaimCollectorService {
	
	private WSClientServiceImpl wsClientService;
	private String username;
	private String password;
	private String USERNAME;
	private String PASSWORD;
	private String CLAIM_USERNAME;
	private String CLAIM_PASSWORD;
	private ClaimCollectorDao claimCollectorDao;
	private Logger log 			= Logger.getLogger(this.getClass());
	private final int SUCCESS 	= 0;
	private int status;
	
	private LogMonitor logMonitor;
	
	public void setLogMonitor(LogMonitor logMonitor) {
		this.logMonitor = logMonitor;
	}

	public void setClaimCollectorDao(ClaimCollectorDao claimCollectorDao) {
		this.claimCollectorDao = claimCollectorDao;
	}
	
	public ClaimCollectorDao getClaimCollectorDao(){
		return this.claimCollectorDao;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSUCCESS() {
		return SUCCESS;
	}

	public BenefitAnalysisModel collectBenefitAnalysisModel(BenefitAnalysisDto benefitAnalysisDto, String callName) {
		log.debug("---- Collect Benefit Analysis Model ----");
		claimCollectorDao.setLogMonitor(logMonitor);
		wsClientService = new WSClientServiceImpl();
		wsClientService.init();
		setStatus(SUCCESS);
		Connection lof4claimConnection = null;
		// Connection lof4posConnection = null;
		// Statement lof4claimStatement = null;
		// Statement lof4posStatement = null;

		try {
			Context ctxLOF4CLAIM = new InitialContext();
			String jndiLOF4CLAIM = wsClientService.getJndiLOF4CLAIM();
			DataSource lof4ClaimDataSource = (DataSource) ctxLOF4CLAIM.lookup(jndiLOF4CLAIM);
			// Datasource || JNDI
			lof4claimConnection = lof4ClaimDataSource.getConnection();
			// lof4claimStatement = lof4claimConnection.createStatement();

			// Context ctxLOF4POS = new InitialContext();
			// String jndiLOF4POS = wsClientService.getJndiLOF4POS();
			// DataSource lof4posDataSource = (DataSource)
			// ctxLOF4POS.lookup(jndiLOF4POS);

			// Datasource || JNDI
			// lof4posConnection =
			// "cisx".equalsIgnoreCase(wsClientService.getCollectorType()) ?
			// lof4posDataSource.getConnection() : lof4ClaimDataSource
			// .getConnection();
			// lof4posStatement = lof4posConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot Connect LOF4CLAIM Benefit Analysis : " + e.getMessage());
		}

		try {
			prepareBenefits(benefitAnalysisDto.getCurrentClaims(), benefitAnalysisDto.getClientID(), benefitAnalysisDto.getClaimNumbers(),
					lof4claimConnection);
			if (benefitAnalysisDto.getContinuityClaim() != null)
				log.debug("----- Continuity Claim size: " + benefitAnalysisDto.getContinuityClaim().size());
			else
				log.debug("----- Continuity claim is null ");
			BenefitAnalysisModel benefitAnalysisModel = new BenefitAnalysisModel(benefitAnalysisDto.getBenefitGroups(), prepareAvailableBenefit(
					prepareFixedClaim(benefitAnalysisDto.getCurrentClaims(), benefitAnalysisDto.getFixedClaims(), benefitAnalysisDto.getClientID(),
							benefitAnalysisDto.getClaimNumbers(), lof4claimConnection), benefitAnalysisDto.getContinuityClaim(),
					benefitAnalysisDto.getBenefitGroups(), lof4claimConnection, callName), benefitAnalysisDto.getFixedClaims());
			try {
				// lof4claimStatement.close();
				lof4claimConnection.close();
				// lof4posStatement.close();
				// lof4posConnection.close();
			}
			catch (Exception e) {
				log.error("[Error Closed Connection] getLOF4CLAIM Benefit Analysis : " + e.getMessage());
				log.error(ExceptionUtils.getStackTrace(e));
			}
			return benefitAnalysisModel;
		}
		catch (Exception e) {
			e.printStackTrace();
			BenefitAnalysisModel benefitAnalysisModel = new BenefitAnalysisModel(null, null, null);
			benefitAnalysisModel.setStatus(-1);
			return benefitAnalysisModel;
		}

	}

	private void prepareBenefits(List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> currentClaims, String clientNumber, List<String> claimNumbers,
			Connection lof4claimConnection) {
		log.debug("---- Prepare Benefits ----");
		for (com.mtl.benefitAnalysis.model.mtlclaim.Claim currentClaim : currentClaims) {
			Date requestDate = null;
			if (currentClaim.getClaimType() == com.mtl.benefitAnalysis.model.enumerator.ClaimType.ACCIDENT){
				requestDate = currentClaim.getIncidentDate();
			}
			else{
				requestDate = currentClaim.getAdmissionDate();
			}
			currentClaim.setCoverage(claimCollectorDao.buildCoverageFromCoverageHistory(currentClaim.getCoverage(), clientNumber,
					currentClaim.getPolicyNumber(), requestDate, claimNumbers, lof4claimConnection));
			currentClaim.getCoverage().getCoverageHistoryID();
			currentClaim.setBenefits(claimCollectorDao.buildBenefitsForClaim(currentClaim, clientNumber, claimNumbers, lof4claimConnection));
		}
		// add by TeTe 23/07/2012 check sumInsured and faceAmount
		for (com.mtl.benefitAnalysis.model.mtlclaim.Claim currentClaim : currentClaims) {
			for (com.mtl.benefitAnalysis.model.mtlclaim.Benefit currentBenefit : currentClaim.getBenefits()) {
				if (currentBenefit.getSumInsured() > 0.00)
					;
				else
					currentBenefit.setSumInsured(currentClaim.getCoverage().getFaceAmount());
			}
		}

	}

	private List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> prepareAvailableBenefit(
			List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> currentClaims, List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> continuityClaims,
			List<com.mtl.benefitAnalysis.model.mtlclaim.BenefitGroup> benefitGroups, Connection lof4claimConnection, String callName) {
		log.debug("---- Prepare Available Benefit ----");
		for (com.mtl.benefitAnalysis.model.mtlclaim.BenefitGroup benefitGroup : benefitGroups)
			benefitGroup.setBilledMaxPercent(benefitGroup.getBilledAmountPercent());
		if (continuityClaims == null) {
			log.debug("- Continuity claim is null -");
			return currentClaims;
		}
		if (continuityClaims.size() == 0) {
			log.debug("- Continuity claim is empty -");
			return currentClaims;
		}
		continuityClaims = claimCollectorDao.buildContinuityClaim(continuityClaims, lof4claimConnection, callName);

		for (com.mtl.benefitAnalysis.model.mtlclaim.Claim currentClaim : currentClaims) {
			for (com.mtl.benefitAnalysis.model.mtlclaim.Claim continuityClaim : continuityClaims) {
				if (currentClaim.getPolicyNumber().equals(continuityClaim.getPolicyNumber())
						&& currentClaim.getCoverage().getCoverageCode().equals(continuityClaim.getCoverage().getCoverageCode())
						&& currentClaim.getCoverage().getTrailerNumber().equals(continuityClaim.getCoverage().getTrailerNumber())) {
					currentClaim.setReferenceClaimGroupID(continuityClaim.getReferenceClaimGroupID());

					for (com.mtl.benefitAnalysis.model.mtlclaim.Benefit continuityBenefit : continuityClaim.getBenefits()) {
						for (com.mtl.benefitAnalysis.model.mtlclaim.Benefit currentBenefit : currentClaim.getBenefits()) {
							if (!currentBenefit.getBenefitGroup().equals("OD1")) {
								if (currentBenefit.getBenefitCode().equals(continuityBenefit.getBenefitCode())) {
									double totalAmount = continuityBenefit.getTotalAmount();
									int totalUseDay = continuityBenefit.getTotalUsedDays();
									totalAmount = (totalAmount > 0) ? totalAmount : 0;
									totalUseDay = (totalUseDay > 0) ? totalUseDay : 0;
									double availableAmount = currentBenefit.getAvailableAmount();
									double availablePercent = currentBenefit.getAvailablePercent();
									double continuityAmount = currentBenefit.getContinuityClaimAmount();
									double sumContinuityPercent = currentBenefit.getSumContinuityPercent();
									int availableDays = currentBenefit.getAvailableDays();

									boolean flag = !((currentBenefit.getBenefitGroup().equals("ICU1") || currentBenefit.getBenefitGroup().equals(
											"RB1")) && (currentClaim.getCoverage().getCoverageGroupCode().equals("RHG")));

									if (flag) {
										currentBenefit.setAvailableAmount((availableAmount > totalAmount) ? availableAmount - totalAmount : 0);
										currentBenefit.setContinuityClaimAmount(continuityAmount + totalAmount);
										currentClaim.getCoverage().calculateMaxAmount(totalAmount);
									}

									currentBenefit.setAvailableDays((availableDays > totalUseDay) ? availableDays - totalUseDay : 0);
									if (currentClaim.getClaimType() == com.mtl.benefitAnalysis.model.enumerator.ClaimType.ACCIDENT)
										currentBenefit.setAvailablePercent((availablePercent > sumContinuityPercent) ? availablePercent
												- sumContinuityPercent : 0);

									currentBenefit.setContinuityClaimDay(currentBenefit.getContinuityClaimDay() + totalUseDay);
									currentBenefit.setContinuityClaimPercent((float) continuityBenefit.getClaimPercent());
									currentBenefit.setContinuityDeDuctAmount(continuityBenefit.getTotalClaimDeDuct());

								}
							}
						}

						if (currentClaim.getCoverage().getMaxAmount() < 0.0)
							currentClaim.getCoverage().setMaxAmount(0.0);
						currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAmount());

						for (com.mtl.benefitAnalysis.model.mtlclaim.BenefitGroup benefitGroup : benefitGroups) {
							if (compareBenefitGroup(continuityBenefit.getBenefitGroup(), benefitGroup.getBenefitGroupCode())) {
								double totalAmount = continuityBenefit.getTotalAmount();
								int totalUseDay = continuityBenefit.getTotalUsedDays();
								double totalDeDuct = continuityBenefit.getTotalDDuctAmount();
								totalAmount = (totalAmount > 0) ? totalAmount : 0;
								totalUseDay = (totalUseDay > 0) ? totalUseDay : 0;
								totalDeDuct = (totalDeDuct > 0) ? totalDeDuct : 0;

								benefitGroup.setContinuityAmount(benefitGroup.getContinuityAmount() + totalAmount);
								benefitGroup.setContinuityAmountDays(benefitGroup.getContinuityAmountDays() + totalUseDay);
								benefitGroup.setContinuityAmountPercent(Math.max(benefitGroup.getContinuityAmountPercent(),
										(float) continuityBenefit.getClaimPercent()));

								benefitGroup.setContinuityDeDuct(benefitGroup.getContinuityDeDuct() + totalDeDuct);

								benefitGroup.setBilledMaxPercent(Math.max(benefitGroup.getBilledMaxPercent(),
										(float) continuityBenefit.getClaimPercent()));
							}
						}
					}
				}
			}
		}
		log.debug("---- Prepare finish ----");
		return currentClaims;
	}

	private boolean compareBenefitGroup(String benefit1, String benefit2){
		if(benefit1.equals(benefit2))return true;
		if((benefit1.equals("ANS1") || benefit1.equals("XANS")) && benefit2.equals("ANS2"))return true;
		if((benefit1.equals("XSF") || benefit1.equals("SF2")) && benefit2.equals("SF1"))return true;
		if(benefit1.equals("XDV") && benefit2.equals("DV1"))return true;
		if(benefit1.equals("XER") && benefit2.equals("ER1"))return true;
		if(benefit1.equals("XOR") && benefit2.equals("OR1"))return true;
		return false;
	}

	private List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> prepareFixedClaim(List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> currentClaims,
			List<com.mtl.benefitAnalysis.model.mtlclaim.Claim> fixedClaims, String clientNumber, List<String> claimNumbers,
			Connection lof4claimConnection) {
		log.debug("---- Prepare Fixed Claim ----");
		if (fixedClaims == null) {
			log.debug("- Fixed claim is null -");
			return currentClaims;
		}
		for (com.mtl.benefitAnalysis.model.mtlclaim.Claim currentClaim : currentClaims) {
			for (com.mtl.benefitAnalysis.model.mtlclaim.Claim fixedClaim : fixedClaims) {
				Date requestDate = null;
				if (fixedClaim.getClaimType() == com.mtl.benefitAnalysis.model.enumerator.ClaimType.ACCIDENT){
					requestDate = fixedClaim.getIncidentDate();
				}
				else{
					requestDate = fixedClaim.getAdmissionDate();
				}

				fixedClaim.setCoverage(claimCollectorDao.buildCoverageFromCoverageHistory(fixedClaim.getCoverage(), clientNumber,
						fixedClaim.getPolicyNumber(), requestDate, claimNumbers, lof4claimConnection));

				if (currentClaim.getPolicyNumber().equals(fixedClaim.getPolicyNumber())
						&& currentClaim.getCoverage().getCoverageCode().equals(fixedClaim.getCoverage().getCoverageCode())) {
					for (com.mtl.benefitAnalysis.model.mtlclaim.Benefit currentBenefit : currentClaim.getBenefits()) {
						for (com.mtl.benefitAnalysis.model.mtlclaim.Benefit fixedBenefit : fixedClaim.getBenefits()) {
							if (currentBenefit.getBenefitCode().equals(fixedBenefit.getBenefitCode())) {
								currentBenefit.printFixedInput();
								fixedBenefit.printFixedInput();
								currentBenefit.getFixedAmounts().addAll(fixedBenefit.getClaimAmounts());
								currentBenefit.getFixedDays().addAll(fixedBenefit.getClaimDays());
								currentBenefit.getFixedInput().addAll(fixedBenefit.getFixedInput());
								currentBenefit.getFixedPercents().addAll(fixedBenefit.getClaimPercents());
								currentBenefit.fixed();
								currentBenefit.fixedUncalculated();
								currentBenefit.printFixedInput();
							}
						}
					}
				}
			}
		}
		return currentClaims;
	}

	public EligibleModel collectEligibleModel(EligibleDto eligibleDto, String callName) {
		log.debug("---- Collect Eligible Model ----");
		claimCollectorDao.setLogMonitor(logMonitor);
		log.debug("ClaimType  : " + eligibleDto.getClaimType());
		log.debug("ClaimCause : " + eligibleDto.getClaimCause());
		wsClientService = new WSClientServiceImpl();
		wsClientService.init();

		username = wsClientService.getUsername();
		password = wsClientService.getPassword();
		USERNAME = wsClientService.getUSERNAME();
		PASSWORD = wsClientService.getPASSWORD();
		CLAIM_USERNAME = wsClientService.getCLAIM_USERNAME();
		CLAIM_PASSWORD = wsClientService.getCLAIM_PASSWORD();

		setStatus(SUCCESS);
		Connection lof4claimConnection = null;
		Connection lof4posConnection = null;
		// Statement lof4claimStatement = null;
		// Statement lof4posStatement = null;

		try {
			Context ctxLOF4CLAIM = new InitialContext();
			String jndiLOF4CLAIM = wsClientService.getJndiLOF4CLAIM();
			DataSource lof4ClaimDataSource = (DataSource) ctxLOF4CLAIM.lookup(jndiLOF4CLAIM);
			// Datasource || JNDI
			lof4claimConnection = lof4ClaimDataSource.getConnection();
			// lof4claimStatement = lof4claimConnection.createStatement();

			 Context ctxLOF4POS = new InitialContext();
			 String jndiLOF4POS = wsClientService.getJndiLOF4POS();
			 DataSource lof4posDataSource = (DataSource) ctxLOF4POS.lookup(jndiLOF4POS);

			 lof4posConnection = lof4posDataSource.getConnection();
			 
			// Datasource || JNDI
			// lof4posConnection =
			// "cisx".equalsIgnoreCase(wsClientService.getCollectorType()) ?
			// lof4posDataSource.getConnection() : lof4ClaimDataSource
			// .getConnection();
			// lof4posStatement = lof4posConnection.createStatement();
		}
		catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot Connect LOF4CLAIM Eligible : " + e.getMessage());
		}

		Date requestDate = null;
		if (eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT) {
			log.debug("Accident case claim");
			requestDate = eligibleDto.getIncidentDate();
		}
		if (eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.HEALTH) {
			log.debug("Health case claim");
			requestDate = eligibleDto.getAdmissionDate();
		}
		if (eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.DEATH) {
			log.debug("Death case claim");
			requestDate = eligibleDto.getDeathDate();
		}
		if (eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.TPD && "T".equals(eligibleDto.getClaimCause())) {
			log.debug("TPD case TPD");
			requestDate = eligibleDto.getDisabilityDate();
		}
		if (eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.TPD && "D".equals(eligibleDto.getClaimCause())) {
			log.debug("TPD case Death");
			requestDate = eligibleDto.getDeathDate();
		}
		log.debug("---requestDate---:" + requestDate);
		EligibleModel eligibleModel = new EligibleModel(eligibleDto.getRequestDiagnoses(), getPolicyByClient(eligibleDto.getClientID(),
				lof4claimConnection, eligibleDto.getClaimType()), eligibleDto.getClaimType(), eligibleDto.getClaimSource(),
				eligibleDto.getAdmissionDate(), eligibleDto.getIncidentDate(), eligibleDto.getDischargeDate(),
				// ging 15-03-2016
				eligibleDto.getDeathDate(), eligibleDto.getDisabilityDate(), "claim", eligibleDto.getCauseStatusCode(),
				eligibleDto.getCauseStatusSubCode(), eligibleDto.getClaimCause(), eligibleDto.getCodeICD9(), eligibleDto.getDotorLicense());

		if (eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.HEALTH
				|| eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT) {
			if (eligibleModel.getPolicies() != null && eligibleModel.getPolicies().size() > 0) {
				List<com.mtl.eligible.model.mtlclaim.Policy> resultList = new ArrayList<com.mtl.eligible.model.mtlclaim.Policy>();
				for (com.mtl.eligible.model.mtlclaim.Policy policy : eligibleModel.getPolicies()) {
					com.mtl.eligible.model.mtlclaim.Policy tempPolicy = claimCollectorDao.updatePolicy(requestDate,
							eligibleDto.getRequestDiagnoses(), policy, callName, eligibleDto.getClaimType(), lof4claimConnection, lof4posConnection);
					tempPolicy = getCoverageRelation(eligibleDto.getClientID(), tempPolicy, eligibleDto.getClaimType());
					resultList.add(tempPolicy);
				}
				eligibleModel.setPolicies(resultList);
				eligibleModel.setTempPolicies(getTempPolicyByClient(eligibleDto.getClientID(), eligibleDto.getClaimType()));// add
																															// new
																															// by
																															// ging
																															// 18-08-2016
			}
		}
		else {
			if (eligibleModel.getPolicies() != null && eligibleModel.getPolicies().size() > 0) {
				List<com.mtl.eligible.model.mtlclaim.Policy> resultList = new ArrayList<com.mtl.eligible.model.mtlclaim.Policy>();
				for (com.mtl.eligible.model.mtlclaim.Policy policy : eligibleModel.getPolicies()) {
					policy.setTempPolicyStatus(policy.getPolicyStatus());
					policy.setTempPolicySubCodeStatus(policy.getPolicyStatusSubCode());

					log.info("init setTempPolicyStatus = " + policy.getTempPolicyStatus());
					log.info("init setTempPolicySubCodeStatus = " + policy.getTempPolicySubCodeStatus());

					if (StringUtils.isBlank(policy.getTempPolicyStatus())) {
						if (StringUtils.isBlank(policy.getMasterModel().getPolicyCurrentStatusCode())) {
							policy.setTempPolicyStatus("");
							log.info("setTempPolicyStatus = \"\"");
						}
						else {
							policy.setTempPolicyStatus(policy.getMasterModel().getPolicyCurrentStatusCode());
							log.info("setTempPolicyStatus = " + policy.getTempPolicyStatus());
						}
					}
					if (StringUtils.isBlank(policy.getTempPolicySubCodeStatus())) {
						if (StringUtils.isBlank(policy.getMasterModel().getPolicyCurrentSubStatusCode())) {
							policy.setTempPolicySubCodeStatus("");
							log.info("setTempPolicySubCodeStatus = \"\"");
						}
						else {
							policy.setTempPolicySubCodeStatus(policy.getMasterModel().getPolicyCurrentSubStatusCode());
							log.info("setTempPolicySubCodeStatus = " + policy.getTempPolicySubCodeStatus());
						}
					}

					PlanDto planDto;
					for (com.mtl.eligible.model.mtlclaim.Coverage coverage : policy.getCoverages()) {
						if (StringUtils.isBlank(coverage.getCoverageStatus()))
							coverage.getMasterModel().setCoverageStatus("");
						if (StringUtils.isBlank(coverage.getMasterModel().getCoverageHistoryID())) {
							log.debug("getCoverageCurrentStatusCode coverage = " + coverage.getMasterModel().getCoverageCurrentStatusCode());
							coverage.setTempCoverageStatus(coverage.getMasterModel().getCoverageCurrentStatusCode());
						}
						if (StringUtils.isBlank(coverage.getCoverageStatusSubCode())) {
							coverage.getMasterModel().setCoverageStatusSubCode("");
							log.debug("CoverageStatusSubCode : " + coverage.getMasterModel().getCoverageStatusSubCode());
						}

						log.debug("CoverageCode = " + coverage.getCoverageCode());
						log.debug("TempCoverageStatus Final = " + coverage.getTempCoverageStatus());
						log.debug("CoverageStatusSubCode Final = " + coverage.getCoverageStatusSubCode());

						if (StringUtils.isBlank(coverage.getPlanGroupCode()) || StringUtils.isBlank(coverage.getCoverageGroupCode())
								|| StringUtils.isBlank(coverage.getClassOfBusiness()) || StringUtils.isBlank(coverage.getSupplementaryBenefit())
								|| StringUtils.isBlank(coverage.getTypeInsurance()) || StringUtils.isBlank(coverage.getUwGroupKey())
								|| StringUtils.isBlank(coverage.getNewUwGroupKey())) {
							planDto = claimCollectorDao.getPlanDto(coverage.getCoverageCode(), callName, eligibleDto.getClaimType(),
									lof4claimConnection);
							if (planDto != null) {
								if (StringUtils.isBlank(coverage.getPlanGroupCode()))
									coverage.getMasterModel().setPlanGroupCode(planDto.getPlanGroupType());
								if (StringUtils.isBlank(coverage.getCoverageGroupCode()))
									coverage.getMasterModel().setCoverageGroupCode(planDto.getPlanGroupCode());
								if (StringUtils.isBlank(coverage.getPlanName()))
									coverage.getMasterModel().setPlanName(planDto.getPlanName());
								if (StringUtils.isBlank(coverage.getClassOfBusiness()))
									coverage.getMasterModel().setClassOfBusiness(planDto.getClassOfBusiness());
								if (StringUtils.isBlank(coverage.getSupplementaryBenefit()))
									coverage.getMasterModel().setSupplementaryBenefit(planDto.getSupplementaryBenefit());
								if (StringUtils.isBlank(coverage.getUwGroupKey()))
									coverage.getMasterModel().setUwGroupKey(planDto.getUwGroupKey());
								if (StringUtils.isBlank(coverage.getNewUwGroupKey()))
									coverage.getMasterModel().setNewUwGroupKey(planDto.getNewUwGroupKey());
								if (StringUtils.isBlank(coverage.getTypeInsurance()))
									coverage.getMasterModel().setTypeInsurance(planDto.getTypeInsurance());
							}
							else {
								coverage.getCoverage().setWaitingDays(0);
							}
						}
					}

					com.mtl.eligible.model.mtlclaim.Policy tempPolicy = getCoverageRelation(eligibleDto.getClientID(), policy,
							eligibleDto.getClaimType());
					resultList.add(tempPolicy);
				}
				eligibleModel.setPolicies(resultList);
				eligibleModel.setTempPolicies(getTempPolicyByClient(eligibleDto.getClientID(), eligibleDto.getClaimType()));
			}
		}

		eligibleModel.setDayCaseStatus(eligibleDto.getDayCaseStatus());
		eligibleModel.setClientID(eligibleDto.getClientID());// add new by ging
																// 18-08-2016
		try {
			// lof4claimStatement.close();
			lof4claimConnection.close();
			// lof4posStatement.close();
			 lof4posConnection.close();
		}
		catch (Exception e) {
			log.error("[Error Closed Connection] getLOF4CLAIM Eligible : " + e.getMessage());
			log.error(ExceptionUtils.getStackTrace(e));
		}
		return eligibleModel;
	}

	public HospitalBlacklistModel collectHospitalBlacklistModel(HospitalBlacklistDto hospitalBlacklistDto){
		log.debug("---- Collect Hospital Blacklist Model ----");
		claimCollectorDao.setLogMonitor(logMonitor);
		wsClientService = new WSClientServiceImpl();
		wsClientService.init();
		setStatus(SUCCESS);
		Connection lof4claimConnection 	= null;
//		Statement lof4claimStatement	= null;
		
		try {
			Context ctxLOF4CLAIM = new InitialContext();
			String jndiLOF4CLAIM = wsClientService.getJndiLOF4CLAIM();
			DataSource lof4ClaimDataSource = (DataSource) ctxLOF4CLAIM.lookup(jndiLOF4CLAIM);
			//Datasource || JNDI
			lof4claimConnection = lof4ClaimDataSource.getConnection();
//			lof4claimStatement = lof4claimConnection.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot Connect LOF4CLAIM Hospital Blacklist : " + e.getMessage());
		}
		
		HospitalBlacklistModel res = new HospitalBlacklistModel();
		try{
			res = claimCollectorDao.buildHospitalClaim(hospitalBlacklistDto.getHospitalCode(),lof4claimConnection);
		}catch (Exception e){
			res.setStatus(-1);
		}
		try{
//			lof4claimStatement.close();
			lof4claimConnection.close();
		} catch (Exception e) {
			log.error("[Error Closed Connection] getLOF4CLAIM Hospital Blacklist : " + e.getMessage());
			log.error(ExceptionUtils.getStackTrace(e));
		}
		return res;
	}
	
	public ContinuityClaimCheckingModel collectContinuityClaimCheckingModel(ContinuityClaimCheckingDto continuityClaimCheckingDto,String callName){
		log.debug("---- Collect Continuity Claim Checking Model ----");
		claimCollectorDao.setLogMonitor(logMonitor);
		wsClientService = new WSClientServiceImpl();
		wsClientService.init();
		setStatus(SUCCESS);
		Connection lof4claimConnection 	= null;
		Statement lof4claimStatement 	= null;
		
		try {
			Context ctxLOF4CLAIM = new InitialContext();
			String jndiLOF4CLAIM = wsClientService.getJndiLOF4CLAIM();
			DataSource lof4ClaimDataSource = (DataSource) ctxLOF4CLAIM.lookup(jndiLOF4CLAIM);
			
			//Datasource || JNDI
			lof4claimConnection = lof4ClaimDataSource.getConnection();
			lof4claimStatement = lof4claimConnection.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot Connect LOF4CLAIM Continuity Claim : " + e.getMessage());
		}
		
		ContinuityClaimCheckingModel linkedClaimCheckingModel = null;
		try{
			linkedClaimCheckingModel = new ContinuityClaimCheckingModel(continuityClaimCheckingDto.getRequestDiagnoses(), 
											claimCollectorDao.buildRelatedClaims(continuityClaimCheckingDto.getClientID(),
											continuityClaimCheckingDto.getClaimType(), 
											continuityClaimCheckingDto.getAdmissionDate(), 
											continuityClaimCheckingDto.getIncidentDate(), 
											continuityClaimCheckingDto.getDischargeDate(),
											continuityClaimCheckingDto.getRequestDiagnoses(),
											callName,lof4claimConnection), 
											continuityClaimCheckingDto.getClaimType(), 
											continuityClaimCheckingDto.getAdmissionDate(), 
											continuityClaimCheckingDto.getIncidentDate(), 
											continuityClaimCheckingDto.getDischargeDate());
		}catch (Exception e){
			linkedClaimCheckingModel = new ContinuityClaimCheckingModel(null, null, null, null, null, null);
			linkedClaimCheckingModel.setStatus(-1);
		}
		try{
			lof4claimStatement.close();
			lof4claimConnection.close();
		} catch (Exception e) {
			log.error("[Error Closed Connection] getLOF4CLAIM Continuity Claim : " + e.getMessage());
			log.error(ExceptionUtils.getStackTrace(e));
		}
		log.debug("---- finish Collect Continuity Claim Checking Model ----");
		return linkedClaimCheckingModel;
	}

	public EligibleModel collectILifeEligibleModel(EligibleModel eligibleModel){
		claimCollectorDao.setLogMonitor(logMonitor);
		wsClientService = new WSClientServiceImpl();
		wsClientService.init();
		setStatus(SUCCESS);
		Connection lof4claimConnection 	= null;
//		Statement lof4claimStatement 	= null;
		
		try {
			Context ctxLOF4CLAIM = new InitialContext();
			String jndiLOF4CLAIM = wsClientService.getJndiLOF4CLAIM();
			DataSource lof4ClaimDataSource = (DataSource) ctxLOF4CLAIM.lookup(jndiLOF4CLAIM);
			//Datasource || JNDI
			lof4claimConnection = lof4ClaimDataSource.getConnection();
//			lof4claimStatement 	= lof4claimConnection.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			setStatus(-1);
			log.debug("Error Cannot Connect LOF4CLAIM EligibleIL : " + e.getMessage());
		}
		if(eligibleModel != null && eligibleModel.getPolicies() != null && eligibleModel.getPolicies().size() > 0){
			for(com.mtl.eligible.model.mtlclaim.Policy policy:eligibleModel.getPolicies()){
				if(policy.getCoverages() != null && policy.getCoverages().size() > 0){
					List<com.mtl.eligible.model.mtlclaim.Coverage> resultList = claimCollectorDao.buildCoverageListFromLOF4CLM(policy.getCoverages(), lof4claimConnection);
					com.mtl.eligible.model.mtlclaim.Coverage coverage2;
					for(com.mtl.eligible.model.mtlclaim.Coverage coverage : policy.getCoverages()){
						coverage2 = null;
						for(com.mtl.eligible.model.mtlclaim.Coverage result : resultList){
							if(StringUtils.equals(coverage.getCoverageCode(), result.getCoverageCode())){
								coverage2 = result;
								break;
							}
						}
						if(coverage2 != null){
							coverage2.getMasterModel().setTrailerNumber(coverage.getTrailerNumber());
							coverage2.getMasterModel().setFaceAmount(coverage.getFaceAmount());
							coverage2.getMasterModel().setSumInsured(coverage.getMasterModel().getSumInsured());
							coverage2.getMasterModel().setMaturityDate(coverage.getMaturityDate());
							coverage2.getMasterModel().setDateStatusChange(coverage.getMasterModel().getDateStatusChange());
							coverage2.getMasterModel().setVoidingPeriod(coverage.getMasterModel().getVoidingPeriod());
							if(coverage2.getMasterModel().getPlanName() == null){
								coverage2.getMasterModel().setPlanName(" ");
							}
							coverage.setCoverageStatusSubCode(coverage2.getCoverageStatusSubCode());
							coverage.setMasterModel(coverage2.getMasterModel());
						}
						else{
							coverage.getMasterModel().setPlanName(" ");
						}
					}
					
//					for(com.mtl.eligible.model.mtlclaim.Coverage coverage:policy.getCoverages()){
//						if(coverage != null){
//							String coverageCode 	= coverage.getCoverageCode();
//							String coverageStatus 	= coverage.getCoverageStatus();
//							Date   issueDate 		= coverage.getIssuedDate();
//							
//							com.mtl.eligible.model.mtlclaim.Coverage coverage2 = new com.mtl.eligible.model.mtlclaim.Coverage();
//							coverage2 = claimCollectorDao.buildCoverageFromLOF4CLM(coverageCode, coverageStatus, issueDate, lof4claimConnection);
//							if(coverage2!=null){
//								//Set For iLife
//								coverage2.getMasterModel().setTrailerNumber(coverage.getTrailerNumber());
//								coverage2.getMasterModel().setFaceAmount(coverage.getFaceAmount());
//								coverage2.getMasterModel().setSumInsured(coverage.getMasterModel().getSumInsured());
//								coverage2.getMasterModel().setMaturityDate(coverage.getMaturityDate());
//								coverage2.getMasterModel().setDateStatusChange(coverage.getMasterModel().getDateStatusChange());
//								coverage2.getMasterModel().setVoidingPeriod(coverage.getMasterModel().getVoidingPeriod());
//								if(coverage2.getMasterModel().getPlanName()==null)coverage2.getMasterModel().setPlanName(" ");
//								coverage.setCoverageStatusSubCode(coverage2.getCoverageStatusSubCode());
//								coverage.setMasterModel(coverage2.getMasterModel());
//							}
//							//edit by ging set planName is Null form P'x
//							else coverage.getMasterModel().setPlanName(" ");
//						}
//					}//Coverage
				}
			}//Policy
		}
		try{
//			lof4claimStatement.close();
			lof4claimConnection.close();
		} catch (Exception e) {
			log.error("[Error Closed Connection] getLOF4CLAIM EligibleIL : " + e.getMessage());
			log.error(ExceptionUtils.getStackTrace(e));
		}
		return eligibleModel;
	}
    
	private com.mtl.eligible.model.mtlclaim.Policy getCoverageRelation(String clientNumber, com.mtl.eligible.model.mtlclaim.Policy policy, com.mtl.eligible.model.enumerator.ClaimType claimType){
		log.debug("getCoverageRelation, clientNumber : " + clientNumber + ", policyNumber : " + policy.getPolicyNumber());
		GetCoverageRelationService_ServiceLocator coverageRelationLocator = new GetCoverageRelationService_ServiceLocator();
		
		QName coverageRelationQName;
		String coverageRelationEndpoint;
		if(claimType == com.mtl.eligible.model.enumerator.ClaimType.HEALTH || claimType == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT){
			coverageRelationQName = wsClientService.getCoverageRelationQNameNew();
			coverageRelationEndpoint = wsClientService.getCoverageRelationEndpointNew();
		}
		else{
			coverageRelationQName = wsClientService.getCoverageRelationQName();
			coverageRelationEndpoint = wsClientService.getCoverageRelationEndpoint();
		}
		
		log.info("using endpoint = " + coverageRelationEndpoint);
		
		GetCoverageRelationService_PortType coverageRelationPortType;
		try{
			coverageRelationLocator.setGetCoverageRelationWsImplPortEndpointAddress(coverageRelationEndpoint);
			coverageRelationPortType = (GetCoverageRelationService_PortType)coverageRelationLocator.getPort(coverageRelationQName, GetCoverageRelationService_PortType.class);
			if(coverageRelationPortType!=null){
				((Stub) coverageRelationPortType).setTimeout(WSConstants.TIME_OUT);
				((Stub) coverageRelationPortType).setHeader("", username, USERNAME);
				((Stub) coverageRelationPortType).setHeader("", password, PASSWORD);
			}
			HeaderGetCoverageRelationRequest request = new HeaderGetCoverageRelationRequest();
			request.setServiceID("BRMWSDataCollector");
			request.setCallName("getCoverageRelation");
			request.setRequestDate(new Date().toString());
			request.setRespondDate(new Date().toString());
			request.setStatus(1);
			request.setReason("");
			request.setDetail("");
			request.setClientNumber(clientNumber);
			request.setPolicyNumber(policy.getPolicyNumber());
			
			logMonitor.requestLog("SOA-CLM-083_GetCoverageRelationService", policy.getPolicyNumber(), clientNumber, null, null, null);
			HeaderGetCoverageRelationResponse response = coverageRelationPortType.getCoverageRelation(request);
			
			if(response != null){
				logMonitor.responseLog("SOA-CLM-083_GetCoverageRelationService", response.getStatus() + "", response.getReason());
				if(response.getCoverageRelationResponseList() != null && response.getCoverageRelationResponseList().length > 0){
					log.debug("coverage before map coverageRelation : " + policy.getCoverages().size());
					log.debug("GetCoverageRelationResponse : ");
					for(GetCoverageRelationResponse relationResponse:response.getCoverageRelationResponseList()){
						log.debug("client number  : >>" + relationResponse.getClientNumber() + "<<");
						log.debug("policy number  : >>" + relationResponse.getPolicyNumber() + "<<");
						log.debug("trailer number : >>" + relationResponse.getTrailerNumber() + "<<");
						log.debug("relationship   : >>" + relationResponse.getRelationship() + "<<");
						log.debug("coverage nuber : >>" + relationResponse.getCoverageNumber() + "<<");
					}
					if(policy.getCoverages()!=null){
						for(int i=0;i<policy.getCoverages().size();){
							boolean flag = true;
							for(GetCoverageRelationResponse relationResponse:response.getCoverageRelationResponseList()){
								// Separate major and minor mappings by benz 20191201
								if(claimType == com.mtl.eligible.model.enumerator.ClaimType.HEALTH || claimType == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT){
									if(relationResponse.getTrailerNumber().equals(policy.getCoverages().get(i).getTrailerNumber())){
										policy.getCoverages().get(i).setClientType(relationResponse.getRelationship());
										flag = false;
										break;
									}
								}
								else{
									if(relationResponse.getCoverageNumber().equals(policy.getCoverages().get(i).getCoverage().getCoverageNumber())){
										policy.getCoverages().get(i).setClientType(relationResponse.getRelationship());
										flag = false;
										break;
									}
								}
							}
							if(flag)policy.getCoverages().remove(i);
							else i++;
						}
					}
					log.debug("after map coverageRelation : " + policy.getCoverages().size());
					for(com.mtl.eligible.model.mtlclaim.Coverage coverage:policy.getCoverages()){
						log.debug("coverage code  : >>" + coverage.getCoverageCode() + "<<");
						log.debug("trailer number : >>" + coverage.getTrailerNumber() + "<<");
						log.debug("client type    : >>" + coverage.getClientType() + "<<");
						log.debug("classOfBusiness : >>"+ coverage.getClassOfBusiness() + "<<");
						log.debug("supplementaryBenefit : >>"+ coverage.getSupplementaryBenefit() +"<<");
					}
					return policy;
				}else log.debug("getCoverageRelation is null");
			}else{
				logMonitor.responseLog("SOA-CLM-083_GetCoverageRelationService", null, null);
				log.debug("getCoverageRelation is null");
			}
		} catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-083_GetCoverageRelationService", null, e.getMessage());
		} catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-083_GetCoverageRelationService", null, e.getMessage());
		}
		catch (Exception e) {
			log.error(e.getClass().getName(), e);
			logMonitor.responseLog("SOA-CLM-083_GetCoverageRelationService", null, e.getMessage());
		}
		policy.setCoverages(new ArrayList<com.mtl.eligible.model.mtlclaim.Coverage>());
		return policy;
	}
	/**/
	private List<com.mtl.eligible.model.mtlclaim.Policy> getPolicyByClient(String clientNumber, Connection lof4claimConnection, com.mtl.eligible.model.enumerator.ClaimType claimType){
		log.debug("start getPolicyByClient, clientNumber : " + clientNumber);
		GetPolicyByClientService_ServiceLocator policyByClientLocator = new GetPolicyByClientService_ServiceLocator();
		
		QName policyByClientQName;
		String policyByClientEndpoint;
		if(claimType == com.mtl.eligible.model.enumerator.ClaimType.HEALTH || claimType == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT){
			policyByClientQName = wsClientService.getPolicyByClientQNameNew();
			policyByClientEndpoint = wsClientService.getPolicyByClientEndpointNew();
		}
		else{
			policyByClientQName = wsClientService.getPolicyByClientQName();
			policyByClientEndpoint = wsClientService.getPolicyByClientEndpoint();
		}
		
		log.info("using endpoint = " + policyByClientEndpoint);
		
		GetPolicyByClientService_PortType policyByClientPortType;
		try{
			policyByClientLocator.setGetPolicyByClientWsImplPortEndpointAddress(policyByClientEndpoint);
			policyByClientPortType = (GetPolicyByClientService_PortType)policyByClientLocator.getPort(policyByClientQName, GetPolicyByClientService_PortType.class);
			if(policyByClientPortType!=null){
				((Stub) policyByClientPortType).setTimeout(WSConstants.TIME_OUT);
				((Stub) policyByClientPortType).setHeader("", username, USERNAME);
				((Stub) policyByClientPortType).setHeader("", password, PASSWORD);
			}
			HeaderGetPolicyByClientRequest request = new HeaderGetPolicyByClientRequest();
			request.setServiceID("BRMWSDataCollector");
			request.setCallName("getPolicyByClient");
			request.setRequestDate(new Date().toString());
			request.setRespondDate(new Date().toString());
			request.setStatus(1);
			request.setReason("");
			request.setDetail("");
			request.setClientNumber(clientNumber);
			
			logMonitor.requestLog("SOA-CLM-084_GetPolicyByClientService", null, clientNumber, null, null, null);
			HeaderGetPolicyByClientResponse response = policyByClientPortType.getPolicyByClient(request);
			
			if(response != null){
				logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", response.getStatus() + "", response.getReason());
				if(response.getGetPolicyByClientResponses() != null && response.getGetPolicyByClientResponses().length > 0){
					log.debug("size : " + response.getGetPolicyByClientResponses().length);
					List<String> policyNumberList = new ArrayList<String>();
					for(GetPolicyByClientResponse clientResponse:response.getGetPolicyByClientResponses())
						policyNumberList.add(clientResponse.getPolicyNumber().trim());
					List<com.mtl.eligible.model.mtlclaim.Policy> result = getPolicyInformation(clientNumber, policyNumberList, lof4claimConnection, claimType);
					log.debug("end getPolicyByClient");
					if(result!=null && result.size()>0){
						for(com.mtl.eligible.model.mtlclaim.Policy policy:result){
							for(GetPolicyByClientResponse clientResponse:response.getGetPolicyByClientResponses()){
								if(clientResponse.getPolicyNumber().equals(policy.getPolicyNumber())){
									if(clientResponse.getRelationship().equalsIgnoreCase("O") || clientResponse.getRelationship().equalsIgnoreCase("I"))
										policy.setClientType(clientResponse.getRelationship());
									else policy.setClientType("");
									break;
								}
							}
						}
					}
					return result;
				}else log.debug("getPolicyByClientResponseList is null");
			}else{
				logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, null);
				log.debug("getPolicyByClientResponse is null");
			}
		} catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		} catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		}
		catch (Exception e) {
			log.error(e.getClass().getName(), e);
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		}
		log.debug("end getPolicyByClient");
		return null;
	}
	/**/
	
	private List<com.mtl.eligible.model.mtlclaim.Policy> getPolicyInformation(String clientNumber, List<String> policyNumberList,
			Connection lof4claimConnection, com.mtl.eligible.model.enumerator.ClaimType claimType) {
		log.debug("start getPolicyInformation");
		GetPolicyInformationServiceLocator policyInformationLocator = new GetPolicyInformationServiceLocator();

		QName policyInformationQName;
		String policyInformationEndpoint;
		if (claimType == com.mtl.eligible.model.enumerator.ClaimType.HEALTH || claimType == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT) {
			policyInformationQName = wsClientService.getPolicyInformationQNameNew();
			policyInformationEndpoint = wsClientService.getPolicyInformationEndpointNew();
		}
		else {
			policyInformationQName = wsClientService.getPolicyInformationQName();
			policyInformationEndpoint = wsClientService.getPolicyInformationEndpoint();
		}

		log.info("using endpoint = " + policyInformationEndpoint);

		GetPolicyInformation policyInformationPortType;
		try {
			policyInformationLocator.setGetPolicyInformationWsImplPortEndpointAddress(policyInformationEndpoint);
			policyInformationPortType = (GetPolicyInformation) policyInformationLocator.getPort(policyInformationQName, GetPolicyInformation.class);
			if (policyInformationPortType != null) {
				((Stub) policyInformationPortType).setTimeout(WSConstants.TIME_OUT);
				((Stub) policyInformationPortType).setHeader("", username, CLAIM_USERNAME);
				((Stub) policyInformationPortType).setHeader("", password, CLAIM_PASSWORD);
			}
			GetPolicyInfoRequest request = new GetPolicyInfoRequest();
			request.setServiceID("BRMWSDataCollector");
			request.setCallName("getPolicyInformation");
			request.setRequestDate(new Date().toString());
			request.setRespondDate(new Date().toString());
			request.setStatus(1);
			request.setReason("");
			request.setDetail("");

			GetPolicyInfoRequestPolicy[] requestPolicies = new GetPolicyInfoRequestPolicy[policyNumberList.size()];
			String policyStr = "";
			for (int i = 0; i < policyNumberList.size(); i++) {
				requestPolicies[i] = new GetPolicyInfoRequestPolicy();
				requestPolicies[i].setClientNumber(clientNumber);
				requestPolicies[i].setPolicyNumber(policyNumberList.get(i));
				policyStr = policyStr + policyNumberList.get(i) + ",";
			}
			log.debug("clientNumber : " + clientNumber);
			log.debug("policyStr    : " + policyStr);

			request.setGetPolicyInfoRequestList(requestPolicies);

			logMonitor.requestLog("SOA_CLM-016_GetPolicyInformationService", policyStr, clientNumber, null, null, null);
			GetPolicyInfoResponse policyInfoResponse = policyInformationPortType.getPolicyInformation(request);

			if (policyInfoResponse != null) {
				logMonitor
						.responseLog("SOA_CLM-016_GetPolicyInformationService", policyInfoResponse.getStatus() + "", policyInfoResponse.getReason());
				if (policyInfoResponse.getGetPolicyInfoResponsePolicyList() != null
						&& policyInfoResponse.getGetPolicyInfoResponsePolicyList().length > 0) {
					log.debug("size : " + policyInfoResponse.getGetPolicyInfoResponsePolicyList().length);
					List<com.mtl.eligible.model.mtlclaim.Policy> policyList = new ArrayList<com.mtl.eligible.model.mtlclaim.Policy>();
					for (GetPolicyInfoResponsePolicy response : policyInfoResponse.getGetPolicyInfoResponsePolicyList()) {
						com.mtl.eligible.model.mtlclaim.master.PolicyMs policyMs = new com.mtl.eligible.model.mtlclaim.master.PolicyMs("",
								response.getPolicyNumber(), "", response.getPlanCode(), 0.0, 0, stringToDate(response.getIssueDate()),
								stringToDate(response.getReInStatementDate()), null, null);
						policyMs.setReInStatementDate(stringToDate(response.getReInStatementDate()));
						policyMs.setPolicyCurrentStatusCode(response.getPOCurrentStatusCode());
						policyMs.setPolicyCurrentSubStatusCode(response.getPoCurrentSubStatusCode());
						policyMs.setPoDateStatusChange(stringToDate(response.getPoDateStatusChange()));
						policyMs.setPolicyComment(response.getPolicyComment());
						policyMs.setModeOfPremium(response.getModeOfPremium() != null ? Integer.parseInt(response.getModeOfPremium()) : null);
						policyMs.setAgentBranch(response.getAgentBranch());
						policyMs.setAgentCode(response.getAgentCode());
						policyMs.setAgentPlacement(response.getAgentPlacement());
						policyMs.setPaymentMode(response.getPaymentMode());
						policyMs.setMailingAddress(response.getMailingAddress());
						policyMs.setCity(response.getCity());
						policyMs.setPostalCode(response.getPostalCode());
						policyMs.setBankIndicator(response.getBankIndicator() != null ? Integer.parseInt(response.getBankIndicator()) : null);
						policyMs.setBankAccountNumber(response.getBankAccountNumber());
						policyMs.setAddressIndicator(response.getAddressIndicator() != null ? Integer.parseInt(response.getAddressIndicator()) : null);
						policyMs.setOwnerClientNumber(response.getOwnerClientNumber());
						policyMs.setApprovedDate(stringToDate(response.getApprovedDate()));
						policyMs.setRelationship(response.getRelationship());
						policyMs.setVoidingPeriod(response.getVoidingPeriod());
						policyMs.setPlanDesc(response.getPlanDesc());
						policyMs.setPolicyGroupType("Ordinary");// add new by
																// ging
																// 18-08-2016
						policyMs.setClaim(true);// add new by ging 19-08-2016
												// fagCliam
						policyMs.setOldStatusCode("");
						log.debug("response.getReInStatementDate : " + response.getReInStatementDate());
						log.debug("policyMs.getReInStatementDate : " + policyMs.getReInStatementDate());
						log.debug("policy code : " + policyMs.getPlanCode());
						List<com.mtl.eligible.model.mtlclaim.Coverage> coverages = new ArrayList<com.mtl.eligible.model.mtlclaim.Coverage>();
						if (response.getGetPolicyInfoResponseCoverageList() != null) {
							for (GetPolicyInfoResponseCoverage coverage : response.getGetPolicyInfoResponseCoverageList()) {

								log.debug("CoverageCode : " + coverage.getCoverageCode());
								log.debug("IssuedDate   : " + coverage.getIssuedDate());
								log.debug("MaturityDate : " + coverage.getMaturityDate());
								log.debug("CoverageCurrentStatusCode    : " + coverage.getCoCurrentStatusCode());
								log.debug("CoverageCurrentStatusSubCode : " + coverage.getCoCurrentStatusSubCode());
								log.debug("DateStatusChange : " + coverage.getCoDateStatusChange());
								log.debug("FaceAmount    : " + coverage.getFaceAmount());
								log.debug("TrailerNumber : " + coverage.getTrailerNumber());
								log.debug("setSumInsured : " + coverage.getSumInsured());
								log.debug("LifeAnnualPremium : " + coverage.getLifeAnnualPremium());
								log.debug("ExhibitReinDate   : " + coverage.getExhibitReinDate());
								log.debug("WpMultiplier   : " + coverage.getWpMultiplier());
								log.debug("CoverageNumber   : " + coverage.getCoverageNumber());

								com.mtl.eligible.model.mtlclaim.master.CoverageMs coverageMs = new com.mtl.eligible.model.mtlclaim.master.CoverageMs();
								coverageMs.setCoverageCode(coverage.getCoverageCode());
								coverageMs.setCoverageNumber(coverage.getCoverageNumber());
								coverageMs.setIssuedDate(stringToDate(coverage.getIssuedDate()));
								coverageMs.setMaturityDate(stringToDate(coverage.getMaturityDate()));
								coverageMs.setCoverageCurrentStatusCode(coverage.getCoCurrentStatusCode());
								coverageMs.setCoverageCurrentStatusSubCode(coverage.getCoCurrentStatusSubCode());
								coverageMs.setDateStatusChange(stringToDate(coverage.getCoDateStatusChange()));
								coverageMs.setFaceAmount((coverage.getFaceAmount() != null) ? coverage.getFaceAmount().doubleValue() : 0.0);
								coverageMs.setTrailerNumber(coverage.getTrailerNumber());
								coverageMs.setSumInsured(coverage.getSumInsured());
								coverageMs.setLifeAnnualPremium((coverage.getLifeAnnualPremium() != null) ? coverage.getLifeAnnualPremium()
										.doubleValue() : 0.0);
								coverageMs.setReInStatementDate(stringToDate(coverage.getExhibitReinDate()));
								coverageMs.setVoidingPeriod(coverage.getVoidingPeriod());

								log.debug("CoverageNumber After: " + coverageMs.getCoverageNumber());
								log.debug("ReInStatementDate After: " + coverageMs.getReInStatementDate());
								log.debug("-----------------------------------------------------");

								List<com.mtl.eligible.model.mtlclaim.Diagnosis> diagnosisList = new ArrayList<com.mtl.eligible.model.mtlclaim.Diagnosis>();
								if (coverage.getGetPolicyInfoResponseExclusion() != null) {
									for (GetPolicyInfoResponseExclusion responseExclusion : coverage.getGetPolicyInfoResponseExclusion()) {
										com.mtl.eligible.model.mtlclaim.master.DiagnosisMs diagnosisMs = new com.mtl.eligible.model.mtlclaim.master.DiagnosisMs(
												responseExclusion.getExclusionCode(), null);// ging
																							// 28-03-2016
										diagnosisMs.setExclusionIndicator(responseExclusion.getExclusionIndicator());
										com.mtl.eligible.model.mtlclaim.Diagnosis diagnosis = new com.mtl.eligible.model.mtlclaim.Diagnosis(
												diagnosisMs);
										diagnosisList.add(diagnosis);
									}
								}
								com.mtl.eligible.model.mtlclaim.Coverage coverageInput = new com.mtl.eligible.model.mtlclaim.Coverage();
								coverageInput.setMasterModel(coverageMs);
								coverageInput.setExcludedDiagnoses(diagnosisList);
								coverageInput.setWpM((coverage.getWpMultiplier() != null) ? coverage.getWpMultiplier().doubleValue() : 0.0);

								claimCollectorDao.getCoveragesPlanCode(coverageInput, lof4claimConnection);

								coverages.add(coverageInput);
							}
						}
						com.mtl.eligible.model.mtlclaim.Policy policy = new com.mtl.eligible.model.mtlclaim.Policy(policyMs, coverages);
						policyList.add(policy);
					}
					log.debug("end getPolicyInformation");
					return policyList;
				}
				else
					log.debug("policyInfoResponse is null");
			}
			else {
				logMonitor.responseLog("SOA_CLM-016_GetPolicyInformationService", null, null);
				log.debug("policyInfoResponse is null");
			}
		}
		catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA_CLM-016_GetPolicyInformationService", null, e.getMessage());
		}
		catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA_CLM-016_GetPolicyInformationService", null, e.getMessage());
		}
		log.debug("end getPolicyInformation");
		return null;
	}
	
	/**/
	private List<String> getTempPolicyByClient(String clientNumber, com.mtl.eligible.model.enumerator.ClaimType claimType){
		log.debug("start getTempPolicyByClient");
		GetPolicyByClientService_ServiceLocator policyByClientLocator = new GetPolicyByClientService_ServiceLocator();
		
		QName policyByClientQName;
		String policyByClientEndpoint;
		if(claimType == com.mtl.eligible.model.enumerator.ClaimType.HEALTH || claimType == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT){
			policyByClientQName = wsClientService.getPolicyByClientQNameNew();
			policyByClientEndpoint = wsClientService.getPolicyByClientEndpointNew();
		}
		else{
			policyByClientQName = wsClientService.getPolicyByClientQName();
			policyByClientEndpoint = wsClientService.getPolicyByClientEndpoint();
		}
		
		log.info("using endpoint = " + policyByClientEndpoint);
		
		GetPolicyByClientService_PortType policyByClientPortType;
		try{
			policyByClientLocator.setGetPolicyByClientWsImplPortEndpointAddress(policyByClientEndpoint);
			policyByClientPortType = (GetPolicyByClientService_PortType)policyByClientLocator.getPort(policyByClientQName, GetPolicyByClientService_PortType.class);
			if(policyByClientPortType!=null){
				((Stub) policyByClientPortType).setTimeout(WSConstants.TIME_OUT);
				((Stub) policyByClientPortType).setHeader("", username, USERNAME);
				((Stub) policyByClientPortType).setHeader("", password, PASSWORD);
			}
			HeaderGetPolicyByClientRequest request = new HeaderGetPolicyByClientRequest();
			request.setServiceID("BRMWSDataCollector");
			request.setCallName("getTempPolicyByClient");
			request.setRequestDate(new Date().toString());
			request.setRespondDate(new Date().toString());
			request.setStatus(1);
			request.setReason("");
			request.setDetail("");
			request.setClientNumber(clientNumber);
			
			logMonitor.requestLog("SOA-CLM-084_GetPolicyByClientService", null, clientNumber, null, null, null);
			HeaderGetPolicyByClientResponse policyByClientResponse = policyByClientPortType.getPolicyByClient(request);
			
			if(policyByClientResponse != null){
				logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", policyByClientResponse.getStatus() + "", policyByClientResponse.getReason());
				if(policyByClientResponse.getGetPolicyByClientResponses() != null && policyByClientResponse.getGetPolicyByClientResponses().length > 0){
					log.debug("size : " + policyByClientResponse.getGetPolicyByClientResponses().length);
					List<String> policyNumberList = new ArrayList<String>();
					for(GetPolicyByClientResponse response:policyByClientResponse.getGetPolicyByClientResponses())
						policyNumberList.add(response.getPolicyNumber());
					log.debug("end getTempPolicyByClient");
					return policyNumberList;
				}else log.debug("getTempPolicyByClientResponseList is null");
			}else{
				logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, null);
				log.debug("getTempPolicyByClientResponse is null");
			}
		} catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		} catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		}
		catch (Exception e) {
			log.error(e.getClass().getName(), e);
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		}
		log.debug("end getTempPolicyByClient");
		return null;
	}
	/**/

	private Date stringToDate(String date){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss" , Locale.US);
		if(date == null || date.equals("null") || date.equals("0"))return null;
		try {
			return format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
