package com.mtl.service.impl;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientResponse;
import th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_PortType;
import th.co.muangthai.application.ws.getpolicybyclient.www.GetPolicyByClientService_ServiceLocator;
import th.co.muangthai.application.ws.getpolicybyclient.www.HeaderGetPolicyByClientRequest;
import th.co.muangthai.application.ws.getpolicybyclient.www.HeaderGetPolicyByClientResponse;
import th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponse;
import th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesResponseList;
import th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesService;
import th.co.muangthai.application.ws.getpolicycoverages.www.GetPolicyCoveragesWsImplPortBindingQSServiceLocator;
import th.co.muangthai.application.ws.getpolicycoverages.www.HeaderGetPolicyCoveragesRequest;

import com.mtl.LogMonitor;
import com.mtl.constants.WSConstants;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.mtlclaim.Coverage;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.service.ILifeCollectorService;
import com.mtl.ws.service.impl.WSClientServiceImpl;
import com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail;
import com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.Getpolicycoveragedetail_client_epLocator;
import com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.Process;
import com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.ProcessResponse;

public class ILifeCollectorServiceImpl implements ILifeCollectorService {
	Logger log 						= Logger.getLogger(this.getClass());
	private final Locale ENG_LOCALE = new Locale("en", "EN");
	private WSClientServiceImpl wsClientService;
	
	private String username;
	private String password;
	private String USERNAME;
	private String PASSWORD;

	private LogMonitor logMonitor;
	
	public void setLogMonitor(LogMonitor logMonitor) {
		this.logMonitor = logMonitor;
	}
	
	public EligibleModel collectEligibleModel(EligibleDto eligibleDto,String callName){
		log.debug("---- Collect Eligible Model ----");
		wsClientService = new WSClientServiceImpl();
		wsClientService.init();
		
		username = wsClientService.getUsername();
		password = wsClientService.getPassword();
		USERNAME = wsClientService.getUSERNAME();
		PASSWORD = wsClientService.getPASSWORD();
		
		Date requestDate = null;
		if(eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT){
			log.debug("Acident case claim");
			requestDate = eligibleDto.getIncidentDate();
		}if(eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.HEALTH){
			log.debug("Health case claim");
			requestDate = eligibleDto.getAdmissionDate();
		}if(eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.DEATH){
			log.debug("Death case claim");
			requestDate = eligibleDto.getDeathDate();
		}if(eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.TPD && "T".equals(eligibleDto.getClaimCause())){
			log.debug("TPD case TPD");
			requestDate = eligibleDto.getDisabilityDate();
		}if(eligibleDto.getClaimType() == com.mtl.eligible.model.enumerator.ClaimType.TPD && "D".equals(eligibleDto.getClaimCause())){
			log.debug("TPD case Death");
			requestDate = eligibleDto.getDeathDate();
		}
		log.debug("request date : " + requestDate);
		EligibleModel eligibleModel = new EligibleModel(eligibleDto.getRequestDiagnoses(), 
											(callName.equals("LOF4CLM_IL"))
													?getPolicyCoverageDetail(eligibleDto.getPolicyNumber())
													:getPolicyCoverages(eligibleDto.getPolicyNumber()),//call service soa
											eligibleDto.getClaimType(), eligibleDto.getClaimSource(), eligibleDto.getAdmissionDate(), 
											eligibleDto.getIncidentDate(), eligibleDto.getDischargeDate(),
											//ging 15-03-2016
											eligibleDto.getDeathDate(), eligibleDto.getDisabilityDate(),"iLife",
											eligibleDto.getCauseStatusCode(),eligibleDto.getCauseStatusSubCode(),
											eligibleDto.getClaimCause(),eligibleDto.getCodeICD9(),eligibleDto.getDotorLicense());
		eligibleModel.setDayCaseStatus(eligibleDto.getDayCaseStatus());
		//add by ging call ws setTempPolicies
		eligibleModel.setClientID(eligibleDto.getClientID());
		if((eligibleModel.getPolicies() != null && eligibleModel.getPolicies().size() > 0) && (StringUtils.isNotBlank(eligibleDto.getClientID())))
			eligibleModel.setTempPolicies(getTempPolicyILByClient(eligibleDto.getClientID(), eligibleDto.getClaimType()));
		return eligibleModel;
	}
	
	private List<com.mtl.eligible.model.mtlclaim.Policy> getPolicyCoverages(String policyNumber){
		log.debug("start getPolicyCoverages, policyNumber : " + policyNumber);
		GetPolicyCoveragesWsImplPortBindingQSServiceLocator getPolicyCoveragesLocator = new GetPolicyCoveragesWsImplPortBindingQSServiceLocator();
		QName policyCoveragesQName		= wsClientService.getPolicyCoveragesQName();
		String policyCoveragesEndpoint 	= wsClientService.getPolicyCoveragesEndpoint();
		GetPolicyCoveragesService getPolicyCoveragesPortType;
		try{
			getPolicyCoveragesLocator.setGetPolicyCoveragesWsImplPortBindingQSPortEndpointAddress(policyCoveragesEndpoint);
			getPolicyCoveragesPortType = (GetPolicyCoveragesService)getPolicyCoveragesLocator.getPort(policyCoveragesQName, GetPolicyCoveragesService.class);
			if(getPolicyCoveragesPortType!=null){
				((Stub) getPolicyCoveragesPortType).setTimeout(WSConstants.TIME_OUT);
				((Stub) getPolicyCoveragesPortType).setHeader("", username, USERNAME);
				((Stub) getPolicyCoveragesPortType).setHeader("", password, PASSWORD);
			}
			HeaderGetPolicyCoveragesRequest request = new HeaderGetPolicyCoveragesRequest();
			request.setServiceID("BRMWSDataCollector");
			request.setCallName("GetPolicyCoverages");
			request.setRequestDate(new Date().toString());
			request.setRespondDate(new Date().toString());
			request.setStatus(1);
			request.setReason("");
			request.setDetail("");
			request.setPolicyNumber(policyNumber);
			
			logMonitor.requestLog("SOA-CLM-085_GetPolicyCoveragesService", policyNumber, null, null, null, null);
			GetPolicyCoveragesResponse response = getPolicyCoveragesPortType.getPolicyCoverages(request);
			
			if(response != null){
				logMonitor.responseLog("SOA-CLM-085_GetPolicyCoveragesService", response.getStatus() + "", response.getReason());
				String format = "yyyy-MM-dd hh:mm:ss";
				if(response.getGetPolicyCoveragesResponseList()!=null && response.getGetPolicyCoveragesResponseList().length > 0){
					com.mtl.eligible.model.mtlclaim.master.PolicyMs policyMs =  new com.mtl.eligible.model.mtlclaim.master.PolicyMs(
									"", 
									response.getGetPolicyCoveragesResponseList(0).getPolicyNumber(), 
									response.getGetPolicyCoveragesResponseList(0).getPolicyStatus(), 
									response.getGetPolicyCoveragesResponseList(0).getPlanCode(),
									response.getGetPolicyCoveragesResponseList(0).getModalPremium(),
									response.getGetPolicyCoveragesResponseList(0).getModeOfPremium(),
									stringToDate(response.getGetPolicyCoveragesResponseList(0).getIssuedDate(),format),
									stringToDate(response.getGetPolicyCoveragesResponseList(0).getBilledToDate(),format),
									stringToDate(response.getGetPolicyCoveragesResponseList(0).getPaidToDate(),format),
									null
									);
					List<com.mtl.eligible.model.mtlclaim.Coverage> coverages = new ArrayList<com.mtl.eligible.model.mtlclaim.Coverage>();
					for(GetPolicyCoveragesResponseList coverageResponse:response.getGetPolicyCoveragesResponseList()){
						com.mtl.eligible.model.mtlclaim.Coverage rec = new com.mtl.eligible.model.mtlclaim.Coverage( 
								new com.mtl.eligible.model.mtlclaim.master.CoverageMs(
										"", 
										coverageResponse.getCoverageCode(), 
										coverageResponse.getCoverageStatus(), 
										stringToDate(coverageResponse.getIssuedDate(),format), 
										null, 
										null, 
										coverageResponse.getTrailerNumber(),
										coverageResponse.getFaceAmount()
									), null);
						coverages.add(rec);
					}
					com.mtl.eligible.model.mtlclaim.Policy policy = new com.mtl.eligible.model.mtlclaim.Policy(policyMs,coverages);
					List<com.mtl.eligible.model.mtlclaim.Policy> policyList = new ArrayList<com.mtl.eligible.model.mtlclaim.Policy>();
					policyList.add(policy);
					log.debug("end getPolicyCoverages");
					return policyList;
				}else log.debug("getPolicyCoveragesResponseList is null");
			}else{
				logMonitor.responseLog("SOA-CLM-085_GetPolicyCoveragesService", null, null);
				log.debug("getPolicyCoveragesResponse is null");
			}
		} catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-085_GetPolicyCoveragesService", null, e.getMessage());
		} catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-085_GetPolicyCoveragesService", null, e.getMessage());
		}
		log.debug("end getPolicyCoverages");
		return null;
	}
	
	private List<com.mtl.eligible.model.mtlclaim.Policy> getPolicyCoverageDetail(String policyNumber){
		log.debug("start getPolicyCoverageDetail, policyNumber : " + policyNumber);
		Getpolicycoveragedetail_client_epLocator policyCoverageDetailLocator = new Getpolicycoveragedetail_client_epLocator();
		QName policyCoverageDetailQName		= wsClientService.getPolicyCoverageDetailQName();
		String policyCoverageDetailEndpoint = wsClientService.getPolicyCoverageDetailEndpoint();
		GetPolicyCoverageDetail policyCoverageDetail;
		try{
			policyCoverageDetailLocator.setGetPolicyCoverageDetail_ptEndpointAddress(policyCoverageDetailEndpoint);
			policyCoverageDetail = (GetPolicyCoverageDetail)policyCoverageDetailLocator.getPort(policyCoverageDetailQName, GetPolicyCoverageDetail.class);
			if(policyCoverageDetail!=null){
				((Stub) policyCoverageDetail).setTimeout(WSConstants.TIME_OUT);
			}
			Process process = new Process();
			process.setPolicyNumber(policyNumber);
			
			logMonitor.requestLog("GetPolicyCoveragesServicePS", policyNumber, null, null, null, null);
			ProcessResponse response = policyCoverageDetail.process(process);
			logMonitor.responseLog("GetPolicyCoveragesServicePS", null, null);
			
			String format = "yyyy-MM-dd";
			List<com.mtl.eligible.model.mtlclaim.Policy> policyList = new ArrayList<com.mtl.eligible.model.mtlclaim.Policy>();
			if(response != null && response.getGetPolicyCoveragesResponseList() != null && response.getGetPolicyCoveragesResponseList().length>0){
				log.debug("response is not null");
				for(com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoveragesResponse policyResponse:response.getGetPolicyCoveragesResponseList()){
					com.mtl.eligible.model.mtlclaim.master.PolicyMs policyMs =  new com.mtl.eligible.model.mtlclaim.master.PolicyMs(
							"", 
							policyResponse.getPolicyNumber(), 
							policyResponse.getPolicyStatus(), 
							StringUtils.isNotBlank(policyResponse.getPlanCode())?policyResponse.getPlanCode().trim():"",
							(StringUtils.isNotBlank(policyResponse.getModalPremium()))?Double.parseDouble(policyResponse.getModalPremium()):0.0,
							(StringUtils.isNotBlank(policyResponse.getModeOfPremium()))?Integer.parseInt(policyResponse.getModeOfPremium()):0,
							stringToDate(policyResponse.getIssuedDate(),format),
							stringToDate(policyResponse.getBilledToDate(),format),
							stringToDate(policyResponse.getPaidToDate(),format),
							null
							);
				
					policyMs.setApprovedDate(stringToDate(policyResponse.getPolicyApprovedDate(),format));
					policyMs.setPoDateStatusChange(stringToDate(policyResponse.getPolicyStatusChangeDate(),format));
					policyMs.setPolicyGroupType("IL");//add new by ging 18-08-2016
					policyMs.setClaim(true);
					List<com.mtl.eligible.model.mtlclaim.Coverage> coverages = new ArrayList<com.mtl.eligible.model.mtlclaim.Coverage>();
					if(policyResponse.getCoveragesDetailList()!=null && policyResponse.getCoveragesDetailList().length>0){
						log.debug("CoveragesDetail is not null");
						BigDecimal sumInsured = null ;
						for(com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.CoveragesDetail res:policyResponse.getCoveragesDetailList()){
							if(res.getSumInsured()!=null)sumInsured = new BigDecimal(res.getSumInsured());
							com.mtl.eligible.model.mtlclaim.master.CoverageMs coverageMs = new com.mtl.eligible.model.mtlclaim.master.CoverageMs();
							coverageMs.setCoverageCode(res.getCoverageCode() != null?res.getCoverageCode().trim():"");
							coverageMs.setCoverageNumber(null);
							coverageMs.setCoverageStatus(res.getCoverageStatus());
							coverageMs.setTrailerNumber(res.getTrailerNumber());
							coverageMs.setFaceAmount(res.getFaceAmount() != null?Double.parseDouble(res.getFaceAmount()):0.0);
							coverageMs.setSumInsured(sumInsured);
							coverageMs.setMaturityDate(stringToDate(res.getMatExpiry(),format));
							coverageMs.setDateStatusChange(stringToDate(res.getCoverageStatusChangeDate(),format));
							coverageMs.setVoidingPeriod(res.getContestablePeriod());
							coverageMs.setIssuedDate(stringToDate(res.getCoverageIssuedDate(),format));
							
							com.mtl.eligible.model.mtlclaim.Coverage coverageInput = new Coverage(coverageMs, null);
							coverageInput.setClientType(res.getClientType());
							
							log.debug("==== Coverage Code  === " + coverageInput.getCoverageCode());
							log.debug("==== Trailer Number === " + coverageInput.getTrailerNumber());
							log.debug("====  Face Amount   === " + coverageInput.getFaceAmount());
							log.debug("====   SumInsured   === " + coverageInput.getMasterModel().getSumInsured());
							log.debug("====  MaturityDate  === " + coverageInput.getMasterModel().getMaturityDate());
							log.debug("====DateStatusChange=== " + coverageInput.getMasterModel().getDateStatusChange());
							log.debug("==== VoidingPeriod  === " + coverageInput.getMasterModel().getVoidingPeriod());
							coverages.add(coverageInput);
						}
					}else log.debug("CoveragesDetail is null");
					com.mtl.eligible.model.mtlclaim.Policy policy = new com.mtl.eligible.model.mtlclaim.Policy(policyMs,coverages);
					policy.setClientType(policyResponse.getClientType());
					policy.setPaymentDate(stringToDate(policyResponse.getPaidToDate(),format));
					policy.setPaidDate(stringToDate(policyResponse.getBilledToDate(),format));
					policyList.add(policy);
				}
				log.debug("end getPolicyCoverageDetail");
				return policyList;
			}
			log.debug("response is null");
			log.debug("end getPolicyCoverageDetail");
			return null;
			/**/
		} catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("GetPolicyCoveragesServicePS", null, e.getMessage());
		} catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("GetPolicyCoveragesServicePS", null, e.getMessage());
		}
		log.debug("end getPolicyCoverageDetail");
		return null;
	}
	/**/
	private List<String> getTempPolicyILByClient(String clientNumber, com.mtl.eligible.model.enumerator.ClaimType claimType){
		log.debug("start getPolicyByClient, clientNumber : " + clientNumber);
		GetPolicyByClientService_ServiceLocator policyByClientLocator = new GetPolicyByClientService_ServiceLocator();
		
		QName policyByClientQName;
		String policyByClientEndpoint;
		if(claimType == com.mtl.eligible.model.enumerator.ClaimType.HEALTH || claimType == com.mtl.eligible.model.enumerator.ClaimType.ACCIDENT){
			policyByClientQName = wsClientService.getPolicyByClientQNameNew();
			policyByClientEndpoint = wsClientService.getPolicyByClientEndpointNew();
		}
		else{
			policyByClientQName = wsClientService.getPolicyByClientQName();
			policyByClientEndpoint = wsClientService.getPolicyByClientEndpoint();
		}
		
		log.info("using endpoint = " + policyByClientEndpoint);
		
		GetPolicyByClientService_PortType policyByClientPortType;
		try{
			policyByClientLocator.setGetPolicyByClientWsImplPortEndpointAddress(policyByClientEndpoint);
			policyByClientPortType = (GetPolicyByClientService_PortType)policyByClientLocator.getPort(policyByClientQName, GetPolicyByClientService_PortType.class);
			if(policyByClientPortType!=null){
				((Stub) policyByClientPortType).setTimeout(WSConstants.TIME_OUT);
				((Stub) policyByClientPortType).setHeader("", username, USERNAME);
				((Stub) policyByClientPortType).setHeader("", password, PASSWORD);
			}
			HeaderGetPolicyByClientRequest request = new HeaderGetPolicyByClientRequest();
			request.setServiceID("BRMWSDataCollector");
			request.setCallName("getPolicyByClient");
			request.setRequestDate(new Date().toString());
			request.setRespondDate(new Date().toString());
			request.setStatus(1);
			request.setReason("");
			request.setDetail("");
			request.setClientNumber(clientNumber);
			
			logMonitor.requestLog("SOA-CLM-084_GetPolicyByClientService", null, clientNumber, null, null, null);
			HeaderGetPolicyByClientResponse response = policyByClientPortType.getPolicyByClient(request);
			
			if(response != null){
				logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", response.getStatus() + "", response.getReason());
				if(response.getGetPolicyByClientResponses()!=null && response.getGetPolicyByClientResponses().length > 0){
					log.debug("size : " + response.getGetPolicyByClientResponses().length);
					List<String> policyNumberList = new ArrayList<String>();
					for(GetPolicyByClientResponse clientResponse:response.getGetPolicyByClientResponses())
						policyNumberList.add(clientResponse.getPolicyNumber());
					log.debug("end getTempPolicyByClient");
					return policyNumberList;
				}else log.debug("getTempPolicyByClientResponseList is null");
			}else{
				logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, null);
				log.debug("getTempPolicyByClientResponse is null");
			}
		} catch (ServiceException e) {
			log.debug("error ServiceException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		} catch (RemoteException e) {
			log.debug("error RemoteException : " + e.getMessage());
			e.printStackTrace();
			logMonitor.responseLog("SOA-CLM-084_GetPolicyByClientService", null, e.getMessage());
		}
		log.debug("end getTempPolicyByClient");
		return null;
	}
	/**/
	private Date stringToDate(String date,String format){
		SimpleDateFormat sf = new SimpleDateFormat(format, ENG_LOCALE);
		if(date != null && !date.trim().equals("")){
			try {
				return sf.parse(date);
			} catch (ParseException e) {
				log.error("Could not parse date,"+e.getMessage(),e);
				e.printStackTrace();
				return null;
			}
		}else return null;
	}
}