package com.mtl.service;

import com.mtl.LogMonitor;
import com.mtl.benefitAnalysis.dto.BenefitAnalysisDto;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisModel;
import com.mtl.continuity.dto.ContinuityClaimCheckingDto;
import com.mtl.continuity.model.xom.mtlclaim.ContinuityClaimCheckingModel;
import com.mtl.dao.ClaimCollectorDao;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.hospital.dto.HospitalBlacklistDto;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;


public interface ClaimCollectorService {	
	public BenefitAnalysisModel collectBenefitAnalysisModel(BenefitAnalysisDto benefitAnalysisDto, String callName);
	public EligibleModel collectEligibleModel(EligibleDto eligibleDto,String callName);
	public HospitalBlacklistModel collectHospitalBlacklistModel(HospitalBlacklistDto hospitalBlacklistDto);
	public ContinuityClaimCheckingModel collectContinuityClaimCheckingModel(ContinuityClaimCheckingDto linkedClaimCheckingDto,String callName);
	public ClaimCollectorDao getClaimCollectorDao();
	public EligibleModel collectILifeEligibleModel(EligibleModel eligibleModel);
	public void setLogMonitor(LogMonitor logMonitor);
}
