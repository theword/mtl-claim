package com.mtl.service;

import com.mtl.LogMonitor;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;

public interface ILifeCollectorService {	
	public EligibleModel collectEligibleModel(EligibleDto eligibleDto,String callName);
	public void setLogMonitor(LogMonitor logMonitor);
}
