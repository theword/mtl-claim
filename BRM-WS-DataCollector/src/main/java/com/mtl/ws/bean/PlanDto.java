package com.mtl.ws.bean;

public class PlanDto {
	
	private String planName;
	private String planGroupCode;
	private String planGroupType;
	private String classOfBusiness;
	private String supplementaryBenefit;
	private String typeInsurance;
	private String uwGroupKey;
	private String newUwGroupKey;
	
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanGroupType() {
		return planGroupType;
	}
	public void setPlanGroupType(String planGroupType) {
		this.planGroupType = planGroupType;
	}
	public String getPlanGroupCode() {
		return planGroupCode;
	}
	public void setPlanGroupCode(String planGroupCode) {
		this.planGroupCode = planGroupCode;
	}
	public String getClassOfBusiness() {
		return classOfBusiness;
	}
	public void setClassOfBusiness(String classOfBusiness) {
		this.classOfBusiness = classOfBusiness;
	}
	public String getSupplementaryBenefit() {
		return supplementaryBenefit;
	}
	public void setSupplementaryBenefit(String supplementaryBenefit) {
		this.supplementaryBenefit = supplementaryBenefit;
	}
	public String getTypeInsurance() {
		return typeInsurance;
	}
	public void setTypeInsurance(String typeInsurance) {
		this.typeInsurance = typeInsurance;
	}
	public String getUwGroupKey() {
		return uwGroupKey;
	}
	public void setUwGroupKey(String uwGroupKey) {
		this.uwGroupKey = uwGroupKey;
	}
	public String getNewUwGroupKey() {
		return newUwGroupKey;
	}
	public void setNewUwGroupKey(String newUwGroupKey) {
		this.newUwGroupKey = newUwGroupKey;
	}
	
}
