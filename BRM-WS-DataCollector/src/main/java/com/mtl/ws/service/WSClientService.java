package com.mtl.ws.service;

import javax.xml.namespace.QName;

public interface WSClientService {
	public void init();
	public String getBulidPoliciesEndpoint();
	public QName  getBulidPoliciesQName();
	public String getBulidCoveragesEndpoint();
	public QName  getBulidCoveragesQName();
	public String getPolicyByClientEndpoint();
	public QName  getPolicyByClientQName();
	public String getPolicyInformationEndpoint();
	public QName  getPolicyInformationQName();
	public String getCoverageRelationEndpoint();
	public QName  getCoverageRelationQName();
	public String getUSERNAME();
	public String getPASSWORD();
	public String getCLAIM_USERNAME();
	public String getCLAIM_PASSWORD();
	public String getUsername();
	public String getPassword();
	public String getJndiLOF4CLAIM();
}
