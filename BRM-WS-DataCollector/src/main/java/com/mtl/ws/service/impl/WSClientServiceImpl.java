package com.mtl.ws.service.impl;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.mtl.utils.ResourceConfigManager;
import com.mtl.ws.service.WSClientService;

public class WSClientServiceImpl implements WSClientService {
	private final Logger log = Logger.getLogger(this.getClass());
	
	private String bulidPoliciesEndpoint;
	private QName bulidPoliciesQName;
	
	private String bulidCoveragesEndpoint;
	private QName bulidCoveragesQName;
	
	private String policyByClientEndpoint;
	private QName policyByClientQName;
	
	private String coverageRelationEndpoint;
	private QName coverageRelationQName;
	
	private String collectorType;
	
	private String USERNAME;
	private String PASSWORD;
	
	static private final String username = "username";
	static private final String password = "password";
	
	private String policyInformationEndpoint;
	private QName policyInformationQName;
	
	private String policyCoveragesEndpoint;
	private QName  policyCoveragesQName;
	
	private String policyCoverageDetailEndpoint;
	private QName  policyCoverageDetailQName;
	
	private String CLAIM_USERNAME;
	private String CLAIM_PASSWORD;
	
	private String jndiLOF4CLAIM;
	private String jndiLOF4POS;
	
	private String policyByClientEndpointNew;
	private QName policyByClientQNameNew;
	
	private String policyInformationEndpointNew;
	private QName policyInformationQNameNew;
	
	private String coverageRelationEndpointNew;
	private QName coverageRelationQNameNew;
	
	public void init(){
		try {
			this.bulidPoliciesEndpoint = ResourceConfigManager.getProperty("bulidPolicies.endpoint");
			this.bulidPoliciesQName = new QName(ResourceConfigManager.getProperty("bulidPolicies.qname"), ResourceConfigManager.getProperty("bulidPolicies.servicename"));
			
			this.bulidCoveragesEndpoint = ResourceConfigManager.getProperty("bulidCoverages.endpoint");
			this.bulidCoveragesQName = new QName(ResourceConfigManager.getProperty("bulidCoverages.qname"), ResourceConfigManager.getProperty("bulidCoverages.servicename"));
			
			this.policyByClientEndpoint = ResourceConfigManager.getProperty("policyByClient.endpoint");
			this.policyByClientQName = new QName(ResourceConfigManager.getProperty("policyByClient.qname"), ResourceConfigManager.getProperty("policyByClient.servicename"));
			
			this.coverageRelationEndpoint = ResourceConfigManager.getProperty("coverageRelation.endpoint");
			this.coverageRelationQName = new QName(ResourceConfigManager.getProperty("coverageRelation.qname"), ResourceConfigManager.getProperty("coverageRelation.servicename"));
			
			this.policyCoveragesEndpoint = ResourceConfigManager.getProperty("policyCoverages.endpoint");
			this.policyCoveragesQName = new QName (ResourceConfigManager.getProperty("policyCoverages.qname"), ResourceConfigManager.getProperty("policyCoverages.servicename"));
			
			this.policyCoverageDetailEndpoint = ResourceConfigManager.getProperty("policyCoverageDetail.endpoint");
			this.policyCoverageDetailQName = new QName (ResourceConfigManager.getProperty("policyCoverageDetail.qname"), ResourceConfigManager.getProperty("policyCoverageDetail.servicename"));
			
			this.USERNAME = ResourceConfigManager.getProperty("soa.authen.username");
			this.PASSWORD = ResourceConfigManager.getProperty("soa.authen.password");
			
			this.policyInformationEndpoint = ResourceConfigManager.getProperty("policyInformation.endpoint");
			this.policyInformationQName = new QName(ResourceConfigManager.getProperty("policyInformation.qname"), ResourceConfigManager.getProperty("policyInformation.servicename"));
			
			this.CLAIM_USERNAME = ResourceConfigManager.getProperty("claim.soa.authen.username");
			this.CLAIM_PASSWORD = ResourceConfigManager.getProperty("claim.soa.authen.password");
			
			this.jndiLOF4CLAIM = ResourceConfigManager.getProperty("jndi.LOF4CLAIM");
			this.jndiLOF4POS = ResourceConfigManager.getProperty("jndi.LOF4POS");
			
			this.collectorType = ResourceConfigManager.getProperty("collectorType");
			
			this.policyByClientEndpointNew = ResourceConfigManager.getProperty("policyByClient_new.endpoint");
			this.policyByClientQNameNew = new QName(ResourceConfigManager.getProperty("policyByClient_new.qname"), ResourceConfigManager.getProperty("policyByClient_new.servicename"));
			
			this.policyInformationEndpointNew = ResourceConfigManager.getProperty("policyInformation_new.endpoint");
			this.policyInformationQNameNew = new QName(ResourceConfigManager.getProperty("policyInformation_new.qname"), ResourceConfigManager.getProperty("policyInformation_new.servicename"));
			
			this.coverageRelationEndpointNew = ResourceConfigManager.getProperty("coverageRelation_new.endpoint");
			this.coverageRelationQNameNew = new QName(ResourceConfigManager.getProperty("coverageRelation_new.qname"), ResourceConfigManager.getProperty("coverageRelation_new.servicename"));
		} 
		catch (Exception e) {
			log.error(e.getClass().getName(), e);
			throw new RuntimeException("Error When Initialize WebService");
		}
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getBulidPoliciesEndpoint() {
		return bulidPoliciesEndpoint;
	}

	public void setBulidPoliciesEndpoint(String bulidPoliciesEndpoint) {
		this.bulidPoliciesEndpoint = bulidPoliciesEndpoint;
	}

	public QName getBulidPoliciesQName() {
		return bulidPoliciesQName;
	}

	public void setBulidPoliciesQName(QName bulidPoliciesQName) {
		this.bulidPoliciesQName = bulidPoliciesQName;
	}

	public String getBulidCoveragesEndpoint() {
		return bulidCoveragesEndpoint;
	}

	public void setBulidCoveragesEndpoint(String bulidCoveragesEndpoint) {
		this.bulidCoveragesEndpoint = bulidCoveragesEndpoint;
	}

	public QName getBulidCoveragesQName() {
		return bulidCoveragesQName;
	}

	public void setBulidCoveragesQName(QName bulidCoveragesQName) {
		this.bulidCoveragesQName = bulidCoveragesQName;
	}

	public String getPolicyByClientEndpoint() {
		return policyByClientEndpoint;
	}

	public void setPolicyByClientEndpoint(String policyByClientEndpoint) {
		this.policyByClientEndpoint = policyByClientEndpoint;
	}

	public QName getPolicyByClientQName() {
		return policyByClientQName;
	}

	public void setPolicyByClientQName(QName policyByClientQName) {
		this.policyByClientQName = policyByClientQName;
	}

	public String getPolicyInformationEndpoint() {
		return policyInformationEndpoint;
	}

	public void setPolicyInformationEndpoint(String policyInformationEndpoint) {
		this.policyInformationEndpoint = policyInformationEndpoint;
	}

	public QName getPolicyInformationQName() {
		return policyInformationQName;
	}

	public void setPolicyInformationQName(QName policyInformationQName) {
		this.policyInformationQName = policyInformationQName;
	}

	public String getCLAIM_USERNAME() {
		return CLAIM_USERNAME;
	}

	public void setCLAIM_USERNAME(String cLAIM_USERNAME) {
		CLAIM_USERNAME = cLAIM_USERNAME;
	}

	public String getCLAIM_PASSWORD() {
		return CLAIM_PASSWORD;
	}

	public void setCLAIM_PASSWORD(String cLAIM_PASSWORD) {
		CLAIM_PASSWORD = cLAIM_PASSWORD;
	}

	public String getCoverageRelationEndpoint() {
		return coverageRelationEndpoint;
	}

	public void setCoverageRelationEndpoint(String coverageRelationEndpoint) {
		this.coverageRelationEndpoint = coverageRelationEndpoint;
	}

	public QName getCoverageRelationQName() {
		return coverageRelationQName;
	}

	public void setCoverageRelationQName(QName coverageRelationQName) {
		this.coverageRelationQName = coverageRelationQName;
	}

	public String getPolicyCoveragesEndpoint() {
		return policyCoveragesEndpoint;
	}

	public void setPolicyCoveragesEndpoint(String policyCoveragesEndpoint) {
		this.policyCoveragesEndpoint = policyCoveragesEndpoint;
	}

	public QName getPolicyCoveragesQName() {
		return policyCoveragesQName;
	}

	public void setPolicyCoveragesQName(QName policyCoveragesQName) {
		this.policyCoveragesQName = policyCoveragesQName;
	}

	public String getPolicyCoverageDetailEndpoint() {
		return policyCoverageDetailEndpoint;
	}

	public void setPolicyCoverageDetailEndpoint(String policyCoverageDetailEndpoint) {
		this.policyCoverageDetailEndpoint = policyCoverageDetailEndpoint;
	}

	public QName getPolicyCoverageDetailQName() {
		return policyCoverageDetailQName;
	}

	public void setPolicyCoverageDetailQName(QName policyCoverageDetailQName) {
		this.policyCoverageDetailQName = policyCoverageDetailQName;
	}

	public String getJndiLOF4CLAIM() {
		return jndiLOF4CLAIM;
	}

	public void setJndiLOF4CLAIM(String jndiLOF4CLAIM) {
		this.jndiLOF4CLAIM = jndiLOF4CLAIM;
	}

	public String getJndiLOF4POS() {
		return jndiLOF4POS;
	}

	public void setJndiLOF4POS(String jndiLOF4POS) {
		this.jndiLOF4POS = jndiLOF4POS;
	}

	public String getCollectorType() {
		return collectorType;
	}

	public String getPolicyByClientEndpointNew() {
		return policyByClientEndpointNew;
	}

	public void setPolicyByClientEndpointNew(String policyByClientEndpointNew) {
		this.policyByClientEndpointNew = policyByClientEndpointNew;
	}

	public QName getPolicyByClientQNameNew() {
		return policyByClientQNameNew;
	}

	public void setPolicyByClientQNameNew(QName policyByClientQNameNew) {
		this.policyByClientQNameNew = policyByClientQNameNew;
	}

	public String getPolicyInformationEndpointNew() {
		return policyInformationEndpointNew;
	}

	public void setPolicyInformationEndpointNew(String policyInformationEndpointNew) {
		this.policyInformationEndpointNew = policyInformationEndpointNew;
	}

	public QName getPolicyInformationQNameNew() {
		return policyInformationQNameNew;
	}

	public void setPolicyInformationQNameNew(QName policyInformationQNameNew) {
		this.policyInformationQNameNew = policyInformationQNameNew;
	}

	public String getCoverageRelationEndpointNew() {
		return coverageRelationEndpointNew;
	}

	public void setCoverageRelationEndpointNew(String coverageRelationEndpointNew) {
		this.coverageRelationEndpointNew = coverageRelationEndpointNew;
	}

	public QName getCoverageRelationQNameNew() {
		return coverageRelationQNameNew;
	}

	public void setCoverageRelationQNameNew(QName coverageRelationQNameNew) {
		this.coverageRelationQNameNew = coverageRelationQNameNew;
	}

}
