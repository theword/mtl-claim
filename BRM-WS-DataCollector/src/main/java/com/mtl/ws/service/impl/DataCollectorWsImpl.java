package com.mtl.ws.service.impl;

import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.mtl.LogMonitor;
import com.mtl.benefitAnalysis.dto.BenefitAnalysisDto;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisModel;
import com.mtl.continuity.dto.ContinuityClaimCheckingDto;
import com.mtl.continuity.model.xom.mtlclaim.ContinuityClaimCheckingModel;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.hospital.dto.HospitalBlacklistDto;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;
import com.mtl.service.ClaimCollectorService;
import com.mtl.service.ILifeCollectorService;
import com.mtl.utils.ApplicationContextUtils;
import com.mtl.ws.bean.DataCollectorDto;
import com.mtl.ws.service.DataCollectorWs;

@WebService(
	endpointInterface = "com.mtl.ws.service.DataCollectorWs"
	, targetNamespace="http://www.dataCollectorWs.ws.application.muangthai.co.th/"
	, serviceName="DataCollectorService"
)
public class DataCollectorWsImpl implements DataCollectorWs{
	
	private Logger log 			= Logger.getLogger(DataCollectorWsImpl.class);
	private Gson gsonService 	= (Gson)ApplicationContextUtils.getBean("gsonService");
	private ClaimCollectorService claimCollectorService = (ClaimCollectorService)ApplicationContextUtils.getBean("claimCollectorService");
	private ILifeCollectorService iLifeCollectorService = (ILifeCollectorService)ApplicationContextUtils.getBean("iLifeCollectorService");
	
	private LogMonitor logMonitor;

	public @WebResult(name = "response")
	DataCollectorDto collectBenefitAnalysisModel(@WebParam(name = "request") DataCollectorDto request) {
		log.debug("------------start [DataCollectorWsImpl.collectBenefitAnalysisModel] ---------");
		String reason = "";
		DataCollectorDto res = new DataCollectorDto();
		logMonitor = new LogMonitor();

		res.setServiceID(request.getServiceID());
		String callName = request.getCallName();
		res.setCallName(callName);
		res.setRequestedDate(request.getRequestedDate());
		try {
			log.debug("********DataCollectorDto benefit request.getJsonXom():" + request.getJsonXom());
			BenefitAnalysisDto dto = gsonService.fromJson(request.getJsonXom(), BenefitAnalysisDto.class);
			log.debug(" ------------ Dto's Data ------------ ");
			log.debug("Client ID : " + dto.getClientID());
			log.debug("Benefit groups : " + dto.getBenefitGroups().size());

			logMonitor.setSystemName("benefit");
			logMonitor.setRequestTime(new Date());
			logMonitor.setClientNo(dto.getClientID());

			if (dto.getCurrentClaims() != null)
				log.debug("Current Claims: " + dto.getCurrentClaims().size());
			else
				log.debug("Current Claims is null");

			if (dto.getContinuityClaim() != null)
				log.debug("Continuity Claims: " + dto.getContinuityClaim().size());
			else
				log.debug("Continuity Claims is null");

			if (dto.getFixedClaims() != null)
				log.debug("Fixed Claims: " + dto.getFixedClaims().size());
			else
				log.debug("Fixed Claims is null");

			claimCollectorService.setLogMonitor(logMonitor);
			BenefitAnalysisModel model = claimCollectorService.collectBenefitAnalysisModel(dto, callName);

			if (model != null && model.getStatus() >= 0) {
				res.setStatus(0);
				res.setReason(reason);
				res.setJsonXom(gsonService.toJson(model));
				log.debug("response jsonXom:" + res.getJsonXom());
			}
			else {
				if (model == null)
					model = new BenefitAnalysisModel(null, null, null);
				model.setStatus(-1);
				res.setJsonXom(gsonService.toJson(model));
				res.setStatus(-1);
				log.debug("response jsonXom:" + res.getJsonXom());
			}
		}
		catch (Exception e) {
			BenefitAnalysisModel model = new BenefitAnalysisModel(null, null, null);
			model.setStatus(-1);
			res.setJsonXom(gsonService.toJson(model));
			log.error("ERROR:" + e, e);
			res.setStatus(-1);
			res.setReason("ERROR:" + e.getMessage());
			e.printStackTrace();
			log.debug("response jsonXom:" + res.getJsonXom());
		}

		log.debug("begin  test write log monitor");
		logMonitor.write();
		log.debug("finish test write log monitor");

		res.setRespondDate(com.mtl.benefitAnalysis.util.CalendarHelper.getCurrentDateTime().getTime());
		log.debug("------------ end Service [DataCollectorWsImpl.collectBenefitAnalysisModel]");
		return res;
	}

	private void validateEligible(EligibleModel model,DataCollectorDto res){
		if(model.getStatus() < 0){
			res.setStatus(-1);
			res.setJsonXom(gsonService.toJson(model));
			log.debug("response jsonXom : "+res.getJsonXom());
		}
	}

	public @WebResult(name = "response")
	DataCollectorDto collectEligibleModel(@WebParam(name = "request") DataCollectorDto request) {
		log.debug("------------start [DataCollectorWsImplcollectEligibleModel] ---------");
		String reason = "";
		DataCollectorDto res = new DataCollectorDto();
		logMonitor = new LogMonitor();

		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(request.getRequestedDate());
		try {
			log.debug("********eligible request.getJsonXom():" + request.getJsonXom());
			EligibleDto dto = gsonService.fromJson(request.getJsonXom(), EligibleDto.class);
			EligibleModel model;

			logMonitor.setSystemName("eligible");
			logMonitor.setRequestTime(new Date());
			logMonitor.setPolicyNumber(dto.getPolicyNumber());
			logMonitor.setClientNo(dto.getClientID());
			logMonitor.setKeyOther(dto.getHospitalCode());

			claimCollectorService.setLogMonitor(logMonitor);
			iLifeCollectorService.setLogMonitor(logMonitor);

			if (dto.getPolicyNumber() != null && !(dto.getPolicyNumber().equals(""))) {
				log.debug("case : policy number");
				model = iLifeCollectorService.collectEligibleModel(dto, res.getCallName());
				validateEligible(model, res);

				model = claimCollectorService.collectILifeEligibleModel(model);
				validateEligible(model, res);
			}
			else {
				log.debug("case : client id");
				model = claimCollectorService.collectEligibleModel(dto, res.getCallName());
				validateEligible(model, res);
			}

			if (model != null && model.getStatus() >= 0) {
				res.setStatus(0);
				res.setReason(reason);
				res.setJsonXom(gsonService.toJson(model));
				log.debug("response jsonXom:" + res.getJsonXom());
			}
			else {
				res.setStatus(-1);
				res.setJsonXom(gsonService.toJson(model));
				log.debug("response jsonXom:" + res.getJsonXom());
			}
		}
		catch (Exception e) {
			log.error("ERROR:" + e, e);
			res.setStatus(-1);
			res.setReason("ERROR:" + e.getMessage());
			e.printStackTrace();
			EligibleModel model = new EligibleModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
			model.setStatus(-1);
			res.setJsonXom(gsonService.toJson(model));
		}

		log.debug("begin  test write log monitor");
		logMonitor.write();
		log.debug("finish test write log monitor");

		res.setRespondDate(com.mtl.eligible.util.CalendarHelper.getCurrentDateTime().getTime());
		log.debug("------------ end Service [DataCollectorWsImpl.collectEligibleModel]");
		return res;
	}

	public @WebResult(name="response") DataCollectorDto collectHospitalBlacklistModel(@WebParam(name="request") DataCollectorDto request){
		log.debug("------------start [DataCollectorWsImpl.collectHospitalBlacklistModel] ---------");
		String reason 			= "";
		DataCollectorDto res 	= new DataCollectorDto();
		logMonitor				= new LogMonitor();
		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(request.getRequestedDate());
		try{			
			log.debug("********hospital request.getJsonXom():"+request.getJsonXom());
			HospitalBlacklistDto dto = gsonService.fromJson(request.getJsonXom(), HospitalBlacklistDto.class);
			
			logMonitor.setSystemName("hospital");
			logMonitor.setRequestTime(new Date());
			logMonitor.setKeyOther(dto.getHospitalCode());
			
			claimCollectorService.setLogMonitor(logMonitor);
			HospitalBlacklistModel model = claimCollectorService.collectHospitalBlacklistModel(dto);
			if(model!=null){
				if(model.getStatus() < 0)res.setStatus(-1);
				else res.setStatus(0);
				
				res.setReason(reason);
				res.setJsonXom(gsonService.toJson(model));
				log.debug("response jsonXom:"+res.getJsonXom());
			}else{
				res.setStatus(-1);
				model = new HospitalBlacklistModel();
				model.setStatus(-1);
				res.setJsonXom(gsonService.toJson(model));
			}			
		}catch (Exception e) {
			log.error("ERROR:"+e,e);
			res.setStatus(-1);
			res.setReason("ERROR:"+e.getMessage());		
			e.printStackTrace();
			HospitalBlacklistModel model = new HospitalBlacklistModel();
			model.setStatus(-1);
			res.setJsonXom(gsonService.toJson(model));
		}
		
		log.debug("begin  test write log monitor");
		logMonitor.write();
		log.debug("finish test write log monitor");
		
		res.setRespondDate(com.mtl.hospital.util.CalendarHelper.getCurrentDateTime().getTime());
		log.debug("------------ end Service [DataCollectorWsImpl.collectHospitalBlacklistModel]");
		return res;
	}
	
	public @WebResult(name="response") DataCollectorDto collectContinuityClaimCheckingModel(@WebParam(name="request") DataCollectorDto request){
		log.debug("------------start [collectContinuityClaimCheckingModel] ---------");
		String reason			= "";
		DataCollectorDto res 	= new DataCollectorDto();
		logMonitor				= new LogMonitor();
		res.setServiceID(request.getServiceID());
		res.setCallName(request.getCallName());
		res.setRequestedDate(request.getRequestedDate());
		try{			
			log.debug("********continuity request.getJsonXom():"+request.getJsonXom());
			ContinuityClaimCheckingDto dto 		= gsonService.fromJson(request.getJsonXom(), ContinuityClaimCheckingDto.class);	
			
			logMonitor.setSystemName("continuity");
			logMonitor.setRequestTime(new Date());
			logMonitor.setClientNo(dto.getClientID());
			logMonitor.setKeyOther(dto.getClaimType().toString());
			
			claimCollectorService.setLogMonitor(logMonitor);
			ContinuityClaimCheckingModel model 	= claimCollectorService.collectContinuityClaimCheckingModel(dto,request.getCallName());
			if(model!=null){
				if(model.getStatus()<0)res.setStatus(-1);
				else res.setStatus(1);
				
				res.setReason(reason);
				res.setJsonXom(gsonService.toJson(model));
				log.debug("response jsonXom:"+res.getJsonXom());
			}else{
				res.setStatus(-1);
				model = new ContinuityClaimCheckingModel(null, null, null, null, null, null);
				model.setStatus(-1);
				res.setJsonXom(gsonService.toJson(model));
			}			
		}catch (Exception e) {
			log.error("ERROR:"+e,e);
			res.setStatus(-1);
			res.setReason("ERROR:"+e.getMessage());	
			ContinuityClaimCheckingModel model = new ContinuityClaimCheckingModel(null, null, null, null, null, null);
			model.setStatus(-1);
			res.setJsonXom(gsonService.toJson(model));
			e.printStackTrace();
		}
		
		log.debug("begin  test write log monitor");
		logMonitor.write();
		log.debug("finish test write log monitor");
		
		res.setRespondDate(com.mtl.continuity.util.CalendarHelper.getCurrentDateTime().getTime());
		log.debug("------------ end Service [DataCollectorWsImpl.collectContinuityClaimCheckingModel]");
		return res;
	}
	
}
