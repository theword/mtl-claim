/*
 * 
 */
package com.mtl.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.mtl.ws.bean.DataCollectorDto;

/**
 * The Interface DataCollectorWs.
 */
@WebService(
		name = "DataCollectorService"
		, targetNamespace = "http://www.dataCollectorWs.ws.application.muangthai.co.th/"
	)
	@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface DataCollectorWs {
	
	/**
	 * Collect benefit analysis model.
	 *
	 * @param request the request
	 * @return the data collector dto
	 */
	public @WebResult(name="response") DataCollectorDto collectBenefitAnalysisModel(@WebParam(name="request") DataCollectorDto request);
	
	/**
	 * Collect eligible model.
	 *
	 * @param request the request
	 * @return the data collector dto
	 */
	public @WebResult(name="response") DataCollectorDto collectEligibleModel(@WebParam(name="request") DataCollectorDto request);
	
	/**
	 * Collect hospital blacklist model.
	 *
	 * @param request the request
	 * @return the data collector dto
	 */
	public @WebResult(name="response") DataCollectorDto collectHospitalBlacklistModel(@WebParam(name="request") DataCollectorDto request);
	
	/**
	 * Collect continuity claim checking model.
	 *
	 * @param request the request
	 * @return the data collector dto
	 */
	public @WebResult(name="response") DataCollectorDto collectContinuityClaimCheckingModel(@WebParam(name="request") DataCollectorDto request);

}
