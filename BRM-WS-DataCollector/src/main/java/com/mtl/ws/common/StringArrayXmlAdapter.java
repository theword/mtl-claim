/*
 * 
 */
package com.mtl.ws.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The Class StringArrayXmlAdapter.
 */
public class StringArrayXmlAdapter extends XmlAdapter<List<String>, String[]> {
	 
 	/* (non-Javadoc)
 	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
 	 */
 	@Override
	 public List<String> marshal(String[] arg) throws Exception {
	 return new ArrayList<String>(Arrays.asList(arg));
	 }
	
	 /* (non-Javadoc)
 	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
 	 */
 	@Override
	 public String[] unmarshal(List<String> arg) throws Exception {
	 return arg.toArray(new String[] {});
	 }
}