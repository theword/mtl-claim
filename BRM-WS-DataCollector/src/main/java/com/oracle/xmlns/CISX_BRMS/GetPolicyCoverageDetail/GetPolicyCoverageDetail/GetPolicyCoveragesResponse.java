/**
 * GetPolicyCoveragesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail;

public class GetPolicyCoveragesResponse  implements java.io.Serializable {
    private java.lang.String policyNumber;

    private java.lang.String policyStatus;

    private java.lang.String planCode;

    private java.lang.String modalPremium;

    private java.lang.String modeOfPremium;

    private java.lang.String issuedDate;

    private java.lang.String billedToDate;

    private java.lang.String paidToDate;

    private java.lang.String policyApprovedDate;

    private java.lang.String clientType;

    private java.lang.String policyStatusChangeDate;

    private com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.CoveragesDetail[] coveragesDetailList;

    public GetPolicyCoveragesResponse() {
    }

    public GetPolicyCoveragesResponse(
           java.lang.String policyNumber,
           java.lang.String policyStatus,
           java.lang.String planCode,
           java.lang.String modalPremium,
           java.lang.String modeOfPremium,
           java.lang.String issuedDate,
           java.lang.String billedToDate,
           java.lang.String paidToDate,
           java.lang.String policyApprovedDate,
           java.lang.String clientType,
           java.lang.String policyStatusChangeDate,
           com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.CoveragesDetail[] coveragesDetailList) {
           this.policyNumber = policyNumber;
           this.policyStatus = policyStatus;
           this.planCode = planCode;
           this.modalPremium = modalPremium;
           this.modeOfPremium = modeOfPremium;
           this.issuedDate = issuedDate;
           this.billedToDate = billedToDate;
           this.paidToDate = paidToDate;
           this.policyApprovedDate = policyApprovedDate;
           this.clientType = clientType;
           this.policyStatusChangeDate = policyStatusChangeDate;
           this.coveragesDetailList = coveragesDetailList;
    }


    /**
     * Gets the policyNumber value for this GetPolicyCoveragesResponse.
     * 
     * @return policyNumber
     */
    public java.lang.String getPolicyNumber() {
        return policyNumber;
    }


    /**
     * Sets the policyNumber value for this GetPolicyCoveragesResponse.
     * 
     * @param policyNumber
     */
    public void setPolicyNumber(java.lang.String policyNumber) {
        this.policyNumber = policyNumber;
    }


    /**
     * Gets the policyStatus value for this GetPolicyCoveragesResponse.
     * 
     * @return policyStatus
     */
    public java.lang.String getPolicyStatus() {
        return policyStatus;
    }


    /**
     * Sets the policyStatus value for this GetPolicyCoveragesResponse.
     * 
     * @param policyStatus
     */
    public void setPolicyStatus(java.lang.String policyStatus) {
        this.policyStatus = policyStatus;
    }


    /**
     * Gets the planCode value for this GetPolicyCoveragesResponse.
     * 
     * @return planCode
     */
    public java.lang.String getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this GetPolicyCoveragesResponse.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String planCode) {
        this.planCode = planCode;
    }


    /**
     * Gets the modalPremium value for this GetPolicyCoveragesResponse.
     * 
     * @return modalPremium
     */
    public java.lang.String getModalPremium() {
        return modalPremium;
    }


    /**
     * Sets the modalPremium value for this GetPolicyCoveragesResponse.
     * 
     * @param modalPremium
     */
    public void setModalPremium(java.lang.String modalPremium) {
        this.modalPremium = modalPremium;
    }


    /**
     * Gets the modeOfPremium value for this GetPolicyCoveragesResponse.
     * 
     * @return modeOfPremium
     */
    public java.lang.String getModeOfPremium() {
        return modeOfPremium;
    }


    /**
     * Sets the modeOfPremium value for this GetPolicyCoveragesResponse.
     * 
     * @param modeOfPremium
     */
    public void setModeOfPremium(java.lang.String modeOfPremium) {
        this.modeOfPremium = modeOfPremium;
    }


    /**
     * Gets the issuedDate value for this GetPolicyCoveragesResponse.
     * 
     * @return issuedDate
     */
    public java.lang.String getIssuedDate() {
        return issuedDate;
    }


    /**
     * Sets the issuedDate value for this GetPolicyCoveragesResponse.
     * 
     * @param issuedDate
     */
    public void setIssuedDate(java.lang.String issuedDate) {
        this.issuedDate = issuedDate;
    }


    /**
     * Gets the billedToDate value for this GetPolicyCoveragesResponse.
     * 
     * @return billedToDate
     */
    public java.lang.String getBilledToDate() {
        return billedToDate;
    }


    /**
     * Sets the billedToDate value for this GetPolicyCoveragesResponse.
     * 
     * @param billedToDate
     */
    public void setBilledToDate(java.lang.String billedToDate) {
        this.billedToDate = billedToDate;
    }


    /**
     * Gets the paidToDate value for this GetPolicyCoveragesResponse.
     * 
     * @return paidToDate
     */
    public java.lang.String getPaidToDate() {
        return paidToDate;
    }


    /**
     * Sets the paidToDate value for this GetPolicyCoveragesResponse.
     * 
     * @param paidToDate
     */
    public void setPaidToDate(java.lang.String paidToDate) {
        this.paidToDate = paidToDate;
    }


    /**
     * Gets the policyApprovedDate value for this GetPolicyCoveragesResponse.
     * 
     * @return policyApprovedDate
     */
    public java.lang.String getPolicyApprovedDate() {
        return policyApprovedDate;
    }


    /**
     * Sets the policyApprovedDate value for this GetPolicyCoveragesResponse.
     * 
     * @param policyApprovedDate
     */
    public void setPolicyApprovedDate(java.lang.String policyApprovedDate) {
        this.policyApprovedDate = policyApprovedDate;
    }


    /**
     * Gets the clientType value for this GetPolicyCoveragesResponse.
     * 
     * @return clientType
     */
    public java.lang.String getClientType() {
        return clientType;
    }


    /**
     * Sets the clientType value for this GetPolicyCoveragesResponse.
     * 
     * @param clientType
     */
    public void setClientType(java.lang.String clientType) {
        this.clientType = clientType;
    }


    /**
     * Gets the policyStatusChangeDate value for this GetPolicyCoveragesResponse.
     * 
     * @return policyStatusChangeDate
     */
    public java.lang.String getPolicyStatusChangeDate() {
        return policyStatusChangeDate;
    }


    /**
     * Sets the policyStatusChangeDate value for this GetPolicyCoveragesResponse.
     * 
     * @param policyStatusChangeDate
     */
    public void setPolicyStatusChangeDate(java.lang.String policyStatusChangeDate) {
        this.policyStatusChangeDate = policyStatusChangeDate;
    }


    /**
     * Gets the coveragesDetailList value for this GetPolicyCoveragesResponse.
     * 
     * @return coveragesDetailList
     */
    public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.CoveragesDetail[] getCoveragesDetailList() {
        return coveragesDetailList;
    }


    /**
     * Sets the coveragesDetailList value for this GetPolicyCoveragesResponse.
     * 
     * @param coveragesDetailList
     */
    public void setCoveragesDetailList(com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.CoveragesDetail[] coveragesDetailList) {
        this.coveragesDetailList = coveragesDetailList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPolicyCoveragesResponse)) return false;
        GetPolicyCoveragesResponse other = (GetPolicyCoveragesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.policyNumber==null && other.getPolicyNumber()==null) || 
             (this.policyNumber!=null &&
              this.policyNumber.equals(other.getPolicyNumber()))) &&
            ((this.policyStatus==null && other.getPolicyStatus()==null) || 
             (this.policyStatus!=null &&
              this.policyStatus.equals(other.getPolicyStatus()))) &&
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              this.planCode.equals(other.getPlanCode()))) &&
            ((this.modalPremium==null && other.getModalPremium()==null) || 
             (this.modalPremium!=null &&
              this.modalPremium.equals(other.getModalPremium()))) &&
            ((this.modeOfPremium==null && other.getModeOfPremium()==null) || 
             (this.modeOfPremium!=null &&
              this.modeOfPremium.equals(other.getModeOfPremium()))) &&
            ((this.issuedDate==null && other.getIssuedDate()==null) || 
             (this.issuedDate!=null &&
              this.issuedDate.equals(other.getIssuedDate()))) &&
            ((this.billedToDate==null && other.getBilledToDate()==null) || 
             (this.billedToDate!=null &&
              this.billedToDate.equals(other.getBilledToDate()))) &&
            ((this.paidToDate==null && other.getPaidToDate()==null) || 
             (this.paidToDate!=null &&
              this.paidToDate.equals(other.getPaidToDate()))) &&
            ((this.policyApprovedDate==null && other.getPolicyApprovedDate()==null) || 
             (this.policyApprovedDate!=null &&
              this.policyApprovedDate.equals(other.getPolicyApprovedDate()))) &&
            ((this.clientType==null && other.getClientType()==null) || 
             (this.clientType!=null &&
              this.clientType.equals(other.getClientType()))) &&
            ((this.policyStatusChangeDate==null && other.getPolicyStatusChangeDate()==null) || 
             (this.policyStatusChangeDate!=null &&
              this.policyStatusChangeDate.equals(other.getPolicyStatusChangeDate()))) &&
            ((this.coveragesDetailList==null && other.getCoveragesDetailList()==null) || 
             (this.coveragesDetailList!=null &&
              java.util.Arrays.equals(this.coveragesDetailList, other.getCoveragesDetailList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPolicyNumber() != null) {
            _hashCode += getPolicyNumber().hashCode();
        }
        if (getPolicyStatus() != null) {
            _hashCode += getPolicyStatus().hashCode();
        }
        if (getPlanCode() != null) {
            _hashCode += getPlanCode().hashCode();
        }
        if (getModalPremium() != null) {
            _hashCode += getModalPremium().hashCode();
        }
        if (getModeOfPremium() != null) {
            _hashCode += getModeOfPremium().hashCode();
        }
        if (getIssuedDate() != null) {
            _hashCode += getIssuedDate().hashCode();
        }
        if (getBilledToDate() != null) {
            _hashCode += getBilledToDate().hashCode();
        }
        if (getPaidToDate() != null) {
            _hashCode += getPaidToDate().hashCode();
        }
        if (getPolicyApprovedDate() != null) {
            _hashCode += getPolicyApprovedDate().hashCode();
        }
        if (getClientType() != null) {
            _hashCode += getClientType().hashCode();
        }
        if (getPolicyStatusChangeDate() != null) {
            _hashCode += getPolicyStatusChangeDate().hashCode();
        }
        if (getCoveragesDetailList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCoveragesDetailList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCoveragesDetailList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPolicyCoveragesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "getPolicyCoveragesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "policyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "policyStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "planCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modalPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "modalPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modeOfPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "modeOfPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "issuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billedToDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "billedToDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paidToDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "paidToDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyApprovedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "policyApprovedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "clientType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyStatusChangeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "policyStatusChangeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coveragesDetailList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "CoveragesDetailList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "CoveragesDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "CoveragesDetail"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
