/**
 * ProcessResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail;

public class ProcessResponse  implements java.io.Serializable {
    private com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoveragesResponse[] getPolicyCoveragesResponseList;

    public ProcessResponse() {
    }

    public ProcessResponse(
           com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoveragesResponse[] getPolicyCoveragesResponseList) {
           this.getPolicyCoveragesResponseList = getPolicyCoveragesResponseList;
    }


    /**
     * Gets the getPolicyCoveragesResponseList value for this ProcessResponse.
     * 
     * @return getPolicyCoveragesResponseList
     */
    public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoveragesResponse[] getGetPolicyCoveragesResponseList() {
        return getPolicyCoveragesResponseList;
    }


    /**
     * Sets the getPolicyCoveragesResponseList value for this ProcessResponse.
     * 
     * @param getPolicyCoveragesResponseList
     */
    public void setGetPolicyCoveragesResponseList(com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoveragesResponse[] getPolicyCoveragesResponseList) {
        this.getPolicyCoveragesResponseList = getPolicyCoveragesResponseList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessResponse)) return false;
        ProcessResponse other = (ProcessResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPolicyCoveragesResponseList==null && other.getGetPolicyCoveragesResponseList()==null) || 
             (this.getPolicyCoveragesResponseList!=null &&
              java.util.Arrays.equals(this.getPolicyCoveragesResponseList, other.getGetPolicyCoveragesResponseList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPolicyCoveragesResponseList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPolicyCoveragesResponseList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPolicyCoveragesResponseList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProcessResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", ">processResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPolicyCoveragesResponseList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "getPolicyCoveragesResponseList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "getPolicyCoveragesResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "getPolicyCoveragesResponse"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
