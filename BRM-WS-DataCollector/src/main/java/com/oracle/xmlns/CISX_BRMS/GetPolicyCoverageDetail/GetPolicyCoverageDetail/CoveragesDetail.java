/**
 * CoveragesDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail;

public class CoveragesDetail  implements java.io.Serializable {
    private java.lang.String coverageCode;

    private java.lang.String coverageStatus;

    private java.lang.String coverageIssuedDate;

    private java.lang.String trailerNumber;

    private java.lang.String faceAmount;

    private java.lang.String sumInsured;

    private java.lang.String matExpiry;

    private java.lang.String clientType;

    private java.lang.String coverageStatusChangeDate;

    private java.lang.String contestablePeriod;

    public CoveragesDetail() {
    }

    public CoveragesDetail(
           java.lang.String coverageCode,
           java.lang.String coverageStatus,
           java.lang.String coverageIssuedDate,
           java.lang.String trailerNumber,
           java.lang.String faceAmount,
           java.lang.String sumInsured,
           java.lang.String matExpiry,
           java.lang.String clientType,
           java.lang.String coverageStatusChangeDate,
           java.lang.String contestablePeriod) {
           this.coverageCode = coverageCode;
           this.coverageStatus = coverageStatus;
           this.coverageIssuedDate = coverageIssuedDate;
           this.trailerNumber = trailerNumber;
           this.faceAmount = faceAmount;
           this.sumInsured = sumInsured;
           this.matExpiry = matExpiry;
           this.clientType = clientType;
           this.coverageStatusChangeDate = coverageStatusChangeDate;
           this.contestablePeriod = contestablePeriod;
    }


    /**
     * Gets the coverageCode value for this CoveragesDetail.
     * 
     * @return coverageCode
     */
    public java.lang.String getCoverageCode() {
        return coverageCode;
    }


    /**
     * Sets the coverageCode value for this CoveragesDetail.
     * 
     * @param coverageCode
     */
    public void setCoverageCode(java.lang.String coverageCode) {
        this.coverageCode = coverageCode;
    }


    /**
     * Gets the coverageStatus value for this CoveragesDetail.
     * 
     * @return coverageStatus
     */
    public java.lang.String getCoverageStatus() {
        return coverageStatus;
    }


    /**
     * Sets the coverageStatus value for this CoveragesDetail.
     * 
     * @param coverageStatus
     */
    public void setCoverageStatus(java.lang.String coverageStatus) {
        this.coverageStatus = coverageStatus;
    }


    /**
     * Gets the coverageIssuedDate value for this CoveragesDetail.
     * 
     * @return coverageIssuedDate
     */
    public java.lang.String getCoverageIssuedDate() {
        return coverageIssuedDate;
    }


    /**
     * Sets the coverageIssuedDate value for this CoveragesDetail.
     * 
     * @param coverageIssuedDate
     */
    public void setCoverageIssuedDate(java.lang.String coverageIssuedDate) {
        this.coverageIssuedDate = coverageIssuedDate;
    }


    /**
     * Gets the trailerNumber value for this CoveragesDetail.
     * 
     * @return trailerNumber
     */
    public java.lang.String getTrailerNumber() {
        return trailerNumber;
    }


    /**
     * Sets the trailerNumber value for this CoveragesDetail.
     * 
     * @param trailerNumber
     */
    public void setTrailerNumber(java.lang.String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }


    /**
     * Gets the faceAmount value for this CoveragesDetail.
     * 
     * @return faceAmount
     */
    public java.lang.String getFaceAmount() {
        return faceAmount;
    }


    /**
     * Sets the faceAmount value for this CoveragesDetail.
     * 
     * @param faceAmount
     */
    public void setFaceAmount(java.lang.String faceAmount) {
        this.faceAmount = faceAmount;
    }


    /**
     * Gets the sumInsured value for this CoveragesDetail.
     * 
     * @return sumInsured
     */
    public java.lang.String getSumInsured() {
        return sumInsured;
    }


    /**
     * Sets the sumInsured value for this CoveragesDetail.
     * 
     * @param sumInsured
     */
    public void setSumInsured(java.lang.String sumInsured) {
        this.sumInsured = sumInsured;
    }


    /**
     * Gets the matExpiry value for this CoveragesDetail.
     * 
     * @return matExpiry
     */
    public java.lang.String getMatExpiry() {
        return matExpiry;
    }


    /**
     * Sets the matExpiry value for this CoveragesDetail.
     * 
     * @param matExpiry
     */
    public void setMatExpiry(java.lang.String matExpiry) {
        this.matExpiry = matExpiry;
    }


    /**
     * Gets the clientType value for this CoveragesDetail.
     * 
     * @return clientType
     */
    public java.lang.String getClientType() {
        return clientType;
    }


    /**
     * Sets the clientType value for this CoveragesDetail.
     * 
     * @param clientType
     */
    public void setClientType(java.lang.String clientType) {
        this.clientType = clientType;
    }


    /**
     * Gets the coverageStatusChangeDate value for this CoveragesDetail.
     * 
     * @return coverageStatusChangeDate
     */
    public java.lang.String getCoverageStatusChangeDate() {
        return coverageStatusChangeDate;
    }


    /**
     * Sets the coverageStatusChangeDate value for this CoveragesDetail.
     * 
     * @param coverageStatusChangeDate
     */
    public void setCoverageStatusChangeDate(java.lang.String coverageStatusChangeDate) {
        this.coverageStatusChangeDate = coverageStatusChangeDate;
    }


    /**
     * Gets the contestablePeriod value for this CoveragesDetail.
     * 
     * @return contestablePeriod
     */
    public java.lang.String getContestablePeriod() {
        return contestablePeriod;
    }


    /**
     * Sets the contestablePeriod value for this CoveragesDetail.
     * 
     * @param contestablePeriod
     */
    public void setContestablePeriod(java.lang.String contestablePeriod) {
        this.contestablePeriod = contestablePeriod;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CoveragesDetail)) return false;
        CoveragesDetail other = (CoveragesDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.coverageCode==null && other.getCoverageCode()==null) || 
             (this.coverageCode!=null &&
              this.coverageCode.equals(other.getCoverageCode()))) &&
            ((this.coverageStatus==null && other.getCoverageStatus()==null) || 
             (this.coverageStatus!=null &&
              this.coverageStatus.equals(other.getCoverageStatus()))) &&
            ((this.coverageIssuedDate==null && other.getCoverageIssuedDate()==null) || 
             (this.coverageIssuedDate!=null &&
              this.coverageIssuedDate.equals(other.getCoverageIssuedDate()))) &&
            ((this.trailerNumber==null && other.getTrailerNumber()==null) || 
             (this.trailerNumber!=null &&
              this.trailerNumber.equals(other.getTrailerNumber()))) &&
            ((this.faceAmount==null && other.getFaceAmount()==null) || 
             (this.faceAmount!=null &&
              this.faceAmount.equals(other.getFaceAmount()))) &&
            ((this.sumInsured==null && other.getSumInsured()==null) || 
             (this.sumInsured!=null &&
              this.sumInsured.equals(other.getSumInsured()))) &&
            ((this.matExpiry==null && other.getMatExpiry()==null) || 
             (this.matExpiry!=null &&
              this.matExpiry.equals(other.getMatExpiry()))) &&
            ((this.clientType==null && other.getClientType()==null) || 
             (this.clientType!=null &&
              this.clientType.equals(other.getClientType()))) &&
            ((this.coverageStatusChangeDate==null && other.getCoverageStatusChangeDate()==null) || 
             (this.coverageStatusChangeDate!=null &&
              this.coverageStatusChangeDate.equals(other.getCoverageStatusChangeDate()))) &&
            ((this.contestablePeriod==null && other.getContestablePeriod()==null) || 
             (this.contestablePeriod!=null &&
              this.contestablePeriod.equals(other.getContestablePeriod())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCoverageCode() != null) {
            _hashCode += getCoverageCode().hashCode();
        }
        if (getCoverageStatus() != null) {
            _hashCode += getCoverageStatus().hashCode();
        }
        if (getCoverageIssuedDate() != null) {
            _hashCode += getCoverageIssuedDate().hashCode();
        }
        if (getTrailerNumber() != null) {
            _hashCode += getTrailerNumber().hashCode();
        }
        if (getFaceAmount() != null) {
            _hashCode += getFaceAmount().hashCode();
        }
        if (getSumInsured() != null) {
            _hashCode += getSumInsured().hashCode();
        }
        if (getMatExpiry() != null) {
            _hashCode += getMatExpiry().hashCode();
        }
        if (getClientType() != null) {
            _hashCode += getClientType().hashCode();
        }
        if (getCoverageStatusChangeDate() != null) {
            _hashCode += getCoverageStatusChangeDate().hashCode();
        }
        if (getContestablePeriod() != null) {
            _hashCode += getContestablePeriod().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CoveragesDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "CoveragesDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "coverageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "coverageStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageIssuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "coverageIssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trailerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "trailerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "faceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sumInsured");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "sumInsured"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matExpiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "matExpiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "clientType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageStatusChangeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "coverageStatusChangeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contestablePeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "contestablePeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
