package com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail;

public class GetPolicyCoverageDetailProxy implements com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail {
  private String _endpoint = null;
  private com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail getPolicyCoverageDetail = null;
  
  public GetPolicyCoverageDetailProxy() {
    _initGetPolicyCoverageDetailProxy();
  }
  
  public GetPolicyCoverageDetailProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetPolicyCoverageDetailProxy();
  }
  
  private void _initGetPolicyCoverageDetailProxy() {
    try {
      getPolicyCoverageDetail = (new com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.Getpolicycoveragedetail_client_epLocator()).getGetPolicyCoverageDetail_pt();
      if (getPolicyCoverageDetail != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getPolicyCoverageDetail)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getPolicyCoverageDetail)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getPolicyCoverageDetail != null)
      ((javax.xml.rpc.Stub)getPolicyCoverageDetail)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail getGetPolicyCoverageDetail() {
    if (getPolicyCoverageDetail == null)
      _initGetPolicyCoverageDetailProxy();
    return getPolicyCoverageDetail;
  }
  
  public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.ProcessResponse process(com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.Process payload) throws java.rmi.RemoteException{
    if (getPolicyCoverageDetail == null)
      _initGetPolicyCoverageDetailProxy();
    return getPolicyCoverageDetail.process(payload);
  }
  
  
}