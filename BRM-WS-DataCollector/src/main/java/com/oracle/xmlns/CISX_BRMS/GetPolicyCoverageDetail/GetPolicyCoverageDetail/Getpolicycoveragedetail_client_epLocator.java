/**
 * Getpolicycoveragedetail_client_epLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail;

public class Getpolicycoveragedetail_client_epLocator extends org.apache.axis.client.Service implements com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.Getpolicycoveragedetail_client_ep {

    public Getpolicycoveragedetail_client_epLocator() {
    }


    public Getpolicycoveragedetail_client_epLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Getpolicycoveragedetail_client_epLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetPolicyCoverageDetail_pt
    private java.lang.String GetPolicyCoverageDetail_pt_address = "http://PeripheralAppI02:8501/MTL_SOA_CISX/BRMS/GetPolicyCoveragesService/ProxyService/GetPolicyCoveragesServicePS";

    public java.lang.String getGetPolicyCoverageDetail_ptAddress() {
        return GetPolicyCoverageDetail_pt_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetPolicyCoverageDetail_ptWSDDServiceName = "GetPolicyCoverageDetail_pt";

    public java.lang.String getGetPolicyCoverageDetail_ptWSDDServiceName() {
        return GetPolicyCoverageDetail_ptWSDDServiceName;
    }

    public void setGetPolicyCoverageDetail_ptWSDDServiceName(java.lang.String name) {
        GetPolicyCoverageDetail_ptWSDDServiceName = name;
    }

    public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail getGetPolicyCoverageDetail_pt() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetPolicyCoverageDetail_pt_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetPolicyCoverageDetail_pt(endpoint);
    }

    public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail getGetPolicyCoverageDetail_pt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetailBindingStub _stub = new com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetailBindingStub(portAddress, this);
            _stub.setPortName(getGetPolicyCoverageDetail_ptWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetPolicyCoverageDetail_ptEndpointAddress(java.lang.String address) {
        GetPolicyCoverageDetail_pt_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail.class.isAssignableFrom(serviceEndpointInterface)) {
                com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetailBindingStub _stub = new com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetailBindingStub(new java.net.URL(GetPolicyCoverageDetail_pt_address), this);
                _stub.setPortName(getGetPolicyCoverageDetail_ptWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetPolicyCoverageDetail_pt".equals(inputPortName)) {
            return getGetPolicyCoverageDetail_pt();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "getpolicycoveragedetail_client_ep");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://xmlns.oracle.com/CISX_BRMS/GetPolicyCoverageDetail/GetPolicyCoverageDetail", "GetPolicyCoverageDetail_pt"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetPolicyCoverageDetail_pt".equals(portName)) {
            setGetPolicyCoverageDetail_ptEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
