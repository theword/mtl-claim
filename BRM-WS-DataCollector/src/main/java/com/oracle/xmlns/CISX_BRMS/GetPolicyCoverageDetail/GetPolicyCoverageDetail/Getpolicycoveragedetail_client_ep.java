/**
 * Getpolicycoveragedetail_client_ep.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail;

public interface Getpolicycoveragedetail_client_ep extends javax.xml.rpc.Service {
    public java.lang.String getGetPolicyCoverageDetail_ptAddress();

    public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail getGetPolicyCoverageDetail_pt() throws javax.xml.rpc.ServiceException;

    public com.oracle.xmlns.CISX_BRMS.GetPolicyCoverageDetail.GetPolicyCoverageDetail.GetPolicyCoverageDetail getGetPolicyCoverageDetail_pt(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
