package com.mtl.hospital.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarHelper {
	
	private static final String DATE_PATTERN = "dd/MM/yyyy";
	private static final String TIME_PATTERN = "HH:mm";
	private static final String DATETIME_PATTERN = "dd/MM/yyyy HH:mm";
	private static final String LONG_DATE_PATTERN = "dd MMM yyyy";
	private static final String LONG_DATETIME_PATTERN = "dd MMM yyyy HH:mm";
	private static final String FULL_DATE_PATTERN = "dd MMMMM yyyy";
	private static final String SORT_DATE_PATTERN = "yyyy/MM/dd";
	private static final String DB_DATE_PATTERN = "yyyy-MM-dd";
	private static final String COMMENT_DATE_PATTERN = "dd-MM-yyyy";
	private static final String YEAR_ONLY_PATTERN = "yyyy";
	private static final String MONTH_ONLY_PATTERN = "M";
	//private static final String DB_TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String ORCL_DB_TIMESTAMP_PATTERN_EN = "yyyy-MM-dd HH:mm:ss";
	private static final String EXPORT_DATE_PATTERN = "yyyyMMdd";
	private static final String SCALE_DATE_PATTERN = "yyyyMMddHHmmssSSS";
	
	private static final Locale THAI_LOCALE = new Locale("th", "TH");
	private static final Locale ENGLISH_LOCALE = Locale.US;

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			DATE_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat(
			TIME_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat(
			DATETIME_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat LONG_DATE_FORMAT = new SimpleDateFormat(
			LONG_DATE_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat LONG_DATETIME_FORMAT = new SimpleDateFormat(
			LONG_DATETIME_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat FULL_DATE_FORMAT = new SimpleDateFormat(
			FULL_DATE_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat SORT_DATE_FORMAT = new SimpleDateFormat(
			SORT_DATE_PATTERN, THAI_LOCALE);
	
	public static final SimpleDateFormat DATE_FORMAT_EN = new SimpleDateFormat(
			DATE_PATTERN, ENGLISH_LOCALE);
	public static final SimpleDateFormat TIME_FORMAT_EN = new SimpleDateFormat(
			TIME_PATTERN, ENGLISH_LOCALE);
	public static final SimpleDateFormat DATETIME_FORMAT_EN = new SimpleDateFormat(
			DATETIME_PATTERN, ENGLISH_LOCALE);

	public static final SimpleDateFormat ORCL_DB_TIMESTAMP_FORMAT_EN = new SimpleDateFormat(
			ORCL_DB_TIMESTAMP_PATTERN_EN, ENGLISH_LOCALE);
	
	public static final SimpleDateFormat YEAR_ONLY_FORMAT_EN = new SimpleDateFormat(
			YEAR_ONLY_PATTERN, ENGLISH_LOCALE);
	
	public static final SimpleDateFormat DATE_EXPORT_FORMAT_EN = new SimpleDateFormat(
			EXPORT_DATE_PATTERN, ENGLISH_LOCALE);
	
	public static final SimpleDateFormat DB_DATE_FORMAT = new SimpleDateFormat(DB_DATE_PATTERN,Locale.US);
//	public static final SimpleDateFormat DB_TIMESTAMP_FORMAT = new SimpleDateFormat(DB_TIMESTAMP_PATTERN,Locale.US);
//	public static final SimpleDateFormat ENG_YEAR_ONLY_FORMAT = new SimpleDateFormat(YEAR_ONLY_PATTERN,Locale.US);
	public static final SimpleDateFormat WF_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);

	public static final SimpleDateFormat THAI_YEAR_ONLY_FORMAT = new SimpleDateFormat(YEAR_ONLY_PATTERN,THAI_LOCALE);
	public static final SimpleDateFormat COMMENT_DATE_FORMAT = new SimpleDateFormat(COMMENT_DATE_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat THAI_MONTH_ONLY_FORMAT = new SimpleDateFormat(MONTH_ONLY_PATTERN,THAI_LOCALE);
	public static final SimpleDateFormat DATET_FORMAT_EXPORT = new SimpleDateFormat(EXPORT_DATE_PATTERN, ENGLISH_LOCALE);
	public static final SimpleDateFormat THAI_DATE_FORMAT_EXPORT = new SimpleDateFormat(EXPORT_DATE_PATTERN, THAI_LOCALE);
	public static final SimpleDateFormat DATE_FORMAT_SCALE = new SimpleDateFormat(SCALE_DATE_PATTERN, ENGLISH_LOCALE);
	
	
	static {
		DATE_FORMAT.setLenient(false);
		TIME_FORMAT.setLenient(false);
		DATETIME_FORMAT.setLenient(false);
		LONG_DATE_FORMAT.setLenient(false);
		FULL_DATE_FORMAT.setLenient(false);
		SORT_DATE_FORMAT.setLenient(false);
		DB_DATE_FORMAT.setLenient(false);
//		DB_TIMESTAMP_FORMAT.setLenient(false);
		THAI_YEAR_ONLY_FORMAT.setLenient(false);
//		ENG_YEAR_ONLY_FORMAT.setLenient(false);
		DATE_FORMAT_EN.setLenient(false);
		TIME_FORMAT_EN.setLenient(false);
		DATETIME_FORMAT_EN.setLenient(false);
		ORCL_DB_TIMESTAMP_FORMAT_EN.setLenient(false);
	}
	
	private CalendarHelper() {
		
	}

	@SuppressWarnings("deprecation")
	public static int getDiffMonth(Date beginDate,Date afterDate){
		int diffMonth = 0;
		if(beginDate != null && afterDate != null){
			int diffYear = afterDate.getYear() - beginDate.getYear();
			diffMonth = diffYear * 12 + afterDate.getMonth() - beginDate.getMonth();
			int diffDay = afterDate.getDay() - beginDate.getDay();
			if(diffDay > 0){
				diffMonth = diffMonth + 1;
			}
		}
		return diffMonth;
	}
	
	@SuppressWarnings("deprecation")
	public static int getDiffYear(Date beginDate,Date afterDate){
		int diffYear = -1;
		if(beginDate != null && afterDate != null){
			diffYear = afterDate.getYear() - beginDate.getYear();
		}
		return diffYear;
	}
	
	public static boolean monthBefore(Calendar cal1, Calendar cal2) { // dateof(cal1)
																		// <
																		// dateof(cal2)
		if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return false;
		}
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return true;
		}
		if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return false;
		}
		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return true;
		}
		return true;
	}

	public static boolean monthAfter(Calendar cal1, Calendar cal2) { // dateof(cal1)
																		// >
																		// dateof(cal2)
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return false;
		}
		if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return true;
		}
		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return false;
		}
		if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return true;
		}
		return true;
	}

	public static boolean dateBefore(Calendar cal1, Calendar cal2) { // dateof(cal1)
																		// <
																		// dateof(cal2)
		if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return false;
		}
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return true;
		}
		if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return false;
		}
		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return true;
		}
		if (cal1.get(Calendar.DATE) >= cal2.get(Calendar.DATE)) {
			return false;
		}
		return true;
	}

	public static boolean dateAfter(Calendar cal1, Calendar cal2) { // dateof(cal1)
																	// >
																	// dateof(cal2)
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return false;
		}
		if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return true;
		}
		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return false;
		}
		if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return true;
		}
		if (cal1.get(Calendar.DATE) <= cal2.get(Calendar.DATE)) {
			return false;
		}
		return true;
	}

	public static boolean dateBetween(Calendar cal, Calendar cal1, Calendar cal2) { // dateof(cal)
																					// >=
																					// dateof(cal1)
																					// &&
																					// dateof(cal)
																					// <=
																					// dateof(cal2)
		return !(dateBefore(cal, cal1) || dateAfter(cal, cal2));
	}

	public static boolean dateEquals(Calendar cal1, Calendar cal2) { // dateof(cal1)
																		// ==
																		// dateof(cal2)
		return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) && cal1
				.get(Calendar.DATE) == cal2.get(Calendar.DATE));
	}

	public static boolean timeBefore(Calendar cal1, Calendar cal2) { // timeof(cal1)
																		// <
																		// timeof(cal2)
		if (cal1.get(Calendar.HOUR_OF_DAY) > cal2.get(Calendar.HOUR_OF_DAY)) {
			return false;
		}
		if (cal1.get(Calendar.HOUR_OF_DAY) < cal2.get(Calendar.HOUR_OF_DAY)) {
			return true;
		}
		if (cal1.get(Calendar.MINUTE) > cal2.get(Calendar.MINUTE)) {
			return false;
		}
		if (cal1.get(Calendar.MINUTE) < cal2.get(Calendar.MINUTE)) {
			return true;
		}
		if (cal1.get(Calendar.SECOND) >= cal2.get(Calendar.SECOND)) {
			return false;
		}
		return true;
	}

	public static boolean timeAfter(Calendar cal1, Calendar cal2) { // timeof(cal1)
																	// >
																	// timeof(cal2)
		if (cal1.get(Calendar.HOUR_OF_DAY) < cal2.get(Calendar.HOUR_OF_DAY)) {
			return false;
		}
		if (cal1.get(Calendar.HOUR_OF_DAY) > cal2.get(Calendar.HOUR_OF_DAY)) {
			return true;
		}
		if (cal1.get(Calendar.MINUTE) < cal2.get(Calendar.MINUTE)) {
			return false;
		}
		if (cal1.get(Calendar.MINUTE) > cal2.get(Calendar.MINUTE)) {
			return true;
		}
		if (cal1.get(Calendar.SECOND) <= cal2.get(Calendar.SECOND)) {
			return false;
		}
		return true;
	}

	public static boolean timeBetween(Calendar cal, Calendar cal1, Calendar cal2) { // timeof(cal)
																					// >=
																					// timeof(cal2)
		return !(timeBefore(cal, cal1) || timeAfter(cal, cal2));
	}

	public static boolean timeBetween(Calendar cal, String time1, String time2) { // timeof(cal)
																					// >=
																					// hh:mm:ss)
		return timeBetween(cal, parseTime(time1), parseTime(time2));
	}

	public static boolean timeEquals(Calendar cal1, Calendar cal2) { // dateof(cal1)
																		// ==
																		// dateof(cal2)
		return (cal1.get(Calendar.HOUR_OF_DAY) == cal2
				.get(Calendar.HOUR_OF_DAY)
				&& cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE) && cal1
				.get(Calendar.SECOND) <= cal2.get(Calendar.SECOND));
	}

	public static Calendar merge(Calendar date, Calendar time) {
		Calendar cal = Calendar.getInstance(THAI_LOCALE);
		cal.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date
				.get(Calendar.DATE), time.get(Calendar.HOUR_OF_DAY), time
				.get(Calendar.MINUTE), time.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, time.get(Calendar.MILLISECOND));
		return cal;
	}

	public static String dateStringOf(Calendar cal) {
		if (cal == null) {
			return "";
		}
		return DATE_FORMAT.format(cal.getTime());
	}

	// public static String dateStringOf(Calendar cal, String datePattern,
	// String _default ) {
	// if ( cal == null ) return _default;
	// SimpleDateFormat sdf = new SimpleDateFormat( datePattern, new
	// Locale("th","TH") );
	// return sdf.format( cal.getTime() );
	// }

	public static String timeStringOf(Calendar cal) {
		if (cal == null) {
			return "";
		}
		return TIME_FORMAT.format(cal.getTime());
	}

	public static String dateTimeStringOf(Calendar cal) {
		if (cal == null) {
			return "";
		}
		return DATETIME_FORMAT.format(cal.getTime());
	}

	public static int dateDiff(Calendar c1, Calendar c2) { // return
															// dateof(cal1) -
															// dateof(cal2)
		if (c1 == null || c2 == null) {
			return 0;
		}
		c1 = truncateDate(c1);
		c2 = truncateDate(c2);

		long l1, l2;
		long l3;
		l1 = c1.getTime().getTime();
		l2 = c2.getTime().getTime();
		l3 = (l1 - l2) / (60 * 60 * 24 * 1000);
		return (int) l3;
	}
	
	public static int dateDiff(Date d1, Date d2) { // return
															// dateof(cal1) -
															// dateof(cal2)
		if (d1 == null || d2 == null) {
		return 0;
		}
		
		long l1, l2;
		long l3;
		l1 = d1.getTime();
		l2 = d2.getTime();
		l3 = (l1 - l2) / (60 * 60 * 24 * 1000);
		return (int) l3;
	}

	/**
	 * 
	 * @param dateString
	 * @param oDateFormat
	 * @return
	 */
	private static Calendar getCalendar(String dateString,
			SimpleDateFormat oDateFormat) {
		return getCalendar(dateString, oDateFormat, Calendar
				.getInstance(THAI_LOCALE));
	}

	/**
	 * 
	 * @param dateString
	 * @param oDateFormat
	 * @return
	 */
	private static Calendar getCalendar(String dateString,
			SimpleDateFormat oDateFormat, Calendar _default) {
		Calendar oCalendar = Calendar.getInstance(THAI_LOCALE);
		try {
			oCalendar.setTime(oDateFormat.parse(dateString));
		} catch (Exception e) {
			if ( _default != null) {
				oCalendar.setTime(_default.getTime());
			} else {
				oCalendar = null;
			}
		}

		return oCalendar;
	}

	public static Calendar getCurrentDate() {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
				c.get(Calendar.DATE), 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	public static Calendar getCurrentDateTime() {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		return c;
	}

	public static Calendar truncateDate(Calendar c) {
		Calendar cTmp = Calendar.getInstance(THAI_LOCALE);
		cTmp.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c
				.get(Calendar.DATE), 0, 0, 0);
		cTmp.set(Calendar.MILLISECOND, 0);
		return cTmp;
	}
	
	public static Calendar roundUpDate(Calendar c){
		Calendar cTmp = Calendar.getInstance(THAI_LOCALE);
		cTmp.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c
				.get(Calendar.DATE), 23, 59, 59);
		cTmp.set(Calendar.MILLISECOND, 999);
		return cTmp;
	}

	public static Calendar getDate(int year, int month, int day) {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		c.set(year, month, day, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	public static void setTime(Calendar c, int hour, int minute, int second) {
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, second);
		c.set(Calendar.MILLISECOND, 0);
	}

	public static void setTime(Calendar c, int hour, int minute, int second,
			int millisecond) {
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, second);
		c.set(Calendar.MILLISECOND, millisecond);
	}

	public static void rollToDay(Calendar c, int dayOfWeek) {
		c.add(Calendar.DAY_OF_YEAR,
				(dayOfWeek - c.get(Calendar.DAY_OF_WEEK) + 7) % 7);
	}

	public static int getRealWeekOfYear(Calendar c) {
		Calendar cTmp = getFirstDateOfYear(c.get(Calendar.YEAR));
		rollToDay(cTmp, c.get(Calendar.DAY_OF_WEEK));
		return (c.get(Calendar.DAY_OF_YEAR) - cTmp.get(Calendar.DAY_OF_YEAR)) / 7;
	}

	public static Calendar getLastDayOfMonth(int year ,int month){
		Calendar c = parseDate("01/"+month+"/"+year);
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, -1);
		
		return c;
	}
	
	public static Calendar getFirstDateOfYear(int year) {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		c.set(year, Calendar.JANUARY, 1, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	public static Calendar getLastDateOfYear(int year) {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		c.set(year, Calendar.DECEMBER, 31, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	public static Calendar getFirstDateOfYear() {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		c.set(c.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	public static Calendar getLastDateOfYear() {
		Calendar c = Calendar.getInstance(THAI_LOCALE);
		c.set(c.get(Calendar.YEAR), Calendar.DECEMBER, 31, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	public static String formatDate(Date d) {
		return d != null ? DATE_FORMAT.format(d) : "";
	}

	public static String formatDate(Calendar cal) {
		return cal != null ? formatDate(cal.getTime()) : "";
	}
	
	public static String formatDateEn(Date d) {
		return d != null ? DATE_FORMAT_EN.format(d) : "";
	}
	
	public static String formatDateEn(Calendar cal) {
		return cal != null ? formatDateEn(cal.getTime()) : "";
	}
	public static String formatWorkFlowDate(Calendar cal) {
		return cal != null ? WF_DATE_FORMAT.format(cal.getTime()) : "";
	}

	public static String formatTime(Date d) {
		return d != null ? TIME_FORMAT.format(d) : "";
	}

	public static String formatTime(Calendar cal) {
		return cal != null ? formatTime(cal.getTime()) : "";
	}
	
	public static String formatTimeEn(Date d) {
		return d != null ? TIME_FORMAT_EN.format(d) : "";
	}

	public static String formatTimeEn(Calendar cal) {
		return cal != null ? formatTimeEn(cal.getTime()) : "";
	}

	public static String formatDateTime(Date d) {
		return d != null ? DATETIME_FORMAT.format(d) : "";
	}

	public static String formatDateTime(Calendar cal) {
		return cal != null ? formatDateTime(cal.getTime()) : "";
	}
	
	public static String formatDateTimeEn(Date d) {
		return d != null ? DATETIME_FORMAT_EN.format(d) : "";
	}

	public static String formatDateTimeEn(Calendar cal) {
		return cal != null ? formatDateTimeEn(cal.getTime()) : "";
	}

	public static String formatSortDate(Date d) {
		return d != null ? SORT_DATE_FORMAT.format(d) : "";
	}

	public static String formatSortDate(Calendar cal) {
		return cal != null ? formatSortDate(cal.getTime()) : "";
	}

	public static String formatLongDate(Date d) {
		return d != null ? LONG_DATE_FORMAT.format(d) : "";
	}

	public static String formatLongDate(Calendar cal) {
		return cal != null ? formatLongDate(cal.getTime()) : "";
	}
	
	public static String formatLongDateTime(Date d) {
		return d != null ? LONG_DATETIME_FORMAT.format(d) : "";
	}

	public static String formatLongDateTime(Calendar cal) {
		return cal != null ? formatLongDateTime(cal.getTime()) : "";
	}
	
	public static String formatFullDate(Date d) {
		return d != null ? FULL_DATE_FORMAT.format(d) : "";
	}

	public static String formatFullDate(Calendar cal) {
		return cal != null ? formatFullDate(cal.getTime()) : "";
	}

	public static Calendar parseDate(String date) {
		return getCalendar(date, DATE_FORMAT);
	}
	
	public static Calendar parseDateEn(String date) {
		return getCalendar(date, DATE_FORMAT_EN);
	}
	
	public static Calendar parseDBDate(String date) {
		return getCalendar(date, DB_DATE_FORMAT);
	}

	public static Calendar parseDateExportTh(String date) {
		return getCalendar(date, THAI_DATE_FORMAT_EXPORT);
	}
	
	public static Calendar parseDate(String date, Calendar _default) {
		return getCalendar(date, DATE_FORMAT, _default);
	}
	
	public static Calendar parseDate(String date, SimpleDateFormat oDateFormat, Calendar _default) {
		return getCalendar(date, oDateFormat, _default);
	}

	public static Calendar parseTime(String time) {
		return getCalendar(time, TIME_FORMAT);
	}
	
	public static Calendar parseDateTime(String dateTime) {
		return getCalendar(dateTime, DATETIME_FORMAT);
	}

	public static boolean validateDate(String date) {
		try {
			DATE_FORMAT.parse(date);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public static boolean validateDateTime(String dateTime) {
		try {
			DATETIME_FORMAT.parse(dateTime);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public static boolean validateTime(String time) {
		try {
			TIME_FORMAT.parse(time);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	public static String formatDateTimeExport(Date d) {
		return d != null ? DATET_FORMAT_EXPORT.format(d) : "";
	}

	public static String formatDateTimeExport(Calendar cal) {
		return cal != null ? formatDateTimeExport(cal.getTime()) : "";
	}
//	public static String convertBEYearToADYear(String thaiYear){
//		Calendar cal = getCalendar(thaiYear, THAI_YEAR_ONLY_FORMAT, Calendar
//				.getInstance(THAI_LOCALE));
//		
//		return ENG_YEAR_ONLY_FORMAT.format(cal.getTime());
//	}

//	public static String convertADYearToBEYear(String thaiYear){
//		Calendar cal = getCalendar(thaiYear, ENG_YEAR_ONLY_FORMAT, Calendar
//				.getInstance(THAI_LOCALE));
//		
//		return THAI_YEAR_ONLY_FORMAT.format(cal.getTime());
//	}
	
	public static void main(String[] s) {

    	String locale = "th_TH";
        Locale preferredLocale = null;
        if (locale != null) {
            int indexOfUnderscore = locale.indexOf('_');
            if (indexOfUnderscore != -1) {
                String language = locale.substring(0, indexOfUnderscore);
                String country = locale.substring(indexOfUnderscore + 1);
                preferredLocale = new Locale(language, country);
            } else {
                preferredLocale = new Locale(locale);
            }
        }
        System.out.println(preferredLocale);
        Calendar cal = Calendar.getInstance();
        System.out.println(cal.get(Calendar.YEAR));
        
	}
}
