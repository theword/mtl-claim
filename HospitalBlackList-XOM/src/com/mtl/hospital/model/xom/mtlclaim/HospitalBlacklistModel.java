/*
 * 
 */
package com.mtl.hospital.model.xom.mtlclaim;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.hospital.model.RuleMessage;
import com.mtl.hospital.model.mtlclaim.Hospital;

/**
 * The Class HospitalBlacklistModel.
 */
@XmlRootElement(name = "HospitalBlacklistModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class HospitalBlacklistModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6576209028387314419L;

	/** The hospital. */
	@XmlElement(name = "hospital", required = false)
	private Hospital hospital = new Hospital();

	@XmlElement(name = "ruleMessages", required = false)
	private List<RuleMessage> ruleMessages = new ArrayList<RuleMessage>();

	private int status = 0;
	
	public Hospital getHospital() {
		return this.hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public RuleMessage newRuleMessage(String key, String blacklistValue) {
		RuleMessage ruleMessage = new RuleMessage();
		ruleMessage.setHospitalStatus(key, blacklistValue);
		ruleMessages.add(ruleMessage);
		return ruleMessage;
	}

	public String getHospitalStatus(String key) {
		return this.hospital.getHospital().getHospitalStatus(key);
	}

	public List<RuleMessage> getRuleMessages() {
		return this.ruleMessages;
	}

	public void setRuleMessages(List<RuleMessage> ruleMessages) {
		this.ruleMessages = ruleMessages;
	}

	public void putHospitalStatus(String key, String value){
		this.hospital.getHospital().putHospitalStatus(key, value);
	}
	
	public String getHospitalCode() {
		return this.hospital.getHospital().getHospitalCode();
	}
	
	public String getHospitalName(){
		return this.hospital.getHospital().getHospitalName();
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
