/*
 * 
 */
package com.mtl.hospital.model.mtlclaim;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.hospital.model.mtlclaim.master.HospitalMs;


/**
 * The Class Hospital.
 */
@XmlRootElement(name = "Hospital")
@XmlAccessorType(XmlAccessType.FIELD)
public class Hospital implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9210236672842416892L;
	
	/** The hospital. */
	@XmlElement(name = "hospital", required = false)
	private HospitalMs hospital = new HospitalMs();

	public HospitalMs getHospital() {
		return hospital;
	}

	public void setHospital(HospitalMs hospital) {
		this.hospital = hospital;
	}
	
}
