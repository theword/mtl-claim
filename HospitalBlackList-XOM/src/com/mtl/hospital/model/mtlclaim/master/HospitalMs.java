/*
 * 
 */
package com.mtl.hospital.model.mtlclaim.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.hospital.model.Map;

/**
 * The Class HospitalMs.
 */
@XmlRootElement(name = "HospitalMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class HospitalMs implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5131226600706061405L;
	
	/** The hospital code. */
	@XmlElement(name = "hospitalCode", required = false)
	private String hospitalCode = "";
	
	@XmlElement(name = "hospitalName", required = false)
	private String hospitalName = "";
	
	@XmlElement(name = "hospitalStatus", required = false)
	private List<Map> hospitalStatus =  new ArrayList<Map>();
	
	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalStatus(String key){
		for(Map m : this.hospitalStatus){
			if(m.getKey().equals(key)){
				return m.getValue();
			}
		}
		return "";
	}
	
	public void putHospitalStatus(String key, String value){
		Map m = new Map();
		m.setKey(key);
		m.setValue(value);
		this.hospitalStatus.add(m);
	}

	public List<Map> getHospitalStatus() {
		return hospitalStatus;
	}

	public void setHospitalStatus(List<Map> hospitalStatus) {
		this.hospitalStatus = hospitalStatus;
	}
}
