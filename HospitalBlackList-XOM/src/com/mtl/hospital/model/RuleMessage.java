/*
 * 
 */
package com.mtl.hospital.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RuleMessage")
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleMessage implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5879743283996951110L;
	
	@XmlElement(name = "messageCode", required = false)
	private String messageCode = "";

	@XmlElement(name = "message", required = false)
	private String message = "";
	
	@XmlElement(name = "hospitalStatus", required = false)
	private Map hospitalStatus = new Map();
	

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setHospitalStatus(String key, String value){
		this.hospitalStatus.setKey(key);
		this.hospitalStatus.setValue(value);
	}

	public Map getHospitalStatus(){
		return this.hospitalStatus;
	}
}
