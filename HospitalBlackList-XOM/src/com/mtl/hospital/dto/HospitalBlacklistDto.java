/*
 * 
 */
package com.mtl.hospital.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class HospitalBlacklistDto.
 */
@XmlRootElement(name = "HospitalBlacklistDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class HospitalBlacklistDto {
	
	/** The hospital code. */
	@XmlElement(name = "hospitalCode", required = false)
	private String hospitalCode;
	
	/**
	 * Instantiates a new hospital blacklist dto.
	 */
	public HospitalBlacklistDto() {
		
	}

	/**
	 * Sets the hospital code.
	 *
	 * @param hospitalCode the new hospital code
	 */
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
	/**
	 * Gets the hospital code.
	 *
	 * @return the hospital code
	 */
	public String getHospitalCode() {
		return hospitalCode;
	}
}
