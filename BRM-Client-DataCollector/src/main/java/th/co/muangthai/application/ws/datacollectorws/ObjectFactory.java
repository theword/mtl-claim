
package th.co.muangthai.application.ws.datacollectorws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the th.co.muangthai.application.ws.datacollectorws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataCollectorDto_QNAME = new QName("http://www.dataCollectorWs.ws.application.muangthai.co.th/", "DataCollectorDto");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: th.co.muangthai.application.ws.datacollectorws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataCollectorDto }
     * 
     */
    public DataCollectorDto createDataCollectorDto() {
        return new DataCollectorDto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataCollectorDto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.dataCollectorWs.ws.application.muangthai.co.th/", name = "DataCollectorDto")
    public JAXBElement<DataCollectorDto> createDataCollectorDto(DataCollectorDto value) {
        return new JAXBElement<DataCollectorDto>(_DataCollectorDto_QNAME, DataCollectorDto.class, null, value);
    }

}
