package com.mtl.rules.common.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T, PK extends Serializable> {

	public abstract T get(PK id);

	public abstract List<T> getAll();

	public abstract PK save(T entity);

	public abstract void delete(T entity);

	public abstract void delete(PK id);

	public abstract boolean exists(PK id);

	public abstract void delete(PK[] ids);

	public abstract void saveOrUpdate(T entity);
	
}
