package com.mtl.rules.common.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.mtl.rules.common.dao.GenericDao;



public class GenericDaoImpl<T, PK extends Serializable> extends HibernateDaoSupport implements GenericDao<T, PK> {

	protected Logger log = Logger.getLogger(getClass());
	protected Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public Criteria createCriteria(){
		return this.getSession().createCriteria(persistentClass);
	}
	//*** for use getHibernateTemplate of Spring manage -> control 1 Session, 1 Connection ***
	public DetachedCriteria createDetachedCriteria(){
		return DetachedCriteria.forClass(persistentClass);
	}
	
	public T get(PK id) {
		T entity = (T) this.getHibernateTemplate().get(this.persistentClass, id);

		if (entity == null) {
			String msg = "Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...";
			log.warn(msg);
		}

		return entity;
	}

	public List<T> getAll() {

		return this.getHibernateTemplate().loadAll(persistentClass);
	}

	@SuppressWarnings("unchecked")
	public PK save(T entity) {
		return (PK)this.getHibernateTemplate().save(entity);
	}
	
	public void update(T entity) {
		this.getHibernateTemplate().update(entity);
	}
	
	public void saveOrUpdate(T entity){
		this.getHibernateTemplate().saveOrUpdate(entity);
	}
	
	public void delete(T entity) {
		this.getHibernateTemplate().delete(entity);
	}

	public void delete(PK id) {
		this.getHibernateTemplate().delete(this.get(id));
	}

	public boolean exists(PK id) {
		T entity = (T) this.get(id);

		if (entity == null) {
			return false;
		} else {
			return true;
		}
	}

	public void delete(PK[] ids) {
		if (ids != null) {
			for (PK id : ids) {
				delete(id);
			}
		}
	}

	public List<T> findAll() {
		return find("from " + persistentClass.getName());
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(String hql){
		return this.getSession().createQuery(hql).list();
	}
}