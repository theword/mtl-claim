package com.mtl.rules.jdbc.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.mtl.rules.jdbc.dao.GenericJdbcDao;


@Repository
public class GenericJdbcDaoImpl extends JdbcDaoSupport implements GenericJdbcDao{

	public JdbcTemplate getTemplate(){
		return this.getJdbcTemplate();
	}
	
}
