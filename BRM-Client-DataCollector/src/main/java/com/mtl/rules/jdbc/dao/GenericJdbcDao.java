package com.mtl.rules.jdbc.dao;

import org.springframework.jdbc.core.JdbcTemplate;


public interface GenericJdbcDao {

	public JdbcTemplate getTemplate();
	
}
