package com.mtl.rules.utils;

import org.apache.log4j.Logger;

import th.co.muangthai.application.ws.datacollectorws.DataCollectorDto;


public class WebServiceUtils {
	
	private static Logger logger = Logger.getLogger(WebServiceUtils.class);
	
	public static void printLogHeader(DataCollectorDto header){
		logger.info("$");
		logger.info("$");
		logger.info("____________ HEADER ___________");
		logger.info(">>>> ServiceID :: '" + header.getServiceID() + "'");
		logger.info(">>>> CallName :: '" + header.getCallName()+ "'");
		logger.info(">>>> RequestDate :: '" + header.getRequestedDate()+ "'");
		logger.info(">>>> RespondDate :: '" + header.getRespondDate()+ "'");
		logger.info(">>>> Status :: '" + header.getStatus()+ "'");
		logger.info(">>>> Reason :: '" + header.getReason()+ "'");
		logger.info("$");
		logger.info("$");
	}
	
}
