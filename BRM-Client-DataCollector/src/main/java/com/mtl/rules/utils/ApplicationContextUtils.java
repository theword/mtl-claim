package com.mtl.rules.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextUtils implements ApplicationContextAware {

	private static ApplicationContext ctx;
	
	public static Object getBean(String name)
	{
		if(ctx==null){
			initialize();
		}
		
		return ctx.getBean(name);
	}

	protected static void initialize(){
		
		ctx = new ClassPathXmlApplicationContext(
				new String[]{
						"applicationContext-service-datacoll-client.xml"});
	}

	 
	public void setApplicationContext(ApplicationContext appCtx)
			throws BeansException {
		this.ctx = appCtx;
	}

}
