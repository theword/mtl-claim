package com.mtl.rules.test.sort;

import java.util.Comparator;

public enum BenifitComparator implements Comparator<BenifitModel>{
	CLAIM_CODE {
		public int compare(BenifitModel o1, BenifitModel o2) {
            return o1.getClaimCode().compareTo(o2.getClaimCode());
        }},        
    POLICY_SORT {		
		public int compare(BenifitModel o1, BenifitModel o2) {
            return o1.getPolicyNumber().compareTo(o2.getPolicyNumber());
        }},   
    COVERAGE_SORT {        
		public int compare(BenifitModel o1, BenifitModel o2) {
            return o1.getCoverageCode().compareTo(o2.getCoverageCode());
        }},
    BENIFIT_SORT {        
		public int compare(BenifitModel o1, BenifitModel o2) {
            return o1.getBenifitCode().compareTo(o2.getBenifitCode());
        }},
    SUM_INSURED_SORT {        
		public int compare(BenifitModel o1, BenifitModel o2) {
            return o1.getSumInsured().compareTo(o2.getSumInsured());
        }},
    EFFECTIVE_DATE_SORT {        
		public int compare(BenifitModel o1, BenifitModel o2) {
            return o1.getEffectiveDate().compareTo(o2.getEffectiveDate());
        }};
}
