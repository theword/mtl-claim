package com.mtl.rules.test.sort;

import java.util.Date;

import com.mtl.benefitAnalysis.util.CalendarHelper;

public class BenifitModel {
	private String claimCode;
	private String policyNumber;	
	private String coverageCode;
	private String benifitCode;	
	private Double sumInsured;
	private Date effectiveDate;
	
	public String toString(){
		String str = "[BenifitModel]";
		str = str+"claimCode:"+claimCode+"\t";
		str = str+"policyNumber:"+policyNumber+"\t";
		str = str+"coverageCode:"+coverageCode+"\t";
		str = str+"benifitCode:"+benifitCode+"\t";
		str = str+"sumInsured:"+sumInsured+"\t";		
		str = str+"effectiveDate:"+CalendarHelper.DATE_FORMAT_EN.format(effectiveDate)+"\t";		
		return str;
	}
	
	public String getClaimCode() {
		return claimCode;
	}
	public void setClaimCode(String claimCode) {
		this.claimCode = claimCode;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getCoverageCode() {
		return coverageCode;
	}
	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}
	public String getBenifitCode() {
		return benifitCode;
	}
	public void setBenifitCode(String benifitCode) {
		this.benifitCode = benifitCode;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Double getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(Double sumInsured) {
		this.sumInsured = sumInsured;
	}
	
}
