package com.mtl.rules.test.sort;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.ComparatorUtils;

import com.mtl.benefitAnalysis.util.CalendarHelper;

public class TestSorting {
	public static void main(String[] args) {
		System.out.println("---- begin Test ----");
		TestSorting t = new TestSorting();
		t.test2();
		System.out.println("---- end Test ----");
	}
	
	public void test1(){
		Comparator[] comparatorArr= {BenifitComparator.CLAIM_CODE,BenifitComparator.POLICY_SORT
				,BenifitComparator.COVERAGE_SORT,BenifitComparator.BENIFIT_SORT
				,BenifitComparator.SUM_INSURED_SORT,BenifitComparator.EFFECTIVE_DATE_SORT};
		Comparator<BenifitModel> orderbyAsc = ComparatorUtils.chainedComparator(comparatorArr);
		Comparator<BenifitModel> orderbyDesc = ComparatorUtils.reversedComparator(orderbyAsc);
		List<BenifitModel> benifitModelList = listBenifitModel();
		System.out.println("=======Before sort======");
		for(BenifitModel m1:benifitModelList){
			System.out.println(m1.toString());
		}
		Collections.sort(benifitModelList, orderbyDesc);
		System.out.println("=======After sort======");
		for(BenifitModel m2:benifitModelList){
			System.out.println(m2.toString());
		}
		
	}
	
	public void test2(){
		Comparator[] comparatorArr= {BenifitComparator.CLAIM_CODE,ComparatorUtils.reversedComparator(BenifitComparator.POLICY_SORT)
				,BenifitComparator.COVERAGE_SORT,BenifitComparator.BENIFIT_SORT
				,BenifitComparator.SUM_INSURED_SORT,BenifitComparator.EFFECTIVE_DATE_SORT};
		
		Comparator<BenifitModel> orderbyAsc = ComparatorUtils.chainedComparator(comparatorArr);
		Comparator<BenifitModel> orderbyDesc = ComparatorUtils.reversedComparator(orderbyAsc);
		List<BenifitModel> benifitModelList = listBenifitModel();
		System.out.println("=======Before sort======");
		for(BenifitModel m1:benifitModelList){
			System.out.println(m1.toString());
		}
		Collections.sort(benifitModelList, orderbyDesc);
		System.out.println("=======After sort======");
		for(BenifitModel m2:benifitModelList){
			System.out.println(m2.toString());
		}
		
	}
	
	public List<BenifitModel> listBenifitModel(){
		List<BenifitModel> benifitModelList = new ArrayList<BenifitModel>();
		for(int i=1;i<10;i++){
			Calendar cal = CalendarHelper.getCurrentDate();
			
			BenifitModel model = new BenifitModel();
			if(i<6){
				model.setClaimCode("C001");
			}else{
				model.setClaimCode("C002");
			}
			if(i%2==0){
				model.setPolicyNumber("P00"+(9-i));
			}else{
				model.setPolicyNumber("P00"+(10-i));
			}
			
			if(i<5){
				model.setCoverageCode("COV"+(50-i));
				cal.add(Calendar.MONTH, i*-1);
			}else{
				model.setCoverageCode("COV"+(30+i));
				cal.add(Calendar.MONTH, i);
			}
			model.setEffectiveDate(cal.getTime());
			model.setBenifitCode(i*100+"");
			model.setSumInsured(5000-i*1000d);
			benifitModelList.add(model);
		}
		return benifitModelList;		
	}
	
}
