package com.mtl.rules.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.log4j.Logger;

import th.co.muangthai.application.ws.datacollectorws.DataCollectorDto;
import th.co.muangthai.application.ws.datacollectorws.DataCollectorService;

import com.google.gson.Gson;
import com.mtl.benefitAnalysis.dto.BenefitAnalysisDto;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisModel;
import com.mtl.benefitAnalysis.util.CalendarHelper;
import com.mtl.continuity.dto.ContinuityClaimCheckingDto;
import com.mtl.continuity.model.xom.mtlclaim.ContinuityClaimCheckingModel;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.hospital.dto.HospitalBlacklistDto;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;
import com.mtl.rules.service.DataCollectorClientService;
import com.mtl.rules.utils.ResourceConfigManager;
import com.mtl.rules.utils.WebServiceUtils;

public class DataCollectorClientServiceImpl implements DataCollectorClientService {
	Logger log = Logger.getLogger(this.getClass());
	private Gson gsonService;
	private URL dataCollUrl;
	private QName dataCollQname;
	private String wsdlAddress;
	private String qname;
	
	public void init() {
		if (gsonService==null ){
			throw new RuntimeException("Daos or Services have not been set");
		}
		this.wsdlAddress = ResourceConfigManager.getProperty("rules.datacoll.ws.wsdl.address");
		this.qname = ResourceConfigManager.getProperty("rules.datacoll.ws.qname");
		initDataCollector(wsdlAddress, qname);
	}
	
	private void initDataCollector(String wsdlAddress,String qname){
		if(wsdlAddress==null||wsdlAddress==""||qname==null||qname==""){
			throw new RuntimeException("wsdlAddress or qname have not been set");
		}
		log.debug("DataCollectorClientServiceImpl,wsdlAddress:"+wsdlAddress);
		log.debug("DataCollectorClientServiceImpl,qname:"+qname);
		
		try{
			this.dataCollUrl = new URL(wsdlAddress);
		}catch (MalformedURLException e) {
			throw new RuntimeException("MalformedURLException [ruleUrl]:"+e);
		}				
        this.dataCollQname = new QName(qname, "DataCollectorService");
	}
	
	public void setServiceAddress(String wsdlAddress,String qname){
		initDataCollector(wsdlAddress, qname);
	}
	
	public BenefitAnalysisModel collectBenefitAnalysisModel(BenefitAnalysisDto benefitAnalysisDto,String callName){
//		BenefitAnalysisDto benefitAnalysisDto = new BenefitAnalysisDto();
		// connect service 		
		Service service = Service.create(dataCollUrl, dataCollQname);
		DataCollectorService dataColl = service.getPort(DataCollectorService.class);
		DataCollectorDto request = getRequestDataCollectorDto(callName);
		request.setJsonXom(gsonService.toJson(benefitAnalysisDto));
		
		// response
		DataCollectorDto response = dataColl.collectBenefitAnalysisModel(request);
		WebServiceUtils.printLogHeader(response);

		// manage	
		List<com.mtl.benefitAnalysis.model.RuleMessage> ruleMsgList = new ArrayList<com.mtl.benefitAnalysis.model.RuleMessage>();
		BenefitAnalysisModel model = new BenefitAnalysisModel(null, null, null);
		if(response!=null&&response.getJsonXom()!=null){
			model = gsonService.fromJson(response.getJsonXom(), BenefitAnalysisModel.class);
		}else{
			//ruleMsgList = underwriteModel.getRuleMesssages();
			com.mtl.benefitAnalysis.model.RuleMessage ruleMsg = new com.mtl.benefitAnalysis.model.RuleMessage();
			ruleMsg.setMessageCode("ERR001");
			ruleMsg.setMessage(ResourceConfigManager.getProperty("rules.message.error.ERR001"));			
			ruleMsg.setPriority(Short.MAX_VALUE);
			ruleMsgList.add(ruleMsg);
			model.setRuleMesssages(ruleMsgList);
			model = null;
		}		
		return model;
	}
	public EligibleModel collectEligibleModel(EligibleDto eligibleDto,String callName){
		// connect service 
		log.debug("collectEligibleModel policy : >>" + eligibleDto.getPolicyNumber() + "<<");
		Service service = Service.create(dataCollUrl, dataCollQname);
		DataCollectorService dataColl = service.getPort(DataCollectorService.class);
		DataCollectorDto request = getRequestDataCollectorDto(callName);
		request.setJsonXom(gsonService.toJson(eligibleDto));
		
		// response
		DataCollectorDto response = dataColl.collectEligibleModel(request);
		WebServiceUtils.printLogHeader(response);
		
		// manage	
		EligibleModel model = new EligibleModel(null, null, null, null, null, null, null, null, null, null,null,null,null,null,null);
		List<com.mtl.eligible.model.RuleMessage> ruleMsgList = new ArrayList<com.mtl.eligible.model.RuleMessage>();
		if(response!=null&&response.getJsonXom()!=null){
			model = gsonService.fromJson(response.getJsonXom(), EligibleModel.class);
		}else{
			//ruleMsgList = underwriteModel.getRuleMesssages();
			com.mtl.eligible.model.RuleMessage ruleMsg = new com.mtl.eligible.model.RuleMessage();
			ruleMsg.setMessageCode("ERR001");
			ruleMsg.setMessage(ResourceConfigManager.getProperty("rules.message.error.ERR001"));			
			ruleMsg.setPriority(Short.MAX_VALUE);
			ruleMsgList.add(ruleMsg);
			model.setRuleMesssages(ruleMsgList);
			model = null;
		}		
		return model;
	}
	public HospitalBlacklistModel collectHospitalBlacklistModel(HospitalBlacklistDto hospitalBlacklistDto,String callName){
		// connect service 
		Service service = Service.create(dataCollUrl, dataCollQname);
		DataCollectorService dataColl = service.getPort(DataCollectorService.class);
		DataCollectorDto request = getRequestDataCollectorDto(callName);
		request.setJsonXom(gsonService.toJson(hospitalBlacklistDto));
		
		// response
		DataCollectorDto response = dataColl.collectHospitalBlacklistModel(request);
		WebServiceUtils.printLogHeader(response);
		
		// manage	
		HospitalBlacklistModel model = new HospitalBlacklistModel();
		List<com.mtl.hospital.model.RuleMessage> ruleMsgList = new ArrayList<com.mtl.hospital.model.RuleMessage>();
		if(response!=null&&response.getJsonXom()!=null){
			model = gsonService.fromJson(response.getJsonXom(), HospitalBlacklistModel.class);
		}else{
			//ruleMsgList = underwriteModel.getRuleMesssages();
			com.mtl.hospital.model.RuleMessage ruleMsg = new com.mtl.hospital.model.RuleMessage();
			ruleMsg.setMessageCode("ERR001");
			ruleMsg.setMessage(ResourceConfigManager.getProperty("rules.message.error.ERR001"));			
			ruleMsgList.add(ruleMsg);
			model.setRuleMessages(ruleMsgList);
			model = null;
		}
		return model;
	}
	public ContinuityClaimCheckingModel collectContinuityClaimCheckingModel(ContinuityClaimCheckingDto continuityClaimCheckingDto,String callName){
		// connect service 
		Service service = Service.create(dataCollUrl, dataCollQname);
		DataCollectorService dataColl = service.getPort(DataCollectorService.class);
		DataCollectorDto request = getRequestDataCollectorDto(callName);
		request.setJsonXom(gsonService.toJson(continuityClaimCheckingDto));
		
		// response
		DataCollectorDto response = dataColl.collectContinuityClaimCheckingModel(request);
		WebServiceUtils.printLogHeader(response);
		
		// manage
		ContinuityClaimCheckingModel model = new ContinuityClaimCheckingModel(null, null, null, null, null, null);
		List<com.mtl.continuity.model.RuleMessage> ruleMsgList = new ArrayList<com.mtl.continuity.model.RuleMessage>();
		if(response!=null&&response.getJsonXom()!=null){
			model = gsonService.fromJson(response.getJsonXom(), ContinuityClaimCheckingModel.class);
		}else{
			//ruleMsgList = underwriteModel.getRuleMesssages();
			com.mtl.continuity.model.RuleMessage ruleMsg = new com.mtl.continuity.model.RuleMessage();
			ruleMsg.setMessageCode("ERR001");
			ruleMsg.setMessage(ResourceConfigManager.getProperty("rules.message.error.ERR001"));			
			ruleMsg.setPriority(Short.MAX_VALUE);
			ruleMsgList.add(ruleMsg);
			model.setRuleMesssages(ruleMsgList);
			model = null;
		}		
		return model;
	}
	
	private DataCollectorDto getRequestDataCollectorDto(String callName){
		DataCollectorDto dto = new DataCollectorDto();
		dto.setCallName(callName);
		dto.setServiceID(CalendarHelper.getCurrentDateTime().getTime().getTime()+"");
		dto.setRequestedDate(CalendarHelper.formatDate(CalendarHelper.getCurrentDateTime()));
		return dto;
	}
    
    //setter
    public void setGsonService(Gson gsonService) {
		this.gsonService = gsonService;
	}
	public void setDataCollUrl(URL dataCollUrl) {
		this.dataCollUrl = dataCollUrl;
	}
	public void setDataCollQname(QName dataCollQname) {
		this.dataCollQname = dataCollQname;
	}
	public void setWsdlAddress(String wsdlAddress) {
		this.wsdlAddress = wsdlAddress;
	}
	public void setQname(String qname) {
		this.qname = qname;
	}
	
    
}
