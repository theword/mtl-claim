package com.mtl.rules.service;

import com.mtl.benefitAnalysis.dto.BenefitAnalysisDto;
import com.mtl.benefitAnalysis.model.xom.mtlclaim.BenefitAnalysisModel;
import com.mtl.continuity.dto.ContinuityClaimCheckingDto;
import com.mtl.continuity.model.xom.mtlclaim.ContinuityClaimCheckingModel;
import com.mtl.eligible.dto.EligibleDto;
import com.mtl.eligible.model.xom.mtlclaim.EligibleModel;
import com.mtl.hospital.dto.HospitalBlacklistDto;
import com.mtl.hospital.model.xom.mtlclaim.HospitalBlacklistModel;


public interface DataCollectorClientService {	
	
	public void setServiceAddress(String wsdlAddress,String qname);
	
	public BenefitAnalysisModel collectBenefitAnalysisModel(BenefitAnalysisDto benefitAnalysisDto,String callName);
	public EligibleModel collectEligibleModel(EligibleDto eligibleDto,String callName);
	public HospitalBlacklistModel collectHospitalBlacklistModel(HospitalBlacklistDto hospitalBlacklistDto,String callName);
	public ContinuityClaimCheckingModel collectContinuityClaimCheckingModel(ContinuityClaimCheckingDto continuityClaimCheckingDto,String callName);
	
}
