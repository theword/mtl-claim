/*
 * 
 */
package com.mtl.benefitAnalysis.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.benefitAnalysis.model.mtlclaim.BenefitGroup;
import com.mtl.benefitAnalysis.model.mtlclaim.Claim;

/**
 * The Class BenefitAnalysisDto.
 */
@XmlRootElement(name = "BenefitAnalysisDto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BenefitAnalysisDto {
	
	/** The client id. */
	@XmlElement(name = "clientID", required = false)
	private String clientID; 
	
	/** The benefit groups. */
	@XmlElement(name = "benefitGroups", required = false)
	private List<BenefitGroup> benefitGroups;
	
	/** The current claims. */
	@XmlElement(name = "currentClaims", required = false)
	private List<Claim> currentClaims; 
	
	/** The fixed claims. */
	@XmlElement(name = "fixedClaims", required = false)
	private List<Claim> fixedClaims; 
	
	/** The continuity claim. */
	@XmlElement(name = "continuityClaim", required = false)
	private List<Claim> continuityClaim;
	
	@XmlElement(name = "claimNumbers", required = false)
	private List<String> claimNumbers;
	
	/**
	 * Instantiates a new benefit analysis dto.
	 */
	public BenefitAnalysisDto() {
		
	}
	
	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public String getClientID() {
		return clientID;
	}

	/**
	 * Sets the client id.
	 *
	 * @param clientID the new client id
	 */
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	/**
	 * Gets the benefit groups.
	 *
	 * @return the benefit groups
	 */
	public List<BenefitGroup> getBenefitGroups() {
		return benefitGroups;
	}

	/**
	 * Sets the benefit groups.
	 *
	 * @param benefitGroups the new benefit groups
	 */
	public void setBenefitGroups(List<BenefitGroup> benefitGroups) {
		this.benefitGroups = benefitGroups;
	}

	/**
	 * Gets the current claims.
	 *
	 * @return the current claims
	 */
	public List<Claim> getCurrentClaims() {
		return currentClaims;
	}

	/**
	 * Sets the current claims.
	 *
	 * @param currentClaims the new current claims
	 */
	public void setCurrentClaims(List<Claim> currentClaims) {
		this.currentClaims = currentClaims;
	}

	/**
	 * Gets the fixed claims.
	 *
	 * @return the fixed claims
	 */
	public List<Claim> getFixedClaims() {
		return fixedClaims;
	}

	/**
	 * Sets the fixed claims.
	 *
	 * @param fixedClaims the new fixed claims
	 */
	public void setFixedClaims(List<Claim> fixedClaims) {
		this.fixedClaims = fixedClaims;
	}

	/**
	 * Gets the continuity claim.
	 *
	 * @return the continuity claim
	 */
	public List<Claim> getContinuityClaim() {
		return continuityClaim;
	}

	/**
	 * Sets the continuity claim.
	 *
	 * @param continuityClaim the new continuity claim
	 */
	public void setContinuityClaim(List<Claim> continuityClaim) {
		this.continuityClaim = continuityClaim;
	}

	public List<String> getClaimNumbers() {
		return claimNumbers;
	}

	public void setClaimNumbers(List<String> claimNumbers) {
		this.claimNumbers = claimNumbers;
	}
}
