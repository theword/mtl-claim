/*
 * 
 */
package com.mtl.benefitAnalysis.model.enumerator;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "ClaimType")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ClaimType{
	
	ACCIDENT, 
	HEALTH;
	
	private static Map<String, ClaimType> stateMap = new HashMap<String, ClaimType>();
	
	static {
		stateMap.put("A", ACCIDENT);
		stateMap.put("H", HEALTH);
	}
	
	/**
	 * Lookup.
	 *
	 * @param key the key
	 * @return the claim type
	 */
	public static ClaimType lookup(String key){
		if(key == null)
			return HEALTH;
		return stateMap.get(key);
	}
}
