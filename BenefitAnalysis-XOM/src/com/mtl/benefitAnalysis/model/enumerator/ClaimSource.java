/*
 * 
 */
package com.mtl.benefitAnalysis.model.enumerator;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "ClaimSource")
@XmlAccessorType(XmlAccessType.FIELD)
public enum ClaimSource {
	
	FAX, 
	DIRECT ;
	
	/** The state map. */
	private static Map<String, ClaimSource> stateMap = new HashMap<String, ClaimSource>();
	
	static {
		stateMap.put("F", FAX);
		stateMap.put("D", DIRECT);
	}
	
	/**
	 * Lookup.
	 *
	 * @param key the key
	 * @return the claim source
	 */
	public static ClaimSource lookup(String key){
		if(key == null)
			return DIRECT;
		return stateMap.get(key);
	}
}
