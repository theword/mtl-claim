/*
 * 
 */
package com.mtl.benefitAnalysis.model.enumerator;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Enum CalculateOption.
 */
@XmlRootElement(name = "CalculateOption")
@XmlAccessorType(XmlAccessType.FIELD)
public enum CalculateOption {
	
	/** The lumpsum. */
	LUMPSUM, 
	
	/** The compensate. */
	COMPENSATE, 
	
	/** The lumpsum percentage. */
	LUMPSUM_PERCENTAGE, 
	
	/** The lumpsum daily. */
	LUMPSUM_DAILY,
	
	/** The compensate percentage. */
	COMPENSATE_PERCENTAGE,
	
	/** The lumpsum daily sp. */
	LUMPSUM_DAILY_SP,
	
	/** The compensate percentage fund. */
	COMPENSATE_PERCENTAGE_FUND,
	
	/** The compensate daily fund. */
	COMPENSATE_DAILY_FUND,
	
	/** The lumpsum limited. */
	LUMPSUM_LIMITED,
	
	/** The lumpsum percentage limited. */
	LUMPSUM_PERCENTAGE_LIMITED,
	
	/** The lumpsum daily old. */
	LUMPSUM_DAILY_OLD,
	
	/** The lumpsum daily limited. */
	LUMPSUM_DAILY_LIMITED,;
	
	/** The state map. */
	private static Map<String, CalculateOption> stateMap = new HashMap<String, CalculateOption>();

	static {
		stateMap.put("", LUMPSUM);
		stateMap.put("1", LUMPSUM);
		stateMap.put("2", COMPENSATE);
		stateMap.put("3", LUMPSUM_PERCENTAGE);
		stateMap.put("4", LUMPSUM_DAILY);
		stateMap.put("5", COMPENSATE_PERCENTAGE);
		stateMap.put("7", LUMPSUM_DAILY_SP);
		stateMap.put("8", COMPENSATE_PERCENTAGE_FUND);
		stateMap.put("9", COMPENSATE_DAILY_FUND);
		stateMap.put("A", LUMPSUM_LIMITED);
		stateMap.put("B", LUMPSUM_PERCENTAGE_LIMITED);
		stateMap.put("C", LUMPSUM_DAILY_OLD);
		stateMap.put("D", LUMPSUM_DAILY_LIMITED);
	}
	
	/**
	 * Lookup.
	 *
	 * @param key the key
	 * @return the calculate option
	 */
	public static CalculateOption lookup(String key){
		if(key == null)
			return LUMPSUM;
		return stateMap.get(key);
	}
}
