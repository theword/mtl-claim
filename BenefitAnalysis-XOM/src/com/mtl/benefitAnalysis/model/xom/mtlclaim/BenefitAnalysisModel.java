/*
 * 
 */
package com.mtl.benefitAnalysis.model.xom.mtlclaim;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.benefitAnalysis.model.RuleMessage;
import com.mtl.benefitAnalysis.model.RuleModel;
import com.mtl.benefitAnalysis.model.mtlclaim.Benefit;
import com.mtl.benefitAnalysis.model.mtlclaim.BenefitGroup;
import com.mtl.benefitAnalysis.model.mtlclaim.Claim;


/**
 * The Class BenefitAnalysisModel.
 */
@XmlRootElement(name = "BenefitAnalysisModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class BenefitAnalysisModel extends RuleModel {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3485024824117969109L;
	
	/** The benefit groups. */
	@XmlElement(name = "benefitGroups", required = false)
	private List<BenefitGroup> benefitGroups;
	
	/** The current claims. */
	@XmlElement(name = "currentClaims", required = false)
	private List<Claim> currentClaims;
	
	/** The fixed claims. */
	@XmlElement(name = "fixedClaims", required = false)
	private List<Claim> fixedClaims;
	
	//Special fields
	/** The benefit group orders. */
	@XmlElement(name = "benefitGroupOrders", required = false)
	private List<String> benefitGroupOrders;
	
	/** The request amount list. */
	@XmlElement(name = "requestAmountList", required = false)
	private List<String> requestAmountList;
	
	/** The request days list. */
	@XmlElement(name = "requestDaysList", required = false)
	private List<String> requestDaysList;
	
	/** The rest amount list. */
	@XmlElement(name = "restAmountList", required = false)
	private List<String> restAmountList;
	
	/** The rest days list. */
	@XmlElement(name = "restDaysList", required = false)
	private List<String> restDaysList;
	
	/** The billed amounts. */
	@XmlElement(name = "billedAmounts", required = false)
	private List<String> billedAmounts;
	
	/** The billed days. */
	@XmlElement(name = "billedDays", required = false)
	private List<String> billedDays;
	
	/** The claim amounts. */
	@XmlElement(name = "claimAmounts", required = false)
	private List<String> claimAmounts;
	
	/** The claim days. */
	@XmlElement(name = "claimDays", required = false)
	private List<String> claimDays;
	
	/** The excess amounts. */
	@XmlElement(name = "excessAmounts", required = false)
	private List<String> excessAmounts;
	
	@XmlElement(name = "claimNumbers", required = false)
	private List<String> claimNumbers;
	
	/** The current sum insured. */
	@XmlElement(name = "currentSumInsured", required = false)
	private double currentSumInsured;
	
	/** The current max available amount. */
	@XmlElement(name = "currentMaxAvailableAmount", required = false)
	private double currentMaxAvailableAmount;
	
	/** The current max days. */
	@XmlElement(name = "currentMaxDays", required = false)
	private int currentMaxDays;
	
	/** The current benefit group index. */
	@XmlElement(name = "currentBenefitGroupIndex", required = false)
	private int currentBenefitGroupIndex;
	
	/** The current claim index. */
	@XmlElement(name = "currentClaimIndex", required = false)
	private int currentClaimIndex;
	
	/** The current fixed claim index. */
	@XmlElement(name = "currentFixedClaimIndex", required = false)
	private int currentFixedClaimIndex;
	
	/** The current used days. */
	@XmlElement(name = "currentUsedDays", required = false)
	private int currentUsedDays;
	//End Special Fields
	
	private int status = 0;
	/**
	 * Instantiates a new benefit analysis model.
	 */
	private BenefitAnalysisModel() {
		super();
		currentBenefitGroupIndex = 0;
		currentClaimIndex = 0;
		currentFixedClaimIndex = 0;
		currentUsedDays = 0;
		currentSumInsured = 0;
		currentMaxDays = 0;
		currentMaxAvailableAmount = 0;
		benefitGroupOrders = new ArrayList<String>();
		requestAmountList = new ArrayList<String>();
		requestDaysList = new ArrayList<String>();
		restAmountList = new ArrayList<String>();
		restDaysList = new ArrayList<String>();
		billedAmounts = new ArrayList<String>();
		billedDays = new ArrayList<String>();
		claimAmounts = new ArrayList<String>();
		claimDays = new ArrayList<String>();
		excessAmounts = new ArrayList<String>();
	}
	
	/**
	 * Instantiates a new benefit analysis model.
	 *
	 * @param benefitGroups the benefit groups
	 * @param currentClaims the current claims
	 * @param fixedClaims the fixed claims
	 */
	public BenefitAnalysisModel(List<BenefitGroup> benefitGroups, List<Claim> currentClaims, List<Claim> fixedClaims) {
		this();
		setBenefitGroups(benefitGroups);
		setCurrentClaims(currentClaims);
		setFixedClaims(fixedClaims);
	}

	/**
	 * Sets the benefit groups.
	 *
	 * @param benefitGroups the new benefit groups
	 */
	public void setBenefitGroups(List<BenefitGroup> benefitGroups) {
		this.benefitGroups = benefitGroups;
	}

	/**
	 * Gets the benefit groups.
	 *
	 * @return the benefit groups
	 */
	public List<BenefitGroup> getBenefitGroups() {
		return benefitGroups;
	}
	
	/**
	 * Sets the current claims.
	 *
	 * @param currentClaims the new current claims
	 */
	public void setCurrentClaims(List<Claim> currentClaims) {
		this.currentClaims = currentClaims;
	}

	/**
	 * Gets the current claims.
	 *
	 * @return the current claims
	 */
	public List<Claim> getCurrentClaims() {
		return currentClaims;
	}
	
	/**
	 * Sets the fixed claims.
	 *
	 * @param fixedClaims the new fixed claims
	 */
	public void setFixedClaims(List<Claim> fixedClaims) {
		this.fixedClaims = fixedClaims;
	}

	/**
	 * Gets the fixed claims.
	 *
	 * @return the fixed claims
	 */
	public List<Claim> getFixedClaims() {
		return fixedClaims;
	}

	//Special Local Methods
	
	/**
	 * Sets the billed amounts.
	 *
	 * @param billedAmounts the new billed amounts
	 */
	public void setBilledAmounts(List<String> billedAmounts) {
		this.billedAmounts = billedAmounts;
	}
	
	/**
	 * Sets the billed days.
	 *
	 * @param billedDays the new billed days
	 */
	public void setBilledDays(List<String> billedDays) {
		this.billedDays = billedDays;
	}

	/**
	 * Sets the claim amounts.
	 *
	 * @param claimAmounts the new claim amounts
	 */
	public void setClaimAmounts(List<String> claimAmounts) {
		this.claimAmounts = claimAmounts;
	}

	/**
	 * Sets the claim days.
	 *
	 * @param claimDays the new claim days
	 */
	public void setClaimDays(List<String> claimDays) {
		this.claimDays = claimDays;
	}

	/**
	 * Sets the excess amounts.
	 *
	 * @param excessAmounts the new excess amounts
	 */
	public void setExcessAmounts(List<String> excessAmounts) {
		this.excessAmounts = excessAmounts;
	}
	
	/**
	 * Sets the current used days.
	 *
	 * @param currentUsedDays the new current used days
	 */
	public void setCurrentUsedDays(int currentUsedDays) {
		this.currentUsedDays = currentUsedDays;
	}

	/**
	 * Gets the current used days.
	 *
	 * @return the current used days
	 */
	public int getCurrentUsedDays() {
		return currentUsedDays;
	}
	
	/**
	 * Gets the rest amount list.
	 *
	 * @return the rest amount list
	 */
	public List<String> getRestAmountList(){
		return restAmountList;
	}
	
	/**
	 * Gets the rest day list.
	 *
	 * @return the rest day list
	 */
	public List<String> getRestDayList(){
		return restDaysList;
	}
	
	/**
	 * Sets the rest amount list.
	 *
	 * @param restAmountList the new rest amount list
	 */
	public void setRestAmountList(List<String> restAmountList){
		this.restAmountList = restAmountList;
	}
	
	/**
	 * Sets the rest day list.
	 *
	 * @param restDaysList the new rest day list
	 */
	public void setRestDayList(List<String> restDaysList){
		this.restDaysList = restDaysList;
	}
	
	/**
	 * Adds the current used days.
	 *
	 * @param usedDay the used day
	 */
	public void addCurrentUsedDays(int usedDay){
		currentUsedDays += usedDay;
	}
	
	/**
	 * Checks for next benefit group.
	 *
	 * @return true, if successful
	 */
	public boolean hasNextBenefitGroup(){
		if(benefitGroups == null){
			return false;
		}
		return currentBenefitGroupIndex < benefitGroups.size();
	}
	
	/**
	 * Sets the current benefit group.
	 *
	 * @param benefitGroup the new current benefit group
	 */
	public void setCurrentBenefitGroup(BenefitGroup benefitGroup){
		benefitGroups.set(currentBenefitGroupIndex-1, benefitGroup);
	}
	
	/**
	 * Gets the next benefit group.
	 *
	 * @return the next benefit group
	 */
	public BenefitGroup getNextBenefitGroup(){
		BenefitGroup benefitGroup = benefitGroups.get(currentBenefitGroupIndex++);
		requestAmountList = benefitGroup.getRequestAmountList();
		requestDaysList = benefitGroup.getRequestDaysList();
		if(!benefitGroup.isRequestAdded())
		{
			addRequestAmount(benefitGroup.getRequestAmount());
			addRequestDays(benefitGroup.getRequestDays());
			benefitGroup.setRequestAdded(true);
		}
		resetCurrentClaimIndex();
		return benefitGroup;
	}
	
	/**
	 * Reset benefit group index.
	 */
	public void resetBenefitGroupIndex(){
		currentBenefitGroupIndex = 0;
	}
	
	/**
	 * Checks for next claim.
	 *
	 * @return true, if successful
	 */
	public boolean hasNextClaim(){
		if(currentClaims==null){
			return false;
		}
		return currentClaimIndex < currentClaims.size();
	}
	
	/**
	 * Gets the next current claim.
	 *
	 * @return the next current claim
	 */
	public Claim getNextCurrentClaim(){
		Claim currentClaim = currentClaims.get(currentClaimIndex++);
		currentClaim.resetBenefitIndex();
		return currentClaim;
	}
	
	/**
	 * Sets the current claim.
	 *
	 * @param currentClaim the new current claim
	 */
	public void setCurrentClaim(Claim currentClaim){
		currentClaims.set(currentClaimIndex-1, currentClaim);
	}
	
	/**
	 * Reset current claim index.
	 */
	private void resetCurrentClaimIndex(){
		currentClaimIndex = 0;
	}
	
	/**
	 * Checks for next fixed claim.
	 *
	 * @return true, if successful
	 */
	public boolean hasNextFixedClaim(){
		if(fixedClaims == null){
			return false;
		}
		return currentFixedClaimIndex < fixedClaims.size();
	}
	
	/**
	 * Sets the current fixed claim.
	 *
	 * @param currentFixedClaim the new current fixed claim
	 */
	public void setCurrentFixedClaim(Claim currentFixedClaim){
		fixedClaims.set(currentFixedClaimIndex-1, currentFixedClaim);
	}
	
	/**
	 * Gets the next fixed claim.
	 *
	 * @return the next fixed claim
	 */
	public Claim getNextFixedClaim(){
		Claim currentClaim = fixedClaims.get(currentFixedClaimIndex++);
		currentClaim.resetBenefitIndex();
		return currentClaim;
	}
	
	/**
	 * Update benefit details.
	 */
	public void updateBenefitDetails(){
		Claim fixedClaim = fixedClaims.get(currentFixedClaimIndex);
		for(Claim currentClaim : currentClaims){
			if(fixedClaim.getCoverage().getCoverageCode().equals(currentClaim.getCoverage().getCoverageCode()) &&
					fixedClaim.getPolicyNumber().equals(fixedClaim.getPolicyNumber())){
				for(Benefit currentBenefit : currentClaim.getBenefits()){
					for(Benefit fixedBenefit : fixedClaim.getBenefits()){
						if(fixedBenefit.getBenefitCode().equals(currentBenefit.getBenefitCode())){
							for(int i = 0; i<fixedBenefit.getBilledAmounts().size(); i++){
								currentBenefit.getBilledAmounts().add(fixedBenefit.getBilledAmounts().get(i));
								currentBenefit.getBilledDays().add(fixedBenefit.getBilledDays().get(i));
								currentBenefit.getClaimAmounts().add(fixedBenefit.getClaimAmounts().get(i));
								currentBenefit.getClaimDays().add(fixedBenefit.getClaimDays().get(i));
								currentBenefit.getExcessAmounts().add(fixedBenefit.getExcessAmounts().get(i));
							}
						}
					}
				}
				break;
			}
		}
		
	}
	
	/**
	 * Sets the current sum insured.
	 *
	 * @param currentSumInsured the new current sum insured
	 */
	public void setCurrentSumInsured(double currentSumInsured) {
		this.currentSumInsured = currentSumInsured;
	}

	/**
	 * Sets the current max days.
	 *
	 * @param currentMaxDays the new current max days
	 */
	public void setCurrentMaxDays(int currentMaxDays) {
		this.currentMaxDays = currentMaxDays;
	}
	
	/**
	 * Gets the current max days.
	 *
	 * @return the current max days
	 */
	public int getCurrentMaxDays() {
		return currentMaxDays;
	}

	/**
	 * Sets the current max available amount.
	 *
	 * @param currentMaxAvailableAmount the new current max available amount
	 */
	public void setCurrentMaxAvailableAmount(double currentMaxAvailableAmount) {
		this.currentMaxAvailableAmount = currentMaxAvailableAmount;
	}

	/**
	 * Gets the current max available amount.
	 *
	 * @return the current max available amount
	 */
	public double getCurrentMaxAvailableAmount() {
		return currentMaxAvailableAmount;
	}
	
	/**
	 * Analyze daily benefit.
	 */
	public void analyzeDailyBenefit(){
		restAmountList = new ArrayList<String>();
		restDaysList = new ArrayList<String>();
		for(int i = 0; i<requestAmountList.size(); i++){
			double oldAmount = Double.parseDouble(requestAmountList.get(i));
			double oldDay = Double.parseDouble(requestDaysList.get(i));
			boolean isNewValue = false;
			boolean isInit = true;
			double billedValue = 0;
			double claimValue = 0;
			int days = 1;
			for(int j = 0; j<oldDay; j++){
				if(currentMaxDays > 0 && currentMaxAvailableAmount > 0){
					double amount = oldAmount - currentSumInsured;
					if(amount > 0){
						if(currentMaxAvailableAmount-currentSumInsured>=0){
							addAmountPair(restAmountList, restDaysList, amount+"", 1+"");
							currentMaxAvailableAmount -= currentSumInsured;
							if(claimValue!=currentSumInsured){
								claimValue = currentSumInsured;
								isNewValue = true;
							}
						}else{
							addAmountPair(restAmountList, restDaysList, oldAmount-currentMaxAvailableAmount+"", 1+"");
							claimValue = currentMaxAvailableAmount;
							isNewValue = true;
							currentMaxAvailableAmount = 0;
						}
					}else{
						if(currentMaxAvailableAmount-oldAmount>=0){
							currentMaxAvailableAmount -= oldAmount;
							if(claimValue!=oldAmount){
								claimValue = oldAmount;
								isNewValue = true;
							}
						}else{
							addAmountPair(restAmountList, restDaysList, oldAmount-currentMaxAvailableAmount+"", 1+"");
							claimValue = currentMaxAvailableAmount;
							isNewValue = true;
							currentMaxAvailableAmount = 0;
						}
					}
				}else{
					addAmountPair(restAmountList, restDaysList, oldAmount+"", 1+"");
					if(claimValue!=0){
						claimValue = 0;
						isNewValue = true;
					}
				}
				if(billedValue!=oldAmount){
					billedValue = oldAmount;
					isNewValue = true;
				}
				if(isInit){
					isInit = false;
					isNewValue = false;
					billedAmounts.add(billedValue+"");
					claimAmounts.add(claimValue+"");
				}else if(isNewValue){
					isNewValue = false;
					billedDays.add(days+"");
					if(claimAmounts.get(claimAmounts.size()-1).equals("0")){
						claimDays.add(0+"");
						currentUsedDays -= days;
					}else{
						claimDays.add(days+"");
					}
					billedAmounts.add(billedValue+"");
					claimAmounts.add(claimValue+"");
					days = 1;
				}else{
					days++;
				}
				currentMaxDays--;
				currentUsedDays++;
			}
			billedDays.add(days+"");
			if(claimAmounts.get(claimAmounts.size()-1).equals("0")){
				claimDays.add(0+"");
				currentUsedDays -= days;
			}else{
				claimDays.add(days+"");
			}
			days = 0;
			isInit = true;
		}
		requestAmountList = restAmountList;
		requestDaysList = restDaysList;
		calculateExcessAmount();
	}
	
	/**
	 * Adds the amount pair.
	 *
	 * @param amount the amount
	 * @param day the day
	 * @param newAmount the new amount
	 * @param newDay the new day
	 */
	private void addAmountPair(List<String> amount, List<String> day,
			String newAmount, String newDay){
		if(amount.contains(newAmount)){
			int index = amount.indexOf(newAmount);
			int addedDay = Integer.parseInt(newDay) + Integer.parseInt(day.get(index));
			day.set(index, addedDay+"");
		}else{
			amount.add(newAmount);
			day.add(newDay);
		}
	}
	
	/**
	 * Calculate excess amount.
	 */
	private void calculateExcessAmount(){
		
		for(int i = 0; i<billedAmounts.size(); i++){
			excessAmounts.add(
				Double.parseDouble(billedAmounts.get(i)) * Integer.parseInt(billedDays.get(i)) - 
				Double.parseDouble(claimAmounts.get(i)) * Integer.parseInt(claimDays.get(i))+"");
		}
	}
	
	/**
	 * Adds the request amount.
	 *
	 * @param billedAmount the billed amount
	 */
	public void addRequestAmount(double billedAmount){
		requestAmountList.add(billedAmount+"");
	}
	
	/**
	 * Adds the request days.
	 *
	 * @param billedAmountDays the billed amount days
	 */
	public void addRequestDays(int billedAmountDays){
		requestDaysList.add(billedAmountDays+"");
	}
	
	/**
	 * Sets the request amount.
	 *
	 * @param billedAmount the new request amount
	 */
	public void setRequestAmount(double billedAmount){
		requestAmountList = new ArrayList<String>();
		requestAmountList.add(billedAmount+"");
	}
	
	/**
	 * Sets the request days.
	 *
	 * @param billedAmountDays the new request days
	 */
	public void setRequestDays(int billedAmountDays){
		requestDaysList = new ArrayList<String>();
		requestDaysList.add(billedAmountDays+"");
	}
	
	/**
	 * Sets the request amount list.
	 */
	public void setRequestAmountList(){
		
	}
	
	/**
	 * Sets the request days list.
	 */
	public void setRequestDaysList(){
		
	}
	
	/**
	 * Gets the request amount list.
	 *
	 * @return the request amount list
	 */
	public List<String> getRequestAmountList(){
		return requestAmountList;
	}
	
	/**
	 * Gets the request days list.
	 *
	 * @return the request days list
	 */
	public List<String> getRequestDaysList(){
		return requestDaysList;
	}

	/**
	 * Adds the benefit group order.
	 *
	 * @param benefitGroupCode the benefit group code
	 */
	public void addBenefitGroupOrder(String benefitGroupCode){
		benefitGroupOrders.add(benefitGroupCode);
	}
	
	/**
	 * Order benefit group.
	 */
	public void orderBenefitGroup(){
		List<BenefitGroup> newBenefitGroups = new ArrayList<BenefitGroup>();
		for(String benefitGroupOrder : benefitGroupOrders){
			for(int i = 0; i < benefitGroups.size(); i++){
				if(benefitGroups.get(i).getBenefitGroupCode().equals(benefitGroupOrder)){
					newBenefitGroups.add(benefitGroups.get(i));
					benefitGroups.remove(i);
					i--;
				}
			}
		}
		newBenefitGroups.addAll(benefitGroups);
		benefitGroups = newBenefitGroups;
	}

	/**
	 * Gets the all rule messages.
	 *
	 * @return the all rule messages
	 */
	public List<RuleMessage> getAllRuleMessages(){
		List<RuleMessage> ruleMessages = new ArrayList<RuleMessage>();
		ruleMessages.addAll(getRuleMesssages());
		return ruleMessages;
	}

	public List<String> getClaimNumbers() {
		return claimNumbers;
	}

	public void setClaimNumbers(List<String> claimNumbers) {
		this.claimNumbers = claimNumbers;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	//End Special Local Methods
}
