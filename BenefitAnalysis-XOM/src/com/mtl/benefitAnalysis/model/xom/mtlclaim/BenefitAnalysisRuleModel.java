/*
 * 
 */
package com.mtl.benefitAnalysis.model.xom.mtlclaim;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mtl.benefitAnalysis.model.RuleModel;
import com.mtl.benefitAnalysis.model.enumerator.CalculateOption;
import com.mtl.benefitAnalysis.model.mtlclaim.Benefit;
import com.mtl.benefitAnalysis.model.mtlclaim.BenefitGroup;
import com.mtl.benefitAnalysis.model.mtlclaim.Claim;
import com.mtl.benefitAnalysis.model.mtlclaim.ClaimRemain;
import com.mtl.benefitAnalysis.model.mtlclaim.ClaimTransfer;
import com.mtl.benefitAnalysis.model.mtlclaim.DeDuct;
import com.mtl.benefitAnalysis.util.CalendarHelper;
import com.mtl.benefitAnalysis.util.ModelUtility;

/**
 * The Class BenefitAnalysisRuleModel.
 */
public class BenefitAnalysisRuleModel extends RuleModel{
	
	final boolean debug = true;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7279793586852031746L;
	
	/** The benefit groups. */
	private List<BenefitGroup> benefitGroups;
	
	/** The current claims. */
	private List<Claim> currentClaims;
	
	/** The fixed claims. */
	private List<Claim> fixedClaims;
	
	/** The claim remain. */
	private ClaimRemain claimRemain;
	
	//field only use in Rule-Claim-BenefitAnalysis-1
	/** The current claim index. */
	private int currentClaimIndex;
	
	/** The benefit group index. */
	private int benefitGroupIndex;
	
	/** The request. */
	List<ClaimRemain> request;		//claim request
	
	/** The claim. */
	List<ClaimRemain> claim;		//available claim
	
	/** The remain. */
	List<ClaimRemain> remain;		//claim remain
	
	//transfer amount and day to other benefit group
	/** The claim transfers. */
	List<ClaimTransfer> claimTransfers;
	
	private List<String> claimNumbers;
	
	/** The claim transfer. */
	ClaimTransfer claimTransfer;
	
	/** The map claim index. */
	private int mapClaimIndex;
	
	private double medicalExpenseRemain;
	
	//initial currentClaimIndex,benefitGroupIndex
	/**
	 * Instantiates a new benefit analysis rule model.
	 */
	public BenefitAnalysisRuleModel() {
		super();
		currentClaimIndex = 0;
		benefitGroupIndex = 0;
		request = new ArrayList<ClaimRemain>();
		claim = new ArrayList<ClaimRemain>();
		remain = new ArrayList<ClaimRemain>();
		claimTransfers = new ArrayList<ClaimTransfer>();
		claimTransfer = new ClaimTransfer();
		mapClaimIndex = 0;
		medicalExpenseRemain = 0.0;
	}
	
	/**
	 * Reset to cal.
	 */
	public void resetToCal(){
		currentClaimIndex = 0;
		benefitGroupIndex = 0;
		request = new ArrayList<ClaimRemain>();
		claim = new ArrayList<ClaimRemain>();
		remain = new ArrayList<ClaimRemain>();
		claimTransfers = new ArrayList<ClaimTransfer>();
		claimTransfer = new ClaimTransfer();
		mapClaimIndex = 0;
		medicalExpenseRemain = 0.0;
		
		for(BenefitGroup benefitGroup: benefitGroups){
			benefitGroup.resetToCal();
		}
		for(Claim claim:currentClaims){
			claim.resetToCal();
		}
	}
	//benefitGroups
		//get benefitGroups at benefitGroupIndex
		/**
	 * Gets the benefit group.
	 *
	 * @return the benefit group
	 */
	public BenefitGroup getBenefitGroup(){
			//System.out.println("in getBenefitGroup");
			//System.out.println("benefitGroupIndex : " + benefitGroupIndex);
			if(benefitGroups != null && benefitGroupIndex < benefitGroups.size() && benefitGroups.get(benefitGroupIndex) != null)
				return benefitGroups.get(benefitGroupIndex);
			return null;
		}
		
		//check null of benefitGroups at benefitGroupIndex
		/**
		 * Checks for benefit group.
		 *
		 * @return true, if successful
		 */
		public boolean hasBenefitGroup(){
			//System.out.println("in hasBenefitGroup");
			//System.out.println("benefitGroupIndex : " + benefitGroupIndex);
			if(benefitGroups != null && benefitGroupIndex < benefitGroups.size() && benefitGroups.get(benefitGroupIndex) != null){
				return true;
			}
			return false;
		}
		
		//add benefitGroupIndex + 1 for next benefitGroups
		/**
		 * Next benefit group.
		 */
		public void nextBenefitGroup(){
			benefitGroupIndex++;
		}
		
		//reset benefitGroupIndex = 0
		/**
		 * Reset benefit group index.
		 */
		public void resetBenefitGroupIndex(){
			benefitGroupIndex = 0;
		}
		
		//set benefitGroups at benefitGroupIndex with value from Rule-Claim-BenefitAnalysis-1
		/**
		 * Sets the benefit group.
		 *
		 * @param benefitGroup the new benefit group
		 */
		public void setBenefitGroup(BenefitGroup benefitGroup){
			benefitGroups.set(benefitGroupIndex, benefitGroup);
		}
		
		//order Benefit Group by benefitGroupCode
		//---------------------------------------------------------------------------
		 /**
		 * Order benefit group.
		 */
		public void orderBenefitGroup() {
			//System.out.println("in orderBenefitGroup");
		    if(benefitGroups != null && benefitGroups.size() > 0){
			    //sort with benefitPriority
			    int size = benefitGroups.size();
			    benefitGroupQuicksort(0, size-1);
			    //end sort with benefitPriority
			    
			    //sort with billedAmount
			    int i = 0;
			    int j = 0;
			    int pivot = benefitGroups.get(0).getBenefitPriority();//set initial value for compare
			    for(int k = 1;k < size;k++){//loop for sort by same benefitPriority
			    	if(pivot != benefitGroups.get(k).getBenefitPriority()){
			    		if(i!=j){
			    			benefitGroupQuicksortBilled(i, j);//quick sort between i to j-1 with same benefitPriority
			    		}
			    		pivot = benefitGroups.get(k).getBenefitPriority();//set pivot to new benefitPriority value
			    		i = k;
			    		j = k;
			    	}else{
			    		j++;
			    	}
			    	
			    }//end for
			    if(i != j){
			    	benefitGroupQuicksortBilled(i, j);//sort for last benefitPriority value 
			    }
			    //end sort with billedAmount
			    i = 0;
			    for(BenefitGroup benefitGroup: benefitGroups){
			    	benefitGroup.setBenefitGroupIndex(i);
			    	i++;
			    }
		    }
		 }
		 //quick sort------------
		 /**
 		 * Benefit group quicksort.
 		 *
 		 * @param low the low
 		 * @param high the high
 		 */
 		private void benefitGroupQuicksort(int low, int high) {
		   int i = low, j = high;
		   
			   int pivot = benefitGroups.get(low + (high-low)/2).getBenefitPriority();
			   while(i <= j){
				   while(benefitGroups.get(i).getBenefitPriority() < pivot){
					   i++;
				   }
				   while(benefitGroups.get(j).getBenefitPriority() > pivot){
					   j--;
				   }
				   if(i <= j){
					   benefitGroupSwap(i, j);
					   i++;
					   j--;
				   }//if(i <= j)
			   }//while(i <= j)
		   // Recursion
		   if (low < j) benefitGroupQuicksort(low, j);
		   if (i < high)benefitGroupQuicksort(i, high);
		 }
		 
		 /**
 		 * Benefit group quicksort billed.
 		 *
 		 * @param low the low
 		 * @param high the high
 		 */
 		private void benefitGroupQuicksortBilled(int low, int high) {
			   int i = low, j = high;
			   
			   double pivot = benefitGroups.get(low + (high-low)/2).getBilledAmount();
			   while(i <= j){
				   while(benefitGroups.get(i).getBilledAmount() > pivot){
					   i++;
				   }
				   while(benefitGroups.get(j).getBilledAmount() < pivot){
					   j--;
				   }
				   if(i <= j){
					   benefitGroupSwap(i, j);
					   i++;
					   j--;
				   }//if(i <= j)
			   }//while(i <= j)
			  // Recursion
			  if (low < j) benefitGroupQuicksortBilled(low, j);
			  if (i < high)benefitGroupQuicksortBilled(i, high);
		}
		 
		 //swap benefitGroup
		 /**
 		 * Benefit group swap.
 		 *
 		 * @param i the i
 		 * @param j the j
 		 */
 		private void benefitGroupSwap(int i, int j) {
		    BenefitGroup temp = benefitGroups.get(i);
		    benefitGroups.set(i, benefitGroups.get(j));
		    benefitGroups.set(j, temp);
		 }
		//quick sort------------
		//---------------------------------------------------------------------------
	//benefitGroups
	
	//currentClaim
		//get currenetClaim at currentClaimIndex
			/**
		 * Gets the current claim.
		 *
		 * @return the current claim
		 */
		public Claim getCurrentClaim(){
				//System.out.println("in getCurrentClaim");
				//System.out.println("currentClaimIndex : " + currentClaimIndex);
				if(currentClaims != null && currentClaimIndex < currentClaims.size() && currentClaims.get(currentClaimIndex) != null)
					return currentClaims.get(currentClaimIndex);
				return null;
			}
			
			//check null of currentClaim at currentClaimIndex
			/**
			 * Checks for current claim.
			 *
			 * @return true, if successful
			 */
			public boolean hasCurrentClaim(){
				//System.out.println("in hasCurrentClaim");
				//System.out.println("currentClaimIndex : " + currentClaimIndex);
				if(currentClaims != null && currentClaimIndex < currentClaims.size() && currentClaims.get(currentClaimIndex) != null){
					return true;
				}
				return false;
			}
			
			//add currentClaim + 1 for next CurrentClaim
			/**
			 * Next current claim.
			 */
			public void nextCurrentClaim(){
				currentClaimIndex++;
			}
			
			//reset CurrentClaimIndex = 0
			/**
			 * Reset current claim index.
			 */
			public void resetCurrentClaimIndex(){
				currentClaimIndex = 0;
			}
			
			//set currentClaim at currenClaimIndex with value from Rule-Claim-BenefitAnalysis-1
			/**
			 * Sets the current claim.
			 *
			 * @param claim the new current claim
			 */
			public void setCurrentClaim(Claim claim){
				currentClaims.set(currentClaimIndex, claim);
			}
		//order CurrentClaim by Coverage,FaceAmount,IssueDate
		//---------------------------------------------------------------------------
		/**
		 * Order claim by issue date.
		 *
		 * @param i the i
		 * @param j the j
		 */
		public void orderClaimByIssueDate(int i,int j){
			//sort with issueDate
			int a = i;
			int b = i;
		    BigDecimal faceAmountPivot = BigDecimal.valueOf(currentClaims.get(a).getCoverage().getFaceAmount());//set initial value for compare
		    for(int k = i+1;k <= j;k++){//loop for sort by same faceAmount
		    	BigDecimal temp = BigDecimal.valueOf(currentClaims.get(k).getCoverage().getFaceAmount());//parse double to BigDecimal for compare
		    	if(faceAmountPivot.compareTo(temp) != 0){
		    		if(a!=b){
		    			quicksort(a, b,"issueDate");//quick sort between i to j-1 with same faceAmount
		    		}
		    		a = k;
		    		b = k;
		    		faceAmountPivot = temp;//set pivot to new faceAmount value
		    	}else{
		    		b++;
		    	}
		    }//end for
		    if(a != b){
		    	quicksort(a, b,"issueDate");//sort for last faceAmount value 
		    }
		    //end sort with issueDate
		}
		 
 		/**
 		 * Order current claim.
 		 */
 		public void orderCurrentClaim() {
			//System.out.println("in orderCurrentClaim");
		    if(currentClaims != null && currentClaims.size() > 0){
		     
			    //sort with coveragePriority
			    int size = currentClaims.size();
			    quicksort(0, size-1,"coverage");
			    //sort with faceAmount
			    int i = 0;
			    int j = 0;
			    int pivot = currentClaims.get(0).getCoveragePriority();//set initial value for compare
			    for(int k = 1;k < currentClaims.size();k++){//loop for sort by same coveragePriority
			    	if(pivot != currentClaims.get(k).getCoveragePriority()){
			    		if(i!=j){
			    			quicksort(i, j,"faceAmount");//quick sort between i to j-1 with same coveragePriority
			    			orderClaimByIssueDate(i,j);
			    		}
			    		pivot = currentClaims.get(k).getCoveragePriority();//set pivot to new coveragePriority value
			    		i = k;
			    		j = k;
			    	}else{
			    		j++;
			    	}
			    	
			    }//end for
			    if(i != j){
			    	quicksort(i, j,"faceAmount");//sort for last coveragePriority value 
			    }
			    //end sort with faceAmount
			    
			    i = 0;
			    for(Claim claim:currentClaims){
			    	claim.setClaimIndex(i);
			    	claim.orderBenefit();
			    	i++;
			    }
			    
		    }
		 }
		 //quick sort------------
		 /**
 		 * Quicksort.
 		 *
 		 * @param low the low
 		 * @param high the high
 		 * @param mode the mode
 		 */
 		private void quicksort(int low, int high,String mode) {
		   int i = low, j = high;
		   
		   //order each mode
		   //coverage mode ++++++++++++++++++++++++++++++++++++++++++++++++
		   if(mode.equalsIgnoreCase("coverage")){
			   int pivot = currentClaims.get(low + (high-low)/2).getCoveragePriority();
			   while(i <= j){
				   while(currentClaims.get(i).getCoveragePriority() < pivot){
					   i++;
				   }
				   while(currentClaims.get(j).getCoveragePriority() > pivot){
					   j--;
				   }
				   if(i <= j){
					   swap(i, j);
					   i++;
					   j--;
				   }//if(i <= j)
			   }//while(i <= j)
		   }//mode == coverage
		   //coverage mode ++++++++++++++++++++++++++++++++++++++++++++++++
		   //faceAmount mode ++++++++++++++++++++++++++++++++++++++++++++++++
		   if(mode.equalsIgnoreCase("faceAmount")){
			   double pivot = currentClaims.get(low + (high-low)/2).getCoverage().getFaceAmount();
			   while(i <= j){
				   if(currentClaims.get(low + (high-low)/2).getCoverage().getCoverageGroupCode().equals("RHD") || currentClaims.get(low + (high-low)/2).getCoverage().getCoverageGroupCode().equals("LHD")){
					   while(currentClaims.get(i).getCoverage().getFaceAmount() < pivot){
						   i++;
					   }
					   while(currentClaims.get(j).getCoverage().getFaceAmount() > pivot){
						   j--;
					   }
				   }else{
					   while(currentClaims.get(i).getCoverage().getFaceAmount() > pivot){
						   i++;
					   }
					   while(currentClaims.get(j).getCoverage().getFaceAmount() < pivot){
						   j--;
					   }
				   }
				   if(i <= j){
					   swap(i, j);
					   i++;
					   j--;
				   }//if(i <= j)
			   }//while(i <= j)
		   }//mode == faceAmount
		   //faceAmount mode ++++++++++++++++++++++++++++++++++++++++++++++++
		   //issueDate mode ++++++++++++++++++++++++++++++++++++++++++++++++
		   if(mode.equalsIgnoreCase("issueDate")){
			   Date pivot = currentClaims.get(low + (high-low)/2).getCoverage().getIssuedDate();
			   while(i <= j){
				   while(currentClaims.get(i).getCoverage().getIssuedDate().before(pivot)){
					   i++;
				   }
				   while(currentClaims.get(j).getCoverage().getIssuedDate().after(pivot)){
					   j--;
				   }
				   if(i <= j){
					   swap(i, j);
					   i++;
					   j--;
				   }//if(i <= j)
			   }//while(i <= j)
		   }//mode == issueDate
		   //issueDate mode ++++++++++++++++++++++++++++++++++++++++++++++++
		   // Recursion
		   if (low < j) quicksort(low, j, mode);
		   if (i < high)quicksort(i, high, mode);
		 }
		 //swap currentClaim
		 /**
 		 * Swap.
 		 *
 		 * @param i the i
 		 * @param j the j
 		 */
 		private void swap(int i, int j) {
		    Claim temp = currentClaims.get(i);
		    currentClaims.set(i, currentClaims.get(j));
		    currentClaims.set(j, temp);
		 }
		//quick sort------------
		//---------------------------------------------------------------------------
	//currentClaim
		 
	//claim calculate
		 /**
		 * Adds the map claim index.
		 */
		public void addMapClaimIndex(){
			 mapClaimIndex++;
		 }
		 
		 /**
 		 * Reset map claim index.
 		 */
 		public void resetMapClaimIndex(){
			 mapClaimIndex = 0;
		 }
		 
		 /**
 		 * Prints the array list.
 		 *
 		 * @param list the list
 		 */
 		private void printArrayList(List<ClaimRemain> list){
			 if(list != null && list.size() > 0){
				 for(int i = 0;i<list.size();i++){
					 System.out.println("request amount[" + i + "] : " + list.get(i).getAmountRemain());
					 System.out.println("request day   [" + i + "] : " + list.get(i).getDayRemain());
					 //System.out.println("request map   [" + i + "] : " + list.get(i).getMapIndex());
					 //System.out.println("request split [" + i + "] : " + list.get(i).getIsOwnSplit());
					 System.out.println("request from ICU [" + i + "] : " + list.get(i).isFromICU());
				 }
			 }
			 else{
				 System.out.println("--------null-------");
			 }
			 System.out.println("----------------------------------------------");
			 
		 }//private void printArrayList(List<ClaimRemain> list){
		 
	/**
	 * Sets the claim benefit group.
	 * 
	 * @param benefitGroup
	 *            the benefit group
	 * @param claimAmount
	 *            the claim amount
	 * @param claimDay
	 *            the claim day
	 * @param mapIndex
	 *            the map index
	 * @return the benefit group
	 */
	private BenefitGroup setClaimBenefitGroup(BenefitGroup benefitGroup, double claimAmount, int claimDay, int mapIndex) {
		if (benefitGroup.getMapBenefitIndex() != null && benefitGroup.getMapBenefitIndex().size() > 0) {
			for (int i = 0; i < benefitGroup.getMapBenefitIndex().size(); i++) {
				if (benefitGroup.getMapBenefitIndex().get(i) == mapIndex) {
					benefitGroup.getClaimAmounts().set(i, benefitGroup.getClaimAmounts().get(i) + claimAmount);
					return benefitGroup;
				}
			}
			benefitGroup.getClaimAmounts().add(claimAmount);
			benefitGroup.getClaimAmountDays().add((short) claimDay);
			
			if(debug){
				System.out.println("11a - benefitGroup.getMapBenefitIndex().add(mapIndex);");
				System.out.println("mapIndex = " + mapIndex);
			}
			
			benefitGroup.getMapBenefitIndex().add(mapIndex);
		}
		else {
			benefitGroup.getClaimAmounts().add(claimAmount);
			benefitGroup.getClaimAmountDays().add((short) claimDay);
			
			if(debug){
				System.out.println("11b - benefitGroup.getMapBenefitIndex().add(mapIndex);");
				System.out.println("mapIndex = " + mapIndex);
			}
			
			benefitGroup.getMapBenefitIndex().add(mapIndex);
		}

		return benefitGroup;
	}

		 /**
 		 * Adds the claim remain.
 		 *
 		 * @param claimRemain the claim remain
 		 */
 		public void addClaimRemain(ClaimRemain claimRemain){
			 System.out.println("in addClaimRemain");
			 //System.out.println("----------------before addClaimRemain-----------------");
			 //printArrayList(remain);
			 //System.out.println("day 	claim remain : " + claimRemain.getDayRemain());
			 //System.out.println("amount claim remain : " + claimRemain.getAmountRemain());
			 remain.add(claimRemain);
			 System.out.println("----------------after  addClaimRemain-----------------");
			 printArrayList(remain);
		 }
		 
		 /**
 		 * Sum claim remain.
 		 *
 		 * @param claim the claim
 		 */
 		private void sumClaimRemain(ClaimRemain claim){
			 double amount 	= claim.getAmountRemain();
			 
			 ClaimRemain tempClaimRemain = new ClaimRemain();
			 tempClaimRemain.setMapIndex(mapClaimIndex);
			 addMapClaimIndex();
			 tempClaimRemain.setIsOwnSplit("F");
			 
			 for(ClaimRemain claimRemain:remain){
				 amount	= amount + claimRemain.getAmountRemain();
 			 }
			 tempClaimRemain.setAmountRemain(amount);
			 remain.clear();
			 remain = new ArrayList<ClaimRemain>();
			 remain.add(tempClaimRemain);
		 }
		 
		 /**
 		 * Adds the claim remain for java.
 		 *
 		 * @param claimRemain the claim remain
 		 * @param labAndOldGE the lab and old ge
 		 */
 		public void addClaimRemainForJava(ClaimRemain claimRemain,boolean labAndOldGE){
			 System.out.println("in addClaimRemainForJava");
			 //System.out.println("----------------before addClaimRemainForJava-----------------");
			 //printArrayList(remain);
			 //System.out.println("day 	claim remain : " + claimRemain.getDayRemain());
			 //System.out.println("amount claim remain : " + claimRemain.getAmountRemain());
			 //System.out.println("LAB and old GE 	 : " + labAndOldGE);
			 if(labAndOldGE && remain!=null && remain.size()>0){
				 System.out.println("in case new GE and sum remain");
				 sumClaimRemain(claimRemain);
			 }
			 else{
				 System.out.println("in other case");
				 remain.add(claimRemain);
			 } 
			 System.out.println("----------------after  addClaimRemainForJava-----------------");
			 printArrayList(remain);
		 }
		 
		 /**
 		 * Adds the claim tranfer.
 		 *
 		 * @param claimTransfer the claim transfer
 		 */
 		public void addClaimTranfer(ClaimTransfer claimTransfer){
			 //System.out.println("in addClaimTranfer");
			 claimTransfers.add(claimTransfer);
		 }
		 
		 /**
 		 * Search claim tranfer.
 		 *
 		 * @param benefitGroupCode the benefit group code
 		 */
 		public void searchClaimTranfer(String benefitGroupCode){
			 System.out.println("in searchClaimTranfer");
			 //System.out.println("benefitGroupCode : " + benefitGroupCode);
			 System.out.println("claimTransfers size :" + claimTransfers.size());
			 System.out.println("------------- Transfer ----------------");
			 for(int i = 0;i<claimTransfers.size();i++){
				 System.out.println("transfer benefit code : " + claimTransfers.get(i).getBenefitGroupCode());
				 System.out.println("transfer claim amount : " + claimTransfers.get(i).getClaimAmount());
				 System.out.println("transfer claim day    : " + claimTransfers.get(i).getClaimDay());
				 System.out.println("transfer isUsed       : " + claimTransfers.get(i).isUsed());
			 }
			 System.out.println("-----------------------------------------");
			 if(claimTransfers != null && claimTransfers.size() > 0){
				 System.out.println("in searching");
				 for(ClaimTransfer transfer:claimTransfers){
					 if(transfer.getBenefitGroupCode().equalsIgnoreCase(benefitGroupCode) && !transfer.isUsed()){
						 transfer.setUsed(true);
						 System.out.println("found");
						 
						 //set to remain
						 ClaimRemain claimRemain = new ClaimRemain();
						 claimRemain.setAmountRemain(transfer.getClaimAmount());
						 claimRemain.setDayRemain(transfer.getClaimDay());
						 claimRemain.setMapIndex(mapClaimIndex);
						 claimRemain.setIsOwnSplit("F");
						 addMapClaimIndex();
						 if(benefitGroupCode.equalsIgnoreCase("LAB,old GE")){
							 addClaimRemainForJava(claimRemain,true);
						 }
//						 else{
//							 addClaimRemainForJava(claimRemain,false);
//						 }
					 }
				 }
				 System.out.println("out searching");
			 }
			 System.out.println("no found");
		 }
		 
		 /**
 		 * Clear remain.
 		 */
 		public void clearRemain(){
			 //System.out.println("in clearRemain");
			 remain.clear();
			 remain = new ArrayList<ClaimRemain>();
			 claimRemain = new ClaimRemain();
			 //System.out.println("out clearRemain");
		 }
		 
		 /**
 		 * Clear transfer.
 		 */
 		public void clearTransfer(){
			 claimTransfers.clear();
			 claimTransfers = new ArrayList<ClaimTransfer>();
		 }
		 
		 /**
 		 * New claim transfer.
 		 */
 		public void newClaimTransfer(){
			 claimTransfer = new ClaimTransfer();
		 }
		 
		 /**
 		 * New claim remain.
 		 */
 		public void newClaimRemain(){
			 claimRemain = new ClaimRemain();
		 }
		 
		 /**
 		 * Search claim remain to transfer.
 		 *
 		 * @return the claim remain
 		 */
 		public ClaimRemain searchClaimRemainToTransfer(){
			 System.out.println("in searchClaimRemainToTransfer");
			 if(remain!=null && 0 < remain.size() && benefitGroupIndex < benefitGroups.size()){
				 System.out.println("remain size : " + remain.size());
				 //System.out.println("benefitGroupIndex : " + benefitGroupIndex);
				 BenefitGroup benefitGroup	= benefitGroups.get(benefitGroupIndex);
				 if(remain.size() == 1 && benefitGroup.isLabAndOldGE()){
					 return remain.get(0);
				 }
//				 //fix for RS0 ICU to RB
////				 for(int i = remain.size()-1;i>=0;i--)
//				 for(int i = 0;i<remain.size();i++){
//					 if(remain.get(i).isFromICU()){
//						 return remain.get(i);
//					 }
//				 }
			 }
			 return null;
		 }

	// calculate amount and day
	/**
	 * Calculate claim.
	 */
	public void calculateClaim() {
		System.out.println("in calculateClaim");
		System.out.println("----------------remain-----------------");
		printArrayList(remain);
		this.claimRemain.setHasRemain(false);
		if (currentClaimIndex < currentClaims.size() && benefitGroupIndex < benefitGroups.size() && checkRemain()) {
			Claim currentClaim = currentClaims.get(currentClaimIndex);
			BenefitGroup benefitGroup = benefitGroups.get(benefitGroupIndex);
			Benefit currentBenefit = currentClaim.getCurrentBenefit();

			if (currentBenefit.isICUType()) {
				currentClaim.setICUactive(true);
			}

			double sumInsuredDouble = (currentBenefit.getSumInsured() * currentBenefit.getPercent()) / 100;
			BigDecimal sumInsured = new BigDecimal(sumInsuredDouble);

			BigDecimal maxLimitAmountForLumsum = new BigDecimal(currentBenefit.getSumInsured());

			double maxAmountDouble = currentBenefit.getAvailableAmount();
			BigDecimal maxAmount = new BigDecimal(maxAmountDouble);
			int maxDay = currentBenefit.getAvailableDays();
			int maxDayForSpecialCase = maxDay;
			// max coverage
			BigDecimal maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getMaxAvailableAmount());

			System.out.println("max coverage amount : " + currentClaim.getCoverage().getMaxAmount());
			System.out.println("max coverage amount available : " + maxCoverageAmount);
			System.out.println("currentBenefit getBenefitGroup : " + currentBenefit.getBenefitGroup());
			System.out.println("calculateClaim max day : " + maxDay);
			System.out.println("calculateClaim max amount : " + maxAmount);

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			boolean groupSumBenefitFlag = false;
			if (currentClaim.getCoverage().isGroupSumBenefit() && currentBenefit.isGroupSumBenefit()) {
				groupSumBenefitFlag = true;
			}

			if (groupSumBenefitFlag) {
				if (maxCoverageAmount.compareTo(new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount())) > 0) {
					maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount());
					if (maxCoverageAmount.compareTo(new BigDecimal(0)) < 0) {
						maxCoverageAmount = new BigDecimal(0);
					}
					System.out.println("max coverage amount avilable after group sum benefit : " + maxCoverageAmount);
				}
			}
			// ----------------------------------------------------------------------------------
			// RHL
			boolean rhlFlag = currentClaim.isRHL();

			// RHH
			boolean rhhFlag = currentClaim.isRHH();
			boolean rhgFlag = currentClaim.isRHG();
			boolean rhpFlag = currentClaim.isRHP();
			boolean rheFlag = currentClaim.isRHE();
			boolean isMaxYearBenefit = currentBenefit.isMaxYearBenefit();
			// share day
			boolean groupShareDay = benefitGroup.isICUAndRB();
			System.out.println("limit day ICU and RB : " + currentClaim.getLimitICUandRB());
			System.out.println("continuity day ICU and RB : " + currentClaim.getContinuityShareDay());
			// if(!rhlFlag && groupShareDay && maxDay >
			// currentClaim.getLimitICUandRB()){
			if ((!rhlFlag || !rhhFlag || !rhgFlag || !rhpFlag || !rheFlag) && groupShareDay && maxDay > currentClaim.getLimitICUandRB()) {
				maxDay = currentClaim.getLimitICUandRB();
			}
			// RHL sum group
			if (rhlFlag && currentBenefit.isRhlFlag()) {
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableRHLAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableRHLAmount());
					System.out.println("max coverage amount available RHL : " + maxCoverageAmount);
				}
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableYearRHLAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableYearRHLAmount());
					System.out.println("max coverage year amount available RHL : " + maxCoverageAmount);
				}
			}

			// RHH sum group
			if (rhhFlag && currentBenefit.isRhhFlag()) {
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableRHHAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableRHHAmount());
					System.out.println("max coverage amount available RHH : " + maxCoverageAmount);
				}
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableYearRHHAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableYearRHHAmount());
					System.out.println("max coverage year amount available RHH : " + maxCoverageAmount);
				}
			}

			if (rhgFlag && currentBenefit.isRhgFlag()) {
				if (!isMaxYearBenefit) {
					if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableYearRHGAmount()) {
						maxCoverageAmount = new BigDecimal(currentClaim.getAvailableYearRHGAmount());
						System.out.println("max coverage year amount available RHG : " + maxCoverageAmount);
					}
				}
			}

			if (rhpFlag && currentBenefit.isRhpFlag()) {
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableYearRHPAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableYearRHPAmount());
					System.out.println("max coverage year amount available RHP : " + maxCoverageAmount);
				}
				if (maxCoverageAmount.doubleValue() > currentClaim.getMaxCoverageApprovedTotal()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getMaxCoverageApprovedTotal());
					System.out.println("max coverage approved total : " + maxCoverageAmount);
				}
			}

			/* 20180417 jame */
			if (rheFlag && currentBenefit.isRheFlag()) {
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableRHEAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableRHEAmount());
					System.out.println("max coverage amount available RHE : " + maxCoverageAmount);
				}
				if (maxCoverageAmount.doubleValue() > currentClaim.getAvailableYearRHEAmount()) {
					maxCoverageAmount = new BigDecimal(currentClaim.getAvailableYearRHEAmount());
					System.out.println("max coverage year amount available RHE : " + maxCoverageAmount);
				}
				if (currentBenefit.getBenefitGroup().equalsIgnoreCase("PSY01")) {
					if (maxCoverageAmount.doubleValue() > currentClaim.getMaxLifeBenefitTotal()) {
						maxCoverageAmount = new BigDecimal(currentClaim.getMaxLifeBenefitTotal());
						System.out.println("max life benefit approved total : " + maxCoverageAmount);
					}
				}

				if (currentBenefit.getBenefitGroup().equalsIgnoreCase("SP1")) {
					double maxTime = currentClaim.getCoverage().getMasterModel().getMaxTimeSP1();
					int maxPayDays = currentBenefit.getMaxPayDays();

					System.out.println("maxTime : " + maxTime);
					System.out.println("maxPayDays : " + maxPayDays);
					System.out.println("maxDay : " + maxDay);

					if (maxTime >= maxPayDays) {
						maxDay = 0;
					}
					else {
						maxDay = (int) (maxPayDays - maxTime);
					}

				}

			}

			System.out.println("after check share day max day : " + maxDay);

			List<String> fixedInputList = (currentBenefit.getFixedInput() != null && currentBenefit.getFixedInput().size() > 0) ? currentBenefit
					.getFixedInput() : null;
			// fixed claim

			currentBenefit.setRequest(remain);
			request = currentBenefit.getRequest();

			int availableClaimDay = 0;
			BigDecimal availableClaimAmount = null;
			List<ClaimRemain> remainClaim = new ArrayList<ClaimRemain>();
			List<ClaimRemain> claimAvailable = new ArrayList<ClaimRemain>();

			// check benefit to claim
			int dductIndex = currentBenefit.getdDuctIndex();
			int fixedIndex = currentBenefit.getFixedIndex();
			// for LUMSUM case
			boolean isLumsum = false;
			if (currentClaim.isLumsum() && benefitGroup.isDV()) {
				isLumsum = true;
			}

			boolean exception = false;

			for (ClaimRemain requestClaim : request) {

				if ((requestClaim.isFromICU() && currentBenefit.isRBType())) {
					remainClaim.add(requestClaim);
					exception = true;
				}
				else {
					// not available claim
					System.out.println("loop max day : " + maxDay);
					System.out.println("loop max amount : " + maxAmount);

					if ((maxDay > 0 && maxAmount.compareTo(new BigDecimal(0.00)) > 0 && maxCoverageAmount.compareTo(new BigDecimal(0.00)) > 0 && maxLimitAmountForLumsum
							.compareTo(new BigDecimal(0.00)) > 0)
							|| (((currentBenefit.getBenefitGroup().equals("ICU1") || currentBenefit.getBenefitGroup().equals("RB1"))
									&& (currentClaim.getCoverage().getCoverageGroupCode().equals("RHG")) && maxDay > 0))) {
						availableClaimDay = requestClaim.getDayRemain() - maxDay;
						int claimDay = (requestClaim.getDayRemain() < maxDay ? requestClaim.getDayRemain() : maxDay);

						// fixed claim
						BigDecimal sumInsuredAvailable = new BigDecimal((requestClaim.getDayRemain() > 0) ? maxAmount.doubleValue()
								/ (double) claimDay : 0);
						System.out.println("sumInsuredAvailable : " + sumInsuredAvailable);
						System.out.println("before   sumInsured : " + sumInsured);
						if (sumInsuredAvailable.compareTo(sumInsured) < 0) {
							sumInsured = new BigDecimal(sumInsuredAvailable.doubleValue());
						}

						// for lumsum case
						BigDecimal tempSumInsuredLumsum = new BigDecimal(maxLimitAmountForLumsum.doubleValue()
								/ ((claimDay > 0) ? (double) claimDay : 0));
						BigDecimal tempSumInsured = (isLumsum) ? tempSumInsuredLumsum : sumInsured;
						availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract(tempSumInsured);

						// fixed claim
						BigDecimal tempfixedMaxAmount = tempSumInsured;
						int tempFixedMaxDay = claimDay;
						boolean isFixed = false;
						boolean isFixedClaim = false;

						// d
						// duct---------------------------------------------------------------------------------------
						if (currentBenefit.isDDuctType() && currentClaim.getCoverage().isCovDDuctType() && currentClaim.getCoverage().isCovDDuctCal()) {
							System.out.println("d duct");
							if (currentBenefit.getDeDuctList().get(dductIndex).isCalculateDeDuct()) {
								System.out.println("case no cal again");
								System.out.println("before requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
								System.out.println("currentBenefit.getClaimDeDuct() : "
										+ currentBenefit.getDeDuctList().get(dductIndex).getClaimDeDuct());
								// double deduct =
								// requestClaim.getAmountRemain() -
								// currentBenefit.getDeDuctList().get(dductIndex).getClaimDeDuct();
								// requestClaim.setAmountRemain(deduct>0?deduct:0);
								BigDecimal tempClaimRemain = new BigDecimal(requestClaim.getAmountRemain());
								BigDecimal dDuctFixedSumInsured = new BigDecimal(currentBenefit.getDeDuctList().get(dductIndex)
										.getClaimAmountDeDuct());
								availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
								tempfixedMaxAmount = dDuctFixedSumInsured;
								isFixed = true;

								System.out.println("after requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
								System.out.println("availableClaimAmount : " + availableClaimAmount);
								System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
							}
							else {
								double dDuctAmount = currentClaim.getTotalRemainDeDuct();
								System.out.println("availableClaimAmount : " + availableClaimAmount);
								if (dDuctAmount <= 0) {

									if (currentClaim.getCoverage().isCovDDuct100()) {
										// 100%
										System.out.println("case 100%");
										BigDecimal tempClaimRemain = new BigDecimal(requestClaim.getAmountRemain());

										currentClaim.getCoverage().setSumBeforeDeDuctAmount(
												currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

										BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue());

										currentBenefit.getDeDuctList().get(dductIndex).setClaimDeDuct(0.0);
										System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
										System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
										System.out.println("tempSumInsured : " + tempSumInsured);
										if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
											availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
											tempfixedMaxAmount = dDuctFixedSumInsured;
											isFixed = true;
										}
									}
									else {
										// 80%
										System.out.println("case 80%");
										BigDecimal tempClaimRemain = new BigDecimal(requestClaim.getAmountRemain());

										// 2017-11-22 TeTe cr sum before cal 80%
										currentClaim.getCoverage().setSumBeforeDeDuctAmount(
												currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

										BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue() * 80 / 100);

										currentBenefit.getDeDuctList().get(dductIndex).setClaimDeDuct(0.0);
										System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
										System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
										System.out.println("tempSumInsured : " + tempSumInsured);
										if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
											availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
											tempfixedMaxAmount = dDuctFixedSumInsured;
											isFixed = true;
										}
									}
									currentClaim.setTotalRemainDeDuct(0.0);
								}
								else {
									if (claimDay > 0) {
										if (currentClaim.getCoverage().isCovDDuct100()) {
											System.out.println("case normal");
											double deduct = dDuctAmount / claimDay;
											double deduct2 = requestClaim.getAmountRemain() - deduct;
											BigDecimal tempClaimRemain = new BigDecimal(deduct2 > 0 ? deduct2 : 0);

											currentClaim.getCoverage().setSumBeforeDeDuctAmount(
													currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

											BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue());
											currentBenefit
													.getDeDuctList()
													.get(dductIndex)
													.setClaimDeDuct(requestClaim.getAmountRemain() > deduct ? deduct : requestClaim.getAmountRemain());
											// requestClaim.setAmountRemain(tempClaimRemain.doubleValue());
											System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
											System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
											System.out.println("tempSumInsured : " + tempSumInsured);
											System.out.println("currentBenefit.getClaimDeDuct() : "
													+ currentBenefit.getDeDuctList().get(dductIndex).getClaimDeDuct());
											if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
												availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
												tempfixedMaxAmount = dDuctFixedSumInsured;
												isFixed = true;
											}
											double deductRemain = dDuctAmount
													- (currentBenefit.getDeDuctList().get(dductIndex).getClaimDeDuct() * claimDay);
											currentClaim.setTotalRemainDeDuct(deductRemain > 0 ? deductRemain : 0.0);
										}
										else {
											System.out.println("case normal");
											double deduct = dDuctAmount / claimDay;
											double deduct2 = requestClaim.getAmountRemain() - deduct;
											BigDecimal tempClaimRemain = new BigDecimal(deduct2 > 0 ? deduct2 : 0);

											// 2017-11-22 TeTe cr sum before cal
											// 80%
											currentClaim.getCoverage().setSumBeforeDeDuctAmount(
													currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

											BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue() * 80 / 100);
											currentBenefit
													.getDeDuctList()
													.get(dductIndex)
													.setClaimDeDuct(requestClaim.getAmountRemain() > deduct ? deduct : requestClaim.getAmountRemain());
											// requestClaim.setAmountRemain(tempClaimRemain.doubleValue());
											System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
											System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
											System.out.println("tempSumInsured : " + tempSumInsured);
											System.out.println("currentBenefit.getClaimDeDuct() : "
													+ currentBenefit.getDeDuctList().get(dductIndex).getClaimDeDuct());
											if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
												availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
												tempfixedMaxAmount = dDuctFixedSumInsured;
												isFixed = true;
											}
											double deductRemain = dDuctAmount
													- (currentBenefit.getDeDuctList().get(dductIndex).getClaimDeDuct() * claimDay);
											currentClaim.setTotalRemainDeDuct(deductRemain > 0 ? deductRemain : 0.0);
										}

									}
									currentBenefit.getDeDuctList().get(dductIndex).setCalculateDeDuct(true);
								}
							}
						}
						else
							// d
							// duct-----------------------------------------------------------------------------------------
							if (currentBenefit.isFixed()) {
								System.out.println("currentBenefit Fixed");
								if (fixedInputList != null && fixedInputList.size() > fixedIndex
										&& fixedInputList.get(fixedIndex).equalsIgnoreCase("T") && currentBenefit.getFixedAmounts() != null
										&& currentBenefit.getFixedAmounts().size() > 0) {
									double fixedMaxAmount = Double.parseDouble(currentBenefit.getFixedAmounts().get(fixedIndex).toString());
									int fixedMaxDay = Integer.parseInt(currentBenefit.getFixedDays().get(fixedIndex).toString());
									System.out.println("currentBenefit.getFixedAmounts().get(" + fixedIndex + ") : " + fixedMaxAmount);
									System.out.println("currentBenefit.getFixedDays().get(" + fixedIndex + ")    : " + fixedMaxDay);

									BigDecimal fixedSumInsured = new BigDecimal(fixedMaxAmount);
									System.out.println("fixedSumInsured : " + fixedSumInsured);
									if (fixedMaxDay < claimDay) {
										availableClaimDay = requestClaim.getDayRemain() - fixedMaxDay;
										System.out.println("fixed availableClaimDay : " + availableClaimDay);
										tempFixedMaxDay = fixedMaxDay;
										isFixed = true;
										isFixedClaim = true;
									}
									if (fixedSumInsured.compareTo(tempSumInsured) < 0) {
										availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract(fixedSumInsured);
										System.out.println("fixed availableClaimAmount : " + availableClaimAmount);
										tempfixedMaxAmount = fixedSumInsured;
										isFixed = true;
										isFixedClaim = true;
									}
								}
								fixedIndex++;
								// currentBenefit.setFixedCalculated(true);
							}
						// fixed claim

						// check max coverage
						BigDecimal availableClaim = null;
						System.out.println("isFixed : " + isFixed);
						System.out.println("tempfixedMaxAmount : " + tempfixedMaxAmount);
						System.out.println("tempSumInsured : " + tempSumInsured);
						System.out.println("tempFixedMaxDay : " + tempFixedMaxDay);
						System.out.println("claimDay : " + claimDay);
						if (isFixed) {
							availableClaim = new BigDecimal(tempfixedMaxAmount.doubleValue() * tempFixedMaxDay);
						}
						else {
							availableClaim = new BigDecimal(tempSumInsured.doubleValue() * claimDay);
						}
						System.out.println("availableClaim 	  : " + availableClaim);
						System.out.println("maxAmount 		  : " + maxAmount);
						System.out.println("maxCoverageAmount : " + maxCoverageAmount);
						System.out.println("tempSumInsured before : " + tempSumInsured);
						System.out.println("tempfixedMaxAmount before : " + tempfixedMaxAmount);
						System.out.println("availableClaimAmount : " + availableClaimAmount);

						if (availableClaim.compareTo(maxAmount) > 0) {
							tempSumInsured = new BigDecimal((claimDay > 0) ? maxAmount.doubleValue() / (double) claimDay : 0);
							tempfixedMaxAmount = new BigDecimal((tempFixedMaxDay > 0) ? maxAmount.doubleValue() / (double) tempFixedMaxDay : 0);
							availableClaim = maxAmount;
							availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract((isFixed) ? tempfixedMaxAmount
									: tempSumInsured);
							System.out.println("tempSumInsured maxAmount : " + tempSumInsured);
							System.out.println("tempfixedMaxAmount maxAmount : " + tempfixedMaxAmount);
							isFixedClaim = false;
						}

						if (currentBenefit.isDDuctType() && currentClaim.getCoverage().isCovDDuctType()
								&& !currentClaim.getCoverage().isCovDDuctCal()) {
							System.out.println("no calculate max coverage amount because case D Duct");
						}
						else {
							if (!((currentBenefit.getBenefitGroup().equals("ICU1") || currentBenefit.getBenefitGroup().equals("RB1")) && (currentClaim
									.getCoverage().getCoverageGroupCode().equals("RHG")))) {
								if (availableClaim.compareTo(maxCoverageAmount) > 0) {
									tempSumInsured = new BigDecimal((claimDay > 0) ? maxCoverageAmount.doubleValue() / (double) claimDay : 0);
									tempfixedMaxAmount = new BigDecimal((tempFixedMaxDay > 0) ? maxCoverageAmount.doubleValue()
											/ (double) tempFixedMaxDay : 0);
									availableClaim = maxCoverageAmount;
									availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract((isFixed) ? tempfixedMaxAmount
											: tempSumInsured);
									System.out.println("tempSumInsured maxCoverageAmount : " + tempSumInsured);
									System.out.println("tempfixedMaxAmount maxCoverageAmount : " + tempfixedMaxAmount);
									isFixedClaim = false;
								}
							}
						}
						// check max coverage

						// set to claim amount remain
						System.out.println("availableClaimAmount : " + availableClaimAmount);
						if (availableClaimAmount.compareTo(new BigDecimal(0.00)) > 0) {
							ClaimRemain claimRemain = new ClaimRemain();
							if (isFixed && tempFixedMaxDay > 0) {
								claimRemain.setDayRemain(requestClaim.getDayRemain() < tempFixedMaxDay ? requestClaim.getDayRemain()
										: tempFixedMaxDay);
							}
							else {
								claimRemain.setDayRemain(claimDay);
							}
							claimRemain.setAmountRemain(availableClaimAmount.doubleValue());
							claimRemain.setMapIndex(requestClaim.getMapIndex());
							claimRemain.setIsOwnSplit("F");
							claimRemain.setRemainType('A');

							if (currentBenefit.isICUType() || (currentBenefit.isRBType() && benefitGroup.isICUType()))
								claimRemain.setFromICU(true);

							remainClaim.add(claimRemain);
							this.claimRemain.setHasRemain(true);

						}
						// set to available claim
						ClaimRemain claim = new ClaimRemain();
						if (isFixed && tempFixedMaxDay > 0) {
							claim.setDayRemain(requestClaim.getDayRemain() < tempFixedMaxDay ? requestClaim.getDayRemain() : tempFixedMaxDay);
						}
						else {
							claim.setDayRemain(claimDay);
						}
						if (isFixed && tempfixedMaxAmount != null) {
							System.out.println("tempfixedMaxAmount : " + tempfixedMaxAmount);
							claim.setAmountRemain((new BigDecimal(requestClaim.getAmountRemain()).compareTo(tempfixedMaxAmount) > 0 ? tempfixedMaxAmount
									: new BigDecimal(requestClaim.getAmountRemain())).doubleValue());
						}
						else {
							claim.setAmountRemain((new BigDecimal(requestClaim.getAmountRemain()).compareTo(tempSumInsured) > 0 ? tempSumInsured
									: new BigDecimal(requestClaim.getAmountRemain())).doubleValue());
						}
						claim.setMapIndex(requestClaim.getMapIndex());
						claim.setIsOwnSplit(requestClaim.getIsOwnSplit());
						claimAvailable.add(claim);

						// set for calculate de duct 2015-08-04
						if (currentBenefit.isDDuctType() && currentClaim.getCoverage().isCovDDuctType() && currentClaim.getCoverage().isCovDDuctCal()) {
							currentBenefit.getDeDuctList().get(dductIndex).setClaimAmountDeDuct(claim.getAmountRemain());
							currentBenefit.getDeDuctList().get(dductIndex).setClaimDayDeDuct(claimDay);
							dductIndex++;
						}

//						// check isFlagCalculate by benz
//						if (benefitGroup.isFlagCalculate()){
//							currentBenefit.getClaimAmounts().add("" + claim.getAmountRemain());
//						}else{
//							currentBenefit.getClaimAmounts().add("0");
//						}
						
						currentBenefit.getClaimAmounts().add("" + claim.getAmountRemain());
						currentBenefit.getClaimDays().add("" + claim.getDayRemain());
						
						if(debug){
							for (int i = 0; i < currentBenefit.getClaimAmounts().size(); i++){
								System.out.println("ClaimAmounts : " + currentBenefit.getClaimAmounts().get(i));
							}
							System.out.println("5a - currentBenefit.getMapBenefitIndex().add(requestClaim.getMapIndex());");
							System.out.println("requestClaim.getMapIndex() = " + requestClaim.getMapIndex());
						}
						
						currentBenefit.getMapBenefitIndex().add(requestClaim.getMapIndex());
						currentBenefit.getFixedOutput().add((isFixedClaim) ? "T" : "F");
						currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - (claim.getAmountRemain() * claim.getDayRemain()));
						currentBenefit.getIsOwnSplit().add(requestClaim.getIsOwnSplit());
						currentBenefit.getBilledAmounts().add("" + requestClaim.getAmountRemain());
						currentBenefit.getBilledDays().add("" + requestClaim.getDayRemain());

						if (rhlFlag && currentBenefit.isRhlFlag()) {
							currentClaim.setAvailableRHLAmount(currentClaim.getAvailableRHLAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							currentClaim.setAvailableYearRHLAmount(currentClaim.getAvailableYearRHLAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							if (currentClaim.getAvailableRHLAmount() < 0.0)
								currentClaim.setAvailableRHLAmount(0.0);
							if (currentClaim.getAvailableYearRHLAmount() < 0.0)
								currentClaim.setAvailableYearRHLAmount(0.0);
						}

						if (rhhFlag && currentBenefit.isRhhFlag()) {
							currentClaim.setAvailableRHHAmount(currentClaim.getAvailableRHHAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							currentClaim.setAvailableYearRHHAmount(currentClaim.getAvailableYearRHHAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							if (currentClaim.getAvailableRHHAmount() < 0.0)
								currentClaim.setAvailableRHHAmount(0.0);
							if (currentClaim.getAvailableYearRHHAmount() < 0.0)
								currentClaim.setAvailableYearRHHAmount(0.0);
						}

						if (rhgFlag && currentBenefit.isRhgFlag()) {
							if (!isMaxYearBenefit) {
								currentClaim.setAvailableYearRHGAmount(currentClaim.getAvailableYearRHGAmount()
										- (claim.getAmountRemain() * claim.getDayRemain()));
								if (currentClaim.getAvailableYearRHGAmount() < 0.0)
									currentClaim.setAvailableYearRHGAmount(0.0);
							}
						}

						if (rhpFlag && currentBenefit.isRhpFlag()) {
							currentClaim.setAvailableYearRHPAmount(currentClaim.getAvailableYearRHPAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							currentClaim.setMaxCoverageApprovedTotal(currentClaim.getMaxCoverageApprovedTotal()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							if (currentClaim.getAvailableYearRHPAmount() < 0.0)
								currentClaim.setAvailableYearRHPAmount(0.0);
							if (currentClaim.getMaxCoverageApprovedTotal() < 0.0)
								currentClaim.setMaxCoverageApprovedTotal(0.0);
						}

						/* 20180417 jame */
						if (rheFlag && currentBenefit.isRheFlag()) {
							currentClaim.setAvailableRHEAmount(currentClaim.getAvailableRHEAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							currentClaim.setAvailableYearRHEAmount(currentClaim.getAvailableYearRHEAmount()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							currentClaim.setMaxLifeBenefitTotal(currentClaim.getMaxLifeBenefitTotal()
									- (claim.getAmountRemain() * claim.getDayRemain()));
							if (currentClaim.getAvailableRHEAmount() < 0.0)
								currentClaim.setAvailableRHEAmount(0.0);
							if (currentClaim.getAvailableYearRHEAmount() < 0.0)
								currentClaim.setAvailableYearRHEAmount(0.0);
							if (currentClaim.getMaxLifeBenefitTotal() < 0.0)
								currentClaim.setMaxLifeBenefitTotal(0.0);
						}

						// edit by TeTe for group sum benefit 2014-07-22
						// -----------------------------------
						if (groupSumBenefitFlag) {
							currentClaim.getCoverage()
									.setAvailableGroupSumBenefitAmount(
											currentClaim.getCoverage().getAvailableGroupSumBenefitAmount()
													- (claim.getAmountRemain() * claim.getDayRemain()));
							if (currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() < 0.0) {
								currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(0.0);
							}
						}
						// ----------------------------------------------------------------------------------

						// d duct
						if (currentBenefit.isDDuctType()) {
							System.out.println("before sum coverage : " + currentClaim.getSumCoverageClaimDDuct());
							System.out.println("claim amount d duct : " + claim.getAmountRemain() * claim.getDayRemain());
							currentClaim.addSumCoverageClaimDDuct(claim.getAmountRemain() * claim.getDayRemain());
							System.out.println("after sum coverage : " + currentClaim.getSumCoverageClaimDDuct());
						}
						// benefitGroup = setClaimBenefitGroup(benefitGroup,
						// claim.getAmountRemain(), claim.getDayRemain(),
						// requestClaim.getMapIndex());
						benefitGroup.getClaimAmounts().add(claim.getAmountRemain());
						benefitGroup.getClaimAmountDays().add((short) claim.getDayRemain());

						// calculate max day and max amount
						maxDay = maxDay - claim.getDayRemain();// subtract
																// maxDay by
																// claim use day

						// for case special add 80% of excess
						maxDayForSpecialCase = maxDayForSpecialCase - claim.getDayRemain();

						// for case cal RB same ICU
						currentBenefit.setAvailableDays(maxDay);

						if ((!rhlFlag || !rhhFlag || !rhgFlag || !rhpFlag || rheFlag) && groupShareDay) {
							currentClaim.calculateShareDayICUandRB(claim.getDayRemain());
							System.out.println("limitICUandRB : " + currentClaim.getLimitICUandRB());
						}

						double claimAmount = claim.getDayRemain() * claim.getAmountRemain();
						System.out.println("claim amount 				: " + claimAmount);
						System.out.println("max claim amount before 	: " + maxAmount);
						System.out.println("max coverage amount before : " + maxCoverageAmount);
						System.out.println("max Limit LUMSUM before 	: " + maxLimitAmountForLumsum);

						if (!((currentBenefit.getBenefitGroup().equals("ICU1") || currentBenefit.getBenefitGroup().equals("RB1")) && (currentClaim
								.getCoverage().getCoverageGroupCode().equals("RHG")))) {
							maxAmount = maxAmount.subtract(new BigDecimal(claimAmount));
							maxCoverageAmount = maxCoverageAmount.subtract(new BigDecimal(claimAmount));
//							currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - claimAmount);
							
							// setMaxAvailableAmount no cal icu&rb
							if (benefitGroup.isFlagCalculate()){
								System.out.println("isFlagCalculate : True");
								currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - claimAmount);
							}
							else{
								System.out.println("isFlagCalculate : Flase");
								currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - 0);
							}
						}
						if (isLumsum)
							maxLimitAmountForLumsum = maxLimitAmountForLumsum.subtract(new BigDecimal(claimAmount));

						System.out.println("max claim amount after  	: " + maxAmount);
						System.out.println("max coverage amount after 	: " + maxCoverageAmount);
						System.out.println("max Limit LUMSUM after 	: " + maxLimitAmountForLumsum);

						if (availableClaimDay > 0) {
							ClaimRemain claimRemain = new ClaimRemain();
							claimRemain.setDayRemain(availableClaimDay);
							claimRemain.setAmountRemain(requestClaim.getAmountRemain());
							claimRemain.setIsOwnSplit("F");
							claimRemain.setRemainType('D');

							if (currentBenefit.isICUType())
								claimRemain.setTransferFlag(true);

							// if(benefitGroup.isICUType())
							// claimRemain.setFromICU(true);
							addMapClaimIndex();
							claimRemain.setMapIndex(mapClaimIndex);
							remainClaim.add(claimRemain);
							this.claimRemain.setHasRemain(true);
						}// if(availableClaimDay > 0)
							// ---------------------------------back change
							// 09/09/2013
					}// if(maxDay > 0 && maxAmount > 0)
					else {
						System.out.println("case claim 0.0");
						// if(currentClaim.getLimitICUandRB() <= 0 ||
						// maxCoverageAmount.compareTo(new BigDecimal(0.00)) <=
						// 0){
						// requestClaim.setFromICU(false);
						// }
						requestClaim.setFromICU(false);
						if (currentBenefit.isICUType())
							requestClaim.setTransferFlag(true);

						remainClaim.add(requestClaim);
						// P'Fon and P'M request to don't show billedAmount when
						// can not claim 19/09/2013 17.50
						// if(!requestClaim.isTransferFlag() /*||
						// currentBenefit.isICUType()*/){
						currentBenefit.getClaimAmounts().add("0.00");
						currentBenefit.getClaimDays().add("0");
						currentBenefit.getIsOwnSplit().add("F");
						currentBenefit.getFixedOutput().add("F");
						
						if(debug){
							for (int i = 0; i < currentBenefit.getClaimAmounts().size(); i++){
								System.out.println("ClaimAmounts : " + currentBenefit.getClaimAmounts().get(i));
							}
							System.out.println("5b - currentBenefit.getMapBenefitIndex().add(-1);");
						}
						
						currentBenefit.getMapBenefitIndex().add(-1);

						if (requestClaim != null) {
							System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
							System.out.println("requestClaim.getDayRemain()    : " + requestClaim.getDayRemain());
							currentBenefit.getBilledAmounts().add("" + requestClaim.getAmountRemain());
							currentBenefit.getBilledDays().add("" + requestClaim.getDayRemain());
						}
						// }
					}
				}//
			}// for claimRemain

			currentBenefit.setdDuctIndex(dductIndex);
			currentBenefit.setFixedIndex(fixedIndex);

			// check claimAmount and day when can not claim
			if ((currentBenefit.getClaimAmounts() == null || !(currentBenefit.getClaimAmounts().size() > 0)) && !exception) {
				currentBenefit.getClaimAmounts().add("0.00");
				currentBenefit.getClaimDays().add("0");
				currentBenefit.getIsOwnSplit().add("F");
				currentBenefit.getFixedOutput().add("F");
				
				if(debug){
					for (int i = 0; i < currentBenefit.getClaimAmounts().size(); i++){
						System.out.println("ClaimAmounts : " + currentBenefit.getClaimAmounts().get(i));
					}
					System.out.println("5c - currentBenefit.getMapBenefitIndex().add(-1);");
				}
				
				currentBenefit.getMapBenefitIndex().add(-1);

				if (remain != null && remain.size() > 0) {
					for (int i = 0; i < remain.size(); i++) {
						if (!remain.get(i).isTransferFlag()) {
							currentBenefit.getBilledAmounts().add("" + remain.get(i).getAmountRemain());
							currentBenefit.getBilledDays().add("" + remain.get(i).getDayRemain());
							break;
						}
					}
				}
				// else{
				// currentBenefit.getBilledAmounts().add("0.0");
				// currentBenefit.getBilledDays().add("0");
				// }
				// benefitGroup.getClaimAmounts().add(0.00);
				// benefitGroup.getClaimAmountDays().add((short) 0);
				
				if(debug){
					System.out.println("16 - setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);");
				}
				
				benefitGroup = setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);
			}
			System.out.println("maxDayForSpecialCase after calculate : " + maxDayForSpecialCase);
			/**/
			if (currentClaim.isCoverageSpecialCaseRS0() && currentBenefit.isBenefitSpecialCaseRS0() && maxDayForSpecialCase <= 0) {
				System.out.println("case RS0");
				// BigDecimal sumTotal = new BigDecimal(0);
				BigDecimal tempMaxCoverageAmount = new BigDecimal(maxCoverageAmount.doubleValue());
				List<ClaimRemain> tempRemainClaim = new ArrayList<ClaimRemain>();

				for (ClaimRemain remain : remainClaim) {
					System.out.println("remain type : >>" + remain.getRemainType() + "<<");
					System.out.println("maxAmount : >>" + maxAmount + "<<");
					System.out.println("tempMaxCoverageAmount : >>" + tempMaxCoverageAmount + "<<");
					if (remain.getRemainType() == 'D' && maxAmount.compareTo(new BigDecimal("0.00")) > 0
							&& tempMaxCoverageAmount.compareTo(new BigDecimal("0.00")) > 0) {
						ClaimRemain claimAvailableRS0 = new ClaimRemain();

						int RS0DayRemain = remain.getDayRemain();
						double RS0AmountRemain = remain.getAmountRemain();

						System.out.println("before remain.getAmountRemain() : >>" + remain.getAmountRemain() + "<<");
						System.out.println("after  remain.getDayRemain() : >>" + remain.getDayRemain() + "<<");
						BigDecimal claim = new BigDecimal(remain.getAmountRemain() * 80.00 / 100.00);

						BigDecimal total = new BigDecimal(remain.getDayRemain() * (claim.doubleValue()));
						if (total.compareTo(tempMaxCoverageAmount) < 0) {
							tempMaxCoverageAmount = tempMaxCoverageAmount.subtract(total);
						}
						else {
							total = new BigDecimal(tempMaxCoverageAmount.doubleValue());
							claim = new BigDecimal((remain.getDayRemain() > 0) ? tempMaxCoverageAmount.doubleValue() / remain.getDayRemain() : 0);
							tempMaxCoverageAmount = new BigDecimal("0.00");
						}

						if (total.compareTo(maxAmount) < 0) {
							maxAmount = maxAmount.subtract(total);
						}
						else {
							total = new BigDecimal(maxAmount.doubleValue());
							claim = new BigDecimal((remain.getDayRemain() > 0) ? maxAmount.doubleValue() / remain.getDayRemain() : 0);
							maxAmount = new BigDecimal("0.00");
						}

						claimAvailableRS0.setAmountRemain(claim.doubleValue());
						claimAvailableRS0.setDayRemain(remain.getDayRemain());
						// sumTotal = sumTotal.add(total);
						System.out.println("after  remain.getAmountRemain() : >>" + claimAvailableRS0.getAmountRemain() + "<<");
						System.out.println("after  remain.getDayRemain() : >>" + claimAvailableRS0.getDayRemain() + "<<");

						// set to claim amount remain
						if ((remain.getAmountRemain() - claim.doubleValue()) > 0) {
							remain.setDayRemain(remain.getDayRemain());
							remain.setAmountRemain(remain.getAmountRemain() - claim.doubleValue());
							this.claimRemain.setHasRemain(true);
						}

						// add claim available
						claimAvailableRS0.setMapIndex(remain.getMapIndex());
						claimAvailableRS0.setIsOwnSplit(remain.getIsOwnSplit());
						claimAvailable.add(claimAvailableRS0);

						currentBenefit.getClaimAmounts().add("" + claimAvailableRS0.getAmountRemain());
						currentBenefit.getClaimDays().add("" + claimAvailableRS0.getDayRemain());
						currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount()
								- (claimAvailableRS0.getAmountRemain() * claimAvailableRS0.getDayRemain()));
						
						if(debug){
							for (int i = 0; i < currentBenefit.getClaimAmounts().size(); i++){
								System.out.println("ClaimAmounts : " + currentBenefit.getClaimAmounts().get(i));
							}
							System.out.println("5d - currentBenefit.getMapBenefitIndex().add(remain.getMapIndex());");
							System.out.println("remain.getMapIndex() = " + remain.getMapIndex());
						}
						
						currentBenefit.getMapBenefitIndex().add(remain.getMapIndex());
						currentBenefit.getFixedOutput().add("F");

						currentBenefit.getIsOwnSplit().add(remain.getIsOwnSplit());
						currentBenefit.getBilledAmounts().add("" + RS0AmountRemain);
						currentBenefit.getBilledDays().add("" + RS0DayRemain);

						// d duct
						if (currentBenefit.isDDuctType()) {
							System.out.println("before sum coverage : " + currentClaim.getSumCoverageClaimDDuct());
							System.out.println("claim amount d duct : " + claimAvailableRS0.getAmountRemain() * claimAvailableRS0.getDayRemain());
							currentClaim.addSumCoverageClaimDDuct(claimAvailableRS0.getAmountRemain() * claimAvailableRS0.getDayRemain());
							System.out.println("after sum coverage : " + currentClaim.getSumCoverageClaimDDuct());
						}
						// benefitGroup = setClaimBenefitGroup(benefitGroup,
						// claim.getAmountRemain(), claim.getDayRemain(),
						// requestClaim.getMapIndex());
						benefitGroup.getClaimAmounts().add(claimAvailableRS0.getAmountRemain());
						benefitGroup.getClaimAmountDays().add((short) claimAvailableRS0.getDayRemain());

						// for case cal RB same ICU
						currentBenefit.setAvailableDays(maxDay);

						if ((!rhlFlag || !rhhFlag || !rhpFlag || !rhgFlag || !rheFlag) && groupShareDay) {
							currentClaim.calculateShareDayICUandRB(claimAvailableRS0.getDayRemain());
							System.out.println("limitICUandRB : " + currentClaim.getLimitICUandRB());
						}

						double claimAmount = claimAvailableRS0.getDayRemain() * claimAvailableRS0.getAmountRemain();
						System.out.println("RS0 claim amount 				: " + claimAmount);
						System.out.println("RS0 max claim amount before 	: " + maxAmount);
						System.out.println("RS0 max coverage amount before : " + maxCoverageAmount);
						System.out.println("RS0 max Limit LUMSUM before 	: " + maxLimitAmountForLumsum);
						maxAmount = maxAmount.subtract(new BigDecimal(claimAmount));
						currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - claimAmount);
						maxCoverageAmount = maxCoverageAmount.subtract(new BigDecimal(claimAmount));
						if (isLumsum)
							maxLimitAmountForLumsum = maxLimitAmountForLumsum.subtract(new BigDecimal(claimAmount));

					}
				}

				if (tempRemainClaim != null && tempRemainClaim.size() > 0)
					remainClaim.addAll(tempRemainClaim);

			}// for RS0
			/**/

			// set value back
			currentBenefit.setClaim(claimAvailable);
			currentBenefit.setRemain(remainClaim);
			benefitGroups.set(benefitGroupIndex, benefitGroup);

			System.out.println("----------------claimAvailable-----------------");
			printArrayList(claimAvailable);
			System.out.println("----------------remainClaim--------------------");
			printArrayList(remainClaim);

			if (remainClaim != null && remainClaim.size() > 0) {
				this.claimRemain.setHasRemain(true);
			}

			// fixed claim, not add to other calculate option
			remain = remainClaim;
			// set currant back
			System.out.println("currentClaimIndex : " + currentClaimIndex);
			System.out.println("benefitGroupIndex : " + benefitGroupIndex);
			System.out.println("currentBenefitIndex : " + currentClaim.getCurrentBenefitIndex());

			currentClaim.setBenefit(currentBenefit);
			// currentClaim.getCoverage().setMaxAvailableAmount(maxCoverageAmount.doubleValue());
			currentClaims.set(currentClaimIndex, currentClaim);

			// for ICU send day to RB in same coverage
			System.out.println("benefit : " + currentClaim.getCurrentBenefit().getBenefitGroup());
			System.out.println("is RB type : >>" + currentClaim.getCurrentBenefit().isRBType() + "<<");

			if (benefitGroup.isICUType() && currentClaim.isICUactive() && !(currentClaim.getCurrentBenefit().isRBType())) {
				System.out.println("in case ICU");
				currentClaim.setICUactive(false);
				int benefitIndex = currentClaim.getCurrentBenefitIndex();
				System.out.println("benefitIndex : " + benefitIndex);
				for (int i = benefitIndex; i < currentClaim.getBenefits().size(); i++) {
					Benefit benefit = currentClaim.getBenefits().get(i);
					if (benefit.isRBType()) {
						currentClaim.setCurrentBenefitIndex(i);
						currentClaims.set(currentClaimIndex, currentClaim);
						System.out.println("calculate recusive");
						calculateClaim();
						break;
					}

				}
				System.out.println("out case ICU");
			}
			// for ICU send day to RB in same coverage
		}
		System.out.println("out calculateClaim");
	}

	// calculate amount only
	/**
	 * Calculate claim amount only.
	 */
	public void calculateClaimAmountOnly() {
		System.out.println("in calculateClaimAmountOnly");
		System.out.println("----------------remain-----------------");
		printArrayList(remain);
		this.claimRemain.setHasRemain(false);
		if (currentClaimIndex < currentClaims.size() && benefitGroupIndex < benefitGroups.size() && checkRemain()) {
			Claim currentClaim = currentClaims.get(currentClaimIndex);
			Benefit currentBenefit = currentClaim.getCurrentBenefit();
			BenefitGroup benefitGroup = benefitGroups.get(benefitGroupIndex);

			// return while is case medical expense PMFMC is already calculate
			// and this benefit is PMF
			if (currentBenefit.isMedicalExpenseReceiver() && currentClaim.isCalculatedMedicalExpense()) {
				return;
			}

			double sumInsuredDouble = currentBenefit.getSumInsured();
			if (sumInsuredDouble < 0.0)
				sumInsuredDouble = 0.0;

			BigDecimal maxAmount = BigDecimal.ZERO;
			System.out.println("benefit group code : " + benefitGroup.getBenefitGroupCode());
			System.out.println("currentBenefit group code : " + currentBenefit.getBenefitGroup());
			System.out.println("currentBenefit option     : " + currentBenefit.getCalculateOption());
			System.out.println("sumInsuredDouble : " + sumInsuredDouble);
			System.out.println("currentBenefit ContinuityClaimAmount : " + currentBenefit.getContinuityClaimAmount());

			float tempClaimPercent = 0;

			// if(ModelUtility.isOneOfInList(benefitGroup.getGroupType(), "SF")
			// && !currentClaim.getCoverage().isCovDDuctType()){
			if (currentBenefit.getBenefitGroupType().equalsIgnoreCase("SF") && !currentClaim.getCoverage().isCovDDuctType()) {
				sumInsuredDouble = (sumInsuredDouble * benefitGroup.getBilledAmountPercent()) / 100.00;
				maxAmount = new BigDecimal(currentBenefit.getSumInsured()
						- ((currentClaim.isROP() || benefitGroup.isHM()) ? 0 : currentBenefit.getContinuityClaimAmount()));
				tempClaimPercent = benefitGroup.getBilledAmountPercent();
			}
			else {
				if (currentBenefit.getCalculateOption().equals(CalculateOption.LUMPSUM_PERCENTAGE)
						|| currentBenefit.getCalculateOption().equals(CalculateOption.LUMPSUM_PERCENTAGE_LIMITED)) {
					sumInsuredDouble = (sumInsuredDouble * benefitGroup.getBilledMaxPercent()) / 100.00;
					tempClaimPercent = benefitGroup.getBilledMaxPercent();
				}
				maxAmount = new BigDecimal(sumInsuredDouble
						- ((currentClaim.isROP() || benefitGroup.isHM()) ? 0 : currentBenefit.getContinuityClaimAmount()));
			}
			System.out.println("sumInsuredDouble : " + sumInsuredDouble);
			System.out.println("maxAmount : " + maxAmount);
			BigDecimal sumInsured = new BigDecimal(sumInsuredDouble);

			boolean isMaxYearBenefit = currentBenefit.isMaxYearBenefit();
			// RHL sum group
			if (currentClaim.isRHL() && currentBenefit.isRhlFlag()) {
				if (maxAmount.doubleValue() > currentClaim.getAvailableRHLAmount()) {
					maxAmount = new BigDecimal(currentClaim.getAvailableRHLAmount());
					System.out.println("max coverage amount avilable RHL : " + maxAmount);
				}
				if (maxAmount.doubleValue() > currentClaim.getAvailableYearRHLAmount()) {
					maxAmount = new BigDecimal(currentClaim.getAvailableYearRHLAmount());
					System.out.println("max coverage year amount avilable RHL : " + maxAmount);
				}
			}

			/* 20160927 */// RHH sum group
			/* 20160927 */if (currentClaim.isRHH() && currentBenefit.isRhhFlag()) {
				if (!isMaxYearBenefit) {
					/* 20160927 */if (maxAmount.doubleValue() > currentClaim.getAvailableRHHAmount()) {
						/* 20160927 */maxAmount = new BigDecimal(currentClaim.getAvailableRHHAmount());
						/* 20160927 */System.out.println("max coverage amount avilable RHH : " + maxAmount);
						/* 20160927 */}
				}
				/* 20160927 */if (maxAmount.doubleValue() > currentClaim.getAvailableYearRHHAmount()) {
					/* 20160927 */maxAmount = new BigDecimal(currentClaim.getAvailableYearRHHAmount());
					/* 20160927 */System.out.println("max coverage year amount avilable RHH : " + maxAmount);
					/* 20160927 */}
				/* 20160927 */}

			if (currentClaim.isRHG() && currentBenefit.isRhgFlag()) {
				if (maxAmount.doubleValue() > currentClaim.getAvailableYearRHGAmount()) {
					maxAmount = new BigDecimal(currentClaim.getAvailableYearRHGAmount());
					System.out.println("max coverage year amount avilable RHG : " + maxAmount);
				}
			}

			if (currentClaim.isRHP() && currentBenefit.isRhpFlag()) {
				if (maxAmount.doubleValue() > currentClaim.getAvailableYearRHPAmount()) {
					maxAmount = new BigDecimal(currentClaim.getAvailableYearRHPAmount());
					System.out.println("max coverage year amount avilable RHP : " + maxAmount);
				}
				if (maxAmount.doubleValue() > currentClaim.getMaxCoverageApprovedTotal()) {
					maxAmount = new BigDecimal(currentClaim.getMaxCoverageApprovedTotal());
					System.out.println("max coverage approved total : " + maxAmount);
				}
			}

			/* 20180417 */// RHE sum group jame
			/* 20180417 */if (currentClaim.isRHE() && currentBenefit.isRheFlag()) {
				if (!isMaxYearBenefit) {
					/* 20180417 */if (maxAmount.doubleValue() > currentClaim.getAvailableRHEAmount()) {
						/* 20180417 */maxAmount = new BigDecimal(currentClaim.getAvailableRHEAmount());
						/* 20180417 */System.out.println("max coverage amount avilable RHE : " + maxAmount);
						/* 20180417 */}
				}
				/* 20180417 */if (maxAmount.doubleValue() > currentClaim.getAvailableYearRHEAmount()) {
					/* 20180417 */maxAmount = new BigDecimal(currentClaim.getAvailableYearRHEAmount());
					/* 20180417 */System.out.println("max coverage year amount avilable RHE : " + maxAmount);
					/* 20180417 */}

				if (currentBenefit.getBenefitGroup().equalsIgnoreCase("PSY01")) {
					if (maxAmount.doubleValue() > currentClaim.getMaxLifeBenefitTotal()) {
						maxAmount = new BigDecimal(currentClaim.getMaxLifeBenefitTotal());
						System.out.println("max life benefit approved total : " + maxAmount);
					}
				}
			}

			if (maxAmount.doubleValue() < 0.00)
				maxAmount = new BigDecimal("0.00");
			System.out.println("max amount : " + maxAmount);

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			boolean groupSumBenefitFlag = false;
			if (currentClaim.getCoverage().isGroupSumBenefit() && currentBenefit.isGroupSumBenefit()) {
				groupSumBenefitFlag = true;
			}

			if (groupSumBenefitFlag) {
				if (maxAmount.compareTo(new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount())) > 0) {
					maxAmount = new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount());
					if (maxAmount.compareTo(new BigDecimal(0)) < 0) {
						maxAmount = new BigDecimal(0);
					}
					System.out.println("max amount after group sum benefit : " + maxAmount);
				}
			}
			// ----------------------------------------------------------------------------------

			System.out.println("before sumInsured : " + sumInsured);
			System.out.println("currentBenefit.getAvailableAmount() : " + currentBenefit.getAvailableAmount());
			if (new BigDecimal(currentBenefit.getAvailableAmount()).compareTo(sumInsured) < 0)
				sumInsured = new BigDecimal(currentBenefit.getAvailableAmount());

			if (maxAmount.compareTo(sumInsured) < 0)
				sumInsured = new BigDecimal(maxAmount.doubleValue());

			System.out.println("after  sumInsured : " + sumInsured);
			BigDecimal tempSumInsured = sumInsured;

			// max coverage
			BigDecimal maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getMaxAvailableAmount());
			System.out.println("maxCoverageAmount : " + maxCoverageAmount);

			// fixed claim
			List<String> fixedInputList = (currentBenefit.getFixedInput() != null && currentBenefit.getFixedInput().size() > 0) ? currentBenefit
					.getFixedInput() : null;

			int dDuctIndex = 0;
			int fixedIndex = 0;

			// set remain to request of benefit
			currentBenefit.setRequest(remain);
			request = currentBenefit.getRequest();

			BigDecimal availableClaimAmount = null;
			List<ClaimRemain> remainClaim = new ArrayList<ClaimRemain>();
			List<ClaimRemain> claimAvailable = new ArrayList<ClaimRemain>();
			// check benefit to claim
			System.out.println("request size : " + request.size());
			boolean exception = false;
			for (ClaimRemain requestClaim : request) {
				if ((benefitGroup.getBenefitGroupCode().equals("PMFMC") || benefitGroup.getBenefitGroupCode().equals("PMF"))
						&& (currentBenefit.getBenefitGroup().equals("ER1") || currentBenefit.getBenefitGroup().equals("GEN14")
								|| currentBenefit.getBenefitGroup().equals("GEN24") || currentBenefit.getBenefitGroup().equals("GEN44"))) {
					remainClaim.add(requestClaim);
					exception = true;
				}
				else {
					// not available claim
					availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract(sumInsured);
					System.out.println("availableClaimAmount : " + availableClaimAmount);

					// fixed claim
					BigDecimal tempfixedMaxAmount = sumInsured;
					boolean isFixed = false;
					boolean isFixedClaim = false;
					// d
					// duct-------------------------------------------------------
					System.out.println("current benefit is fixed : >>" + currentBenefit.isFixed() + "<<");
					System.out.println("current benefit is fixed calculated : >>" + currentBenefit.isFixedCalculated() + "<<");

					if (currentBenefit.isDDuctType() && currentClaim.getCoverage().isCovDDuctType() && currentClaim.getCoverage().isCovDDuctCal()) {
						if (currentBenefit.getDeDuctList().get(dDuctIndex).isCalculateDeDuct()) {
							System.out.println("case no cal again");
							System.out.println("before requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
							System.out
									.println("currentBenefit.getClaimDeDuct() : " + currentBenefit.getDeDuctList().get(dDuctIndex).getClaimDeDuct());
							// double deduct = requestClaim.getAmountRemain() -
							// currentBenefit.getDeDuctList().get(dDuctIndex).getClaimDeDuct();
							// requestClaim.setAmountRemain(deduct>0?deduct:0);
							BigDecimal tempClaimRemain = new BigDecimal(requestClaim.getAmountRemain());
							BigDecimal dDuctFixedSumInsured = new BigDecimal(currentBenefit.getDeDuctList().get(dDuctIndex).getClaimAmountDeDuct());
							availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
							tempfixedMaxAmount = dDuctFixedSumInsured;
							isFixed = true;

							System.out.println("after requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
							System.out.println("availableClaimAmount : " + availableClaimAmount);
							System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
						}
						else {
							double dDuctAmount = currentClaim.getTotalRemainDeDuct();
							System.out.println("availableClaimAmount : " + availableClaimAmount);
							if (dDuctAmount <= 0.0) {

								if (currentClaim.getCoverage().isCovDDuct100()) {
									BigDecimal tempClaimRemain = new BigDecimal(requestClaim.getAmountRemain());

									currentClaim.getCoverage().setSumBeforeDeDuctAmount(
											currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

									BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue());
									currentBenefit.getDeDuctList().get(dDuctIndex).setClaimDeDuct(0.0);
									System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
									System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
									System.out.println("tempSumInsured : " + tempSumInsured);
									if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
										availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
										tempfixedMaxAmount = dDuctFixedSumInsured;
										isFixed = true;
									}
									currentClaim.setTotalRemainDeDuct(0.0);
								}
								else {
									// 80%
									BigDecimal tempClaimRemain = new BigDecimal(requestClaim.getAmountRemain());

									// 2017-11-22 TeTe cr sum before cal 80%
									currentClaim.getCoverage().setSumBeforeDeDuctAmount(
											currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

									BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue() * 80 / 100);
									currentBenefit.getDeDuctList().get(dDuctIndex).setClaimDeDuct(0.0);
									System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
									System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
									System.out.println("tempSumInsured : " + tempSumInsured);
									if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
										availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
										tempfixedMaxAmount = dDuctFixedSumInsured;
										isFixed = true;
									}
									currentClaim.setTotalRemainDeDuct(0.0);
								}
							}
							else {
								if (currentClaim.getCoverage().isCovDDuct100()) {
									System.out.println("case normal");
									double deduct = requestClaim.getAmountRemain() - dDuctAmount;
									BigDecimal tempClaimRemain = new BigDecimal(deduct > 0 ? deduct : 0);

									currentClaim.getCoverage().setSumBeforeDeDuctAmount(
											currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

									BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue());
									currentBenefit
											.getDeDuctList()
											.get(dDuctIndex)
											.setClaimDeDuct(
													requestClaim.getAmountRemain() > dDuctAmount ? dDuctAmount : requestClaim.getAmountRemain());

									// requestClaim.setAmountRemain(tempClaimRemain.doubleValue());
									System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
									System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
									System.out.println("tempSumInsured : " + tempSumInsured);
									System.out.println("currentBenefit.getClaimDeDuct() : "
											+ currentBenefit.getDeDuctList().get(dDuctIndex).getClaimDeDuct());
									if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
										availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
										tempfixedMaxAmount = dDuctFixedSumInsured;
										isFixed = true;
									}
									double deductRemain = dDuctAmount - (currentBenefit.getDeDuctList().get(dDuctIndex).getClaimDeDuct());
									currentClaim.setTotalRemainDeDuct(deductRemain > 0 ? deductRemain : 0.0);
								}

								else {
									System.out.println("case normal");
									double deduct = requestClaim.getAmountRemain() - dDuctAmount;
									BigDecimal tempClaimRemain = new BigDecimal(deduct > 0 ? deduct : 0);

									// 2017-11-22 TeTe cr sum before cal 80%
									currentClaim.getCoverage().setSumBeforeDeDuctAmount(
											currentClaim.getCoverage().getSumBeforeDeDuctAmount().add(tempClaimRemain));

									BigDecimal dDuctFixedSumInsured = new BigDecimal(tempClaimRemain.doubleValue() * 80 / 100);
									currentBenefit
											.getDeDuctList()
											.get(dDuctIndex)
											.setClaimDeDuct(
													requestClaim.getAmountRemain() > dDuctAmount ? dDuctAmount : requestClaim.getAmountRemain());

									// requestClaim.setAmountRemain(tempClaimRemain.doubleValue());
									System.out.println("requestClaim.getAmountRemain() : " + requestClaim.getAmountRemain());
									System.out.println("dDuctFixedSumInsured : " + dDuctFixedSumInsured);
									System.out.println("tempSumInsured : " + tempSumInsured);
									System.out.println("currentBenefit.getClaimDeDuct() : "
											+ currentBenefit.getDeDuctList().get(dDuctIndex).getClaimDeDuct());
									if (dDuctFixedSumInsured.compareTo(tempSumInsured) < 0) {
										availableClaimAmount = tempClaimRemain.subtract(dDuctFixedSumInsured);
										tempfixedMaxAmount = dDuctFixedSumInsured;
										isFixed = true;
									}
									double deductRemain = dDuctAmount - (currentBenefit.getDeDuctList().get(dDuctIndex).getClaimDeDuct());
									currentClaim.setTotalRemainDeDuct(deductRemain > 0 ? deductRemain : 0.0);
								}
								currentBenefit.getDeDuctList().get(dDuctIndex).setCalculateDeDuct(true);
							}
						}
					}
					else
						// d
						// duct-------------------------------------------------------
						if (currentBenefit.isFixed() && !currentBenefit.isFixedCalculated()) {
							System.out.println("case fixed");
							System.out.println("have in put list" + (fixedInputList != null ? "true" : "false"));
							System.out.println("fixedIndex " + fixedIndex);
							System.out.println("currentBenefit.getFixedAmounts().size()  : "
									+ (currentBenefit.getFixedAmounts() != null ? currentBenefit.getFixedAmounts().size() : "null"));
							System.out.println("currentBenefit.getFixedPercents().size() : "
									+ (currentBenefit.getFixedPercents() != null ? currentBenefit.getFixedPercents().size() : "null"));
							if (fixedInputList != null && fixedInputList.size() > fixedIndex && fixedInputList.get(fixedIndex).equalsIgnoreCase("T")) {
								if (currentBenefit.getFixedPercents() != null && currentBenefit.getFixedPercents().size() > 0) {
									float fixedPercent = Float.parseFloat(currentBenefit.getFixedPercents().get(fixedIndex).toString());
									benefitGroup.setBilledAmountPercent(fixedPercent);
									System.out.println("currentBenefit.getFixedPercents().get(" + fixedIndex + ") : " + fixedPercent);

									sumInsuredDouble = (currentBenefit.getSumInsured() * fixedPercent) / 100.00;

									sumInsured = new BigDecimal(sumInsuredDouble);

									System.out.println("before fixed sumInsured : " + sumInsured);
									if (maxAmount.compareTo(sumInsured) < 0) {
										sumInsured = new BigDecimal(maxAmount.doubleValue());
									}
									System.out.println("after  fixed sumInsured : " + sumInsured);

									availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract(sumInsured);
									System.out.println("fixed availableClaimAmount : " + availableClaimAmount);
									tempfixedMaxAmount = sumInsured;

								}
								if (currentBenefit.getFixedAmounts() != null && currentBenefit.getFixedAmounts().size() > 0) {
									double fixedSumInsured = Double.parseDouble(currentBenefit.getFixedAmounts().get(fixedIndex).toString());
									System.out.println("currentBenefit.getFixedAmounts().get(" + fixedIndex + ") : " + fixedSumInsured);

									BigDecimal fixedMaxAmount = new BigDecimal(fixedSumInsured);
									System.out.println("fixedMaxAmount : " + fixedMaxAmount);
									if (fixedMaxAmount.compareTo(tempSumInsured) != 0) {// check
																						// no
																						// fixed
																						// amount
										if (fixedMaxAmount.compareTo(sumInsured) < 0) {
											availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract(fixedMaxAmount);
											System.out.println("fixed availableClaimAmount : " + availableClaimAmount);
											tempfixedMaxAmount = fixedMaxAmount;
											isFixed = true;
											isFixedClaim = true;
										}
									}
								}
							}
							fixedIndex++;
							// currentBenefit.setFixedCalculated(true);
						}
					// fixed claim

					// check max coverage
					BigDecimal availableClaim = new BigDecimal((isFixed) ? tempfixedMaxAmount.doubleValue() : sumInsured.doubleValue());
					System.out.println("availableClaim 	  : " + availableClaim);
					System.out.println("maxAmount 		  : " + maxAmount);
					System.out.println("maxCoverageAmount : " + maxCoverageAmount);
					System.out.println("sumInsured before : " + sumInsured);
					System.out.println("tempfixedMaxAmount before : " + tempfixedMaxAmount);
					if (availableClaim.compareTo(maxAmount) > 0) {
						sumInsured = new BigDecimal(maxAmount.doubleValue());
						tempfixedMaxAmount = new BigDecimal(maxAmount.doubleValue());
						availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract((isFixed) ? tempfixedMaxAmount : sumInsured);
						System.out.println("sumInsured maxAmount : " + sumInsured);
						System.out.println("tempfixedMaxAmount maxAmount : " + tempfixedMaxAmount);
						isFixedClaim = false;
					}

					if (currentBenefit.isDDuctType() && currentClaim.getCoverage().isCovDDuctType() && !currentClaim.getCoverage().isCovDDuctCal()) {
						System.out.println("no calculate max coverage amount because case D Duct");
					}
					else {
						/*
						 * if((!currentBenefit.getBenefitGroup().equals("OD1"))
						 * ||
						 * (!currentBenefit.getBenefitGroup().equals("GEN12"))
						 * ||
						 * (!currentBenefit.getBenefitGroup().equals("GEN22"))
						 * ||
						 * (!currentBenefit.getBenefitGroup().equals("GEN42"))
						 * ||
						 * (!currentBenefit.getBenefitGroup().equals("GEN13"))
						 * ||
						 * (!currentBenefit.getBenefitGroup().equals("GEN23"))
						 * ||
						 * (!currentBenefit.getBenefitGroup().equals("GEN43"))){
						 */
						if (!isMaxYearBenefit) {
							if (availableClaim.compareTo(maxCoverageAmount) > 0) {
								sumInsured = new BigDecimal(maxCoverageAmount.doubleValue());
								tempfixedMaxAmount = new BigDecimal(maxCoverageAmount.doubleValue());
								availableClaimAmount = new BigDecimal(requestClaim.getAmountRemain()).subtract((isFixed) ? tempfixedMaxAmount
										: sumInsured);
								System.out.println("sumInsured maxCoverageAmount : " + sumInsured);
								System.out.println("tempfixedMaxAmount maxCoverageAmount : " + tempfixedMaxAmount);
								isFixedClaim = false;
							}
						}
					}
					// check max coverage

					// set to claim remain
					if (availableClaimAmount.compareTo(new BigDecimal(0.00)) > 0) {
						ClaimRemain claimRemain = new ClaimRemain();
						claimRemain.setAmountRemain(availableClaimAmount.doubleValue());
						claimRemain.setDayRemain(0);
						claimRemain.setIsOwnSplit("F");
						claimRemain.setMapIndex(requestClaim.getMapIndex());
						remainClaim.add(claimRemain);
						this.claimRemain.setHasRemain(true);
					}
					// set to available claim
					ClaimRemain claim = new ClaimRemain();
					if (isFixed && tempfixedMaxAmount != null) {
						System.out.println("tempfixedMaxAmount : " + tempfixedMaxAmount);
						claim.setAmountRemain((new BigDecimal(requestClaim.getAmountRemain()).compareTo(tempfixedMaxAmount) > 0 ? tempfixedMaxAmount
								: new BigDecimal(requestClaim.getAmountRemain())).doubleValue());
					}
					else {
						claim.setAmountRemain((new BigDecimal(requestClaim.getAmountRemain()).compareTo(sumInsured) > 0 ? sumInsured
								: new BigDecimal(requestClaim.getAmountRemain())).doubleValue());
					}
					claim.setDayRemain(0);
					claim.setMapIndex(requestClaim.getMapIndex());
					claimAvailable.add(claim);

					// set for calculate de duct 2015-08-04
					if (currentBenefit.isDDuctType() && currentClaim.getCoverage().isCovDDuctType() && currentClaim.getCoverage().isCovDDuctCal()) {
						currentBenefit.getDeDuctList().get(dDuctIndex).setClaimAmountDeDuct(claim.getAmountRemain());
						dDuctIndex++;
					}

					currentBenefit.getClaimAmounts().add("" + claim.getAmountRemain());
					currentBenefit.getClaimDays().add("" + claim.getDayRemain());
					currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - claim.getAmountRemain());
					
					if(debug){
						for (int i = 0; i < currentBenefit.getClaimAmounts().size(); i++){
							System.out.println("ClaimAmounts : " + currentBenefit.getClaimAmounts().get(i));
						}
						System.out.println("6a - currentBenefit.getMapBenefitIndex().add(requestClaim.getMapIndex());");
						System.out.println("requestClaim.getMapIndex() = " + requestClaim.getMapIndex());
					}
					
					currentBenefit.getMapBenefitIndex().add(requestClaim.getMapIndex());
					currentBenefit.setClaimPercent(benefitGroup.getBilledClaimPercent());
					currentBenefit.getIsOwnSplit().add(requestClaim.getIsOwnSplit());
					currentBenefit.getFixedOutput().add(isFixedClaim ? "T" : "F");

					currentBenefit.setBilledPercent(benefitGroup.getBilledClaimPercent());
					currentBenefit.getBilledAmounts().add("" + requestClaim.getAmountRemain());
					currentBenefit.getBilledDays().add("" + requestClaim.getDayRemain());

					if (!isMaxYearBenefit) {
						currentClaim.getCoverage()
								.setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - claim.getAmountRemain());
						maxCoverageAmount = maxCoverageAmount.subtract(new BigDecimal(claim.getAmountRemain()));
					}

					System.out.println("MaxAvailableAmount : " + currentClaim.getCoverage().getMaxAvailableAmount());

					if (currentClaim.isRHL() && currentBenefit.isRhlFlag()) {
						currentClaim.setAvailableRHLAmount(currentClaim.getAvailableRHLAmount() - claim.getAmountRemain());
						if (currentClaim.getAvailableRHLAmount() < 0.0)
							currentClaim.setAvailableRHLAmount(0.0);
						currentClaim.setAvailableYearRHLAmount(currentClaim.getAvailableYearRHLAmount() - claim.getAmountRemain());
						if (currentClaim.getAvailableYearRHLAmount() < 0.0)
							currentClaim.setAvailableYearRHLAmount(0.0);
					}

					if (currentClaim.isRHH() && currentBenefit.isRhhFlag()) {
						if (!isMaxYearBenefit) {
							currentClaim.setAvailableRHHAmount(currentClaim.getAvailableRHHAmount() - claim.getAmountRemain());
						}
						if (currentClaim.getAvailableRHHAmount() < 0.0)
							currentClaim.setAvailableRHHAmount(0.0);
						currentClaim.setAvailableYearRHHAmount(currentClaim.getAvailableYearRHHAmount() - claim.getAmountRemain());
						if (currentClaim.getAvailableYearRHHAmount() < 0.0)
							currentClaim.setAvailableYearRHHAmount(0.0);
						}

					if (currentClaim.isRHG() && currentBenefit.isRhgFlag()) {
						currentClaim.setAvailableYearRHGAmount(currentClaim.getAvailableYearRHGAmount() - claim.getAmountRemain());
						if (currentClaim.getAvailableYearRHGAmount() < 0.0)
							currentClaim.setAvailableYearRHGAmount(0.0);
					}

					if (currentClaim.isRHP() && currentBenefit.isRhpFlag()) {
						currentClaim.setAvailableYearRHPAmount(currentClaim.getAvailableYearRHPAmount() - claim.getAmountRemain());
						currentClaim.setMaxCoverageApprovedTotal(currentClaim.getMaxCoverageApprovedTotal() - claim.getAmountRemain());
						if (currentClaim.getAvailableYearRHPAmount() < 0.0)
							currentClaim.setAvailableYearRHPAmount(0.0);
						if (currentClaim.getMaxCoverageApprovedTotal() < 0.0)
							currentClaim.setMaxCoverageApprovedTotal(0.0);
					}

					/* 20180417 jame */
					if (currentClaim.isRHE() && currentBenefit.isRheFlag()) {
						if (!isMaxYearBenefit) {
							currentClaim.setAvailableRHEAmount(currentClaim.getAvailableRHEAmount() - claim.getAmountRemain());
						}
						if (currentClaim.getAvailableRHEAmount() < 0.0)
							currentClaim.setAvailableRHEAmount(0.0);
						currentClaim.setAvailableYearRHEAmount(currentClaim.getAvailableYearRHEAmount() - claim.getAmountRemain());
						currentClaim.setMaxLifeBenefitTotal(currentClaim.getMaxLifeBenefitTotal() - claim.getAmountRemain());
						if (currentClaim.getAvailableYearRHEAmount() < 0.0)
							currentClaim.setAvailableYearRHEAmount(0.0);
						if (currentClaim.getMaxLifeBenefitTotal() < 0.0)
							currentClaim.setMaxLifeBenefitTotal(0.0);
					}

					// edit by TeTe for group sum benefit 2014-07-22
					// -----------------------------------
					if (groupSumBenefitFlag) {
						currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(
								currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() - claim.getAmountRemain());
						if (currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() < 0.0) {
							currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(0.0);
						}
					}
					// ----------------------------------------------------------------------------------

					if (currentBenefit.isMedicalExpenseSender()){
						currentClaim.setCalculatedMedicalExpense(true);
					}

					// benefitGroup.getClaimAmounts().add(claim.getAmountRemain());
					// benefitGroup.getClaimAmountDays().add((short)
					// claim.getDayRemain());
					
					if(debug){
						System.out.println("17a - setClaimBenefitGroup(benefitGroup, claim.getAmountRemain(), claim.getDayRemain(), requestClaim.getMapIndex());");
						System.out.println("requestClaim.getMapIndex() = " + requestClaim.getMapIndex());
					}
					
					benefitGroup = setClaimBenefitGroup(benefitGroup, claim.getAmountRemain(), claim.getDayRemain(), requestClaim.getMapIndex());
					benefitGroup.setClaimAmountPercent(tempClaimPercent);

					// d duct
					if (currentBenefit.isDDuctType()) {
						System.out.println("before sum coverage : " + currentClaim.getSumCoverageClaimDDuct());
						System.out.println("claim amount d duct : " + claim.getAmountRemain());
						currentClaim.addSumCoverageClaimDDuct(claim.getAmountRemain());
						System.out.println("after sum coverage : " + currentClaim.getSumCoverageClaimDDuct());
					}
				}

			}// for claimRemain

			// check claimAmount and day when can not claim
			if (currentBenefit.getClaimAmounts() == null || !(currentBenefit.getClaimAmounts().size() > 0) && !exception) {
				currentBenefit.getClaimAmounts().add("0.00");
				currentBenefit.getClaimDays().add("0");
				
				if(debug){
					for (int i = 0; i < currentBenefit.getClaimAmounts().size(); i++){
						System.out.println("ClaimAmounts : " + currentBenefit.getClaimAmounts().get(i));
					}
					System.out.println("6b - currentBenefit.getMapBenefitIndex().add(-1);");
				}
				
				currentBenefit.getMapBenefitIndex().add(-1);
				currentBenefit.setClaimPercent(benefitGroup.getBilledClaimPercent());
				currentBenefit.setBilledPercent(benefitGroup.getBilledClaimPercent());
				currentBenefit.getFixedOutput().add("F");

				if (remain != null && remain.size() > 0) {
					currentBenefit.getBilledAmounts().add("" + remain.get(0).getAmountRemain());
					currentBenefit.getBilledDays().add("" + remain.get(0).getDayRemain());
				}
				else {
					currentBenefit.getBilledAmounts().add("0.0");
					currentBenefit.getBilledDays().add("0");
				}

				currentBenefit.getIsOwnSplit().add("F");
				
				if(debug){
					System.out.println("17b - setClaimBenefitGroup(benefitGroup, 0.00, (short) 0, -1);");
				}
				
				benefitGroup = setClaimBenefitGroup(benefitGroup, 0.00, (short) 0, -1);
				benefitGroup.setClaimAmountPercent(tempClaimPercent);
			}

			/**/
			// for RS0(zero)
			// if(currentClaim.getCoverage() != null &&
			// currentClaim.getCoverage().getCoverageCode() != null &&
			// currentClaim.getCoverage().getCoverageCode().length() > 3 &&
			// currentClaim.getCoverage().getCoverageCode().substring(0,
			// 3).equalsIgnoreCase("RS0")){
			if (currentClaim.isCoverageSpecialCaseRS0()) {
				BigDecimal tempMaxCoverageAmount = new BigDecimal(maxCoverageAmount.doubleValue());
				System.out.println("case RS0");
				BigDecimal sumTotal = new BigDecimal(0);
				for (ClaimRemain remain : remainClaim) {
					System.out.println("before remain.getAmountRemain() : >>" + remain.getAmountRemain() + "<<");
					BigDecimal total = new BigDecimal(remain.getAmountRemain() * 80.00 / 100.00);
					if (total.compareTo(tempMaxCoverageAmount) < 0) {
						tempMaxCoverageAmount = tempMaxCoverageAmount.subtract(total);
					}
					else {
						total = new BigDecimal(tempMaxCoverageAmount.doubleValue());
						tempMaxCoverageAmount = new BigDecimal("0.00");
					}
					remain.setAmountRemain(remain.getAmountRemain() - total.doubleValue());
					sumTotal = sumTotal.add(total);
					System.out.println("sumTotal : >>" + sumTotal + "<<");
					System.out.println("after  remain.getAmountRemain() : >>" + remain.getAmountRemain() + "<<");
				}

				int lastIndex = 1;
				if (maxCoverageAmount.compareTo(new BigDecimal("0")) >= 0) {
					System.out.println("before sumTotal compare maxCoverageAmount : " + sumTotal);
					if (maxCoverageAmount.subtract(sumTotal).compareTo(new BigDecimal("0")) < 0) {
						sumTotal = maxCoverageAmount;
					}
					System.out.println("after  sumTotal compare maxCoverageAmount : " + sumTotal);

					if (claimAvailable != null && claimAvailable.size() > 0) {
						while (currentBenefit.getFixedOutput().size() - lastIndex >= 0) {
							System.out.println("in fixed out put");
							if (currentBenefit.getFixedOutput().get(currentBenefit.getFixedOutput().size() - lastIndex).equalsIgnoreCase("F")) {
								ClaimRemain availableRemain = claimAvailable.get(claimAvailable.size() - lastIndex);
								availableRemain.setAmountRemain(availableRemain.getAmountRemain() + sumTotal.doubleValue());
								claimAvailable.set(claimAvailable.size() - lastIndex, availableRemain);

								currentBenefit.getClaimAmounts().set(currentBenefit.getClaimAmounts().size() - lastIndex,
										"" + availableRemain.getAmountRemain());
								currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - availableRemain.getAmountRemain());
								currentBenefit.getClaimDays().set(currentBenefit.getClaimAmounts().size() - lastIndex,
										"" + availableRemain.getDayRemain());
								
								if(debug){
									System.out.println("6c - currentBenefit.getMapBenefitIndex().get(currentBenefit.getMapBenefitIndex().size() - lastIndex)");
									System.out.println("currentBenefit.getMapBenefitIndex().size() = " + currentBenefit.getMapBenefitIndex().size());
									System.out.println("lastIndex = " + lastIndex);
									
									System.out.println("17c - setClaimBenefitGroup(benefitGroup, sumTotal.doubleValue(), availableRemain.getDayRemain(), currentBenefit.getMapBenefitIndex().get(currentBenefit.getMapBenefitIndex().size() - lastIndex));");
									System.out.println("currentBenefit.getMapBenefitIndex().get(currentBenefit.getMapBenefitIndex().size() - lastIndex) = " + currentBenefit.getMapBenefitIndex().get(currentBenefit.getMapBenefitIndex().size() - lastIndex));
								}
								
								benefitGroup = setClaimBenefitGroup(benefitGroup, sumTotal.doubleValue(), availableRemain.getDayRemain(),
										currentBenefit.getMapBenefitIndex().get(currentBenefit.getMapBenefitIndex().size() - lastIndex));

								if (currentBenefit.isDDuctType()) {
									currentClaim.addSumCoverageClaimDDuct(sumTotal.doubleValue());
								}
								maxCoverageAmount = maxCoverageAmount.subtract(sumTotal);
								currentClaim.getCoverage().setMaxAvailableAmount(
										currentClaim.getCoverage().getMaxAvailableAmount() - sumTotal.doubleValue());
								lastIndex = currentBenefit.getFixedOutput().size() + 1;
								break;
							}
							else {
								lastIndex++;
							}
						}
					}
				}
			}// for RS0
			/**/

			// set value back
			currentBenefit.setClaim(claimAvailable);
			currentBenefit.setRemain(remainClaim);
			benefitGroups.set(benefitGroupIndex, benefitGroup);

			System.out.println("----------------claimAvailable-----------------");
			printArrayList(claimAvailable);
			System.out.println("----------------remainClaim--------------------");
			printArrayList(remainClaim);

			if (remainClaim != null && remainClaim.size() > 0) {
				this.claimRemain.setHasRemain(true);
			}

			remain = remainClaim;
			// set currant back
			System.out.println("currentClaimIndex : " + currentClaimIndex);
			System.out.println("benefitGroupIndex : " + benefitGroupIndex);
			System.out.println("currentBenefitIndex : " + currentClaim.getCurrentBenefitIndex());
			System.out.println("maxCoverageAmount : " + maxCoverageAmount);
			currentBenefit.fixedCalculated();
			currentClaim.setBenefit(currentBenefit);
			// currentClaim.getCoverage().setMaxAvailableAmount(maxCoverageAmount.doubleValue());
			currentClaims.set(currentClaimIndex, currentClaim);
		}
		System.out.println("out calculateClaimAmountOnly");
	}// calculate amount only

	// claim for compensate type
	/**
	 * Calculate compensate type.
	 */
	public void calculateCompensateType() {
		System.out.println("in calculateCompensateType");

		final double week = 7;
		final double divider = 100000;
		
		if(debug){
			System.out.println("currentClaimIndex = " + currentClaimIndex);
			System.out.println("currentClaims.size() = " + currentClaims.size());
			System.out.println("benefitGroupIndex = " + benefitGroupIndex);
			System.out.println("benefitGroups.size() = " + benefitGroups.size());
		}
		
		if (currentClaimIndex < currentClaims.size() && benefitGroupIndex < benefitGroups.size()) {
			Claim currentClaim = currentClaims.get(currentClaimIndex);
			BenefitGroup benefitGroup = benefitGroups.get(benefitGroupIndex);
			Benefit currentBenefit = currentClaim.getCurrentBenefit();

			boolean deformFlag = false;
			for (Benefit benefit : currentClaim.getBenefits()) {
				if (benefit != null && benefit.isDeformedCase()) {
					if (benefit.getContinuityClaimAmount() > 0) {
						deformFlag = true;
						break;
					}
				}
			}

			if (currentBenefit.isDisabledCase() && (currentClaim.isHandicapeCalculated() || deformFlag)) {
				return;
			}

			double sumInsuredDouble = (currentBenefit.getSumInsured() * currentBenefit.getPercent()) / 100;
			double sumInsuredAccident = currentBenefit.getSumInsured();

			int maxDay = 0;
			if (currentClaim.isRBO()){
				maxDay = currentBenefit.getAvailableDaysIntial();
			}
			else{
				maxDay = currentBenefit.getAvailableDays();
			}

			int claimDay = benefitGroup.getRequestDays();
			System.out.println("maxDay : " + maxDay);

			List<Integer> requestList = new ArrayList<Integer>();

			System.out.println("claimDay : " + claimDay);
			if (currentClaim.isRBO() && currentBenefit.isReceiverRBO()) {
				System.out.println("in Receive RBO");
				System.out.println("before con maxDay : " + maxDay);
				int continuityClaimDay = currentClaim.getTotalContinuityClaimDay();
				System.out.println("continuity claim day RBO: " + continuityClaimDay);
				maxDay = maxDay - continuityClaimDay;
				if (maxDay < 0)
					maxDay = 0;

				System.out.println("after con maxDay : " + maxDay);

				System.out.println("transfer claim day size : " + currentClaim.getTransferDay().size());
				if (claimDay <= 0 && currentClaim.getTransferDay().size() <= 0) {
					System.out.println("out calculateCompensateType because no request for HB1");
					return;
				}
				requestList.addAll(currentClaim.getTransferDay());
				System.out.println("requestList size 1: " + requestList.size());
				currentClaim.setTransferDay(new ArrayList<Integer>());

				if (currentClaim.isNoShareHB()) {
					System.out.println("case no share HB currentBenefit.getMaxPayDays() : " + currentBenefit.getMaxPayDays());
					maxDay = currentBenefit.getMaxPayDays() < maxDay ? currentBenefit.getMaxPayDays() : maxDay;
				}
				else {
					System.out.println("case share HB currentClaim.getDayCover() : " + currentClaim.getDayCover());
					maxDay = currentClaim.getDayCover() < maxDay ? currentClaim.getDayCover() : maxDay;
				}
			}

			double maxAmountDouble = currentBenefit.getAvailableAmount();
			System.out.println("calculateClaim max amount : " + maxAmountDouble);

			// RHL sum group
			if (currentClaim.isRHL() && currentBenefit.isRhlFlag()) {
				if (maxAmountDouble > currentClaim.getAvailableRHLAmount()) {
					maxAmountDouble = currentClaim.getAvailableRHLAmount();
					System.out.println("max coverage amount avilable RHL : " + maxAmountDouble);
				}
				if (maxAmountDouble > currentClaim.getAvailableYearRHLAmount()) {
					maxAmountDouble = currentClaim.getAvailableYearRHLAmount();
					System.out.println("max coverage year amount avilable RHL : " + maxAmountDouble);
				}
			}

			/* 20160927 */// RHH sum group
			/* 20160927 */if (currentClaim.isRHH() && currentBenefit.isRhhFlag()) {
				/* 20160927 */if (maxAmountDouble > currentClaim.getAvailableRHHAmount()) {
					/* 20160927 */maxAmountDouble = currentClaim.getAvailableRHHAmount();
					/* 20160927 */System.out.println("max coverage amount avilable RHH : " + maxAmountDouble);
					/* 20160927 */}
				/* 20160927 */if (maxAmountDouble > currentClaim.getAvailableYearRHHAmount()) {
					/* 20160927 */maxAmountDouble = currentClaim.getAvailableYearRHHAmount();
					/* 20160927 */System.out.println("max coverage year amount avilable RHH : " + maxAmountDouble);
					/* 20160927 */}
				/* 20160927 */}

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			boolean groupSumBenefitFlag = false;
			if (currentClaim.getCoverage().isGroupSumBenefit() && currentBenefit.isGroupSumBenefit()) {
				groupSumBenefitFlag = true;
			}

			if (groupSumBenefitFlag) {
				if (maxAmountDouble > currentClaim.getCoverage().getAvailableGroupSumBenefitAmount()) {
					maxAmountDouble = currentClaim.getCoverage().getAvailableGroupSumBenefitAmount();
					if (maxAmountDouble < 0) {
						maxAmountDouble = 0.0;
					}
					System.out.println("max amount after group sum benefit : " + maxAmountDouble);
				}
			}
			// ----------------------------------------------------------------------------------

			System.out.println("before   sumInsured : " + sumInsuredDouble);
			if (maxAmountDouble < sumInsuredDouble) {
				sumInsuredDouble = maxAmountDouble;
			}
			System.out.println("after    sumInsured : " + sumInsuredDouble);

			if (currentClaim.isRBO() && currentBenefit.isSenderRBO()) {
				System.out.println("in Send RBO");
				System.out.println("before con maxDay : " + maxDay);

				if (currentClaim.isNoShareHB())
					System.out.println("no share HB continuityClaimDay : " + currentBenefit.getContinuityClaimDay());
				else
					System.out.println("share HB continuityClaimDay : " + currentClaim.getTotalContinuityClaimDaySender());

				int continuityClaimDay = (currentClaim.isNoShareHB()) ? currentBenefit.getContinuityClaimDay() : currentClaim
						.getTotalContinuityClaimDaySender();

				maxDay = maxDay - continuityClaimDay;
				if (maxDay < 0)
					maxDay = 0;

				System.out.println("after con maxDay : " + maxDay);

				if (currentClaim.isNoShareHB()) {
					System.out.println("no share HB currentBenefit.getMaxPayDays() : " + currentBenefit.getMaxPayDays());
					maxDay = currentBenefit.getMaxPayDays() < maxDay ? currentBenefit.getMaxPayDays() : maxDay;
				}
				else {
					System.out.println("share HB currentClaim.getDaySpecial() : " + currentClaim.getDaySpecial());
					System.out.println("share HB currentClaim.getDayCover()   : " + currentClaim.getDayCover());
					maxDay = currentClaim.getDaySpecial() < maxDay ? currentClaim.getDaySpecial() : maxDay;
					maxDay = currentClaim.getDayCover() < maxDay ? currentClaim.getDayCover() : maxDay;
				}

				if (claimDay > maxDay) {
					int excessDay = claimDay - maxDay;
					System.out.println("before transfer claim day : " + currentClaim.getTransferDay());
					currentClaim.addTransferDay(excessDay);
					System.out.println("after  transfer claim day : " + currentClaim.getTransferDay());
				}
			}

			if (currentBenefit.isHandicappedShareDay()) {
				int limitDay = (currentClaim.getMaxHandicapDay() - currentClaim.getClaimHandicapDay())
						- currentClaim.totalContinuityHandicappedShareDay();
				maxDay = (limitDay > 0) ? limitDay : 0;
			}

			if (claimDay > 0){
				requestList.add(claimDay);
			}

			// fixed claim
			List<String> fixedInputList = (currentBenefit.getFixedInput() != null && currentBenefit.getFixedInput().size() > 0) ? currentBenefit
					.getFixedInput() : null;
			int fixedIndex = 0;

			System.out.println("request list size : " + requestList.size());
			for (int requestDay : requestList) {
				System.out.println("requestDay : " + requestDay);
				System.out.println("maxDay		: " + maxDay);
				BigDecimal maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getMaxAvailableAmount());
				System.out.println("maxCoverageAmount : " + maxCoverageAmount);
				int availableClaimDay = 0;
				BigDecimal availableClaimAmount = null;

				// check benefit to claim
				availableClaimDay = requestDay < maxDay ? requestDay : maxDay;
				// new by jkjkjk
				if (currentBenefit.isDisabledCase()) {
					availableClaimAmount = new BigDecimal(sumInsuredDouble - currentClaim.getTotalContinuityClaimCaseDisabled());
				}
				System.out.println("maxCoverageAmount : " + availableClaimAmount);

				if (currentBenefit.isHandicappedType()) {
					System.out.println("in accident case");
					String value = Double.toString(((benefitGroup.getBilledClaimPercent() > 0.0) ? benefitGroup.getBilledClaimPercent() : 1)
							* currentBenefit.getHandicappedMultiplier() * (availableClaimDay / week) * (sumInsuredAccident / divider));
					BigDecimal temp = new BigDecimal(value);
					availableClaimAmount = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
				}
				else {
					System.out.println("in health case");
					availableClaimAmount = new BigDecimal(availableClaimDay * sumInsuredDouble);
				}
				System.out.println("availableClaimDay : " + availableClaimDay);
				System.out.println("availableClaimAmount : " + availableClaimAmount);

				boolean isFixedClaim = false;
				// fixed claim
				if (currentBenefit.isFixed() && !currentBenefit.isFixedCalculated()) {
					if (fixedInputList != null && fixedInputList.size() > fixedIndex && fixedInputList.get(fixedIndex).equalsIgnoreCase("T")
							&& currentBenefit.getFixedAmounts() != null && currentBenefit.getFixedAmounts().size() > 0) {
						int fixedMaxDay = Integer.parseInt(currentBenefit.getFixedDays().get(fixedIndex).toString());
						System.out.println("currentBenefit.getFixedDays().get(" + fixedIndex + ") : " + fixedMaxDay);

						if (fixedMaxDay < availableClaimDay) {
							availableClaimDay = fixedMaxDay;
							System.out.println("fixed availableClaimDay : " + availableClaimDay);

							if (currentBenefit.isHandicappedType()) {
								String value = Double.toString(benefitGroup.getBilledClaimPercent() * currentBenefit.getHandicappedMultiplier()
										* (availableClaimDay / week) * (sumInsuredAccident / divider));
								BigDecimal temp = new BigDecimal(value);
								availableClaimAmount = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
							}
							else {
								System.out.println("in fixed health case");
								availableClaimAmount = new BigDecimal(availableClaimDay * sumInsuredDouble);
							}

							isFixedClaim = true;
						}
					}
					fixedIndex++;
					// currentBenefit.setFixedCalculated(true);
				}
				// fixed claim

				BigDecimal claimAvailable = maxCoverageAmount.subtract(availableClaimAmount);
				System.out.println("claimAvailable : " + claimAvailable);
				if (claimAvailable.compareTo(new BigDecimal(0.00)) < 0) {
					availableClaimAmount = maxCoverageAmount;
					isFixedClaim = false;
				}
				System.out.println("availableClaimAmount after  : " + availableClaimAmount);

				currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - availableClaimAmount.doubleValue());
				currentBenefit.getClaimAmounts().add("" + ((availableClaimDay > 0) ? availableClaimAmount.doubleValue() / availableClaimDay : 0));
				currentBenefit.getClaimDays().add("" + availableClaimDay);
				currentBenefit.setClaimPercent(benefitGroup.getBilledClaimPercent());
				currentBenefit.getIsOwnSplit().add("T");
				currentBenefit.getFixedOutput().add(isFixedClaim ? "T" : "F");
				
				if(debug){
					System.out.println("4a - currentBenefit.getMapBenefitIndex().add(-1);" );
				}
				
				currentBenefit.getMapBenefitIndex().add(-1);

				if (currentClaim.isRHL() && currentBenefit.isRhlFlag()) {
					currentClaim.setAvailableRHLAmount(currentClaim.getAvailableRHLAmount() - availableClaimAmount.doubleValue());
					if (currentClaim.getAvailableRHLAmount() < 0.0)
						currentClaim.setAvailableRHLAmount(0.0);
					currentClaim.setAvailableYearRHLAmount(currentClaim.getAvailableYearRHLAmount() - availableClaimAmount.doubleValue());
					if (currentClaim.getAvailableYearRHLAmount() < 0.0)
						currentClaim.setAvailableYearRHLAmount(0.0);
				}

				/* 20160927 */if (currentClaim.isRHH() && currentBenefit.isRhhFlag()) {
					/* 20160927 */currentClaim.setAvailableRHHAmount(currentClaim.getAvailableRHHAmount() - availableClaimAmount.doubleValue());
					/* 20160927 */if (currentClaim.getAvailableRHHAmount() < 0.0)
						/* 20160927 */currentClaim.setAvailableRHHAmount(0.0);
					/* 20160927 */currentClaim.setAvailableYearRHHAmount(currentClaim.getAvailableYearRHHAmount()
							- availableClaimAmount.doubleValue());
					/* 20160927 */if (currentClaim.getAvailableYearRHHAmount() < 0.0)
						/* 20160927 */currentClaim.setAvailableYearRHHAmount(0.0);
					/* 20160927 */}
				
				if(debug){
					System.out.println("15a - setClaimBenefitGroup(benefitGroup, (availableClaimDay > 0) ? availableClaimAmount.doubleValue() / availableClaimDay : 0, availableClaimDay, -1);");
				}
				
				benefitGroup = setClaimBenefitGroup(benefitGroup, (availableClaimDay > 0) ? availableClaimAmount.doubleValue() / availableClaimDay
						: 0, availableClaimDay, -1);

				currentClaim.setDayCover(currentClaim.getDayCover() - availableClaimDay);
				currentClaim.setDaySpecial(currentClaim.getDaySpecial() - availableClaimDay);

				// edit by TeTe for group sum benefit 2014-07-22
				// -----------------------------------
				if (groupSumBenefitFlag) {
					currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(
							currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() - availableClaimAmount.doubleValue());
					if (currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() < 0.0) {
						currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(0.0);
					}
				}
				// ----------------------------------------------------------------------------------

				if (currentBenefit.isHandicappedShareDay()) {
					currentClaim.setClaimHandicapDay(currentClaim.getClaimHandicapDay() + availableClaimDay);
				}

				maxDay = maxDay - availableClaimDay;

				// d duct
				if (currentBenefit.isDDuctType()) {
					currentClaim.addSumCoverageClaimDDuct(availableClaimAmount.doubleValue());
				}

				maxCoverageAmount = maxCoverageAmount.subtract(availableClaimAmount);
				currentClaim.getCoverage().setMaxAvailableAmount(
						currentClaim.getCoverage().getMaxAvailableAmount() - availableClaimAmount.doubleValue());
				// for display in Application
				currentBenefit.getBilledAmounts().add("0.00");
				currentBenefit.getBilledDays().add("" + requestDay);
				currentBenefit.setBilledPercent(benefitGroup.getBilledClaimPercent());
				
				if(debug){
					System.out.println("currentBenefit.getClaimAmounts() = " + currentBenefit.getClaimAmounts());
					if(currentBenefit.getClaimAmounts() != null){
						System.out.println("currentBenefit.getClaimAmounts().size() = " + currentBenefit.getClaimAmounts().size());
					}
				}
				
				if (currentBenefit.getClaimAmounts() == null || currentBenefit.getClaimAmounts().size() <= 0) {
					currentBenefit.getClaimAmounts().add("0.00");
					currentBenefit.getClaimDays().add("0");
					currentBenefit.setClaimPercent(benefitGroup.getBilledClaimPercent());
					currentBenefit.getIsOwnSplit().add("F");
					currentBenefit.getFixedOutput().add("F");
					
					if(debug){
						System.out.println("4b - currentBenefit.getMapBenefitIndex().add(-1);" );
					}
					
					currentBenefit.getMapBenefitIndex().add(-1);

					if (remain != null && remain.size() > 0) {
						currentBenefit.getBilledAmounts().add("0.0");
						currentBenefit.getBilledDays().add("" + requestDay);
						currentBenefit.setBilledPercent(benefitGroup.getBilledClaimPercent());
					}
					else {
						currentBenefit.getBilledAmounts().add("0.0");
						currentBenefit.getBilledDays().add("0");
						currentBenefit.setBilledPercent(benefitGroup.getBilledClaimPercent());
					}
					
					if(debug){
						System.out.println("15b - setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);");
					}
					
					benefitGroup = setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);
				}

				// set value back
				// benefitGroups.set(benefitGroupIndex, benefitGroup);
				currentBenefit.fixedCalculated();
				currentClaim.setBenefit(currentBenefit);
				// currentClaim.getCoverage().setMaxAvailableAmount(maxCoverageAmount.doubleValue());
			}// for(int requestDay:requestList)
			currentClaims.set(currentClaimIndex, currentClaim);
			benefitGroups.set(benefitGroupIndex, benefitGroup);
		}
		System.out.println("out calculateCompensateType");
	}

	// claim for compensate

	// claim for accident percent
	public void calculateAccidentPercent() {
		System.out.println("in calculateAccidentPercent");
		
		if(debug){
			System.out.println("currentClaimIndex = " + currentClaimIndex);
			System.out.println("currentClaims.size() = " + currentClaims.size());
			System.out.println("benefitGroupIndex = " + benefitGroupIndex);
			System.out.println("benefitGroups.size() = " + benefitGroups.size());
		}
		
		if (currentClaimIndex < currentClaims.size() && benefitGroupIndex < benefitGroups.size()) {
			Claim currentClaim = currentClaims.get(currentClaimIndex);
			BenefitGroup benefitGroup = benefitGroups.get(benefitGroupIndex);
			Benefit currentBenefit = currentClaim.getCurrentBenefit();

			double conAmountDeformedCase = 0.0;
			if (currentBenefit.isDeformedCase()) {
				currentClaim.setHandicapeCalculated(true);
				for (Benefit benefit : currentClaim.getBenefits()) {
					if (benefit != null && benefit.isDisabledCase()) {
						conAmountDeformedCase = conAmountDeformedCase + benefit.getContinuityClaimAmount();
					}
				}
			}
			double sumPercent = benefitGroup.getBilledMaxPercent();
			/*
			 * comment benefit RDD
			 * if(currentClaim.getCoverage().getCoverageGroupCode
			 * ().equals("RDP")){
			 * if(!(currentBenefit.getBenefitCode().equals("DPE01"
			 * )||currentBenefit.getBenefitCode().equals("DPD01"))){ double
			 * tempSumPercent = sumPercent - currentBenefit.getMaxPercentRDP();
			 * if(tempSumPercent<0.0)tempSumPercent = 0.0;
			 * System.out.println("tempSumPercent : " + tempSumPercent);
			 * //if(sumPercent > tempSumPercent)sumPercent = tempSumPercent;
			 * sumPercent = tempSumPercent; } }
			 * if(currentBenefit.isInRDDPeriod()){ sumPercent = 1.2;//fixed
			 * BilledAmount for RDD701 }
			 */
			if (currentBenefit.isLockPAByYearCase()){
				sumPercent = (sumPercent < currentBenefit.getAvailablePercent()) ? sumPercent : currentBenefit.getAvailablePercent();
			}

			double sumInsuredDouble = 0.0;
			if (currentBenefit.isMotorCycleFlag()){
				sumInsuredDouble = (currentBenefit.getSumInsured() * (currentBenefit.getMotorCyclePercent()) * sumPercent) / 100;
			}
			else{
				sumInsuredDouble = (currentBenefit.getSumInsured() * (currentBenefit.getPercent() / 100) * sumPercent) / 100;
			}

			if (currentBenefit.getCalculateOption().equals(CalculateOption.COMPENSATE_PERCENTAGE_FUND) && currentBenefit.isLostFlag()) {
				// sumInsuredDouble = sumInsuredDouble *
				// currentBenefit.getPercent();
				// edit by TeTe change from P'Fon
				// add condition HPB1 20time by ging from P'x
				if (currentBenefit.getContinuityClaimPercent() == 20 && currentBenefit.getBenefitGroup().equals("HDIM1")) {
					sumInsuredDouble = sumInsuredDouble * 0;
				}
				else {
					sumInsuredDouble = sumInsuredDouble * 100;
				}
			}

			// pay time
			/*
			 * if(currentClaim.getCoverage().getCoverageGroupCode().equals("RDP")
			 * ){
			 * if(!(currentBenefit.getBenefitCode().equals("DPE01")||currentBenefit
			 * .getBenefitCode().equals("DPD01"))){ sumInsuredDouble =
			 * sumInsuredDouble - currentBenefit.getMaxPercentRDP(); } }
			 */
			if (currentBenefit.isHandicappedType()) {
				sumInsuredDouble = sumInsuredDouble * currentBenefit.getHandicappedMultiplier() / 100;
			}

			sumInsuredDouble = sumInsuredDouble - conAmountDeformedCase;
			if (sumInsuredDouble < 0.0){
				sumInsuredDouble = 0.0;
			}

			// fixed claim
			List<String> fixedInputList = (currentBenefit.getFixedInput() != null && currentBenefit.getFixedInput().size() > 0) ? currentBenefit
					.getFixedInput() : null;
			int fixedIndex = 0;

			BigDecimal maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getMaxAvailableAmount());
			BigDecimal availableClaimAmount = new BigDecimal(sumInsuredDouble);
			
			System.out.println("maxCoverageAmount : " + maxCoverageAmount);
			System.out.println("sumInsuredDouble : " + sumInsuredDouble);

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			boolean groupSumBenefitFlag = false;
			if (currentClaim.getCoverage().isGroupSumBenefit() && currentBenefit.isGroupSumBenefit()) {
				groupSumBenefitFlag = true;
			}

			if (groupSumBenefitFlag) {
				if (maxCoverageAmount.compareTo(new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount())) > 0) {
					maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount());
					if (maxCoverageAmount.compareTo(new BigDecimal(0)) < 0) {
						maxCoverageAmount = new BigDecimal(0);
					}
					System.out.println("maxCoverageAmount after group sum benefit : " + maxCoverageAmount);
				}
			}
			// ----------------------------------------------------------------------------------

			boolean isFixedClaim = false;
			// fixed claim
			if (currentBenefit.isFixed() && !currentBenefit.isFixedCalculated()) {
				if (fixedInputList != null && fixedInputList.size() > fixedIndex && fixedInputList.get(fixedIndex).equalsIgnoreCase("T")
						&& currentBenefit.getFixedAmounts() != null && currentBenefit.getFixedAmounts().size() > 0) {
					BigDecimal fixedMaxAmount = new BigDecimal(currentBenefit.getFixedAmounts().get(fixedIndex).toString());
					System.out.println("currentBenefit.getFixedDays().get(" + fixedIndex + ") : " + fixedMaxAmount);

					if (fixedMaxAmount.compareTo(availableClaimAmount) < 0) {
						availableClaimAmount = fixedMaxAmount;
						System.out.println("fixed availableClaimAmount : " + availableClaimAmount);
						isFixedClaim = true;
					}
				}
				fixedIndex++;
			}
			// fixed claim

			// jame add for rdd
			double continuity = 0.0;
			if (currentBenefit.getCalculateOption().equals(CalculateOption.COMPENSATE_PERCENTAGE_FUND)) {
				if (currentBenefit.getContinuityClaimAmount() > 0) {
					continuity = currentBenefit.getContinuityClaimAmount();
				}
				System.out.println("continuity : " + continuity);

				double sumContinuity = sumInsuredDouble - continuity;
				System.out.println("sumContinuity : " + sumContinuity);
				if (sumContinuity < 0)
					sumContinuity = 0;

				currentBenefit.setAvailableAmount(sumContinuity);
			}// end rdd

			System.out.println("currentBenefit.getAvailableAmount() : " + currentBenefit.getAvailableAmount());

			if (maxCoverageAmount.compareTo(availableClaimAmount) < 0) {
				availableClaimAmount = maxCoverageAmount;
				isFixedClaim = false;
			}
			if (new BigDecimal(currentBenefit.getAvailableAmount()).compareTo(availableClaimAmount) < 0) {
				availableClaimAmount = new BigDecimal(currentBenefit.getAvailableAmount());
				isFixedClaim = false;
			}
			System.out.println("availableClaimAmount after  : " + availableClaimAmount);

			currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - availableClaimAmount.doubleValue());
			if (currentBenefit.isLockPAByYearCase()) {
				double percent = currentBenefit.getAvailablePercent() - sumPercent;
				currentBenefit.setAvailablePercent((percent > 0) ? percent : 0.0);
			}
			currentBenefit.getClaimAmounts().add("" + availableClaimAmount);
			currentBenefit.getClaimDays().add("0");
			currentBenefit.setClaimPercent(sumPercent);
			currentBenefit.getIsOwnSplit().add("T");
			currentBenefit.getFixedOutput().add(isFixedClaim ? "T" : "F");
			
			if(debug){
				System.out.println("1a - currentBenefit.getMapBenefitIndex().add(-1);" );
			}
			
			currentBenefit.getMapBenefitIndex().add(-1);

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			if (groupSumBenefitFlag) {
				currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(
						currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() - availableClaimAmount.doubleValue());
				if (currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() < 0.0) {
					currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(0.0);
				}
			}
			// ----------------------------------------------------------------------------------
			
			if(debug){
				System.out.println("12a - setClaimBenefitGroup(benefitGroup, availableClaimAmount.doubleValue(), 0, -1);");
			}
			
			benefitGroup = setClaimBenefitGroup(benefitGroup, availableClaimAmount.doubleValue(), 0, -1);

			// d duct
			if (currentBenefit.isDDuctType()) {
				currentClaim.addSumCoverageClaimDDuct(availableClaimAmount.doubleValue());
			}

			maxCoverageAmount = maxCoverageAmount.subtract(availableClaimAmount);
			currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - availableClaimAmount.doubleValue());
			// for display in Application
			currentBenefit.getBilledAmounts().add("0.00");
			currentBenefit.getBilledDays().add("0");
			currentBenefit.setBilledPercent(benefitGroup.getBilledMaxPercent());

			if(debug){
				System.out.println("currentBenefit.getClaimAmounts() = " + currentBenefit.getClaimAmounts());
				if(currentBenefit.getClaimAmounts() != null){
					System.out.println("currentBenefit.getClaimAmounts().size() = " + currentBenefit.getClaimAmounts().size());
				}
			}
			
			if (currentBenefit.getClaimAmounts() == null || currentBenefit.getClaimAmounts().size() <= 0) {
				currentBenefit.getClaimAmounts().add("0.00");
				currentBenefit.getClaimDays().add("0");
				currentBenefit.setClaimPercent(sumPercent);
				currentBenefit.getIsOwnSplit().add("F");
				currentBenefit.getFixedOutput().add("F");
				
				if(debug){
					System.out.println("1b - currentBenefit.getMapBenefitIndex().add(-1);");
				}
				
				currentBenefit.getMapBenefitIndex().add(-1);

				currentBenefit.getBilledAmounts().add("0.0");
				currentBenefit.getBilledDays().add("0");
				
				if(debug){
					System.out.println("12b - setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);");
				}
				
				benefitGroup = setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);
			}

			// set value back
			// benefitGroups.set(benefitGroupIndex, benefitGroup);
			currentBenefit.fixedCalculated();
			currentClaim.setBenefit(currentBenefit);
			// currentClaim.getCoverage().setMaxAvailableAmount(maxCoverageAmount.doubleValue());
			currentClaims.set(currentClaimIndex, currentClaim);
			benefitGroups.set(benefitGroupIndex, benefitGroup);
		}
		
		System.out.println("out calculateAccidentPercent");
	}

	// claim for accident percent

	public void calculateCaseTemporaryDisablement() {
		System.out.println("in calculateCaseTemporaryDisablement");
		
		if(debug){
			System.out.println("currentClaimIndex = " + currentClaimIndex);
			System.out.println("currentClaims.size() = " + currentClaims.size());
			System.out.println("benefitGroupIndex = " + benefitGroupIndex);
			System.out.println("benefitGroups.size() = " + benefitGroups.size());
		}
		
		if (currentClaimIndex < currentClaims.size() && benefitGroupIndex < benefitGroups.size()) {
			Claim currentClaim = currentClaims.get(currentClaimIndex);
			BenefitGroup benefitGroup = benefitGroups.get(benefitGroupIndex);
			Benefit currentBenefit = currentClaim.getCurrentBenefit();

			int diffYear = CalendarHelper.getDiffYear(currentClaim.getCoverage().getIssuedDate(), currentClaim.getCoverage().getMaturityDate());

			double sumInsured = (currentClaim.getCoverage().getFaceAmount() / (diffYear * 12));
			System.out.println("sumInsured : " + sumInsured);
			sumInsured = Math.ceil(sumInsured);// edit by TeTe form round to
												// ceil for CR up dot 2014-07-23
			System.out.println("cal sumInsured : " + sumInsured);

			// fixed claim
			List<String> fixedInputList = (currentBenefit.getFixedInput() != null && currentBenefit.getFixedInput().size() > 0) ? currentBenefit
					.getFixedInput() : null;
			int fixedIndex = 0;

			if (currentBenefit.getMaxClaimAmount() < sumInsured) {
				sumInsured = currentBenefit.getMaxClaimAmount();
			}

			BigDecimal maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getMaxAvailableAmount());
			BigDecimal availableClaimAmount = new BigDecimal(sumInsured);
			System.out.println("maxCoverageAmount : " + maxCoverageAmount);
			System.out.println("sumInsured : " + sumInsured);

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			boolean groupSumBenefitFlag = false;
			if (currentClaim.getCoverage().isGroupSumBenefit() && currentBenefit.isGroupSumBenefit()) {
				groupSumBenefitFlag = true;
			}

			if (groupSumBenefitFlag) {
				if (maxCoverageAmount.compareTo(new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount())) > 0) {
					maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getAvailableGroupSumBenefitAmount());
					if (maxCoverageAmount.compareTo(new BigDecimal(0)) < 0) {
						maxCoverageAmount = new BigDecimal(0);
					}
					System.out.println("maxCoverageAmount after group sum benefit : " + maxCoverageAmount);
				}
			}
			// ----------------------------------------------------------------------------------

			boolean isFixedClaim = false;
			// fixed claim
			if (currentBenefit.isFixed() && !currentBenefit.isFixedCalculated()) {
				if (fixedInputList != null && fixedInputList.size() > fixedIndex && fixedInputList.get(fixedIndex).equalsIgnoreCase("T")
						&& currentBenefit.getFixedAmounts() != null && currentBenefit.getFixedAmounts().size() > 0) {
					BigDecimal fixedMaxAmount = new BigDecimal(currentBenefit.getFixedAmounts().get(fixedIndex).toString());
					System.out.println("currentBenefit.getFixedAmounts().get(" + fixedIndex + ") : " + fixedMaxAmount);

					if (fixedMaxAmount.compareTo(availableClaimAmount) < 0) {
						availableClaimAmount = fixedMaxAmount;
						System.out.println("fixed availableClaimAmount : " + availableClaimAmount);
						isFixedClaim = true;
					}
				}
				fixedIndex++;
			}
			// fixed claim

			if (maxCoverageAmount.compareTo(availableClaimAmount) < 0) {
				availableClaimAmount = maxCoverageAmount;
				isFixedClaim = false;
			}
			System.out.println("availableClaimAmount after  : " + availableClaimAmount);
			currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - availableClaimAmount.doubleValue());
			currentBenefit.getClaimAmounts().add("" + availableClaimAmount);
			currentBenefit.getClaimDays().add("0");
			currentBenefit.getIsOwnSplit().add("T");
			currentBenefit.getFixedOutput().add(isFixedClaim ? "T" : "F");
			
			if(debug){
				System.out.println("3a - currentBenefit.getMapBenefitIndex().add(-1);" );
			}
			
			currentBenefit.getMapBenefitIndex().add(-1);

			// edit by TeTe for group sum benefit 2014-07-22
			// -----------------------------------
			if (groupSumBenefitFlag) {
				currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(
						currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() - availableClaimAmount.doubleValue());
				if (currentClaim.getCoverage().getAvailableGroupSumBenefitAmount() < 0.0) {
					currentClaim.getCoverage().setAvailableGroupSumBenefitAmount(0.0);
				}
			}
			// ----------------------------------------------------------------------------------
			
			if(debug){
				System.out.println("14a - setClaimBenefitGroup(benefitGroup, availableClaimAmount.doubleValue(), 0, -1);");
			}
			
			benefitGroup = setClaimBenefitGroup(benefitGroup, availableClaimAmount.doubleValue(), 0, -1);

			// d duct
			if (currentBenefit.isDDuctType()) {
				currentClaim.addSumCoverageClaimDDuct(availableClaimAmount.doubleValue());
			}

			maxCoverageAmount = maxCoverageAmount.subtract(availableClaimAmount);
			currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - availableClaimAmount.doubleValue());
			// for display in Application
			currentBenefit.getBilledAmounts().add("0.00");
			currentBenefit.getBilledDays().add("0");
			currentBenefit.setBilledPercent(benefitGroup.getBilledMaxPercent());

			if(debug){
				System.out.println("currentBenefit.getClaimAmounts() = " + currentBenefit.getClaimAmounts());
				if(currentBenefit.getClaimAmounts() != null){
					System.out.println("currentBenefit.getClaimAmounts().size() = " + currentBenefit.getClaimAmounts().size());
				}
			}
			
			if (currentBenefit.getClaimAmounts() == null || currentBenefit.getClaimAmounts().size() <= 0) {
				currentBenefit.getClaimAmounts().add("0.00");
				currentBenefit.getClaimDays().add("0");
				currentBenefit.getIsOwnSplit().add("F");
				currentBenefit.getFixedOutput().add("F");
				
				if(debug){
					System.out.println("3b - currentBenefit.getMapBenefitIndex().add(-1);" );
				}
				
				currentBenefit.getMapBenefitIndex().add(-1);

				currentBenefit.getBilledAmounts().add("0.0");
				currentBenefit.getBilledDays().add("0");
				
				if(debug){
					System.out.println("14b - setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);");
				}
				
				benefitGroup = setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);
			}

			// set value back
			// benefitGroups.set(benefitGroupIndex, benefitGroup);
			currentBenefit.fixedCalculated();
			currentClaim.setBenefit(currentBenefit);
			// currentClaim.getCoverage().setMaxAvailableAmount(maxCoverageAmount.doubleValue());
			currentClaims.set(currentClaimIndex, currentClaim);
			benefitGroups.set(benefitGroupIndex, benefitGroup);
		}
		System.out.println("out calculateCaseTemporaryDisablement");
	}

	public void calculateCaseCriticalIllness() {
		System.out.println("in calculateCaseCriticalIllness");
		
		if(debug){
			System.out.println("currentClaimIndex = " + currentClaimIndex);
			System.out.println("currentClaims.size() = " + currentClaims.size());
			System.out.println("benefitGroupIndex = " + benefitGroupIndex);
			System.out.println("benefitGroups.size() = " + benefitGroups.size());
		}
		
		if (currentClaimIndex < currentClaims.size() && benefitGroupIndex < benefitGroups.size()) {
			Claim currentClaim = currentClaims.get(currentClaimIndex);
			BenefitGroup benefitGroup = benefitGroups.get(benefitGroupIndex);
			Benefit currentBenefit = currentClaim.getCurrentBenefit();

			Double sumInsured = currentBenefit.getInsuredAmount();
			Double percent = currentBenefit.getPercent();
			System.out.println("sumInsured : " + sumInsured);
			System.out.println("percent 	: " + percent);
			System.out.println("cal sumInsured : " + (sumInsured * (percent / 100)));

			// fixed claim
			List<String> fixedInputList = (currentBenefit.getFixedInput() != null && currentBenefit.getFixedInput().size() > 0) ? currentBenefit
					.getFixedInput() : null;
			int fixedIndex = 0;

			if (currentBenefit.getMaxClaimAmount() < sumInsured){
				sumInsured = currentBenefit.getMaxClaimAmount();
			}

			System.out.println("maxClaimAmount : " + currentBenefit.getMaxClaimAmount());

			BigDecimal maxCoverageAmount = new BigDecimal(currentClaim.getCoverage().getMaxAvailableAmount());
			BigDecimal availableClaimAmount = new BigDecimal(sumInsured);
			System.out.println("maxCoverageAmount : " + maxCoverageAmount);
			System.out.println("sumInsured : " + availableClaimAmount);

			boolean isFixedClaim = false;
			// fixed claim
			if (currentBenefit.isFixed() && !currentBenefit.isFixedCalculated()) {
				if (fixedInputList != null && fixedInputList.size() > fixedIndex && fixedInputList.get(fixedIndex).equalsIgnoreCase("T")
						&& currentBenefit.getFixedAmounts() != null && currentBenefit.getFixedAmounts().size() > 0) {
					BigDecimal fixedMaxAmount = new BigDecimal(currentBenefit.getFixedAmounts().get(fixedIndex).toString());
					System.out.println("currentBenefit.getFixedAmounts().get(" + fixedIndex + ") : " + fixedMaxAmount);

					if (fixedMaxAmount.compareTo(availableClaimAmount) < 0) {
						availableClaimAmount = fixedMaxAmount;
						System.out.println("fixed availableClaimAmount : " + availableClaimAmount);
						isFixedClaim = true;
					}
				}
				fixedIndex++;
			}
			// fixed claim

			if (maxCoverageAmount.compareTo(availableClaimAmount) < 0) {
				availableClaimAmount = maxCoverageAmount;
				isFixedClaim = false;
			}
			System.out.println("availableClaimAmount after  : " + availableClaimAmount);
			
			currentBenefit.setAvailableAmount(currentBenefit.getAvailableAmount() - availableClaimAmount.doubleValue());
			currentBenefit.getClaimAmounts().add("" + availableClaimAmount);
			currentBenefit.getClaimDays().add("0");
			currentBenefit.getIsOwnSplit().add("T");
			currentBenefit.getFixedOutput().add(isFixedClaim ? "T" : "F");
			
			if(debug){
				System.out.println("2a - currentBenefit.getMapBenefitIndex().add(-1);" );
			}
			
			currentBenefit.getMapBenefitIndex().add(-1);
			
			if(debug){
				System.out.println("13a - setClaimBenefitGroup(benefitGroup, availableClaimAmount.doubleValue(), 0, -1);");
			}
			benefitGroup = setClaimBenefitGroup(benefitGroup, availableClaimAmount.doubleValue(), 0, -1);

			// d duct
			if (currentBenefit.isDDuctType()) {
				currentClaim.addSumCoverageClaimDDuct(availableClaimAmount.doubleValue());
			}

			maxCoverageAmount = maxCoverageAmount.subtract(availableClaimAmount);
			currentClaim.getCoverage().setMaxAvailableAmount(currentClaim.getCoverage().getMaxAvailableAmount() - availableClaimAmount.doubleValue());
			// for display in Application
			currentBenefit.getBilledAmounts().add("0.00");
			currentBenefit.getBilledDays().add("0");
			currentBenefit.setBilledPercent(benefitGroup.getBilledMaxPercent());
			
			if(debug){
				System.out.println("currentBenefit.getClaimAmounts() = " + currentBenefit.getClaimAmounts());
				if(currentBenefit.getClaimAmounts() != null){
					System.out.println("currentBenefit.getClaimAmounts().size() = " + currentBenefit.getClaimAmounts().size());
				}
			}
			
			if (currentBenefit.getClaimAmounts() == null || currentBenefit.getClaimAmounts().size() <= 0) {
				currentBenefit.getClaimAmounts().add("0.00");
				currentBenefit.getClaimDays().add("0");
				currentBenefit.getIsOwnSplit().add("F");
				currentBenefit.getFixedOutput().add("F");
				
				if(debug){
					System.out.println("2b - currentBenefit.getMapBenefitIndex().add(-1);" );
				}
				
				currentBenefit.getMapBenefitIndex().add(-1);

				currentBenefit.getBilledAmounts().add("0.0");
				currentBenefit.getBilledDays().add("0");
				
				if(debug){
					System.out.println("13b - setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);");
				}
				
				benefitGroup = setClaimBenefitGroup(benefitGroup, 0.00, 0, -1);
			}

			currentBenefit.fixedCalculated();
			currentClaim.setBenefit(currentBenefit);
			
			currentClaims.set(currentClaimIndex, currentClaim);
			benefitGroups.set(benefitGroupIndex, benefitGroup);
		}
		System.out.println("out calculateCaseCriticalIllness");
	}

		 /**
 		 * Check remain.
 		 *
 		 * @return true, if successful
 		 */
 		public boolean checkRemain(){
			 if(remain != null && remain.size() > 0)
				 return true;
			 return false;
		 }
		 
	//claim
		/**
		 * Calculate d duct.
		 *
		 * @return true, if successful
		 */
	public boolean calculateDDuct() {
		System.out.println("-----------------------in calculateDDuct---------------------");
		resetCurrentClaimIndex();
		double sumTotal = 0.00;
		while (hasCurrentClaim()) {
			Claim claim = currentClaims.get(currentClaimIndex);

			if (claim.getCoverage().isCovDDuctType() && !(claim.getCoverage().isCovDDuctCal())) {

				double dDuctAmount = (claim.getCoverage().getCovDDuctAmount() - claim.getTotalContinuityAmountDDuct())
						- (claim.getBilledDeDuct() + claim.getTotalContinuityDDuct());

				boolean dductFlag = false;
				if (claim.getBenefits() != null && claim.getBenefits().size() > 0) {
					for (Benefit benefit : claim.getBenefits()) {
						if (benefit.isDDuctType() && benefit.getContinuityClaimAmount() > 0) {
							double continueClaimAmount = (benefit.getContinuityClaimDay() > 0 ? benefit.getContinuityClaimAmount()
									* benefit.getContinuityClaimDay() : benefit.getContinuityClaimAmount());
							double dductContinue = benefit.getContinuityDeDuctAmount();
							System.out.println("benefit continue claim   amount : " + continueClaimAmount);
							System.out.println("benefit continue de duct amount : " + dductContinue);
							if (!(dductContinue > 0.0)) {
								dductFlag = true;
								break;
							}
						}// d duct type
					}// for benefit
				}
				if (dductFlag){
					dDuctAmount = 0.0;
				}
				System.out.println("sumTotal    : " + sumTotal);
				System.out.println("dDuctAmount : " + dDuctAmount);
				dDuctAmount = dDuctAmount - sumTotal > 0 ? dDuctAmount - sumTotal : 0;
				System.out.println("dDuctAmount : " + dDuctAmount);
				claim.getCoverage().setCovDDuctCal(true);
				claim.setTotalClaimDeDuct(sumTotal);
				claim.setTotalRemainDeDuct(dDuctAmount);
				currentClaims.set(currentClaimIndex, claim);
				System.out.println("-----------------------out calculateDDuct is true-----------------------");
				return true;
			}
			sumTotal += claim.getSumCoverageClaimDDuct() + claim.getTotalContinuityAmountDDuct();

			if (claim.getCoverage().isCovDDuctType() && claim.getCoverage().isCovDDuctCal() && claim.getBenefits() != null
					&& claim.getBenefits().size() > 0) {
				for (Benefit benefit : claim.getBenefits()) {
					if (benefit.isDDuctType()) {
						for (DeDuct deDuct : benefit.getDeDuctList()) {
							if (deDuct.isCalculateDeDuct()) {
								System.out.println("calculated benefit claim de duct : " + deDuct.getClaimDeDuct());
								System.out.println("calculated benefit claim day : " + deDuct.getClaimDayDeDuct());
								sumTotal = sumTotal
										+ (deDuct.getClaimDeDuct() * ((deDuct.getClaimDayDeDuct() != null) ? deDuct.getClaimDayDeDuct() : 1));
							}
						}
					}
				}
			}

			nextCurrentClaim();
		}
		System.out.println("-----------------------out calculateDDuct is false-----------------------");
		return false;
	}
	//d duct calculate	 
	
	//get and set method
	/**
	 * Gets the benefit groups.
	 *
	 * @return the benefit groups
	 */
	public List<BenefitGroup> getBenefitGroups() {
		return benefitGroups;
	}
	
	/**
	 * Sets the benefit groups.
	 *
	 * @param benefitGroups the new benefit groups
	 */
	public void setBenefitGroups(List<BenefitGroup> benefitGroups) {
		this.benefitGroups = benefitGroups;
	}
	
	/**
	 * Gets the current claims.
	 *
	 * @return the current claims
	 */
	public List<Claim> getCurrentClaims() {
		return currentClaims;
	}
	
	/**
	 * Sets the current claims.
	 *
	 * @param currentClaims the new current claims
	 */
	public void setCurrentClaims(List<Claim> currentClaims) {
		this.currentClaims = currentClaims;
	}
	
	/**
	 * Gets the fixed claims.
	 *
	 * @return the fixed claims
	 */
	public List<Claim> getFixedClaims() {
		return fixedClaims;
	}
	
	/**
	 * Sets the fixed claims.
	 *
	 * @param fixedClaims the new fixed claims
	 */
	public void setFixedClaims(List<Claim> fixedClaims) {
		this.fixedClaims = fixedClaims;
	}
	
	/**
	 * Gets the current claim index.
	 *
	 * @return the current claim index
	 */
	public int getCurrentClaimIndex() {
		return currentClaimIndex;
	}
	
	/**
	 * Sets the current claim index.
	 *
	 * @param currentClaimIndex the new current claim index
	 */
	public void setCurrentClaimIndex(int currentClaimIndex) {
		this.currentClaimIndex = currentClaimIndex;
	}
	
	/**
	 * Gets the benefit group index.
	 *
	 * @return the benefit group index
	 */
	public int getBenefitGroupIndex() {
		return benefitGroupIndex;
	}
	
	/**
	 * Sets the benefit group index.
	 *
	 * @param benefitGroupIndex the new benefit group index
	 */
	public void setBenefitGroupIndex(int benefitGroupIndex) {
		this.benefitGroupIndex = benefitGroupIndex;
	}
	
	/**
	 * Gets the claim remain.
	 *
	 * @return the claim remain
	 */
	public ClaimRemain getClaimRemain() {
		return claimRemain;
	}
	
	/**
	 * Sets the claim remain.
	 *
	 * @param claimRemain the new claim remain
	 */
	public void setClaimRemain(ClaimRemain claimRemain) {
		this.claimRemain = claimRemain;
	}
	
	/**
	 * Gets the request.
	 *
	 * @return the request
	 */
	public List<ClaimRemain> getRequest() {
		return request;
	}
	
	/**
	 * Sets the request.
	 *
	 * @param request the new request
	 */
	public void setRequest(List<ClaimRemain> request) {
		this.request = request;
	}
	
	/**
	 * Gets the claim.
	 *
	 * @return the claim
	 */
	public List<ClaimRemain> getClaim() {
		return claim;
	}
	
	/**
	 * Sets the claim.
	 *
	 * @param claim the new claim
	 */
	public void setClaim(List<ClaimRemain> claim) {
		this.claim = claim;
	}
	
	/**
	 * Gets the remain.
	 *
	 * @return the remain
	 */
	public List<ClaimRemain> getRemain() {
		return remain;
	}
	
	/**
	 * Sets the remain.
	 *
	 * @param remain the new remain
	 */
	public void setRemain(List<ClaimRemain> remain) {
		this.remain = remain;
	}
	
	/**
	 * Gets the claim transfers.
	 *
	 * @return the claim transfers
	 */
	public List<ClaimTransfer> getClaimTransfers() {
		return claimTransfers;
	}
	
	/**
	 * Sets the claim transfers.
	 *
	 * @param claimTransfers the new claim transfers
	 */
	public void setClaimTransfers(List<ClaimTransfer> claimTransfers) {
		this.claimTransfers = claimTransfers;
	}
	
	/**
	 * Gets the claim transfer.
	 *
	 * @return the claim transfer
	 */
	public ClaimTransfer getClaimTransfer() {
		return claimTransfer;
	}
	
	/**
	 * Sets the claim transfer.
	 *
	 * @param claimTransfer the new claim transfer
	 */
	public void setClaimTransfer(ClaimTransfer claimTransfer) {
		this.claimTransfer = claimTransfer;
	}

	/**
	 * Gets the map claim index.
	 *
	 * @return the map claim index
	 */
	public int getMapClaimIndex() {
		return mapClaimIndex;
	}

	/**
	 * Sets the map claim index.
	 *
	 * @param mapClaimIndex the new map claim index
	 */
	public void setMapClaimIndex(int mapClaimIndex) {
		this.mapClaimIndex = mapClaimIndex;
	}
	
	public boolean checkClaimTransfer(){
		if(claimTransfers != null && claimTransfers.size() > 0){
			System.out.println("claimTransfers size : " + claimTransfers.size());
			return true;
		}
		System.out.println("claimTransfers size is null");
		return false;
	}

	public double getMedicalExpenseRemain() {
		return medicalExpenseRemain;
	}

	public void setMedicalExpenseRemain(double medicalExpenseRemain) {
		this.medicalExpenseRemain = medicalExpenseRemain;
	}
	
	public double sumAmountRemain(){
		 double amount 	= 0.0;
		 for(ClaimRemain claimRemain:remain){
			 amount	= amount + claimRemain.getAmountRemain();
		 }
		 return amount;
	 }
	
	public double checkClaimDay(int maxPayDays,double maxTime){
		 if(maxTime>=maxPayDays){
			 return maxPayDays;
		 }
		 return maxPayDays-maxTime;
	 }

	public List<String> getClaimNumbers() {
		return claimNumbers;
	}

	public void setClaimNumbers(List<String> claimNumbers) {
		this.claimNumbers = claimNumbers;
	}
	
	public void requestRHH(BenefitGroup benefitGroup){
		claimRemain = new ClaimRemain();
		int mapIndexClaimRemain = mapClaimIndex;
		if(benefitGroup.getClaimAmounts().size()>0 && benefitGroup.getClaimAmounts()!=null){
			for(int i = 0;i < benefitGroup.getClaimAmounts().size();i++){
				Double tempClaimAmount = benefitGroup.getBilledAmount() - benefitGroup.getClaimAmounts().get(i);
				Short tempClaimDay = benefitGroup.getClaimAmountDays().get(i);
				 claimRemain.setAmountRemain(tempClaimAmount);
				 claimRemain.setDayRemain(tempClaimDay);
				 claimRemain.setMapIndex(mapIndexClaimRemain);
				 claimRemain.setIsOwnSplit("F");	
				 addMapClaimIndex();
				 addClaimRemain(claimRemain);
			}
		}else{
			 claimRemain.setAmountRemain(benefitGroup.getBilledAmount());
			 claimRemain.setDayRemain(benefitGroup.getBilledAmountDays());
			 claimRemain.setMapIndex(mapIndexClaimRemain);
			 claimRemain.setIsOwnSplit("T");	
			 addMapClaimIndex();
			 addClaimRemain(claimRemain);
		}
		
		
		
	}
	public Boolean checkIsRHH(){
		for(Claim tempCurrentClaim:currentClaims){
			if(tempCurrentClaim.isRHH()){
			return true;
			}
		}
		return false;
	}

}
