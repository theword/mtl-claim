/*
 * 
 */
package com.mtl.benefitAnalysis.model.mtlclaim.master;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.mtl.benefitAnalysis.model.enumerator.CalculateOption;
import com.mtl.benefitAnalysis.model.templates.IMasterModel;


/**
 * The Class BenefitGroupMs.
 */
@XmlRootElement(name = "BenefitGroupMs")
@XmlAccessorType(XmlAccessType.FIELD)
public class BenefitGroupMs implements IMasterModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6036707647857958201L;
	
	/** The benefit group code. */
	@XmlElement(name = "benefitGroupCode", required = false)
	private String benefitGroupCode;
	
	/** The benefit group short name. */
	@XmlElement(name = "benefitGroupShortName", required = false)
	private String benefitGroupShortName;
	
	/** The calculate option. */
	@XmlElement(name = "calculateOption", required = false)
	private CalculateOption calculateOption;
	
	/**
	 * Instantiates a new benefit group ms.
	 */
	private BenefitGroupMs() {
		
	}
	
	/**
	 * Instantiates a new benefit group ms.
	 *
	 * @param benefitGroupCode the benefit group code
	 * @param calculateOption the calculate option
	 */
	public BenefitGroupMs(String benefitGroupCode, CalculateOption calculateOption) {
		this();
		setCode(benefitGroupCode);
		setCalculateOption(calculateOption);
	}

	/**
	 * Instantiates a new benefit group ms.
	 *
	 * @param benefitGroupCode the benefit group code
	 * @param calculateOption the calculate option
	 * @param benefitGroupShortName the benefit group short name
	 */
	public BenefitGroupMs(String benefitGroupCode, CalculateOption calculateOption, String benefitGroupShortName) {
		this();
		setCode(benefitGroupCode);
		setCalculateOption(calculateOption);
		setBenefitGroupShortName(benefitGroupShortName);
	}
	
	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#getCode()
	 */
	@Override
	public String getCode() {
		return benefitGroupCode;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.IMasterModel#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.benefitGroupCode = code;
	}

	/**
	 * Sets the calculate option.
	 *
	 * @param calculateOption the new calculate option
	 */
	public void setCalculateOption(CalculateOption calculateOption) {
		this.calculateOption = calculateOption;
	}

	/**
	 * Gets the calculate option.
	 *
	 * @return the calculate option
	 */
	public CalculateOption getCalculateOption() {
		return calculateOption;
	}

	/**
	 * Gets the benefit group short name.
	 *
	 * @return the benefit group short name
	 */
	public String getBenefitGroupShortName() {
		return benefitGroupShortName;
	}

	/**
	 * Sets the benefit group short name.
	 *
	 * @param benefitGroupShortName the new benefit group short name
	 */
	public void setBenefitGroupShortName(String benefitGroupShortName) {
		this.benefitGroupShortName = benefitGroupShortName;
	}

}
