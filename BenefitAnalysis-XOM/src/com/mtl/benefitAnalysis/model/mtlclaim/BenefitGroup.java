/*
 * 
 */
package com.mtl.benefitAnalysis.model.mtlclaim;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.mtl.benefitAnalysis.model.enumerator.CalculateOption;
import com.mtl.benefitAnalysis.model.mtlclaim.master.BenefitGroupMs;
import com.mtl.benefitAnalysis.model.templates.IMasterModel;
import com.mtl.benefitAnalysis.model.templates.ITransactionModel;
import com.mtl.benefitAnalysis.util.DateTimeAdapter;


/**
 * The Class BenefitGroup.
 */
@XmlRootElement(name = "BenefitGroup")
@XmlAccessorType(XmlAccessType.FIELD)
public class BenefitGroup implements ITransactionModel, Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6482559989828914517L;
	
	/** The benefit group. */
	@XmlElement(name = "benefitGroup", required = false)
	private BenefitGroupMs benefitGroup;
	
	/** The group key. */
	@XmlElement(name = "groupKey", required = false)
	private String groupKey;
	
	/** The billed amount. */
	@XmlElement(name = "billedAmount", required = false)
	private double billedAmount;
	
	/** The request amount. */
	@XmlElement(name = "requestAmount", required = false)
	private double requestAmount;
	
	/** The billed amount percent. */
	@XmlElement(name = "billedAmountPercent", required = false)
	private float billedAmountPercent;
	
	/** The billed claim percent. */
	@XmlElement(name = "billedClaimPercent", required = false)
	private float billedClaimPercent;
	
	@XmlElement(name = "billedMaxPercent", required = false)
	private float billedMaxPercent = 0;
	
	/** The billed amount days. */
	@XmlElement(name = "billedAmountDays", required = false)
	private int billedAmountDays;
	
	/** The request days. */
	@XmlElement(name = "requestDays", required = false)
	private int requestDays;
	
	/** The claim amounts. */
	@XmlElement(name = "claimAmounts", required = false)
	private List<Double> claimAmounts;
	
	/** The claim amount percent. */
	@XmlElement(name = "claimAmountPercent", required = false)
	private float claimAmountPercent;
	
	/** The claim amount days. */
	@XmlElement(name = "claimAmountDays", required = false)
	private List<Short> claimAmountDays;
	
	/** The continuity amount. */
	@XmlElement(name = "continuityAmount", required = false)
	private double continuityAmount;
	
	/** The continuity amount percent. */
	@XmlElement(name = "continuityAmountPercent", required = false)
	private float continuityAmountPercent;
	
	/** The continuity amount days. */
	@XmlElement(name = "continuityAmountDays", required = false)
	private int continuityAmountDays;
	
	@XmlElement(name = "continuityDeDuct", required = false)
	private double continuityDeDuct;
	
	/** The request amount list. */
	@XmlElement(name = "requestAmountList", required = false)
	private List<String> requestAmountList;
	
	/** The request days list. */
	@XmlElement(name = "requestDaysList", required = false)
	private List<String> requestDaysList;
	
	/** The is request added. */
	@XmlElement(name = "isRequestAdded", required = false)
	private boolean isRequestAdded;
	
	/** The benefit priority. */
	@XmlElement(name = "benefitPriority", required = false)
	private int benefitPriority;
	
	/** The d_ duct type. */
	@XmlElement(name = "d_DuctType", required = false)
	private boolean d_DuctType;
	
	/** The ICU and rb type. */
	@XmlElement(name = "ICUAndRBType", required = false)
	private boolean ICUAndRBType;
	
	/** The DV type. */
	@XmlElement(name = "DVType", required = false)
	private boolean DVType;
	
	/** The Lab and old ge type. */
	@XmlElement(name = "LabAndOldGEType", required = false)
	private boolean LabAndOldGEType;
	
	/** The GE type. */
	@XmlElement(name = "GEType", required = false)
	private boolean GEType;
	
	/** The ICU type. */
	@XmlElement(name = "ICUType", required = false)
	private boolean ICUType;
	
	/** The HB accident type. */
	@XmlElement(name = "HBAccidentType", required = false)
	private boolean HBAccidentType;
	
	/** The map benefit index. */
	@XmlElement(name = "mapBenefitIndex", required = false)
	private List<Integer> mapBenefitIndex;
	
	/** The benefit group index. */
	@XmlElement(name = "benefitGroupIndex", required = false)
	private int benefitGroupIndex;
	
	/** The billed claim de duct. */
	@XmlElement(name = "billedClaimDeDuct", required = false)
	private double billedClaimDeDuct;
	
	@XmlJavaTypeAdapter(value=DateTimeAdapter.class, type=Date.class)
	@XmlElement(name = "treatmentDate", required = false)
	private Date treatmentDate;
	
	@XmlElement(name = "HB", required = false)
	private boolean HB;
	
	@XmlElement(name = "medicalExpenseSender", required = false)
	private boolean medicalExpenseSender;
	
	@XmlElement(name = "medicalExpenseReceiver", required = false)
	private boolean medicalExpenseReceiver;
	
	@XmlElement(name = "HM", required = false)
	private boolean HM;
	
	@XmlElement(name = "groupType", required = false)
	private List<String> groupType = new ArrayList<String>();
	
	@XmlElement(name = "flagCalculate", required = false)
	private boolean flagCalculate;


	/**
	 * Instantiates a new benefit group.
	 */
	private BenefitGroup() {
		billedAmount 			= 0;
		billedAmountPercent 	= 0;
		billedAmountDays 		= 0;
		billedClaimPercent		= 0;
		claimAmounts 			= new ArrayList<Double>();
		claimAmountPercent 		= 0;
		claimAmountDays 		= new ArrayList<Short>();
		continuityAmount 		= 0;
		continuityAmountPercent = 0;
		continuityAmountDays 	= 0;
		continuityDeDuct		= 0;
		isRequestAdded 			= false;
		requestAmountList 		= new ArrayList<String>();
		requestDaysList 		= new ArrayList<String>();
		benefitPriority 		= 0;
		d_DuctType 				= false;
		ICUAndRBType			= false;
		DVType					= false;	
		LabAndOldGEType			= false;
		mapBenefitIndex			= new ArrayList<Integer>();
		GEType					= false;
		ICUType					= false;
		benefitGroupIndex		= -1;
		HBAccidentType			= false;
		billedClaimDeDuct		= 0;
		treatmentDate			= null;
		HB						= false;
		medicalExpenseSender	= false;
		medicalExpenseReceiver	= false;
		HM						= false;
	}
	
	/**
	 * Instantiates a new benefit group.
	 *
	 * @param benefitGroup the benefit group
	 * @param groupKey the group key
	 * @param billedAmount the billed amount
	 * @param billedAmountPercent the billed amount percent
	 * @param billedAmountDays the billed amount days
	 */
	public BenefitGroup(BenefitGroupMs benefitGroup, String groupKey, double billedAmount,
			float billedAmountPercent, int billedAmountDays) {
		this();
		setMasterModel(benefitGroup);
		setGroupKey(groupKey);
		setBilledAmount(billedAmount);
		setRequestAmount(billedAmount);
		setBilledAmountDays(billedAmountDays);
		setRequestDays(billedAmountDays);
		setBilledAmountPercent(billedAmountPercent);
		setBilledClaimPercent(billedAmountPercent);
	}

	/**
	 * Reset to cal.
	 */
	public void resetToCal(){
		claimAmounts 		= new ArrayList<Double>();
		claimAmountPercent 	= 0;
		claimAmountDays 	= new ArrayList<Short>();
		mapBenefitIndex		= new ArrayList<Integer>();
	}
	
	/**
	 * Calculate billed amount days.
	 *
	 * @param billedAmountDays the billed amount days
	 */
	public void calculateBilledAmountDays(int billedAmountDays) {
		this.billedAmountDays = (this.billedAmountDays > billedAmountDays)?this.billedAmountDays - billedAmountDays:0;
	}
	
	/**
	 * Sets the billed amount.
	 *
	 * @param billedAmount the new billed amount
	 */
	public void setBilledAmount(double billedAmount) {
		this.billedAmount = billedAmount;
	}

	/**
	 * Gets the billed amount.
	 *
	 * @return the billed amount
	 */
	public double getBilledAmount() {
		return billedAmount;
	}

	/**
	 * Sets the billed amount days.
	 *
	 * @param billedAmountDays the new billed amount days
	 */
	public void setBilledAmountDays(int billedAmountDays) {
		this.billedAmountDays = billedAmountDays;
	}
	
	/**
	 * Gets the billed amount days.
	 *
	 * @return the billed amount days
	 */
	public int getBilledAmountDays() {
		return billedAmountDays;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#getMasterModel()
	 */
	@Override
	public BenefitGroupMs getMasterModel() {
		return benefitGroup;
	}

	/* (non-Javadoc)
	 * @see com.mtl.model.templates.ITransactionModel#setMasterModel(com.mtl.model.templates.IMasterModel)
	 */
	@Override
	public void setMasterModel(IMasterModel masterModel) {
		benefitGroup = (BenefitGroupMs)masterModel;
	}

	/**
	 * Sets the billed amount percent.
	 *
	 * @param billedAmountPercent the new billed amount percent
	 */
	public void setBilledAmountPercent(float billedAmountPercent) {
		this.billedAmountPercent = billedAmountPercent;
	}

	/**
	 * Gets the billed amount percent.
	 *
	 * @return the billed amount percent
	 */
	public float getBilledAmountPercent() {
		return billedAmountPercent;
	}
	
	/**
	 * Gets the benefit group code.
	 *
	 * @return the benefit group code
	 */
	public String getBenefitGroupCode() {
		return benefitGroup.getCode();
	}
	
	/**
	 * Gets the benefit group short name.
	 *
	 * @return the benefit group short name
	 */
	public String getBenefitGroupShortName() {
		return benefitGroup.getBenefitGroupShortName();
	}
	
	/**
	 * Gets the calculate option.
	 *
	 * @return the calculate option
	 */
	public CalculateOption getCalculateOption(){
		return benefitGroup.getCalculateOption();
	}

	/**
	 * Sets the request amount list.
	 *
	 * @param requestAmountList the new request amount list
	 */
	public void setRequestAmountList(List<String> requestAmountList) {
		this.requestAmountList = requestAmountList;
	}

	/**
	 * Gets the request amount list.
	 *
	 * @return the request amount list
	 */
	public List<String> getRequestAmountList() {
		return requestAmountList;
	}

	/**
	 * Sets the request days list.
	 *
	 * @param requestDaysList the new request days list
	 */
	public void setRequestDaysList(List<String> requestDaysList) {
		this.requestDaysList = requestDaysList;
	}

	/**
	 * Gets the request days list.
	 *
	 * @return the request days list
	 */
	public List<String> getRequestDaysList() {
		return requestDaysList;
	}

	/**
	 * Sets the claim amount percent.
	 *
	 * @param claimAmountPercent the new claim amount percent
	 */
	public void setClaimAmountPercent(float claimAmountPercent) {
		this.claimAmountPercent = claimAmountPercent;
	}

	/**
	 * Gets the claim amount percent.
	 *
	 * @return the claim amount percent
	 */
	public float getClaimAmountPercent() {
		return claimAmountPercent;
	}

	/**
	 * Sets the continuity amount.
	 *
	 * @param continuityAmount the new continuity amount
	 */
	public void setContinuityAmount(double continuityAmount) {
		this.continuityAmount = continuityAmount;
	}

	/**
	 * Gets the continuity amount.
	 *
	 * @return the continuity amount
	 */
	public double getContinuityAmount() {
		return continuityAmount;
	}

	/**
	 * Sets the continuity amount percent.
	 *
	 * @param continuityAmountPercent the new continuity amount percent
	 */
	public void setContinuityAmountPercent(float continuityAmountPercent) {
		this.continuityAmountPercent = continuityAmountPercent;
	}

	/**
	 * Gets the continuity amount percent.
	 *
	 * @return the continuity amount percent
	 */
	public float getContinuityAmountPercent() {
		return continuityAmountPercent;
	}

	/**
	 * Sets the continuity amount days.
	 *
	 * @param continuityAmountDays the new continuity amount days
	 */
	public void setContinuityAmountDays(int continuityAmountDays) {
		this.continuityAmountDays = continuityAmountDays;
	}

	/**
	 * Gets the continuity amount days.
	 *
	 * @return the continuity amount days
	 */
	public int getContinuityAmountDays() {
		return continuityAmountDays;
	}

	/**
	 * Sets the claim amount days.
	 *
	 * @param claimAmountDays the new claim amount days
	 */
	public void setClaimAmountDays(List<Short> claimAmountDays) {
		this.claimAmountDays = claimAmountDays;
	}

	/**
	 * Gets the claim amount days.
	 *
	 * @return the claim amount days
	 */
	public List<Short> getClaimAmountDays() {
		return claimAmountDays;
	}

	/**
	 * Sets the claim amounts.
	 *
	 * @param claimAmounts the new claim amounts
	 */
	public void setClaimAmounts(List<Double> claimAmounts) {
		this.claimAmounts = claimAmounts;
	}

	/**
	 * Gets the claim amounts.
	 *
	 * @return the claim amounts
	 */
	public List<Double> getClaimAmounts() {
		return claimAmounts;
	}

	/**
	 * Sets the group key.
	 *
	 * @param groupKey the new group key
	 */
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	/**
	 * Gets the group key.
	 *
	 * @return the group key
	 */
	public String getGroupKey() {
		return groupKey;
	}

	/**
	 * Sets the request amount.
	 *
	 * @param requestAmount the new request amount
	 */
	public void setRequestAmount(double requestAmount) {
		this.requestAmount = requestAmount;
	}

	/**
	 * Gets the request amount.
	 *
	 * @return the request amount
	 */
	public double getRequestAmount() {
		return requestAmount;
	}

	/**
	 * Sets the request days.
	 *
	 * @param requestDays the new request days
	 */
	public void setRequestDays(int requestDays) {
		this.requestDays = requestDays;
	}

	/**
	 * Gets the request days.
	 *
	 * @return the request days
	 */
	public int getRequestDays() {
		return requestDays;
	}
	
	/**
	 * Gets the incurred amount.
	 *
	 * @return the incurred amount
	 */
	public double getIncurredAmount(){
		if(billedAmountDays > 0){
			return billedAmountDays * billedAmount;
		}else{
			return billedAmount;
		}
	}
	
	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public List<Double> getTotalAmount(){
		ArrayList<Double> totalAmounts = new ArrayList<Double>();
		if(billedAmountDays > 0){
			for(int i = 0; i < claimAmounts.size(); i++){
				totalAmounts.add(claimAmounts.get(i) * claimAmountDays.get(i));
			}
		}else{
			for(int i = 0; i < claimAmounts.size(); i++){
				totalAmounts.add(claimAmounts.get(i));
			}
		}
		return totalAmounts;
	}
	
	/**
	 * Gets the excess amount.
	 *
	 * @return the excess amount
	 */
	public double getExcessAmount(){
		if(billedAmount == 0){
			return 0;
		}
		double incurredAmount = getIncurredAmount();
		if(claimAmounts != null && claimAmounts.size() > 0){
			double excessAmount = 0;
			for(Double d : getTotalAmount()){
				excessAmount += d;
			}
			return incurredAmount - excessAmount;
		}
		return incurredAmount;
	}

	/**
	 * Sets the request added.
	 *
	 * @param isRequestAdded the new request added
	 */
	public void setRequestAdded(boolean isRequestAdded) {
		this.isRequestAdded = isRequestAdded;
	}

	/**
	 * Checks if is request added.
	 *
	 * @return true, if is request added
	 */
	public boolean isRequestAdded() {
		return isRequestAdded;
	}

	/**
	 * Gets the benefit priority.
	 *
	 * @return the benefit priority
	 */
	public int getBenefitPriority() {
		return benefitPriority;
	}

	/**
	 * Sets the benefit priority.
	 *
	 * @param benefitPriority the new benefit priority
	 */
	public void setBenefitPriority(int benefitPriority) {
		this.benefitPriority = benefitPriority;
	}

	/**
	 * Checks if is d_ duct type.
	 *
	 * @return true, if is d_ duct type
	 */
	public boolean isD_DuctType() {
		return d_DuctType;
	}

	/**
	 * Sets the d_ duct type.
	 *
	 * @param d_DuctType the new d_ duct type
	 */
	public void setD_DuctType(boolean d_DuctType) {
		this.d_DuctType = d_DuctType;
	}

	/**
	 * Checks if is iCU and rb.
	 *
	 * @return true, if is iCU and rb
	 */
	public boolean isICUAndRB() {
		return ICUAndRBType;
	}

	/**
	 * Sets the iCU and rb.
	 *
	 * @param iCUAndRB the new iCU and rb
	 */
	public void setICUAndRB(boolean iCUAndRB) {
		ICUAndRBType = iCUAndRB;
	}

	/**
	 * Checks if is dv.
	 *
	 * @return true, if is dv
	 */
	public boolean isDV() {
		return DVType;
	}

	/**
	 * Sets the dv.
	 *
	 * @param dVType the new dv
	 */
	public void setDV(boolean dVType) {
		DVType = dVType;
	}

	/**
	 * Checks if is lab and old ge.
	 *
	 * @return true, if is lab and old ge
	 */
	public boolean isLabAndOldGE() {
		return LabAndOldGEType;
	}

	/**
	 * Sets the lab and old ge.
	 *
	 * @param labAndOldGE the new lab and old ge
	 */
	public void setLabAndOldGE(boolean labAndOldGE) {
		LabAndOldGEType = labAndOldGE;
	}

	/**
	 * Gets the map benefit index.
	 *
	 * @return the map benefit index
	 */
	public List<Integer> getMapBenefitIndex() {
		return mapBenefitIndex;
	}

	/**
	 * Sets the map benefit index.
	 *
	 * @param mapBenefitIndex the new map benefit index
	 */
	public void setMapBenefitIndex(List<Integer> mapBenefitIndex) {
		this.mapBenefitIndex = mapBenefitIndex;
	}

	/**
	 * Checks if is gE type.
	 *
	 * @return true, if is gE type
	 */
	public boolean isGEType() {
		return GEType;
	}

	/**
	 * Sets the gE type.
	 *
	 * @param gEType the new gE type
	 */
	public void setGEType(boolean gEType) {
		GEType = gEType;
	}

	/**
	 * Gets the billed claim percent.
	 *
	 * @return the billed claim percent
	 */
	public float getBilledClaimPercent() {
		return billedClaimPercent;
	}

	/**
	 * Sets the billed claim percent.
	 *
	 * @param billedClaimPercent the new billed claim percent
	 */
	public void setBilledClaimPercent(float billedClaimPercent) {
		this.billedClaimPercent = billedClaimPercent;
	}

	/**
	 * Checks if is iCU type.
	 *
	 * @return true, if is iCU type
	 */
	public boolean isICUType() {
		return ICUType;
	}

	/**
	 * Sets the iCU type.
	 *
	 * @param iCUType the new iCU type
	 */
	public void setICUType(boolean iCUType) {
		ICUType = iCUType;
	}

	/**
	 * Gets the benefit group index.
	 *
	 * @return the benefit group index
	 */
	public int getBenefitGroupIndex() {
		return benefitGroupIndex;
	}

	/**
	 * Sets the benefit group index.
	 *
	 * @param benefitGroupIndex the new benefit group index
	 */
	public void setBenefitGroupIndex(int benefitGroupIndex) {
		this.benefitGroupIndex = benefitGroupIndex;
	}

	/**
	 * Checks if is hB accident type.
	 *
	 * @return true, if is hB accident type
	 */
	public boolean isHBAccidentType() {
		return HBAccidentType;
	}

	/**
	 * Sets the hB accident type.
	 *
	 * @param hBAccidentType the new hB accident type
	 */
	public void setHBAccidentType(boolean hBAccidentType) {
		HBAccidentType = hBAccidentType;
	}

	public double getBilledClaimDeDuct() {
		return billedClaimDeDuct;
	}

	public void setBilledClaimDeDuct(double billedClaimDeDuct) {
		this.billedClaimDeDuct = billedClaimDeDuct;
	}

	public double getContinuityDeDuct() {
		return continuityDeDuct;
	}

	public void setContinuityDeDuct(double continuityDeDuct) {
		this.continuityDeDuct = continuityDeDuct;
	}

	public Date getTreatmentDate() {
		return treatmentDate;
	}

	public void setTreatmentDate(Date treatmentDate) {
		this.treatmentDate = treatmentDate;
	}

	public boolean isHB() {
		return HB;
	}

	public void setHB(boolean hB) {
		HB = hB;
	}

	public boolean isMedicalExpenseSender() {
		return medicalExpenseSender;
	}

	public void setMedicalExpenseSender(boolean medicalExpenseSender) {
		this.medicalExpenseSender = medicalExpenseSender;
	}

	public boolean isMedicalExpenseReceiver() {
		return medicalExpenseReceiver;
	}

	public void setMedicalExpenseReceiver(boolean medicalExpenseReceiver) {
		this.medicalExpenseReceiver = medicalExpenseReceiver;
	}

	public boolean isHM() {
		return HM;
	}

	public void setHM(boolean hM) {
		HM = hM;
	}

	public float getBilledMaxPercent() {
		return billedMaxPercent;
	}

	public void setBilledMaxPercent(float billedMaxPercent) {
		this.billedMaxPercent = billedMaxPercent;
	}

	public List<String> getGroupType() {
		return groupType;
	}

	public void setGroupType(List<String> groupType) {
		this.groupType = groupType;
	}
	
	public boolean isFlagCalculate() {
		return flagCalculate;
	}

	public void setFlagCalculate(boolean flagCalculate) {
		this.flagCalculate = flagCalculate;
	}

}
