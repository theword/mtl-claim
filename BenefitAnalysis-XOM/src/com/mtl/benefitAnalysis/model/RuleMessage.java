/*
 * 
 */
package com.mtl.benefitAnalysis.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RuleMessage")
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleMessage implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5879743283996951110L;
	
	@XmlElement(name = "messageCode", required = false)
	private String messageCode;
	
	/** The desc code. */
	@XmlElement(name = "descCode", required = false)
	private String descCode;
	
	/** The priority. */
	@XmlElement(name = "priority", required = false)
	private short priority;
	
	@XmlElement(name = "message", required = false)
	private String message;
	
	/** The message map code. */
	@XmlElement(name = "messageMapCode", required = false)
	private String messageMapCode;
	
	/**
	 * Instantiates a new rule message.
	 */
	public RuleMessage() {
		message = "";
		messageCode = "";
		descCode = "";
		priority = Short.MAX_VALUE;
		messageMapCode = "";
	}
	
	/**
	 * Instantiates a new rule message.
	 *
	 * @param messageCode the message code
	 */
	public RuleMessage(String messageCode){
		this();
		setMessageCode(messageCode);
		setMessage(messageCode);
	}
	
	/**
	 * Instantiates a new rule message.
	 *
	 * @param messageCode the message code
	 * @param descCode the desc code
	 */
	public RuleMessage(String messageCode,String descCode){
		this();
		setMessageCode(messageCode);
		setMessage(messageCode);
		setDescCode(descCode);
	} 

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		if(this.descCode!=""){
			this.message = "["+this.descCode+"]| "+message;
		}else{
			this.message = message;
		}		
	}
	
	/**
	 * Sets the rule message.
	 *
	 * @param message the new rule message
	 */
	public void setRuleMessage(String message) {
		this.messageCode = message;
		this.message = message;	
	}

	/**
	 * Sets the message rule.
	 *
	 * @param message the message
	 * @param show the show
	 * @param planName the plan name
	 */
	public void setMessageRule(String message, boolean show,String planName) {
		if(show){
			this.message = planName + " " + message;
		}else{
			this.message = message;
		}		
	}
	
	/**
	 * Adds the message rule.
	 *
	 * @param message the message
	 */
	public void addMessageRule(String message) {
		this.message = this.message + message;
	}
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(short priority) {
		this.priority = priority;
	}

	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public short getPriority() {
		return priority;
	}

	/**
	 * Sets the message code.
	 *
	 * @param messageCode the new message code
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;		
	}

	/**
	 * Gets the message code.
	 *
	 * @return the message code
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * Gets the desc code.
	 *
	 * @return the desc code
	 */
	public String getDescCode() {
		return descCode;
	}

	/**
	 * Sets the desc code.
	 *
	 * @param descCode the new desc code
	 */
	public void setDescCode(String descCode) {
		this.descCode = descCode;
	}

	/**
	 * Gets the message map code.
	 *
	 * @return the message map code
	 */
	public String getMessageMapCode() {
		return messageMapCode;
	}

	/**
	 * Sets the message map code.
	 *
	 * @param messageMapCode the new message map code
	 */
	public void setMessageMapCode(String messageMapCode) {
		this.messageMapCode = messageMapCode;
	}
}
